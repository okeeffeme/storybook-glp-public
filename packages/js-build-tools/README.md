# Confirmit JS Build Tools

## getModuleCssLoader

It returns `css-loader` instance that process css files of confirmit-react-packages.
This can be used as drop-in replacement for the css-loader for modules in most cases.
In webpack config add loader as

```js
import {getCssModuleLoader} from '@confirmit/js-build-tools';

module.exports = {
  module: {
    rules: [
      {
        test: /\.module\.(scss|css)$/,
        use: [
          'style-loader',
          getCssModuleLoader({
            importLoaders: 1,
          }),
          'sass-loader',
        ],
      },
    ],
  },
};
```

If you need to customize classnames even more you can provide `localIdentName` and `confirmitReactPackagesLocalIdentName`.
See API section below for advanced usage.

```js
import {getCssModuleLoader} from '@confirmit/js-build-tools';
const widgetName = process.env.widgetName;
module.exports = {
  module: {
    rules: [
      {
        test: /\.module\.(scss|css)$/,
        use: [
          'style-loader',
          getCssModuleLoader({
            importLoaders: 1,
            localIdentName: `[local]___widgetName`,
            confirmitReactPackagesLocalIdentName: `[local]_[confirmit-identifier]___${widgetName}`,
          }),
          'sass-loader',
        ],
      },
    ],
  },
};
```

By default it adds unique identifier to modules that match `/node_modules(\\|\/)(confirmit-|@confirmit).*\.module\.css$/i` regexp.
All other modules process as with regular `css-module` loader

### Api

getCssModuleLoader takes some options:

#### `importLoaders`

Type: `Number`
Default: `0`

Enables/Disables or setups number of loaders applied before CSS loader.

See https://github.com/webpack-contrib/css-loader#importloaders

#### `sourceMap`

Type: `Boolean`
Default: `false`

Enables/Disables generation of source maps.

See https://github.com/webpack-contrib/css-loader#sourcemap

#### `localIdentName`

Type: `String`
Default: `'[local]_[confirmit-identifier]'`

Pattern that is used to generated classNames for every module except that matches `confirmitReactPackagesRegExp`.
Check tokens on [loaderUtils.interpolateName](https://github.com/webpack/loader-utils#interpolatename)
that is used under the hood. Extra tokens supported:

- `[local]` token refers to a original className

#### `confirmitReactPackagesLocalIdentName`

Type: `String`
Default: `'[local]_[confirmit-identifier]'`

Pattern that is used to generated classNames for modules that match `confirmitReactPackagesRegExp`.
Check tokens on [loaderUtils.interpolateName](https://github.com/webpack/loader-utils#interpolatename)
that is used under the hood. Extra tokens supported:

- `[local]` token refers to a original className
- `[confirmit-identifier]` token refers to a uniquely generated identifier base on resource path. This is integer sequence by default.

If empty string is passed then all modules will be processed with `localIdentName` pattern

#### `confirmitReactPackagesRegExp`

Type: `RegExp`
Default: `/node_modules(\\|\/)(confirmit-|@confirmit).*\.module\.css$/i`

RegExp that is used to find css modules in Forsta packages

#### `getClassNameIdentifier`

Type: `Function`
Default `undefined`

Overrides logic of generating class name identifier that will be used as `[confirmit-identifier]`

Takes 4 parameters

- `context: webpack.LoaderContext` context of file
- `localIdentName` - the same as `localIdentName` option that passed to loader
- `localName` - original class name
- `options` - options that are used in [loaderUtils.interpolateName](https://github.com/webpack/loader-utils#interpolatename).
  Generated `confirmitIdentifier` passed as option as well.
