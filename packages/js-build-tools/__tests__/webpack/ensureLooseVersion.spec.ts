import {ensureLooseVersion} from '../../src/webpack/ensureLooseVersion';

describe('ensureLooseVersion', () => {
  it('should set the version as loose if not loose already', () => {
    expect(ensureLooseVersion('17.1.1')).toBe('^17.1.1');
  });

  it('should not change the version if it already contains ^', () => {
    expect(ensureLooseVersion('^17.1.1')).toBe('^17.1.1');
  });

  it('should not change the version if it already contains *', () => {
    expect(ensureLooseVersion('17.*')).toBe('17.*');
  });

  it('should not change the version if it already contains *', () => {
    expect(ensureLooseVersion('~17')).toBe('~17');
  });

  it('should not change the version if it already contains *', () => {
    expect(ensureLooseVersion('17.1.*')).toBe('17.1.*');
  });
});
