import {getSharedDependencies} from '../../src/webpack/getSharedDependencies';

const alwaysSetFixture = {
  react: {
    singleton: true,
    requiredVersion: '^16.8 || ^17 || ^18',
  },
  'react-dom': {
    singleton: true,
    requiredVersion: '^16.8 || ^17 || ^18',
  },
  '@emotion/react': {
    requiredVersion: '^11.1.1',
  },
};

const commonSetFixture = {
  moment: {
    requiredVersion: '^1.2.3',
  },
};

const allDeps = {
  redux: '1.2.3',
  'react-redux': '1.2.3',
  '@reduxjs/toolkit': '1.2.3',
  'react-responsive': '1.2.3',
  'react-rnd': '1.2.3',
  'react-transition-group': '1.2.3',
  'react-use': '1.2.3',
  moment: '1.2.3',
  'moment-timezone': '1.2.3',
};

describe('getSharedDependencies', () => {
  it('should always contain fixed libraries', () => {
    expect(getSharedDependencies({}, {})).toEqual(alwaysSetFixture);
  });

  it('should include common libraries, if present', () => {
    expect(getSharedDependencies(allDeps, {})).toEqual({
      ...alwaysSetFixture,
      ...commonSetFixture,
    });
  });

  it('should not change versions for extraShared, this is up to consumer', () => {
    expect(
      getSharedDependencies(allDeps, {
        ['lib1']: {
          singleton: true,
          requiredVersion: '1.0.0',
        },
        ['lib2']: {
          singleton: true,
          requiredVersion: '2.0.0',
        },
      })
    ).toEqual({
      ...alwaysSetFixture,
      ...commonSetFixture,
      ['lib1']: {
        singleton: true,
        requiredVersion: '1.0.0',
      },
      ['lib2']: {
        singleton: true,
        requiredVersion: '2.0.0',
      },
    });
  });

  it('should not override always set libraries, if set as extra', () => {
    expect(
      getSharedDependencies(allDeps, {
        ['react']: {
          singleton: true,
          requiredVersion: '18.0.0',
        },
        ['react-dom']: {
          singleton: true,
          requiredVersion: '18.0.0',
        },
      })
    ).toEqual({
      react: {
        singleton: true,
        requiredVersion: '18.0.0',
      },
      'react-dom': {
        singleton: true,
        requiredVersion: '18.0.0',
      },
      '@emotion/react': {
        requiredVersion: '^11.1.1',
      },
      ...commonSetFixture,
    });
  });

  it('should not override common libraries, if set as extra', () => {
    expect(
      getSharedDependencies(allDeps, {
        ['moment']: {
          singleton: true,
          requiredVersion: '2.0.0',
        },
      })
    ).toEqual({
      ...alwaysSetFixture,
      moment: {
        singleton: true,
        requiredVersion: '2.0.0',
      },
    });
  });

  it('should include any @confirmit packages found', () => {
    expect(
      getSharedDependencies({
        ...allDeps,
        '@confirmit/react-button': '1.2.3',
        '@confirmit/react-chip': '1.2.3',
        lib1: '1.0.0',
        lib2: '1.0.0',
      })
    ).toEqual({
      ...alwaysSetFixture,
      ...commonSetFixture,
      ['@confirmit/react-button']: {
        requiredVersion: '^1.2.3',
      },
      ['@confirmit/react-chip']: {
        requiredVersion: '^1.2.3',
      },
    });
  });

  it('should include any @jotunheim packages found', () => {
    expect(
      getSharedDependencies({
        ...allDeps,
        '@jotunheim/react-button': '1.2.3',
        '@jotunheim/react-chip': '1.2.3',
        lib1: '1.0.0',
        lib2: '1.0.0',
      })
    ).toEqual({
      ...alwaysSetFixture,
      ...commonSetFixture,
      ['@jotunheim/react-button']: {
        requiredVersion: '^1.2.3',
      },
      ['@jotunheim/react-chip']: {
        requiredVersion: '^1.2.3',
      },
    });
  });
});
