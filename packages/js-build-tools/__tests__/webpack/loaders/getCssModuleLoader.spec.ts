import {getCssModuleLoader} from '../../../src/webpack/loaders/getCssModuleLoader';
import cssLoaderResourceIds from '../../../src/webpack/loaders/CssLoaderResourceIds';
import {LoaderContext, LoaderDefinition} from 'webpack';

jest.mock('loader-utils', () => {
  return {
    interpolateName: (context, name) => name,
  };
});

const createPartialMock = <T>(props: Partial<T>): T => {
  return props as T;
};

describe('getCssLoader', () => {
  beforeEach(() => {
    cssLoaderResourceIds.clear();
  });

  describe('options', () => {
    it('should pass importLoaders to css loader', () => {
      const cssLoader = getCssModuleLoader({
        importLoaders: 1,
      });

      expect(cssLoader.options.importLoaders).toBe(1);
    });

    it('should pass sourceMap to css loader', () => {
      const cssLoader = getCssModuleLoader({
        sourceMap: true,
      });

      expect(cssLoader.options.sourceMap).toBe(true);
    });
  });

  describe('getLocalIdent', () => {
    it('should interpolate with localIdentName if ', () => {
      const cssLoader = getCssModuleLoader({
        confirmitReactPackagesLocalIdentName: '[local]',
      });

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-button/dist/esm/css/react-button.module.css',
      });
      const className = 'react-button';
      const localIdentName = `dummy`;

      const interpolatedName = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className
      );
      expect(interpolatedName).toBe('react-button');
    });

    it('should interpolate className with [local]', () => {
      const cssLoader = getCssModuleLoader({
        confirmitReactPackagesLocalIdentName: '[local]',
      });

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-button/dist/esm/css/react-button.module.css',
      });
      const className = 'react-button';
      const localIdentName = `dummy`;

      const interpolatedName = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className
      );
      expect(interpolatedName).toBe('react-button');
    });

    it('should interpolate className with [confirmit-identifier]', () => {
      const cssLoader = getCssModuleLoader({
        confirmitReactPackagesLocalIdentName:
          'staticText_[confirmit-identifier]',
      });

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/@confirmit/react-button/dist/esm/css/react-button.module.css',
      });
      const className = 'react-button';
      const localIdentName = `dummy`;

      const interpolatedName = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className
      );
      expect(interpolatedName).toBe('staticText_0');
    });

    it('should interpolate className with [local] and [confirmit-identifier]', () => {
      const cssLoader = getCssModuleLoader();

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-button/dist/esm/css/react-button.module.css',
      });
      const className = 'react-button';
      const localIdentName = `dummy`;

      const interpolatedName = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className
      );
      expect(interpolatedName).toBe('react-button_0');
    });

    it('should interpolate classNames from the same module with [confirmit-identifier] and assign the same identifier', () => {
      const cssLoader = getCssModuleLoader();

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-button/dist/esm/css/react-button.module.css',
      });
      const className1 = 'react-button';
      const className2 = 'react-button__icon';
      const localIdentName = `dummy`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className1
      );
      const interpolatedName2 = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className2
      );

      expect(interpolatedName1).toBe('react-button_0');
      expect(interpolatedName2).toBe('react-button__icon_0');
    });

    it('should interpolate classNames from the different modules with [confirmit-identifier] and assign the different identifiers', () => {
      const cssLoader = getCssModuleLoader();

      const context1 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-modal/dist/esm/css/react-button.module.css',
      });
      const context2 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/@confirmit/react-text-field/dist/esm/css/react-text-field.module.css',
      });
      const className1 = 'react-button';
      const className2 = 'react-text-field__icon';
      const localIdentName = `dummy`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context1,
        localIdentName,
        className1
      );
      const interpolatedName2 = cssLoader.options.modules.getLocalIdent(
        context2,
        localIdentName,
        className2
      );

      expect(interpolatedName1).toBe('react-button_0');
      expect(interpolatedName2).toBe('react-text-field__icon_1');
    });

    it('should override confirmitReactPackagesRegExp', () => {
      const cssLoader = getCssModuleLoader({
        confirmitReactPackagesRegExp: /confirmit-react-modal/,
      });

      const context1 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-modal/dist/esm/css/react-button.module.css',
      });
      const context2 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/@confirmit/react-text-field/dist/esm/css/react-text-field.module.css',
      });
      const className1 = 'react-button';
      const className2 = 'react-text-field__icon';
      const localIdentName = `dummy`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context1,
        localIdentName,
        className1
      );
      const interpolatedName2 = cssLoader.options.modules.getLocalIdent(
        context2,
        localIdentName,
        className2
      );

      expect(interpolatedName1).toBe('react-button_0');
      expect(interpolatedName2).toBe('react-text-field__icon');
    });

    it('should use localIdentName if confirmitReactPackagesLocalIdentName is empty', () => {
      const cssLoader = getCssModuleLoader({
        confirmitReactPackagesLocalIdentName: '',
      });

      const context1 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-modal/dist/esm/css/react-button.module.css',
      });
      const context2 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/@confirmit/react-text-field/dist/esm/css/react-text-field.module.css',
      });
      const className1 = 'react-button';
      const className2 = 'react-text-field__icon';
      const localIdentName = `dummy`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context1,
        localIdentName,
        className1
      );
      const interpolatedName2 = cssLoader.options.modules.getLocalIdent(
        context2,
        localIdentName,
        className2
      );

      expect(interpolatedName1).toBe('react-button');
      expect(interpolatedName2).toBe('react-text-field__icon');
    });

    it('should use localIdentName if package is not match the pattern', () => {
      const cssLoader = getCssModuleLoader();

      const context1 = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/some-package/dist/esm/css/react-button.module.css',
      });
      const className1 = 'react-button';
      const localIdentName = `dummy`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context1,
        localIdentName,
        className1
      );

      expect(interpolatedName1).toBe('react-button');
    });

    it('should override className generation with getClassNameIdentifier', () => {
      const cssLoader = getCssModuleLoader({
        getClassNameIdentifier: (
          context,
          localIdentName,
          localName,
          options
        ) => {
          return 'v' + options.confirmitIdentifier;
        },
      });

      const context = createPartialMock<LoaderContext<LoaderDefinition>>({
        resourcePath:
          '/c/dev/app/node_modules/confirmit-react-modal/dist/esm/css/react-button.module.css',
      });
      const className = 'react-button';
      const localIdentName = `[local]_[confirmit-identifier]`;

      const interpolatedName1 = cssLoader.options.modules.getLocalIdent(
        context,
        localIdentName,
        className
      );

      expect(interpolatedName1).toBe('react-button_v0');
    });
  });
});
