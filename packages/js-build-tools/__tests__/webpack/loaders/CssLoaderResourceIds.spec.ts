import cssLoaderResourceIds from '../../../src/webpack/loaders/CssLoaderResourceIds';

describe('cssLoaderResourceIds', () => {
  it('should return the same Id for the same resourcePath', () => {
    const id1 = cssLoaderResourceIds.getId('path1');
    const id2 = cssLoaderResourceIds.getId('path1');

    expect(id1).toBe('0');
    expect(id2).toBe('0');
  });

  it('should return different ids for different resourcePaths', () => {
    const id1 = cssLoaderResourceIds.getId('path1');
    const id2 = cssLoaderResourceIds.getId('path2');
    const id3 = cssLoaderResourceIds.getId('path3');

    expect(id1).toBe('0');
    expect(id2).toBe('1');
    expect(id3).toBe('2');
  });

  it('should clear ids', () => {
    const id1 = cssLoaderResourceIds.getId('path1');
    const id2 = cssLoaderResourceIds.getId('path2');
    cssLoaderResourceIds.clear();
    const id3 = cssLoaderResourceIds.getId('path3');

    expect(id1).toBe('0');
    expect(id2).toBe('1');
    expect(id3).toBe('0');
  });
});
