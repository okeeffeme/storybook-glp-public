import ConfirmitThemePlugin from '../../../../src/webpack/plugins/ConfirmitThemePlugin/ConfirmitThemePlugin';

const getNonThemeFilePath = () => {
  return `c:/dev/packageName/node_modules/confirmit-icons-material/src/Icon.js`;
};
const getThemeFilePath = (packageName, themeName) => {
  return `c:/dev/packageName/node_modules/${packageName}/src/themes/${themeName}/theme.js`;
};
const packageNames = ['@confirmit/react-button', '@confirmit/react-calendar'];

describe(`ConfirmitThemePlugin`, () => {
  it('should not exclude non-themed files with "material" in name', () => {
    const plugin = new ConfirmitThemePlugin('default');

    const resource = getNonThemeFilePath();
    const module = {
      resource,
    };
    plugin.onReplace(module);

    expect(module.resource).toEqual(resource);
  });

  it('should not exclude theme via single argument', () => {
    const plugin = new ConfirmitThemePlugin('default');

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'default');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(resource);
    }
  });

  it('should exclude theme via single argument', () => {
    const plugin = new ConfirmitThemePlugin('default');

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'material');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(expect.stringMatching(/no-theme\.js$/));
    }
  });

  it('should not exclude theme via array argument', () => {
    const plugin = new ConfirmitThemePlugin(['default', 'material']);

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'default');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(resource);
    }
  });

  it('should exclude theme via array argument', () => {
    const plugin = new ConfirmitThemePlugin(['default']);

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'material');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(expect.stringMatching(/no-theme\.js$/));
    }
  });

  it('should not exclude theme via options.include', () => {
    const plugin = new ConfirmitThemePlugin({
      include: ['default', 'material'],
    });

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'default');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(resource);
    }
  });

  it('should exclude theme via options.include', () => {
    const plugin = new ConfirmitThemePlugin({include: ['default']});

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'material');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(expect.stringMatching(/no-theme\.js$/));
    }
  });

  it('should not exclude theme via options.exclude', () => {
    const plugin = new ConfirmitThemePlugin({exclude: ['material']});

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'default');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(resource);
    }
  });

  it('should exclude theme via options.exclude', () => {
    const plugin = new ConfirmitThemePlugin({exclude: ['default']});

    for (const packageName of packageNames) {
      const resource = getThemeFilePath(packageName, 'default');
      const module = {
        resource,
      };
      plugin.onReplace(module);

      expect(module.resource).toEqual(expect.stringMatching(/no-theme\.js$/));
    }
  });

  it('should throw Error if no arguments', () => {
    expect(() => {
      new ConfirmitThemePlugin();
    }).toThrow();
  });

  it('should throw Error if no include or exclude options', () => {
    expect(() => {
      new ConfirmitThemePlugin({});
    }).toThrow();
  });
});
