import ConfirmitThemePlugin from './webpack/plugins/ConfirmitThemePlugin/ConfirmitThemePlugin';
import {getCssModuleLoader} from './webpack/loaders/getCssModuleLoader';
import {confirmitReactPackagesRegExp} from './webpack/loaders/confirmitReactPackagesRegExp';
import {ensureLooseVersion} from './webpack/ensureLooseVersion';
import {getSharedDependencies} from './webpack/getSharedDependencies';
import {loadRemoteEntry} from './webpack/loadRemoteEntry';

export const webpack = {
  ConfirmitThemePlugin,
  confirmitReactPackagesRegExp,
  getCssModuleLoader,
  federation: {
    ensureLooseVersion,
    getSharedDependencies,
    loadRemoteEntry,
  },
};
