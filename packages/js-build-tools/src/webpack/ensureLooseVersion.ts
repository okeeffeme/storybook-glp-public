// ensure loose version dep
export const ensureLooseVersion = (version: string): string => {
  if (version.match(/[~*x^]/gm) === null) {
    return `^${version}`;
  }

  return version;
};
