import webpack from 'webpack';
import path from 'path';

const defaultOptions = {
  include: [],
  exclude: [],
};

const regex = new RegExp(
  `(.*[\\/\\\\]confirmit-.*|@confirmit[\\/\\\\].*[\\/\\\\]themes)[\\/\\\\](default|material)[\\/\\\\](.*\\.jsx?)`,
  'i'
);

/*
 * ConfirmitThemePlugin plugin includes or excludes themes from a bundle
 * which is helpful if application supports only single theme
 */
export default class ConfirmitThemePlugin {
  constructor(options) {
    if (typeof options === 'string') {
      options = {
        include: [options],
      };
    } else if (Array.isArray(options)) {
      options = {
        include: options,
      };
    } else if (!options || (!options.include && !options.exclude)) {
      throw new Error(
        'ConfirmitThemePlugin constructor takes a string argument or an array of string arguments, or an object with either include or exclude properties.'
      );
    }

    options = {
      ...defaultOptions,
      ...options,
    };

    this._options = options;
    this._normalModuleReplacementPlugin = new webpack.NormalModuleReplacementPlugin(
      regex,
      this.onReplace
    );
  }

  onReplace = module => {
    const {include, exclude} = this._options;
    const inclusionMode = !!include.length;
    const exclusionMode = !!exclude.length;

    const match = regex.exec(module.resource);
    if (!match) return;

    const [, , $2] = match;
    const themeName = $2;

    const shouldBeExcluded =
      (inclusionMode && !include.includes(themeName)) ||
      (exclusionMode && exclude.includes(themeName));

    if (shouldBeExcluded) {
      /*
       * This callback is called while resolving "request" path and when "path" is resolved to a particular resource on disk.
       * Roughly speaking relative and absolute paths.
       * We are looking for absolute paths and replace resolved resource path
       * If we would use relative paths then there is change that some consumer application modules will be erased.
       */
      module.resource = path.resolve(__dirname, './no-theme.js');
    }
  };

  apply(compiler) {
    this._normalModuleReplacementPlugin.apply.call(
      this._normalModuleReplacementPlugin,
      compiler
    );
  }
}
