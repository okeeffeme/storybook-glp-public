import {ensureLooseVersion} from './ensureLooseVersion';

type SharedObject = {
  [name: string]: {
    singleton: boolean;
    requiredVersion: string | false;
    strictVersion?: boolean;
  };
};

type PackageJsonDeps = {
  [name: string]: string;
};

// always mark these as shared
const alwaysShared = {
  react: {
    requiredVersion: '^16.8 || ^17 || ^18',
  },
  'react-dom': {
    requiredVersion: '^16.8 || ^17 || ^18',
  },
  '@emotion/react': {
    // this one gets pulled in via react-select
    requiredVersion: '^11.1.1',
  },
};

// mark these as shared if they are found in the deps
const commonlyShared = ['moment', 'confirmit-utils'];

// these should be marked as singleton by default
const singletons = ['react', 'react-dom', '@jotunheim/react-contexts'];

export const getSharedDependencies = (
  allDeps: PackageJsonDeps,
  extraShared: SharedObject = {}
) => {
  const shared = Object.keys(allDeps).reduce(
    (result, dep) => {
      if (
        dep.startsWith('@confirmit/react-') ||
        dep.startsWith('@jotunheim/react-')
      ) {
        result[dep] = {
          requiredVersion: ensureLooseVersion(allDeps[dep]),
        };
      }

      return result;
    },
    {
      ...alwaysShared,
    }
  );

  commonlyShared.forEach((name) => {
    if (allDeps[name]) {
      shared[name] = {
        requiredVersion: ensureLooseVersion(allDeps[name]),
      };
    }
  });

  Object.keys(shared).forEach((name) => {
    if (singletons.includes(name)) {
      shared[name].singleton = true;
    }
  });

  // allow custom overrides from host application to override the defaults
  Object.assign(shared, extraShared);

  return shared;
};
