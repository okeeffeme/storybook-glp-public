// https://webpack.js.org/concepts/module-federation/#promise-based-dynamic-remotes

export const loadRemoteEntry = (
  hostName: string,
  moduleName: string,
  moduleMap = 'window.CONFIG.federatedModules'
) =>
  `promise new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.src = ${moduleMap}['${hostName}'];
    script.defer = true;
    script.onerror = reject;
    script.onload = () => {
      const proxy = {
        get: (request) => window.${moduleName}.get(request),
        init: (arg) => {
          try {
            return window.${moduleName}.init(arg)
          } catch(e) {
            console.log('remote container already initialized')
          }
        }
      }
      resolve(proxy)
    };

    document.head.appendChild(script);
  });
`;
