class CssLoaderResourceIds {
  private resourceIds = new Map<string, string>();

  getId(resourcePath: string): string {
    if (!this.resourceIds.has(resourcePath)) {
      this.resourceIds.set(resourcePath, this.resourceIds.size.toString());
    }

    return this.resourceIds.get(resourcePath) as string;
  }

  clear(): void {
    this.resourceIds.clear();
  }
}

/* We provide singleton is ids are shared in the same process*/
export default new CssLoaderResourceIds();
