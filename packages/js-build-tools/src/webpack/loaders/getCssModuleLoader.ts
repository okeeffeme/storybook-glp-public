import {interpolateName} from 'loader-utils';
import {LoaderContext, LoaderDefinition} from 'webpack';
import cssLoaderResourceIds from './CssLoaderResourceIds';
import {confirmitReactPackagesRegExp} from './confirmitReactPackagesRegExp';
import {escapeRegExp} from './escapeRegExp';

type CssLoaderOptions = {
  importLoaders?: number;
  localIdentName: string;
  confirmitReactPackagesLocalIdentName: string;
  sourceMap?: boolean;
  confirmitReactPackagesRegExp: RegExp;
  getClassNameIdentifier?: (
    context: LoaderContext<LoaderDefinition>,
    localIdentName: string,
    localName: string,
    options: any // eslint-disable-line @typescript-eslint/no-explicit-any
  ) => string;
};

const confirmitIdentifierToken = '[confirmit-identifier]';
const localToken = '[local]';

const defaultOptions: CssLoaderOptions = {
  confirmitReactPackagesRegExp,
  localIdentName: localToken,
  confirmitReactPackagesLocalIdentName: `${localToken}_${confirmitIdentifierToken}`,
};

export function getCssModuleLoader(partialOptions?: Partial<CssLoaderOptions>) {
  const options = {
    ...defaultOptions,
    ...partialOptions,
  };
  const {
    importLoaders,
    localIdentName,
    sourceMap,
    getClassNameIdentifier,
    confirmitReactPackagesRegExp,
    confirmitReactPackagesLocalIdentName,
  } = options;

  return {
    loader: 'css-loader',
    options: {
      modules: {
        localIdentName,
        getLocalIdent: function (
          context: LoaderContext<LoaderDefinition>,
          dummyLocalIdentName: string,
          localName: string,
          options?: any // eslint-disable-line @typescript-eslint/no-explicit-any
        ) {
          const {resourcePath} = context;
          const isConfirmitReactPackageModule =
            confirmitReactPackagesLocalIdentName &&
            confirmitReactPackagesRegExp.test(resourcePath);
          const identName = isConfirmitReactPackageModule
            ? confirmitReactPackagesLocalIdentName
            : localIdentName;

          let interpolatedName = interpolateName(context, identName, options);

          if (isConfirmitReactPackageModule) {
            let resourceId = cssLoaderResourceIds.getId(context.resourcePath);
            if (getClassNameIdentifier) {
              resourceId = getClassNameIdentifier(
                context,
                identName,
                localName,
                {
                  ...options,
                  confirmitIdentifier: resourceId,
                }
              );
            }

            interpolatedName = interpolatedName.replace(
              new RegExp(escapeRegExp(confirmitIdentifierToken), 'g'),
              resourceId
            );
          }

          interpolatedName = interpolatedName.replace(
            new RegExp(escapeRegExp(localToken), 'g'),
            localName
          );

          return interpolatedName;
        },
      },
      importLoaders,
      sourceMap,
    },
  };
}
