# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@4.0.0&sourceBranch=refs/tags/@confirmit/js-build-tools@4.1.0&targetRepoId=1246) (2022-07-06)

### Features

- federation util for loading dynamic remotes ([NPM-1038](https://jiraosl.firmglobal.com/browse/NPM-1038)) ([45c295f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/45c295f1efe3eef0acbc3aab0acda3545ca3eabc))

## [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@3.1.0&sourceBranch=refs/tags/@confirmit/js-build-tools@3.1.1&targetRepoId=1246) (2022-06-29)

### Bug Fixes

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@3.0.1&sourceBranch=refs/tags/@confirmit/js-build-tools@3.0.2&targetRepoId=1246) (2022-06-27)

### Features

- accept "jotunheim" scope for css module loader ([NPM-1029](https://jiraosl.firmglobal.com/browse/NPM-1029)) ([4c600ee](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4c600ee31767e809ccc96b45ac8c6d35ae0c07be))

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@3.0.0&sourceBranch=refs/tags/@confirmit/js-build-tools@3.0.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- accept react 18 for federated deps ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([cf87a24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cf87a245dcf448a4427f2faf477f44d8ba6f8274))

## v3.0.0

### Breaking changes

- getSharedDependencies now no longer marks all @confirmit deps as singletons, only @confirmit/react-contexts

- getSharedDependencies no longer does anything to the "extraShared" argument, whatever is passed here will be applied as is,
  and will overwrite anything else in the config with the same keys

- redux related packages are no longer included by default

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@2.1.0&sourceBranch=refs/tags/@confirmit/js-build-tools@2.1.1&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/js-build-tools

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/js-build-tools@2.0.0&sourceBranch=refs/tags/@confirmit/js-build-tools@2.1.0&targetRepoId=1246) (2021-07-23)

### Features

- new utilities for setting up shared modules when using module federation (getFederatedModules, ensureLooseVersion) ([NPM-829](https://jiraosl.firmglobal.com/browse/NPM-829)) ([e0d7c96](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e0d7c963477678fd545f8e6c6302209fd314ab06))

## CHANGELOG

## v2.0.0

- Refactor: New package name: `@confirmit/js-build-tools`

### v1.1.0

- Feat: Added getCssLoader that allows generate unique classnames for components from confirmit-react-packages.
  See readme with examples.

### v1.0.7

- Bugfix: some relative imports haven't been excluded properly.

### v1.0.6

- Bugfix: confirmit-icons-material could be excluded from bundle occasionally.

### v1.0.5

- Feat: Support for @confirmit scope

### v1.0.0

- **BREAKING CHANGE:** ConfirmitThemesReplacerPlugin is replaced by ConfirmitThemePlugin. ConfirmitThemePlugin allows to include or exclude one or more themes from bundle. Useful if application supports only single theme.
  ```js
  const {webpack: {ConfirmitThemePlugin}} = require('confirmit-js-build-tools')
  {
    ...
    plugins: [new ConfirmitThemePlugin("default")]
    ...
    plugins: [new ConfirmitThemePlugin(["material", "default"])]
    ...
    plugins: [new ConfirmitThemePlugin({includes: ["material"]})]
    ...
    plugins: [new ConfirmitThemePlugin({excludes: ["material"]})]
    ...
  }
  ```

### v0.0.1

- Feat: ConfirmitThemesReplacerPlugin to be able to replace themes that are not used in the application
