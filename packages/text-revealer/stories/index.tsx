/* eslint-disable react/prop-types */

import React from 'react';
import {storiesOf} from '@storybook/react';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {TextRevealer} from '../src';

/* eslint-disable-next-line */
const TopIndent = ({children}) => (
  <div style={{top: '200px', position: 'relative'}}>{children}</div>
);

/* eslint-disable-next-line */
const ShiftToTheRight = ({children}) => (
  <div style={{left: '400px', position: 'relative'}}>{children}</div>
);

/* eslint-disable-next-line */
const PercentageWidthContainer = ({children}) => (
  <div style={{width: '80%'}}>{children}</div>
);

/* eslint-disable-next-line */
const WithMarginContainer = ({children}) => (
  <div style={{width: '30%', marginTop: '20px'}}>{children}</div>
);

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

const FixedWidthContainer = ({widthPx = 300, children}) => (
  <div style={{width: `${widthPx}px`}}>{children}</div>
);

storiesOf('Components/text-revealer', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('short text', () => (
    <FixedWidthContainer>
      <TextRevealer>
        There is no text to reveal - tooltip with delay.
      </TextRevealer>
    </FixedWidthContainer>
  ))
  .add('long text', () => (
    <FixedWidthContainer>
      <TextRevealer>Point the text for revealing. {lorem}</TextRevealer>
    </FixedWidthContainer>
  ))
  .add('tooltip placements', () => (
    <FixedWidthContainer>
      <TextRevealer>Default "BOTTOM" placement. {lorem}</TextRevealer>
      <TopIndent>
        <TextRevealer defaultTooltipPlacement="top">
          "TOP" placement. {lorem}
        </TextRevealer>
      </TopIndent>
      <ShiftToTheRight>
        <TextRevealer defaultTooltipPlacement="left">
          "LEFT" placement. {lorem}
        </TextRevealer>
      </ShiftToTheRight>
      <TextRevealer defaultTooltipPlacement="right">
        "RIGHT" placement. {lorem}
      </TextRevealer>
    </FixedWidthContainer>
  ))
  .add('custom tooltip content', () => (
    <FixedWidthContainer widthPx={200}>
      <TextRevealer tooltipContent="Cowabunga!">{lorem}</TextRevealer>
    </FixedWidthContainer>
  ))
  .add('percentage container', () => (
    <PercentageWidthContainer>
      <TextRevealer>
        Width is 80%. There is no text to reveal (there is no ellipsis). Try to
        resize window and check the text revealing.
      </TextRevealer>
    </PercentageWidthContainer>
  ))
  .add('multi line text', () => (
    <div>
      <h4>Three lines:</h4>
      <WithMarginContainer>
        <TextRevealer lines={3}>{lorem.repeat(2)}</TextRevealer>
      </WithMarginContainer>
      <br />
      <h4>Four lines:</h4>
      <WithMarginContainer>
        <TextRevealer lines={4}>{lorem.repeat(2)}</TextRevealer>
      </WithMarginContainer>
      <br />
      <h4>Five lines:</h4>
      <WithMarginContainer>
        <TextRevealer lines={5}>{lorem.repeat(2)}</TextRevealer>
      </WithMarginContainer>
    </div>
  ))
  .add('wrapped to custom element', () => (
    <div>
      <FixedWidthContainer>
        <a href="#">
          <TextRevealer>long link {lorem}</TextRevealer>
        </a>
      </FixedWidthContainer>
      <FixedWidthContainer>
        <div style={{color: 'red', fontSize: 25}}>
          <TextRevealer>styled div {lorem}</TextRevealer>
        </div>
      </FixedWidthContainer>
    </div>
  ));
