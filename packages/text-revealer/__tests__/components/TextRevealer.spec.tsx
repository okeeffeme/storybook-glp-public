import React from 'react';
import {render, screen} from '@testing-library/react';

import TextReveler from '../../src/components/TextRevealer';

describe('TextReveler', () => {
  it('should be rendered', () => {
    render(<TextReveler>Lorem ipsum</TextReveler>);

    expect(screen.getByText(/lorem ipsum/i)).toBeTruthy();
  });
});
