# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.10&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.11&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.9&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.10&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.8&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.9&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.7&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.8&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.6&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.7&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.5&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.6&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.4&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.5&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.3&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.4&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.2&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.3&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.1&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.2&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@7.0.0&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-text-revealer

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.6&sourceBranch=refs/tags/@jotunheim/react-text-revealer@7.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of TextRevealer ([NPM-958](https://jiraosl.firmglobal.com/browse/NPM-958)) ([807def3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/807def33f851638bedc781381849c2b4ff5a3d4d))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [6.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.5&sourceBranch=refs/tags/@jotunheim/react-text-revealer@6.0.6&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.4&sourceBranch=refs/tags/@jotunheim/react-text-revealer@6.0.5&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.2&sourceBranch=refs/tags/@jotunheim/react-text-revealer@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.1&sourceBranch=refs/tags/@jotunheim/react-text-revealer@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-text-revealer

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-revealer@6.0.0&sourceBranch=refs/tags/@jotunheim/react-text-revealer@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-text-revealer

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.18&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.19&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.17&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.18&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.16&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.17&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.15&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.16&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.14&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.15&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.13&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.14&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.12&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.13&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.11&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.12&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.10&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.11&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.9&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.10&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.8&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.9&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.7&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.8&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.5&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.6&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.4&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.5&targetRepoId=1246) (2020-12-18)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.3&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.4&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.2&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.3&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@5.0.0&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.1&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-text-revealer

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@4.1.4&sourceBranch=refs/tags/@confirmit/react-text-revealer@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@4.1.3&sourceBranch=refs/tags/@confirmit/react-text-revealer@4.1.4&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [4.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@4.1.2&sourceBranch=refs/tags/@confirmit/react-text-revealer@4.1.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [4.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-revealer@4.1.1&sourceBranch=refs/tags/@confirmit/react-text-revealer@4.1.2&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-text-revealer

## [4.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-revealer@4.1.0...@confirmit/react-text-revealer@4.1.1) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-text-revealer

# [4.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-revealer@4.0.4...@confirmit/react-text-revealer@4.1.0) (2020-08-12)

### Features

- add typescript support ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([87a06d9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/87a06d995aefe32bff58f38079831b2a5bd088ce))

## CHANGELOG

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Removing package version in class names.

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.10

- Refactor: update internal components using poppers to use `placement` instead of `defaultPlacement` prop

### v2.0.0

- **BREAKING**
  - Removed 'onLineHeightCalculated' prop

### v1.0.0

- Tooltip is now always presented (even if text isn't truncated), but showing with some delay

### v0.0.1

- Initial version
