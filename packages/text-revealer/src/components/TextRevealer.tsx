import React, {ReactNode} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import {Tooltip, Placement} from '@jotunheim/react-tooltip';

import classNames from './TextRevealer.module.css';
import {Truncate} from '@jotunheim/react-truncate';

type TextRevealerProps = {
  tooltipContent?: ReactNode;
  defaultTooltipPlacement?: Placement;
  lines?: number;
  children: string | number | string[];
};

export default function TextRevealer(props: TextRevealerProps) {
  const {
    tooltipContent,
    defaultTooltipPlacement = 'bottom',
    children,
    lines = 1,
    ...rest
  } = props;

  const content = tooltipContent ? tooltipContent : children;
  const {block} = bemFactory({baseClassName: 'co-text-revealer', classNames});

  return (
    <Tooltip content={content} placement={defaultTooltipPlacement}>
      <span
        className={block()}
        data-testid="text-revealer"
        {...extractDataAndAriaProps(rest)}>
        <Truncate lines={lines}>{children}</Truncate>
      </span>
    </Tooltip>
  );
}
