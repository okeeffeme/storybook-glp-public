import React from 'react';

import Icon, {star, starOutline} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import ToggleButton, {baseClassName} from './ToggleButton';
import {FavoriteToggleButtonProps} from '../types';

import classNames from './ToggleButton.module.css';

const FavoriteToggleButton = ({
  onChange,
  id,
  checked,
  text,
  disabled,
  value,
  tooltipText,
  tooltipPlacement,
  ...rest
}: FavoriteToggleButtonProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });

  return (
    <ToggleButton
      icon={
        <Icon
          className={element('favorite-icon')}
          path={checked ? star : starOutline}
        />
      }
      checked={checked}
      onChange={onChange}
      id={id}
      text={text}
      disabled={disabled}
      value={value}
      tooltipText={tooltipText}
      tooltipPlacement={tooltipPlacement}
      {...extractDataAriaIdProps(rest)}
    />
  );
};

export default FavoriteToggleButton;
