import React from 'react';

import {GroupContext} from '../types';

export const ButtonGroupContext = React.createContext<GroupContext>({
  isInGroup: false,
});
