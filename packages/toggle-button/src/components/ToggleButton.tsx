import React, {useContext} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps, useUuid} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {Tooltip} from '@jotunheim/react-tooltip';

import {ButtonGroupContext} from './buttonGroupContext';
import {ToggleButtonProps} from '../types';

import classNames from './ToggleButton.module.css';

const DEFAULT_ICON_SIZE = '24';
const ICON_WITH_TEXT_ICON_SIZE = '16';

export const baseClassName = 'comd-toggle-button';
const groupedButtonBaseClassName = 'comd-toggle-button-grouped';

const ToggleButton = ({
  id,
  text,
  checked = false,
  disabled = false,
  onChange,
  value,
  icon,
  tooltipText,
  tooltipPlacement = 'top',
  ...rest
}: ToggleButtonProps) => {
  const uuid = useUuid();

  let stringId = typeof id === 'number' ? String(id) : id;
  if (stringId == null) stringId = uuid;

  const {isInGroup} = useContext(ButtonGroupContext);

  const getBaseClassName = () => {
    if (isInGroup) {
      return groupedButtonBaseClassName;
    }

    if (!icon && !!text) {
      return groupedButtonBaseClassName;
    }

    return baseClassName;
  };

  const {block, element, modifier} = bemFactory({
    baseClassName: getBaseClassName(),
    classNames,
  });

  const getIcon = () => {
    if (!icon) {
      return null;
    }

    const size =
      isInGroup && text ? ICON_WITH_TEXT_ICON_SIZE : DEFAULT_ICON_SIZE;

    return React.cloneElement(icon, {
      size,
      className: cn(icon.props.className, element('icon')),
    });
  };

  const getText = () => {
    if (!text) {
      return null;
    }

    if (!isInGroup && icon) {
      return null;
    }

    return (
      <span
        data-testid="toggle-button-grouped-text"
        className={element('text')}>
        {text}
      </span>
    );
  };

  return (
    <Tooltip content={tooltipText} placement={tooltipPlacement}>
      <div
        data-testid="toggle-button"
        tooltip-content={tooltipText ? tooltipText : null}
        tooltip-placement={tooltipPlacement}
        className={cn(block(), {
          [modifier('checked')]: checked,
        })}
        {...extractDataAriaIdProps(rest)}>
        <input
          type="checkbox"
          id={stringId}
          checked={checked}
          value={String(value)}
          className={element('checkbox')}
          onChange={onChange}
          disabled={disabled}
        />
        <label
          className={cn(element('label'), {
            [element('label', 'disabled')]: disabled,
          })}
          htmlFor={stringId}>
          {getIcon()}
          {getText()}
        </label>
      </div>
    </Tooltip>
  );
};

export default ToggleButton;
