import React from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Tooltip from '@jotunheim/react-tooltip';

import {ButtonGroupContext} from './buttonGroupContext';

import {ToggleButtonGroupProps} from '../types';

import classNames from './ToggleButton.module.css';

const buttonGroupContextValue = {isInGroup: true};

const {block, element} = bemFactory({
  baseClassName: 'comd-toggle-button-group',
  classNames,
});

const ToggleButtonGroup = ({
  children,
  error = false,
  required = false,
  label = '',
  helperText = '',
  ...rest
}: ToggleButtonGroupProps) => {
  return (
    <ButtonGroupContext.Provider value={buttonGroupContextValue}>
      <div
        data-testid="toggle-button-group"
        className={block()}
        {...extractDataAriaIdProps(rest)}>
        {label && (
          <div
            className={cn(element('label'), {
              [element('label', 'error')]: error,
            })}>
            {`${label}${required ? ' *' : ''}`}
          </div>
        )}
        <Tooltip
          type={error ? Tooltip.Types.error : Tooltip.Types.info}
          open={error ? true : undefined}
          placement="bottom"
          content={helperText}>
          <div
            className={cn(element('buttons'), {
              [element('buttons', 'error')]: error,
            })}>
            {children}
          </div>
        </Tooltip>
      </div>
    </ButtonGroupContext.Provider>
  );
};

export default ToggleButtonGroup;
