import React from 'react';
import cn from 'classnames';

import Icon from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import classNames from './JumboToggleButton.module.css';

import {JumboToggleButtonProps} from '../types';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-jumbo-toggle-button',
  classNames,
});

export const JumboButton = ({
  title,
  text,
  iconPath,
  imageSrc,
  onChange,
  checked,
  id,
  disabled,
  ...rest
}: JumboToggleButtonProps) => {
  const classes = cn(block(), {
    [modifier('checked')]: checked,
    [modifier('disabled')]: disabled,
  });

  return (
    <label
      data-testid="jumbo-toggle-button"
      className={classes}
      {...extractDataAriaIdProps(rest)}>
      <input
        type="checkbox"
        id={id}
        checked={checked}
        className={element('checkbox')}
        onChange={onChange}
        disabled={disabled}
      />
      <div
        data-testid="jumbo-toggle-button-graphic"
        className={cn(element('graphic'), {
          [element('graphic', 'disabled')]: disabled,
        })}>
        {iconPath ? (
          <div
            data-testid="jumbo-toggle-button-icon"
            className={cn(element('icon'), {
              [element('icon', 'checked')]: checked,
            })}>
            <Icon path={iconPath} size={60} fill="currentColor" />
          </div>
        ) : (
          imageSrc && <img className={element('img')} src={imageSrc} />
        )}
      </div>
      <div
        data-testid="jumbo-toggle-button-text-content"
        className={cn(element('text-content'), {
          [element('text-content', 'checked')]: checked,
          [element('text-content', 'disabled')]: disabled,
        })}>
        <div className={cn(element('text'), element('text', 'title'))}>
          {title}
        </div>
        {text && (
          <div
            data-test-jumbo-toggle-button="text"
            data-testid="jumbo-toggle-button-text"
            className={element('text')}>
            {text}
          </div>
        )}
      </div>
    </label>
  );
};

export default JumboButton;
