import {
  AriaAttributes,
  ChangeEventHandler,
  ReactElement,
  ReactNode,
} from 'react';
import {Placement} from '@jotunheim/react-tooltip';

export interface GroupContext {
  isInGroup: boolean;
}

export type ToggleButtonProps = {
  onChange?: ChangeEventHandler<HTMLInputElement>;
  id?: string | number;
  text?: string;
  checked?: boolean;
  disabled?: boolean;
  icon?: ReactElement;
  value?: string | number | boolean;
  tooltipText?: string;
  tooltipPlacement?: Placement;
};

export type FavoriteToggleButtonProps = Omit<ToggleButtonProps, 'icon'>;

export interface ToggleButtonGroupProps {
  label?: string;
  helperText?: ReactNode;
  error?: boolean;
  required?: boolean;
  children: ReactNode;
}

export type JumboToggleButtonProps = {
  title: ReactNode;
  text?: ReactNode;
  iconPath?: string;
  imageSrc?: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  checked?: boolean;
  disabled?: boolean;
  id: string;
} & AriaAttributes;
