# Jotunheim React Toggle Button Group

[Changelog](./CHANGELOG.md)

Toggle button can be used for one of two reasons: to showcase a selected action or setting, or to group related options.
