# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.44&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.45&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.44&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.44&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.42&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.43&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.41&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.42&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.40&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.41&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.39&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.40&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.38&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.39&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.37&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.38&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.36&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.37&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.35&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.36&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.34&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.35&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.33&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.34&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.32&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.33&targetRepoId=1246) (2023-01-27)

### Bug Fixes

- adding data-testids to ToggleButton components ([NPM-1218](https://jiraosl.firmglobal.com/browse/NPM-1218)) ([33d7ff0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/33d7ff0a15bb3c2a6e2caba53d80340d448cf72f))

## [6.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.31&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.32&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.30&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.31&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.28&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.30&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.28&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.27&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.28&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.26&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.25&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.24&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.22&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.22&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.23&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.22&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.21&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.20&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.18&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.19&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.17&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.18&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.15&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.16&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.14&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.15&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.13&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.14&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.12&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.13&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.11&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.10&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.9&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.7&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.4&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.3&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.4&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.2&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.3&targetRepoId=1246) (2022-07-12)

### Bug Fixes

- stop federating unused components ([NPM-1040](https://jiraosl.firmglobal.com/browse/NPM-1040)) ([e54b864](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e54b864af94acbc92a3ad1605334f11dd963e48b))

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.1&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-toggle-button

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle-button@6.0.0&sourceBranch=refs/tags/@jotunheim/react-toggle-button@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-toggle-button

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.5&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.6&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [5.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.4&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.5&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [5.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.3&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.4&targetRepoId=1246) (2022-06-02)

### Bug Fixes

- Removing autoprefixer autoplace ([NPM-993](https://jiraosl.firmglobal.com/browse/NPM-993)) ([7bf66bb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7bf66bbdeb3acb09e839cb094475ff447e8d15bf))

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.2&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.3&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.1&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.2&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.1.0&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.1&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-toggle-button

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.0.4&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.1.0&targetRepoId=1246) (2022-04-06)

### Features

- Make id property optional in Switch, Toggle and ToggleButton components ([NPM-994](https://jiraosl.firmglobal.com/browse/NPM-994)) ([b3d2356](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b3d23568079a5e7f4d1e813436a0141858e6a54e))

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.0.3&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.0.4&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.0.2&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.0.3&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@5.0.1&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.0.2&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-toggle-button

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.22&sourceBranch=refs/tags/@confirmit/react-toggle-button@5.0.0&targetRepoId=1246) (2022-02-07)

### Bug Fixes

- Remove className prop and theming ([NPM-920](https://jiraosl.firmglobal.com/browse/NPM-920)) ([ad60ff6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ad60ff6cd82602d134ce7c2ed73dc33203aac084))

### Features

- Federate ToggleButton component ([94903ed](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/94903ed839fae4270579910fb72a3014dc79a5dc))

### BREAKING CHANGES

- Remove className prop and theming (NPM-920)

## [4.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.21&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.22&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.20&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.21&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.19&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.20&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.18&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.19&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.17&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.18&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.16&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.17&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.15&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.16&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [4.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.14&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.15&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.12&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.13&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.11&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.12&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.10&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.11&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.9&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.10&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.8&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.9&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.7&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.8&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.6&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.7&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.5&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.6&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.4&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.5&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.3&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.4&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.2&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.1&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.2&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.1.0&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.1&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-toggle-button

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.35&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.1.0&targetRepoId=1246) (2021-06-14)

### Features

- add JumboToggleButton component ([NPM-802](https://jiraosl.firmglobal.com/browse/NPM-802)) ([5465940](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5465940727088b90b04c502879074476f3ca6d92))

## [4.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.34&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.35&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.33&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.34&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.32&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.33&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.31&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.32&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.30&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.31&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.29&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.30&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.27&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.28&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.26&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.27&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.25&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.26&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [4.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.24&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.25&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.23&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.24&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.22&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.23&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.21&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.22&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.20&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.21&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.19&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.20&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.17&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.18&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.16&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.17&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.15&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.16&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.14&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.15&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.13&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.12&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.11&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.10&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.9&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.6&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.3&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.2&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@4.0.1&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-toggle-button

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.40&sourceBranch=refs/tags/@confirmit/react-toggle-button@4.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [3.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.39&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.40&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([8be8715](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8be8715dc6d685a53a6594b84673b787a7e29de8))

## [3.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.38&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.39&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.37&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.38&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.34&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.35&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.33&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.34&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.32&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.33&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.29&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.30&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.27&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.28&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle-button@3.0.25&sourceBranch=refs/tags/@confirmit/react-toggle-button@3.0.26&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle-button@3.0.23...@confirmit/react-toggle-button@3.0.24) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle-button@3.0.21...@confirmit/react-toggle-button@3.0.22) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle-button@3.0.16...@confirmit/react-toggle-button@3.0.17) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-toggle-button

## [3.0.16](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle-button@3.0.15...@confirmit/react-toggle-button@3.0.16) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-toggle-button

## CHANGELOG

### v3.0.13

- Fix: Incorrect toggle button group label color.

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v2.0.0

- **BREAKING**: `FavoriteToggleButton` no longer supports `icon` prop, and now always shows `starOutline` icon when off and `star` icon when on

### v1.2.1

- Fix: Set container for buttons to `display: inline-flex` so that tooltip aligns better when there is lots of available space.
- Fix: Overlap for buttons to avoid double borders was not correct.
- Fix: Set `flex: none` on icon and text to prevent them from shrinking when there is less available space.

### v1.2.0

- Feat: Show `helperText` in tooltip instead of below the controls.
- Feat: Add red border when there is an error.
- Fix: IE11 layout issues, text was not vertically centered.

### v1.1.0

- Removing package version in class names.

### v1.0.0

- **BREAKING**:
  - rename prop from "errorText" to "helperText"
  - helperText is rendered regardless of error flag when set
  - added "required" prop, automatically add "\*" to label when set

### v0.0.8

- Fix: Accept React.Fragment as a child without giving PropType warning

### v0.0.4

- Refactor: use `comd-click-ripple` from `confirmit-global-styles-material` package instead of custom implementation

### v0.0.3

- Fix: Add ripple when clicking standalone toggle button

### v0.0.2

- Font weight is normal instead of bold for the hovered and selected button states

### v0.0.1

- Initial version
