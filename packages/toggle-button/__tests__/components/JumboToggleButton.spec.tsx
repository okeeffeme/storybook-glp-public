import React from 'react';
import {render, screen} from '@testing-library/react';
import {JumboToggleButton} from '../../src';
import {pencil} from '../../../icons/src';

describe('JumboToggleButton :: ', () => {
  it('should add data- attributes to dom element when passed', () => {
    render(
      <JumboToggleButton title="Toggle" id="toggle1" data-testid="attribute" />
    );
    expect(screen.getByTestId('attribute')).toBeInTheDocument();
  });

  it('should add disabled modifiers when disabled is true', () => {
    render(
      <JumboToggleButton
        title="Toggle"
        id="toggle1"
        iconPath={pencil}
        disabled={true}
      />
    );
    expect(screen.getByTestId('jumbo-toggle-button')).toHaveClass(
      'comd-jumbo-toggle-button--disabled'
    );
    expect(screen.getByTestId('jumbo-toggle-button-graphic')).toHaveClass(
      'comd-jumbo-toggle-button__graphic--disabled'
    );
    expect(screen.getByTestId('jumbo-toggle-button-text-content')).toHaveClass(
      'comd-jumbo-toggle-button__text-content--disabled'
    );
  });

  it('should not add disabled modifiers when disabled is false', () => {
    render(
      <JumboToggleButton
        title="Toggle"
        id="toggle1"
        iconPath={pencil}
        disabled={false}
      />
    );

    expect(screen.getByTestId('jumbo-toggle-button')).not.toHaveClass(
      'comd-jumbo-toggle-button--disabled'
    );
    expect(screen.getByTestId('jumbo-toggle-button-graphic')).not.toHaveClass(
      'comd-jumbo-toggle-button__graphic--disabled'
    );
    expect(
      screen.getByTestId('jumbo-toggle-button-text-content')
    ).not.toHaveClass('comd-jumbo-toggle-button__text-content--disabled');
  });

  it('should add checked modifiers when checked is true', () => {
    render(
      <JumboToggleButton
        title="Toggle"
        id="toggle1"
        iconPath={pencil}
        checked={true}
      />
    );

    expect(screen.getByTestId('jumbo-toggle-button')).toHaveClass(
      'comd-jumbo-toggle-button--checked'
    );
    expect(screen.getByTestId('jumbo-toggle-button-icon')).toHaveClass(
      'comd-jumbo-toggle-button__icon--checked'
    );
    expect(screen.getByTestId('jumbo-toggle-button-text-content')).toHaveClass(
      'comd-jumbo-toggle-button__text-content--checked'
    );
  });

  it('should not add checked modifiers when checked is false', () => {
    render(
      <JumboToggleButton
        title="Toggle"
        id="toggle1"
        iconPath={pencil}
        checked={false}
      />
    );

    expect(screen.getByTestId('jumbo-toggle-button')).not.toHaveClass(
      'comd-jumbo-toggle-button--checked'
    );
    expect(screen.getByTestId('jumbo-toggle-button-icon')).not.toHaveClass(
      'comd-jumbo-toggle-button__icon--checked'
    );
    expect(
      screen.getByTestId('jumbo-toggle-button-text-content')
    ).not.toHaveClass('comd-jumbo-toggle-button__text-content--checked');
  });

  it('should not render img when iconPath is passed', () => {
    render(<JumboToggleButton title="Toggle" id="toggle1" iconPath={pencil} />);

    expect(screen.queryByRole('img')).not.toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      pencil
    );
  });

  it('should not render img when iconPath is passed', () => {
    render(
      <JumboToggleButton title="Toggle" id="toggle1" imageSrc="image.svg" />
    );
    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.getByRole('img')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', 'image.svg');
  });

  it('should render text content when passed', () => {
    render(<JumboToggleButton title="Toggle" id="toggle1" text="some text" />);
    expect(screen.getByText('some text')).toBeInTheDocument();
  });

  it('should not render text content when not passed', () => {
    render(<JumboToggleButton title="Toggle" id="toggle1" />);
    expect(
      screen.queryByTestId('jumbo-toggle-button-text')
    ).not.toBeInTheDocument();
  });
});
