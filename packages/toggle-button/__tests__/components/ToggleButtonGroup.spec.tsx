import React from 'react';
import {render, screen} from '@testing-library/react';

import ToggleButton, {ToggleButtonGroup} from '../../src';
import Icon, {viewSequential} from '../../../icons/src/index';

const noOp = jest.fn();

describe('Toggle button group', () => {
  it('should render group of buttons with icons and text', () => {
    render(
      <ToggleButtonGroup>
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button0"
          value="0"
          text="text"
        />
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button1"
          value="1"
          text="text"
        />
      </ToggleButtonGroup>
    );

    const icon = screen.getAllByTestId('icon');

    expect(screen.getAllByText(/text/i)).toHaveLength(2);
    expect(icon[0].querySelector('path')).toHaveAttribute('d', viewSequential);
    expect(icon[1].querySelector('path')).toHaveAttribute('d', viewSequential);
  });

  it('should render group of buttons with icons only', () => {
    render(
      <ToggleButtonGroup data-testid="toggle-button-group">
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button0"
          value="0"
        />
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button1"
          value="1"
        />
      </ToggleButtonGroup>
    );

    const toggleButtonGroup = screen.getByTestId('toggle-button-group');
    const icon = screen.getAllByTestId('icon');

    expect(
      toggleButtonGroup.querySelectorAll('.comd-toggle-button-grouped').length
    ).toBe(2);
    expect(
      toggleButtonGroup.querySelectorAll('.comd-toggle-button').length
    ).toBe(0);
    expect(icon[0].querySelector('path')).toHaveAttribute('d', viewSequential);
    expect(icon[1].querySelector('path')).toHaveAttribute('d', viewSequential);
  });

  it('should render group of buttons with error text and label', () => {
    render(
      <ToggleButtonGroup
        data-testid="toggle-button-group"
        helperText="error"
        error={true}
        label="label"
        required={true}>
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button0"
          value="0"
        />
      </ToggleButtonGroup>
    );

    const toggleButtonGroup = screen.getByTestId('toggle-button-group');
    const tooltipContent = screen.getByText(/error/i);

    expect(
      toggleButtonGroup.querySelector('.comd-toggle-button-group__label--error')
    ).toHaveTextContent('label *');
    expect(tooltipContent).toHaveAttribute('data-tooltip-content-error');
  });

  it('should not render tooltip when helperText is not set', () => {
    render(
      <ToggleButtonGroup error={true}>
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button0"
          value="0"
        />
      </ToggleButtonGroup>
    );
    expect(screen.getByTestId('toggle-button')).not.toHaveAttribute(
      'tooltip-content'
    );
  });

  it('should render group of buttons within fragments', () => {
    render(
      <ToggleButtonGroup>
        <ToggleButton
          icon={<Icon path={viewSequential} />}
          checked={false}
          onChange={noOp}
          id="button0"
          value="0"
        />
        <>
          <ToggleButton
            icon={<Icon path={viewSequential} />}
            checked={false}
            onChange={noOp}
            id="button1"
            value="0"
          />
          <ToggleButton
            icon={<Icon path={viewSequential} />}
            checked={false}
            onChange={noOp}
            id="button2"
            value="0"
          />
        </>
      </ToggleButtonGroup>
    );
    expect(screen.getAllByTestId('toggle-button')).toHaveLength(3);
    expect(screen.getAllByTestId('toggle-button')[0]).not.toHaveClass(
      'comd-toggle-button'
    );
  });
});
