import React from 'react';
import {render, screen} from '@testing-library/react';
import {FavoriteToggleButton} from '../../src';
import {star, starOutline} from '../../../icons/src/index';

const noOp = jest.fn();

describe('ToggleButton :: FavoriteToggleButton :: ', () => {
  it('should render favorite toggle button with star icon when checked=true', () => {
    render(<FavoriteToggleButton onChange={noOp} id="button" checked={true} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      star
    );
  });

  it('should render favorite toggle button with starOutline icon when checked=false', () => {
    render(
      <FavoriteToggleButton onChange={noOp} id="button" checked={false} />
    );

    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      starOutline
    );
  });
});
