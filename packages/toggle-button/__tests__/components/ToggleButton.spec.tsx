import React from 'react';

import ToggleButton from '../../src';
import {render, screen} from '@testing-library/react';
import Icon, {viewSequential} from '../../../icons/src/index';

const noOp = jest.fn();

describe('Toggle button', () => {
  it('should render toggle button with icon', () => {
    render(
      <ToggleButton
        onChange={noOp}
        icon={<Icon path={viewSequential} />}
        text="test"
        id="button"
        value="1"
      />
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', '24');
    expect(screen.getByRole('checkbox')).toHaveAttribute('value', '1');
    expect(screen.getByTestId('toggle-button')).not.toHaveClass(
      'comd-toggle-button-group__text'
    );
    expect(screen.getByTestId('toggle-button')).not.toHaveClass(
      'comd-toggle-button-group'
    );
  });

  it('should render checked toggle button', () => {
    render(
      <ToggleButton
        onChange={noOp}
        icon={<Icon path={viewSequential} />}
        text="test"
        id="button"
        checked={true}
      />
    );
    expect(screen.getByRole('checkbox')).not.toBeDisabled();
  });

  it('should render disabled toggle button', () => {
    render(
      <ToggleButton
        onChange={noOp}
        icon={<Icon path={viewSequential} />}
        text="test"
        id="button"
        disabled={true}
      />
    );

    expect(screen.getByRole('checkbox')).toBeDisabled();
  });

  it('should render toggle button with text', () => {
    render(<ToggleButton onChange={noOp} text="test" id="button" />);

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.getByTestId('toggle-button-grouped-text')).toHaveTextContent(
      'test'
    );
    expect(
      screen.getByTestId('toggle-button-grouped-text')
    ).toBeInTheDocument();
  });

  it('should render tooltip on top by default', () => {
    render(
      <ToggleButton
        onChange={noOp}
        text="test"
        id="button"
        tooltipText="tooltip"
      />
    );
    expect(screen.getByTestId('toggle-button')).toHaveAttribute(
      'tooltip-placement',
      'top'
    );
  });

  it('should render tooltip', () => {
    render(
      <ToggleButton
        onChange={noOp}
        text="test"
        id="button"
        tooltipText="tooltip"
        tooltipPlacement="left"
      />
    );
    expect(screen.getByTestId('toggle-button')).toHaveAttribute(
      'tooltip-placement',
      'left'
    );
  });
});
