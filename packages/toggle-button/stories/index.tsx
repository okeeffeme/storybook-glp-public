import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import Icon, {viewSequential, codeTags, cube, paperclip} from '../../icons/src';
import {Fieldset} from '../../fieldset/src';

import {
  ToggleButton,
  ToggleButtonGroup,
  FavoriteToggleButton,
  JumboToggleButton,
} from '../src';

import {Switch} from '../../switch/src';

import illustration from './illustration-example.png';

storiesOf('Components/toggle-button', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Toggle button', () => {
    const [btn1State, setBtn1State] = React.useState(false);
    const [btn2State, setBtn2State] = React.useState(true);

    return (
      <>
        <div>
          <ToggleButton
            checked={btn1State}
            icon={<Icon path={viewSequential} />}
            onChange={(e) => {
              setBtn1State(e.target.checked);
            }}
            id="button0"
            text="text"
          />
        </div>

        <div>
          <ToggleButton
            checked={btn2State}
            icon={<Icon path={viewSequential} />}
            onChange={(e) => {
              setBtn2State(e.target.checked);
            }}
            id="button1"
          />
        </div>

        <div>
          <div>Disabled and off</div>
          <ToggleButton
            icon={<Icon path={viewSequential} />}
            onChange={() => {}}
            id="button2"
            disabled={true}
          />
        </div>

        <div>
          <div>Disabled and on</div>
          <ToggleButton
            icon={<Icon path={viewSequential} />}
            onChange={() => {}}
            id="button2"
            disabled={true}
            checked={true}
          />
        </div>
      </>
    );
  })
  .add('Toggle button favorite', () => {
    const [btn1State, setBtn1State] = React.useState(false);
    const [btn2State, setBtn2State] = React.useState(true);
    return (
      <>
        <div>
          <FavoriteToggleButton
            checked={btn1State}
            onChange={(e) => {
              setBtn1State(e.target.checked);
            }}
            id="button0"
          />
        </div>

        <div>
          <FavoriteToggleButton
            checked={btn2State}
            onChange={(e) => {
              setBtn2State(e.target.checked);
            }}
            id="button1"
          />
        </div>

        <div>
          <div>Disabled and off</div>
          <FavoriteToggleButton
            onChange={() => {}}
            id="button2"
            disabled={true}
          />
        </div>

        <div>
          <div>Disabled and on</div>
          <FavoriteToggleButton
            onChange={() => {}}
            checked={true}
            id="button2"
            disabled={true}
          />
        </div>
      </>
    );
  })
  .add('Toggle button with tooltip', () => {
    const [btn1State, setBtn1State] = React.useState(false);

    return (
      <ToggleButton
        checked={btn1State}
        icon={<Icon path={viewSequential} />}
        onChange={(e) => {
          setBtn1State(e.target.checked);
        }}
        id="button0"
        text="text"
        tooltipText="tooltip"
        tooltipPlacement="right"
      />
    );
  })
  .add('JumboToggleButton', () => {
    const [btn1State, setBtn1State] = React.useState(false);
    const [btn2State, setBtn2State] = React.useState(false);

    return (
      <Fieldset>
        <JumboToggleButton
          checked={btn1State}
          iconPath={viewSequential}
          onChange={(e) => {
            setBtn1State(e.target.checked);
          }}
          id="button0"
          title="Do some Action"
          text="Some text explaining what the Action does"
          disabled={boolean('disabled jumbobutton', false)}
        />
        <JumboToggleButton
          checked={btn2State}
          imageSrc={illustration}
          onChange={(e) => {
            setBtn2State(e.target.checked);
          }}
          id="button0"
          title="Do some other Action"
          text="The imageSrc can be SVG (preferred) or any other valid img src value"
          disabled={boolean('disabled jumbobutton', false)}
        />
      </Fieldset>
    );
  })
  .add('Toggle button standalone with no icon', () => {
    const [btn1State, setBtn1State] = React.useState(false);
    const [btn2State, setBtn2State] = React.useState(true);

    return (
      <div style={{display: 'flex'}}>
        <div style={{padding: '30px'}}>
          <ToggleButton
            checked={btn1State}
            onChange={(e) => {
              setBtn1State(e.target.checked);
            }}
            text="text"
            id="button0"
          />
        </div>

        <div style={{padding: '30px'}}>
          <ToggleButton
            checked={btn2State}
            onChange={(e) => {
              setBtn2State(e.target.checked);
            }}
            text="text"
            id="button1"
          />
        </div>

        <div style={{padding: '30px'}}>
          <ToggleButton
            onChange={() => {}}
            text="text"
            id="button3"
            disabled={true}
          />
        </div>
      </div>
    );
  })
  .add('Button group, exclusive', () => {
    const [value, setValue] = React.useState([
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ]);

    const onChange = (e) => {
      setValue(
        value.map((item, i) => {
          return i === Number(e.target.value) ? e.target.checked : false;
        })
      );
    };

    return (
      <div style={{padding: '30px'}}>
        <ToggleButtonGroup>
          <ToggleButton
            checked={value[0]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button0"
            value="0"
          />
          <ToggleButton
            checked={value[1]}
            icon={<Icon path={codeTags} />}
            onChange={onChange}
            id="button1"
            value="1"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[2]}
            onChange={onChange}
            id="button2"
            value="2"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[3]}
            onChange={onChange}
            id="button3"
            value="3"
          />
        </ToggleButtonGroup>

        <ToggleButtonGroup>
          <ToggleButton
            checked={value[4]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button4"
            text="text"
            value="4"
          />
          <ToggleButton
            icon={<Icon path={codeTags} />}
            checked={value[5]}
            onChange={onChange}
            id="button5"
            text="text text text text"
            value="5"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[6]}
            onChange={onChange}
            id="button6"
            text="text"
            value="6"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[7]}
            onChange={onChange}
            id="button7"
            text="text"
            value="7"
            disabled={true}
          />
        </ToggleButtonGroup>
      </div>
    );
  })
  .add('Button group, multiple', () => {
    const [value, setValue] = React.useState([
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ]);
    const onChange = (e) => {
      setValue(
        value.map((item, i) => {
          return i === Number(e.target.value) ? e.target.checked : item;
        })
      );
    };
    return (
      <div style={{padding: '30px'}}>
        <ToggleButtonGroup>
          <ToggleButton
            checked={value[0]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button0"
            value="0"
          />
          <ToggleButton
            checked={value[1]}
            icon={<Icon path={codeTags} />}
            onChange={onChange}
            id="button1"
            value="1"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[2]}
            onChange={onChange}
            id="button2"
            value="2"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[3]}
            onChange={onChange}
            id="button3"
            value="3"
          />
        </ToggleButtonGroup>

        <ToggleButtonGroup>
          <ToggleButton
            checked={value[4]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button4"
            text="text"
            value="4"
          />
          <ToggleButton
            icon={<Icon path={codeTags} />}
            checked={value[5]}
            onChange={onChange}
            id="button5"
            text="text text text text"
            value="5"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[6]}
            onChange={onChange}
            id="button6"
            text="text"
            value="6"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[7]}
            onChange={onChange}
            id="button7"
            text="text"
            value="7"
            disabled={true}
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={true}
            onChange={onChange}
            id="button8"
            text="text"
            value="8"
            disabled={true}
          />
        </ToggleButtonGroup>
      </div>
    );
  })
  .add('Button group, required, error and label', () => {
    const [value, setValue] = React.useState([false, false, false, false]);

    const onChange = (e) => {
      setValue(
        value.map((item, i) => {
          return i === Number(e.target.value) ? e.target.checked : false;
        })
      );
    };

    return (
      <div style={{padding: '30px'}}>
        <ToggleButtonGroup
          error={boolean('error', true)}
          required={true}
          helperText={text('helperText', 'Helper or error text')}
          label={text('label', 'Label')}>
          <ToggleButton
            checked={value[0]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button0"
            value="0"
            text="text"
            tooltipText="tooltip"
          />
          <ToggleButton
            checked={value[1]}
            icon={<Icon path={codeTags} />}
            onChange={onChange}
            id="button1"
            value="1"
            text="text"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[2]}
            onChange={onChange}
            id="button2"
            value="2"
            text="text text"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[3]}
            onChange={onChange}
            id="button3"
            value="3"
            text="text"
          />
        </ToggleButtonGroup>
      </div>
    );
  })
  .add('Button group, helperText and label', () => {
    const [value, setValue] = React.useState([false, false, false, false]);

    const onChange = (e) => {
      setValue(
        value.map((item, i) => {
          return i === Number(e.target.value) ? e.target.checked : false;
        })
      );
    };

    return (
      <div style={{padding: '30px'}}>
        <ToggleButtonGroup helperText="Helper text" label="Label">
          <ToggleButton
            checked={value[0]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button0"
            value="0"
            text="text"
            tooltipText="tooltip"
          />
          <ToggleButton
            checked={value[1]}
            icon={<Icon path={codeTags} />}
            onChange={onChange}
            id="button1"
            value="1"
            text="text"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[2]}
            onChange={onChange}
            id="button2"
            value="2"
            text="text text"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[3]}
            onChange={onChange}
            id="button3"
            value="3"
            text="text"
          />
        </ToggleButtonGroup>
      </div>
    );
  })
  .add('Button group, multiple lines', () => {
    const [value, setValue] = React.useState([
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ]);

    const onChange = (e) => {
      setValue(
        value.map((item, i) => {
          return i === Number(e.target.value) ? e.target.checked : false;
        })
      );
    };

    return (
      <div style={{padding: '30px', width: '350px'}}>
        <ToggleButtonGroup>
          <ToggleButton
            checked={value[0]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button0"
            value="0"
            text="text"
          />
          <ToggleButton
            checked={value[1]}
            icon={<Icon path={codeTags} />}
            onChange={onChange}
            id="button1"
            value="1"
            text="text"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[2]}
            onChange={onChange}
            id="button2"
            value="2"
            text="text text"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[3]}
            onChange={onChange}
            id="button3"
            value="3"
            text="text"
          />
          <ToggleButton
            icon={<Icon path={cube} />}
            checked={value[4]}
            onChange={onChange}
            id="button4"
            value="4"
            text="text text"
          />
          <ToggleButton
            icon={<Icon path={paperclip} />}
            checked={value[5]}
            onChange={onChange}
            id="button5"
            value="5"
            text="text"
          />
          <ToggleButton
            checked={value[6]}
            icon={<Icon path={viewSequential} />}
            onChange={onChange}
            id="button6"
            value="6"
            text="text"
          />
        </ToggleButtonGroup>
      </div>
    );
  })
  .add('Button group, conditional toggles', () => {
    const [state, setState] = React.useState({
      isShowing: false,
      button1: false,
      button2: false,
      button3: false,
    });

    const onChange = (name, checked) => {
      setState({
        ...state,
        [name]: checked,
      });
    };

    return (
      <div style={{padding: '30px', width: '350px'}}>
        <Switch
          id="toggle"
          label="Show hidden toggles"
          checked={state.isShowing}
          onChange={(e) => onChange('isShowing', e.currentTarget.checked)}
        />

        <ToggleButtonGroup>
          <ToggleButton
            checked={state.button1}
            icon={<Icon path={viewSequential} />}
            onChange={(e) => onChange('button1', e.currentTarget.checked)}
            id="button1"
            value="0"
            text="text"
          />
          {state.isShowing && (
            <>
              <ToggleButton
                checked={state.button2}
                icon={<Icon path={codeTags} />}
                onChange={(e) => onChange('button2', e.currentTarget.checked)}
                id="button2"
                value="1"
                text="text"
              />
              <ToggleButton
                icon={<Icon path={cube} />}
                checked={state.button3}
                onChange={(e) => onChange('button3', e.currentTarget.checked)}
                id="button3"
                value="2"
                text="text text"
              />
            </>
          )}
        </ToggleButtonGroup>
      </div>
    );
  });
