import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Fieldset} from '../src';

import Switch from '../../switch/src';
import TextField from '../../text-field/src';

const Pad = ({children}) => {
  return <div style={{padding: '20px'}}>{children}</div>;
};

const InputContainer = ({children}) => {
  return <div style={{width: '300px'}}>{children}</div>;
};

const noop = () => {};

storiesOf('Components/fieldset', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <Pad>
      <Pad>
        <p>
          You still need to control the width's of the children somehow (with
          some wrappers f.ex), the fieldset is just for setting the layout
          direction and add an optional header and helperText.
        </p>

        <p>
          If you need to show multiple Fieldset's next to each other, be sure to
          also use the Fieldset.Group wrapper (see next storybook example).
        </p>
      </Pad>
      <Fieldset
        title="Horizontal layout is usually used for TextFields and similar controls"
        helperText="It will wrap onto a new row when there is not enough space.">
        <InputContainer>
          <TextField label="This" onChange={noop} />
        </InputContainer>
        <InputContainer>
          <TextField label="uses" onChange={noop} />
        </InputContainer>
        <InputContainer>
          <TextField label="horizontal" onChange={noop} />
        </InputContainer>
        <InputContainer>
          <TextField label="layout" onChange={noop} />
        </InputContainer>
      </Fieldset>
      <Fieldset
        layoutStyle={Fieldset.LayoutStyle.Vertical}
        title="Vertical layout is usually used for Switches, radios, checkboxes and similar controls">
        <div>
          <Switch label="This" id="switch-1" />
        </div>
        <div>
          <Switch label="uses" id="switch-2" />
        </div>
        <div>
          <Switch label="vertical" id="switch-3" />
        </div>
        <div>
          <Switch label="layout" id="switch-4" />
        </div>
      </Fieldset>
    </Pad>
  ))

  .add('Fieldset Group', () => (
    <Pad>
      <Pad>
        <p>
          Fieldset Group does not have a title, it just is a container to ensure
          there is correct spacing between multiple adjacent Fieldset's.
        </p>
      </Pad>
      <Fieldset.Group>
        <Fieldset
          title="Horizontal layout is usually used for TextFields and similar controls"
          helperText="It will wrap onto a new row when there is not enough space.">
          <InputContainer>
            <TextField label="This" onChange={noop} />
          </InputContainer>
          <InputContainer>
            <TextField label="uses" onChange={noop} />
          </InputContainer>
          <InputContainer>
            <TextField label="horizontal" onChange={noop} />
          </InputContainer>
          <InputContainer>
            <TextField label="layout" onChange={noop} />
          </InputContainer>
        </Fieldset>
        <Fieldset
          layoutStyle={Fieldset.LayoutStyle.Vertical}
          title="Vertical layout is usually used for Switches, radios, checkboxes and similar controls">
          <div>
            <Switch label="This" id="switch-1" />
          </div>
          <div>
            <Switch label="uses" id="switch-2" />
          </div>
          <div>
            <Switch label="vertical" id="switch-3" />
          </div>
          <div>
            <Switch label="layout" id="switch-4" />
          </div>
        </Fieldset>
      </Fieldset.Group>
    </Pad>
  ));
