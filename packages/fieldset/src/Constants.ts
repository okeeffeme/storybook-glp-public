export enum LayoutStyle {
  Horizontal = 'horizontal',
  Vertical = 'vertical',
}
