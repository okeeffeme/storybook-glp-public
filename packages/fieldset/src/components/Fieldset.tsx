import React from 'react';
import cn from 'classnames';

import {
  extractDataAndAriaProps,
  hasVisibleChildren,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {InformationIcon} from '@jotunheim/react-icons';
import Tooltip from '@jotunheim/react-tooltip';

import FieldsetGroup from './FieldsetGroup';
import {LayoutStyle} from '../Constants';
import classNames from './Fieldset.module.css';

type FieldsetPropTypes = {
  title?: string;
  children?: React.ReactNode;
  helperText?: React.ReactNode;
  layoutStyle?: LayoutStyle;
};

// This is used for easier alignment of content that has no text
// Adding this character makes the element behave as it has text content
// This has to be implemented as a react fragment, to avoid react escaping the values
const ZeroWidthSpaceChar = () => <React.Fragment>&#8203;</React.Fragment>;

const {block, element} = bemFactory({
  baseClassName: 'comd-fieldset',
  classNames,
});

/**
 * @param children if no children are passed, nothing will be rendered
 * @param title title for the fieldset
 * @param helperText helpertext in a help icon next to the title. If a title is not set, this will not be visible either
 * @param layoutStyle how the layout flows for the children, horizontal or vertical
 */
export const Fieldset = ({
  title,
  children,
  helperText,
  layoutStyle = LayoutStyle.Horizontal,
  ...rest
}: FieldsetPropTypes) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return (
    <section
      data-testid="fieldset"
      className={block()}
      {...extractDataAndAriaProps(rest)}>
      {title && (
        <div
          data-testid="fieldset-title"
          className={element('title')}
          data-test-fieldset-title="">
          <span>
            <span>{title}</span>
            {helperText && (
              <span
                data-testid="fieldset-helpertext"
                className={element('helpertext-icon')}
                data-test-fieldset-helpertext="">
                <ZeroWidthSpaceChar />

                <Tooltip content={helperText}>
                  <InformationIcon />
                </Tooltip>
              </span>
            )}
          </span>
        </div>
      )}
      <div
        className={cn(element('content'), element('content', layoutStyle))}
        data-test-fieldset-content="">
        {children}
      </div>
    </section>
  );
};

Fieldset.Group = FieldsetGroup;
Fieldset.LayoutStyle = LayoutStyle;

export default Fieldset;
