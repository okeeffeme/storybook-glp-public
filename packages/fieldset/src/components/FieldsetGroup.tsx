import React from 'react';
import cn from 'classnames';

import {
  extractDataAndAriaProps,
  hasVisibleChildren,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {LayoutStyle} from '../Constants';
import classNames from './FieldsetGroup.module.css';

type FieldsetPropTypes = {
  children?: React.ReactNode;
  layoutStyle?: LayoutStyle;
};

const {block, modifier} = bemFactory({
  baseClassName: 'comd-fieldset-group',
  classNames,
});

/**
 * @param children if no children are passed, nothing will be rendered
 * @param layoutStyle how the layout flows for the children, horizontal or vertical
 */
export const FieldsetGroup = ({
  children,
  layoutStyle = LayoutStyle.Horizontal,
  ...rest
}: FieldsetPropTypes) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return (
    <div
      data-testid="fieldset-group"
      className={cn(block(), modifier(layoutStyle))}
      {...extractDataAndAriaProps(rest)}>
      {children}
    </div>
  );
};

FieldsetGroup.LayoutStyle = LayoutStyle;

export default FieldsetGroup;
