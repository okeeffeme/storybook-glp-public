import React from 'react';
import {render, screen} from '@testing-library/react';
import {Fieldset} from '../../src/components/Fieldset';

describe('Jotunheim React Fieldset :: ', () => {
  it('should show helperText if helperText and title is provided', () => {
    render(
      <Fieldset title="title" helperText="helperText">
        Content
      </Fieldset>
    );

    expect(screen.getByTestId('fieldset-title')).toBeInTheDocument();
    expect(screen.getByTestId('fieldset-helpertext')).toBeInTheDocument();
  });

  it('should not show helperText if helperText is not provided', () => {
    render(<Fieldset title="title">Content</Fieldset>);

    expect(screen.getByTestId('fieldset-title')).toBeInTheDocument();
    expect(screen.queryByTestId('fieldset-helpertext')).not.toBeInTheDocument();
  });

  it('should not show helperText if helperText is provided, but title is not provided', () => {
    render(<Fieldset helperText="helperText">Content</Fieldset>);

    expect(screen.queryByTestId('fieldset-title')).not.toBeInTheDocument();
    expect(screen.queryByTestId('fieldset-helpertext')).not.toBeInTheDocument();
  });

  it('should not show title when title is not provided', () => {
    render(<Fieldset>Content</Fieldset>);

    expect(screen.queryByTestId('fieldset-title')).not.toBeInTheDocument();
    expect(screen.queryByTestId('fieldset-helpertext')).not.toBeInTheDocument();
  });

  it('should not render the fieldset when there are no children', () => {
    render(<Fieldset></Fieldset>);

    expect(screen.queryByTestId('fieldset')).not.toBeInTheDocument();
  });

  it('should not render the fieldset when there are no visible children', () => {
    render(<Fieldset>{false && <div>not rendered</div>}</Fieldset>);

    expect(screen.queryByTestId('fieldset')).not.toBeInTheDocument();
  });

  it('should not render the fieldset when there are no visible children, even when title is set', () => {
    render(
      <Fieldset title="title">{false && <div>not rendered</div>}</Fieldset>
    );

    expect(screen.queryByTestId('fieldset-title')).not.toBeInTheDocument();
    expect(screen.queryByTestId('fieldset')).not.toBeInTheDocument();
  });

  it('should render the fieldset when there are visible children', () => {
    render(
      <Fieldset>
        <div>rendered</div>
      </Fieldset>
    );

    expect(screen.getByTestId('fieldset')).toBeInTheDocument();
  });
});
