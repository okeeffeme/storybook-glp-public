import React from 'react';
import {render, screen} from '@testing-library/react';
import {FieldsetGroup} from '../../src/components/FieldsetGroup';

describe('Jotunheim React FieldsetGroup :: ', () => {
  it('should not render the FieldsetGroup when there are no visible children', () => {
    render(<FieldsetGroup>{false && <div>not rendered</div>}</FieldsetGroup>);

    expect(screen.queryByTestId('fieldset-group')).not.toBeInTheDocument();
  });

  it('should render the FieldsetGroup when there are visible children', () => {
    render(<FieldsetGroup>{true && <div>rendered</div>}</FieldsetGroup>);

    expect(screen.getByTestId('fieldset-group')).toBeInTheDocument();
  });
});
