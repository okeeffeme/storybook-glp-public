import React, {ForwardedRef, forwardRef, ReactNode} from 'react';
import SinglelineTruncate from './Singleline';
import MultilineTruncate from './Multiline';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

const Truncate = forwardRef(
  (
    {children, lines = 1, ...rest}: TruncateProps,
    ref: ForwardedRef<HTMLDivElement>
  ) => {
    return lines === 1 ? (
      <SinglelineTruncate ref={ref} {...extractDataAriaIdProps(rest)}>
        {children}
      </SinglelineTruncate>
    ) : (
      <MultilineTruncate
        ref={ref}
        lines={lines}
        {...extractDataAriaIdProps(rest)}>
        {children}
      </MultilineTruncate>
    );
  }
);

export type TruncateProps = {
  lines?: number;
  children: ReactNode;
};

Truncate.displayName = 'Truncate';

export default Truncate;
