import React, {CSSProperties, ForwardedRef, forwardRef, ReactNode} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

const MultilineTruncate = forwardRef(function MultilineTruncate(
  {lines, children, ...rest}: MultilineTruncateProps,
  ref: ForwardedRef<HTMLDivElement>
) {
  const multiLineTruncateStyles: CSSProperties = {
    position: 'relative',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: lines,
    overflow: 'hidden',
  };

  return (
    <div
      data-testid="multi-line-truncate"
      role="article"
      style={multiLineTruncateStyles}
      ref={ref}
      {...extractDataAriaIdProps(rest)}>
      {children}
    </div>
  );
});

type MultilineTruncateProps = {
  lines: number;
  children: ReactNode;
};

MultilineTruncate.displayName = 'MultilineTruncate';

export default MultilineTruncate;
