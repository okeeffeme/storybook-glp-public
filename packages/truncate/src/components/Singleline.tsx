import React, {CSSProperties, ForwardedRef, forwardRef, ReactNode} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

const singleLineTruncateStyles: CSSProperties = {
  position: 'relative',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
};

const SinglelineTruncate = forwardRef(function SinglelineTruncate(
  {children, ...rest}: SinglelineTruncateProps,
  ref: ForwardedRef<HTMLDivElement>
) {
  return (
    <div
      data-testid="single-line-truncate"
      role="article"
      style={singleLineTruncateStyles}
      ref={ref}
      {...extractDataAriaIdProps(rest)}>
      {children}
    </div>
  );
});

type SinglelineTruncateProps = {
  children: ReactNode;
};

SinglelineTruncate.displayName = 'SinglelineTruncate';

export default SinglelineTruncate;
