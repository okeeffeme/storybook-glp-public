import Truncate, {TruncateProps} from './components/Truncate';

export type {TruncateProps};
export {Truncate};
export default Truncate;
