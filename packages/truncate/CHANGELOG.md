# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-truncate@4.1.1&sourceBranch=refs/tags/@jotunheim/react-truncate@4.1.2&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-truncate

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-truncate@4.1.0&sourceBranch=refs/tags/@jotunheim/react-truncate@4.1.1&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-truncate@4.0.1&sourceBranch=refs/tags/@jotunheim/react-truncate@4.1.0&targetRepoId=1246) (2022-12-15)

### Features

- add role 'article' to Multiline component ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([8a45d73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8a45d73d2d378eb8e00a0f38115f4356cc012022))
- add role 'article' to Singleline component ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([3dfa4e1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3dfa4e14cb416a1ac2802690b787e3fb179b3c48))

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-truncate@4.0.0&sourceBranch=refs/tags/@jotunheim/react-truncate@4.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- fix ForwardRef types ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([78c0c4b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/78c0c4beb906bbe65a920f4ca0278f34398d160d))

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-truncate@3.0.0&sourceBranch=refs/tags/@jotunheim/react-truncate@4.0.0&targetRepoId=1246) (2022-10-11)

### Features

- convert Truncate to TypeScript ([NPM-1081](https://jiraosl.firmglobal.com/browse/NPM-1081)) ([48226cc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/48226cc5c6c4833551127f4dda64fc5137b57d4d))
- remove className prop of Truncate ([NPM-963](https://jiraosl.firmglobal.com/browse/NPM-963)) ([d963332](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d96333267fe976fe39034003316342f4a46e8127))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-truncate@2.2.2&sourceBranch=refs/tags/@confirmit/react-truncate@2.2.3&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-truncate@2.2.1&sourceBranch=refs/tags/@confirmit/react-truncate@2.2.2&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-truncate

## [2.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-truncate@2.2.0&sourceBranch=refs/tags/@confirmit/react-truncate@2.2.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [2.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-truncate@2.1.0&sourceBranch=refs/tags/@confirmit/react-truncate@2.2.0&targetRepoId=1246) (2020-12-18)

### Features

- wrap Truncate in forwardRef to enable wrapping it in Tooltip without extra div wrapper ([NPM-666](https://jiraosl.firmglobal.com/browse/NPM-666)) ([b45bbc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b45bbc600db0d75352609eb87fcc409c69526981))

## CHANGELOG

### v2.1.0

- Refactor: restructure folder names in repository.

### v2.0.0

- **BREAKING**
  - Removed fallback for IE11, there will be single line truncate only.
  - Removed "onLineHeightCalculated" prop

### v1.1.0

- Added prop "onLineHeightCalculated". This callback provides line-height in pixels

### v1.0.0

Component completely rewritten. It now uses css-only truncate
and limits block height by the number of lines as a fallback for unsupported browsers.

- **BREAKING**
  - All props has been removed, except "className", "lines" and "onTruncatedStatusChange" (prev. "onTextProcessed")
  - Callback "onTextProcessed" renamed to "onTruncatedStatusChange"
    and fires only when text starts or stops overflowing its container.
    Arguments also changed, now it's just a boolean that determines if the text is truncated

### v0.0.1

- Initial version
