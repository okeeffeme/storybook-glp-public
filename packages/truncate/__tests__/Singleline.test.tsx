import React from 'react';
import {screen, render} from '@testing-library/react';
import SingleLine from '../src/components/Singleline';

describe('Singleline', () => {
  it('should render Singleline component without children', () => {
    render(<SingleLine>{null}</SingleLine>);

    const article = screen.getByRole('article');

    expect(article.children.length).toBe(0);
  });

  it('should render Singleline component', () => {
    const text = 'This is a single line comment!';

    render(<SingleLine>{text}</SingleLine>);

    const article = screen.getByRole('article');

    expect(article.textContent).toBe(text);
    expect(article).toHaveStyle('white-space: nowrap');
    expect(article).not.toHaveStyle('display: -webkit-box');
    expect(article).toHaveStyle('overflow: hidden');
  });
});
