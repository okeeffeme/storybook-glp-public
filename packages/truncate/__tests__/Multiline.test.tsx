import React from 'react';
import {screen, render} from '@testing-library/react';
import MultiLine from '../src/components/Multiline';

describe('Singleline', () => {
  it('should render Multiline component without children', () => {
    render(<MultiLine lines={5}>{null}</MultiLine>);

    const article = screen.getByRole('article');

    expect(article.children.length).toBe(0);
  });

  it('should render Multiline component', () => {
    const text =
      'This is a multi line comment! This is a multi line comment! This is a multi line comment! This is a multi line comment! This is a multi line comment! This is a multi line comment!';

    render(<MultiLine lines={5}>{text}</MultiLine>);

    const article = screen.getByRole('article');

    expect(article.textContent).toBe(text);
    expect(article).not.toHaveStyle('white-space: nowrap');
    expect(article).toHaveStyle('display: -webkit-box');
    expect(article).toHaveStyle('overflow: hidden');
  });
});
