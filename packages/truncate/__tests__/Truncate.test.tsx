import React from 'react';
import {screen, render} from '@testing-library/react';
import {Truncate} from '../src';

const defaultContainerProps = {
  width: '200px',
  marginBottom: '24px',
};

const text =
  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

const Container = ({children}) => (
  <div style={defaultContainerProps}>{children}</div>
);

describe('CheckMark', () => {
  it('should render the Truncate component', () => {
    render(
      <Container>
        <Truncate>{text}</Truncate>
      </Container>
    );

    const article = screen.getByRole('article');

    expect(article.textContent).toBe(text);
    expect(article).toHaveStyle('white-space:nowrap');
    expect(article).toHaveStyle('overflow:hidden');
  });

  it('should render the Truncate component with multiple lines', () => {
    render(
      <Container>
        <Truncate lines={3}>{text}</Truncate>
      </Container>
    );

    const article = screen.getByRole('article');

    expect(article.textContent).toBe(text);
    expect(article).not.toHaveStyle('white-space: nowrap');
    expect(article).toHaveStyle('display: -webkit-box');
    expect(article).toHaveStyle('overflow: hidden');
  });
});
