/* eslint-disable react/prop-types */

import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Truncate} from '../src/index';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{width: '200px', marginBottom: '24px'}}>{children}</div>
);

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac nunc dignissim purus condimentum efficitur ac eu nisl. Donec ultricies id nisl non porta. Mauris rhoncus ipsum non ornare varius. Donec sollicitudin mauris ut elit pharetra, at luctus enim pellentesque. Curabitur tristique odio vel purus fermentum, sed posuere lorem mollis. Phasellus elementum ut leo vel gravida. Fusce varius risus nec mauris elementum, ac facilisis purus placerat. Nam at purus sit amet mauris placerat placerat. Nulla semper metus sed ligula accumsan laoreet. Nam in ex sed sem sollicitudin dignissim.`;

storiesOf('Components/Truncate', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('basic', () => (
    <Container>
      <Truncate>
        Super long text Super long text Super long text Super long text Super
        long text
      </Truncate>
    </Container>
  ))
  .add('multiline', () => (
    <>
      <Container>
        <h4>3 lines</h4>
        <Truncate lines={3}>{lorem}</Truncate>
      </Container>
      <Container>
        <h4>4 lines</h4>
        <Truncate lines={4}>{lorem}</Truncate>
      </Container>
      <Container>
        <h4>5 lines</h4>
        <Truncate lines={5}>{lorem}</Truncate>
      </Container>
    </>
  ))
  .add('supported children', () => (
    <div>
      <h2>
        Truncate works with text or array of texts. Any other types of children
        will be transformed to text.
      </h2>
      <Container>
        <h4>Text</h4>
        <Truncate lines={1}>{lorem}</Truncate>
      </Container>
      <Container>
        <h4>Array of Text</h4>
        <Truncate lines={2}>{[lorem, lorem, lorem]}</Truncate>
      </Container>
      <Container>
        <h4>Link</h4>
        <Truncate lines={3}>
          <a href="#">{lorem}</a>
        </Truncate>
      </Container>
      <Container>
        <h4>Nested div</h4>
        <Truncate lines={4}>
          <div>
            <div>{lorem}</div>
          </div>
        </Truncate>
      </Container>
    </div>
  ));
