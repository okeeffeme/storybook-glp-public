# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-file-download@2.1.0&sourceBranch=refs/tags/@jotunheim/react-file-download@2.1.1&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-file-download

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-file-download@2.0.2&sourceBranch=refs/tags/@jotunheim/react-file-download@2.1.0&targetRepoId=1246) (2023-01-30)

### Features

- Convert slider package to TypeScript ([NPM-1230](https://jiraosl.firmglobal.com/browse/NPM-1230)) ([eef2966](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eef296614c11d9840a9144362d70e022f3b4f02b))

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-file-download@2.0.1&sourceBranch=refs/tags/@jotunheim/react-file-download@2.0.2&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-file-download

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-file-download@2.0.0&sourceBranch=refs/tags/@jotunheim/react-file-download@2.0.1&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-file-download

## 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-file-download@1.1.4&sourceBranch=refs/tags/@confirmit/react-file-download@1.1.5&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-file-download@1.1.3&sourceBranch=refs/tags/@confirmit/react-file-download@1.1.4&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-file-download

## [1.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-file-download@1.1.2&sourceBranch=refs/tags/@confirmit/react-file-download@1.1.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [1.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-file-download@1.1.1&sourceBranch=refs/tags/@confirmit/react-file-download@1.1.2&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-file-download

## [1.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-file-download@1.1.0...@confirmit/react-file-download@1.1.1) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-file-download

## CHANGELOG

### v1.1.0

- Refactor: restructure folder names in repository.

### v0.0.1

- Initial version
