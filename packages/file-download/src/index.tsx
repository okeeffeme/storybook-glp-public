import withDownload from './components/with-download';
import downloader from './utils/downloader';

export {withDownload, downloader};
export default withDownload;
