import React, {useContext, FC, PropsWithChildren, ChangeEvent} from 'react';

import downloader from '../utils/downloader';
import {AuthorizationContext} from '@jotunheim/react-utils';

interface WithDownload {
  onClick: (e: ChangeEvent) => void;
}
interface WithDownloadProps {
  fileUrl: string;
  onDownloadStarted: () => void;
  onDownloadFinished: () => void;
  onDownloadFailed: () => void;

  authorization: {
    accessToken: string;
  };
}

const withDownload = (Component: FC<PropsWithChildren<WithDownload>>) => {
  const WithDownload: FC<WithDownloadProps> = (props) => {
    const {
      fileUrl,
      onDownloadStarted,
      onDownloadFinished,
      onDownloadFailed,
      ...compProps
    } = props;

    const context = useContext(AuthorizationContext);

    const onDownloadClicked = (evt: ChangeEvent) => {
      evt.preventDefault();
      downloader.download(
        fileUrl,
        props.authorization || context,
        onDownloadStarted,
        onDownloadFinished,
        onDownloadFailed
      );
    };

    return <Component onClick={onDownloadClicked} {...compProps} />;
  };

  return WithDownload;
};

export default withDownload;
