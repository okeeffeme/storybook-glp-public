interface Authorization {
  accessToken: string;
}

interface NavigatorWithSaveBlob extends Navigator {
  msSaveBlob: (blob: Blob, fileName: string) => boolean;
}

const downloader = {
  createRequest(url: string, authorization: Authorization): XMLHttpRequest {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.responseType = 'arraybuffer';
    this.setRequestHeaders(xhr, authorization.accessToken);

    return xhr;
  },

  setRequestHeaders(xhr: XMLHttpRequest, accessToken: string) {
    if (accessToken) {
      xhr.setRequestHeader('Authorization', `Bearer ${accessToken}`);
    }

    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  },

  saveFile(xhr: XMLHttpRequest) {
    const fileName = this.getFileName(xhr);
    const type = xhr.getResponseHeader('Content-Type') ?? undefined;
    const blob = new Blob([xhr.response], {
      type: type,
    });

    if (this.browserHasIEBlobBug(window.navigator)) {
      window.navigator.msSaveBlob(blob, fileName);
    } else {
      const URL = window.URL || window.webkitURL;
      const downloadUrl = URL.createObjectURL(blob);
      const link = this.createLinkElement(fileName, downloadUrl);

      if (link) {
        link.click();
        this.cleanUp(link, downloadUrl);
      } else {
        window.location.assign(downloadUrl);
      }
    }
  },

  createErrorMessage(xhr: XMLHttpRequest): string {
    return xhr.response.byteLength
      ? String.fromCharCode.apply(null, [...new Uint8Array(xhr.response)])
      : '';
  },

  getFileName(request: XMLHttpRequest): string {
    let fileName = '';
    const disposition = request.getResponseHeader('Content-Disposition');
    if (disposition && disposition.indexOf('attachment') !== -1) {
      const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      const matches = filenameRegex.exec(disposition);
      if (matches !== null && matches[1])
        fileName = matches[1].replace(/['"]/g, '');
    }

    return fileName;
  },

  browserHasIEBlobBug(value: Navigator): value is NavigatorWithSaveBlob {
    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
    return value['msSaveBlob'] !== undefined;
  },

  createLinkElement(
    fileName: string,
    downloadUrl: string
  ): HTMLAnchorElement | null {
    if (fileName) {
      // use HTML5 a[download] attribute to specify filename
      const link = document.createElement('a');
      // safari doesn't support this yet
      if (typeof link.download !== 'undefined') {
        link.className = 'downloadHelper';
        link.href = downloadUrl;
        link.download = fileName;
        document.body.appendChild(link);
        return link;
      }
    }

    return null;
  },

  cleanUp(link: HTMLAnchorElement, url: string) {
    setTimeout(function () {
      const URL = window.URL || window.webkitURL;
      URL.revokeObjectURL(url);
      if (link) {
        document.body.removeChild(link);
      }
    }, 3000);
  },

  download(
    url: string,
    authorization: Authorization,
    startHandler: () => void,
    successHandler: () => void,
    errorHandler: (err: string) => void
  ) {
    const request = this.createRequest(url, authorization),
      me = {...this};

    request.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        successHandler && successHandler();
        me.saveFile(request);
      } else {
        errorHandler(me.createErrorMessage(request));
      }
    };

    startHandler && startHandler();
    request.send();
  },
};

export default downloader;
