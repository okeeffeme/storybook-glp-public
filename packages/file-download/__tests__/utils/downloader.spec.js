import downloader from '../../src/utils/downloader';

function stringToArrayBuffer(str) {
  const arrayBuffer = new ArrayBuffer(str.length),
    bufferView = new Uint8Array(arrayBuffer);

  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufferView[i] = str.charCodeAt(i);
  }
  return arrayBuffer;
}

describe('Utils: downloader', () => {
  const mockXHR = (status, errorMsg, setRequestHeaderMock) =>
    class {
      open = jest.fn();
      send = jest.fn(() => {
        this.status = status;
        this.onload();
      });
      getResponseHeader = jest.fn(() => 'attachment;filename=gurba.foo');
      setRequestHeader = setRequestHeaderMock;
      response = stringToArrayBuffer(errorMsg);
    };

  const mockURL = {
    createObjectURL: jest.fn(() => 'http://localhost/foo/bar'),
    revokeObjectURL: jest.fn(),
  };

  let downloadEventHandlers, oldXHR, oldURL, setRequestHeaderMock;

  beforeEach(() => {
    oldXHR = window.XMLHttpRequest;
    oldURL = window.URL;

    window.URL = mockURL;

    downloadEventHandlers = {
      onDownloadStarted: jest.fn(),
      onDownloadFinished: jest.fn(),
      onDownloadFailed: jest.fn(),
    };

    setRequestHeaderMock = jest.fn();
  });

  afterEach(() => {
    window.XMLHttpRequest = oldXHR;
    window.URL = oldURL;
  });

  describe('creating request', () => {
    it('should set request authorization header if present', () => {
      window.XMLHttpRequest = mockXHR(200, '', setRequestHeaderMock);

      downloader.createRequest('http://dummy.url', {
        accessToken: 'bar',
      });

      expect(setRequestHeaderMock.mock.calls.length).toBe(2);
      expect(setRequestHeaderMock.mock.calls[0][0]).toEqual('Authorization');
      expect(setRequestHeaderMock.mock.calls[0][1]).toEqual('Bearer bar');
    });

    it('should not set request authorization header if not present', () => {
      window.XMLHttpRequest = mockXHR(200, '', setRequestHeaderMock);

      downloader.createRequest('http://dummy.url', {
        foo: 'bar',
      });

      expect(setRequestHeaderMock.mock.calls.length).toBe(1);
    });
  });

  describe('downloading', () => {
    it('should download file when remote request succeeds', () => {
      jest.useFakeTimers();

      window.XMLHttpRequest = mockXHR(200, '', setRequestHeaderMock);

      downloader.download(
        'http://dummy.url',
        {foo: 'bar', gurba: 'lurba'},
        downloadEventHandlers.onDownloadStarted,
        downloadEventHandlers.onDownloadFinished,
        downloadEventHandlers.onDownloadFailed
      );

      const nodes = document.body.childNodes,
        link = nodes[0];

      expect(nodes.length).toBe(1);
      expect(link.href).toEqual('http://localhost/foo/bar');
      expect(link.download).toEqual('gurba.foo');

      jest.runAllTimers();

      expect(document.body.childNodes.length).toBe(0);
      expect(downloadEventHandlers.onDownloadStarted).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFinished).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFailed).not.toHaveBeenCalled();
      expect(window.URL.revokeObjectURL).toHaveBeenCalled();
    });

    it('should download file when browser has IE bug', () => {
      window.XMLHttpRequest = mockXHR(200, '', setRequestHeaderMock);
      const oldMsSaveBlob = window.navigator.msSaveBlob;
      window.navigator.msSaveBlob = jest.fn();

      downloader.download(
        'http://dummy.url',
        {foo: 'bar', gurba: 'lurba'},
        downloadEventHandlers.onDownloadStarted,
        downloadEventHandlers.onDownloadFinished,
        downloadEventHandlers.onDownloadFailed
      );

      expect(window.navigator.msSaveBlob).toHaveBeenCalled();
      expect(document.body.childNodes.length).toBe(0);
      expect(downloadEventHandlers.onDownloadStarted).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFinished).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFailed).not.toHaveBeenCalled();

      window.navigator.msSaveBlob = oldMsSaveBlob;
    });

    it('should download file in Safari', () => {
      jest.useFakeTimers();

      const oldLocationAssign = window.location.assign,
        oldDocumentCreateElement = window.document.createElement;

      window.XMLHttpRequest = mockXHR(200, '', setRequestHeaderMock);
      window.document.createElement = () => ({});
      window.location.assign = jest.fn();

      downloader.download(
        'http://dummy.url',
        {foo: 'bar', gurba: 'lurba'},
        downloadEventHandlers.onDownloadStarted,
        downloadEventHandlers.onDownloadFinished,
        downloadEventHandlers.onDownloadFailed
      );

      expect(document.body.childNodes.length).toBe(0);
      expect(window.location.assign).toHaveBeenCalledWith(
        'http://localhost/foo/bar'
      );
      expect(downloadEventHandlers.onDownloadStarted).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFinished).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFailed).not.toHaveBeenCalled();

      jest.runAllTimers();
      expect(window.URL.revokeObjectURL).toHaveBeenCalled();

      window.document.createElement = oldDocumentCreateElement;
      window.location.assign = oldLocationAssign;
    });

    it('should handle error when remote request fails', () => {
      window.XMLHttpRequest = mockXHR(500, 'gurba fail', setRequestHeaderMock);

      downloader.download(
        'http://dummy.url',
        {foo: 'bar', gurba: 'lurba'},
        downloadEventHandlers.onDownloadStarted,
        downloadEventHandlers.onDownloadFinished,
        downloadEventHandlers.onDownloadFailed
      );

      expect(document.body.childNodes.length).toBe(0);
      expect(downloadEventHandlers.onDownloadStarted).toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFinished).not.toHaveBeenCalled();
      expect(downloadEventHandlers.onDownloadFailed).toHaveBeenCalledWith(
        'gurba fail'
      );
    });
  });
});
