import React from 'react';
import renderer from 'react-test-renderer';
import {render, screen} from '@testing-library/react';
import {AuthorizationContext} from '../../../utils/src';
import withDownload from '../../src/components/with-download';
import downloader from '../../src/utils/downloader';
import userEvent from '@testing-library/user-event';

jest.mock('../../src/utils/downloader', () => ({
  download: jest.fn(),
}));

describe('File download', () => {
  let DownloadComponent, TestComp;

  beforeEach(() => {
    //eslint-disable-next-line
    TestComp = (props) => {
      return <a {...props} />;
    };

    DownloadComponent = withDownload(TestComp);
  });

  describe('rendering', () => {
    it('should render correct component', () => {
      const rendered = renderer
        .create(
          <DownloadComponent className="foo bar" fooProp="barVal">
            Gurba
          </DownloadComponent>
        )
        .toJSON();
      expect(rendered).toMatchSnapshot();
    });
  });

  describe('clicking', () => {
    let clickEvent, downloadEventHandlers;

    beforeEach(() => {
      downloader.download.mockClear();
      clickEvent = {
        preventDefault: jest.fn(),
      };

      downloadEventHandlers = {
        onDownloadStarted: jest.fn(),
        onDownloadFinished: jest.fn(),
        onDownloadFailed: jest.fn(),
      };
    });

    it('should handle click when authorization header is set in context', () => {
      render(
        <AuthorizationContext.Provider value={{accessToken: 'foo'}}>
          <DownloadComponent
            data-testid="download-component"
            fileUrl="http://dummy.url"
            {...downloadEventHandlers}
          />
        </AuthorizationContext.Provider>
      );
      userEvent.click(screen.getByTestId('download-component'));

      expect(downloader.download.mock.calls[0][0]).toEqual('http://dummy.url');
      expect(downloader.download.mock.calls[0][1]).toEqual({
        accessToken: 'foo',
      });
    });

    it('should handle click when authorization header is not set in context', () => {
      render(
        <DownloadComponent
          data-testid="download-component"
          fileUrl="http://dummy.url"
          {...downloadEventHandlers}
        />
      );
      userEvent.click(screen.getByTestId('download-component'), clickEvent);

      expect(downloader.download.mock.calls[0][0]).toEqual('http://dummy.url');
      // expect to get the default context when no provider is set
      expect(downloader.download.mock.calls[0][1]).toEqual({
        accessToken: null,
      });
    });
  });
});
