import React from 'react';
import {render, waitFor} from '@testing-library/react';
import user from '@testing-library/user-event';

import NotificationsNav from '../src/NotificationsNav';

const MyNotifications = () => {
  return <div data-testid="my-notifications" />;
};

describe('Notifications :: ', () => {
  const defaultProps = {
    notificationsChildren: <MyNotifications />,
  };

  it('shows notifications component when opened', async () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = render(<NotificationsNav {...props} />);

    expect(wrapper.queryByTestId('my-notifications')).not.toBeInTheDocument();

    const button = wrapper.getByTestId('app-header-notifications-toggle');

    user.click(button);
    await waitFor(() => {
      expect(wrapper.getByTestId('my-notifications')).toBeInTheDocument();
    });
  });
});
