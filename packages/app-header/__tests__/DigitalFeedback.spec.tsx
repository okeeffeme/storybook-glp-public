import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import useScript from '../src/feedback/useScript';
import DigitalFeedback from '../src/feedback/DigitalFeedback';
import AppFeedback from '../src/feedback/AppFeedback';

jest.mock('../src/feedback/useScript');
const useScriptMock = useScript as jest.Mock;

const defaultProps = {
  user: {
    firstName: 'First Name',
    lastName: 'Last Name',
    extra: 'whatever',
  },
  title: 'App Name',
};

describe('Digital Feedback :: ', () => {
  it('should set context on AppFeedback', () => {
    render(<DigitalFeedback {...defaultProps} />);

    const {
      user: {firstName, lastName},
      title: applicationName,
    } = defaultProps;
    expect(AppFeedback.getContext()).toEqual({
      firstName,
      lastName,
      applicationName,
      url: window.location.href,
    });
  });

  it('should not render nav button if script not loaded', () => {
    useScriptMock.mockReturnValue(false);
    const wrapper = render(<DigitalFeedback />);
    expect(wrapper.queryByTestId('feedback-button')).not.toBeInTheDocument();
  });

  it('should render nav button if script loaded', () => {
    useScriptMock.mockReturnValue(true);
    const wrapper = render(<DigitalFeedback />);
    expect(wrapper.queryByTestId('feedback-button')).toBeInTheDocument();
  });

  it('should call legacy click handler by default', () => {
    const legacy = (window['cfRunDigitalFeedback'] = jest.fn());
    const modern = jest.spyOn(AppFeedback, 'emit');

    useScriptMock.mockReturnValue(true);
    const wrapper = render(<DigitalFeedback {...defaultProps} />);
    fireEvent.click(wrapper.getByTestId('feedback-button'));

    expect(legacy).toHaveBeenCalledWith(AppFeedback.getContext());
    expect(modern).not.toHaveBeenCalled();
  });

  it('should emit AppFeedback event if legacy handler not set', () => {
    delete window['cfRunDigitalFeedback'];
    const modern = jest.spyOn(AppFeedback, 'emit');

    useScriptMock.mockReturnValue(true);
    const wrapper = render(<DigitalFeedback />);
    fireEvent.click(wrapper.getByTestId('feedback-button'));

    expect(modern).toHaveBeenCalledWith('app-feedback:show');
  });
});
