import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import UserMenu from '../src/UserMenu';

describe('User Menu :: ', () => {
  const defaultProps = {
    user: {
      firstName: 'FirstName',
      lastName: 'LastName',
    },
  };

  it('renders correctly with default props', () => {
    render(<UserMenu {...defaultProps} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.getByText('FirstName LastName')).toBeInTheDocument();
    expect(screen.getByText('Logout')).toBeInTheDocument();
  });

  it('should not show user settings link if userSettingsHref is not set', () => {
    const props = {
      ...defaultProps,
      userSettingsText: 'Custom User Settings',
    };

    render(<UserMenu {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.queryAllByTestId('user-settings-link')).toHaveLength(0);
  });

  it('renders correctly with custom logout and user settings texts and hrefs', () => {
    const props = {
      ...defaultProps,
      logoutText: 'Custom Logout',
      logoutHref: '/customLogout',
      userSettingsText: 'Custom User Settings',
      userSettingsHref: 'user-settings/',
    };

    render(<UserMenu {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.getByText('Custom User Settings')).toBeInTheDocument();
    expect(screen.getByText('Custom Logout')).toBeInTheDocument();
    expect(screen.getAllByTestId('simple-link')[1]).toHaveAttribute(
      'href',
      '/customLogout'
    );
  });

  it('pass onLogoutClick to MenuItem', () => {
    const onLogoutClick = jest.fn();
    const props = {
      onLogoutClick,
      ...defaultProps,
    };

    render(<UserMenu {...props} />);

    userEvent.click(screen.getByTestId('icon'));
    userEvent.click(screen.getByText('Logout'));

    expect(onLogoutClick).toHaveBeenCalled();
  });

  it('pass onUserSettingsClick to MenuItem', () => {
    const onUserSettingsClick = jest.fn();
    const props = {
      onUserSettingsClick,
      userSettingsHref: 'user-settings/',
      ...defaultProps,
    };

    render(<UserMenu {...props} />);

    userEvent.click(screen.getByTestId('icon'));
    userEvent.click(screen.getByText('User settings'));

    expect(onUserSettingsClick).toHaveBeenCalled();
  });
});
