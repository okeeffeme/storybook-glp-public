import React from 'react';
import {render, waitFor} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import SearchNav from '../src/SearchNav';

const MySearch = () => {
  return <div data-testid="my-search" />;
};

describe('Search :: ', () => {
  const defaultProps = {
    searchChildren: <MySearch />,
  };

  it('shows search component when opened', async () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = render(<SearchNav {...props} />);

    expect(wrapper.queryByTestId('my-search')).not.toBeInTheDocument();

    const button = wrapper.getByTestId('app-header-search-button');

    userEvent.click(button);
    await waitFor(() => {
      expect(wrapper.getByTestId('my-search')).toBeInTheDocument();
    });
  });
});
