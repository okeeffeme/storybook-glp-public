import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import RecentMenu from '../src/RecentMenu';

describe('Recent Menu :: ', () => {
  const onCreateNewClick = jest.fn();
  const onRecentMenuToggle = () => {};
  const defaultProps = {
    allItemsHref: '/allItems',
    allItemsText: 'All items',
    recentItemsText: 'Recent items',
    items: [
      {
        id: '1',
        name: 'item 1',
        href: '/item1',
      },
      {
        id: '2',
        name: 'item 2',
        href: '/item2',
      },
      {
        id: '3',
        name: 'item 3',
        href: '/item3',
      },
    ],
    createNewHref: '/createNew',
    createNewText: 'Create new',
    onCreateNewClick,
    viewDeletedHref: '/viewDeleted',
    viewDeletedText: 'Deleted items',
    openRecentMenu: true,
    title: 'App title',
    onRecentMenuToggle,
  };

  it('should render correctly with default props', () => {
    render(<RecentMenu {...defaultProps} />);

    expect(screen.getByText('App title')).toBeInTheDocument();
    expect(screen.getAllByTestId('simple-link')).toHaveLength(6);
  });

  it('should render "all items" items', () => {
    const props = {
      ...defaultProps,
      showAllItems: true,
    };

    render(<RecentMenu {...props} />);

    expect(screen.getAllByTestId('all-items')).toHaveLength(1);
  });

  it('should not render "all items" items', () => {
    const props = {
      ...defaultProps,
      showAllItems: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryAllByTestId('all-items')).toHaveLength(0);
  });

  it('should render "create new" item', () => {
    const props = {
      ...defaultProps,
      showCreateNew: true,
    };

    render(<RecentMenu {...props} />);

    expect(screen.getAllByTestId('create-new')).toHaveLength(1);
  });

  it('should not render "create new" item', () => {
    const props = {
      ...defaultProps,
      showCreateNew: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryAllByTestId('create-new')).toHaveLength(0);
  });

  it('should render "view deleted" item', () => {
    const props = {
      ...defaultProps,
      showViewDeleted: true,
    };

    render(<RecentMenu {...props} />);

    expect(screen.getAllByTestId('deleted-items')).toHaveLength(1);
  });

  it('should not render "view deleted" item', () => {
    const props = {
      ...defaultProps,
      showViewDeleted: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryAllByTestId('deleted-items')).toHaveLength(0);
  });

  it('should render "recent" items', () => {
    const props = {
      ...defaultProps,
      showRecentItems: true,
    };

    render(<RecentMenu {...props} />);

    expect(screen.getAllByTestId('recent-item')).toHaveLength(3);
  });

  it('should not render "recent" items', () => {
    const props = {
      ...defaultProps,
      showRecentItems: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryAllByTestId('recent-item')).toHaveLength(0);
  });

  it('should render recent items header when there are items', () => {
    render(<RecentMenu {...defaultProps} />);

    expect(screen.getByTestId('recent-item-header')).toBeInTheDocument();
  });

  it('should not render recent items header when there are no items', () => {
    const props = {
      ...defaultProps,
      items: [],
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryByTestId('recent-item-header')).not.toBeInTheDocument();
  });

  it('should call onRecentItemClick whenever recent menu item is clicked', () => {
    const onRecentItemClick = jest.fn();
    const props = {
      ...defaultProps,
      onRecentItemClick,
    };

    render(<RecentMenu {...props} />);

    const items = screen.getAllByTestId('simple-link');
    const firstRecentItem = items[3];
    const secondRecentItem = items[4];

    expect(firstRecentItem).toHaveAttribute('href', '/item1');
    expect(secondRecentItem).toHaveAttribute('href', '/item2');

    userEvent.click(firstRecentItem);
    userEvent.click(secondRecentItem);

    expect(onRecentItemClick).toHaveBeenCalledWith(
      expect.anything(),
      props.items[0]
    );
    expect(onRecentItemClick).toHaveBeenCalledWith(
      expect.anything(),
      props.items[1]
    );
  });

  it('should call onCreateNewClick when click create new', () => {
    const props = {
      ...defaultProps,
    };

    render(<RecentMenu {...props} />);

    userEvent.click(screen.getAllByTestId('simple-link')[2]);

    expect(onCreateNewClick).toHaveBeenCalled();
  });

  it('should render loading indicators', () => {
    const props = {
      ...defaultProps,
      recentMenuLoading: true,
    };

    render(<RecentMenu {...props} />);

    expect(screen.getByTestId('busy-dots')).toBeInTheDocument();
  });

  it('should not render loading indicators', () => {
    const props = {
      ...defaultProps,
      recentMenuLoading: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryByTestId('busy-dots')).not.toBeInTheDocument();
  });

  it('should not render recent menu dropdown', () => {
    const props = {
      ...defaultProps,
      showRecentMenuDropDown: false,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryByTestId('popover-content')).not.toBeInTheDocument();
  });

  it('should render custom items in the dropdown menu', () => {
    const onCustomItemClick = jest.fn();
    const onCustomItem2Click = jest.fn();
    const props = {
      ...defaultProps,
      customItems: [
        {
          id: 'testID',
          text: 'Custom Item',
          onClick: onCustomItemClick,
        },
        {
          text: 'Custom Item2',
          onClick: onCustomItem2Click,
        },
        {
          text: 'Custom Item3',
          href: '#testLink',
        },
      ],
    };

    render(<RecentMenu {...props} />);

    const customItem = screen.getByText('Custom Item');
    expect(customItem).toBeInTheDocument();
    userEvent.click(customItem);
    expect(onCustomItemClick).toHaveBeenCalledWith('testID');

    const customItem2 = screen.getByText('Custom Item2');
    expect(customItem2).toBeInTheDocument();
    userEvent.click(customItem2);
    expect(onCustomItem2Click).toHaveBeenCalled();

    expect(screen.getByText('Custom Item3')).toBeInTheDocument();
    expect(screen.getByRole('link', {name: 'Custom Item3'})).toHaveAttribute(
      'href',
      '#testLink'
    );
  });

  it('should not render custom items in the dropdown menu if customItems is empty', () => {
    const props = {
      ...defaultProps,
      customItems: [],
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryByTestId('custom-menu-item')).not.toBeInTheDocument();
  });

  it('should not render custom items in the dropdown menu if customItems is not defined', () => {
    const props = {
      ...defaultProps,
    };

    render(<RecentMenu {...props} />);

    expect(screen.queryByTestId('custom-menu-item')).not.toBeInTheDocument();
  });
});
