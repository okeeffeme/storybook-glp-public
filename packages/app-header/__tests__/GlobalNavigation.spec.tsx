import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import GlobalNavigation from '../src/GlobalNavigation';

describe('Global Navigation :: ', () => {
  const defaultProps = {
    applications: [
      {
        id: 'discoveryanalytics',
        name: 'Discovery Analytics',
        links: {
          self: 'http://localhost/discoveryanalytics/',
        },
      },
      {
        id: 'endusermanagement',
        name: 'End User Management',
        isExternal: true,
        links: {
          self: 'http://localhost/confirm/authoring/Confirmit.aspx?loc=endusers',
        },
      },
      {
        id: 'hierarchymanagement',
        name: 'Hierarchy Management',
        links: {
          self: 'http://localhost/hierarchymanagement/',
        },
      },
      {
        id: 'professionalauthoring',
        name: 'Professional Authoring',
        isExternal: true,
        links: {
          self: 'http://localhost/confirm/authoring/',
        },
      },
      {
        id: 'reportal',
        name: 'Reportal',
        isExternal: true,
        links: {
          self: 'http://localhost/confirm/reportal/',
        },
      },
      {
        id: 'smarthub',
        name: 'SmartHub',
        current: true,
        links: {
          self: 'http://localhost/hub/',
        },
      },
      {
        id: 'surveydesigner',
        name: 'Survey Designer',
        links: {
          self: 'http://localhost/surveydesigner/',
        },
      },
      {
        id: 'actions',
        name: 'Actions',
        links: {
          self: 'http://localhost/actions/',
        },
      },
      {
        id: 'actionmanagement',
        name: 'Action Management',
        links: {
          self: 'http://localhost/actionmanagement/',
        },
      },
    ],
  };

  it('renders correctly ', () => {
    render(<GlobalNavigation {...defaultProps} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.getAllByTestId('global-navigation-menu-app')).toHaveLength(9);
  });

  it('has correct current app', () => {
    render(<GlobalNavigation {...defaultProps} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.getAllByTestId('global-navigation-menu-app')).toHaveLength(9);
    expect(screen.getByTestId('SmartHub-app')).toHaveAttribute(
      'href',
      'http://localhost/hub/'
    );
  });

  it('should have icon when is marked as external', () => {
    const props = {
      ...defaultProps,
    };

    render(<GlobalNavigation {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    expect(
      screen.getAllByTestId('external-global-navigation-app')
    ).toHaveLength(3);
  });
});
