import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import AppHeader from '../src/AppHeader';

import {basicHub} from '../../icons/src';

describe('Jotunheim React App Header :: ', () => {
  const defaultProps = {
    oidcEnabled: false,
    title: 'App Title',
    user: {
      firstName: 'FirstName',
      lastName: 'LastName',
    },
  };

  const applications = [
    {
      id: 'home',
      name: 'Home',
      isExternal: true,
      links: {
        self: '#http://localhost/confirm/home/',
      },
    },
    {
      id: 'professionalauthoring',
      name: 'Professional Authoring',
      isExternal: true,
      links: {
        self: '#http://localhost/confirm/authoring/',
      },
    },

    {
      id: 'discoveryanalytics',
      name: 'Discovery Analytics',
      links: {
        self: 'http://localhost/discoveryanalytics/',
      },
    },
    {
      id: 'endusermanagement',
      name: 'End User Management',
      isExternal: true,
      links: {
        self: 'http://localhost/confirm/authoring/Confirmit.aspx?loc=endusers',
      },
    },
  ];

  it('should render with default props', () => {
    renderRTL(<AppHeader {...defaultProps} />);

    expect(screen.getByTestId('app-header')).toBeInTheDocument();
    expect(screen.getByRole('banner')).toBeInTheDocument();
    expect(screen.getByTestId('user-help-section')).toBeInTheDocument();
    expect(
      screen.queryByTestId('current-container-icon')
    ).not.toBeInTheDocument();
  });

  it('should render with given testid when provided', () => {
    renderRTL(<AppHeader {...defaultProps} data-testid="my-app-header" />);

    expect(screen.getByTestId('my-app-header')).toBeInTheDocument();
    expect(screen.queryByTestId('app-header')).not.toBeInTheDocument();
  });

  it('should render with default props', () => {
    renderRTL(<AppHeader {...defaultProps} />);

    expect(screen.getByRole('banner')).toBeInTheDocument();
    expect(screen.getByTestId('user-help-section')).toBeInTheDocument();
    expect(
      screen.queryByTestId('current-container-icon')
    ).not.toBeInTheDocument();
  });

  it('should render with default props & hub icon', () => {
    const props = {
      ...defaultProps,
      currentContainerIconPath: basicHub,
    };

    renderRTL(<AppHeader {...props} />);

    expect(screen.getByTestId('current-container-icon')).toBeInTheDocument();
  });

  describe('Global Navigation', () => {
    it('should render GlobalNavigation', () => {
      const props = {
        ...defaultProps,
        applications,
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.getByTestId('global-navigation')).toBeInTheDocument();
    });

    it('should not render GlobalNavigation if showGlobalNavigation is false', () => {
      const props = {
        ...defaultProps,
        showGlobalNavigation: false,
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.queryByTestId('global-navigation')).not.toBeInTheDocument();
    });
  });

  describe('User Menu', () => {
    it('should render UserMenu', () => {
      const props = {
        ...defaultProps,
        applications,
        user: {
          firstName: 'NextFirstName',
          lastName: 'NextLastName',
        },
        userSettingsText: 'UserSettings Text',
        logoutText: 'Logout Text',
        logoutHref: 'Logout Href',
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.getByTestId('user')).toBeInTheDocument();
    });

    it('should not render UserMenu if showUserMenu is false', () => {
      const props = {
        ...defaultProps,
        showUserMenu: false,
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.queryByTestId('user')).not.toBeInTheDocument();
    });

    it('should not show user settings link if user does not have access to professional, and no access to new user settings', () => {
      const props = {
        ...defaultProps,
        applications: applications.filter(
          (app) => app.id !== 'professionalauthoring'
        ),
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.getByTestId('user')).toBeInTheDocument();
      expect(
        screen.queryByTestId('user-settings-link')
      ).not.toBeInTheDocument();
    });

    it('should link user settings to professional if user does not have access to new settings page', () => {
      const props = {
        ...defaultProps,
        applications,
      };

      renderRTL(<AppHeader {...props} />);

      userEvent.click(screen.getAllByTestId('icon')[2]);

      expect(screen.getAllByTestId('simple-link')[0]).toHaveAttribute(
        'href',
        '#http://localhost/confirm/authoring/Confirmit.aspx?loc=usersettings'
      );
    });

    it('should link user settings to new user settings if user has access to new settings page', () => {
      const props = {
        ...defaultProps,
        applications,
        oidcEnabled: true,
      };

      renderRTL(<AppHeader {...props} />);

      userEvent.click(screen.getAllByTestId('icon')[2]);

      expect(screen.getAllByTestId('simple-link')[0]).toHaveAttribute(
        'href',
        '#http://localhost/confirm/home/userSettings'
      );
    });
  });

  describe('Recent Menu', () => {
    const onCreateNewClick = () => {};
    const onRecentMenuToggle = () => {};
    const recentMenuDefaultProps = {
      ...defaultProps,
      allItemsHref: '/allItems',
      allItemsText: 'All items',
      recentItemsText: 'Recent items',
      items: [
        {
          id: '1',
          name: 'item 1',
          href: '/item1',
        },
        {
          id: '2',
          name: 'item 2',
          href: '/item2',
        },
        {
          id: '3',
          name: 'item 3',
          href: '/item3',
        },
      ],
      createNewHref: '/createNew',
      createNewText: 'Create new',
      onCreateNewClick,
      viewDeletedHref: '/viewDeleted',
      viewDeletedText: 'Deleted items',
      openRecentMenu: true,
      onRecentMenuToggle,
      recentMenuLoading: true,
    };

    it('should not render recent menu if showRecentMenu is false', () => {
      const props = {
        ...recentMenuDefaultProps,
        showRecentMenu: false,
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.queryByTestId('recent')).not.toBeInTheDocument();
    });

    it('renders Recent Menu with default props', () => {
      const props = {
        ...recentMenuDefaultProps,
      };

      renderRTL(<AppHeader {...props} />);

      expect(screen.getByTestId('recent')).toBeInTheDocument();
    });
  });

  describe('Search', () => {
    it('should render Search when specified', () => {
      renderRTL(<AppHeader {...defaultProps} showSearch={true} />);

      expect(screen.getByTestId('search')).toBeInTheDocument();
    });

    it('should not render Search when not specified', () => {
      renderRTL(<AppHeader {...defaultProps} />);

      expect(screen.queryByTestId('search')).not.toBeInTheDocument();
    });
  });

  describe('Notifications', () => {
    it('should render Notifications when specified', () => {
      const component = renderRTL(
        <AppHeader {...defaultProps} showNotifications={true} />
      );
      expect(
        component.getByTestId('app-header-notifications-nav')
      ).toBeInTheDocument();
    });

    it('should not render Notifications when not specified', () => {
      const component = renderRTL(<AppHeader {...defaultProps} />);

      expect(
        component.queryByTestId('app-header-notifications-nav')
      ).not.toBeInTheDocument();
    });

    it('should show empty count of unread notifications when there are no unread', () => {
      const component = renderRTL(
        <AppHeader
          {...defaultProps}
          showNotifications={true}
          unreadNotifications={0}
        />
      );

      expect(
        component.getByTestId('app-header-notifications-badge')
      ).toHaveTextContent('');
    });

    it('should show count of unread notifications', () => {
      const component = renderRTL(
        <AppHeader
          {...defaultProps}
          showNotifications={true}
          unreadNotifications={3}
        />
      );

      expect(
        component.getByTestId('app-header-notifications-badge')
      ).toHaveTextContent('3');
    });
  });
});
