import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Help from '../src/Help';

describe('Help :: ', () => {
  it('should not link to knowledge base when url is not specified', () => {
    render(<Help />);

    userEvent.click(screen.getByTestId('icon'));

    expect(screen.getByTestId('simple-link')).toBeInTheDocument();
  });

  it('should show link to knowledge base', () => {
    const onKnowledgeBaseClick = jest.fn();
    const props = {
      knowledgeBaseHref: '/help',
      onKnowledgeBaseClick,
      shouldShowKnowledgeBaseLink: true,
    };

    render(<Help {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    const help = screen.getAllByTestId('simple-link')[0];

    userEvent.click(help);

    expect(onKnowledgeBaseClick).toHaveBeenCalled();
    expect(help).toHaveAttribute('href', '/help');
  });

  it('should show link to knowledge base with new props if old ones are not specified', () => {
    const props = {
      knowledgeBaseLink: {
        href: '/help',
        target: '_help',
      },
    };

    render(<Help {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    const help = screen.getAllByTestId('simple-link')[0];

    expect(help).toHaveAttribute('href', '/help');
    expect(help).toHaveAttribute('target', '_help');
  });

  it('should not show link to REST API when restApiLink is undefined', () => {
    render(<Help />);

    expect(screen.queryAllByTestId('simple-link')).toHaveLength(0);
  });

  it('should show link to REST API', () => {
    const props = {
      restApiLink: {
        href: '/api',
        target: '_api',
      },
    };

    render(<Help {...props} />);

    userEvent.click(screen.getByTestId('icon'));

    const help = screen.getAllByTestId('simple-link')[0];

    expect(help).toHaveAttribute('href', '/api');
    expect(help).toHaveAttribute('target', '_api');
  });

  it('should not show link to news when shouldShowNewsLink is false', () => {
    render(<Help />);

    expect(screen.queryAllByTestId('simple-link')).toHaveLength(0);
  });

  it('should show link to news when shouldShowNewsLink is true', () => {
    const onNewsClick = jest.fn();
    const props = {
      onNewsClick,
      shouldShowNewsLink: true,
    };

    render(<Help {...props} />);

    userEvent.click(screen.getByTestId('icon'));
    userEvent.click(screen.getByText('What’s New'));

    expect(onNewsClick).toHaveBeenCalled();
  });

  it('should not show link to tour guide reset when shouldShowTourGuideReset is false', () => {
    render(<Help />);

    expect(screen.queryByText('Reset Tour Guide')).not.toBeInTheDocument();
  });

  it('should show link to tour guide reset when shouldShowTourGuideReset is true', () => {
    const onTourGuideReset = jest.fn();
    const e = {
      preventDefault: jest.fn(),
    };
    const props = {
      onTourGuideReset,
      shouldShowTourGuideReset: true,
    };

    render(<Help {...props} />);

    userEvent.click(screen.getByTestId('icon'));
    fireEvent.click(screen.getByText('Reset Tour Guide'), e);

    expect(onTourGuideReset).toHaveBeenCalled();
  });
});
