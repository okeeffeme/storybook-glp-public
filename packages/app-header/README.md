# Jotunheim React App Header

A component to have a shared header in all applications.

[Changelog](./CHANGELOG.md)

## Usage

You should also install `confirmit-applications-service` to get the list of available applications, and to get the apps to show up in the correct order. They will **not** appear in the correct order in the storybook example.

### Normal usage

Install the package from npm (@confirmit/react-app-header), and use the default export.
This will use the component that has been compiled and prepared for distribution, as any normal 3rd party component would usually be.

### Module federation

App Header also includes a component that can be used for module federation.
You can use this component instead by importing from {FederatedAppHeader} from '@jotunheim/react-app-header'.

For this to work you also need to enable module federation in your webpack config.

See Survey Designer implementation for more information.

NB!
When creating a new major version of this package, you also need to do the following:

- Change import from "ds" in src/federated/AppHeader.tsx component to use the new version
- Change the alias in .storybook/webpack.config.js to use the new version
- Expose the new major version in the Confirmit.Components repo (and keep exposing the previous one via aliasing)
- Add an alias webpack.config.js in the Confirmit.Components repo for the new version

## Design decisions

The component is not easy to extend with custom menu items for the reason we want to keep it the same across the apps, and not because no one ever thought of it.
The API is intentionally not very generic, since apps should not be allowed to pass on whatever menu items they want here.
It's probably a good idea ask in [#frontend](https://forstaglobal.slack.com/archives/C03FDUTHF8U) slack channel before you add any new props here to extend the functionality, and please make sure UX is fully aligned on any proposed changes since this will affect app apps.
