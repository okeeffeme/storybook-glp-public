# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [17.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.4.2&sourceBranch=refs/tags/@jotunheim/react-app-header@17.4.3&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.4.1&sourceBranch=refs/tags/@jotunheim/react-app-header@17.4.2&targetRepoId=1246) (2023-04-11)

### Bug Fixes

- App Header should support data and aria attributes on top-level dom element ([NPM-1295](https://jiraosl.firmglobal.com/browse/NPM-1295)) ([3d2f479](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3d2f479d8f718f01c1f08222c2da923ba7928060))

## [17.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.4.1&sourceBranch=refs/tags/@jotunheim/react-app-header@17.4.1&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-app-header

# [17.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.36&sourceBranch=refs/tags/@jotunheim/react-app-header@17.4.0&targetRepoId=1246) (2023-03-30)

### Features

- add custom items in app-header dropdown menu [NPM-1289](https://jiraosl.firmglobal.com/browse/NPM-1289) ([NPM-1289](https://jiraosl.firmglobal.com/browse/NPM-1289)) ([7b88fde](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7b88fdea079568291e1a3c7d368b74303fb517a5))
- add new utils file either.ts that export Either utility type ([NPM-1289](https://jiraosl.firmglobal.com/browse/NPM-1289)) ([5169551](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/516955137c84404e238f5b8c15ea69e2fd081183))

## [17.3.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.35&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.36&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.34&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.35&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.33&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.34&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.32&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.33&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.31&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.32&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.30&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.31&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.29&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.30&targetRepoId=1246) (2023-02-20)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.27&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.29&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- migrate tests from Enzyme to RTL within app-header package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([afe5d33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/afe5d33fb82589cc35226774625fd1e3b1a0003f))

## [17.3.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.27&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.28&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- migrate tests from Enzyme to RTL within app-header package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([c812890](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c8128904a5ba376c2424b3538af7ee9c8db675d1))

## [17.3.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.26&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.27&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.25&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.26&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.24&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.25&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.23&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.24&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.22&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.23&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.21&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.22&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.20&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.21&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.18&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.20&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.18&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.19&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.17&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.18&targetRepoId=1246) (2023-01-16)

### Bug Fixes

- set applicationName to be undefined ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([0f7ebbe](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0f7ebbeebc1712d547fc15b74702adafa82143e8))

## [17.3.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.16&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.17&targetRepoId=1246) (2023-01-12)

### Bug Fixes

- delete unsupported operator ([d8aa32f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d8aa32fc078c8c5cac065e0869e58e68249cb5dc))

## [17.3.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.15&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.16&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.14&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.15&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.13&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.14&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.12&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.13&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.10&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.12&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.10&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.11&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.7&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.10&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.7&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.9&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.7&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.8&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.6&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.7&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.5&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.6&targetRepoId=1246) (2022-11-09)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.4&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.5&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.2&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.3&targetRepoId=1246) (2022-10-26)

### Bug Fixes

- only expose necessary fields to AppFeedback ([SE-3777](https://jiraosl.firmglobal.com/browse/SE-3777)) ([b5931da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b5931da9279a29d27194a6e2f9b50a874dcb70dc))

## [17.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.3.1&sourceBranch=refs/tags/@jotunheim/react-app-header@17.3.2&targetRepoId=1246) (2022-10-21)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@18.0.0&sourceBranch=refs/tags/@jotunheim/react-app-header@18.0.1&targetRepoId=1246) (2022-10-18)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.2.0&sourceBranch=refs/tags/@jotunheim/react-app-header@17.2.1&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-app-header

# [17.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.12&sourceBranch=refs/tags/@jotunheim/react-app-header@17.2.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- close Search and Notifications nav when clicking the toggle while its open ([NPM-1032](https://jiraosl.firmglobal.com/browse/NPM-1032)) ([aa9df92](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/aa9df9296f232d3fcd14836bb1a270bb433968d0))

### Features

- Add ability to display notifications ([NPM-1032](https://jiraosl.firmglobal.com/browse/NPM-1032)) ([f291dff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f291dff43789b09c2706e83d20ffd98b079814cf))
- expose AppHeaderProps type ([NPM-1032](https://jiraosl.firmglobal.com/browse/NPM-1032)) ([26af96d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/26af96d7df531e8acc46e4b85b5da4538f9bc877))

## [17.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.11&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.12&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.10&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.11&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.9&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.10&targetRepoId=1246) (2022-09-09)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.8&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.7&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.8&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.6&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.7&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.4&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.5&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.1&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.2&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.1.0&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.1&targetRepoId=1246) (2022-08-08)

### Bug Fixes

- switch to strict version of events package ([SE-3705](https://jiraosl.firmglobal.com/browse/SE-3705)) ([11dfd9a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/11dfd9af19cd26cc70edc429f3fd0c44f5eca908))

## [17.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.0.4&sourceBranch=refs/tags/@jotunheim/react-app-header@17.1.0&targetRepoId=1246#packages/table/__tests__/components/table.spec.js) (2022-08-03)

### Changes

- AppFeedback object globally exposed on window for DF scripting
- Applications can use AppFeedback to emit custom lifecycle events for DF
- Avoid loading the same feedback program multiple times

## [17.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.0.3&sourceBranch=refs/tags/@jotunheim/react-app-header@17.0.4&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.0.2&sourceBranch=refs/tags/@jotunheim/react-app-header@17.0.3&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-app-header

## [17.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.0.1&sourceBranch=refs/tags/@jotunheim/react-app-header@17.0.2&targetRepoId=1246) (2022-06-30)

### Bug Fixes

- expose v17 of federated header ([NPM-1036](https://jiraosl.firmglobal.com/browse/NPM-1036)) ([cf7c7fd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cf7c7fdb2b5ff4e6f8e3b2c50edccba593a8572f))

## [17.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-app-header@17.0.0&sourceBranch=refs/tags/@jotunheim/react-app-header@17.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-app-header

# 17.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [16.9.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.16&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.17&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.15&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.16&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.14&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.15&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [16.9.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.13&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.14&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.12&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.13&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.10&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.11&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.8&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.9&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.7&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.8&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.6&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.7&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.5&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.6&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.4&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.5&targetRepoId=1246) (2022-02-25)

### Bug Fixes

- update feedback icon in AppHeader ([SE-3562](https://jiraosl.firmglobal.com/browse/SE-3562)) ([15021aa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/15021aa870a35697ab189ae02a60fcac6425f554))

## [16.9.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.2&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.9.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.9.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.9.1&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.8.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.8.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.8.3&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.8.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.8.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.8.2&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.8.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.8.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.8.1&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.7.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.7.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.7.3&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.7.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.7.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.7.2&targetRepoId=1246) (2021-11-23)

### Bug Fixes

- make onCatch an optional prop on federated app header ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([6a69366](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6a693667ad06e79f6590377bb49aa155f734484f))

## [16.7.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.7.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.7.1&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

# [16.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.6.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.7.0&targetRepoId=1246) (2021-11-01)

### Features

- update help/about texts ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([229a52e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/229a52ef53244bc0618ede989f074e54176f93dd))
- update to new forsta logo in app-header ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([0b59376](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0b59376c70ae3baa2fb8f76acfad6911dd61d866))

# [16.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.5.4&sourceBranch=refs/tags/@confirmit/react-app-header@16.6.0&targetRepoId=1246) (2021-10-22)

### Features

- add restApiLink and knowledgeBaseLink properties in AppHeader help menu ([NPM-886](https://jiraosl.firmglobal.com/browse/NPM-886)) ([f25b843](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f25b8430257c6e750d3ac27f50d4d9181dbb43eb))

## [16.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.5.3&sourceBranch=refs/tags/@confirmit/react-app-header@16.5.4&targetRepoId=1246) (2021-10-20)

### Bug Fixes

- replace @confirmit/react-modal with @confirmit/react-dialog because of package renaming ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([cb47cac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cb47cac4cebcc454b6e83b9d461f168b5edf5311))

## [16.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.5.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.5.2&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.5.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.5.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-app-header

# [16.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.4.4&sourceBranch=refs/tags/@confirmit/react-app-header@16.5.0&targetRepoId=1246) (2021-09-14)

### Bug Fixes

- add missing overflow styles, to allow dropdowns in app header to scroll in cases where window height is too small to accomodate the content of the dropdowns ([NPM-866](https://jiraosl.firmglobal.com/browse/NPM-866)) ([ed6a482](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ed6a482f38d4ae4181aa86ef581e34ce7fba7a85))
- move data attribute to correct element ([NPM-866](https://jiraosl.firmglobal.com/browse/NPM-866)) ([9f3df2e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9f3df2e7bb16482586860aaf262b1081d091867f))

### Features

- wrap App Header in material theme provider. It doesnt really make sense to allow apps to override this setting for the app header ([NPM-866](https://jiraosl.firmglobal.com/browse/NPM-866)) ([4645756](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4645756ef971c6c0d9433661decfb2ca5b40983b))

## [16.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.4.3&sourceBranch=refs/tags/@confirmit/react-app-header@16.4.4&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.4.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.4.3&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.4.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.4.2&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.4.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.4.1&targetRepoId=1246) (2021-08-30)

### Bug Fixes

- adjust css import in app-header/federated/Loader ([NPM-845](https://jiraosl.firmglobal.com/browse/NPM-845)) ([70ba0c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70ba0c2544c1185c79579ecb3c312c0086ef3b26))

## [16.4.0]

### Features

- Convert to TypeScript. Add more types for Digital Feedback. ([NPM-843](https://jiraosl.firmglobal.com/browse/NPM-843)) ([257e923](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/257e923a8843df9972dc3e423a0f6790009d0396))

### Cleanup

- Remove Feedback component and related props (showFeedbackButton, feedbackOptions)
  (Decided to be pragmatic and make this a minor instead of breaking since no usage of Feedback was found in our apps)
- Remove all unused "hidden..." props.

## [16.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.3.4&sourceBranch=refs/tags/@confirmit/react-app-header@16.3.5&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.3.3&sourceBranch=refs/tags/@confirmit/react-app-header@16.3.4&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.3.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.3.3&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/pull-requests/886/diff) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-app-header

### Features

- Add currentContainerIconPath prop to allow for consistent icon view

## [16.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.3.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.3.1&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-app-header

# [16.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.51&sourceBranch=refs/tags/@confirmit/react-app-header@16.3.0&targetRepoId=1246) (2021-07-22)

### Features

- Expose federated app header wrapper ([NPM-827](https://jiraosl.firmglobal.com/browse/NPM-827)) ([c869de0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c869de06c74dcda6c46c570a108a29e05f56e889))

## [16.2.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.50&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.51&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.49&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.50&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.48&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.49&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.47&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.48&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.46&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.47&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.45&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.46&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.44&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.45&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.43&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.44&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.42&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.43&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.41&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.42&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.40&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.41&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.39&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.40&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.38&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.39&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.37&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.38&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.36&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.37&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.35&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.36&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.34&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.35&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.32&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.33&targetRepoId=1246) (2021-05-05)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.31&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.32&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.30&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.31&targetRepoId=1246) (2021-04-21)

### Bug Fixes

- Move "Deleted Items" link below "All Items" link ([NPM-766](https://jiraosl.firmglobal.com/browse/NPM-766)) ([ee74124](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ee74124f513b4b3f67eff5e9ee6d179c40f0fb71))

## [16.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.29&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.30&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.28&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.29&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.27&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.28&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.26&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.27&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [16.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.25&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.26&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.24&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.25&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.23&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.24&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.22&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.23&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.21&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.22&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.20&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.21&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.19&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.20&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.18&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.19&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.17&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.18&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.16&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.17&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.15&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.16&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.14&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.15&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.13&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.14&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.12&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.13&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.11&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.12&targetRepoId=1246) (2021-03-09)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.10&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.11&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.9&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.10&targetRepoId=1246) (2021-02-22)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.8&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.9&targetRepoId=1246) (2021-02-19)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.6&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.7&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.5&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.6&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.3&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.4&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.3&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.2.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.2&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-app-header

# [16.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.8&sourceBranch=refs/tags/@confirmit/react-app-header@16.2.0&targetRepoId=1246) (2021-02-10)

### Features

- menu icons on the right use position sticky, to stay in viewport when app has a horizontal scrollbar ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([96c3c7e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/96c3c7e864eff218fa764c4c09be4c8e7ac612ef))

## [16.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.7&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.8&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.6&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.7&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.5&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.6&targetRepoId=1246) (2021-01-24)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.4&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.5&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.3&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.4&targetRepoId=1246) (2021-01-20)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.2&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.3&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-app-header

## [16.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.1.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.2&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-app-header

# [16.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.0.1&sourceBranch=refs/tags/@confirmit/react-app-header@16.1.0&targetRepoId=1246) (2021-01-04)

### Features

- upgrade to new Confirmit logo ([NPM-640](https://jiraosl.firmglobal.com/browse/NPM-640)) ([90a54e8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/90a54e842d2d51d89b43a16f7098413ff6314d1f))

## [16.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@16.0.0&sourceBranch=refs/tags/@confirmit/react-app-header@16.0.1&targetRepoId=1246) (2020-12-18)

**Note:** Version bump only for package @confirmit/react-app-header

# [16.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.17&sourceBranch=refs/tags/@confirmit/react-app-header@16.0.0&targetRepoId=1246) (2020-12-15)

### Bug Fixes

- use Simple Link for all items link ([NPM-658](https://jiraosl.firmglobal.com/browse/NPM-658)) ([dbb9995](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dbb9995f40fc1be975f011b2d8b2c8263071d115))

### BREAKING CHANGES

- if supplying basename to the history instance, and supplying the history instance via context to simple-link, then allItemsHref should no longer include the basename
- if NOT both supplying basename to history and using HistoryContext, then this change should not affect you

#### BEFORE

```
const history = createBrowserHistory({
  basename: '/app'
});

<HistoryContext value={history}>
  <App>
    <AppHeader
      allItemsHref="/app/all-items"
    />
  </App>
</HistoryContext>
```

#### AFTER

```
const history = createBrowserHistory({
  basename: '/app'
});

<HistoryContext value={history}>
  <App>
    <AppHeader
      allItemsHref="/all-items"
    />
  </App>
</HistoryContext>
```

## [15.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.16&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.15&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.14&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.13&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.12&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.11&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.10&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.9&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.6&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.3&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.2&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-app-header

## [15.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@15.0.1&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-app-header

# [15.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@14.0.1&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [15.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@14.0.1&sourceBranch=refs/tags/@confirmit/react-app-header@15.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [14.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.38&sourceBranch=refs/tags/@confirmit/react-app-header@14.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([70c6fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70c6fc66e591517275e65be695c787ec215a9841))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

## [13.3.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.37&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.38&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.36&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.37&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.34&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.35&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.33&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.34&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.30&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.31&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.29&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.30&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.27&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.28&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.23&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.24&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.22&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.23&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.20&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.21&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-app-header@13.3.18&sourceBranch=refs/tags/@confirmit/react-app-header@13.3.19&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.15](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.3.14...@confirmit/react-app-header@13.3.15) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.11](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.3.10...@confirmit/react-app-header@13.3.11) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.3.8...@confirmit/react-app-header@13.3.9) (2020-08-27)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.3.5...@confirmit/react-app-header@13.3.6) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-app-header

## [13.3.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.3.1...@confirmit/react-app-header@13.3.2) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-app-header

# [13.3.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-app-header@13.2.21...@confirmit/react-app-header@13.3.0) (2020-08-12)

### Features

- Dropdown components obtain z-index from zIndexStack ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([65cc197](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/65cc197c2cb09b7844f836bc7a59d4befc1ed57e))

# CHANGELOG

## v13.2.0

- Feat: Add more user fields to be tracked by in-app feedback
  - Supported props of `user` object being passed to app header (all optional):
    `userId`,
    `userName`,
    `firstName`,
    `lastName`,
    `companyId`,
    `companyName`,
    `languageId`,
    `email`.

## v13.1.0

- Feat: Add support for search popover using new "showSearch" and "searchChildren" props
  - Also supports a searchTooltip prop

## v13.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-app-header`

## v12.2.0

- Feat: Error text in Feedback Dialog is moved to a tooltip

## v12.1.4

- Refactor: remove required from `className` prop on AppHeaderButton

## v12.1.0

- Refactor: Update icons in app header to Material Icons
- Fix: Change default text in help menu from "News" to "What’s New"
- Fix: Change default text in feedback tooltip from "Provide feedback" to "Give us feedback"

## v12.0.4

- Refactor: change from `type` to `appearance` on Button's

## v12.0.2

- Fix: Increase max-height for dropdowns to avoid scrolling

## v12.0.0

- **BREAKING**:
  - Major UI changes to @confirmit/react-text-field @confirmit/react-textarea

## v11.1.0

- Feature: use external DigitalFeedback program to capture in-product feedback.
  - Use new props `showDigitalFeedbackButton` and `digitalFeedbackSettings: { programUrl }` to control behavior.
  - Use existing props `title` and `user: { userName }` to pass contextual data.

## v11.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency

## v10.3.21

-Fix: Add preventDefault to reset tour guide Help item.

## v10.3.20

- Refactor: move a variable for estimated scrollbar size to confirmit-global-styles-material project

## v10.3.2

- Fix: Dont show Recent Items header when there are no recent items
- Fix: Increase width of Global navigation dropdown to allow a scrollbar to appear without reflowing items

## v10.3.0

- Feature: New Reset TourGuide button in Help menu.
  - New props:
    `shouldShowTourGuideReset`: to control if the button in Help dropdown is visible.
    `onTourGuideReset`: callback on Reset Tour Guide click.
    `tourGuideResetText`: text to be displayed in the button.

## v10.2.2

- Refactor: update internal components using poppers to use `placement` instead of `defaultPlacement` prop

## v10.2.0

- Feature: New About page, available from the Help menu. This change means that the Help menu will always be visible.
  - New prop aboutLinkText, to control the text of the menu item

## v10.1.5

- Fix: use more color variables from global-styles-material package

## v10.1.0

- Add a tooltip for the feedback button
- Add `Other` option for the `Product Area` selector in the Feedback dialog
- New props:
  `feedbackTooltipContent` - To pass tooltip content for the feedback button.
  `otherOptionLabel` property in `feedbackOptions` to pass the label for `Other` option.

## v9.1.5

- Chore: Update from confirmit-textarea component to @confirmit/react-textarea.

## v9.0.2

- Refactor: Move variables from the deprecated confirmit-css-standards package into this project.

## v9.0.0

- Major changes in @confirmit/react-truncate

## v8.1.2

- Bugfix: Styles were not properly applied as theme.js were skipped in sideEffects in packages.json

## v8.1.0

- Feat: New props:
  - `showFeedbackButton` and `feedbackOptions`. Both props should be set to display feedback button in the top right corner. `onSendFeedbackClick` - callback function should be set in `feedbackOptions`. It accepts an object with the following properties `productArea`,`summary`,`description`,`contactName`,`contactEmail`.

## v8.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

## v7.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

## v6.0.0

- **BREAKING** `helpHref` renamed to `knowledgeBaseHref`.
- **BREAKING** `onHelpClick` renamed to `onKnowledgeBaseClick`.
- **BREAKING** `showHelp` renamed to `shouldShowKnowledgeBaseLink`.
- **BREAKING** Removed props:
  - `userSettingsHref` removed, and link is now automatically created, based on `oidcEnabled` prop. If `oidcEnabled` is `true`, link to user settings will be the new user settings page. Otherwise it will be linked to professional. If the user doesnt have access to professional or new user settings, the link will not appear.
- Feat: New props:
  - **BREAKING** `oidcEnabled`: bool - required prop.
  - `shouldShowNewsLink`: bool - if your app supports in-app-news, this should be set to true, and you should add `confirmit-in-app-news` to your app.
  - `onNewsClick`: func - callback when news link is clicked.
  - `newsLinkText`: string - text to appear in help dropdown for the news link.
  -

## v5.4.7

- Fix: Added user settings link to the user menu

## v5.4.2

- Fix: Added home link to the confirmit logo (tests are fixed)

## v5.4.0

- Added home link to the confirmit logo (tests are failing)

## v5.3.23

- Fix: Use correct placement prop in user menu dropdown, to avoid warning message in console.

## v5.3.6

- Chore: Use same list of apps in demo pages as confirmit-icons app icons to reduce code duplication.

## v5.3.0

- Fix: When clicking inside the usermenu it should close.

## v5.2.1

- Fix: When opening any of the dropdowns on a low-resolution monitor, in some cases the dropdown would cover the dropdown-toggle which might look weird for the user. This happened because of the positioning library we use (popper.js) tries to fit the entire dropdown into viewport. Turning this feature off for the dropdowns in the app-header resolved this.

## v5.2.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

## v5.1.12

- Fix: an incorrect classname on recent list dropdown caused it to not get the correct styling when it was active

## v5.1.0

- Added CSS modules for scoping the CSS to the packages. Read more about the changes and how to adopt them in your project here: [How to modify the webpack config file](../../docs/CssModules.md)

## v5.0.3

- Fix: Remove ThemeProvider as Dropdowns no longer use theme

## v5.0.1

- Fix: Some incorrect classnames after refactoring caused global nav dropdown to have too much padding
- Fix: Use !important on all css. No external css should be able to override styling in app-header, so it should be fine with using that here.
- Fix: Add "missing" css that comes from dropdown theme, so it is not dependent on the default theme being imported (mostly useful for material apps)

## v5.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only

## v4.3.0

- Revert the v3.3.0 change that removed Action Management app. This change will be done in the backend.

## v4.2.0

- Fix: Update AppIcon reference to use component from confirmit-icons instead of confirmit-react-components

## v4.1.0

- Bugfix: Missing update on classname to change toggle background

## v4.0.0

- Update dropdowns to work with new dropdowns from confirmit-react-components@4.0.0 package. No external changes.

## v3.4.0

- Change Global Navigation waffle menu to have a dark background instead of white

## v3.3.0

- Remove Action Management app from the waffle menu (global navigation), since this app should be a "sub-application" of Actions

## v3.2.0

- Remove `z-index: 9999` on `.co-app-header`, because it would display header above modals etc. which was not the intention. The intention was to always show dropdowns in the header above other items
- Add `z-index: 9999` on `.co-app-header .co-dd-menu` to ensure they will always be shown above other items
- Add `pointer-events: none` to `.co-dd.co-dd--theme-co-global-navigation .app.app--current` because clicking the current active item should not cause navigation.

## v3.1.5

- Use AppIcon from react-components package

## v3.0.0

- precompiled css is used in components (might be a breaking change for webpack scss and css loaders)
