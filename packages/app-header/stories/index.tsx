import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text, object, select} from '@storybook/addon-knobs';
import {action} from '@storybook/addon-actions';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import AppHeader, {FederatedAppHeader} from '../src';
import {basicHub} from '../../icons/src';

import applications from './apps';
import {TextField} from '../../text-field/src';
import Fieldset, {LayoutStyle} from '../../fieldset/src';
import type {Application} from '../src/types';
import Inbox from '../../inbox/src';

const noop = () => {};

const logAction = (e, name) => {
  e.preventDefault && e.preventDefault();
  action(name)(e);
};

const appsWithoutHome = applications.filter((app) => app.id !== 'home');

const appSelection: Record<string, Application[]> = {
  'All apps': applications,
  'All apps (except Home)': appsWithoutHome,
};

const customItems = [
  {
    text: text('customItem1', 'Custom Item1', 'customItems'),
    onClick: (e) => logAction(e, 'onCustomItemClick'),
  },
  {
    text: text('customItem2', 'Custom Item2', 'customItems'),
    onClick: (e) => logAction(e, 'onCustomItemClick'),
  },
  {
    text: text('customItem3', 'Custom Item3', 'customItems'),
    onClick: (e) => logAction(e, 'onCustomItemClick'),
  },
];

const getKnobs = () => ({
  oidcEnabled: boolean('oidcEnabled', true, 'App header'),
  showRecentMenu: boolean('showRecentMenu', true, 'App header'),
  showGlobalNavigation: boolean('showGlobalNavigation', true, 'App header'),
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore: This is for some reason not compatible
  applications: select<Application[]>(
    'applications',
    appSelection,
    applications,
    'App header'
  ),

  // User dropdown
  user: object(
    'user',
    {
      userId: 1,
      userName: 'administrator',
      firstName: 'Admin I.',
      lastName: 'Strator',
      companyId: 1,
      companyName: 'Confirmit',
      languageId: 9,
      email: 'test@firmglobal.com',
    },
    'User dropdown'
  ),
  userSettingsText: text('userSettingsText', 'User settings', 'User dropdown'),
  logoutText: text('logoutText', 'Log Out', 'User dropdown'),
  logoutHref: text('logoutHref', '#logout', 'User dropdown'),
  showUserMenu: boolean('showUserMenu', true, 'User dropdown'),
  onLogoutClick: (e) => logAction(e, 'onLogoutClick'),
  onUserSettingsClick: (e) => logAction(e, 'onUserSettingsClick'),

  // Recent menu/dropdown
  title: text('title', 'Smart Hub', 'Recent menu'),
  showAllItems: boolean('showAllItems', true, 'Recent menu'),
  showRecentItems: boolean('showRecentItems', true, 'Recent menu'),
  showRecentMenuDropDown: boolean(
    'showRecentMenuDropDown',
    true,
    'Recent menu'
  ),
  showCreateNew: boolean('showCreateNew', true, 'Recent menu'),
  showViewDeleted: boolean('showViewDeleted', true, 'Recent menu'),
  allItemsHref: text('allItemsHref', '#allItems', 'Recent menu'),
  allItemsText: text('allItemsText', 'All hubs', 'Recent menu'),
  recentItemsText: text('recentItemsText', 'Recent hubs', 'Recent menu'),
  items: object(
    'items',
    [
      {
        id: '15',
        name: 'SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1 SmartHub1',
        href: '#smarthub1',
      },
      {id: '16', name: 'SmartHub2', href: '#smarthub2'},
      {id: '17', name: 'SmartHub3', href: '#smarthub3'},
    ],
    'Recent menu'
  ),
  createNewHref: text('createNewHref', '#createNew', 'Recent menu'),
  createNewText: text('createNewText', 'Create hub', 'Recent menu'),
  viewDeletedHref: text('viewDeletedHref', '#viewdeleted', 'Recent menu'),
  viewDeletedText: text('viewDeletedText', 'Deleted hubs', 'Recent menu'),
  recentMenuLoading: boolean('recentMenuLoading', false, 'Recent menu'),
  // openRecentMenu: true, // Cannot click on the DD to toggle if this is set
  onAllItemsClick: (e) => logAction(e, 'onAllItemsClick'),
  onRecentItemClick: (e) => logAction(e, 'onRecentItemClick'),
  onCreateNewClick: (e) => logAction(e, 'onCreateNewClick'),
  onViewDeletedClick: (e) => logAction(e, 'onViewDeletedClick'),
  onRecentMenuToggle: (e) => logAction(e, 'onRecentMenuToggle'),

  // Help menu
  shouldShowKnowledgeBaseLink: boolean(
    'shouldShowKnowledgeBaseLink',
    true,
    'Help menu'
  ),
  knowledgeBaseHref: text('knowledgeBaseHref', '#help', 'Help menu'),
  knowledgeBaseLinkText: text(
    'knowledgeBaseLinkText',
    'Knowledge base',
    'Help menu'
  ),
  shouldShowNewsLink: boolean('shouldShowNewsLink', true, 'Help menu'),
  onKnowledgeBaseClick: (e) => logAction(e, 'onKnowledgeBaseClick'),
  onNewsClick: (e) => logAction(e, 'onNewsClick'),
  knowledgeBaseLink: {
    text: text('knowledgeBaseLink.text', 'Knowledge base', 'Help menu'),
    href: text('knowledgeBaseLink.href', '#help', 'Help menu'),
  },
  restApiLink: {
    text: text('restApiLink.text', 'Developer Portal', 'Help menu'),
    href: text('restApiLink.href', '#api', 'Help menu'),
  },
});

storiesOf('Components/app-header', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    return (
      <AppHeader {...getKnobs()}>
        <div>My survey name</div>
      </AppHeader>
    );
  })
  .add('Default w/ icon', () => {
    return (
      <AppHeader {...getKnobs()} currentContainerIconPath={basicHub}>
        <div>My survey name</div>
      </AppHeader>
    );
  })
  .add('Custom Items', () => {
    return (
      <AppHeader {...getKnobs()} customItems={customItems}>
        <div>My survey name</div>
      </AppHeader>
    );
  })
  .add('Custom Items with no Create and no Recent', () => {
    return (
      <AppHeader
        {...getKnobs()}
        customItems={customItems}
        showCreateNew={false}
        showRecentItems={false}>
        <div>My survey name</div>
      </AppHeader>
    );
  })
  .add('Federated App Header', () => {
    return (
      <FederatedAppHeader
        {...getKnobs()}
        onCatch={(error) => {
          console.error('Could not load federated header', error);
        }}>
        <div>Federated App Header</div>
      </FederatedAppHeader>
    );
  })
  .add('With Search and Digital Feedback button', () => {
    return (
      <AppHeader
        {...getKnobs()}
        showSearch={true}
        searchChildren={
          <div style={{width: '200px'}}>
            <Fieldset layoutStyle={LayoutStyle.Vertical}>
              <TextField label={'Search field 1'} onChange={noop} />
              <TextField label={'Search field 2'} onChange={noop} />
              <TextField label={'Search field 3'} onChange={noop} />
              <TextField label={'Search field 4'} onChange={noop} />
              <TextField label={'Search field 5'} onChange={noop} />
            </Fieldset>
          </div>
        }
        searchTooltip={<span>Search!</span>}
        showDigitalFeedbackButton={true}
        digitalFeedbackSettings={{
          programUrl:
            'https://digitalfeedback.euro.confirmit.com/api/digitalfeedback/loader?programKey=bgyRbM',
        }}
      />
    );
  })
  .add('With Notifications', () => {
    const messageGroups = [
      {
        title: 'Group 1',
        messages: [
          {
            id: 1,
            text: 'Message 1',
            isRead: false,
          },
          {
            id: 2,
            text: 'Message 2',
            isRead: false,
          },
          {
            id: 3,
            text: 'Message 3',
            isRead: false,
          },
        ],
      },
      {
        title: 'Group 2',
        messages: [
          {
            id: 1,
            text: 'Message 1',
            isRead: false,
          },
          {
            id: 2,
            text: 'Message 2',
            isRead: false,
          },
          {
            id: 3,
            text: 'Message 3',
            isRead: false,
          },
        ],
      },
      {
        title: 'Group 3',
        messages: [
          {
            id: 1,
            text: 'Message 1',
            isRead: false,
          },
          {
            id: 2,
            text: 'Message 2',
            isRead: false,
          },
          {
            id: 3,
            text: 'Message 3',
            isRead: false,
          },
        ],
      },
    ];

    return (
      <AppHeader
        {...getKnobs()}
        showNotifications={true}
        unreadNotifications={0}
        notificationsChildren={
          <div style={{width: '600px', height: '500px'}}>
            <Inbox messageGroups={messageGroups} onSelect={() => {}} />
          </div>
        }
      />
    );
  });
