const applications = [
  {
    id: 'accountoverview',
    name: 'Account Overview',
    links: {
      self: '#http://localhost/accountoverview/',
    },
  },
  {
    id: 'actionmanagement',
    name: 'Action Management',
    links: {
      self: '#http://localhost/actionmanagement/',
    },
  },
  {
    id: 'actions',
    name: 'Actions',
    links: {
      self: '#http://localhost/actions/',
    },
  },
  {
    id: 'activedashboards',
    name: 'Active Dashboards',
    links: {
      self: '#http://localhost/activedashboards/',
    },
  },
  {
    id: 'cati',
    name: 'Cati',
    links: {
      self: '#http://localhost/cati/',
    },
  },
  {
    id: 'crmconnectorforsalesforce',
    name: 'CRM Connector for Salesforce',
    links: {
      self: '#http://localhost/crmconnectorforsalesforce/',
    },
  },
  {
    id: 'discoveryanalytics',
    name: 'Discovery Analytics',
    links: {
      self: '#http://localhost/discoveryanalytics/',
    },
  },
  {
    id: 'endusermanagement',
    name: 'End User Management',
    isExternal: true,
    links: {
      self: '#http://localhost/confirm/authoring/Confirmit.aspx?loc=endusers',
    },
  },
  {
    id: 'geniussocial',
    name: 'Genius social',
    links: {
      self: '#http://localhost/genius/',
    },
  },
  {
    id: 'hierarchymanagement',
    name: 'Hierarchy Management',
    links: {
      self: '#http://localhost/hierarchymanagement/',
    },
  },
  {
    id: 'home',
    name: 'Home',
    isExternal: true,
    links: {
      self: '#http://localhost/confirm/home/',
    },
  },
  {
    id: 'instantanalytics',
    name: 'Instant Analytics',
    links: {
      self: '#http://localhost/instantanalytics/',
    },
  },
  {
    id: 'modelbuilder',
    name: 'Model Builder',
    links: {
      self: '#http://localhost/modelbuilder/',
    },
  },
  {
    id: 'professionalauthoring',
    name: 'Professional Authoring',
    isExternal: true,
    links: {
      self: '#http://localhost/confirm/authoring/',
    },
  },
  {
    id: 'reportal',
    name: 'Reportal',
    isExternal: true,
    links: {
      self: '#http://localhost/confirm/reportal/',
    },
  },
  {
    id: 'smarthub',
    name: 'SmartHub',
    current: true,
    links: {
      self: '#http://localhost/hub/',
    },
  },
  {
    id: 'studio',
    name: 'Studio',
    links: {
      self: '#http://localhost/studio/',
    },
  },
  {
    id: 'surveydesigner',
    name: 'Survey Designer',
    links: {
      self: '#http://localhost/surveydesigner/',
    },
  },
  {
    id: 'customquestions',
    name: 'Custom Questions',
    links: {
      self: '#http://localhost/customquestions/',
    },
  },
  {
    id: 'digitalfeedback',
    name: 'Digital Feedback',
    links: {
      self: '#http://localhost/digitalfeedback/',
    },
  },
  {
    id: 'unknownid',
    name: 'Unknown app id',
    links: {
      self: '#http://localhost/',
    },
  },
];

export default applications;
