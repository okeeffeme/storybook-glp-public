import React, {forwardRef, MouseEventHandler, ReactNode, Ref} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';

import classNames from './AppHeaderButton.module.css';

const {block, modifier} = bemFactory('co-app-header-button', classNames);

type AppHeaderButtonProps = {
  className?: string;
  onClick?: MouseEventHandler;
  children: ReactNode;
  isActive?: boolean;
};

const AppHeaderButton = forwardRef(function AppHeaderButton(
  {
    className,
    onClick,
    isActive = false,
    children,
    ...rest
  }: AppHeaderButtonProps,
  ref: Ref<HTMLButtonElement>
) {
  return (
    <button
      ref={ref}
      onClick={onClick}
      className={cn(block(), className, {
        [modifier('active')]: isActive,
      })}
      {...rest}>
      {children}
    </button>
  );
});

export default AppHeaderButton;
