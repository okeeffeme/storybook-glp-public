import React, {ReactNode} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './MenuItem.module.css';

const {block} = bemFactory('co-app-header-menu-item', classNames);

type MenuItemProps = {
  children: ReactNode;
};

export const MenuItem = ({children, ...rest}: MenuItemProps) => {
  return (
    <div className={block()} {...extractDataAriaIdProps(rest)}>
      {children}
    </div>
  );
};
