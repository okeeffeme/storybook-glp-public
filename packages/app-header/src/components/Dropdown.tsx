import React, {MouseEventHandler, ReactElement, ReactNode} from 'react';
import cn from 'classnames';

import SimpleLink from '@jotunheim/react-simple-link';
import {Placement, Popover, TriggerElement} from '@jotunheim/react-popover';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import {bemFactory} from '@jotunheim/react-themes';

import {MenuItem} from './MenuItem';
import classNames from './Dropdown.module.css';

const {block, element, modifier} = bemFactory(
  'co-app-header-dd-menu',
  classNames
);

// Custom Dropdown implementations for App Header, since they dont look anything like the shared Dropdown component
// The DOM structure should match Dropdown closely, to avoid breaking tests that are based on the old implementation

type DropdownProps = {
  menu: ReactElement;
  open: boolean;
  onToggle: (open: boolean) => void;
  placement?: Placement;
  children: TriggerElement;
};

export const Dropdown = ({
  menu,
  open,
  onToggle,
  placement = 'bottom-end',
  children,
  ...rest
}: DropdownProps) => {
  return (
    <Popover
      hasPadding={false}
      open={open}
      noArrow={true}
      content={menu}
      onToggle={onToggle}
      placement={placement}
      {...extractDataAriaIdProps(rest)}>
      {children}
    </Popover>
  );
};

type DropdownMenuProps = {
  hasMinMaxSize?: boolean;
  useGridLayout?: boolean;
  children?: ReactNode;
};

export const DropdownMenu = ({
  hasMinMaxSize = false,
  useGridLayout = false,
  children,
  ...rest
}: DropdownMenuProps) => {
  return (
    <ul
      className={cn(block(), {
        [modifier('min-max-size')]: hasMinMaxSize,
        [modifier('use-grid-layout')]: useGridLayout,
      })}
      {...extractDataAriaIdProps(rest)}>
      {children}
    </ul>
  );
};

type DropdownMenuLinkItemProps = {
  href?: string;
  onClick?: MouseEventHandler;
  children: ReactNode;
  target?: string;
};

export const DropdownMenuLinkItem = ({
  href,
  onClick,
  children,
  target,
  ...rest
}: DropdownMenuLinkItemProps) => {
  return (
    <li className={element('menu-item')} {...extractDataAriaIdProps(rest)}>
      <SimpleLink
        data-testid="simple-link"
        className={element('menu-item-link')}
        data-menu-link-item-anchor=""
        target={target}
        href={href}
        onClick={onClick}>
        <MenuItem>{children}</MenuItem>
      </SimpleLink>
    </li>
  );
};

type DropdownMenuTextItemProps = {
  children: ReactNode;
};

export const DropdownMenuTextItem = ({
  children,
  ...rest
}: DropdownMenuTextItemProps) => {
  return (
    <li className={element('menu-item-text')} {...extractDataAriaIdProps(rest)}>
      <MenuItem>{children}</MenuItem>
    </li>
  );
};

type DropdownMenuHeaderProps = {
  children: ReactNode;
};

export const DropdownMenuHeader = ({
  children,
  ...rest
}: DropdownMenuHeaderProps) => {
  return (
    <li className={element('header')} {...extractDataAriaIdProps(rest)}>
      <MenuItem>{children}</MenuItem>
    </li>
  );
};

export const Divider = () => {
  return <li className={element('divider')} />;
};
