import React, {useState} from 'react';
import Icon, {accountCircle, openInNew} from '@jotunheim/react-icons';

import {NavbarIconFill} from './constants';
import AppHeaderButton from './components/AppHeaderButton';
import {
  Dropdown,
  DropdownMenu,
  DropdownMenuLinkItem,
  DropdownMenuHeader,
} from './components/Dropdown';

import type {UserMenuProps} from './types';

const preventDefault = (e) => e.preventDefault();

const UserMenu = ({
  logoutText = 'Logout',
  onLogoutClick = () => {},
  logoutHref = '',
  userSettingsText = 'User settings',
  onUserSettingsClick = () => {},
  userSettingsHref = '',
  user,
}: UserMenuProps) => {
  const [open, setOpen] = useState(false);

  const menu = (
    <DropdownMenu data-test-app-header="user-menu" data-testid="user-menu">
      {user && (
        <DropdownMenuHeader data-test-app-header="user-menu-current-user">{`${user.firstName} ${user.lastName}`}</DropdownMenuHeader>
      )}
      {userSettingsHref && (
        <DropdownMenuLinkItem
          data-testid="user-settings-link"
          data-test-user-settings-link
          target="_authoring"
          onClick={(e) => {
            setOpen(false);
            onUserSettingsClick(e);
          }}
          href={userSettingsHref}>
          {userSettingsText}
          <Icon path={openInNew} size={14} fill={NavbarIconFill} />
        </DropdownMenuLinkItem>
      )}
      <DropdownMenuLinkItem
        data-test-logout-link
        href={logoutHref}
        onClick={(e) => {
          setOpen(false);
          onLogoutClick(e);
        }}>
        {logoutText}
      </DropdownMenuLinkItem>
    </DropdownMenu>
  );

  return (
    <Dropdown placement="bottom-end" open={open} onToggle={setOpen} menu={menu}>
      <AppHeaderButton
        data-toggle-user-menu
        isActive={open}
        onClick={preventDefault}>
        <Icon path={accountCircle} fill={NavbarIconFill} />
      </AppHeaderButton>
    </Dropdown>
  );
};

export default UserMenu;
