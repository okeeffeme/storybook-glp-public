import React, {useState} from 'react';

import Icon, {menuDown, plus} from '@jotunheim/react-icons';
import {Truncate} from '@jotunheim/react-truncate';
import {BusyDots} from '@jotunheim/react-busy-dots';
import SimpleLink from '@jotunheim/react-simple-link';
import {bemFactory} from '@jotunheim/react-themes';

import {NavbarIconFill} from './constants';
import AppHeaderButton from './components/AppHeaderButton';
import {
  Dropdown,
  DropdownMenu,
  DropdownMenuLinkItem,
  DropdownMenuTextItem,
  DropdownMenuHeader,
  Divider,
} from './components/Dropdown';

import classNames from './css/RecentMenu.module.css';

import type {
  RecentMenuProps,
  TitleProps,
  AllItemsProps,
  LoadingIndicatorProps,
  RecentItemsHeaderProps,
  RecentItemsProps,
  CreateNewProps,
  TopDividerProps,
  BottomDividerProps,
  ViewDeletedProps,
  CustomItemsProps,
  CustomItem,
} from './types';

type HandleToggleProps = {
  handleToggle: (boolean) => void;
};

const {block, element} = bemFactory('co-recent-list', classNames);

const Title = ({allItemsHref, title, onAllItemsClick}: TitleProps) => {
  return (
    <SimpleLink
      href={allItemsHref}
      className={element('main-link')}
      onClick={onAllItemsClick}
      data-test-app-header="recent-menu-main-link">
      {title}
    </SimpleLink>
  );
};

const AllItems = ({
  showAllItems,
  allItemsHref,
  allItemsText,
  onAllItemsClick,
  handleToggle,
}: AllItemsProps & HandleToggleProps) => {
  if (!showAllItems) {
    return null;
  }

  return (
    <DropdownMenuLinkItem
      data-testid="all-items"
      data-item-type="all-items"
      href={allItemsHref}
      onClick={(e) => {
        handleToggle(false);
        onAllItemsClick?.(e);
      }}>
      <span>{allItemsText}</span>
    </DropdownMenuLinkItem>
  );
};

const LoadingIndicator = ({
  showRecentItems,
  recentMenuLoading,
}: LoadingIndicatorProps) => {
  if (!recentMenuLoading || !showRecentItems) {
    return null;
  }

  return (
    <DropdownMenuTextItem data-item-type="recent-item-busy-dots">
      <BusyDots />
    </DropdownMenuTextItem>
  );
};

const RecentItemsHeader = ({
  showRecentItems,
  recentItemsText,
}: RecentItemsHeaderProps) => {
  if (!showRecentItems) {
    return null;
  }

  return (
    <DropdownMenuHeader
      data-testid="recent-item-header"
      data-item-type="recent-item-header">
      {recentItemsText}
    </DropdownMenuHeader>
  );
};

const RecentItems = ({
  items,
  showRecentItems,
  recentMenuLoading,
  onRecentItemClick,
  handleToggle,
}: RecentItemsProps & HandleToggleProps) => {
  if (recentMenuLoading || !showRecentItems || !items || !items.length) {
    return null;
  }

  return (
    <>
      {items.map((item) => {
        const {href, name, id} = item;
        const idTemplate = `(${id})`;
        const handleClick = (e) => {
          onRecentItemClick?.(e, {href, name, id});
        };

        return (
          <DropdownMenuLinkItem
            data-testid="recent-item"
            data-item-type="recent-item"
            key={id}
            href={href}
            onClick={(e) => {
              handleToggle(false);
              handleClick(e);
            }}>
            <Truncate>{name}</Truncate>
            <span>{idTemplate}</span>
          </DropdownMenuLinkItem>
        );
      })}
    </>
  );
};

const CreateNew = ({
  showCreateNew,
  createNewHref,
  createNewText,
  onCreateNewClick,
  handleToggle,
}: CreateNewProps & HandleToggleProps) => {
  if (!showCreateNew) return null;

  return (
    <DropdownMenuLinkItem
      data-testid="create-new"
      data-item-type="create-new"
      onClick={(e) => {
        handleToggle(false);
        onCreateNewClick?.(e);
      }}
      href={createNewHref}>
      <Icon path={plus} fill={NavbarIconFill} size={16} />
      {createNewText}
    </DropdownMenuLinkItem>
  );
};

const CustomItems = ({
  customItems = [],
  handleToggle,
}: CustomItemsProps & HandleToggleProps) => {
  if (customItems.length === 0) return null;

  return (
    <>
      {customItems.map((item, index) => (
        <CustomItemMenu
          key={`${item.text}-${index}`}
          item={item}
          handleToggle={handleToggle}
        />
      ))}
    </>
  );
};

const CustomItemMenu = ({
  item,
  handleToggle,
}: {item: CustomItem} & HandleToggleProps) => {
  return (
    <DropdownMenuLinkItem
      data-testid={`custom-menu-item`}
      data-item-type={`custom-menu-item`}
      onClick={() => {
        handleToggle(false);
        item.onClick?.(item.id);
      }}
      href={item.href}
      target={item.target}>
      {item.text}
    </DropdownMenuLinkItem>
  );
};

const TopDivider = ({
  showAllItems,
  showCreateNew,
  showViewDeleted,
}: TopDividerProps) => {
  if ((showAllItems || showViewDeleted) && showCreateNew) {
    return <Divider />;
  }

  return null;
};

const BottomDivider = ({
  showAllItems,
  showRecentItems,
  showCreateNew,
  showViewDeleted,
}: BottomDividerProps) => {
  if ((showAllItems || showCreateNew || showViewDeleted) && showRecentItems) {
    return <Divider />;
  }

  return null;
};

const ViewDeleted = ({
  showViewDeleted,
  viewDeletedText,
  viewDeletedHref,
  onViewDeletedClick,
  handleToggle,
}: ViewDeletedProps & HandleToggleProps) => {
  if (!showViewDeleted) {
    return null;
  }

  return (
    <DropdownMenuLinkItem
      data-testid="deleted-items"
      data-item-type="deleted-items"
      href={viewDeletedHref}
      onClick={(e) => {
        handleToggle(false);
        onViewDeletedClick?.(e);
      }}>
      {viewDeletedText}
    </DropdownMenuLinkItem>
  );
};

const preventDefault = (e) => e.preventDefault();

const RecentMenu = ({
  openRecentMenu = false,
  onRecentMenuToggle,
  showRecentMenuDropDown = true,
  showCreateNew = true,
  showViewDeleted = true,
  showRecentItems = true,
  recentMenuLoading = false,
  showAllItems = true,
  title,
  allItemsHref,
  allItemsText,
  onAllItemsClick,
  items = [],
  viewDeletedText,
  viewDeletedHref,
  onViewDeletedClick,
  createNewHref,
  createNewText,
  onCreateNewClick,
  recentItemsText,
  onRecentItemClick,
  customItems = [],
}: RecentMenuProps) => {
  const [open, setOpen] = useState(openRecentMenu);

  const handleToggle = (e) => {
    setOpen(e);
    onRecentMenuToggle?.(e);
  };

  const menu = (
    <DropdownMenu hasMinMaxSize={true} data-test-app-header="recent-menu">
      <AllItems
        showAllItems={showAllItems}
        allItemsHref={allItemsHref}
        allItemsText={allItemsText}
        onAllItemsClick={onAllItemsClick}
        handleToggle={handleToggle}
      />
      <ViewDeleted
        showViewDeleted={showViewDeleted}
        viewDeletedText={viewDeletedText}
        viewDeletedHref={viewDeletedHref}
        onViewDeletedClick={onViewDeletedClick}
        handleToggle={handleToggle}
      />
      <TopDivider
        showAllItems={showAllItems}
        showCreateNew={showCreateNew}
        showViewDeleted={showViewDeleted}
      />
      <CreateNew
        showCreateNew={showCreateNew}
        createNewHref={createNewHref}
        createNewText={createNewText}
        onCreateNewClick={onCreateNewClick}
        handleToggle={handleToggle}
      />
      {customItems.length > 0 && (
        <>
          <Divider />
          <CustomItems customItems={customItems} handleToggle={handleToggle} />
        </>
      )}
      {items.length > 0 && (
        <>
          <BottomDivider
            showAllItems={showAllItems}
            showRecentItems={showRecentItems}
            showCreateNew={showCreateNew}
            showViewDeleted={showViewDeleted}
          />
          <RecentItemsHeader
            showRecentItems={showRecentItems}
            recentItemsText={recentItemsText}
          />
          <LoadingIndicator
            showRecentItems={showRecentItems}
            recentMenuLoading={recentMenuLoading}
          />
          <RecentItems
            items={items}
            showRecentItems={showRecentItems}
            recentMenuLoading={recentMenuLoading}
            onRecentItemClick={onRecentItemClick}
            handleToggle={handleToggle}
          />
        </>
      )}
    </DropdownMenu>
  );

  return (
    <div className={block()}>
      <Title
        allItemsHref={allItemsHref}
        title={title}
        onAllItemsClick={onAllItemsClick}
      />
      {showRecentMenuDropDown && (
        <div className={element('dd')}>
          <Dropdown
            placement={'bottom-start'}
            menu={menu}
            open={open}
            onToggle={handleToggle}>
            <AppHeaderButton
              onClick={preventDefault}
              isActive={openRecentMenu}
              data-test-app-header="recent-menu-dropdown-toggle">
              <Icon path={menuDown} fill={NavbarIconFill} />
            </AppHeaderButton>
          </Dropdown>
        </div>
      )}
    </div>
  );
};

export default RecentMenu;
