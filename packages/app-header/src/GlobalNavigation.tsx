import React, {useState} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import Icon, {apps, openInNew} from '@jotunheim/react-icons';
import {ApplicationIcon} from '@jotunheim/react-graphics';

import SimpleLink from '@jotunheim/react-simple-link';

import {NavbarIconFill} from './constants';
import AppHeaderButton from './components/AppHeaderButton';
import {Dropdown, DropdownMenu} from './components/Dropdown';
import type {GlobalNavigationProps} from './types';

import classNames from './css/GlobalNavigation.module.css';

const {block, element, modifier} = bemFactory('app-header-app', classNames);

const preventDefault = (e) => e.preventDefault();

const GlobalNavigation = ({applications = []}: GlobalNavigationProps) => {
  const [open, setOpen] = useState(false);

  const menu = (
    <DropdownMenu
      useGridLayout={true}
      data-test-app-header="global-navigation-menu">
      {applications.map((app) => {
        const appCn = cn(block(), {
          [modifier('current')]: app.current,
        });

        return (
          <li key={app.id}>
            <div
              className={appCn}
              data-testid="global-navigation-menu-app"
              data-test-app-header="global-navigation-menu-app"
              data-test-app-header-current-app={`${app.current}`}>
              <SimpleLink
                data-testid={`${app.name}-app`}
                className={element('link')}
                href={app.links.self}
                onClick={() => setOpen(false)}
                target={app.target}>
                <div className={element('app-icon')}>
                  <ApplicationIcon
                    width={60}
                    height={60}
                    applicationId={ApplicationIcon.ApplicationId[app.id]}
                  />
                </div>
                <div className={element('text-outer')}>
                  <div className={element('text')}>
                    <div className={element('text-inner')}>{app.name}</div>
                    {app.isExternal && (
                      <>
                        <span className={element('nbsp')}>&nbsp;</span>
                        <div
                          data-testid="external-global-navigation-app"
                          className={element('external-link')}>
                          <Icon path={openInNew} fill="#5f6366" size={14} />
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </SimpleLink>
            </div>
          </li>
        );
      })}
    </DropdownMenu>
  );

  return (
    <Dropdown
      open={open}
      menu={menu}
      onToggle={setOpen}
      placement={'bottom-start'}>
      <AppHeaderButton
        isActive={open}
        onClick={preventDefault}
        data-test-app-header="global-navigation-dropdown-toggle">
        <Icon path={apps} fill={NavbarIconFill} />
      </AppHeaderButton>
    </Dropdown>
  );
};

export default GlobalNavigation;
