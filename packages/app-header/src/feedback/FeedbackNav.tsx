import React, {ReactNode} from 'react';
import {Tooltip} from '@jotunheim/react-tooltip';
import Icon, {messageInAppFeedback} from '@jotunheim/react-icons';

import {NavbarIconFill} from '../constants';
import AppHeaderButton from '../components/AppHeaderButton';

type FeedbackNavProps = {
  tooltipContent?: ReactNode;
  onClick?: () => void;
};

const FeedbackNav = ({tooltipContent, onClick}: FeedbackNavProps) => {
  return (
    <Tooltip content={tooltipContent}>
      <AppHeaderButton
        data-app-header-feedback-button
        data-testid="feedback-button"
        onClick={onClick}>
        <Icon path={messageInAppFeedback} fill={NavbarIconFill} />
      </AppHeaderButton>
    </Tooltip>
  );
};

export default FeedbackNav;
