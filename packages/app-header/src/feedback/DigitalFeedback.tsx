import React, {useEffect} from 'react';
import FeedbackNav from './FeedbackNav';
import type {DigitalFeedbackProps} from '../types';
import AppFeedback from './AppFeedback';
import useScript from './useScript';

const triggerLegacy = () => {
  const fn = window['cfRunDigitalFeedback'];
  if (fn) {
    fn(AppFeedback.getContext());
    return true;
  }
  return false;
};

const DigitalFeedback = ({
  user = {},
  title: applicationName,
  feedbackTooltipContent = 'Give us a feedback',
  digitalFeedbackSettings: {programUrl} = {},
}: DigitalFeedbackProps) => {
  useEffect(() => {
    AppFeedback.setContext(user, applicationName);
  }, [applicationName, user]);

  const loaded = useScript(programUrl);

  const handleClick = () => {
    // remove after when all apps are migrated
    if (triggerLegacy()) return;
    AppFeedback.emit('app-feedback:show');
  };

  if (!loaded) return null;

  return (
    <FeedbackNav
      tooltipContent={feedbackTooltipContent}
      onClick={handleClick}
    />
  );
};

export default DigitalFeedback;
