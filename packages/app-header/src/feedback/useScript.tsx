import {useEffect, useState} from 'react';

function loadScript(url) {
  return new Promise<void>((resolve, reject) => {
    try {
      const script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = url;
      script.async = true;
      script.addEventListener('load', () => resolve());
      script.addEventListener('error', () =>
        reject(new Error(`Failed to load script ${url}`))
      );
      document.body.appendChild(script);
    } catch (e) {
      reject(e);
    }
  });
}

const scripts = {};
async function loadScriptOnce(url) {
  const promise = (scripts[url] = loadScript(url));
  try {
    await promise;
    return true;
  } catch {
    delete scripts[url];
    return false;
  }
}

function useScript(url) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (!url) return;

    let cancel = false;
    (async function () {
      const isLoaded = await loadScriptOnce(url);
      if (cancel) {
        return;
      }
      setLoaded(isLoaded);
    })();
    return () => {
      cancel = true;
    };
  }, [url]);

  return loaded;
}

export default useScript;
