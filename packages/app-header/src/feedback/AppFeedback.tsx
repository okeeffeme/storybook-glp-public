import {EventEmitter} from 'events';
import {User} from '../types';

const extractProps = ({
  userId,
  userName,
  firstName,
  lastName,
  email,
  languageId,
  companyId,
  companyName,
}: User) => ({
  userId,
  userName,
  firstName,
  lastName,
  email,
  languageId,
  companyId,
  companyName,
});

class AppFeedback extends EventEmitter {
  private _user?: User;
  private _applicationName?: string;

  constructor() {
    super();
  }

  emit(event, ...args) {
    if (process.env.NODE_ENV !== 'production')
      console.log(`AppFeedback event '${event}' emitted with args: `, args);
    return super.emit(event, ...args);
  }

  setContext(user: User, applicationName: string | undefined) {
    this._user = extractProps(user);
    this._applicationName = applicationName;
  }

  getContext() {
    return {
      ...this._user,
      applicationName: this._applicationName,
      url: window.location.href,
    };
  }
}

export default window['AppFeedback'] = new AppFeedback();
