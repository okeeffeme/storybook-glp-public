import React from 'react';
import Badge from '@jotunheim/react-badge';
import {Tooltip} from '@jotunheim/react-tooltip';
import Icon, {bellCircle} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';
import {bemFactory} from '@jotunheim/react-themes';

import {NavbarIconFill} from './constants';
import AppHeaderButton from './components/AppHeaderButton';

import type {NotificationsNavProps} from './types';

import classNames from './css/AppHeader.css';

const {element} = bemFactory('co-app-header', classNames);

const NotificationsNav = ({
  unreadNotifications = 0,
  notificationsTooltip,
  notificationsChildren,
}: NotificationsNavProps) => {
  const [isVisible, setIsVisible] = React.useState(false);

  return (
    <Popover
      open={isVisible}
      noArrow={true}
      hasPadding={false}
      content={notificationsChildren}
      onToggle={setIsVisible}
      placement={'bottom-end'}
      closeOnTriggerClick={true}>
      <Tooltip content={notificationsTooltip}>
        <AppHeaderButton data-testid="app-header-notifications-toggle">
          <Badge
            value={unreadNotifications}
            data-testid="app-header-notifications-badge">
            <div className={element('notifications-badge')}>
              <Icon path={bellCircle} fill={NavbarIconFill} />
            </div>
          </Badge>
        </AppHeaderButton>
      </Tooltip>
    </Popover>
  );
};

export default NotificationsNav;
