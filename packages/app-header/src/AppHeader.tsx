import React from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {theme, ThemeContext, bemFactory} from '@jotunheim/react-themes';
import SimpleLink from '@jotunheim/react-simple-link';
import {ForstaLogo} from '@jotunheim/react-graphics';
import {Icon} from '@jotunheim/react-icons';

import GlobalNavigation from './GlobalNavigation';
import UserMenu from './UserMenu';
import RecentMenu from './RecentMenu';
import Help from './Help';
import DigitalFeedback from './feedback/DigitalFeedback';
import SearchNav from './SearchNav';
import NotificationsNav from './NotificationsNav';

import type {AppHeaderProps} from './types';

import {
  extractSearchProps,
  extractUserMenuProps,
  extractGlobalNavigationProps,
  extractRecentMenuProps,
  extractHelpProps,
  extractDigitalFeedbackProps,
  extractNotificationProps,
} from './utils/extract-props';

import classNames from './css/AppHeader.css';

const {block, element} = bemFactory('co-app-header', classNames);

const getHomeLink = (applications) => {
  const homeIndex = applications.map((item) => item.id).indexOf('home');

  return homeIndex > -1 ? applications[homeIndex].links.self : null;
};

const getUserSettingsLink = (applications, homeLink, oidcEnabled) => {
  if (homeLink && oidcEnabled) {
    return `${homeLink}userSettings`;
  }

  const professionalIndex = applications
    .map((item) => item.id)
    .indexOf('professionalauthoring');
  if (professionalIndex > -1) {
    return `${applications[professionalIndex].links.self}Confirmit.aspx?loc=usersettings`;
  }

  return null;
};

const Logo = () => {
  return (
    <ForstaLogo
      appearance={ForstaLogo.Appearance.WhitePlus}
      width="100"
      height="20"
    />
  );
};

const AppHeader = (props: AppHeaderProps) => {
  const {
    showUserMenu = true,
    showRecentMenu = true,
    showGlobalNavigation = true,
    children,
    applications = [],
    oidcEnabled,
    showSearch = false,
    showNotifications = false,
    showDigitalFeedbackButton = false,
    currentContainerIconPath = null,
    ...rest
  } = props;

  const homeLink = getHomeLink(applications);
  const userSettingsLink = getUserSettingsLink(
    applications,
    homeLink,
    oidcEnabled
  );

  return (
    <ThemeContext.Provider value={theme.themeNames.material}>
      <header
        data-testid="app-header"
        className={block()}
        role="banner"
        {...extractDataAriaIdProps(rest)}>
        <section className={cn(element('section'), element('section', 'main'))}>
          {homeLink ? (
            <div className={element('logo')}>
              <SimpleLink href={homeLink}>
                <Logo />
              </SimpleLink>
            </div>
          ) : (
            <div className={element('logo')}>
              <Logo />
            </div>
          )}

          {showGlobalNavigation && (
            <nav
              className={cn(
                element('item'),
                element('item', 'global-navigation')
              )}
              data-test-app-header="global-navigation"
              data-testid="global-navigation">
              <GlobalNavigation {...extractGlobalNavigationProps(props)} />
            </nav>
          )}

          {showRecentMenu && (
            <nav
              className={element('item')}
              data-testid="recent"
              data-test-app-header="recent">
              <RecentMenu {...extractRecentMenuProps(props)} />
            </nav>
          )}
          <div
            className={element('current-container')}
            data-current-container-icon="">
            {currentContainerIconPath ? (
              <div
                data-testid="current-container-icon"
                className={element('current-container-icon')}>
                <Icon
                  path={currentContainerIconPath}
                  fill="currentColor"
                  size={'14'}
                />
              </div>
            ) : null}
            <div data-current-container-text="">{children}</div>
          </div>
        </section>

        <section
          data-test-app-header="user-help-section"
          data-testid="user-help-section"
          className={cn(element('section'), element('section', 'user-help'))}>
          {showDigitalFeedbackButton && (
            <nav
              className={cn(element('item'), element('item', 'no-left-border'))}
              data-test-app-header="feedback">
              <DigitalFeedback {...extractDigitalFeedbackProps(props)} />
            </nav>
          )}

          {showSearch && (
            <nav
              className={cn(element('item'), element('item', 'no-left-border'))}
              data-testid="search"
              data-test-app-header="search">
              <SearchNav {...extractSearchProps(props)} />
            </nav>
          )}

          {showNotifications && (
            <nav
              className={cn(element('item'), element('item', 'no-left-border'))}
              data-testid="app-header-notifications-nav">
              <NotificationsNav {...extractNotificationProps(props)} />
            </nav>
          )}

          {showUserMenu && (
            <nav
              className={element('item')}
              data-test-app-header="user"
              data-testid="user">
              <UserMenu
                {...extractUserMenuProps(props)}
                userSettingsHref={userSettingsLink}
              />
            </nav>
          )}
          <nav className={element('item')} data-test-app-header="help">
            <Help {...extractHelpProps(props)} />
          </nav>
        </section>
      </header>
    </ThemeContext.Provider>
  );
};

export default AppHeader;
