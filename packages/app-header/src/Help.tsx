import React from 'react';

import Icon, {helpCircle, openInNew} from '@jotunheim/react-icons';

import {NavbarIconFill} from './constants';
import About from './About';
import AppHeaderButton from './components/AppHeaderButton';
import {
  Dropdown,
  DropdownMenu,
  DropdownMenuLinkItem,
} from './components/Dropdown';

import type {HelpProps} from './types';

const preventDefault = (e) => e.preventDefault();

const Help = ({
  shouldShowNewsLink = false,
  shouldShowKnowledgeBaseLink = false,
  newsLinkText = 'What’s New',
  knowledgeBaseLinkText = 'Knowledge base',
  knowledgeBaseLink,
  restApiLink,
  aboutLinkText = 'About',
  aboutDialogHeader = 'About Forsta Plus',
  aboutDialogClose = 'Close',
  onKnowledgeBaseClick = () => {},
  onNewsClick = () => {},
  shouldShowTourGuideReset = false,
  onTourGuideReset = () => {},
  tourGuideResetText = 'Reset Tour Guide',
  knowledgeBaseHref,
}: HelpProps) => {
  const [showAbout, setShowAbout] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const {target: knowledgeBaseLinkTarget = '_confirmit-kb'} =
    knowledgeBaseLink ?? {};
  if (knowledgeBaseLink) {
    shouldShowKnowledgeBaseLink = true;
    knowledgeBaseLinkText = knowledgeBaseLink.text ?? knowledgeBaseLinkText;
    knowledgeBaseHref = knowledgeBaseLink.href ?? knowledgeBaseHref;
  }

  const {
    text: restApiLinkText = 'Developer Portal',
    href: restApiHref,
    target: restApiLinkTarget = '_confirmit-api',
  } = restApiLink ?? {};

  const menu = (
    <DropdownMenu data-test-app-header="help-menu">
      {shouldShowKnowledgeBaseLink && (
        <DropdownMenuLinkItem
          data-test-help-link
          target={knowledgeBaseLinkTarget}
          onClick={(e) => {
            setOpen(false);
            !knowledgeBaseLink && onKnowledgeBaseClick(e);
          }}
          href={knowledgeBaseHref}>
          {knowledgeBaseLinkText}
          <Icon path={openInNew} size={14} fill={NavbarIconFill} />
        </DropdownMenuLinkItem>
      )}

      {!!restApiLink && (
        <DropdownMenuLinkItem
          data-test-api-link
          target={restApiLinkTarget}
          onClick={() => {
            setOpen(false);
          }}
          href={restApiHref}>
          {restApiLinkText}
          <Icon path={openInNew} size={14} fill={NavbarIconFill} />
        </DropdownMenuLinkItem>
      )}

      {shouldShowNewsLink && (
        <DropdownMenuLinkItem
          data-test-news-link
          onClick={(e) => {
            setOpen(false);
            onNewsClick(e);
          }}
          href="#">
          {newsLinkText}
        </DropdownMenuLinkItem>
      )}

      <DropdownMenuLinkItem
        data-test-about-link
        onClick={(e) => {
          e.preventDefault();
          setOpen(false);
          setShowAbout(true);
        }}
        href="#">
        {aboutLinkText}
      </DropdownMenuLinkItem>
      {shouldShowTourGuideReset && (
        <DropdownMenuLinkItem
          data-test-tour-guide-reset-link
          onClick={(e) => {
            e.preventDefault();
            setOpen(false);
            onTourGuideReset();
          }}
          href="#">
          {tourGuideResetText}
        </DropdownMenuLinkItem>
      )}
    </DropdownMenu>
  );

  return (
    <>
      <Dropdown
        open={open}
        onToggle={setOpen}
        placement="bottom-end"
        menu={menu}>
        <AppHeaderButton
          data-app-header-help-dropdown
          isActive={open}
          onClick={preventDefault}>
          <Icon path={helpCircle} fill={NavbarIconFill} />
        </AppHeaderButton>
      </Dropdown>
      {showAbout && (
        <About
          aboutDialogHeader={aboutDialogHeader}
          aboutDialogClose={aboutDialogClose}
          onCloseButtonClick={() => setShowAbout(false)}
        />
      )}
    </>
  );
};

export default Help;
