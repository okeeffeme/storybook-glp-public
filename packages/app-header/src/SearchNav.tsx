import React from 'react';

import {Tooltip} from '@jotunheim/react-tooltip';
import Icon, {magnify} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';

import {NavbarIconFill} from './constants';
import AppHeaderButton from './components/AppHeaderButton';
import type {SearchNavProps} from './types';

const SearchNav = ({searchTooltip, searchChildren}: SearchNavProps) => {
  const [isVisible, setIsVisible] = React.useState(false);

  return (
    <Popover
      open={isVisible}
      noArrow={true}
      content={searchChildren}
      onToggle={setIsVisible}
      placement={'bottom-end'}
      closeOnTriggerClick={true}>
      <Tooltip content={searchTooltip}>
        <AppHeaderButton data-testid="app-header-search-button">
          <Icon path={magnify} fill={NavbarIconFill} />
        </AppHeaderButton>
      </Tooltip>
    </Popover>
  );
};

export default SearchNav;
