export const extractUserMenuProps = ({
  user,
  onUserSettingsClick,
  userSettingsText,
  onLogoutClick,
  logoutText,
  logoutHref,
  showUserMenu,
}) => ({
  user,
  onUserSettingsClick,
  userSettingsText,
  onLogoutClick,
  logoutText,
  logoutHref,
  showUserMenu,
});

export const extractNotificationProps = ({
  unreadNotifications,
  notificationsTooltip,
  notificationsChildren,
}) => ({
  unreadNotifications,
  notificationsTooltip,
  notificationsChildren,
});

export const extractRecentMenuProps = ({
  title,
  showAllItems,
  showRecentItems,
  showRecentMenuDropDown,
  showCreateNew,
  showViewDeleted,
  allItemsHref,
  allItemsText,
  onAllItemsClick,
  recentItemsText,
  onRecentItemClick,
  items,
  createNewHref,
  createNewText,
  onCreateNewClick,
  viewDeletedHref,
  viewDeletedText,
  onViewDeletedClick,
  openRecentMenu,
  onRecentMenuToggle,
  recentMenuLoading,
  customItems,
}) => ({
  title,
  showAllItems,
  showRecentItems,
  showRecentMenuDropDown,
  showCreateNew,
  showViewDeleted,
  allItemsHref,
  allItemsText,
  onAllItemsClick,
  recentItemsText,
  onRecentItemClick,
  items,
  createNewHref,
  createNewText,
  onCreateNewClick,
  viewDeletedHref,
  viewDeletedText,
  onViewDeletedClick,
  openRecentMenu,
  onRecentMenuToggle,
  recentMenuLoading,
  customItems,
});

export const extractGlobalNavigationProps = ({applications}) => ({
  applications,
});

export const extractHelpProps = ({
  onNewsClick,
  newsLinkText,
  aboutLinkText,
  onTourGuideReset,
  knowledgeBaseHref,
  tourGuideResetText,
  shouldShowNewsLink,
  onKnowledgeBaseClick,
  knowledgeBaseLinkText,
  shouldShowTourGuideReset,
  shouldShowKnowledgeBaseLink,
  knowledgeBaseLink,
  restApiLink,
}) => ({
  onNewsClick,
  newsLinkText,
  aboutLinkText,
  onTourGuideReset,
  knowledgeBaseHref,
  tourGuideResetText,
  shouldShowNewsLink,
  onKnowledgeBaseClick,
  knowledgeBaseLinkText,
  shouldShowTourGuideReset,
  shouldShowKnowledgeBaseLink,
  knowledgeBaseLink,
  restApiLink,
});

export const extractDigitalFeedbackProps = ({
  user,
  title,
  feedbackTooltipContent,
  digitalFeedbackSettings,
}) => ({
  user,
  title,
  feedbackTooltipContent,
  digitalFeedbackSettings,
});

export const extractSearchProps = ({searchTooltip, searchChildren}) => ({
  searchTooltip,
  searchChildren,
});
