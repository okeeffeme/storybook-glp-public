import React from 'react';

import Dialog from '@jotunheim/react-dialog';
import Button from '@jotunheim/react-button';

type AboutProps = {
  aboutDialogHeader?: string;
  aboutDialogClose?: string;
  onCloseButtonClick: () => void;
};

const About = ({
  aboutDialogHeader,
  aboutDialogClose,
  onCloseButtonClick,
}: AboutProps) => {
  return (
    <Dialog open={true} size="medium">
      <Dialog.Header
        title={aboutDialogHeader}
        onCloseButtonClick={onCloseButtonClick}
      />
      <Dialog.Body>
        Copyright &copy; {new Date().getFullYear()} Forsta AS. All rights
        reserved.
      </Dialog.Body>
      <Dialog.Footer>
        <Button
          data-app-header-about-close
          appearance={Button.appearances.secondaryNeutral}
          onClick={onCloseButtonClick}>
          {aboutDialogClose}
        </Button>
      </Dialog.Footer>
    </Dialog>
  );
};

export default About;
