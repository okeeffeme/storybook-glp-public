import type {Either} from '@jotunheim/react-utils';
import {MouseEventHandler, MouseEvent, ReactNode} from 'react';

export type User = {
  userId?: number;
  userName?: string;
  firstName?: string;
  lastName?: string;
  companyId?: number;
  companyName?: string;
  languageId?: number;
  email?: string;
};

export type UserMenuProps = {
  user?: User;
  onUserSettingsClick?: MouseEventHandler;
  userSettingsText?: string;
  userSettingsHref?: string;
  onLogoutClick?: MouseEventHandler;
  logoutText?: string;
  logoutHref?: string;
};

type DigitalFeedbackSettings = {
  programUrl?: string;
};

export type DigitalFeedbackProps = {
  user?: User;
  title?: string;
  feedbackTooltipContent?: ReactNode;
  digitalFeedbackSettings?: DigitalFeedbackSettings;
};

type Link = {
  self: string;
};

export type Application = {
  id: string;
  name: string;
  isExternal?: boolean;
  current?: boolean;
  target?: string;
  links: Link;
};

export type GlobalNavigationProps = {
  applications?: Application[];
};

export type SearchNavProps = {
  searchTooltip?: ReactNode;
  searchChildren?: ReactNode;
};

export type NotificationsNavProps = {
  unreadNotifications?: number;
  notificationsTooltip?: ReactNode;
  notificationsChildren?: ReactNode;
};

export type HelpProps = {
  shouldShowKnowledgeBaseLink?: boolean;
  knowledgeBaseHref?: string;
  knowledgeBaseLinkText?: string;
  onKnowledgeBaseClick?: MouseEventHandler;
  knowledgeBaseLink?: {
    text?: string;
    href: string;
    target?: string;
  };
  restApiLink?: {
    text?: string;
    href: string;
    target?: string;
  };
  shouldShowNewsLink?: boolean;
  newsLinkText?: string;
  onNewsClick?: MouseEventHandler;
  aboutLinkText?: string;
  aboutDialogHeader?: string;
  aboutDialogClose?: string;
  shouldShowTourGuideReset?: boolean;
  onTourGuideReset?: () => void;
  tourGuideResetText?: string;
};

type RecentMenuItem = {
  id: string;
  name: string;
  href: string;
};

export type TitleProps = {
  allItemsHref?: string;
  title: string;
  onAllItemsClick?: MouseEventHandler;
};

export type AllItemsProps = {
  showAllItems?: boolean;
  allItemsText?: string;
  allItemsHref?: string;
  onAllItemsClick?: MouseEventHandler;
};

export type LoadingIndicatorProps = {
  showRecentItems?: boolean;
  recentMenuLoading?: boolean;
};

export type RecentItemsHeaderProps = {
  showRecentItems?: boolean;
  recentItemsText?: string;
};

export type RecentItemsProps = {
  items?: RecentMenuItem[];
  showRecentItems?: boolean;
  onRecentItemClick?: (
    event: MouseEvent,
    {
      href,
      name,
      id,
    }: {
      href: string;
      name: string;
      id: string;
    }
  ) => void;
  recentMenuLoading?: boolean;
};

export type CreateNewProps = {
  showCreateNew?: boolean;
  createNewHref?: string;
  createNewText?: string;
  onCreateNewClick?: MouseEventHandler;
};

export type CustomItemsProps = {
  customItems?: CustomItem[];
};

export type CustomItemHref = {
  href: string;
  target?: string;
};
export type CustomItemOnClick = {
  onClick: (id?: string) => void;
};

export type CustomItem = {
  id?: string;
  text: string;
} & Either<CustomItemHref, CustomItemOnClick>;

export type TopDividerProps = {
  showAllItems?: boolean;
  showCreateNew?: boolean;
  showViewDeleted?: boolean;
};

export type BottomDividerProps = {
  showViewDeleted?: boolean;
  showCreateNew?: boolean;
  showAllItems?: boolean;
  showRecentItems?: boolean;
};

export type ViewDeletedProps = {
  showViewDeleted?: boolean;
  viewDeletedHref?: string;
  viewDeletedText?: string;
  onViewDeletedClick?: MouseEventHandler;
};

export type RecentMenuProps = TitleProps &
  AllItemsProps &
  LoadingIndicatorProps &
  RecentItemsHeaderProps &
  RecentItemsProps &
  CreateNewProps &
  TopDividerProps &
  BottomDividerProps &
  CustomItemsProps &
  ViewDeletedProps & {
    showRecentMenuDropDown?: boolean;
    openRecentMenu?: boolean;
    onRecentMenuToggle?: (isOpen: boolean) => void;
  };

export type AppHeaderProps = {
  oidcEnabled: boolean;
  children?: ReactNode;
  currentContainerIconPath?: string;
  showSearch?: boolean;
  showUserMenu?: boolean;
  showRecentMenu?: boolean;
  showGlobalNavigation?: boolean;
  showFeedbackButton?: boolean;
  feedbackTooltipContent?: ReactNode;
  showDigitalFeedbackButton?: boolean;
  showNotifications?: boolean;
} & UserMenuProps &
  GlobalNavigationProps &
  RecentMenuProps &
  SearchNavProps &
  HelpProps &
  DigitalFeedbackProps &
  NotificationsNavProps;
