import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Loader.module.css';

const {block} = bemFactory({
  baseClassName: 'comd-app-header-loader',
  classNames,
});

const Loader = () => <div className={block()} />;

export default Loader;
