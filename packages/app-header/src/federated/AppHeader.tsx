import React, {lazy, Suspense, Component} from 'react';

import Loader from './Loader';
import type {AppHeaderProps} from '../types';

/*
  NB!
  When introducing a breaking change in the App Header package,
  and a new major version is introduced,
  remember to also upgrade the version below: i.e. ds/AppHeader17.
*/

/* eslint-disable import/no-unresolved */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const FederatedAppHeader = lazy(() => import('ds/AppHeader17'));
/* eslint-enable import/no-unresolved */

const AppHeaderFallback = lazy(() => import('../AppHeader'));

type FederatedAppHeaderProps = {
  onCatch?: (Error) => void;
} & AppHeaderProps;

export default class AppHeader extends Component<FederatedAppHeaderProps> {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    return {
      hasError: true,
    };
  }

  componentDidCatch(error) {
    if (process.env.NODE_ENV !== 'production') {
      console.warn(
        'Error caught in federated AppHeader, showing fallback header. ' +
          'Did you forget to register "ds" in your webpack config?'
      );
    }

    this.props.onCatch?.(error);
  }

  render() {
    if (this.state.hasError) {
      return (
        <Suspense fallback={<Loader />}>
          <AppHeaderFallback {...this.props} />
        </Suspense>
      );
    }

    return (
      <Suspense fallback={<Loader />}>
        <FederatedAppHeader {...this.props} />
      </Suspense>
    );
  }
}
