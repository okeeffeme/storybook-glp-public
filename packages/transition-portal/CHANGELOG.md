# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.6&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.7&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-transition-portal

## [3.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.5&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.6&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- adding data-testid to Transition-portal component ([NPM-1208](https://jiraosl.firmglobal.com/browse/NPM-1208)) ([ac34407](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ac344074a986b9ce79c1a751b7cab202e51feccc))
- renaming data-testids in Section components ([NPM-1208](https://jiraosl.firmglobal.com/browse/NPM-1208)) ([3ff4908](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3ff49086fcb1a21c3971896d527be1e80249bc79))

## [3.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.4&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.5&targetRepoId=1246) (2023-01-20)

### Bug Fixes

- add animation attribute on TransitionPortal component for testing purposes ([NPM-1203](https://jiraosl.firmglobal.com/browse/NPM-1203)) ([1ab2028](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1ab2028dd9dded44f1710b687622147852c5ffe1))

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.3&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.4&targetRepoId=1246) (2023-01-06)

### Bug Fixes

- add test id for TransitionPortal component ([NPM-1204](https://jiraosl.firmglobal.com/browse/NPM-1204)) ([12e833e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/12e833e1ea7456c7b6720ea572cea8d422203dd0))

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.2&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.3&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-transition-portal

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.1&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-transition-portal

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-transition-portal@3.0.0&sourceBranch=refs/tags/@jotunheim/react-transition-portal@3.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-transition-portal

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.6&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.7&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.5&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.6&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-transition-portal

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.4&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.5&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.3&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.4&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-transition-portal

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.2&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.0&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.1&targetRepoId=1246) (2020-12-11)

### Fix

- Change TransitionPortal to use a nodeRef, to support React strict mode

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@2.0.0&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-transition-portal

# [2.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@1.3.1&sourceBranch=refs/tags/@confirmit/react-transition-portal@2.0.0&targetRepoId=1246) (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- import usePortal from react-context instead of transition portal ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([af924fb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/af924fb38ae4cf5877fe6dd61d2ec2d21fe0a326))

### BREAKING CHANGES

- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.
- @confirmit/react-context is new peer dependency

## [1.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-transition-portal@1.3.0&sourceBranch=refs/tags/@confirmit/react-transition-portal@1.3.1&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-transition-portal

# [1.3.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-transition-portal@1.2.0...@confirmit/react-transition-portal@1.3.0) (2020-08-12)

### Features

- export CSSTransitionClassNames type ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([646508f](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/646508fa85e81b1d2df0802c68f5b2a0fc21e07e))

## CHANGELOG

### v1.2.0

- Feat: Typescript support

### v1.1.0

- Refactor: restructure folder names in repository.

### v1.0.4

- Bugfix: additional check added on `unmountPortalElement` (double-calling it will not trigger the error)

### v1.0.1

- Bugfix: Method `remove` is replaced with `removeChild` to work in Internet Explorer

### v1.0.0

- Feat: Add `PortalRootContext` react context to provide custom parent container for rendering portals.
- Feat: Add `usePortal` hook and `withPortal` HOC to work with React Portals (the root container can be provided via `PortalRootContext`).
- **BREAKING** Depricated `transitionName` prop removed. In case you need it, use `transitionClassNames` instead.

### v0.0.4

- Lazy portal mount and automatic unmount

### v0.0.1

- Initial version
