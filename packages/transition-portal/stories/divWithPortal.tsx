import React, {useEffect, useState, useCallback} from 'react';
import PropTypes from 'prop-types';
import {usePortal} from '../../contexts/src';

const DivWithPortal = ({buttonText}) => {
  const {
    renderInPortal,
    mountPortalElement,
    unmountPortalElement,
  } = usePortal();

  const [open, setOpen] = useState(false);
  const toggleModal = useCallback(() => setOpen(!open), [open]);
  const closeModal = useCallback(() => setOpen(false), []);

  useEffect(() => {
    if (open) {
      mountPortalElement();
    } else {
      unmountPortalElement();
    }
  }, [mountPortalElement, open, unmountPortalElement]);

  const diwWithPortal = renderInPortal(
    <div onClick={closeModal} className={'demo-modal'}>
      {'Demo Modal Div'}
    </div>
  );
  return (
    <div>
      <button type={'button'} className={'demo-button'} onClick={toggleModal}>
        {buttonText}
      </button>
      {diwWithPortal}
    </div>
  );
};

DivWithPortal.propTypes = {
  buttonText: PropTypes.node,
};

export default DivWithPortal;
