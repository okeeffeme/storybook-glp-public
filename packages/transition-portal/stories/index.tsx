/* eslint-disable react/prop-types */
import React from 'react';
import {storiesOf} from '@storybook/react';

import {PortalRootContext} from '../../contexts/src';
import DivWithPortal from './divWithPortal';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import './index.module.scss';

const DivWithMargin = ({color, style}) => (
  <div className={'div-with-padding'}>
    <DivWithPortal
      buttonText={`open div within the ${color} ${style} square *`}
    />
  </div>
);

const ColoredSquare = ({color, style}) => {
  const ref = React.createRef<HTMLDivElement>();
  const border = `2px ${style} ${color}`;

  return (
    <div className={'col'}>
      <PortalRootContext.Provider value={ref}>
        <DivWithMargin color={color} style={style} />
        <div className={'root-container'} style={{border}} ref={ref} />
      </PortalRootContext.Provider>
    </div>
  );
};

storiesOf('Components/transition-portal', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('with default context', () => (
    <div className={'div-with-padding'}>
      <DivWithPortal buttonText="open div with overlay over the whole document *" />
      <div className={'note'}>* Close the div by clicking on it.</div>
    </div>
  ))
  .add('with custom contexts', () => (
    <>
      <div className={'important-info'}>
        To make browser calc position relatively to your container for items
        with Fixed position, don't forget to specify something for the following
        css props: <b>transform, perspective, filter</b>
      </div>
      <div className={'important-info'}>
        To make browser calc position relatively to your container for items
        with Absolute position, don't forget to set <b>position</b> css property
        to anything but 'static' for your root container
      </div>
      <div className={'row'}>
        <ColoredSquare color={'blue'} style={'dashed'} />
        <ColoredSquare color={'red'} style={'solid'} />
        <ColoredSquare color={'green'} style={'dotted'} />
      </div>
      <div className={'note'}>
        * Close the div by clicking on the button or on the div itself.
      </div>
    </>
  ));
