import TransitionPortal, {
  CSSTransitionClassNames,
} from './components/TransitionPortal';

export {TransitionPortal};
export type {CSSTransitionClassNames};
export default TransitionPortal;
