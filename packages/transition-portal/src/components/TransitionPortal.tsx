import React, {useCallback, useEffect, useLayoutEffect} from 'react';
import {CSSTransition} from 'react-transition-group';

import {usePortal} from '@jotunheim/react-contexts';

const animationMinTick = 17;

export type CSSTransitionClassNames = {
  appear?: string;
  appearActive?: string;
  appearDone?: string;
  enter?: string;
  enterActive?: string;
  enterDone?: string;
  exit?: string;
  exitActive?: string;
  exitDone?: string;
};

type TransitionPortalProps = {
  open?: boolean;
  animation?: boolean;
  transitionClassNames: string | CSSTransitionClassNames;
  transitionEnterTimeout?: number;
  transitionLeaveTimeout?: number;
  children;
  onTransitionEnter?: (node: HTMLElement) => void;
  onTransitionEntering?: (node: HTMLElement) => void;
  onTransitionEntered?: (node: HTMLElement) => void;
  onTransitionLeave?: (node: HTMLElement) => void;
  onTransitionLeaving?: (node: HTMLElement) => void;
  onTransitionLeft?: (node: HTMLElement) => void;
  className?: string;
  portalRef?: (ref: HTMLElement | null) => void;
};

const TransitionPortal = ({
  open,
  transitionClassNames,
  transitionEnterTimeout,
  transitionLeaveTimeout,
  children,
  animation,
  onTransitionEntering,
  onTransitionEntered,
  onTransitionLeave,
  onTransitionLeaving,
  className,
  onTransitionEnter,
  onTransitionLeft,
  portalRef,
}: TransitionPortalProps) => {
  const {
    isPortalElementInDom,
    renderInPortal,
    mountPortalElement,
    unmountPortalElement,
    portalElement,
  } = usePortal({className});

  useLayoutEffect(() => {
    portalRef && portalRef(isPortalElementInDom ? portalElement : null);
    return () => {
      portalRef && portalRef(null);
    };
  }, [portalElement, portalRef, isPortalElementInDom]);

  const handleTransitionEnter = useCallback(
    (node) => {
      mountPortalElement();
      onTransitionEnter && onTransitionEnter(node);
    },
    [mountPortalElement, onTransitionEnter]
  );

  const handleTransitionExit = useCallback(
    (node) => {
      unmountPortalElement();
      onTransitionLeft && onTransitionLeft(node);
    },
    [onTransitionLeft, unmountPortalElement]
  );

  /* Unmount portal element if TransitionPortal is unmounted from DOM */
  useEffect(() => {
    return () => {
      unmountPortalElement();
    };
  }, [unmountPortalElement]);

  const nodeRef = React.useRef(null);
  const transitioningChild = React.cloneElement(React.Children.only(children), {
    ref: nodeRef,
  });

  return renderInPortal(
    <CSSTransition
      data-testid="transition-portal"
      data-animation={animation}
      data-transition-portal={open}
      nodeRef={nodeRef}
      in={open}
      appear
      mountOnEnter
      unmountOnExit
      timeout={{
        enter: transitionEnterTimeout || animationMinTick,
        exit: transitionLeaveTimeout || animationMinTick,
      }}
      classNames={transitionClassNames}
      onEnter={handleTransitionEnter}
      onEntering={onTransitionEntering}
      onEntered={onTransitionEntered}
      onExit={onTransitionLeave}
      onExiting={onTransitionLeaving}
      onExited={handleTransitionExit}>
      {transitioningChild}
    </CSSTransition>
  );
};

export default TransitionPortal;
