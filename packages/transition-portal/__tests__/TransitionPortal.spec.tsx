import React from 'react';
import {render, screen} from '@testing-library/react';
import TransitionPortal from '../src/components/TransitionPortal';

describe('TransitionPortal', () => {
  it('should render transition portal with children', () => {
    render(
      <TransitionPortal open={true} transitionClassNames="co-transition-name">
        <span>Span Children</span>
      </TransitionPortal>
    );

    const transitionPortal = screen.getByText(/span children/i);

    expect(transitionPortal).toBeInTheDocument();
  });

  it('should not render transition portal with children', () => {
    render(
      <TransitionPortal transitionClassNames="co-transition-name">
        <span>Span Children</span>
      </TransitionPortal>
    );

    const transitionPortal = screen.queryByText(/span children/i);

    expect(transitionPortal).not.toBeInTheDocument();
  });
});
