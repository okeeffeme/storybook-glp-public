# Jotunheim React Transition Portal

## Usage

### Standard usage

```javascript
import {TransitionPortal} from '@jotunheim/react-transition-portal';

<TransitionPortal {...someProps}>
  <div>A child</div>
</TransitionPortal>;
```
