# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.32&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.33&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.32&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.32&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.30&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.31&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.29&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.30&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.28&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.29&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.27&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.28&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.26&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.27&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.25&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.26&targetRepoId=1246) (2023-03-20)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.24&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.25&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.23&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.24&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.22&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.23&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.21&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.22&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.20&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.21&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.19&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.20&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.18&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.19&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.17&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.18&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.16&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.17&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.15&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.16&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.14&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.15&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.13&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.14&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.12&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.13&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.11&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.12&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.10&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.11&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.9&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.10&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.8&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.9&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.7&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.8&targetRepoId=1246) (2023-01-30)

### Bug Fixes

- use the same version (13.1.1) of react-beautiful-dnd among all packages; bump the version of @types/react-beautiful-dnd ([HUB-9705](https://jiraosl.firmglobal.com/browse/HUB-9705)) ([f65c6d4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f65c6d42d89506ed47af76d313fffbc139244ea2))

## [9.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.6&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.7&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.5&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.6&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.3&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.5&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.3&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.4&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.2&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.3&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.1&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.2&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.0&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.1&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

# [9.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.13&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.2.0&targetRepoId=1246) (2023-01-05)

### Features

- add tooltip to the row for drag-drop-columns ([65e418b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/65e418b0d2a9b00546e32126d9762d19221a05f1))

# [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.13&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.1.0&targetRepoId=1246) (2022-12-28)

### Features

- add tooltip to the row for drag-drop-columns ([5e0b2ae](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5e0b2ae77c7ba3d76e63cc66bec8b5d48a83c2d5))

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.12&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.13&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.11&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.12&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.10&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.11&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.9&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.10&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.8&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.5&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.5&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.5&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.4&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.3&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.2&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.0&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.17&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@9.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of IconButton from DragAndDropColumns ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([67e0a43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/67e0a4384abd819b2b08c578a61f6a1bdadeef61))

### Features

- remove className prop of DragDropColumns ([NPM-937](https://jiraosl.firmglobal.com/browse/NPM-937)) ([fe22e1d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fe22e1d9a737b973965b42ced416a6503d7e4f14))
- remove default theme from DragDropColumns ([NPM-1069](https://jiraosl.firmglobal.com/browse/NPM-1069)) ([9ab1b52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9ab1b52d60d7ee66862b51e94c0624dab4787d9a))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.16&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.15&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.14&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.13&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.12&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.11&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.10&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.9&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.7&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.4&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.3&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.2&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.1&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.0&sourceBranch=refs/tags/@jotunheim/react-drag-drop-columns@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-drag-drop-columns

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.2.88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.87&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.88&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.86&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.87&targetRepoId=1246) (2022-06-27)

### Bug Fixes

- Removed unused variables and added a comment to ESLint setting ignoreRestSiblings ([NPM-1022](https://jiraosl.firmglobal.com/browse/NPM-1022)) ([85c2453](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85c24532ffeaaa2229b753caf7510ad367d9998e))

## [7.2.86](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.85&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.86&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.84&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.85&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.2.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.83&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.84&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.83](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.82&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.83&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.81&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.82&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.79&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.80&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.78&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.79&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.77&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.78&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.76&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.77&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.75&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.76&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.74&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.75&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.69&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.70&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.68&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.69&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.67&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.68&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.66&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.67&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.65&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.66&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.64&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.65&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.63&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.64&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.62&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.63&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.61&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.62&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.60&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.61&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.59&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.60&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.2.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.58&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.59&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.56&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.57&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.55&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.56&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.54&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.55&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.53&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.54&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.52&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.53&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.51&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.52&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.50&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.51&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.49&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.50&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.48&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.49&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.47&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.48&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.46&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.47&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.45&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.46&targetRepoId=1246) (2021-08-25)

### Bug Fixes

- Use "import type" to import types ([STUD-2915](https://jiraosl.firmglobal.com/browse/STUD-2915)) ([a938059](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a9380593ba722f5db4bc41663b145a07bc58c1e3))

## [7.2.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.44&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.45&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.43&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.44&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.42&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.43&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.41&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.42&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.40&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.41&targetRepoId=1246) (2021-07-23)

### Bug Fixes

- export type definitions as types, to avoid console warnings that exports are not found ([NPM-810](https://jiraosl.firmglobal.com/browse/NPM-810)) ([2009962](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2009962f53e6feffa600457a3b1ee131fb961120))

## [7.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.39&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.40&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.38&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.39&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.37&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.38&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.36&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.37&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.34&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.35&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.33&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.34&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.32&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.33&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.31&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.32&targetRepoId=1246) (2021-06-22)

### Bug Fixes

- using z-index context for the element being dragged to be visible ([NPM-607](https://jiraosl.firmglobal.com/browse/NPM-607)) ([8bed23b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8bed23b73010d3e5031326d0484ca6ca3d3c256a))

## [7.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.30&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.31&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.29&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.30&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.28&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.29&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.27&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.28&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.26&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.27&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.25&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.26&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.24&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.25&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.23&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.24&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.22&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.23&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.20&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.21&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.19&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.20&targetRepoId=1246) (2021-04-22)

### Bug Fixes

- remove invalid className usage on CheckBox, and fix incorrect type on onCheck callback ([NPM-598](https://jiraosl.firmglobal.com/browse/NPM-598)) ([a1290c8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a1290c8e6a5407b89d41f7fd6b1f8febf6d4dec2))
- update types for CheckBox onChange callback to make TypeScript happy ([NPM-598](https://jiraosl.firmglobal.com/browse/NPM-598)) ([ef8de73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ef8de73414182f1b8c3b443055be2fcf2a3cc4d3))

## [7.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.18&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.19&targetRepoId=1246) (2021-04-20)

### Bug Fixes

- onRemoveAll button should be at the bottom ([NPM-740](https://jiraosl.firmglobal.com/browse/NPM-740)) ([0e5fd4c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0e5fd4c1012fd9836a81667e7725ece683141ca9))

## [7.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.17&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.18&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.16&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.17&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.15&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.16&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.14&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.15&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.13&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.14&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.12&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.13&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.11&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.12&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.10&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.11&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.9&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.10&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.8&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.9&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.7&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.8&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.6&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.7&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.5&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.6&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.4&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.5&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.3&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.4&targetRepoId=1246) (2021-03-10)

### Bug Fixes

- remove style override from DragDropColumns for Tabs, to avoid issue where Tab styles is loaded after DnDColumns, breaking the layout. Replaced with new hasPadding prop on Tab. ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([4953618](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/49536186b8163727234c4d9a2f10e5db61a5c475))

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.2&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.3&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.2&targetRepoId=1246) (2021-03-03)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.0&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.1&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.22&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.2.0&targetRepoId=1246) (2021-02-18)

### Bug Fixes

- Fix selector in unit tests ([NPM-716](https://jiraosl.firmglobal.com/browse/NPM-716)) ([e9dc682](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e9dc6821ea6fbbcc70883d9299171d22e7a78e28))

### Features

- Add no data label ([NPM-716](https://jiraosl.firmglobal.com/browse/NPM-716)) ([5ceee0d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5ceee0da9b7b1a8d90770bbf623ab6f8bf028746))

## [7.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.21&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.22&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.20&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.21&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.19&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.20&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.17&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.18&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.16&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.17&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.15&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.16&targetRepoId=1246) (2021-01-29)

### Bug Fixes

- fix console error about onChange undefined callback ([NPM-697](https://jiraosl.firmglobal.com/browse/NPM-697)) ([238c681](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/238c681af776d6cd1297ee4f52f2072b6c97b798))
- fix story dnd component, it handles disabled items on add all/deselect all ([NPM-694](https://jiraosl.firmglobal.com/browse/NPM-694)) ([03b14d6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/03b14d6cc12e5a060e3867b0f8a481c5503f9424))

## [7.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.14&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.15&targetRepoId=1246) (2021-01-27)

### Bug Fixes

- fix selection several items by clicking shift + left mouse button ([NPM-605](https://jiraosl.firmglobal.com/browse/NPM-605)) ([7c72cb4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7c72cb414de5a7b025fef5adbbe41111561f3536))

## [7.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.13&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.14&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.12&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.13&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.11&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.12&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.10&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.11&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.8&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.9&targetRepoId=1246) (2020-12-16)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.7&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.8&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.6&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.7&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.5&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.6&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.4&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.5&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.3&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.4&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.2&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.3&targetRepoId=1246) (2020-12-10)

### Bug Fixes

- FUpdate importing path to DragDropColumnsProps ([NPM-650](https://jiraosl.firmglobal.com/browse/NPM-650)) ([4b7cebb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4b7cebbb829b4136dd084ba397e931d451b8b04d))

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.2&targetRepoId=1246) (2020-12-09)

### Bug Fixes

- DragDropColumnsProps was not found in DragDropColumns ([NPM-650](https://jiraosl.firmglobal.com/browse/NPM-650)) ([9a3d4b0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9a3d4b070a4a09d752d77c55ff4dec13cecec155))

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.0&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.1&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.10&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.1.0&targetRepoId=1246) (2020-12-08)

### Features

- Export the neccessary types with DragDropColumns component ([NPM-649](https://jiraosl.firmglobal.com/browse/NPM-649)) ([61b4865](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/61b4865cfb36817035987ce50ac9c18fa182ee2f))

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.9&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.6&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.3&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.2&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@6.0.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@6.0.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@7.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@6.0.1&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@6.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.29&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [5.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.28&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.29&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.27&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.28&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.26&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.27&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.25&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.26&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.22&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.23&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.21&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.22&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.19&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.20&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.14&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.15&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.13&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.14&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.11&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.12&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.9&sourceBranch=refs/tags/@confirmit/react-drag-drop-columns@5.2.10&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.7](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.2.6...@confirmit/react-drag-drop-columns@5.2.7) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.2.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.2.2...@confirmit/react-drag-drop-columns@5.2.3) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

# [5.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.1.1...@confirmit/react-drag-drop-columns@5.2.0) (2020-08-24)

### Features

- Add possibility to disable items and remove menu buttons if actions are not passed ([NPM-491](https://jiraosl.firmglobal.com/browse/NPM-491)) ([b220319](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/b2203194622f73b9cbfb78fb89667e9b11262598))

* Possibility to remove select all checkbox from list header.

## [5.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.1.0...@confirmit/react-drag-drop-columns@5.1.1) (2020-08-21)

### Bug Fixes

- downgrade react-beautiful-dnd to 12.2.0 ([NPM-503](https://jiraosl.firmglobal.com/browse/NPM-503)) ([ca6e840](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/ca6e840e1448310286caf0214c2c9c7e5ea101a6))

# [5.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.0.29...@confirmit/react-drag-drop-columns@5.1.0) (2020-08-21)

### Features

- add typescript support ([NPM-376](https://jiraosl.firmglobal.com/browse/NPM-376)) ([6542be5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/6542be5921aea4f22234c4830c8599c1fbe9f0a0))

## [5.0.29](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.0.28...@confirmit/react-drag-drop-columns@5.0.29) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.0.25](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.0.24...@confirmit/react-drag-drop-columns@5.0.25) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## [5.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-drag-drop-columns@5.0.23...@confirmit/react-drag-drop-columns@5.0.24) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-drag-drop-columns

## CHANGELOG

### v5.0.13

- Fix: lock version dependencies of `react-beautiful-dnd` and `react-window`, and upgrade to `react-beautiful-dnd@12.2.0` to match the same version used in `@confirmit/react-section`

### v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-drag-drop-columns`

### v4.3.15

- Fix: Remove autofocus from search box.

### v4.3.0

- Removing package version in class names.

### v4.2.0

- Feat: add disabled prop. The whole component can be disabled including drag, drop, checkboxes and middle column buttons.

### v4.1.0

- Feat: Show middle menu button text in tooltip on hover
- Refactor: Use IconButton component instead of custom implementation for middle buttons
- Refactor: Update to new icon package
- Refactor: Remove custom styling for Checkbox

### 4.0.0

- **BREAKING**:
  - Major UI changes to @confirmit/react-search-field

### v3.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v2.2.0

Feat: Add loading dots

### v2.1.0

Feat: Use new @confirmit/react-search-field instead of custom component

### v2.0.0

- **BREAKING**: data shape changed:

  - `firstColumnTabs` and `secondColumnTabs` data shape changed:
    - `id` field renamed to 'tabId'.
    - `items` objects no more require `label` field.
    - `renderItem` removed .(see the point about render multiple columns below)
    - `onItemClick` removed (see the point about render multiple columns below).
    - Feat: `dragEnabled` prop added.
    - Feat: `dropEnabled` prop added.
    - Feat: `searchPlaceholder` and `searchText` field added for each tab column.
    - Feat: `showHeader`prop added .

- **BREAKING**: search made uncontrolled:

  - Data lists should be filtered outside of the component.
  - Feat: `onSearch` prop added.
  - Feat: `showSearch` prop added.
  - `firstColumnSearchPlaceholder` and `secondColumnSearchPlaceholder` props replaced by `searchPlaceholder` field per column.

- **BREAKING**: Render multiple columns support added:

  - Feat: `Column` component added to define which fields from the item object should be rendered and how. By default, each line in the list contains only data from the item object, defined by prop `idFieldName`. This prop default value is "label". Is more than one column required, than `<Column />` child component should be provided for each data field in `items` object. Examples are provided in stories.
  - `<Column />` component props are:
    - `title`: define the title of the column in a hearer row.
    - `onRenderCell`: rendered prop, define how to render column cells.
    - `columnId`: key for the field in the `items` object.
    - `className`: cell class name.
    - `headerClassName`: header cel class name.
  - Feat: `idFieldName` prop added to choose what field in `items` array object should be used as an id.

- Feat: Added multiple drag-n-drop support.
- Feat: Added multiple items selection by shift+click
- Feat: Added middle column with buttons "Add", "Add all", "Remove", "Remove all".
  - Feat: `onAddAll`, `onRemoveAll`, `onAddItems`, `onRemoveItems` props added.
  - Feat: `addAllText`, `addItemsText`, `removeAllText`, `removeItemsText` props added.
- Feat: Added column header with "check all" checkbox. Header visibility is set by `showHeader` prop for each tab column.
- Feat: `showTabs` prop added to set if tabs menu is visible or not. If set to `false` then only one column will be available for each tab.
- Feat: support of the list virtualization added.
  - `virtualListEnabled` prop added
  - `virtualListItemHeight` prop added. This prop is required for the underlying virtualization library to calculate.
- Feat: `listHeight` prop added.
- `portalEnabled` prop removed.

### v1.3.15

- Fix: use more color variables from global-styles-material package

### v1.3.3

- `zindexStack` is taken from `useZIndexStack` react context

### v1.2.0

- Added an option to put dragging element in the React portal.

### v1.1.0

- Update TextField usage to be controlled

### v1.0.1

- **BREAKING**: Update items in each tab to be an object instead of string. Update propTypes as well.

### v0.3.0

- **BREAKING**
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v0.2.0

- **BREAKING**: Update `react` and `react-dom` requirement to ^16.8.0
- **BREAKING**: Shape of `data` prop has changed.
  - `secondColumnTab` renamed to `secondColumnTabs` and now expects an array (identical to `firsColumnTabs`)
  - `firstColumnTabs` and `secondColumnTabs` are now identical
  - `onAdd` and `onRemove` props have been renamed to `onItemClick`
- feat: support more than 1 tab in second column
- refactor: use new version of tabs, cleanup and simplify component and css

### v0.0.1

- Initial version
