export const actionTypes = {
  addItems: 'addItems',
  removeItems: 'removeItems',
  removeAll: 'removeAll',
  setSearchTest: 'setSearchText',
};

export const actionCreators = {
  addItems: ({ids, index}) => ({
    type: actionTypes.addItems,
    payload: {ids, index},
  }),
  removeItems: ({ids}) => ({
    type: actionTypes.removeItems,
    payload: {ids},
  }),
  removeAll: () => ({
    type: actionTypes.removeAll,
  }),
  setSearchText: ({value, tabId}) => ({
    type: actionTypes.setSearchTest,
    payload: {value, tabId},
  }),
};
