import {DragDropColumnsProps} from '../../src/types';
import {ReactElement} from 'react';

export type DragDropColumnsControlledWrapProps<TItem> = {
  renderDragAndDrop: (props: DragDropColumnsProps<TItem>) => ReactElement;
  searchFn: (item, searchText) => boolean;
  initialSelectedData?: string[];
};

export type ExampleOfListItem = {id: string; title: string; disabled?: boolean};
