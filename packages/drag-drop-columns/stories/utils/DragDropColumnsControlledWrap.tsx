import {useReducer} from 'react';
import {actionCreators, actionTypes} from './actions';
import {tabIds} from './constants';
import {DragEndCallback} from '../../src/types';
import {DragDropColumnsControlledWrapProps, ExampleOfListItem} from './types';

const itemsById = {
  item_1_1: {id: 'item_1_1', title: 'Lorem'},
  item_1_2: {id: 'item_1_2', title: 'Ipsum', disabled: true},
  item_1_3: {id: 'item_1_3', title: 'dolor'},
  item_1_4: {id: 'item_1_4', title: 'sit', disabled: true},
  item_1_5: {id: 'item_1_5', title: 'amet'},
  item_1_6: {
    id: 'item_1_6',
    title:
      'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium',
  },
  item_1_7: {id: 'item_1_7', title: 'adipiscing'},
  item_1_8: {id: 'item_1_8', title: 'elit'},
  item_1_9: {id: 'item_1_9', title: 'sed'},
  item_1_10: {id: 'item_1_10', title: 'sed'},
  item_1_11: {id: 'item_1_11', title: 'sed'},
  item_1_12: {
    id: 'item_1_12',
    title:
      'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium',
  },
  item_1_13: {id: 'item_1_13', title: 'sed'},
  item_1_14: {id: 'item_1_14', title: 'sed'},
  item_1_15: {id: 'item_1_15', title: 'sed'},
  item_3: {id: 'item_3', title: 'incididunt', disabled: true},
  item_4: {id: 'item_4', title: 'ut'},
  item_2_1: {id: 'item_2_1', title: 'do'},
  item_2_2: {id: 'item_2_2', title: 'eiusmod'},
  item_2_3: {id: 'item_2_3', title: 'tempor'},
};

const sourceLists = {
  [tabIds.firstTab]: [
    'item_1_1',
    'item_1_2',
    'item_1_3',
    'item_1_4',
    'item_1_5',
    'item_1_6',
    'item_1_7',
    'item_1_8',
    'item_1_10',
    'item_1_11',
    'item_1_12',
    'item_1_13',
    'item_1_14',
    'item_1_15',
    'item_3',
    'item_4',
  ],
  [tabIds.secondTab]: ['item_2_1', 'item_2_2', 'item_2_3'],
};

const getSourceColumnAll = ({list, state}) => {
  return list
    .filter((id) => !state.selectedColumn.includes(id))
    .map((id) => itemsById[id]);
};

const getSelectedColumnFiltered = ({state}) => {
  return state.selectedColumn.map((id) => itemsById[id]);
};

const getColumnFiltered = ({all, state, tabId, searchFn}) => {
  return all.filter((item) => searchFn(item, state.searchTextByTabId[tabId]));
};

const getSourceColumnData = ({list, state, tabId, searchFn}) => {
  const all = getSourceColumnAll({list, state});

  const filtered = getColumnFiltered({all, state, tabId, searchFn});

  return {
    all,
    filtered,
  };
};

const getSelectedColumnData = ({state, tabId, searchFn}) => {
  const all = getSelectedColumnFiltered({state});

  const filtered = getColumnFiltered({all, state, tabId, searchFn});

  return {
    all,
    filtered,
  };
};

const initialState = {
  selectedColumn: ['item_3', 'item_4'],
  searchTextByTabId: {
    [tabIds.firstTab]: '',
    [tabIds.secondTab]: '',
    [tabIds.selectedColumn]: '',
  },
};

const reducer = (state, action) => {
  switch (action.type) {
    case actionTypes.addItems: {
      return {
        ...state,
        selectedColumn: [
          ...state.selectedColumn.slice(0, action.payload.index),
          ...action.payload.ids,
          ...state.selectedColumn.slice(action.payload.index),
        ],
      };
    }
    case actionTypes.removeItems:
      return {
        ...state,
        selectedColumn: state.selectedColumn.filter(
          (id) => !action.payload.ids.includes(id)
        ),
      };
    case actionTypes.removeAll:
      return {
        ...state,
        selectedColumn: state.selectedColumn.filter(
          (id) => itemsById[id].disabled
        ),
      };

    case actionTypes.setSearchTest:
      return {
        ...state,
        searchTextByTabId: {
          ...state.searchTextByTabId,
          [action.payload.tabId]: action.payload.value,
        },
      };

    default:
      return state;
  }
};

function DragDropColumnsControlledWrap({
  renderDragAndDrop,
  searchFn,
  initialSelectedData,
}: DragDropColumnsControlledWrapProps<ExampleOfListItem>) {
  const [state, dispatch] = useReducer(reducer, {
    ...initialState,
    selectedColumn: initialSelectedData ?? initialState.selectedColumn,
  });

  const onSearch = ({value, tabId}) => {
    dispatch(actionCreators.setSearchText({value, tabId}));
  };

  const dataByTabId = {
    [tabIds.firstTab]: getSourceColumnData({
      list: sourceLists[tabIds.firstTab],
      state,
      tabId: tabIds.firstTab,
      searchFn,
    }),
    [tabIds.secondTab]: getSourceColumnData({
      list: sourceLists[tabIds.secondTab],
      state,
      tabId: tabIds.secondTab,
      searchFn,
    }),
    [tabIds.selectedColumn]: getSelectedColumnData({
      state,
      tabId: tabIds.selectedColumn,
      searchFn,
    }),
  };

  const getIndexInListWithSearchFilter = ({
    tabId,
    idFieldName,
    source,
    destination,
  }) => {
    let indexInNonFilteredList = 0;

    const {all, filtered} = dataByTabId[tabId];

    if (filtered[destination.index - 1]) {
      indexInNonFilteredList =
        all.findIndex(
          (item) =>
            filtered[destination.index - 1][idFieldName] === item[idFieldName]
        ) + 1;
    }

    // If going to reorder item in the same list, increase target index if it is bigger than source because react-beautiful-dnd removes dragged item from the list
    if (source.droppableId === destination.droppableId) {
      if (source.index < destination.index) {
        indexInNonFilteredList++;
      }
    }

    return indexInNonFilteredList;
  };

  const onDragEnd: DragEndCallback = ({payload, source, destination}) => {
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      return;
    }

    const index = getIndexInListWithSearchFilter({
      tabId: destination.droppableId,
      idFieldName: 'id',
      source,
      destination,
    });

    if (destination.droppableId === 'selectedColumn') {
      dispatch(
        actionCreators.addItems({
          ids: payload.draggableIds,
          index,
        })
      );
    } else {
      dispatch(
        actionCreators.removeItems({
          ids: payload.draggableIds,
        })
      );
    }
  };

  const onAddAll = ({tabId}) => {
    const ids = sourceLists[tabId].filter(
      (id) => !state.selectedColumn.includes(id) && !itemsById[id].disabled
    );

    dispatch(
      actionCreators.addItems({
        ids,
        index: state.selectedColumn.length,
      })
    );
  };

  const onAddItems = ({draggableIds}) => {
    dispatch(
      actionCreators.addItems({
        ids: draggableIds,
        index: state.selectedColumn.length,
      })
    );
  };

  const onRemoveItems = ({draggableIds}) => {
    dispatch(
      actionCreators.removeItems({
        ids: draggableIds,
      })
    );
  };

  const onRemoveAll = () => {
    dispatch(actionCreators.removeAll());
  };

  const commonProps = {
    onDragEnd,
    onAddAll,
    onAddItems,
    onRemoveItems,
    onRemoveAll,
    onSearch,
    idFieldName: 'id',
    data: {
      firstColumnTabs: [
        {
          tabId: tabIds.firstTab,
          label: 'First Tab',
          items: dataByTabId[tabIds.firstTab].filtered,
          showHeader: true,
          searchText: state.searchTextByTabId[tabIds.firstTab],
          searchPlaceholder: 'search tab 1',
        },
        {
          tabId: tabIds.secondTab,
          label: 'Second Tab',
          items: dataByTabId[tabIds.secondTab].filtered,
          showHeader: true,
          searchText: state.searchTextByTabId[tabIds.secondTab],
          searchPlaceholder: 'search tab 2',
        },
      ],
      secondColumnTabs: [
        {
          tabId: tabIds.selectedColumn,
          label: 'Selected Column',
          items: dataByTabId[tabIds.selectedColumn].filtered,
          showHeader: true,
          searchText: state.searchTextByTabId[tabIds.selectedColumn],
          searchPlaceholder: 'search selected',
          noDataText: 'There are no any items',
        },
      ],
    },
  };

  return renderDragAndDrop(commonProps);
}

export default DragDropColumnsControlledWrap;
