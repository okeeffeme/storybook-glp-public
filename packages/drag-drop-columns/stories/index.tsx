import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import DragDropColumns, {Column} from '../src';
import DragDropColumnsControlledWrap from './utils/DragDropColumnsControlledWrap';
import {caseInsensitiveSearch} from '../src/utils/utils';
import {Dialog} from '../../dialog/src';
import {Button} from '../../button/src';

const searchByIdAndTitle = (item, searchText) =>
  caseInsensitiveSearch(item.id, searchText) ||
  caseInsensitiveSearch(item.title, searchText);

const searchById = (item, searchText) =>
  caseInsensitiveSearch(item.id, searchText);

const getRowTooltipContent = (listItem) => {
  return (
    <div>
      <p>Id: {listItem.id}</p>
      <p>Title: {listItem.title}</p>
    </div>
  );
};

storiesOf('Components/drag-drop-columns', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('in normal div', () => (
    <DragDropColumnsControlledWrap
      searchFn={searchById}
      renderDragAndDrop={(commonProps) => {
        return <DragDropColumns {...commonProps} />;
      }}
    />
  ))
  .add('in empty data column', () => (
    <DragDropColumnsControlledWrap
      searchFn={searchById}
      initialSelectedData={[]}
      renderDragAndDrop={(commonProps) => {
        return <DragDropColumns {...commonProps} />;
      }}
    />
  ))
  .add('Custom elements', () => {
    return (
      <DragDropColumnsControlledWrap
        searchFn={searchByIdAndTitle}
        renderDragAndDrop={(commonProps) => {
          return (
            <DragDropColumns {...commonProps}>
              <Column
                columnId="id"
                title="Id"
                className="custom-cell-1"
                headerClassName="custom-cell-1"
              />
              <Column
                columnId="title"
                title="Title"
                className="custom-cell-2"
                headerClassName="custom-cell-2"
                onRenderCell={(value) => {
                  return (
                    <span>
                      <i>{value}</i>
                    </span>
                  );
                }}
              />
            </DragDropColumns>
          );
        }}
      />
    );
  })
  .add('In a modal', () => {
    const props = {
      virtualListEnabled: false,
    };

    return (
      <Dialog open={true} size={'large'}>
        <Dialog.Header
          title="Modal Title"
          onCloseButtonClick={(e) => {
            e.preventDefault();
          }}
        />
        <Dialog.Body>
          <DragDropColumnsControlledWrap
            searchFn={searchByIdAndTitle}
            renderDragAndDrop={(commonProps) => {
              return (
                <DragDropColumns {...commonProps} {...props}>
                  <Column
                    columnId="id"
                    title="Id"
                    className="custom-cell-1"
                    headerClassName="custom-cell-1"
                  />
                  <Column
                    columnId="title"
                    title="Title"
                    className="custom-cell-2"
                    headerClassName="custom-cell-2"
                  />
                </DragDropColumns>
              );
            }}
          />
        </Dialog.Body>
        <Dialog.Footer>
          <Button>Cancel</Button>
        </Dialog.Footer>
      </Dialog>
    );
  })
  .add('In a modal with virtual list', () => {
    const props = {
      virtualListEnabled: true,
    };

    return (
      <Dialog open={true} size={'large'}>
        <Dialog.Header
          title="Modal Title"
          onCloseButtonClick={(e) => {
            e.preventDefault();
          }}
        />
        <Dialog.Body>
          <DragDropColumnsControlledWrap
            searchFn={searchByIdAndTitle}
            renderDragAndDrop={(commonProps) => {
              return (
                <DragDropColumns {...commonProps} {...props}>
                  <Column
                    columnId="id"
                    title="Id"
                    className="custom-cell-1"
                    headerClassName="custom-cell-1"
                  />
                  <Column
                    columnId="title"
                    title="Title"
                    className="custom-cell-2"
                    headerClassName="custom-cell-2"
                  />
                </DragDropColumns>
              );
            }}
          />
        </Dialog.Body>
      </Dialog>
    );
  })
  .add('Hide tabs', () => {
    const props = {
      showTabs: false,
      showSearch: false,
    };

    return (
      <DragDropColumnsControlledWrap
        searchFn={searchById}
        renderDragAndDrop={(commonProps) => {
          return <DragDropColumns {...commonProps} {...props} />;
        }}
      />
    );
  })
  .add('Hide Select all and add all buttons', () => {
    return (
      <DragDropColumnsControlledWrap
        searchFn={searchById}
        renderDragAndDrop={(commonProps) => {
          const props = {
            ...commonProps,
            data: {
              ...commonProps.data,
              firstColumnTabs: commonProps.data.firstColumnTabs.map((tab) => ({
                ...tab,
                hideSelectAll: true,
              })),
            },
          };
          return <DragDropColumns {...props} />;
        }}
      />
    );
  })
  .add('Drag/drop disabled', () => {
    return (
      <DragDropColumnsControlledWrap
        searchFn={searchById}
        renderDragAndDrop={(commonProps) => {
          commonProps = {
            ...commonProps,
            data: {
              ...commonProps.data,
              secondColumnTabs: commonProps.data.secondColumnTabs.map(
                (tab) => ({
                  ...tab,
                  dropEnabled: false,
                  dragEnabled: false,
                })
              ),
            },
          };

          return <DragDropColumns {...commonProps} />;
        }}
      />
    );
  })
  .add('Component disabled', () => {
    const props = {
      disabled: true,
    };

    return (
      <DragDropColumnsControlledWrap
        searchFn={searchById}
        renderDragAndDrop={(commonProps) => {
          return <DragDropColumns {...commonProps} {...props} />;
        }}
      />
    );
  })
  .add('With row tooltip', () => {
    const props = {
      getRowTooltipContent,
    };

    return (
      <DragDropColumnsControlledWrap
        searchFn={searchById}
        renderDragAndDrop={(commonProps) => {
          return <DragDropColumns {...commonProps} {...props} />;
        }}
      />
    );
  })
  .add('In a modal with virtual list and row tooltip', () => {
    const props = {
      virtualListEnabled: true,
      getRowTooltipContent,
    };

    return (
      <Dialog open={true} size={'large'}>
        <Dialog.Header
          title="Modal Title"
          onCloseButtonClick={(e) => {
            e.preventDefault();
          }}
        />
        <Dialog.Body>
          <DragDropColumnsControlledWrap
            searchFn={searchByIdAndTitle}
            renderDragAndDrop={(commonProps) => {
              return (
                <DragDropColumns {...commonProps} {...props}>
                  <Column
                    columnId="id"
                    title="Id"
                    className="custom-cell-1"
                    headerClassName="custom-cell-1"
                  />
                  <Column
                    columnId="title"
                    title="Title"
                    className="custom-cell-2"
                    headerClassName="custom-cell-2"
                  />
                </DragDropColumns>
              );
            }}
          />
        </Dialog.Body>
      </Dialog>
    );
  });
