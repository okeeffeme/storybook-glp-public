import React from 'react';

import DragDropColumns from '../../src/components/DragDropColumns';
import {produce} from 'immer';
import {render, screen, fireEvent, within} from '@testing-library/react';
import {DragDropColumnsProps} from '../../src/types';

const commonProps: DragDropColumnsProps<{label: string; disabled?: boolean}> = {
  data: {
    firstColumnTabs: [
      {
        tabId: 'firstTab',
        label: 'First Tab',
        items: [
          {label: 'item_1_1'},
          {label: 'item_1_2'},
          {label: 'item_1_3'},
          {label: 'item_1_4'},
        ],
        showHeader: true,
        searchPlaceholder: 'search first',
        searchText: '',
        isLoading: false,
      },
      {
        tabId: 'secondTab',
        label: 'Second Tab',
        items: [{label: 'item_2_1'}, {label: 'item_2_2'}],
        showHeader: true,
        searchPlaceholder: 'search second',
        searchText: '',
        isLoading: false,
      },
    ],
    secondColumnTabs: [
      {
        tabId: 'selectedColumn',
        label: 'Selected Column',
        items: [{label: 'item_3'}, {label: 'item_4'}],
        showHeader: true,
        searchPlaceholder: 'search selected',
        searchText: 'test',
        noDataText: 'No data',
      },
    ],
  },
  onDragEnd: jest.fn,
  onSearch: jest.fn,
};

describe('Jotunheim React Drag Drop Columns :: ', () => {
  it('should render no-data label if there are no any items in the selected data column', () => {
    const props = produce(commonProps, (draft) => {
      draft.data.secondColumnTabs[0].items = [];
    });

    render(<DragDropColumns {...props} />);

    expect(screen.getByText('No data')).toBeInTheDocument();
  });

  it('should not render no-data label if there are items in the selected data column', () => {
    render(<DragDropColumns {...commonProps} />);

    expect(screen.queryByText('No data')).not.toBeInTheDocument();
  });

  it('should call controlled search callback', () => {
    const onSearchMock = jest.fn();
    const props = {
      ...commonProps,
      onSearch: onSearchMock,
    };

    render(<DragDropColumns {...props} />);

    fireEvent.change(screen.getByPlaceholderText('search first'), {
      target: {value: 'foo'},
    });

    expect(onSearchMock).toBeCalledWith({
      tabId: 'firstTab',
      value: 'foo',
    });
  });

  it('should set controlled search text', () => {
    render(<DragDropColumns {...commonProps} />);

    expect(screen.getByPlaceholderText('search selected')).toHaveValue('test');
  });

  it('should check an item on first shift+click', () => {
    render(<DragDropColumns {...commonProps} />);

    const checkboxWrap = screen.getByTestId('checkbox-wrap-item_1_1');

    fireEvent.mouseDown(checkboxWrap);

    expect(within(checkboxWrap).getByRole('checkbox')).toHaveProperty(
      'checked',
      true
    );
  });

  it('should check multiple items on shift+click', () => {
    render(<DragDropColumns {...commonProps} />);

    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_1'), {
      shiftKey: true,
    });
    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_3'), {
      shiftKey: true,
    });

    const checkboxes = screen.getAllByRole('checkbox');

    expect(checkboxes[1]).toHaveProperty('checked', true);
    expect(checkboxes[2]).toHaveProperty('checked', true);
    expect(checkboxes[3]).toHaveProperty('checked', true);
    expect(checkboxes[4]).toHaveProperty('checked', false);
  });

  it('should reset first checked element if search was applied', () => {
    render(<DragDropColumns {...commonProps} />);

    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_1'), {
      shiftKey: true,
    });
    fireEvent.change(screen.getByPlaceholderText('search first'), {
      target: {value: 'foo'},
    });
    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_3'), {
      shiftKey: true,
    });

    const checkboxes = screen.getAllByRole('checkbox');

    expect(checkboxes[1]).toHaveProperty('checked', true);
    expect(checkboxes[2]).toHaveProperty('checked', false);
    expect(checkboxes[3]).toHaveProperty('checked', true);
  });

  it('should add all elements', () => {
    const onAddAllMock = jest.fn();
    const props = produce(commonProps, (draft) => {
      draft.onAddAll = onAddAllMock;
    });

    render(<DragDropColumns {...props} />);

    fireEvent.click(screen.getByTestId('add_all_variables'));

    expect(onAddAllMock).toBeCalledWith({
      tabId: 'firstTab',
    });
  });

  it('should add checked elements', () => {
    const onAddItemsMock = jest.fn();
    const props = produce(commonProps, (draft) => {
      draft.onAddItems = onAddItemsMock;
    });

    render(<DragDropColumns {...props} />);

    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_1'), {
      shiftKey: true,
    });
    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_1_3'), {
      shiftKey: true,
    });

    fireEvent.click(screen.getByTestId('add_variable'));

    expect(onAddItemsMock).toBeCalledWith({
      draggableIds: ['item_1_1', 'item_1_2', 'item_1_3'],
      tabId: 'firstTab',
    });
  });

  it('should remove all elements', () => {
    const onRemoveAllMock = jest.fn();
    const props = produce(commonProps, (draft) => {
      draft.onRemoveAll = onRemoveAllMock;
    });

    render(<DragDropColumns {...props} />);

    fireEvent.click(screen.getByTestId('remove_all_variables'));

    expect(onRemoveAllMock).toBeCalledWith({
      tabId: 'selectedColumn',
    });
  });

  it('should remove checked elements', () => {
    const onRemoveItemsMock = jest.fn();
    const props = produce(commonProps, (draft) => {
      draft.onRemoveItems = onRemoveItemsMock;
    });

    render(<DragDropColumns {...props} />);

    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_3'), {
      shiftKey: true,
    });
    fireEvent.mouseDown(screen.getByTestId('checkbox-wrap-item_4'), {
      shiftKey: true,
    });

    fireEvent.click(screen.getByTestId('remove_variable'));

    expect(onRemoveItemsMock).toBeCalledWith({
      draggableIds: ['item_3', 'item_4'],
      tabId: 'selectedColumn',
    });
  });

  it('should not show menu buttons when actions are not passed', () => {
    render(<DragDropColumns {...commonProps} />);

    expect(screen.queryByTestId('add_all_variables')).not.toBeInTheDocument();
    expect(screen.queryByTestId('remove_variable')).not.toBeInTheDocument();
    expect(
      screen.queryByTestId('remove_all_variables')
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('add_variable')).not.toBeInTheDocument();
  });

  it('should show checkbox to select all when hideSelectAll is not passed', () => {
    render(<DragDropColumns {...commonProps} />);

    expect(screen.getByTestId('header_firstTab')).toBeInTheDocument();
    expect(screen.getByTestId('header_selectedColumn')).toBeInTheDocument();
  });

  it('should not show checkbox to select all when hideSelectAll equals true', () => {
    const props = produce(commonProps, (draft) => {
      draft.data.firstColumnTabs[0].hideSelectAll = true;
    });

    render(<DragDropColumns {...props} />);

    expect(screen.queryByTestId('header_firstTab')).not.toBeInTheDocument();
    expect(screen.getByTestId('header_selectedColumn')).toBeInTheDocument();
  });
});
