import React from 'react';

import {ZIndexStackContext} from '../../../contexts/src';
import {zindexStackFactory} from '../../../z-index/src';

import ListItem from '../../src/components/ListItem';

import {render, screen} from '@testing-library/react';
const customZIndexStack = zindexStackFactory();

const commonProps = {
  listRowClassName: '',
  index: 1,
  isDragging: false,
  checked: false,
  numberOfChecked: 0,
  columns: [],
  onItemShiftClick: jest.fn(),
  listItem: {
    id: 'item_1',
  },
  disabled: false,
  idFieldName: 'id',
  provided: {
    innerRef: jest.fn(),
    draggableProps: {
      'data-rbd-draggable-context-id': '123',
      'data-rbd-draggable-id': '321',
    },
    dragHandleProps: undefined,
  },
  tabId: 'tab',
  currentSourceTabId: 'tab',
  onItemCheck: jest.fn(),
  getRowTooltipContent: jest.fn(),
};

describe('List item', () => {
  test('it should show checkbox if component is enabled', () => {
    render(<ListItem {...commonProps} />);

    expect(screen.getByRole('checkbox')).not.toBeDisabled();
  });

  test('it should not show checkbox if component is disabled', () => {
    const props = {
      ...commonProps,
      disabled: true,
    };

    render(<ListItem {...props} />);

    expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
  });

  test('it should disable checkbox if item is disabled', () => {
    const props = {
      ...commonProps,
      listItem: {...commonProps.listItem, disabled: true},
    };

    render(<ListItem {...props} />);

    expect(screen.getByRole('checkbox')).toBeDisabled();
  });

  test('it should have proper z-index when being dragged', () => {
    const props = {
      ...commonProps,
      isDragging: true,
    };

    const {container} = render(<ListItem {...props} />);

    expect(container.firstChild).toHaveStyle('z-index: 10');
  });

  test('it should have proper z-index when being dragged inside of a custom z-index context', () => {
    const props = {
      ...commonProps,
      isDragging: true,
    };

    customZIndexStack.obtainFixed(5000);

    const {container} = render(
      <ZIndexStackContext.Provider value={customZIndexStack}>
        <ListItem {...props} />
      </ZIndexStackContext.Provider>
    );

    expect(container.firstChild).toHaveStyle('z-index: 5010');
  });
});
