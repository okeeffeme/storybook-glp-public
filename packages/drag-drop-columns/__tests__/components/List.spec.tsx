import List from '../../src/components/List';
import React from 'react';
import {render, screen} from '@testing-library/react';

describe('List', () => {
  it('renders loading dots', () => {
    const listProps = {
      tabId: 'firstTab',
      label: 'First Tab',
      items: [],
      searchPlaceholder: 'search first',
      searchText: '',
      isLoading: true,
      virtualListEnabled: false,
      checkedById: {},
      listHeight: 350,
      virtualListItemHeight: 0,
      showHeader: false,
      onCheckAll: jest.fn(),
      hideSelectAll: false,
      allChecked: false,
      droppableProvided: {
        innerRef: jest.fn(),
        placeholder: null,
        droppableProps: {
          'data-rbd-droppable-context-id': '123',
          'data-rbd-droppable-id': '321',
        },
      },
      idFieldName: '',
      isDragDisabled: false,
      isDropDisabled: false,
      onItemCheck: jest.fn(),
      onItemShiftClick: jest.fn(),
      currentSourceTabId: '',
      numberOfChecked: 0,
      columns: [],
      listRowClassName: '',
      disabled: false,
      getRowTooltipContent: jest.fn(),
    };

    render(<List {...listProps} />);

    expect(screen.getByTestId('busy-dots')).toBeInTheDocument();
  });
});
