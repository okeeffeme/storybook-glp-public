import {render, screen} from '@testing-library/react';
import MiddleMenu from '../../src/components/MiddleMenu';
import userEvent from '@testing-library/user-event';
import React from 'react';
import MiddleMenuButton from '../../src/components/MiddleMenuButton';

type ButtonType = 'Add All' | 'Add' | 'Remove All' | 'Remove';

const renderMiddleMenuAndClickButton = (button: ButtonType) => {
  const onAddItems = jest.fn();
  const onRemoveItems = jest.fn();
  const onRemoveAll = jest.fn();
  const onAddAll = jest.fn();
  const addAllText = 'Add All';
  const addItemsText = 'Add';
  const removeAllText = 'Remove All';
  const removeItemsText = 'Remove';

  render(
    <MiddleMenu
      onAddItems={onAddItems}
      onRemoveItems={onRemoveItems}
      onRemoveAll={onRemoveAll}
      onAddAll={onAddAll}
      addAllText={addAllText}
      addItemsText={addItemsText}
      removeAllText={removeAllText}
      removeItemsText={removeItemsText}
      disabled={false}
    />
  );

  switch (button) {
    case 'Add All':
      userEvent.click(screen.getByLabelText(addAllText));
      break;
    case 'Add':
      userEvent.click(screen.getByLabelText(addItemsText));
      break;
    case 'Remove All':
      userEvent.click(screen.getByLabelText(removeAllText));
      break;
    case 'Remove':
      userEvent.click(screen.getByLabelText(removeItemsText));
      break;
  }

  return {onAddAll, onAddItems, onRemoveAll, onRemoveItems};
};

describe('Jotunheim React Drag Drop Columns Middle Menu :: ', () => {
  it('should add all when the "Add All" menu button is clicked', () => {
    const {onAddAll, onAddItems, onRemoveAll, onRemoveItems} =
      renderMiddleMenuAndClickButton('Add All');

    expect(onAddAll).toHaveBeenCalled();
    expect(onAddItems).not.toHaveBeenCalled();
    expect(onRemoveAll).not.toHaveBeenCalled();
    expect(onRemoveItems).not.toHaveBeenCalled();
  });

  it('should add when the "Add" menu button is clicked', () => {
    const {onAddAll, onAddItems, onRemoveAll, onRemoveItems} =
      renderMiddleMenuAndClickButton('Add');

    expect(onAddItems).toHaveBeenCalled();
    expect(onAddAll).not.toHaveBeenCalled();
    expect(onRemoveAll).not.toHaveBeenCalled();
    expect(onRemoveItems).not.toHaveBeenCalled();
  });

  it('should remove all when the "Remove All" menu button is clicked', () => {
    const {onAddAll, onAddItems, onRemoveAll, onRemoveItems} =
      renderMiddleMenuAndClickButton('Remove All');

    expect(onRemoveAll).toHaveBeenCalled();
    expect(onAddAll).not.toHaveBeenCalled();
    expect(onAddItems).not.toHaveBeenCalled();
    expect(onRemoveItems).not.toHaveBeenCalled();
  });

  it('should remove when the "Remove" menu button is clicked', () => {
    const {onAddAll, onAddItems, onRemoveAll, onRemoveItems} =
      renderMiddleMenuAndClickButton('Remove');

    expect(onRemoveItems).toHaveBeenCalled();
    expect(onAddAll).not.toHaveBeenCalled();
    expect(onAddItems).not.toHaveBeenCalled();
    expect(onRemoveAll).not.toHaveBeenCalled();
  });

  it('should have expected behaviour when middle menu button is clicked', () => {
    const onAddItems = jest.fn();
    const addItemsText = 'Add';

    render(
      <MiddleMenuButton
        onClick={onAddItems}
        title={addItemsText}
        disabled={false}
        id="add_variable">
        <span />
      </MiddleMenuButton>
    );

    const addButton = screen.getByLabelText(addItemsText);
    userEvent.click(addButton);
    expect(onAddItems).toHaveBeenCalled();
  });

  it('should not have any behaviour when a disabled middle menu button is clicked', () => {
    const onAddItems = jest.fn();
    const addItemsText = 'Add';

    render(
      <MiddleMenuButton
        onClick={onAddItems}
        title={addItemsText}
        disabled={true}
        id="add_variable">
        <span />
      </MiddleMenuButton>
    );

    const addButton = screen.getByLabelText(addItemsText);
    userEvent.click(addButton);
    expect(onAddItems).not.toHaveBeenCalled();
  });
});
