import React from 'react';
import ListHeader from '../../src/components/ListHeader';
import {render, screen} from '@testing-library/react';

const commonProps = {
  idFieldName: 'id',
  tabId: 'tab',
  columns: [],
  checked: false,
  onCheck: jest.fn(),
  disabled: false,
  hideSelectAll: false,
};

describe('List header', () => {
  test('it should show checkbox if component is enabled', () => {
    render(<ListHeader {...commonProps} />);

    expect(screen.getByRole('checkbox')).toBeInTheDocument();
  });

  test('it should not show checkbox if component is disabled', () => {
    const props = {...commonProps, disabled: true};

    render(<ListHeader {...props} />);

    expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
  });

  test('it should not show checkbox if prop hideSelectAll is true', () => {
    const props = {...commonProps, hideSelectAll: true};

    render(<ListHeader {...props} />);

    expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
  });
});
