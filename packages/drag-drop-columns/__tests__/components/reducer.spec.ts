import {
  NO_PREVIOUS_CHECKED_VARIABLE,
  reducer,
} from '../../src/components/hooks/reducer';
import {ListTypes} from '../../src/utils/constants';
import {
  checkAllItems,
  checkItem,
  dragEnd,
  dragStart,
  resetCheck,
  resetLastCheckedIndexes,
  setTab,
  shiftCheckItem,
} from '../../src/components/hooks/actionCreators';

const initialState = {
  checkedById: {
    item1: false,
  },
  selectedTabIdByListId: {
    [ListTypes.availableFields]: 'tab1',
    [ListTypes.selectedFields]: 'tab2',
  },
  currentSourceTabId: '',
  lastCheckedIndexByTabId: {},
};

describe('Jotunheim React Drag Drop Columns reducer :: ', () => {
  it('it should set one checked ', () => {
    const action = checkItem({
      id: 'item2',
      checked: true,
      tabId: 'tab1',
      index: 0,
    });

    const result = reducer(initialState, action);

    expect(result.checkedById).toEqual({
      item1: false,
      item2: true,
    });

    expect(result.lastCheckedIndexByTabId).toEqual({
      tab1: 0,
    });
  });

  it('should check all items in tab without disabled', () => {
    const action = checkAllItems({
      metaItems: [{id: 'item1'}, {id: 'item2', disabled: true}, {id: 'item3'}],
      checked: true,
      tabId: 'tab1',
    });

    const result = reducer(initialState, action);

    expect(result.checkedById).toEqual({
      item1: true,
      item3: true,
    });

    expect(result.lastCheckedIndexByTabId).toEqual({
      tab1: NO_PREVIOUS_CHECKED_VARIABLE,
    });
  });

  it('should check one item if no previous item was shift+clicked', () => {
    const action = shiftCheckItem({
      tabId: 'tab1',
      index: 0,
      metaItems: [{id: 'item1'}, {id: 'item2'}, {id: 'item3'}, {id: 'item3'}],
      id: 'item1',
    });

    const result = reducer(initialState, action);

    expect(result.checkedById).toEqual({
      item1: true,
    });

    expect(result.lastCheckedIndexByTabId).toEqual({
      tab1: 0,
    });
  });

  it('should check many items except disabled if previous item was shift+clicked', () => {
    const state = {...initialState, lastCheckedIndexByTabId: {tab1: 0}};

    const action = shiftCheckItem({
      tabId: 'tab1',
      index: 2,
      metaItems: [
        {id: 'item1'},
        {id: 'item2', disabled: true},
        {id: 'item3'},
        {id: 'item3'},
      ],
      id: 'item3',
    });

    const result = reducer(state, action);

    expect(result.checkedById).toEqual({
      item1: true,
      item3: true,
    });

    expect(result.lastCheckedIndexByTabId).toEqual({
      tab1: 2,
    });
  });

  it('should set selected tab for list type', () => {
    const action = setTab({
      listType: ListTypes.selectedFields,
      tabId: 'tab3',
    });

    expect(
      reducer(initialState, action).selectedTabIdByListId[
        ListTypes.selectedFields
      ]
    ).toEqual('tab3');
  });

  it('should should reset check', () => {
    const action = resetCheck();

    const state = {
      ...initialState,
      checkedById: {item1: true, item2: true},
      lastCheckedIndexByTabId: {tab1: 2, tab2: 1},
    };

    const result = reducer(state, action);

    expect(result.checkedById).toEqual({});
    expect(result.lastCheckedIndexByTabId).toEqual({tab1: -1, tab2: -1});
  });

  it('should reset last checked indexes', () => {
    const action = resetLastCheckedIndexes({tabId: 'tab1'});

    const state = {
      ...initialState,
      lastCheckedIndexByTabId: {tab1: 2, tab2: 1},
    };

    const result = reducer(state, action);

    expect(result.lastCheckedIndexByTabId).toEqual({tab1: -1, tab2: 1});
  });

  it('should should set current source tab on drag start', () => {
    const action = dragStart({
      currentSourceTabId: 'tab1',
    });

    expect(reducer(initialState, action).currentSourceTabId).toBe('tab1');
  });

  it('should reset current tab on drag end', () => {
    const action = dragEnd();

    const state = {
      ...initialState,
      currentSourceTabId: 'tab1',
    };

    expect(reducer(state, action).currentSourceTabId).toBe('');
  });
});
