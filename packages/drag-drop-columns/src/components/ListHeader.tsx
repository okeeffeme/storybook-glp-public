import React, {useContext} from 'react';
import cn from 'classnames';
import {CheckBox, ToggleState} from '@jotunheim/react-toggle';
import {BemContext} from './BemContext';
import {Column, TabListItem, TabId} from '../types';

type ListHeaderProps<TItem extends TabListItem> = {
  onCheck: (checked?: boolean | ToggleState) => void;
  hideSelectAll: boolean;
  tabId: TabId;
  checked: boolean;
  columns: Column<TItem>[];
  idFieldName: string;
  disabled: boolean;
};

function ListHeader<TItem extends TabListItem>({
  onCheck,
  hideSelectAll,
  tabId,
  checked,
  columns,
  idFieldName,
  disabled,
}: ListHeaderProps<TItem>) {
  const {element} = useContext(BemContext);

  return (
    <div className={element('tab-header')}>
      {!disabled && (
        <div className={element('checkbox-cell')}>
          {!hideSelectAll && (
            <CheckBox
              id={`header_${tabId}`}
              onChange={onCheck}
              checked={checked}
              data-testid={`header_${tabId}`}
            />
          )}
        </div>
      )}

      <div
        className={cn(element('header-content'), {
          [element('header-content', 'disabled')]: disabled,
        })}>
        {columns.length ? (
          columns.map(({title, columnId, headerClassName}) => {
            return (
              <div
                key={columnId}
                className={cn([element('header-cell')], headerClassName)}>
                {title}
              </div>
            );
          })
        ) : (
          <div className={cn([element('header-cell')])}>{idFieldName}</div>
        )}
      </div>
    </div>
  );
}

export default ListHeader;
