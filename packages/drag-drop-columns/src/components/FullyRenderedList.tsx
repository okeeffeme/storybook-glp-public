import React, {Ref, useMemo} from 'react';
import {Draggable} from 'react-beautiful-dnd';
import ListItem from './ListItem';
import {
  Column,
  GetRowTooltipCallback,
  ItemCheckCallback,
  ItemShiftCheckCallback,
  TabId,
  TabListItem,
} from '../types';

type FullyRenderedListProps<TItem extends TabListItem> = {
  height: number;
  outerRef: Ref<HTMLDivElement>;
  placeholder?: React.ReactNode | null;
  itemData: {
    items: TItem[];
    checkedById: Record<string, boolean>;
    idFieldName: string;
    isDragDisabled: boolean;
    tabId: TabId;
    onItemCheck: ItemCheckCallback<TItem>;
    onItemShiftClick: ItemShiftCheckCallback<TItem>;
    currentSourceTabId: TabId;
    numberOfChecked: number;
    columns: Column<TItem>[];
    listRowClassName?: string;
    disabled: boolean;
    getRowTooltipContent?: GetRowTooltipCallback<TItem>;
  };
};

function FullyRenderedList<TItem extends TabListItem>({
  height,
  outerRef,
  placeholder,
  itemData,
}: FullyRenderedListProps<TItem>) {
  const style = useMemo(() => ({height: `${height}px`}), [height]);

  const {idFieldName, isDragDisabled, items, checkedById, ...rest} = itemData;

  return (
    <div ref={outerRef} style={style}>
      {items.map((listItem, index) => {
        return (
          <Draggable
            draggableId={listItem[idFieldName] + ''}
            index={index}
            key={listItem[idFieldName] + ''}
            isDragDisabled={isDragDisabled || listItem.disabled}>
            {(provided, snapshot) => (
              <ListItem
                listItem={listItem}
                provided={provided}
                isDragging={snapshot.isDragging}
                idFieldName={idFieldName}
                index={index}
                checked={checkedById[listItem[idFieldName]] || false}
                {...rest}
              />
            )}
          </Draggable>
        );
      })}
      {placeholder}
    </div>
  );
}

export default FullyRenderedList;
