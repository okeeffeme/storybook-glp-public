import React, {CSSProperties, useContext, useEffect, useState} from 'react';
import cn from 'classnames';
import {CheckBox} from '@jotunheim/react-toggle';
import {BemContext} from './BemContext';
import {
  Column,
  GetRowTooltipCallback,
  ItemCheckCallback,
  ItemShiftCheckCallback,
  TabId,
  TabListItem,
} from '../types';
import {DraggableProvided} from 'react-beautiful-dnd';
import {useZIndexStack} from '@jotunheim/react-contexts';
import {Tooltip} from '@jotunheim/react-tooltip';

type ListItemProps<TItem extends TabListItem> = {
  listItem: TItem;
  index: number;
  checked: boolean;
  provided: DraggableProvided;
  isDragging: boolean;
  idFieldName: string;
  style?: CSSProperties;
  tabId: TabId;
  onItemCheck: ItemCheckCallback<TItem>;
  onItemShiftClick: ItemShiftCheckCallback<TItem>;
  currentSourceTabId: TabId;
  numberOfChecked: number;
  columns: Column<TItem>[];
  listRowClassName?: string;
  disabled: boolean;
  getRowTooltipContent?: GetRowTooltipCallback<TItem>;
};

function stub() {}

function ListItem<TItem extends TabListItem>({
  listItem,
  index,
  onItemCheck,
  onItemShiftClick,
  tabId,
  currentSourceTabId,
  checked,
  provided,
  isDragging,
  idFieldName,
  numberOfChecked,
  columns,
  style,
  listRowClassName,
  disabled,
  getRowTooltipContent,
}: ListItemProps<TItem>) {
  const {element} = useContext(BemContext);

  const zIndexStack = useZIndexStack();
  const [zIndex, setZIndex] = useState<number | undefined>();
  useEffect(() => {
    if (isDragging) {
      const newZIndex = zIndexStack.obtain();
      setZIndex(newZIndex);
      return () => zIndexStack.release(newZIndex);
    }
  }, [zIndexStack, isDragging]);

  const draggableClasses = cn([element('draggable')], listRowClassName, {
    [element('draggable', 'active')]: isDragging,
    [element('draggable', 'ghosting')]:
      currentSourceTabId === tabId && !isDragging && checked,
  });

  const handleMouseDown = (e) => {
    e.preventDefault();
    if (e.shiftKey && onItemShiftClick) {
      onItemShiftClick({listItem, checked: !checked, index});
    } else {
      onItemCheck({listItem, checked: !checked, index});
    }
  };

  const showNumberOfChecked = isDragging && numberOfChecked > 1 && checked;
  const styles = {...provided.draggableProps.style, ...style, zIndex};

  return (
    <div
      className={draggableClasses}
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      style={styles}>
      {!disabled && (
        <div className={element('checkbox-cell')}>
          <div
            className={element('checkbox-wrap')}
            onMouseDown={handleMouseDown}
            data-testid={`checkbox-wrap-${listItem[idFieldName]}`}>
            <CheckBox
              onChange={stub}
              id={`checkbox_${listItem[idFieldName]}`}
              checked={checked}
              disabled={Boolean(listItem['disabled'])}
            />
          </div>
        </div>
      )}

      <Tooltip content={getRowTooltipContent?.(listItem)} placement="top">
        <div className={element('content')}>
          {columns.length ? (
            columns.map(({columnId, className, onRenderCell}) => {
              return (
                <div
                  key={columnId}
                  className={cn([element('content-cell')], className)}
                  tabIndex={-1}>
                  {onRenderCell(listItem[columnId], listItem)}
                </div>
              );
            })
          ) : (
            <div className={element('content-cell')} tabIndex={-1}>
              {listItem[idFieldName]}
            </div>
          )}
        </div>
      </Tooltip>

      {showNumberOfChecked && (
        <div className={element('number-of-checked')}>{numberOfChecked}</div>
      )}
    </div>
  );
}

export default ListItem;
