import React, {useContext, useCallback, Dispatch} from 'react';
import Tabs from '@jotunheim/react-tabs';
import {BemContext} from './BemContext';
import DragDropColumnsTab from './DragDropColumnsTab';
import {Action, setTab} from './hooks/actionCreators';
import {ListTypes} from '../utils/constants';
import {
  Column,
  ItemId,
  TabListItem,
  SearchCallback,
  Tab,
  TabId,
  GetRowTooltipCallback,
} from '../types';

const getTabById = ({tabs, selectedTabId}) => {
  return tabs.find(({tabId}) => tabId === selectedTabId);
};

type DragDropColumnsTabs<TItem extends TabListItem> = {
  tabs: Tab<TItem>[];
  selectedTabId: TabId;
  currentSourceTabId: TabId;
  listType: ListTypes;
  onSearch: SearchCallback;
  dispatch: Dispatch<Action>;
  idFieldName: string;
  checkedById: Record<ItemId, boolean>;
  showTabs: boolean;
  showSearch: boolean;
  columns: Column<TItem>[];
  listRowClassName?: string;
  virtualListEnabled: boolean;
  listHeight: number;
  virtualListItemHeight: number;
  disabled: boolean;
  getRowTooltipContent?: GetRowTooltipCallback<TItem>;
};

function DragDropColumnsTabs<TItem extends TabListItem>({
  tabs,
  selectedTabId,
  currentSourceTabId,
  listType,
  onSearch,
  dispatch,
  checkedById,
  idFieldName,
  showTabs,
  showSearch,
  columns,
  listRowClassName,
  virtualListEnabled,
  listHeight,
  virtualListItemHeight,
  disabled,
  getRowTooltipContent,
}: DragDropColumnsTabs<TItem>) {
  const {element} = useContext(BemContext);

  const handleTabClicked = useCallback(
    (tabId) => {
      dispatch(setTab({tabId, listType}));
    },
    [dispatch, listType]
  );

  const tabProps = {
    onSearch,
    currentSourceTabId,
    checkedById,
    dispatch,
    idFieldName,
    listType,
    showSearch,
    columns,
    listRowClassName,
    virtualListEnabled,
    listHeight,
    virtualListItemHeight,
    disabled,
    getRowTooltipContent,
  };

  return (
    <div className={element('column')}>
      {showTabs ? (
        <Tabs selectedTabId={selectedTabId} onTabClicked={handleTabClicked}>
          {tabs.map((tab) => {
            const {label, tabId} = tab;

            return (
              <Tabs.Tab title={label} key={tabId} id={tabId} hasPadding={false}>
                <DragDropColumnsTab {...tab} {...tabProps} />
              </Tabs.Tab>
            );
          })}
        </Tabs>
      ) : (
        <div id={selectedTabId + ''}>
          <DragDropColumnsTab
            {...getTabById({tabs, selectedTabId})}
            {...tabProps}
          />
        </div>
      )}
    </div>
  );
}

export default DragDropColumnsTabs;
