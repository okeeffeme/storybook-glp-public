import {ListTypes} from '../../utils/constants';
import {ActionTypes, Action} from './actionCreators';
import {selectInRange} from '../../utils/utils';
import {ItemId, MetaItem, TabId} from '../../types';

export const NO_PREVIOUS_CHECKED_VARIABLE = -1;

export type State = {
  checkedById: Record<ItemId, boolean>;
  selectedTabIdByListId: {
    [ListTypes.availableFields]: string;
    [ListTypes.selectedFields]: string;
  };
  currentSourceTabId: string;
  lastCheckedIndexByTabId: Record<TabId, number>;
};

export const getInitialState = ({
  firstColumnSelectedTabId,
  secondColumnSelectedTabId,
}) => {
  return {
    checkedById: {},
    selectedTabIdByListId: {
      [ListTypes.availableFields]: firstColumnSelectedTabId,
      [ListTypes.selectedFields]: secondColumnSelectedTabId,
    },
    currentSourceTabId: '',
    lastCheckedIndexByTabId: {},
  };
};

const getCheckedByIdAsObject = ({
  metaItems,
  checked,
}: {
  metaItems: MetaItem[];
  checked: boolean;
}) => {
  return metaItems.reduce((acc, item) => {
    !item.disabled && (acc[item.id] = checked);
    return acc;
  }, {});
};

const resetLastCheckedIndexes = (state: State) => {
  return Object.keys(state.lastCheckedIndexByTabId).reduce((acc, key) => {
    acc[key] = NO_PREVIOUS_CHECKED_VARIABLE;
    return acc;
  }, {});
};

const resetLastCheckedIndexByTabId = (state: State, action: Action) => {
  return {
    ...state.lastCheckedIndexByTabId,
    [action.payload.tabId]: NO_PREVIOUS_CHECKED_VARIABLE,
  };
};

const setLastCheckedIndexByTabId = (state: State, action: Action) => {
  return {
    ...state.lastCheckedIndexByTabId,
    [action.payload.tabId]: action.payload.index,
  };
};

const setCheckedById = (state: State, checkedById: Record<string, boolean>) => {
  return {
    ...state.checkedById,
    ...checkedById,
  };
};

export const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.checkItem:
      return {
        ...state,
        checkedById: setCheckedById(state, {
          [action.payload.id]: action.payload.checked,
        }),
        lastCheckedIndexByTabId: setLastCheckedIndexByTabId(state, action),
      };

    case ActionTypes.checkAllItems: {
      const checkedById = getCheckedByIdAsObject({
        metaItems: action.payload.metaItems,
        checked: action.payload.checked,
      });

      return {
        ...state,
        checkedById: setCheckedById(state, checkedById),
        lastCheckedIndexByTabId: resetLastCheckedIndexByTabId(state, action),
      };
    }

    case ActionTypes.shiftCheckItem: {
      const lastCheckedIndex =
        state.lastCheckedIndexByTabId[action.payload.tabId];

      let checkedById;

      if (
        lastCheckedIndex === undefined ||
        lastCheckedIndex === NO_PREVIOUS_CHECKED_VARIABLE
      ) {
        checkedById = {
          [action.payload.id]: true,
        };
      } else {
        checkedById = getCheckedByIdAsObject({
          metaItems: selectInRange(
            action.payload.metaItems,
            lastCheckedIndex,
            action.payload.index
          ),
          checked: true,
        });
      }

      return {
        ...state,
        checkedById: setCheckedById(state, checkedById),
        lastCheckedIndexByTabId: setLastCheckedIndexByTabId(state, action),
      };
    }

    case ActionTypes.setTab:
      return {
        ...state,
        selectedTabIdByListId: {
          ...state.selectedTabIdByListId,
          [action.payload.listType]: action.payload.tabId,
        },
      };

    case ActionTypes.resetCheck: {
      return {
        ...state,
        checkedById: {},
        lastCheckedIndexByTabId: resetLastCheckedIndexes(state),
      };
    }

    case ActionTypes.resetLastCheckedIndexes: {
      return {
        ...state,
        lastCheckedIndexByTabId: resetLastCheckedIndexByTabId(state, action),
      };
    }

    case ActionTypes.dragStart:
      return {
        ...state,
        currentSourceTabId: action.payload.currentSourceTabId,
      };

    case ActionTypes.dragEnd:
      return {
        ...state,
        currentSourceTabId: '',
      };

    default:
      return state;
  }
};
