import {ListTypes} from '../../utils/constants';
import {ItemId, MetaItem, TabId} from '../../types';
import {ToggleState} from '@jotunheim/react-toggle';

export enum ActionTypes {
  checkItem = 'checkItem',
  shiftCheckItem = 'shiftCheckItem',
  checkAllItems = 'checkAllItems',
  setTab = 'setTab',
  resetCheck = 'resetCheck',
  resetLastCheckedIndexes = 'resetLastCheckedIndexes',
  dragStart = 'dragStart',
  dragEnd = 'dragEnd',
}

/* TODO: good to implement reducer and actions types properly. Maybe use @reduxjs/toolkit.  */
/* eslint-disable @typescript-eslint/no-explicit-any */
function createAction<Payload = any>(type: ActionTypes) {
  return function (payload?: Payload) {
    return {
      type,
      payload,
    };
  };
}
/* eslint-enable @typescript-eslint/no-explicit-any */

type CheckItemPayload = {
  id: ItemId;
  checked: boolean;
  tabId: TabId;
  index: number;
};
export const checkItem = createAction<CheckItemPayload>(ActionTypes.checkItem);

type CheckAllItemsPayload = {
  metaItems: MetaItem[];
  checked?: boolean | ToggleState;
  tabId: TabId;
};
export const checkAllItems = createAction<CheckAllItemsPayload>(
  ActionTypes.checkAllItems
);

type ShiftCheckItemPayload = {
  id: ItemId;
  metaItems: MetaItem[];
  tabId: TabId;
  index: number;
};
export const shiftCheckItem = createAction<ShiftCheckItemPayload>(
  ActionTypes.shiftCheckItem
);

type SetTabPayload = {listType: ListTypes; tabId: TabId};
export const setTab = createAction<SetTabPayload>(ActionTypes.setTab);

export const resetCheck = createAction(ActionTypes.resetCheck);

type ResetLastCheckedIndexesPayload = {tabId: TabId};
export const resetLastCheckedIndexes =
  createAction<ResetLastCheckedIndexesPayload>(
    ActionTypes.resetLastCheckedIndexes
  );

type DragStartPayload = {currentSourceTabId: TabId};
export const dragStart = createAction<DragStartPayload>(ActionTypes.dragStart);

export const dragEnd = createAction(ActionTypes.dragEnd);

export type Action =
  | ReturnType<typeof checkItem>
  | ReturnType<typeof checkAllItems>
  | ReturnType<typeof shiftCheckItem>
  | ReturnType<typeof setTab>
  | ReturnType<typeof resetCheck>
  | ReturnType<typeof resetLastCheckedIndexes>
  | ReturnType<typeof dragStart>
  | ReturnType<typeof dragEnd>;
