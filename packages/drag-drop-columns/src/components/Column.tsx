import React from 'react';
import {RenderCellCallback, TabListItem} from '../types';

export type ColumnProps<TITem extends TabListItem> = {
  title: string;
  onRenderCell?: RenderCellCallback<TITem>;
  columnId: string;
  className?: string;
  headerClassName?: string;
};

// Need to declare props to have a proper type coverage.
// Can't use FunctionalComponent cause of generic
/* eslint-disable @typescript-eslint/no-unused-vars */
function Column<TITem>(props: ColumnProps<TITem>) {
  return null;
}
/* eslint-enable @typescript-eslint/no-unused-vars */

export default React.memo(Column);
