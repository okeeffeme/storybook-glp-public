import React, {useMemo, useCallback, Dispatch} from 'react';
import SearchField from '@jotunheim/react-search-field';
import {ToggleState} from '@jotunheim/react-toggle';
import {DraggableChildrenFn, Droppable} from 'react-beautiful-dnd';
import {
  checkItem,
  checkAllItems,
  shiftCheckItem,
  resetLastCheckedIndexes,
  Action,
} from './hooks/actionCreators';
import ListItem from './ListItem';
import List from './List';
import {
  Column,
  GetRowTooltipCallback,
  ItemId,
  SearchCallback,
  TabId,
  TabListItem,
} from '../types';

type DragDropColumnsTabProps<TItem extends TabListItem> = {
  items: TItem[];
  tabId: TabId;
  searchText?: string;
  onSearch: SearchCallback;
  idFieldName: string;
  showHeader?: boolean;
  currentSourceTabId: TabId;
  checkedById: Record<ItemId, boolean>;
  dispatch: Dispatch<Action>;
  searchPlaceholder?: string;
  showSearch: boolean;
  dropEnabled?: boolean;
  dragEnabled?: boolean;
  columns: Column<TItem>[];
  listRowClassName?: string;
  virtualListEnabled: boolean;
  listHeight: number;
  virtualListItemHeight: number;
  isLoading?: boolean;
  disabled: boolean;
  hideSelectAll?: boolean;
  noDataText?: string;
  getRowTooltipContent?: GetRowTooltipCallback<TItem>;
};

function DragDropColumnsTab<TItem extends TabListItem>({
  items,
  tabId,
  currentSourceTabId,
  checkedById,
  dispatch,
  showSearch,
  onSearch,
  searchPlaceholder,
  idFieldName,
  columns,
  listRowClassName,
  virtualListEnabled,
  listHeight,
  virtualListItemHeight,
  disabled,
  searchText = '',
  showHeader = false,
  dropEnabled = true,
  dragEnabled = true,
  isLoading = false,
  hideSelectAll = false,
  noDataText,
  getRowTooltipContent,
}: DragDropColumnsTabProps<TItem>) {
  const metaItems = useMemo(
    () =>
      items.map((item) => ({id: item[idFieldName], disabled: item.disabled})),
    [items, idFieldName]
  );

  const selectableItems = useMemo(
    () => items.filter((item) => !item.disabled),
    [items]
  );

  const allChecked = useMemo(
    () =>
      !!selectableItems.length &&
      selectableItems.every((element) => checkedById[element[idFieldName]]),
    [selectableItems, checkedById, idFieldName]
  );

  const numberOfChecked = useMemo(
    () =>
      items.reduce((acc, element) => {
        if (checkedById[element[idFieldName]]) {
          acc++;
        }
        return acc;
      }, 0),
    [items, checkedById, idFieldName]
  );

  const onCheckAll = useCallback(
    (checked?: boolean | ToggleState): void => {
      dispatch(
        checkAllItems({
          checked,
          tabId,
          metaItems,
        })
      );
    },
    [dispatch, tabId, metaItems]
  );

  const onItemCheck = useCallback(
    ({listItem, checked, index}) => {
      if (listItem.disabled) return;
      dispatch(checkItem({id: listItem[idFieldName], checked, tabId, index}));
    },
    [dispatch, idFieldName, tabId]
  );

  const onItemShiftClick = useCallback(
    ({listItem, index}) => {
      dispatch(
        shiftCheckItem({
          tabId,
          metaItems,
          index,
          id: listItem[idFieldName],
        })
      );
    },
    [dispatch, tabId, idFieldName, metaItems]
  );

  const handleSearch = useCallback(
    (value) => {
      onSearch({value, tabId});
      dispatch(resetLastCheckedIndexes({tabId}));
    },
    [tabId, onSearch, dispatch]
  );

  const listItemProps = {
    tabId,
    onItemCheck,
    onItemShiftClick,
    currentSourceTabId,
    numberOfChecked,
    columns,
    listRowClassName,
    idFieldName,
    isLoading,
    disabled,
    getRowTooltipContent,
  };

  const renderClone: DraggableChildrenFn = (provided, snapshot, rubric) => {
    const cloneProps = {
      listItem: items[rubric.source.index],
      index: rubric.source.index,
      checked: checkedById[rubric.draggableId] || false,
      provided,
      isDragging: snapshot.isDragging,
      ...listItemProps,
    };

    return <ListItem {...cloneProps} />;
  };

  const mode = virtualListEnabled ? 'virtual' : 'standard';

  const isDropDisabled = disabled || !dropEnabled;
  const isDragDisabled = disabled || !dragEnabled;

  return (
    <>
      {showSearch && (
        <SearchField
          placeholder={searchPlaceholder}
          onChange={handleSearch}
          value={searchText}
          minimizable={false}
          autoFocus={false}
        />
      )}

      <Droppable
        key={tabId}
        droppableId={tabId + ''}
        isDropDisabled={isDropDisabled}
        mode={mode}
        renderClone={renderClone}>
        {(droppableProvided) => {
          return (
            <List
              listHeight={listHeight}
              virtualListItemHeight={virtualListItemHeight}
              droppableProvided={droppableProvided}
              isDropDisabled={isDropDisabled}
              items={items}
              checkedById={checkedById}
              virtualListEnabled={virtualListEnabled}
              onCheckAll={onCheckAll}
              hideSelectAll={hideSelectAll}
              allChecked={allChecked}
              showHeader={showHeader}
              isDragDisabled={isDragDisabled}
              noDataText={noDataText}
              {...listItemProps}
            />
          );
        }}
      </Droppable>
    </>
  );
}

export default DragDropColumnsTab;
