import React, {ReactNode, useContext} from 'react';
import Icon, {
  minus,
  minusCircle,
  plus,
  plusCircle,
} from '@jotunheim/react-icons';
import {BemContext} from './BemContext';
import MiddleMenuButton from './MiddleMenuButton';

type MiddleMenuProps = {
  onAddAll?: () => void;
  onAddItems?: () => void;
  onRemoveAll?: () => void;
  onRemoveItems?: () => void;
  addAllText: ReactNode;
  addItemsText: ReactNode;
  removeAllText: ReactNode;
  removeItemsText: ReactNode;
  disabled: boolean;
};

const MiddleMenu = ({
  onAddAll,
  onAddItems,
  onRemoveAll,
  onRemoveItems,
  addAllText,
  addItemsText,
  removeAllText,
  removeItemsText,
  disabled,
}: MiddleMenuProps) => {
  const {element} = useContext(BemContext);
  return (
    <div className={element('middle-menu')}>
      {onAddAll && (
        <MiddleMenuButton
          onClick={onAddAll}
          title={addAllText}
          disabled={disabled}
          id="add_all_variables">
          <Icon path={plusCircle} />
        </MiddleMenuButton>
      )}

      {onAddItems && (
        <MiddleMenuButton
          onClick={onAddItems}
          title={addItemsText}
          disabled={disabled}
          id="add_variable">
          <Icon path={plus} />
        </MiddleMenuButton>
      )}

      {onRemoveItems && (
        <MiddleMenuButton
          onClick={onRemoveItems}
          title={removeItemsText}
          disabled={disabled}
          id="remove_variable">
          <Icon path={minus} />
        </MiddleMenuButton>
      )}

      {onRemoveAll && (
        <MiddleMenuButton
          onClick={onRemoveAll}
          title={removeAllText}
          disabled={disabled}
          id="remove_all_variables">
          <Icon path={minusCircle} />
        </MiddleMenuButton>
      )}
    </div>
  );
};

export default MiddleMenu;
