import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';

const {block, element, modifier} = bemFactory('co-drag-drop-columns');
export const BemContext = React.createContext({
  block,
  element,
  modifier,
});
