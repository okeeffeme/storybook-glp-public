import React, {MouseEventHandler, ReactNode, useContext} from 'react';
import {BemContext} from './BemContext';
import cn from 'classnames';

import {IconButton} from '@jotunheim/react-button';

type MiddleMenuButtonProps = {
  title: ReactNode;
  onClick: MouseEventHandler;
  children: ReactNode;
  id: string;
  disabled: boolean;
};

const MiddleMenuButton = ({
  title,
  onClick,
  children,
  id,
  disabled,
}: MiddleMenuButtonProps) => {
  const {element} = useContext(BemContext);
  const labelClass = cn(element('middle-menu-button-label'), {
    [element('middle-menu-button-label', 'disabled')]: disabled,
  });
  return (
    <React.Fragment>
      <div className={element('middle-menu-button')}>
        <IconButton
          onClick={onClick}
          id={id}
          disabled={disabled}
          data-testid={id}>
          {children}
        </IconButton>
      </div>
      <label htmlFor={id} className={labelClass}>
        {title}
      </label>
    </React.Fragment>
  );
};

export default MiddleMenuButton;
