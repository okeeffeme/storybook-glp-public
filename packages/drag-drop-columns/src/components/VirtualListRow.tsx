import React, {CSSProperties} from 'react';
import {Draggable} from 'react-beautiful-dnd';
import ListItem from './ListItem';
import {
  Column,
  ItemCheckCallback,
  ItemShiftCheckCallback,
  TabListItem,
  TabId,
  GetRowTooltipCallback,
} from '../types';

type VirtualListRowProps<TItem extends TabListItem> = {
  data: {
    idFieldName: string;
    isDragDisabled: boolean;
    items: TItem[];
    checkedById: Record<string, boolean>;
    tabId: TabId;
    onItemCheck: ItemCheckCallback<TItem>;
    onItemShiftClick: ItemShiftCheckCallback<TItem>;
    currentSourceTabId: TabId;
    numberOfChecked: number;
    columns: Column<TItem>[];
    listRowClassName?: string;
    disabled: boolean;
    getRowTooltipContent: GetRowTooltipCallback<TItem>;
  };
  index: number;
  style: CSSProperties;
};

function VirtualListRow<TItem extends TabListItem>({
  data: {idFieldName, isDragDisabled, items, checkedById, ...rest},
  index,
  style,
}: VirtualListRowProps<TItem>) {
  const listItem = items[index];

  return (
    <Draggable
      draggableId={listItem[idFieldName] + ''}
      index={index}
      key={listItem[idFieldName] + ''}
      isDragDisabled={isDragDisabled || listItem.disabled}>
      {(provided, snapshot) => (
        <ListItem
          listItem={listItem}
          provided={provided}
          isDragging={snapshot.isDragging}
          idFieldName={idFieldName}
          index={index}
          checked={checkedById[listItem[idFieldName]] || false}
          style={style}
          {...rest}
        />
      )}
    </Draggable>
  );
}

export default VirtualListRow;
