import React, {useContext, useMemo} from 'react';
import {BusyDots} from '@jotunheim/react-busy-dots';
import VirtualListRow from './VirtualListRow';
import {FixedSizeList as VirtualList} from 'react-window';
import cn from 'classnames';
import {BemContext} from './BemContext';
import ListHeader from './ListHeader';
import FullyRenderedList from './FullyRenderedList';
import {ToggleState} from '@jotunheim/react-toggle';
import {
  Column,
  ItemCheckCallback,
  ItemShiftCheckCallback,
  TabListItem,
  TabId,
  GetRowTooltipCallback,
} from '../types';
import {DroppableProvided} from 'react-beautiful-dnd';

type ListProps<TItem extends TabListItem> = {
  virtualListEnabled: boolean;
  items: TItem[];
  checkedById: Record<string, boolean>;
  listHeight: number;
  virtualListItemHeight: number;
  showHeader: boolean;
  onCheckAll: (checked?: boolean | ToggleState) => void;
  hideSelectAll: boolean;
  allChecked: boolean;
  droppableProvided: DroppableProvided;
  idFieldName: string;
  isDragDisabled: boolean;
  isDropDisabled: boolean;
  tabId: TabId;
  onItemCheck: ItemCheckCallback<TItem>;
  onItemShiftClick: ItemShiftCheckCallback<TItem>;
  currentSourceTabId: TabId;
  numberOfChecked: number;
  columns: Column<TItem>[];
  listRowClassName?: string;
  isLoading?: boolean;
  disabled: boolean;
  noDataText?: string;
  getRowTooltipContent?: GetRowTooltipCallback<TItem>;
};

function List<TItem extends TabListItem>({
  virtualListEnabled,
  items,
  checkedById,
  listHeight,
  virtualListItemHeight,
  showHeader,
  onCheckAll,
  hideSelectAll,
  allChecked,
  droppableProvided,
  idFieldName,
  isDragDisabled,
  isDropDisabled,
  tabId,
  onItemCheck,
  onItemShiftClick,
  currentSourceTabId,
  numberOfChecked,
  columns,
  listRowClassName,
  isLoading = false,
  disabled,
  noDataText,
  getRowTooltipContent,
}: ListProps<TItem>) {
  const {element} = useContext(BemContext);

  const itemData = useMemo(
    () => ({
      tabId,
      onItemCheck,
      onItemShiftClick,
      currentSourceTabId,
      numberOfChecked,
      columns,
      listRowClassName,
      idFieldName,
      isDragDisabled,
      items,
      checkedById,
      disabled,
      getRowTooltipContent,
    }),
    [
      tabId,
      onItemCheck,
      onItemShiftClick,
      currentSourceTabId,
      numberOfChecked,
      columns,
      listRowClassName,
      idFieldName,
      isDragDisabled,
      items,
      checkedById,
      disabled,
      getRowTooltipContent,
    ]
  );

  const droppableClassName = cn([element('droppable')], {
    [element('droppable', 'disabled')]: isDropDisabled,
  });

  return (
    <>
      {showHeader && (
        <ListHeader
          onCheck={onCheckAll}
          hideSelectAll={hideSelectAll}
          tabId={tabId}
          checked={allChecked}
          columns={columns}
          idFieldName={idFieldName}
          disabled={disabled}
        />
      )}
      {isLoading ? (
        <div className={element('loading-dots')}>
          <BusyDots data-testid="busy-dots" />
        </div>
      ) : (
        <div
          className={droppableClassName}
          {...droppableProvided.droppableProps}>
          {noDataText && items.length === 0 && (
            <div className={element('no-items')}>{noDataText}</div>
          )}
          {virtualListEnabled ? (
            <VirtualList
              height={listHeight}
              itemCount={items.length}
              itemSize={virtualListItemHeight}
              outerRef={droppableProvided.innerRef}
              itemData={itemData}>
              {VirtualListRow}
            </VirtualList>
          ) : (
            <FullyRenderedList
              height={listHeight}
              outerRef={droppableProvided.innerRef}
              placeholder={droppableProvided.placeholder}
              itemData={itemData}
            />
          )}
        </div>
      )}
    </>
  );
}

export default List;
