import React, {
  useReducer,
  useMemo,
  ReactNode,
  FunctionComponentElement,
} from 'react';
import {DragDropContext, DropResult} from 'react-beautiful-dnd';
import {bemFactory} from '@jotunheim/react-themes';
import {ListTypes} from '../utils/constants';
import {BemContext} from './BemContext';
import DragDropColumnsTabs from './DragDropColumnsTabs';
import MiddleMenu from './MiddleMenu';
import {getInitialState, reducer} from './hooks/reducer';
import {getCheckedItemIds} from '../utils/utils';
import {dragEnd, dragStart, resetCheck} from './hooks/actionCreators';

import {ColumnProps} from './Column';
import {Column, TabListItem, DragDropColumnsProps} from '../types';

import classNames from './DragDropColumns.module.css';

const {block, element, modifier} = bemFactory(
  'comd-drag-drop-columns',
  classNames
);

const noOp = () => {};

function onRenderCellDefault(value: ReactNode) {
  return <span>{value}</span>;
}

function convertToDefinition<TItem extends TabListItem>({
  props: {
    columnId,
    className,
    title,
    headerClassName,
    onRenderCell = onRenderCellDefault,
  },
}: FunctionComponentElement<ColumnProps<TItem>>): Column<TItem> {
  return {
    columnId,
    className,
    title,
    headerClassName,
    onRenderCell,
  };
}

function DragDropColumns<TItem>({
  listRowClassName,
  data: {firstColumnTabs, secondColumnTabs},
  onDragEnd = noOp,
  onSearch,
  children,
  idFieldName = 'label',
  onAddAll,
  onRemoveAll,
  onAddItems,
  onRemoveItems,
  getRowTooltipContent,
  addAllText = 'Add All',
  addItemsText = 'Add',
  removeAllText = 'Remove All',
  removeItemsText = 'Remove',
  showTabs = true,
  showSearch = true,
  virtualListEnabled = false,
  listHeight = 350,
  virtualListItemHeight = 30,
  disabled = false,
}: DragDropColumnsProps<TItem>) {
  const bemProps = useMemo(() => {
    return {block, element, modifier};
  }, []);

  const childrenArray = React.Children.toArray(
    children
  ) as FunctionComponentElement<ColumnProps<TItem>>[];

  const columns = childrenArray.map(convertToDefinition);

  const [state, dispatch] = useReducer(
    reducer,
    getInitialState({
      firstColumnSelectedTabId: firstColumnTabs[0].tabId,
      secondColumnSelectedTabId: secondColumnTabs[0].tabId,
    })
  );

  const onDragStart = (start) => {
    dispatch(
      dragStart({
        currentSourceTabId: start.source.droppableId,
      })
    );
  };

  const handleDragEnd = (dropResult: DropResult) => {
    const {source, destination, draggableId} = dropResult;

    if (!destination) {
      dispatch(dragEnd());
      return;
    }

    let draggableIds;
    // if current item is in checked list then we are in the multiple drag mode, get all checked items from the current tab.
    if (state.checkedById[draggableId]) {
      draggableIds = getCheckedItemIds({
        firstColumnTabs,
        secondColumnTabs,
        tabId: source.droppableId,
        state,
        idFieldName,
      });
    } else {
      draggableIds = [draggableId];
    }

    if (
      source.droppableId !== destination.droppableId &&
      state.checkedById[draggableId]
    ) {
      dispatch(resetCheck());
    }
    dispatch(dragEnd());

    onDragEnd({
      ...dropResult,
      payload: {draggableIds},
    });
  };

  const handleAddAll = () => {
    const tabId = state.selectedTabIdByListId[ListTypes.availableFields];
    dispatch(resetCheck());
    onAddAll && onAddAll({tabId});
  };

  const handleRemoveAll = () => {
    const tabId = state.selectedTabIdByListId[ListTypes.selectedFields];
    dispatch(resetCheck());
    onRemoveAll && onRemoveAll({tabId});
  };

  const handleAddItems = () => {
    const tabId = state.selectedTabIdByListId[ListTypes.availableFields];
    const draggableIds = getCheckedItemIds({
      firstColumnTabs,
      secondColumnTabs,
      tabId,
      state,
      idFieldName,
    });

    dispatch(resetCheck());
    onAddItems && onAddItems({draggableIds, tabId});
  };

  const handleRemoveItems = () => {
    const tabId = state.selectedTabIdByListId[ListTypes.selectedFields];
    const draggableIds = getCheckedItemIds({
      firstColumnTabs,
      secondColumnTabs,
      tabId,
      state,
      idFieldName,
    });

    dispatch(resetCheck());
    onRemoveItems && onRemoveItems({draggableIds, tabId});
  };

  const tabsProps = {
    onSearch,
    virtualListEnabled,
    idFieldName,
    dispatch,
    checkedById: state.checkedById,
    currentSourceTabId: state.currentSourceTabId,
    showTabs,
    columns,
    showSearch,
    listRowClassName,
    listHeight,
    virtualListItemHeight,
    disabled,
    getRowTooltipContent,
  };

  const middleMenuProps = {
    onAddAll: onAddAll && handleAddAll,
    onRemoveAll: onRemoveAll && handleRemoveAll,
    onAddItems: onAddItems && handleAddItems,
    onRemoveItems: onRemoveItems && handleRemoveItems,
    addAllText,
    addItemsText,
    removeAllText,
    removeItemsText,
    disabled,
  };

  return (
    <BemContext.Provider value={bemProps}>
      <DragDropContext onDragEnd={handleDragEnd} onDragStart={onDragStart}>
        <div className={block()}>
          <DragDropColumnsTabs
            {...tabsProps}
            tabs={firstColumnTabs}
            listType={ListTypes.availableFields}
            selectedTabId={
              state.selectedTabIdByListId[ListTypes.availableFields]
            }
          />

          <MiddleMenu {...middleMenuProps} />

          <DragDropColumnsTabs
            {...tabsProps}
            tabs={secondColumnTabs}
            listType={ListTypes.selectedFields}
            selectedTabId={
              state.selectedTabIdByListId[ListTypes.selectedFields]
            }
          />
        </div>
      </DragDropContext>
    </BemContext.Provider>
  );
}

export default DragDropColumns;
