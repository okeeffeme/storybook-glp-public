import {Tab, TabId, TabListItem} from '../types';
import {State} from '../components/hooks/reducer';

function getTabFromColumns<TItem extends TabListItem>({
  firstColumnTabs,
  secondColumnTabs,
  tabId,
}: {
  firstColumnTabs: Tab<TItem>[];
  secondColumnTabs: Tab<TItem>[];
  tabId: TabId;
}) {
  const tab = firstColumnTabs.find((tab) => tab.tabId === tabId);

  if (tab) {
    return tab;
  }

  return secondColumnTabs.find((tab) => tab.tabId === tabId);
}

export function getCheckedItemIds<TItem>({
  firstColumnTabs,
  secondColumnTabs,
  tabId,
  state,
  idFieldName,
}: {
  firstColumnTabs: Tab<TItem>[];
  secondColumnTabs: Tab<TItem>[];
  tabId: TabId;
  state: State;
  idFieldName: string;
}) {
  const tab = getTabFromColumns({
    firstColumnTabs,
    secondColumnTabs,
    tabId,
  });

  if (!tab) return [];

  return tab.items
    .map((item) => item[idFieldName])
    .filter((id) => state.checkedById[id]);
}

export const caseInsensitiveSearch = (where: string, what: string) => {
  return where.toLowerCase().indexOf(what.toLowerCase()) > -1;
};

export const selectInRange = (arr, begin, end) => {
  if (end < begin) {
    [end, begin] = [begin, end];
  }

  return arr.slice(begin, end + 1);
};
