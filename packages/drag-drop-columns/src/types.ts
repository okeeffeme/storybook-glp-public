import {FunctionComponentElement, ReactElement, ReactNode} from 'react';
import {DropResult} from 'react-beautiful-dnd';
import {ColumnProps} from './components/Column';

export type ItemId = string | number;
export type MetaItem = {id: ItemId; disabled?: boolean};

export type TabId = string | number;

export type TabListItem = {
  disabled?: boolean;
};

export type Tab<TItem extends TabListItem> = {
  // tab id. Should be unique through all tabs to let drag-n-drop component work correctly.
  tabId: TabId;
  label: string;
  items: TItem[];
  showHeader?: boolean;
  dragEnabled?: boolean;
  dropEnabled?: boolean;
  searchPlaceholder?: string;
  searchText?: string;
  isLoading?: boolean;
  hideSelectAll?: boolean;
  noDataText?: string;
};

export type Column<TItem extends TabListItem> = {
  columnId: string;
  className?: string;
  title: string;
  headerClassName?: string;
  onRenderCell: RenderCellCallback<TItem>;
};

/* eslint-disable @typescript-eslint/no-unused-vars */
export type ItemCheckCallback<TItem extends TabListItem> = ({
  listItem: TItem,
  checked: boolean,
  index: number,
}) => void;

export type ItemShiftCheckCallback<TItem extends TabListItem> = ({
  listItem: TItem,
  checked: boolean,
  index: number,
}) => void;
/* eslint-enable @typescript-eslint/no-unused-vars */

export type SearchCallback = ({
  tabId,
  value,
}: {
  tabId: TabId;
  value: string;
}) => void;

export type RenderCellCallback<TItem extends TabListItem> = (
  node: ReactNode,
  listItem: TItem
) => ReactElement;

export type DragEndCallback = (
  dropResult: DropResult & {payload: {draggableIds: string[]}}
) => void;

export type GetRowTooltipCallback<TItem extends TabListItem> = (
  listItem: TItem
) => ReactNode;

export type DragDropColumnsProps<TItem extends TabListItem> = {
  listRowClassName?: string;
  disabled?: boolean;
  data: {firstColumnTabs: Tab<TItem>[]; secondColumnTabs: Tab<TItem>[]};
  // On drag end, organize data and pass back in.
  onDragEnd?: DragEndCallback;
  // Functions for add and remove all or selected components. Used in middle menu.
  onAddAll?: ({tabId}: {tabId: TabId}) => void;
  onRemoveAll?: ({tabId}: {tabId: TabId}) => void;
  onAddItems?: ({
    tabId,
    draggableIds,
  }: {
    tabId: TabId;
    draggableIds: ItemId[];
  }) => void;
  onRemoveItems?: ({
    tabId,
    draggableIds,
  }: {
    tabId: TabId;
    draggableIds: ItemId[];
  }) => void;
  // Texts for middle menu buttons.
  addAllText?: ReactNode;
  addItemsText?: ReactNode;
  removeAllText?: ReactNode;
  removeItemsText?: ReactNode;
  // callback for search.
  onSearch: SearchCallback;
  // Id field. It should be unique through all variable lists to let drag-n-drop component work correctly.
  idFieldName?: string;
  // show tabs menu, if false - shows only 1st tab in each column
  showTabs?: boolean;
  showSearch?: boolean;
  // Enables virtual list support
  virtualListEnabled?: boolean;
  listHeight?: number;
  virtualListItemHeight?: number;
  getRowTooltipContent?: GetRowTooltipCallback<TItem>;
  children?:
    | FunctionComponentElement<ColumnProps<TItem>>
    | FunctionComponentElement<ColumnProps<TItem>>[];
};
