import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {boolean, text} from '@storybook/addon-knobs';

import {WaitButton} from '../src';
import {Tooltip} from '../../tooltip/src';

storiesOf('Components/wait-button', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('default', () => {
    return (
      <div style={{margin: '20px'}}>
        {Object.values(WaitButton.appearances).map((appearance) => {
          return (
            <WaitButton
              key={appearance}
              busy={boolean('busy', true)}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              href={text('href', '')}
              appearance={appearance}>
              {appearance}
            </WaitButton>
          );
        })}
      </div>
    );
  })
  .add('custom busyChildren', () => {
    return (
      <div style={{margin: '20px'}}>
        {Object.values(WaitButton.appearances).map((appearance) => {
          return (
            <WaitButton
              key={appearance}
              busy={boolean('busy', true)}
              busyChildren={text('busyChildren', 'Waiting')}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              href={text('href', '')}
              appearance={appearance}>
              {appearance}
            </WaitButton>
          );
        })}
      </div>
    );
  })
  .add('with tooltip', () => {
    return (
      <div style={{margin: '20px'}}>
        <Tooltip content={<div>With Tooltip</div>}>
          <WaitButton
            busy={boolean('busy', true)}
            fullWidth={boolean('fullWidth', false)}
            disabled={boolean('disabled', false)}
            href={text('href', '')}
            appearance={'primary-success'}>
            With Tooltip
          </WaitButton>
        </Tooltip>
      </div>
    );
  });
