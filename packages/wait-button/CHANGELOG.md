# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [7.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.15&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.15&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.13&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.14&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.12&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.13&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.11&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.12&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.10&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.11&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.9&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.10&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.8&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.9&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.7&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.8&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.6&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.7&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.5&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.6&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.4&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.5&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.3&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.4&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.2&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.3&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.0&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.2&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.1.0&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.1&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-wait-button

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.11&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.1.0&targetRepoId=1246) (2023-01-11)

### Features

- add test id for WaitButton ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([4919c14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4919c14037263c75a7ed3120b9ba62128f6ac615))

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.10&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.9&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.7&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.7&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.4&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.4&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.4&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.3&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.2&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@7.0.0&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-wait-button

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.11&sourceBranch=refs/tags/@jotunheim/react-wait-button@7.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of WaitButton ([NPM-964](https://jiraosl.firmglobal.com/browse/NPM-964)) ([10f2715](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/10f271564a5e1ce0f911a1597a7663d94d1219a2))
- remove default theme from WaitButton ([NPM-1077](https://jiraosl.firmglobal.com/browse/NPM-1077)) ([e3fe407](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e3fe4078135b21147390c6e59c3261196191ed97))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.10&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.9&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.8&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.6&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.3&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.2&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.1&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-wait-button

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-wait-button@6.0.0&sourceBranch=refs/tags/@jotunheim/react-wait-button@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-wait-button

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.43&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.44&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.42&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.43&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.41&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.42&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.40&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.41&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.39&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.40&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.37&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.38&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.36&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.37&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.35&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.36&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.34&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.35&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.33&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.34&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.32&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.33&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.29&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.30&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.28&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.29&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.27&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.28&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.26&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.27&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.25&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.26&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.24&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.25&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.23&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.24&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.22&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.23&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.20&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.21&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.19&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.20&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.18&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.19&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.17&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.18&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.16&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.17&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.15&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.16&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.14&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.15&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.13&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.14&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.12&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.13&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.11&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.12&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.10&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.11&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.9&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.10&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.8&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.9&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.7&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.8&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.6&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.7&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.5&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.6&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.4&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.5&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.3&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.4&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.2&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.3&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-wait-button

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@5.0.1&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.2&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-wait-button

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@4.0.0&sourceBranch=refs/tags/@confirmit/react-wait-button@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@3.2.1&sourceBranch=refs/tags/@confirmit/react-wait-button@4.0.0&targetRepoId=1246) (2020-10-21)

### Features

- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. ([NPM-549](https://jiraosl.firmglobal.com/browse/NPM-549)) ([ec92807](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec92807ea763c63d503963c3a3a42cedd6d863a6))

### BREAKING CHANGES

- meaning of the `type` prop has changed
- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. (NPM-549)

## [3.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-wait-button@3.2.0&sourceBranch=refs/tags/@confirmit/react-wait-button@3.2.1&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-wait-button

# [3.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-wait-button@3.1.1...@confirmit/react-wait-button@3.2.0) (2020-08-12)

### Features

- typescript support ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([41642c6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/41642c696dd568e67958111b262af0d58de54417))

## CHANGELOG

### v3.2.0

- Feat: `WaitButton` wrapped with forwardRef.

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v2.2.0

- Removing package version in class names.

### v2.1.0

- Feat: Add support for Button's `appearance` prop
- Refactor: Most classes are removed, as styles are not nescessary. Added some data-attributes for testing purposes

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.0.9

- Fix: Remove unused `WaitButton.sizes` values

### v1.0.0

- **BREAKING** - Removed props: baseClassName, classNames

### v0.0.1

- Initial version
