import WaitButton from './components/WaitButton';
import {Appearances, ButtonTypes} from '@jotunheim/react-button';

export {WaitButton, Appearances, ButtonTypes};
export default WaitButton;
