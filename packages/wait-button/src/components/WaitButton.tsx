import React, {forwardRef, MouseEventHandler, ReactNode, Ref} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {
  Appearances,
  Button,
  ButtonProps,
  ButtonTypes,
} from '@jotunheim/react-button';
import {BusyDots} from '@jotunheim/react-busy-dots';

import classNames from './WaitButton.module.css';

const {element} = bemFactory('comd-waitbutton', classNames);

export const WaitButton: ForwardRefWaitButton = forwardRef(function WaitButton(
  {busy = false, busyChildren, children, onClick, ...rest}: WaitButtonProps,
  ref: Ref<HTMLElement>
) {
  const handleClick: MouseEventHandler = (e) => {
    !busy && onClick && onClick(e);
  };

  return (
    <Button
      data-testid="wait-button"
      ref={ref}
      {...rest}
      onClick={handleClick}
      data-waitbutton-waiting={busy}>
      {busy ? (
        <span data-waitbutton-busy-text="">{busyChildren || children}</span>
      ) : (
        <span data-waitbutton-idle-text="">{children}</span>
      )}
      {busy && (
        <div className={element('dots')}>
          <BusyDots data-waitbutton-busy-dots="" />
        </div>
      )}
    </Button>
  );
}) as ForwardRefWaitButton;

type WaitButtonProps = {
  busy?: boolean;
  busyChildren?: ReactNode;
} & ButtonProps;

type ForwardRefWaitButton = React.ForwardRefExoticComponent<
  WaitButtonProps & React.RefAttributes<HTMLElement>
> & {
  appearances: typeof Appearances;
  types: typeof ButtonTypes;
};

WaitButton.types = Button.types;
WaitButton.appearances = Button.appearances;
WaitButton.displayName = 'WaitButton';

export default WaitButton;
