import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {WaitButton} from '../../src/components/WaitButton';

describe('WaitButton', () => {
  it('should render busyDots and busyText when busy is true', () => {
    render(<WaitButton busy>wait button</WaitButton>);

    const waitButton = screen.getByTestId('wait-button');

    expect(
      waitButton.querySelectorAll('[data-waitbutton-idle-text]').length
    ).toBe(0);
    expect(
      waitButton.querySelectorAll('[data-waitbutton-busy-dots]').length
    ).toBe(1);
    expect(
      waitButton.querySelectorAll('[data-waitbutton-busy-text]').length
    ).toBe(1);
  });

  it('should not render busyDots and busyText when busy is false', () => {
    render(<WaitButton>wait button</WaitButton>);

    const waitButton = screen.getByTestId('wait-button');

    expect(
      waitButton.querySelectorAll('[data-waitbutton-idle-text]').length
    ).toBe(1);
    expect(
      waitButton.querySelectorAll('[data-waitbutton-busy-dots]').length
    ).toBe(0);
    expect(
      waitButton.querySelectorAll('[data-waitbutton-busy-text]').length
    ).toBe(0);
  });

  it('should render custom busyChildren when busy is true', () => {
    render(
      <WaitButton busy busyChildren={<span data-custom-children>waiting</span>}>
        wait button
      </WaitButton>
    );

    const waitButton = screen.getByTestId('wait-button');

    expect(waitButton.querySelectorAll('[data-custom-children]').length).toBe(
      1
    );
  });

  it('should not render custom busyChildren when busy is false', () => {
    render(
      <WaitButton busyChildren={<span data-custom-children>waiting</span>}>
        wait button
      </WaitButton>
    );

    const waitButton = screen.getByTestId('wait-button');

    expect(waitButton.querySelectorAll('[data-custom-children]').length).toBe(
      0
    );
  });

  it('should call click whenever button is clicked', () => {
    const click = jest.fn();

    render(<WaitButton onClick={click}>wait button</WaitButton>);

    const waitButton = screen.getByTestId('wait-button');

    fireEvent.click(waitButton);

    expect(click).toBeCalled();
  });

  it('should not call click if button is busy', () => {
    const onClick = jest.fn();

    render(
      <WaitButton busy onClick={onClick}>
        wait button
      </WaitButton>
    );

    const waitButton = screen.getByTestId('wait-button');

    fireEvent.click(waitButton);

    expect(onClick).not.toBeCalled();
  });
});
