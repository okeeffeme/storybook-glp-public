import {Row} from 'react-table';
import {getMaxDepth} from '../../src/utils/getMaxDepth';

describe('Jotunheim React Table :: Utils/getMaxDepth', () => {
  it('should get max depths', () => {
    const flatRows = [
      {
        depth: 0,
      },
      {
        depth: 2,
      },
      {
        depth: 4,
      },
      {
        depth: 1,
      },
      {
        depth: 1,
      },
      {
        depth: 4,
      },
    ] as unknown as Row[];

    const result = getMaxDepth({flatRows});

    expect(result).toEqual(4);
  });

  it('should return 0 when flatRows has length of 0', () => {
    const flatRows = [];

    const result = getMaxDepth({flatRows});

    expect(result).toEqual(0);
  });
});
