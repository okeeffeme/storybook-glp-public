import {getPillText} from '../../src/utils/getPillText';
import {Language} from '../../src/types';
import {Operator} from '../../src/plugins/useColumnFilters/types';

describe('Jotunheim React Table :: Utils/getPillText', () => {
  const colName = 'ColName';
  const defaultFilter = {
    value: 1,
    operator: Operator.Equal,
  };

  it('should return correct values and operators', () => {
    const containsFilter = {...defaultFilter, operator: Operator.Contains};
    const emptyFilter = {operator: Operator.IsNull, valueDisabled: true};

    expect(getPillText(colName, defaultFilter, Language.EN)).toEqual(
      'ColName= 1'
    );
    expect(getPillText(colName, containsFilter, Language.EN)).toEqual(
      'ColName: 1'
    );
    expect(getPillText(colName, emptyFilter, Language.EN)).toEqual(
      'ColName: is empty'
    );
  });

  it('should return no value if valueDisabled', () => {
    const emptyFilter = {
      ...defaultFilter,
      valueDisabled: true,
    };
    expect(getPillText(colName, emptyFilter, Language.EN)).toEqual('ColName=');
  });

  it('should join values for match operators', () => {
    const matchFilter = {
      value: [{label: 'first value'}, {label: 'second value'}],
      operator: Operator.Match,
    };
    expect(getPillText(colName, matchFilter, Language.EN)).toEqual(
      'ColName: first value, second value'
    );
  });
});
