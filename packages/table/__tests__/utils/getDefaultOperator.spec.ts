import {getDefaultOperator} from '../../src/utils/getDefaultOperator';
import {ColumnFilterType, Operator} from '../../src';
import {FilterOperator} from '../../src/plugins/useColumnFilters/types';

describe('Jotunheim React Table :: Utils/getDefaultOperator', () => {
  const allowedOperators = [
    {operator: Operator.Contains},
    {operator: Operator.Match},
    {operator: Operator.Equal},
  ] as FilterOperator[];

  it('should default to contains operator', () => {
    expect(getDefaultOperator(undefined, allowedOperators)).toEqual(
      Operator.Contains
    );
  });

  it('should default to contains operator for text filters', () => {
    expect(getDefaultOperator(ColumnFilterType.Text, allowedOperators)).toEqual(
      Operator.Contains
    );
  });

  it('should return equals operator for numeric filters', () => {
    expect(
      getDefaultOperator(ColumnFilterType.Numeric, allowedOperators)
    ).toEqual(Operator.Equal);
  });

  it('should return match operator for multiselect filters', () => {
    expect(
      getDefaultOperator(ColumnFilterType.Multiselect, allowedOperators)
    ).toEqual(Operator.Match);
  });

  it('should default to the first operator when using custom list without default operator', () => {
    const customAllowedOperators = [
      {operator: Operator.InList},
      {operator: Operator.IsNotNull},
      {operator: Operator.EndWith},
    ] as FilterOperator[];
    expect(
      getDefaultOperator(ColumnFilterType.Text, customAllowedOperators)
    ).toEqual(Operator.InList);
  });

  it('should return undefined for date filter when customAllowedOperators are not defined in columns settings', () => {
    expect(getDefaultOperator(ColumnFilterType.Date, [])).toBeUndefined();
  });
});
