import {Option} from '../../src/plugins/useOptionsMenu/types';
import {getOrderedOptions} from '../../src/utils/getOrderedOptions';

const options: Option[] = [
  {command: 'edit', name: 'Edit'},
  {command: 'duplicate', name: 'Duplicate'},
  {command: 'reports', name: 'Reports'},
];

describe('Jotunheim React Table :: Utils/getOrderedOptions', () => {
  it('should return an empty array when given an empty optionsOrder array', () => {
    const result = getOrderedOptions(options, []);
    expect(result).toEqual([]);
  });

  it('should return an array of Option objects in the order specified by the optionsOrder array', () => {
    const optionsOrder = ['duplicate', 'reports', 'edit'];
    const result = getOrderedOptions(options, optionsOrder);
    expect(result).toEqual([
      {command: 'duplicate', name: 'Duplicate'},
      {command: 'reports', name: 'Reports'},
      {command: 'edit', name: 'Edit'},
    ]);
  });

  it('should exclude options that are not present in optionsByCommand', () => {
    const optionsOrder = ['duplicate', 'not_present', 'reports'];
    const result = getOrderedOptions(options, optionsOrder);
    expect(result).toEqual([
      {command: 'duplicate', name: 'Duplicate'},
      {command: 'reports', name: 'Reports'},
    ]);
  });
});
