import React from 'react';
import {render} from '@testing-library/react';
import Cell, {CellProps} from '../../../src/plugins/useExpandToggle/Cell';
import {menuRight} from '../../../../icons/src';

describe('useExpandToggle :: Cell', () => {
  const isRowExpandable = jest.fn();
  const props = {
    row: {getToggleRowExpandedProps: () => ({})},
    isRowExpandable,
  } as unknown as CellProps<Record<string, unknown>>;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders expand toggle button when the row is expandable', () => {
    isRowExpandable.mockReturnValue(true);

    const {getByRole, queryByTestId, container} = render(<Cell {...props} />);

    expect(getByRole('button')).toBeInTheDocument();
    expect(queryByTestId('table-expand-toggle-cell')).toBeInTheDocument();
    expect(queryByTestId('table-empty-toggle-cell')).not.toBeInTheDocument();
    expect(container.querySelector('svg > path')).toHaveAttribute(
      'd',
      menuRight
    );
  });

  it('renders an empty span when the row is un-expandable', () => {
    isRowExpandable.mockReturnValue(false);

    const {queryByRole, queryByTestId, container} = render(<Cell {...props} />);

    expect(queryByRole('button')).not.toBeInTheDocument();
    expect(queryByTestId('table-expand-toggle-cell')).not.toBeInTheDocument();
    expect(queryByTestId('table-empty-toggle-cell')).toBeInTheDocument();
    expect(container.querySelector('svg > path')).toBeNull();
  });
});
