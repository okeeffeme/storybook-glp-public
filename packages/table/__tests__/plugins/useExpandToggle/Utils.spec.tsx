import {calculateDrawerPadding} from '../../../src/plugins/useExpandToggle/utils';
import {Cell} from 'react-table';

describe('getSubRowPadding', () => {
  it('Should calculate correct padding if the table has selection and expand columns', () => {
    const cells = [
      {
        column: {id: 'selection-column'},
        row: {id: 1},
      },
      {
        column: {id: 'expand-column'},
        row: {id: 1},
      },
      {
        column: {id: 'id'},
        row: {id: 1},
      },
    ] as unknown as Cell[];

    const result = calculateDrawerPadding(cells);
    expect(result).toEqual('92px');
  });

  it('Should calculate correct padding if the table has expand column', () => {
    const cells = [
      {
        column: {id: 'expand-column'},
        row: {id: 1},
      },
      {
        column: {id: 'id'},
        row: {id: 1},
      },
    ] as unknown as Cell[];

    const result = calculateDrawerPadding(cells);
    expect(result).toEqual('52px');
  });

  it('Should calculate correct padding if the table has custom columns before expand column', () => {
    const cells = [
      {
        column: {id: 'icon-column'},
        row: {id: 1},
      },
      {
        column: {id: 'expand-column'},
        row: {id: 1},
      },
      {
        column: {id: 'id'},
        row: {id: 1},
      },
    ] as unknown as Cell[];

    const result = calculateDrawerPadding(cells);
    expect(result).toEqual('92px');
  });
});
