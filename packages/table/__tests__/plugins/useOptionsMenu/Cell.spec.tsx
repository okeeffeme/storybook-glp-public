import React from 'react';
import {Row} from 'react-table';
import {mount, shallow} from 'enzyme';
import {IconButton} from '../../../../button/src';
import Cell from '../../../src/plugins/useOptionsMenu/Cell';
import Dropdown from '../../../../dropdown/src';
import {chartBox, Icon, pencil, trashBin, wrench} from '../../../../icons/src';
import TableContext from '../../../src/components/TableContext';
import {Language, TableType} from '../../../src/types';

describe('useOptionMenu :: Cell', () => {
  it('should not use href in options in row without dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                command,
                name: command,
                iconPath: trashBin,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('href')
    ).toBeUndefined();
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('href')
    ).toBeUndefined();
    expect(
      wrapper
        .find(`[data-react-table-option="command3"]`)
        .find(IconButton)
        .prop('href')
    ).toBeUndefined();
  });

  it('should use href in options in row without dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                iconPath: trashBin,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('href')
    ).toBe('http://mysite/command1');
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('href')
    ).toBe('http://mysite/command2');
    expect(
      wrapper
        .find(`[data-react-table-option="command3"]`)
        .find(IconButton)
        .prop('href')
    ).toBe('http://mysite/command3');
  });

  it('should not use href in options in row with dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                command,
                name: command,
                iconPath: trashBin,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('href')
    ).toBeUndefined();
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('href')
    ).toBeUndefined();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .prop('href')
    ).toBeUndefined();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .prop('href')
    ).toBeUndefined();
  });

  it('should use href in options in row with dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                iconPath: trashBin,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('href')
    ).toBe(`http://mysite/command1`);
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('href')
    ).toBe(`http://mysite/command2`);
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .prop('href')
    ).toBe(`http://mysite/command3`);
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .prop('href')
    ).toBe(`http://mysite/command4`);
  });

  it('should render built-in Icon with passed iconPath in row without dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3'];
    const iconPaths = [trashBin, pencil, chartBox];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command, index) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                iconPath: iconPaths[index],
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(Icon)
        .prop('path')
    ).toBe(trashBin);
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(Icon)
        .prop('path')
    ).toBe(pencil);
    expect(
      wrapper
        .find(`[data-react-table-option="command3"]`)
        .find(Icon)
        .prop('path')
    ).toBe(chartBox);
  });

  it('should render built-in Icon with passed iconPath in row with dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const iconPaths = [trashBin, pencil, chartBox, wrench];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command, index) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                iconPath: iconPaths[index],
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(Icon)
        .prop('path')
    ).toBe(trashBin);
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(Icon)
        .prop('path')
    ).toBe(pencil);
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .prop('iconPath')
    ).toBe(chartBox);
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .prop('iconPath')
    ).toBe(wrench);
  });

  it('should render custom Icon in row without dropdown', () => {
    function CustomIcon() {}

    const allCommands = ['command1', 'command2', 'command3'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                icon: CustomIcon,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = shallow(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper.find(`[data-react-table-option="command1"]`).find(CustomIcon)
        .exists
    ).toBeTruthy();
    expect(
      wrapper.find(`[data-react-table-option="command2"]`).find(CustomIcon)
        .exists
    ).toBeTruthy();
    expect(
      wrapper.find(`[data-react-table-option="command3"]`).find(CustomIcon)
        .exists
    ).toBeTruthy();
  });

  it('should render custom Icon in row with dropdown', () => {
    function CustomIcon() {}

    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                href: `http://mysite/${command}`,
                command,
                name: command,
                icon: CustomIcon,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = shallow(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper.find(`[data-react-table-option="command1"]`).find(CustomIcon)
        .exists
    ).toBeTruthy();
    expect(
      wrapper.find(`[data-react-table-option="command2"]`).find(CustomIcon)
        .exists
    ).toBeTruthy();

    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .find(CustomIcon).exists
    ).toBeTruthy();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .find(CustomIcon).exists
    ).toBeTruthy();
  });

  it('should render options that are not disabled by default', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                command,
                name: command,
                iconPath: trashBin,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeUndefined();
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeUndefined();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .prop('disabled')
    ).toBeUndefined();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .prop('disabled')
    ).toBeUndefined();
  });

  it('should render disabled options', () => {
    const allCommands = ['command1', 'command2', 'command3'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                command,
                name: command,
                iconPath: trashBin,
                disabled: true,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-option="command3"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeTruthy();
  });

  it('should render disabled menu item in dropdown', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions() {
            return allCommands.map((command) => {
              return {
                command,
                name: command,
                iconPath: trashBin,
                disabled: true,
              };
            });
          },
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper
        .find(`[data-react-table-option="command1"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-option="command2"]`)
        .find(IconButton)
        .prop('disabled')
    ).toBeTruthy();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command3"]`)
        .prop('disabled')
    ).toBeTruthy();
    expect(
      menuWrapper
        .find(`[data-react-table-dropdown-option="command4"]`)
        .prop('disabled')
    ).toBeTruthy();
  });

  it('respects orderOptions when an overflow menu is needed', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions: () => {
            return [
              {
                name: 'command1',
                command: 'command1',
                iconPath: trashBin,
              },
              {
                name: 'command3',
                command: 'command3',
                iconPath: trashBin,
              },
            ];
          },
          optionsOrder: allCommands,
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper.find(
        `[data-react-table-option="command1"] + [data-react-table-empty-option="command2"]`
      )
    ).toBeTruthy();
    expect(
      menuWrapper.find(`[data-react-table-dropdown-option="command3"]`)
    ).toBeTruthy();
    expect(
      menuWrapper.find(`[data-react-table-dropdown-option="command4"]`)
    ).toHaveLength(0);
  });

  it('respects orderOptions when an overflow Menu is needed but NOT rendered', () => {
    const allCommands = ['command1', 'command2', 'command3', 'command4'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions: () => {
            return [
              {
                name: 'command1',
                command: 'command1',
                iconPath: trashBin,
              },
            ];
          },
          optionsOrder: allCommands,
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper.find(`[data-react-table-option="command1"]`).find(IconButton)
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-empty-option="command2"]`)
        .find(IconButton)
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-empty-option="overflow"]`)
        .find(IconButton)
    ).toBeTruthy();
  });

  it('respects orderOptions when an overflow Menu is NOT needed', () => {
    const allCommands = ['command1', 'command2'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions: () => {
            return [
              {
                name: 'command1',
                command: 'command1',
                iconPath: trashBin,
              },
            ];
          },
          optionsOrder: allCommands,
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(<Cell row={row} allCommands={allCommands} />);

    expect(
      wrapper
        .find(
          `[data-react-table-option="command1"] + [data-react-table-empty-option="command2"]`
        )
        .find(IconButton)
    ).toBeTruthy();
    expect(
      wrapper
        .find(`[data-react-table-empty-option="overflow"]`)
        .find(IconButton)
    ).toHaveLength(0);
  });

  it('should render all options in a dropdown when mobile context is provided', () => {
    const allCommands = ['command1', 'command2'];
    const row = {
      getOptionsMenuProps() {
        return {
          getOptions: () => {
            return [
              {
                name: 'command2',
                command: 'command2',
                iconPath: trashBin,
              },
            ];
          },
          optionsOrder: allCommands,
          onSelect() {},
        };
      },
    } as unknown as Row;
    const wrapper = mount(
      <TableContext.Provider
        value={{
          type: TableType.Mobile,
          language: Language.EN,
          hasToolbar: false,
          usePageScroll: false,
        }}>
        <Cell row={row} allCommands={allCommands} />
      </TableContext.Provider>
    );
    const menuWrapper = shallow(wrapper.find(Dropdown).prop('menu'));

    expect(
      wrapper.find(`[data-react-table-options-menu]`).find(IconButton)
    ).toBeTruthy();
    expect(
      menuWrapper
        .find(`[data-react-table-empty-option="command1"]`)
        .find(IconButton)
    ).toHaveLength(0);
    expect(
      menuWrapper
        .find(`[data-react-table-empty-option="command2"]`)
        .find(IconButton)
    ).toBeTruthy();
  });
});
