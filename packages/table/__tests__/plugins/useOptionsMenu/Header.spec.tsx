import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Header, {HeaderProps} from '../../../src/plugins/useOptionsMenu/Header';

describe('Header :: Options menu :: ', () => {
  it('should not render options menu if drawer used', () => {
    const props = {
      maxRowDepth: 1,
      toggleAllRowsExpanded: () => {},
      // eslint-disable-next-line react/display-name
      renderDrawer: ({id = 1, name = 'test'}) => (
        <div>
          {id} {name}
        </div>
      ),
    } as unknown as HeaderProps;

    const wrapper = render(<Header {...props} />);
    expect(
      wrapper.queryByTestId('table-options-menu-header')
    ).not.toBeInTheDocument();
  });

  it('should render options menu if drawer not used', () => {
    const props = {
      maxRowDepth: 1,
      toggleAllRowsExpanded: () => {},
      renderDrawer: null,
    } as unknown as HeaderProps;

    const wrapper = render(<Header {...props} />);

    expect(
      wrapper.getByTestId('table-options-menu-header')
    ).toBeInTheDocument();
  });

  it('should show sub-menu when header toggle is clicked', () => {
    // ARRANGE
    const props = {
      maxRowDepth: 1,
      toggleAllRowsExpanded: () => {},
      renderDrawer: null,
    } as unknown as HeaderProps;
    const wrapper = render(<Header {...props} />);

    expect(
      wrapper.queryByTestId('table-expand-all-rows')
    ).not.toBeInTheDocument();
    expect(
      wrapper.queryByTestId('table-collapse-all-rows')
    ).not.toBeInTheDocument();

    // ACT
    userEvent.click(wrapper.getByTestId('table-options-menu-trigger'));

    // ASSERT
    expect(wrapper.getByTestId('table-expand-all-rows')).toBeInTheDocument();
    expect(wrapper.getByTestId('table-collapse-all-rows')).toBeInTheDocument();
  });
});
