import {getAllCommands} from '../../../src/plugins/useOptionsMenu/getAllCommands';
import {Row} from 'react-table';
import {OptionsMenu} from '../../../src/plugins/useOptionsMenu/types';

const flatRows = [
  {
    id: 1,
  },
  {
    id: 2,
  },
  {
    id: 3,
  },
  {
    id: 4,
  },
] as unknown as Row[];

describe('Jotunheim React Table :: Utils/getAllCommands', () => {
  it('should get all commands without duplicates', () => {
    const optionsMenu = {
      getOptions: (rowId) => {
        switch (rowId) {
          case 1:
            return [
              {command: 'copy'},
              {command: 'duplicate'},
              {command: 'delete'},
            ];
          case 2:
            return [{command: 'copy'}, {command: 'delete'}];
          case 3:
            return [{command: 'copy'}, {command: 'replace'}];
          case 4:
            return [{command: 'copy'}, {command: 'delete'}];
        }
      },
    } as unknown as OptionsMenu;

    const result = getAllCommands({flatRows, optionsMenu});

    expect(result).toEqual(['copy', 'duplicate', 'delete', 'replace']);
  });

  it('should get all commands without duplicates if optionsOrder presented', () => {
    const optionsMenu = {
      getOptions: (rowId) => {
        switch (rowId) {
          case 1:
            return [
              {command: 'copy'},
              {command: 'duplicate'},
              {command: 'delete'},
            ];
          case 2:
            return [{command: 'copy'}, {command: 'delete'}];
          case 3:
            return [{command: 'copy'}, {command: 'replace'}];
          case 4:
            return [{command: 'copy'}, {command: 'delete'}];
        }
      },
      optionsOrder: ['copy', 'copy', 'duplicate', 'delete'],
    } as unknown as OptionsMenu;

    const result = getAllCommands({flatRows, optionsMenu});

    expect(result).toEqual(['copy', 'duplicate', 'delete']);
  });

  it('should get all commands with order according to "allOptions" property', () => {
    const optionsMenu = {
      optionsOrder: ['copy', 'replace', 'duplicate', 'delete'],
      getOptions: (rowId) => {
        switch (rowId) {
          case 1:
            return [
              {command: 'copy'},
              {command: 'duplicate'},
              {command: 'delete'},
            ];
          case 2:
            return [{command: 'copy'}, {command: 'delete'}];
          case 3:
            return [{command: 'copy'}, {command: 'replace'}];
          case 4:
            return [{command: 'copy'}, {command: 'delete'}];
        }
      },
    } as unknown as OptionsMenu;

    const result = getAllCommands({flatRows, optionsMenu});

    expect(result).toEqual(['copy', 'replace', 'duplicate', 'delete']);
  });

  it('should get all commands with order according to "allOptions" property even if "getOptions" is missing', () => {
    const optionsMenu = {
      optionsOrder: ['copy', 'replace', 'duplicate', 'delete'],
    } as unknown as OptionsMenu;

    const result = getAllCommands({flatRows, optionsMenu});

    expect(result).toEqual(['copy', 'replace', 'duplicate', 'delete']);
  });

  it('should return empty array if no options', () => {
    const optionsMenu = undefined;

    const result = getAllCommands({flatRows, optionsMenu});

    expect(result).toEqual([]);
  });
});
