import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  Table,
  CellAlignment,
  ColumnFilterType,
  Operator,
  QuickDate,
} from '../../src';
import Button from '../../../button/src';
import {Column} from 'react-table';

type TestData = {
  id: number | string;
  name: string;
  email?: string;
  subRows?: TestData[];
};

const columns: Column<TestData>[] = [
  {
    Header: 'ID',
    accessor: 'id',
  },
  {
    Header: 'Name',
    accessor: 'name',
    alignment: CellAlignment.Center,
  },
  {
    Header: 'Email',
    accessor: 'email',
    alignment: CellAlignment.Right,
  },
];

const buildData = (count) => {
  const result: TestData[] = [];
  for (let i = 1; i <= count; i++) {
    result.push({
      id: i,
      name: `test${i}`,
    });
  }
  return result;
};
const data = buildData(50);

const dataWithSubRows: TestData[] = [];
for (let i = 1; i < 51; i++) {
  dataWithSubRows.push({
    id: i,
    name: `test${i}`,
    subRows: [{id: `${i}.0`, name: `test${i} sub row`}],
  });
}

const prefix = 'data-react-table';
const selectors = {
  loading: `[${prefix}-loading]`,
  emptyState: `[${prefix}-empty-state]`,
  header: `[${prefix}-header]`,
  headerById: (id) => `[${prefix}-header-id="${id}"]`,
  headerTitleById: (id) =>
    `${selectors.headerById(id)} [data-react-table-header-title]`,
  columnFilterByHeaderId: (id) =>
    `${selectors.headerById(id)} [data-react-table-column-filter]`,
  columnFilterPopoverByHeaderId: (id) =>
    `[data-react-table-filter-dialog-id="${id}"]`,
  row: `[${prefix}-row]`,
  rowById: (id) => `[${prefix}-row-id="${id}"]`,
  cellByColumnId: (columnId) => `[${prefix}-cell-id="${columnId}"]`,
  checkboxHeader: `[${prefix}-checkbox-header]`,
  checkboxCell: (checked) =>
    `[${prefix}-checkbox-cell="${checked ? `checked` : `unchecked`}"]`,
  headerSortIcon: `[${prefix}-header-sort]`,
  optionsMenuButton: `[${prefix}-options-menu]`,
  optionMenuHeader: `[${prefix}-option-menu-header]`,
  expandToggleCell: (expanded) =>
    `[${prefix}-expand-toggle-cell="${
      expanded ? `expanded` : `collapsed`
    }"] IconButton`,
  pagerChip: `[data-locator="pager-range-label"]`,
  pagerNav: (type) => `button[data-locator="pager-${type}"]`,
  title: `comd-table-toolbar__title`,
  titleContainer: `comd-table-toolbar__title-container`,
  filterPill: `[${prefix}-filter-pill-id]`,
  filterPillById: (id) => `[${prefix}-filter-pill-id="${id}"]`,
  searchField: `.comd-table-toolbar__search-field`,
};

const pageScrollClassName = 'comd-table--page-level-scroll';

const renderContentOnly = (props) =>
  render(<Table content={<Table.Content {...props} />} />);

const renderDrawer = ({id = 1, name = 'test'}) => (
  <div>
    {id} {name}
  </div>
);

describe('Jotunheim React Table :: ', () => {
  it('sets correct className for usePageScroll enabled', () => {
    render(<Table usePageScroll={true} />);
    expect(screen.getByTestId('table')).toHaveClass(pageScrollClassName);
  });

  it('sets correct className for usePageScroll disabled', () => {
    render(<Table usePageScroll={false} />);
    expect(screen.getByTestId('table')).not.toHaveClass(pageScrollClassName);
  });

  it('should render title if set', () => {
    render(<Table toolbar={<Table.Toolbar title={'test'} />} />);
    expect(screen.getByText('test')).toBeInTheDocument();
  });

  it('should not render title if not set', () => {
    render(<Table toolbar={<Table.Toolbar>test</Table.Toolbar>} />);
    expect(screen.getByText('test')).not.toHaveClass(selectors.title);
  });

  it('should render children in title', () => {
    render(
      <Table
        toolbar={
          <Table.Toolbar
            titleChildren={<Button data-custom-button>text</Button>}>
            test
          </Table.Toolbar>
        }
      />
    );
    expect(screen.getByTestId('title-container')).toContainHTML('text');
  });

  it('should have searchAutoFocus prop', () => {
    render(
      <Table.Toolbar onSearch={() => {}} searchAutoFocus={true}>
        test
      </Table.Toolbar>
    );
    expect(screen.getByTestId('search-field')).toHaveAttribute(
      'data-autofocus',
      'true'
    );
  });

  it('should not have searchAutoFocus prop', () => {
    render(
      <Table.Toolbar onSearch={() => {}} searchAutoFocus={false}>
        test
      </Table.Toolbar>
    );
    expect(screen.getByTestId('search-field')).toHaveAttribute(
      'data-autofocus',
      'false'
    );
  });

  it('should reserve minimum height when showEmptyState is enabled', () => {
    renderContentOnly({
      columns,
      data,
    });
    expect(screen.getByRole('table')).toHaveClass(
      'comd-table__container--min-height'
    );
  });

  it('should not reserve minimum height for content when showEmptyState is disabled', () => {
    renderContentOnly({
      showEmptyState: false,
      columns,
      data,
    });

    expect(screen.getByRole('table')).not.toHaveClass(
      'comd-table__container--min-height'
    );
  });

  it('should truncate cell contents and show tooltip by default', () => {
    let truncate = false;
    let tooltip = false;

    renderContentOnly({
      columns,
      data,
    });
    const article = screen.getAllByRole('article');
    article.forEach((i) => {
      if (i.hasAttribute('data-table-cell-truncate')) truncate = true;
    });
    const tableHeader = screen.getAllByTestId('table-header');
    tableHeader.forEach((i) => {
      if (i.getElementsByClassName('comd-table__tooltip-target'))
        tooltip = true;
    });
    expect(truncate && tooltip);
  });

  it('should not truncate cell contents when turned off', () => {
    let truncate = false;
    let tooltip = false;

    const accessor = 'noTruncation';
    renderContentOnly({
      columns: [
        ...columns,
        {
          Header: 'test',
          accessor,
          suppressTruncation: true,
        },
      ],
      data,
    });
    const article = screen.getAllByRole('article');
    article.forEach((i) => {
      if (i.hasAttribute('data-table-cell-truncate')) truncate = true;
    });
    const tableHeader = screen.getAllByTestId('table-header');
    tableHeader.forEach((i) => {
      if (i.getElementsByClassName('comd-table__tooltip-target'))
        tooltip = true;
    });
    expect(!truncate && tooltip);
  });

  it('does not set className for page scroll if table type is mobile', () => {
    render(<Table usePageScroll={true} type={Table.TableType.Mobile} />);
    expect(screen.getByTestId('table')).toHaveClass('comd-table--mobile');
    expect(screen.getByTestId('table')).not.toHaveClass(pageScrollClassName);
  });

  it('should add padding class modifier by default', () => {
    render(<Table />);
    expect(screen.getByTestId('table')).toHaveClass('comd-table--padding');
  });
  it('should not add padding class modifier when hasPadding is false', () => {
    render(<Table hasPadding={false} />);
    expect(screen.getByTestId('table')).not.toHaveClass('comd-table--padding');
  });

  it('renders loading dots and headers when isLoading is true', () => {
    renderContentOnly({
      columns,
      data,
      isLoading: true,
    });
    expect(screen.getAllByTestId('table-header')).not.toHaveLength(0);
    expect(screen.getAllByTestId('busy-dots')).toHaveLength(1);
  });

  it('renders empty state component when there are no rows', () => {
    renderContentOnly({
      columns,
      data: [],
    });
    expect(screen.getAllByTestId('empty-state')[0]).toBeInTheDocument();
  });

  it('does not render empty state component when isLoading is true', () => {
    renderContentOnly({
      columns,
      data: [],
      isLoading: true,
    });

    expect(screen.queryByTestId('empty-state')).not.toBeInTheDocument();
  });

  it('renders correct number of columns and row data', () => {
    renderContentOnly({
      columns,
      data,
    });
    const header = screen.getAllByTestId('table-header');
    const row = screen.getAllByTestId('data-react-table-row');

    expect(header).toHaveLength(columns.length);
    expect(row).toHaveLength(data.length);
  });

  it('renders the correct headers and row data', () => {
    renderContentOnly({
      columns,
      data,
    });
    expect(screen.getByTestId('header-id')).toHaveTextContent('ID');
    expect(screen.getByTestId('header-name')).toHaveTextContent('Name');
    expect(screen.getByTestId('header-email')).toHaveTextContent('Email');

    const row = screen.getAllByTestId('data-react-table-row');
    for (let i = 0; i < row.length; i++) {
      expect(row[i]).toHaveTextContent(`test${i + 1}`);
    }
  });

  it('renders the correct header and cell alignments', () => {
    renderContentOnly({
      columns,
      data,
    });

    expect(screen.getByTestId('headertitle-id')).toHaveClass(
      'comd-table__flex-cell--align-left'
    );
    expect(screen.getByTestId('headertitle-name')).toHaveClass(
      'comd-table__flex-cell--align-center'
    );
    expect(screen.getByTestId('headertitle-email')).toHaveClass(
      'comd-table__flex-cell--align-right'
    );

    screen.getAllByTestId('column-id').forEach((i) => {
      expect(i).toHaveClass('comd-table__flex-cell--align-left');
    });
    screen.getAllByTestId('column-name').forEach((i) => {
      expect(i).toHaveClass('comd-table__flex-cell--align-center');
    });
    screen.getAllByTestId('column-email').forEach((i) => {
      expect(i).toHaveClass('comd-table__flex-cell--align-right');
    });
  });

  it('renders flex correctly on cells and headers depending on isFixedWidth', () => {
    renderContentOnly({
      columns: columns.map((c) => ({
        ...c,
        isFixedWidth: c.accessor === 'id',
      })),
      data,
    });

    expect(screen.getByTestId('header-id')).toHaveStyle('flex: 0 0 auto');

    screen.getAllByTestId('header-id').forEach((i) => {
      expect(i).toHaveStyle('flex: 0 0 auto');
    });

    screen.getAllByTestId('header-name').forEach((i) => {
      expect(i).not.toHaveStyle('flex: 0 0 auto');
    });
  });

  it('renders checkbox column when onSelect is defined and calls onSelect correctly', () => {
    const handleSelect = jest.fn();
    renderContentOnly({
      columns,
      data,
      onSelect: handleSelect,
    });
    expect(screen.getByTestId('checkbox_column')).toHaveAttribute(
      'data-react-table-checkbox-header',
      'true'
    );

    expect(screen.getAllByTestId('checkbox_cell')).toHaveLength(data.length);
    screen.getAllByTestId('checkbox_cell').forEach((i) => {
      expect(i).toHaveAttribute('data-react-table-checkbox-cell', 'unchecked');
    });
    const row = screen.getAllByTestId('input-checked')[0];
    userEvent.click(row);
    expect(handleSelect).toHaveBeenCalledTimes(1);
  });

  it('should call onSelect and stop propagation to onRowClick when both are defined', () => {
    const handleSelect = jest.fn();
    const onRowClick = jest.fn();
    renderContentOnly({
      columns,
      data,
      onSelect: handleSelect,
      onRowClick,
    });

    const row = screen.getAllByTestId('input-checked')[0];
    userEvent.click(row);
    expect(handleSelect).toHaveBeenCalledTimes(1);
    expect(onRowClick).toHaveBeenCalledTimes(0);
  });

  it('should include pre-selected rows in onSelect when selecting new row', () => {
    const preSelectedRowIds = ['3', '4'];
    const handleSelect = jest.fn((items) => Object.keys(items));

    renderContentOnly({
      columns,
      data,
      onSelect: handleSelect,
      defaultSelectedRows: preSelectedRowIds,
    });

    const row = screen.getAllByTestId('input-checked')[1];
    userEvent.click(row);
    expect(handleSelect).toHaveNthReturnedWith(1, ['1', '3', '4']);
  });

  it('renders new rows if data expand', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      data: [
        {
          id: 1,
          name: 'test_1',
          subRows: [
            {id: '1.0', name: 'test_1_0'},
            {id: '1.1', name: 'test_1_1'},
          ],
        },
        {
          id: 2,
          name: 'test_2',
        },
      ],
      onExpand: handleExpand,
    });

    expect(screen.getAllByTestId('data-react-table-row')).toHaveLength(2);
    const row = screen.getAllByRole('button')[1];
    userEvent.click(row);

    expect(screen.getAllByTestId('data-react-table-row')).toHaveLength(4);
  });

  it('re-renders row if data updates', () => {
    const data: TestData[] = [{id: 1, name: 'test_1'}];
    let props = {columns, data};
    const {rerender} = render(<Table.Content {...props} />);

    expect(screen.getByTestId('column-id')).toHaveTextContent('1');
    expect(screen.getByTestId('column-name')).toHaveTextContent('test_1');
    props = {columns, data: [{id: 2, name: 'test_2'}]};
    rerender(<Table.Content {...props} />);

    expect(screen.getByTestId('column-id')).toHaveTextContent('2');
    expect(screen.getByTestId('column-name')).toHaveTextContent('test_2');
  });

  it('should not trigger handleSelect again when changing data', () => {
    const handleSelect = jest.fn();

    const props = {
      columns,
      data,
      onSelect: handleSelect,
    };

    const {rerender} = render(<Table.Content {...props} />);

    const row = screen.getAllByTestId('input-checked')[0];
    userEvent.click(row);
    expect(handleSelect).toHaveBeenCalledTimes(1);
    props.data = buildData(10);
    rerender(<Table.Content {...props} />);

    expect(handleSelect).toHaveBeenCalledTimes(1);
  });

  it('renders expand toggle column when there are sub rows in the data and calls onExpand correctly', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      data: dataWithSubRows,
      onExpand: handleExpand,
    });
    expect(screen.getByTestId('table-options-menu-header')).toBeInTheDocument();
    expect(screen.getAllByTestId('table-expand-toggle-cell')).toHaveLength(
      data.length
    );

    const row = screen.getAllByRole('button')[1];
    userEvent.click(row);
    expect(handleExpand).toHaveBeenCalledTimes(1);
  });

  it('should not trigger onExpand when changing data', () => {
    const handleExpand = jest.fn();
    const props = {
      columns,
      data,
      onExpand: handleExpand,
    };

    const {rerender} = render(<Table.Content {...props} />);

    expect(handleExpand).toHaveBeenCalledTimes(0);
    props.data = buildData(10);
    rerender(<Table.Content {...props} />);

    expect(handleExpand).toHaveBeenCalledTimes(0);
  });

  it('renders expand toggle column when there are drawer callback in the content and calls onExpand correctly', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      data,
      // eslint-disable-next-line react/display-name
      renderDrawer,
    });

    expect(
      screen.queryByTestId('table-options-menu-header')
    ).not.toBeInTheDocument();
    expect(screen.getAllByTestId('table-expand-toggle-cell')).toHaveLength(
      data.length
    );

    const row = screen.getAllByRole('button')[1];
    userEvent.click(row);
    expect(handleExpand).toHaveBeenCalledTimes(1);
  });

  it('renders drawers correctly if row isExpanded', () => {
    const handleExpand = jest.fn();
    const data = buildData(2);
    const drawerPadding = '52px';
    const getExpectedInnerHtml = ({id, name}) => `<div>${id} ${name}</div>`;

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      data,
      // eslint-disable-next-line react/display-name
      renderDrawer,
      isRowExpandable: () => true,
    });

    screen.getAllByRole('button').forEach((i) => userEvent.click(i));

    const drawers = screen.getAllByTestId('expanded-drawer');

    const drawerOne = drawers[0];
    const drawerTwo = drawers[1];

    expect(drawerOne).toHaveStyle({paddingLeft: drawerPadding});
    expect(drawerOne).toContainHTML(getExpectedInnerHtml(data[0]));
    expect(drawerTwo).toHaveStyle({paddingLeft: drawerPadding});
    expect(drawerTwo).toContainHTML(getExpectedInnerHtml(data[1]));
  });

  it('should set autoResetExpanded prop to true if drawer set up', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      data,
      // eslint-disable-next-line react/display-name
      renderDrawer: ({id = 1, name = 'test'}) => (
        <div>
          {id} {name}
        </div>
      ),
    });
    expect(screen.getAllByTestId('container')[0]).toHaveAttribute(
      'data-autoResetExpanded',
      'true'
    );
  });

  it('should set autoResetExpanded prop to false if drawer set up and autoResetExpanded prop passed as false', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      data,
      autoResetExpanded: false,
      // eslint-disable-next-line react/display-name
      renderDrawer: ({id = 1, name = 'test'}) => (
        <div>
          {id} {name}
        </div>
      ),
    });
    expect(screen.getAllByTestId('container')[0]).toHaveAttribute(
      'data-autoResetExpanded',
      'false'
    );
  });

  it('should set autoResetExpanded prop to false by default for expansion rows', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      data: dataWithSubRows,
    });

    expect(screen.getAllByTestId('container')[0]).toHaveAttribute(
      'data-autoResetExpanded',
      'false'
    );
  });

  it('should set autoResetExpanded prop to true for expansion rows if autoResetExpanded passed', () => {
    const handleExpand = jest.fn();

    renderContentOnly({
      columns,
      onExpand: handleExpand,
      autoResetExpanded: true,
      data: dataWithSubRows,
      renderDrawer,
    });

    expect(screen.getAllByTestId('container')[0]).toHaveAttribute(
      'data-autoResetExpanded',
      'true'
    );
  });

  it('onSelect should return parent rows and their sub rows when selecting parent rows', () => {
    const handleExpand = jest.fn();
    const handleSelect = jest.fn();

    renderContentOnly({
      columns,
      data: dataWithSubRows,
      onExpand: handleExpand,
      onSelect: handleSelect,
    });

    const row = screen.getAllByTestId('input-checked');
    userEvent.click(row[1]);

    const handleSelectResult = handleSelect.mock.calls[0][0];
    expect(Object.values(handleSelectResult)).toHaveLength(2);
    expect(handleSelectResult['1'].id).toBe(1);
    expect(handleSelectResult['1.0'].id).toBe('1.0');
  });

  it('renders sort header when onSort is defined and calls onSort correctly', () => {
    const handleSort = jest.fn();
    renderContentOnly({
      columns,
      data,
      onSort: handleSort,
    });
    expect(screen.getAllByTestId('table-header')[0]).not.toHaveAttribute(
      selectors.headerSortIcon
    );
    userEvent.click(screen.getAllByTestId('table-header')[0]);
    expect(handleSort).toHaveBeenCalledTimes(1);
    expect(handleSort).toHaveBeenCalledWith({
      sortField: 'id',
      sortOrder: 'asc',
    });
  });

  it('renders pager when onPageChange is defined', () => {
    const handlePageChange = jest.fn();
    const pageSize = 20;
    const pageNumber = 1;
    const totalCount = data.length;

    renderContentOnly({
      columns,
      data,
      onPageChange: handlePageChange,
      totalCount,
      pageSize,
      pageNumber,
    });

    const pagerChip = screen.getByTestId('pager-range-id');
    const rangeStart = (pageNumber - 1) * pageSize + 1;
    const rangeEnd = rangeStart + pageSize - 1;
    expect(pagerChip).toHaveTextContent(
      `${rangeStart}-${rangeEnd} of ${totalCount}`
    );

    expect(screen.getByTestId('pager-first')).toHaveProperty('disabled', true);

    expect(screen.getByTestId('pager-previous')).toHaveProperty(
      'disabled',
      true
    );

    expect(screen.getByTestId('pager-next')).toHaveProperty('disabled', false);

    expect(screen.getByTestId('pager-last')).toHaveProperty('disabled', false);

    userEvent.click(screen.getByTestId('pager-next'));
    expect(handlePageChange).toHaveBeenCalledWith({
      pageIndex: 1,
      pageSize: 20,
      pageNumber: 2,
    });
  });

  it('does not render pager when onPageChange is not defined', () => {
    const totalCount = data.length;

    renderContentOnly({
      columns,
      data,
      totalCount,
    });
    expect(screen.queryByTestId('pager-range-id')).not.toBeInTheDocument();
  });

  it('renders sticky column on left', () => {
    renderContentOnly({
      columns: columns.map((column) => {
        if (column.Header === 'Name') {
          return {
            ...column,
            sticky: true,
          };
        }
        return column;
      }),
      data,
    });
    const firstHeader = screen.getByTestId('header-name');
    expect(firstHeader).toHaveTextContent('Name');
    expect(firstHeader).toHaveStyle({position: 'sticky', left: 0});
  });

  it('the first user-defined sticky column should be sticky on the right of selection column', () => {
    renderContentOnly({
      columns: columns.map((column) => {
        if (column.Header === 'Name') {
          return {
            ...column,
            sticky: true,
          };
        }
        return column;
      }),
      data,
      onSelect: () => {},
    });
    const selectionColumnHeader = screen.getAllByTestId(
      'header-selection-column'
    )[0];
    expect(selectionColumnHeader).toHaveAttribute(
      'data-react-table-header-id',
      'selection-column'
    );
    expect(selectionColumnHeader).toHaveStyle({position: 'sticky', left: 0});

    const nameColumnHeader = screen.getAllByTestId('header-name')[0];
    expect(nameColumnHeader).toHaveAttribute(
      'data-react-table-header-id',
      'name'
    );
    expect(nameColumnHeader).toHaveStyle({position: 'sticky', left: '40px'});
  });

  it('the first user-defined sticky column should be sticky on the right of selection and expansion column', () => {
    renderContentOnly({
      columns: columns.map((column) => {
        if (column.Header === 'Name') {
          return {
            ...column,
            sticky: true,
          };
        }
        return column;
      }),
      data: dataWithSubRows,
      onSelect: () => {},
    });

    const selectionColumnHeader = screen.getAllByTestId(
      'header-selection-column'
    )[0];
    expect(selectionColumnHeader).toHaveAttribute(
      'data-react-table-header-id',
      'selection-column'
    );
    expect(selectionColumnHeader).toHaveStyle({position: 'sticky', left: 0});

    const expandColumnHeader = screen.getAllByTestId('header-expand-column')[0];
    expect(expandColumnHeader).toHaveAttribute(
      'data-react-table-header-id',
      'expand-column'
    );
    expect(expandColumnHeader).toHaveStyle({position: 'sticky', left: '40px'});

    const nameColumnHeader = screen.getAllByTestId('header-name')[0];
    expect(nameColumnHeader).toHaveAttribute(
      'data-react-table-header-id',
      'name'
    );
    expect(nameColumnHeader).toHaveStyle({position: 'sticky', left: '80px'});
  });

  it('should apply class for highlighting selected row', () => {
    renderContentOnly({
      columns,
      data,
      onSelect: () => {},
    });

    const row = screen.getAllByTestId('input-checked')[0];

    expect(screen.getAllByTestId('data-react-table-row')[0]).not.toHaveClass(
      'comd-table__tbody-tr--selected'
    );

    userEvent.click(row);
    expect(screen.getAllByTestId('data-react-table-row')[0]).toHaveClass(
      'comd-table__tbody-tr--selected'
    );
  });

  it('should apply class for highlighting selected row with drawer', () => {
    renderContentOnly({
      columns,
      data,
      onSelect: () => {},
      renderDrawer,
      defaultExpandedRows: [1],
    });

    const row = screen.getAllByTestId('input-checked')[0];
    expect(screen.getAllByTestId('expanded-drawer')[0]).not.toHaveClass(
      'comd-table__tbody-tr--drawer comd-table__tbody-tr--selected'
    );

    userEvent.click(row);
    expect(screen.getAllByTestId('expanded-drawer')[0]).toHaveClass(
      'comd-table__tbody-tr--drawer comd-table__tbody-tr--selected'
    );
  });

  it('should apply custom cell style when getCellBackgroundColor is defined', () => {
    renderContentOnly({
      columns: [
        {
          Header: 'ID',
          accessor: 'id',
          getCellBackgroundColor: () => 'red',
        },
        {
          Header: 'Name',
          accessor: 'name',
          alignment: CellAlignment.Center,
        },
        {
          Header: 'Email',
          accessor: 'email',
          alignment: CellAlignment.Right,
        },
      ],
      data,
      onSelect: () => {},
      renderDrawer,
      defaultExpandedRows: [1],
    });
    const cell = screen.getAllByTestId('column-id');
    expect(cell[0]).toHaveStyle('background-color: red');
  });

  describe('Column filter', () => {
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        filterSettings: {
          type: ColumnFilterType.Numeric,
        },
      },
      {
        Header: 'Name',
        accessor: 'name',
        filterSettings: {
          type: ColumnFilterType.Text,
          operators: [{value: Operator.Equal, label: 'Equal'}],
        },
      },
      {
        Header: 'Email',
        accessor: 'email',
        filterSettings: {
          type: ColumnFilterType.Text,
        },
      },
      {
        Header: 'Date Created',
        accessor: 'dateCreated',
        filterSettings: {
          type: ColumnFilterType.Date,
        },
      },
      {
        Header: 'Choose One',
        accessor: 'chooseOne',
        filterSettings: {
          type: ColumnFilterType.Multiselect,
        },
      },
      {
        Header: 'Choose Many',
        accessor: 'chooseMany',
        filterSettings: {
          type: ColumnFilterType.Multiselect,
        },
      },
      {
        Header: 'Comment',
        accessor: 'comment',
      },
    ];

    const filterValues = [
      {
        id: 'id',
        operator: Operator.InList,
        type: ColumnFilterType.Numeric,
        value: 1,
      },
      {
        id: 'name',
        operator: Operator.Contains,
        type: ColumnFilterType.Text,
        value: 'abc',
      },
      {
        id: 'dateCreated',
        type: ColumnFilterType.Date,
        value: {
          optionId: QuickDate.Last7Days,
        },
      },
      {
        id: 'chooseOne',
        operator: Operator.Match,
        type: ColumnFilterType.Multiselect,
        value: [
          {
            label: 'Option 1',
            value: '1',
          },
          {
            label: 'Option 2',
            value: '2',
          },
        ],
      },
    ];

    const emptyData = [];
    const noFilterValues = [];

    it('should render column filters', () => {
      const onFilterApplied = jest.fn();
      const onFilterDeleted = jest.fn();

      renderContentOnly({
        columns,
        data: emptyData,
        onApplyFilter: onFilterApplied,
        onDeleteFilter: onFilterDeleted,
        filterValues: noFilterValues,
      });

      columns.forEach(({accessor, filterSettings}) => {
        expect(
          screen.queryAllByTestId('columnFilter-' + accessor)
        ).toHaveLength(filterSettings ? 1 : 0);
      });
    });

    it('should be able to apply filter from popover', () => {
      const onFilterApplied = jest.fn();

      renderContentOnly({
        columns,
        data: emptyData,
        onApplyFilter: onFilterApplied,
        onDeleteFilter: () => {},
        filterValues: filterValues,
      });
      const filterButton = screen.getAllByTestId('filter-button')[0];
      userEvent.click(filterButton);
      const filterPopover = screen.getAllByTestId('filter-popover');
      expect(filterPopover).toHaveLength(1);
      const doneButton = screen.getByTestId('done-button');
      userEvent.click(doneButton);
      expect(onFilterApplied).toBeCalledTimes(1);
      expect(onFilterApplied).toBeCalledWith(filterValues[0]);
    });

    it('should render filter pills', () => {
      renderContentOnly({
        columns,
        data: emptyData,
        onApplyFilter: () => {},
        onDeleteFilter: () => {},
        filterValues,
      });

      filterValues.forEach((filterValue) => {
        const filterPill = screen.getAllByTestId(
          'filterpill-' + filterValue.id
        );
        expect(filterPill).toHaveLength(1);
        const header = columns.find(
          (column) => column.accessor === filterValue.id
        )?.Header;

        let label;
        switch (filterValue.type) {
          case ColumnFilterType.Date:
            label = 'last 7 days';
            break;
          case ColumnFilterType.Multiselect:
            label = 'Option 1, Option 2';
            break;
          default:
            label = filterValue.value;
            break;
        }
        expect(filterPill[0]).toHaveTextContent(`${header}: ${label}`);
      });
    });

    it('should call onToggleFilter when toggling filter pill', () => {
      const onToggleFilter = jest.fn();
      renderContentOnly({
        columns,
        data: emptyData,
        onApplyFilter: () => {},
        onDeleteFilter: () => {},
        onToggleFilter,
        filterValues,
      });
      filterValues.forEach((filterValue, index) => {
        const actionButton = screen.getAllByTestId('action-button');
        userEvent.click(actionButton[index]);

        expect(onToggleFilter).toBeCalledTimes(index + 1);
        expect(onToggleFilter).toBeCalledWith(filterValue.id);
      });
    });

    it('should call onDeleteFilter when deleting filter pill', () => {
      const onDeleteFilter = jest.fn();
      renderContentOnly({
        columns,
        data: emptyData,
        onDeleteFilter,
        filterValues,
      });
      filterValues.forEach((filterValue, index) => {
        const deleteButton = screen.getAllByRole('button');
        userEvent.click(deleteButton[index]);
        expect(onDeleteFilter).toBeCalledTimes(index + 1);
        expect(onDeleteFilter).toBeCalledWith(filterValue.id);
      });
    });
  });
});
