import React from 'react';
import {render, screen} from '@testing-library/react';
import {ColumnFilterType, Operator} from '../../src';
import FilterBox from '../../src/plugins/useColumnFilters/views/FilterBox';
import {FilterOperator} from '../../src/plugins/useColumnFilters/types';

describe('Jotunheim React Table :: FilterBox', () => {
  const defaultProps = {
    columnId: 'textColumn',
    columnHeader: 'Text Column',
    filter: undefined,
    filterType: ColumnFilterType.Text,
    onApplyFilter: jest.fn(),
    onCancelFilter: jest.fn(),
    allowedFilterOperators: [
      {
        name: 'Contains',
        operator: Operator.Contains,
      },
      {
        name: 'Is empty',
        operator: Operator.IsNull,
        valueDisabled: true,
      },
      {
        name: 'Is not empty',
        operator: Operator.IsNotNull,
        valueDisabled: true,
      },
    ],
    quickDateOptions: [],
    options: undefined,
  };

  it('should disable button on invalid or empty value', () => {
    render(
      <FilterBox
        {...defaultProps}
        filter={{
          id: 'textColumn',
          operator: Operator.Contains,
          type: ColumnFilterType.Text,
          value: undefined,
        }}
      />
    );
    expect(screen.getByTestId('done-button')).toBeDisabled();
  });

  it('should enable button on valid value', () => {
    render(
      <FilterBox
        {...defaultProps}
        filter={{
          id: 'textColumn',
          operator: Operator.Contains,
          type: ColumnFilterType.Text,
          value: 'valid value',
        }}
      />
    );
    expect(screen.getByTestId('done-button')).not.toBeDisabled();
  });

  it('should allow valueDisabled operators without a value', () => {
    const {rerender} = render(
      <FilterBox
        {...defaultProps}
        filter={{
          id: 'textColumn',
          operator: Operator.IsNull,
          type: ColumnFilterType.Text,
          value: undefined,
        }}
      />
    );
    expect(screen.getByTestId('done-button')).not.toBeDisabled();
    rerender(
      <FilterBox
        {...defaultProps}
        filter={{
          id: 'textColumn',
          operator: Operator.IsNotNull,
          type: ColumnFilterType.Text,
          value: undefined,
        }}
      />
    );
    expect(screen.getByTestId('done-button')).not.toBeDisabled();
  });

  it('should use default operator', () => {
    render(<FilterBox {...defaultProps} />);
    expect(screen.getByTestId('text-and-number-filter')).toHaveAttribute(
      'data-operator',
      Operator.Contains
    );
  });

  it('should use first operator in custom list if default is not present', () => {
    render(
      <FilterBox
        {...defaultProps}
        allowedFilterOperators={
          [
            {
              name: 'Ends With',
              operator: Operator.EndWith,
            },
            {
              name: 'Equal',
              operator: Operator.Equal,
            },
          ] as FilterOperator[]
        }
      />
    );
    expect(screen.getByTestId('text-and-number-filter')).toHaveAttribute(
      'data-operator',
      Operator.EndWith
    );
  });
});
