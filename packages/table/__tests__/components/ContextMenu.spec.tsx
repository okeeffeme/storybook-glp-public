import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {Table} from '../../src';

const columns = [
  {
    Header: 'ID',
    accessor: 'id',
  },
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Email',
    accessor: 'email',
  },
];

type TestData = {
  id: number;
  name: string;
};

const buildData = (count: number) => {
  const result: TestData[] = [];
  for (let i = 1; i <= count; i++) {
    result.push({
      id: i,
      name: `test${i}`,
    });
  }
  return result;
};
const data = buildData(50);

const getOptionsMenu = ({onSelect, enableContextMenu = false}) => ({
  getOptions: () => [
    {
      name: 'Edit',
      command: 'edit',
    },
    {
      name: 'Duplicate',
      command: 'duplicate',
    },
  ],
  onSelect,
  enableContextMenu,
});

const renderContentOnly = (props) =>
  render(<Table content={<Table.Content {...props} />} />);

describe('Jotunheim React Table Context Menu :: ', () => {
  it('should render context menu if enableContextMenu defined and handle option click properly', () => {
    const onSelect = jest.fn();
    renderContentOnly({
      columns,
      data,
      optionsMenu: getOptionsMenu({onSelect, enableContextMenu: true}),
    });

    expect(screen.queryAllByTestId('react-table-context-menu')).toBeTruthy();
    expect(screen.queryAllByTestId('data-react-table-row')).toBeTruthy();

    fireEvent.contextMenu(screen.queryAllByTestId('data-react-table-row')[0]);

    const editOption = screen.getByText('Edit');
    expect(editOption).toBeInTheDocument();
    fireEvent.click(editOption);

    expect(onSelect).toHaveBeenCalledWith('edit', data[0]);
  });

  it('should not render context menu if no option enableContextMenu defined', () => {
    const onSelect = jest.fn();

    renderContentOnly({
      columns,
      data,
      optionsMenu: getOptionsMenu({onSelect}),
    });

    expect(
      screen.queryAllByTestId('react-table-context-menu').length
    ).toBeFalsy();
    expect(screen.queryAllByTestId('data-react-table-row')).toBeTruthy();
  });
});
