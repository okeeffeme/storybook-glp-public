import React from 'react';
import {action} from '@storybook/addon-actions';
import {TextField} from '../../text-field/src';
import DatePicker from '../../date-picker/src';
import {Select} from '../../select/src';
import {CellAlignment, Operator, ColumnFilterType} from '../src';
import {pencil, contentDuplicate, printer, trashBin} from '../../icons/src';
import {Column} from 'react-table';
import {OptionsMenu} from '../src/plugins/useOptionsMenu/types';

export const buildColumns = ({
  columnSettings,
  stickyColumns = [],
  suppressHeaderTruncation = false,
}: {
  columnSettings: string[];
  stickyColumns?: string[];
  suppressHeaderTruncation?: boolean;
}) =>
  [
    columnSettings.includes('Id') && {
      Header: 'Id',
      accessor: 'id',
      sticky: stickyColumns.includes('Id'),
      width: 100,
      filterSettings: {
        type: ColumnFilterType.Numeric,
        operators: [
          {value: Operator.LessThan, label: 'Less than'},
          {value: Operator.GreaterThan, label: 'Greater than'},
          {value: Operator.LessThanOrEqual, label: 'Less than or equal'},
        ],
      },
    },
    columnSettings.includes('First Name') && {
      Header: 'First Name',
      accessor: 'firstName',
      sticky: stickyColumns.includes('First Name'),
      disableTruncation: true,
      filterSettings: {
        type: ColumnFilterType.Text,
      },
      Cell: function Cell({cell}) {
        return <div style={{fontWeight: 600}}>{cell.value}</div>;
      },
    },
    columnSettings.includes('Last Name') && {
      Header: 'Last Name Very Very Very Very Long Header',
      accessor: 'lastName',
      sticky: stickyColumns.includes('Last Name'),
      filterSettings: {
        type: ColumnFilterType.Text,
      },
      suppressHeaderTruncation,
    },
    columnSettings.includes('Age') && {
      Header: 'Age',
      sticky: stickyColumns.includes('Age'),
      accessor: 'age',
      filterSettings: {
        type: ColumnFilterType.Numeric,
      },
      width: 100,
      alignment: CellAlignment.Right,
    },
    columnSettings.includes('Status') && {
      Header: 'Status',
      accessor: 'status',
      suppressTooltip: true,
      sticky: stickyColumns.includes('Status'),
    },
    columnSettings.includes('City Picker') && {
      Header: 'City Picker',
      accessor: 'city',
      disableSortBy: true,
      suppressTooltip: true,
      sticky: stickyColumns.includes('City Picker'),
      Cell() {
        const [value, setValue] = React.useState(null);
        return (
          <Select
            placeholder="Please select a value"
            onChange={setValue}
            value={value}>
            <Select.Option value="1">Vancouver</Select.Option>
            <Select.Option value="2">Oslo</Select.Option>
            <Select.Option value="3">London</Select.Option>
          </Select>
        );
      },
      width: 200,
    },
    columnSettings.includes('Text Field') && {
      Header: 'Text Field',
      accessor: 'textField',
      disableSortBy: true,
      sticky: stickyColumns.includes('Text Field'),
      Cell() {
        const [value, setValue] = React.useState('');
        return (
          <TextField
            placeholder={'Enter Text in Field'}
            onChange={setValue}
            value={value}
          />
        );
      },
    },
    columnSettings.includes('Date Picker') && {
      Header: 'Date Picker',
      accessor: 'date',
      disableSortBy: true,
      sticky: stickyColumns.includes('Date Picker'),
      filterSettings: {
        type: ColumnFilterType.Date,
        quickDateOptions: [{label: 'last 10 days', id: 'last10days'}],
      },
      Cell() {
        return <DatePicker />;
      },
    },
  ].filter(Boolean) as Column<TestDataItem>[];

export type TestDataItem = {
  id: string;
  firstName: string;
  lastName: string;
  age: string;
  status: string;
  subRows?: TestDataItem[];
};

const makeSubRows = (
  subRowDepth: number,
  prefix: string
): TestDataItem[] | undefined => {
  if (!subRowDepth || subRowDepth === 0) return undefined;

  return [
    {
      id: `${prefix}.0`,
      firstName: 'John',
      lastName: 'Doe',
      age: '20',
      status: 'Offline',
      subRows: makeSubRows(subRowDepth - 1, `${prefix}.0`),
    },
    {
      id: `${prefix}.1`,
      firstName: 'Hello',
      lastName: 'World',
      age: '20',
      status: 'Online',
      subRows: makeSubRows(subRowDepth - 1, `${prefix}.1`),
    },
  ];
};

const makeData = (
  len: number,
  subRowDepth: number,
  objects: Omit<TestDataItem, 'id'>[]
) => {
  const array: TestDataItem[] = [];
  for (let i = 0; i < len; i++) {
    objects.forEach((obj) => {
      if (array.length < len) {
        const id = String(array.length + 1);
        array.push({
          id,
          subRows: makeSubRows(subRowDepth, id),
          ...obj,
        });
      }
    });
  }

  return array;
};

export const buildRows = ({
  numberOfRows,
  subRowDepth,
}: {
  numberOfRows: number;
  subRowDepth: number;
}) =>
  makeData(numberOfRows, subRowDepth, [
    {
      firstName: 'Captain',
      lastName: 'Reindeer very very very very very long string',
      age: '21',
      status: 'Online',
    },
    {
      firstName: 'Patrick',
      lastName: 'Kane',
      age: '30',
      status: 'Busy',
    },
    {
      firstName: 'Russell',
      lastName: 'Wilson',
      age: '31',
      status: 'Online',
    },
  ]);

const options = {
  Duplicate: {
    command: 'duplicate',
    name: 'Duplicate',
    iconPath: contentDuplicate,
    href: '/duplicate',
  },
  Print: {
    command: 'print',
    name: 'Print',
    iconPath: printer,
  },
  Edit: {
    command: 'edit',
    name: 'Edit',
    iconPath: pencil,
    href: '/edit',
  },
  Delete: {
    command: 'delete',
    name: 'Delete',
    disabled: true,
    iconPath: trashBin,
  },
};

const buildOptionsOrder = (useOptionsOrder) =>
  useOptionsOrder ? ['duplicate', 'print', 'edit', 'delete'] : undefined;

export const buildOptionsMenu = <D extends Record<string, unknown>>(
  optionMenuItems,
  useOptionsOrder
): OptionsMenu<D> => ({
  onSelect: action('onSelectMenuItem'),
  optionsOrder: buildOptionsOrder(useOptionsOrder),
  getOptions: () => {
    return optionMenuItems.map((option) => options[option]);
  },
});

const applyFilterOperator = (operator, rowValue, filterValue) => {
  const formatFilterValue =
    typeof filterValue === 'string' ? filterValue.toLowerCase() : filterValue;
  const formatRowValue =
    typeof rowValue === 'string' ? rowValue.toLowerCase() : rowValue;

  if (operator === Operator.InRange) {
    const [start, end] = filterValue.split('-');
    return rowValue >= +start && rowValue <= +end;
  }

  switch (operator) {
    case Operator.Equal:
      return formatRowValue == formatFilterValue;
    case Operator.LessThan:
      return formatRowValue < +formatFilterValue;
    case Operator.GreaterThan:
      return formatRowValue > +formatFilterValue;
    case Operator.GreaterThanOrEqual:
      return formatRowValue >= +formatFilterValue;
    case Operator.LessThanOrEqual:
      return formatRowValue <= +formatFilterValue;
    case Operator.IsNull:
      return !formatRowValue;
    case Operator.IsNotNull:
      return (
        formatRowValue !== undefined ||
        formatRowValue !== null ||
        formatRowValue !== ''
      );
    case Operator.Contains:
      return formatRowValue.includes(formatFilterValue);
    case Operator.StartWith:
      return formatRowValue.startsWith(formatFilterValue);
    case Operator.EndWith:
      return formatRowValue.endsWith(formatFilterValue);
    case Operator.InList:
      return filterValue.split(' ').some((value) => formatRowValue === +value);
    default:
      return formatRowValue === formatFilterValue;
  }
};

export const matchedFilters = (filters, row) => {
  for (const filter of filters) {
    if (
      !filter.idle &&
      !applyFilterOperator(filter.operator, row[filter.id], filter.value)
    ) {
      return false;
    }
  }
  return true;
};
