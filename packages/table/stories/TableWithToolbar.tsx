import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  array,
  boolean,
  number,
  optionsKnob as options,
} from '@storybook/addon-knobs';
import {
  columnDefaults,
  columnValues,
  menuOptions,
  menuOptionsDefaults,
} from './constants';
import {
  buildColumns,
  buildOptionsMenu,
  buildRows,
  matchedFilters,
  TestDataItem,
} from './utils';
import chunk from 'lodash/chunk';
import isEqual from 'lodash/isEqual';
import Table, {
  ColumnOption,
  Language,
  TableType,
  FilterValue,
  SortOrder,
} from '../src';
import Button, {IconButton} from '../../button/src';
import Icon, {pencil, trashBin, viewColumn} from '../../icons/src';
import SplitButton from '../../split-button/src';

const TAKE = 20;
export const defaultExpandedRowIds = array(
  'Default expanded row ids',
  ['1', '1.1', '5', '10'],
  '\n'
);

const TableWithToolbar = ({type}) => {
  const columnSettings = options('Columns', columnValues, columnDefaults, {
    display: 'multi-select',
  }) as unknown as string[];
  const stickyColumns = options('Sticky Column', columnValues, 'Id', {
    display: 'multi-select',
  }) as unknown as string[];
  const optionMenuItems = options('Options', menuOptions, menuOptionsDefaults, {
    display: 'multi-select',
  }) as unknown as string[];
  const useOptionsOrder = boolean('Use Options Order', true);

  const numberOfRows = number('Number of rows', 30);
  const numberOfRowsPerPage = number('Number of rows per page', 20);
  const subRowDepth = number('Depth of Sub Rows', 1);
  const enablePagination = boolean('Enable pagination', true);
  const enableSelection = boolean('Enable row selection', true);
  const enableSorting = boolean('Enable sorting', true);
  const showToolbar = boolean('Show toolbar', true);
  const showOptionsMenu = boolean('Show options menu', true);
  const showEmptyState = boolean('Show empty state', true);
  const usePageScroll = boolean('Page level scroll', false);
  const isSearchDisabled = boolean('Is search disabled', false);
  const enableFiltering = boolean('Enable filtering', true);
  const suppressHeaderTruncation = boolean('Suppress header truncation', false);

  const columns = useMemo(
    () =>
      buildColumns({columnSettings, stickyColumns, suppressHeaderTruncation}),
    [columnSettings, stickyColumns, suppressHeaderTruncation]
  );

  const [rows, setRows] = useState(() => {
    return buildRows({numberOfRows, subRowDepth});
  });

  useEffect(() => {
    setRows(buildRows({numberOfRows, subRowDepth}));
  }, [numberOfRows, subRowDepth]);

  const optionsMenu = useMemo(
    () => buildOptionsMenu(optionMenuItems, useOptionsOrder),
    [optionMenuItems, useOptionsOrder]
  );

  const searchColumnOptions: ColumnOption[] = useMemo(
    () =>
      columns
        .filter((column) =>
          ['First Name', 'Age'].includes(column.Header as string)
        )
        .map((column) => ({
          value: String(column.accessor),
          label: String(column.Header),
        })),
    [columns]
  );

  const batchOperationOptions = useMemo(
    () => [
      {
        value: 'edit',
        label: 'Edit',
        iconPath: pencil,
      },
      {
        value: 'delete',
        label: 'Delete',
        iconPath: trashBin,
      },
    ],
    []
  );

  const [sortField, setSortField] = useState<
    Exclude<keyof TestDataItem, 'subRows'> | undefined
  >(undefined);
  const [sortOrder, setSortOrder] = useState<SortOrder | undefined>(undefined);
  const [pageNumber, setPageNumber] = useState(1);
  const [searchParams, setSearchParams] = useState<{
    field?: string;
    value?: string;
  }>({
    field: searchColumnOptions[0].value as string,
  });
  const [loadedCount, setLoadedCount] = useState(numberOfRows);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isMoreLoading, setIsMoreLoading] = useState<boolean>(false);
  const [filters, setFilters] = useState<FilterValue[]>([]);

  const handleApplyFilter = useCallback(
    (newFilter: FilterValue) => {
      setIsLoading(true);
      setTimeout(() => {
        const existingFilter = filters.find(({id}) => id === newFilter.id);
        if (existingFilter) {
          setFilters((filterValues) =>
            filterValues.map((filter) =>
              filter.id === newFilter.id ? newFilter : filter
            )
          );
        } else {
          setFilters((filterValues) => [newFilter, ...filterValues]);
        }
        setIsLoading(false);
      }, 1000);
    },
    [filters, setFilters]
  );

  const handleFilterToggle = useCallback(
    (id) => {
      setFilters((filters) => {
        return filters.map((filter) => ({
          ...filter,
          isIdle: filter.id === id ? !filter.isIdle : filter.isIdle,
        }));
      });
    },
    [setFilters]
  );

  const handleFilterDelete = useCallback(
    (id) => {
      setFilters((filters) => filters.filter((filter) => filter.id !== id));
    },
    [setFilters]
  );

  const handleSort = useCallback(({sortField, sortOrder} = {}) => {
    setIsLoading(true);
    setTimeout(() => {
      setSortField(sortField);
      setSortOrder(sortOrder);
      setIsLoading(false);
    }, 1000);
  }, []);

  const handlePageChange = useCallback(({pageIndex}) => {
    setIsLoading(true);
    setTimeout(() => {
      setPageNumber(pageIndex + 1);
      setIsLoading(false);
    }, 1000);
  }, []);

  const handleSearch = useCallback((value: string, field?: string) => {
    setIsLoading(true);
    setSearchParams({value, field});
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  const handleSelect = useCallback(() => {}, []);

  const handleLoadMore = useCallback(() => {
    setIsMoreLoading(true);
    setTimeout(() => {
      setLoadedCount(loadedCount + TAKE);
      setIsMoreLoading(false);
    }, 1000);
  }, [loadedCount]);

  const onRowClick = useCallback(() => {
    console.log('row clicked');
  }, []);

  useEffect(() => {
    // mock fetch
    let newRowData =
      type === TableType.Mobile
        ? buildRows({numberOfRows: loadedCount, subRowDepth})
        : buildRows({numberOfRows, subRowDepth});

    const sorted = sortField
      ? newRowData.sort((a: TestDataItem, b: TestDataItem) => {
          if (a[sortField] > b[sortField]) return sortOrder === 'desc' ? 1 : -1;
          if (a[sortField] < b[sortField]) return sortOrder === 'desc' ? -1 : 1;
          return 0;
        })
      : newRowData;

    const filterRawValue = (newRowData, filters) => {
      const results: TestDataItem[] = [];

      for (const row of newRowData) {
        if (matchedFilters(filters, row)) {
          results.push({
            ...row,
            subRows: row.subRows.filter((row) => {
              return matchedFilters(filters, row);
            }),
          });
        } else {
          row.subRows.forEach((row) => {
            if (matchedFilters(filters, row)) {
              results.push(row);
            }
          });
        }
      }

      return results;
    };
    const filtered =
      filters && filters.length > 0
        ? filterRawValue(sorted, filters)
        : newRowData;

    if (type === TableType.Default) {
      newRowData = chunk(filtered, numberOfRowsPerPage)[
        pageNumber ? pageNumber - 1 : 0
      ];
    }

    const searchResult = newRowData
      ? newRowData.filter((row) => {
          return (
            !searchParams ||
            !searchParams.value ||
            !searchParams.value.length ||
            row[searchParams.field as string] === searchParams.value
          );
        })
      : [];

    if (!isEqual(searchResult, rows)) {
      setRows(searchResult);
    }
  }, [
    numberOfRows,
    sortField,
    sortOrder,
    rows,
    numberOfRowsPerPage,
    pageNumber,
    searchParams,
    loadedCount,
    type,
    subRowDepth,
    filters,
  ]);

  return (
    <div
      style={{
        height: '90vh',
        width: type === TableType.Mobile ? 500 : 'auto',
      }}>
      <Table
        hasPadding={boolean('hasPadding', true)}
        type={type}
        key={defaultExpandedRowIds.join('')} //should be remounted, if user changes defaultExpanded prop via storybook
        usePageScroll={usePageScroll}
        language={Language.EN}
        toolbar={
          showToolbar && (
            <Table.Toolbar
              title="Table"
              count={numberOfRows}
              isSearchDisabled={isSearchDisabled}
              batchOperationOptions={batchOperationOptions}
              onBatchOperationSelect={() => {}}
              batchOperationPlaceholder="Apply to selected"
              columnOptions={searchColumnOptions}
              onSearch={handleSearch}
              searchValue={searchParams.value}
              searchColumn={searchParams.field}
              searchPlaceholder="Search">
              <IconButton>
                <Icon path={viewColumn} />
              </IconButton>
              <SplitButton button={<Button>Additional button</Button>}>
                <SplitButton.MenuItem>Menu text item</SplitButton.MenuItem>
                <SplitButton.MenuItem href="#">
                  Another action
                </SplitButton.MenuItem>
              </SplitButton>
            </Table.Toolbar>
          )
        }
        content={
          <Table.Content
            data={rows}
            columns={columns}
            defaultExpandedRows={defaultExpandedRowIds}
            optionsMenu={showOptionsMenu ? optionsMenu : undefined}
            onSelect={enableSelection ? handleSelect : undefined}
            onSort={enableSorting ? handleSort : undefined}
            onApplyFilter={enableFiltering ? handleApplyFilter : undefined}
            onToggleFilter={enableFiltering ? handleFilterToggle : undefined}
            onDeleteFilter={enableFiltering ? handleFilterDelete : undefined}
            onRowClick={onRowClick}
            filterValues={filters}
            sortField={sortField}
            sortOrder={sortOrder}
            {...(type === TableType.Mobile
              ? {onLoadMore: handleLoadMore}
              : {
                  onPageChange: enablePagination ? handlePageChange : undefined,
                  pageSize: numberOfRowsPerPage,
                  pageNumber: pageNumber,
                  totalCount: numberOfRows,
                })}
            isLoading={isLoading}
            isMoreLoading={isMoreLoading}
            showEmptyState={showEmptyState}
          />
        }
      />
    </div>
  );
};

export default TableWithToolbar;
