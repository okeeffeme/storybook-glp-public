import {DataItemWithSubRows} from './types';

export const columnValues = {
  id: 'Id',
  firstName: 'First Name',
  textField: 'Text Field',
  lastName: 'Last Name',
  age: 'Age',
  status: 'Status',
  date: 'Date Picker',
  city: 'City Picker',
};

export const columnDefaults = [
  'Id',
  'First Name',
  'Text Field',
  'Last Name',
  'Age',
  'Status',
  'Date Picker',
  'City Picker',
];

export const menuOptions = {
  duplicate: 'Duplicate',
  print: 'Print',
  edit: 'Edit',
  delete: 'Delete',
};

export const sampleData: DataItemWithSubRows[] = [
  {
    id: '1',
    name: 'Michael',
  },
  {
    id: '2',
    name: 'Scottie',
    subRows: [
      {id: 'nested-2-1', name: 'Charles'},
      {id: 'nested-2-2', name: 'Willy'},
    ],
  },
  {
    id: '3',
    name: 'Barney',
  },
  {
    id: '4',
    name: 'Chris',
  },
];

export const menuOptionsDefaults = ['Duplicate', 'Edit'];
