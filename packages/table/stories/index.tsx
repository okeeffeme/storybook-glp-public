import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {Column} from 'react-table';
import {storiesOf} from '@storybook/react';
import {boolean, optionsKnob as options} from '@storybook/addon-knobs';

import moment from 'moment';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {
  columnDefaults,
  menuOptions,
  menuOptionsDefaults,
  sampleData,
} from './constants';
import {buildColumns, buildOptionsMenu, buildRows, TestDataItem} from './utils';

import TableWithToolbar, {defaultExpandedRowIds} from './TableWithToolbar';
import Table, {
  Operator,
  QuickDate,
  ColumnFilterType,
  CustomDate,
  FilterValue,
} from '../src';
import {
  pencil,
  chartBox,
  briefcase,
  contentDuplicate,
  wrench,
} from '../../icons/src';
import {Dropdown} from '../../dropdown/src';
import Button from '../../button/src';
import TextField from '../../text-field/src';
import Tabs from '../../tabs/src';
import {DateFilterValue} from '../src/plugins/useColumnFilters/types';
import {
  DataItem,
  DataItemWithColumnFiltering,
  DataItemWithSubRows,
} from './types';

storiesOf('Components/table', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Basic', () => {
    const [data, setData] = useState<DataItem[]>([
      {
        id: '1',
        firstName: 'Michael',
        points: 0,
        extraColumn1:
          'really long string really long string really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
      {
        id: '2',
        firstName: 'Scottie',
        points: 0,
        extraColumn1: 'really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
    ]);

    const columns: Column<DataItem>[] = useMemo(
      () => [
        {
          Header: 'Id',
          accessor: 'id',
          width: 40,
          isFixedWidth: true,
        },
        {
          Header: 'First name',
          accessor: 'firstName',
        },
        {
          Header: 'Extra Column 1',
          accessor: 'extraColumn1',
        },
        {
          Header: 'Extra Column 2',
          accessor: 'extraColumn2',
        },
        {
          Header: 'Extra Column 3',
          accessor: 'extraColumn3',
        },
        {
          Header: 'Points',
          accessor: 'points',
          width: 100,
          isFixedWidth: true,
          Cell: function Cell({row}) {
            return (
              <TextField
                type="number"
                value={row.original.points}
                onChange={(value) => {
                  setData((d) =>
                    d.map((player) => {
                      if (player.id === row.original.id) {
                        return {
                          ...player,
                          points: value,
                        };
                      }

                      return player;
                    })
                  );
                }}
              />
            );
          },
        },
      ],
      []
    );

    const optionsMenu = useMemo(
      () => ({
        getOptions() {
          return [
            {
              name: 'Reports',
              command: 'reports',
              menu: (
                <Dropdown.Menu>
                  <Dropdown.MenuItem href={'/reports/1'}>
                    Report 1
                  </Dropdown.MenuItem>
                  <Dropdown.MenuItem href={'/reports/2'}>
                    Report 2
                  </Dropdown.MenuItem>
                </Dropdown.Menu>
              ),
              iconPath: chartBox,
            },
            {
              name: 'Edit',
              command: 'edit',
              iconPath: pencil,
            },
            {
              name: 'Duplicate',
              command: 'duplicate',
              iconPath: contentDuplicate,
            },
            {
              name: 'Other actions',
              command: 'actions',
              menu: (
                <Dropdown.Menu>
                  <Dropdown.MenuItem>Move up</Dropdown.MenuItem>
                  <Dropdown.MenuItem>Move down</Dropdown.MenuItem>
                </Dropdown.Menu>
              ),
              iconPath: wrench,
            },
          ];
        },
        onSelect: (command) => {
          console.log(command);
        },
      }),
      []
    );

    return (
      <div style={{height: '90vh'}}>
        <div>
          <Button
            onClick={() => {
              setData([...data]);
            }}>
            Re-render with same data
          </Button>
        </div>

        <Table
          content={
            <Table.Content
              columns={columns}
              data={data}
              optionsMenu={optionsMenu}
            />
          }
        />
      </div>
    );
  })
  .add('With context menu', () => {
    const [data, setData] = useState([
      {
        id: '1',
        firstName: 'Michael',
        points: 0,
        extraColumn1:
          'really long string really long string really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
      {
        id: '2',
        firstName: 'Scottie',
        points: 0,
        extraColumn1: 'really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
      {
        id: '3',
        firstName: 'Third',
        points: 0,
        extraColumn1: 'a',
        extraColumn2: 'b',
        extraColumn3: 'c',
      },
      {
        id: '4',
        firstName: 'Forth',
        points: 0,
        extraColumn1: 'a',
        extraColumn2: 'b',
        extraColumn3: 'c',
      },
    ]);

    const columns: Column<DataItem>[] = useMemo(
      () => [
        {
          Header: 'Id',
          accessor: 'id',
          width: 40,
          isFixedWidth: true,
        },
        {
          Header: 'First name',
          accessor: 'firstName',
        },
        {
          Header: 'Extra Column 1',
          accessor: 'extraColumn1',
        },
        {
          Header: 'Extra Column 2',
          accessor: 'extraColumn2',
        },
        {
          Header: 'Extra Column 3',
          accessor: 'extraColumn3',
        },
        {
          Header: 'Points',
          accessor: 'points',
          width: 100,
          isFixedWidth: true,
          Cell: function Cell({row}) {
            return (
              <TextField
                type="number"
                value={row.original.points}
                onChange={(value) => {
                  setData((d) =>
                    d.map((player) => {
                      if (player.id === row.original.id) {
                        return {
                          ...player,
                          points: value,
                        };
                      }

                      return player;
                    })
                  );
                }}
              />
            );
          },
        },
      ],
      []
    );

    const optionsMenu = useMemo(
      () => ({
        getOptions() {
          return [
            {
              name: 'Reports',
              command: 'reports',
              menu: (
                <Dropdown.Menu>
                  <Dropdown.MenuItem href={'/reports/1'}>
                    Report 1
                  </Dropdown.MenuItem>
                  <Dropdown.MenuItem href={'/reports/2'}>
                    Report 2
                  </Dropdown.MenuItem>
                </Dropdown.Menu>
              ),
              iconPath: chartBox,
            },
            {
              name: 'Edit',
              command: 'edit',
              iconPath: pencil,
            },
            {
              name: 'Duplicate',
              command: 'duplicate',
              iconPath: contentDuplicate,
            },
            {
              name: 'Other actions',
              command: 'actions',
              menu: (
                <Dropdown.Menu>
                  <Dropdown.MenuItem onClick={() => alert('move up')}>
                    Move up
                  </Dropdown.MenuItem>
                  <Dropdown.MenuItem>Move down</Dropdown.MenuItem>
                </Dropdown.Menu>
              ),
              iconPath: wrench,
            },
          ];
        },
        onSelect: (command, original) => {
          console.log(command, original);
        },
        enableContextMenu: true,
      }),
      []
    );

    return (
      <div style={{height: '90vh'}}>
        <div>
          <Button
            onClick={() => {
              setData([...data]);
            }}>
            Re-render with same data
          </Button>
        </div>

        <Table
          content={
            <Table.Content
              onSelect={(original) => console.log('onSelect', original.id)}
              onRowClick={(original) => console.log('onRowClick', original.id)}
              columns={columns}
              data={data}
              optionsMenu={optionsMenu}
            />
          }
        />
      </div>
    );
  })
  .add('Default with knobs', () => (
    <TableWithToolbar type={Table.TableType.Default} />
  ))
  .add('Stable order of dynamic menu options (uses knobs)', () => {
    const optionMenuItems = options(
      'Options',
      menuOptions,
      menuOptionsDefaults,
      {
        display: 'multi-select',
      }
    );
    const useOptionsOrder = boolean('Use Options Order', true);

    const optionsMenu = useMemo(
      () => buildOptionsMenu(optionMenuItems, useOptionsOrder),
      [optionMenuItems, useOptionsOrder]
    );

    const columns: Column<DataItemWithSubRows>[] = React.useMemo(
      () => [
        {
          Header: 'Id',
          accessor: 'id',
          width: 40,
          isFixedWidth: true,
        },
        {
          Header: 'Name',
          accessor: 'name',
        },
      ],
      []
    );

    const generateRowId = (item) => `${item.id}`;

    const handleSelect = useCallback((selected) => {
      console.log('selected rows', selected);
    }, []);

    return (
      <div>
        <Table
          content={
            <Table.Content
              optionsMenu={optionsMenu}
              getRowId={generateRowId}
              columns={columns}
              data={sampleData}
              onSelect={handleSelect}
            />
          }
        />
      </div>
    );
  })
  .add('Mobile table', () => <TableWithToolbar type={Table.TableType.Mobile} />)
  .add('With default empty state', () => {
    return (
      <div style={{height: '90vh'}}>
        <Table
          content={
            <Table.Content
              columns={buildColumns({columnSettings: columnDefaults})}
              data={[]}
              pageSize={20}
              pageNumber={0}
              onPageChange={() => {}}
            />
          }
        />
      </div>
    );
  })
  .add('With custom empty state', () => {
    const getEmptyStateProps = () => ({
      icon: briefcase,
      text: 'No cases have been found',
    });

    return (
      <div style={{height: '90vh'}}>
        <Table
          content={
            <Table.Content
              columns={buildColumns({columnSettings: columnDefaults})}
              data={[]}
              pageSize={20}
              pageNumber={0}
              onPageChange={() => {}}
              getEmptyStateProps={getEmptyStateProps}
            />
          }
        />
      </div>
    );
  })
  .add('With drawer', () => {
    const Drawer = ({row}) => (
      <div>
        <div>{row.id}</div>
        <div>{row.firstName}</div>
        <div>Additional: Here is a message of lesser priority</div>
        <div>
          Secondary data: Information that was not important enough for the
          columns
        </div>
      </div>
    );

    const enableSelection = boolean('Enable row selection', true);
    const rowData: TestDataItem[] = [
      {
        id: '1',
        firstName: 'Captain - should not truncate',
        lastName:
          'Reindeer - should truncate very very very very very long string',
        age: '21',
        status: 'Online',
      },
      {
        id: '2',
        firstName: 'Patrick',
        lastName: 'Kane',
        age: '30',
        status: 'Busy',
      },
      {
        id: '3',
        firstName: 'Russell',
        lastName: 'Wilson',
        age: '31',
        status: 'Online',
      },
    ];

    const optionsMenu = {
      onSelect: () => {},
      getOptions: () => [
        {
          name: 'Edit',
          command: 'edit',
          iconPath: pencil,
        },
        {
          name: 'Duplicate',
          command: 'duplicate',
          iconPath: contentDuplicate,
        },
      ],
    };

    return (
      <div style={{height: '90vh'}}>
        <Table
          content={
            <Table.Content
              columns={buildColumns({columnSettings: columnDefaults})}
              data={rowData}
              optionsMenu={optionsMenu}
              defaultExpandedRows={defaultExpandedRowIds}
              onSelect={enableSelection ? () => {} : undefined}
              renderDrawer={(row) => {
                return <Drawer row={row} />;
              }}
              isRowExpandable={(row) => {
                return row.id !== '2';
              }}
            />
          }
        />
      </div>
    );
  })
  .add('With custom title element', () => {
    const rowData: TestDataItem[] = [
      {
        id: '1',
        firstName: 'Captain - should not truncate',
        lastName:
          'Reindeer - should truncate very very very very very long string',
        age: '21',
        status: 'Online',
      },
      {
        id: '2',
        firstName: 'Patrick',
        lastName: 'Kane',
        age: '30',
        status: 'Busy',
      },
      {
        id: '3',
        firstName: 'Russell',
        lastName: 'Wilson',
        age: '31',
        status: 'Online',
      },
    ];

    const [tabId, setTabId] = useState('tab1');

    return (
      <div style={{height: '90vh'}}>
        <Table
          toolbar={
            <Table.Toolbar
              titleChildren={
                <Tabs
                  showBorder={false}
                  onTabClicked={setTabId}
                  selectedTabId={tabId}>
                  <Tabs.Tab id={'tab1'} title="Tab1" />
                  <Tabs.Tab id={'tab2'} title="Tab2" />
                </Tabs>
              }
            />
          }
          content={
            <Table.Content
              columns={buildColumns({columnSettings: columnDefaults})}
              data={rowData}
            />
          }
        />
      </div>
    );
  })
  .add('With column filtering', () => {
    const originalRowData: DataItemWithColumnFiltering[] = useMemo(
      () => [
        {
          id: '1',
          firstName: 'Captain',
          lastName: 'Reindeer',
          age: '21',
          status: 'Online',
          dateCreated: '01-02-2019',
          multiselect: 'Multi Option 1',
          lastLogin: '01-01-2020',
        },
        {
          id: '2',
          firstName: 'Patrick',
          lastName: 'Kane',
          age: '30',
          status: 'Busy',
          dateCreated: '01-05-2019',
          lastLogin: '01-05-2020',
        },
        {
          id: '3',
          firstName: 'Russell',
          lastName: 'Wilson',
          age: '31',
          status: 'Online',
          dateCreated: '01-10-2019',
          multiselect: 'Multi Option 3',
          lastLogin: '01-10-2020',
        },
        {
          id: '4',
          firstName: 'Harry',
          lastName: 'Potter',
          status: 'Online',
          multiselect: 'Multi Option 1, Multi Option 2',
          dateCreated: '01-10-2019',
        },
        {
          id: '5',
          lastName: 'James',
          age: '31',
          status: 'Online',
          dateCreated: '01-10-2019',
          multiselect: 'Multi Option 2, Multi Option 3',
          lastLogin: '01-10-2020',
        },
      ],
      []
    );

    const columns = useMemo(() => {
      const columns: Column<DataItemWithColumnFiltering>[] = [
        {
          Header: 'Id',
          accessor: 'id',
          sticky: true,
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Numeric,
            operators: [
              {value: Operator.LessThan, label: 'Less than'},
              {value: Operator.GreaterThan, label: 'Greater than'},
              {value: Operator.LessThanOrEqual, label: 'Less than or equal'},
            ],
          },
        },
        {
          Header: 'First Name',
          accessor: 'firstName',
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Text,
          },
        },
        {
          Header: 'Last Name',
          accessor: 'lastName',
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Text,
            operators: [{value: Operator.Equal, label: 'Equal'}],
          },
        },
        {
          Header: 'Age',
          accessor: 'age',
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Numeric,
          },
        },
        {
          Header: 'Status',
          accessor: 'status',
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Text,
          },
        },
        {
          Header: 'Multiselect',
          accessor: 'multiselect',
          width: 100,
          filterSettings: {
            type: ColumnFilterType.Multiselect,
            options: [
              {label: 'Multi Option 1', value: '1'},
              {label: 'Multi Option 2', value: '2'},
              {label: 'Multi Option 3', value: '3'},
            ],
            operators: [
              {value: Operator.Match, label: 'Match'},
              {value: Operator.NotMatch, label: 'Not Match'},
            ],
          },
        },
        {
          Header: 'Date Created',
          accessor: 'dateCreated',
          width: 200,
          filterSettings: {
            type: ColumnFilterType.Date,
            quickDateOptions: [
              {label: 'last 7 days', id: QuickDate.Last7Days},
              {label: 'last 10 days', id: 'last10days'},
              {label: 'last 5 years', id: 'last5year'},
            ],
          },
        },
        {
          Header: 'Last Login Date',
          accessor: 'lastLogin',
          width: 200,
          filterSettings: {
            type: ColumnFilterType.Date,
          },
        },
      ];

      return columns;
    }, []);

    const [rowData, setRowData] = useState(originalRowData);
    const [filterValues, setFilterValues] = useState<FilterValue[]>([]);

    const onFilterApplied = useCallback(
      (newFilter: FilterValue) => {
        const existingFilter = filterValues.find(({id}) => id === newFilter.id);
        if (existingFilter) {
          setFilterValues((filterValues) =>
            filterValues.map((filter) =>
              filter.id === newFilter.id ? newFilter : filter
            )
          );
        } else {
          setFilterValues((filterValues) => [newFilter, ...filterValues]);
        }
      },
      [filterValues]
    );

    const onFilterToggled = useCallback((filterId) => {
      setFilterValues((filters) =>
        filters.map((filter) => ({
          ...filter,
          isIdle: filter.id === filterId ? !filter.isIdle : filter.isIdle,
        }))
      );
    }, []);

    const onFilterDeleted = useCallback((filterId) => {
      setFilterValues((filters) =>
        filters.filter((filter) => filter.id !== filterId)
      );
    }, []);

    const handleNumericFilter = (filterValue, cellValue) => {
      switch (filterValue.operator) {
        case Operator.Equal:
          return cellValue === filterValue.value;
        case Operator.LessThan:
          return cellValue < filterValue.value;
        case Operator.LessThanOrEqual:
          return cellValue <= filterValue.value;
        case Operator.GreaterThan:
          return cellValue > filterValue.value;
        case Operator.GreaterThanOrEqual:
          return cellValue >= filterValue.value;
        case Operator.InList:
          return filterValue.value
            .split(' ')
            .some((value) => value == cellValue);
        case Operator.InRange: {
          const rangeValues = filterValue.value.split('-');
          return +rangeValues[0] <= cellValue && +rangeValues[1] >= cellValue;
        }
        case Operator.IsNull:
          return !cellValue;
        case Operator.IsNotNull:
          return !!cellValue;
        default:
          return false;
      }
    };

    const handleTextFilter = (filterValue, cellValue) => {
      switch (filterValue.operator) {
        case Operator.Equal:
          return cellValue === filterValue.value;
        case Operator.Contains:
          return cellValue?.includes(filterValue.value);
        case Operator.StartWith:
          return cellValue && `${cellValue}`.startsWith(filterValue.value);
        case Operator.EndWith:
          return cellValue && `${cellValue}`.endsWith(filterValue.value);
        case Operator.IsNull:
          return !cellValue;
        case Operator.IsNotNull:
          return !!cellValue;
        default:
          return false;
      }
    };

    const handleDateFilter = (filterValue: FilterValue, cellValue) => {
      const value = filterValue.value as DateFilterValue;

      switch (value.optionId) {
        case CustomDate.Custom: {
          const {dateStart, dateEnd} = value;
          const parsedDateStart = Date.parse(dateStart as string);
          const parsedDateEnd = Date.parse(dateEnd as string);
          const parsedCellValue = Date.parse(cellValue);

          const isDateStartBefore = parsedDateStart
            ? parsedDateStart < parsedCellValue
            : true;
          const isDateEndAfter = parsedDateEnd
            ? parsedDateEnd > parsedCellValue
            : true;
          return isDateStartBefore && isDateEndAfter;
        }
        case QuickDate.Last1Hour: {
          const dateStart = moment.utc().subtract(1, 'hour');
          return dateStart.isBefore(cellValue);
        }
        case QuickDate.Last24Hours: {
          const dateStart = moment.utc().subtract(24, 'hour');
          return dateStart.isBefore(cellValue);
        }
        case QuickDate.Last7Days: {
          const dateStart = moment.utc().subtract(7, 'day');
          return dateStart.isBefore(cellValue);
        }
        case QuickDate.Last31Days: {
          const dateStart = moment.utc().subtract(31, 'day');
          return dateStart.isBefore(cellValue);
        }
        case QuickDate.Empty:
          return !cellValue;
        case QuickDate.NotEmpty:
          return !!cellValue;
        case 'last10days': {
          const dateStart = moment.utc().subtract(10, 'day');
          return dateStart.isBefore(cellValue);
        }
        case 'last5year': {
          const dateStart = moment.utc().subtract(5, 'year');
          return dateStart.isBefore(cellValue);
        }
        default:
          return false;
      }
    };

    const handleMultiSelectFilter = (filterValue, cellValue = '') => {
      const getMatches = (filterValue, cellValue) => {
        const filterValues = filterValue.value?.map((option) => option.label);
        const cellValues = cellValue.split(',').map((value) => value.trim());

        return cellValues.some((value) => filterValues.includes(value));
      };

      switch (filterValue.operator) {
        case Operator.Equal:
          return cellValue === filterValue.value;
        case Operator.Contains:
          return cellValue?.includes(filterValue.value);
        case Operator.StartWith:
          return cellValue && `${cellValue}`.startsWith(filterValue.value);
        case Operator.EndWith:
          return cellValue && `${cellValue}`.endsWith(filterValue.value);
        case Operator.IsNull:
          return !cellValue;
        case Operator.IsNotNull:
          return !!cellValue;
        case Operator.Match:
          return getMatches(filterValue, cellValue);
        case Operator.NotMatch:
          return !getMatches(filterValue, cellValue);
        default:
          return false;
      }
    };

    useEffect(() => {
      const filteredData = originalRowData.filter((row) => {
        return filterValues.every((filterValue) => {
          if (filterValue.isIdle) return true;

          switch (filterValue.type) {
            case ColumnFilterType.Numeric:
              return handleNumericFilter(filterValue, row[filterValue.id]);
            case ColumnFilterType.Text:
              return handleTextFilter(filterValue, row[filterValue.id]);
            case ColumnFilterType.Date:
              return handleDateFilter(filterValue, row[filterValue.id]);
            case ColumnFilterType.Multiselect:
              return handleMultiSelectFilter(filterValue, row[filterValue.id]);
            default:
              return false;
          }
        });
      });
      setRowData(filteredData);
    }, [filterValues, originalRowData]);

    return (
      <div style={{height: '90vh'}}>
        <Table
          content={
            <Table.Content
              onApplyFilter={onFilterApplied}
              onToggleFilter={onFilterToggled}
              onDeleteFilter={onFilterDeleted}
              filterValues={filterValues}
              columns={columns}
              data={rowData}
            />
          }
        />
      </div>
    );
  })
  .add('Custom cell background color', () => {
    const [data, setData] = React.useState<DataItem[]>([
      {
        id: '1',
        firstName: 'Michael',
        points: 0,
        extraColumn1:
          'really long string really long string really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
      {
        id: '2',
        firstName: 'Scottie',
        points: 0,
        extraColumn1: 'really long string really long string ',
        extraColumn2: 'string string string',
        extraColumn3: 'string string string',
      },
    ]);

    const columns = React.useMemo(() => {
      const columns: Column<DataItem>[] = [
        {
          Header: 'Id',
          accessor: 'id',
          width: 40,
          isFixedWidth: true,
          getCellBackgroundColor: ({cell}) => (cell.value > 1 ? 'red' : 'grey'),
        },
        {
          Header: 'First name',
          accessor: 'firstName',
        },
        {
          Header: 'Extra Column 1',
          accessor: 'extraColumn1',
        },
        {
          Header: 'Extra Column 2',
          accessor: 'extraColumn2',
        },
        {
          Header: 'Extra Column 3',
          accessor: 'extraColumn3',
        },
        {
          Header: 'Points',
          accessor: 'points',
          width: 100,
          isFixedWidth: true,
          Cell: function Cell({row}) {
            return (
              <TextField
                type="number"
                value={row.original.points}
                onChange={(value) => {
                  setData((d) =>
                    d.map((player) => {
                      if (player.id === row.original.id) {
                        return {
                          ...player,
                          points: value,
                        };
                      }

                      return player;
                    })
                  );
                }}
              />
            );
          },
        },
      ];

      return columns;
    }, []);

    return <Table content={<Table.Content columns={columns} data={data} />} />;
  })
  .add('pre selected rows', () => {
    const [defaultSelectedRows, setDefaultSelectedRows] = useState([
      '1',
      '3',
      'nested-2-1',
    ]);
    const defaultExpandedRows = ['2'];

    const columns = React.useMemo(() => {
      const columns: Column<DataItemWithSubRows>[] = [
        {
          Header: 'Id',
          accessor: 'id',
          width: 40,
          isFixedWidth: true,
        },
        {
          Header: 'Name',
          accessor: 'name',
        },
      ];

      return columns;
    }, []);

    const generateRowId = (item) => `${item.id}`;

    const handleSelect = useCallback((selected) => {
      console.log('selected rows', selected);
    }, []);

    const handleClearSelection = () => {
      setDefaultSelectedRows([]);
    };

    return (
      <div>
        <Table
          content={
            <Table.Content
              defaultSelectedRows={defaultSelectedRows}
              defaultExpandedRows={defaultExpandedRows}
              autoResetSelectedRows={true}
              getRowId={generateRowId}
              columns={columns}
              data={sampleData}
              onSelect={handleSelect}
            />
          }
        />

        <button onClick={handleClearSelection}>Clear selection</button>
      </div>
    );
  })
  .add('With toolbar without search and buttons', () => {
    const rows = useMemo(
      () => buildRows({numberOfRows: 2, subRowDepth: 0}),
      []
    );
    const columns = useMemo(
      () => buildColumns({columnSettings: columnDefaults}),
      []
    );
    return (
      <div>
        <Table
          toolbar={<Table.Toolbar title="Respondents" count={30} />}
          content={<Table.Content columns={columns} data={rows} />}
        />
      </div>
    );
  });
