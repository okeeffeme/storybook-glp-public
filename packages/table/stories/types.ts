export type DataItem = {
  id: string;
  firstName: string;
  points: number;
  extraColumn1: string;
  extraColumn2: string;
  extraColumn3: string;
};

export type DataItemWithSubRows = {
  id: string;
  name: string;
  subRows?: DataItemWithSubRows[];
};

export type DataItemWithColumnFiltering = {
  id?: string;
  firstName?: string;
  lastName?: string;
  age?: string;
  status?: string;
  dateCreated?: string;
  multiselect?: string;
  lastLogin?: string;
};
