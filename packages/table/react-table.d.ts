import {MouseEventHandler} from 'react';

import {
  UseColumnOrderInstanceProps,
  UseColumnOrderState,
  UseExpandedHooks,
  UseExpandedInstanceProps,
  UseExpandedOptions,
  UseExpandedRowProps,
  UseExpandedState,
  UseFiltersColumnProps,
  UseFiltersInstanceProps,
  UseFiltersOptions,
  UseFiltersState,
  UseGlobalFiltersInstanceProps,
  UseGlobalFiltersOptions,
  UseGlobalFiltersState,
  UseGroupByCellProps,
  UseGroupByColumnProps,
  UseGroupByHooks,
  UseGroupByInstanceProps,
  UseGroupByOptions,
  UseGroupByRowProps,
  UseGroupByState,
  UsePaginationInstanceProps,
  UsePaginationOptions,
  UsePaginationState,
  UseResizeColumnsColumnProps,
  UseResizeColumnsOptions,
  UseResizeColumnsState,
  UseRowSelectHooks,
  UseRowSelectInstanceProps,
  UseRowSelectOptions,
  UseRowSelectRowProps,
  UseRowSelectState,
  UseRowStateCellProps,
  UseRowStateInstanceProps,
  UseRowStateOptions,
  UseRowStateRowProps,
  UseRowStateState,
  UseSortByColumnProps,
  UseSortByHooks,
  UseSortByInstanceProps,
  UseSortByOptions,
  UseSortByState,
} from 'react-table';

import {
  UseOptionsMenuOptions,
  UseOptionsMenuInstanceProps,
  UseOptionsMenuRowProps,
} from './src/plugins/useOptionsMenu/types';
import {
  UseStickyColumnsOptions,
  UseStickyColumnsInstanceProps,
} from './src/plugins/useStickyColumns/types';
import {
  UseSortHeaderColumnInstance,
  UseSortHeaderInstanceProps,
  UseSortHeaderOptions,
} from './src/plugins/useSortHeader/types';
import {
  UseExpandToggleInstanceProps,
  UseExpandToggleOptions,
} from './src/plugins/useExpandToggle/types';
import {UseColumnFiltersOptions} from './src/plugins/useColumnFilters/types';
import {Language, LanguageLiteral} from './src/constants';
import {
  UseColumnOptions,
  TableColumnOptions,
  TableColumnProps,
} from './src/types';

declare module 'react-table' {
  // take this file as-is, or comment out the sections that don't apply to your plugin configuration

  export interface TableOptions<
    D extends Record<string, unknown>
  > extends UseTableOptions<D>,
      UseExpandedOptions<D>,
      UseSortByOptions<D>,
      // note that having Record here allows you to add anything to the options, this matches the spirit of the
      // underlying js library, but might be cleaner if it's replaced by a more specific type that matches your
      // feature set, this is a safe default.
      //Record<string, any>
      UseFiltersOptions<D>,
      UseGlobalFiltersOptions<D>,
      UseGroupByOptions<D>,
      UsePaginationOptions<D>,
      UseResizeColumnsOptions<D>,
      UseRowStateOptions<D>,
      UseRowSelectOptions<D>,
      UseOptionsMenuOptions<D>,
      UseSortHeaderOptions,
      UseExpandToggleOptions<D>,
      UseColumnFiltersOptions,
      UseStickyColumnsOptions {
    language: Language | LanguageLiteral;
    getRowId: (
      originalRow: D,
      relativeIndex: number,
      parent?: Row<D>
    ) => string | number;
  }

  export interface Hooks<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseExpandedHooks<D>,
      UseGroupByHooks<D>,
      UseRowSelectHooks<D>,
      UseSortByHooks<D> {}

  export interface TableInstance<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseColumnOrderInstanceProps<D>,
      UseExpandedInstanceProps<D>,
      UseFiltersInstanceProps<D>,
      UseGlobalFiltersInstanceProps<D>,
      UseGroupByInstanceProps<D>,
      UsePaginationInstanceProps<D>,
      UseRowSelectInstanceProps<D>,
      UseRowStateInstanceProps<D>,
      UseSortByInstanceProps<D>,
      UseOptionsMenuInstanceProps<D>,
      UseSortHeaderInstanceProps,
      UseExpandToggleInstanceProps<D>,
      UseStickyColumnsInstanceProps {}

  export interface TableState<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseColumnOrderState<D>,
      UseExpandedState<D>,
      UseFiltersState<D>,
      UseGlobalFiltersState<D>,
      UseGroupByState<D>,
      UsePaginationState<D>,
      UseResizeColumnsState<D>,
      UseRowSelectState<D>,
      UseRowStateState<D>,
      UseSortByState<D> {}

  export interface ColumnInterface<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseColumnOptions<D>,
      TableColumnOptions {}

  export interface ColumnInstance<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseFiltersColumnProps<D>,
      UseGroupByColumnProps<D>,
      UseResizeColumnsColumnProps<D>,
      UseSortByColumnProps<D>,
      UseSortHeaderColumnInstance,
      TableColumnProps {}

  export interface Cell<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseGroupByCellProps<D>,
      UseRowStateCellProps<D> {}

  export interface Row<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseExpandedRowProps<D>,
      UseGroupByRowProps<D>,
      UseRowSelectRowProps<D>,
      UseRowStateRowProps<D>,
      UseOptionsMenuRowProps<D> {}

  export interface TableExpandedToggleProps {
    onClick: MouseEventHandler;
  }
}
