# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [13.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.54&sourceBranch=refs/tags/@jotunheim/react-table@13.1.55&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.53&sourceBranch=refs/tags/@jotunheim/react-table@13.1.54&targetRepoId=1246) (2023-04-12)

### Bug Fixes

- made the context menu consider the options order ([NPM-1280](https://jiraosl.firmglobal.com/browse/NPM-1280)) ([b64c581](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b64c5810f56d8df5fad14d043e3bd381f3243bbf))
- remove duplicates from allCommands if optionsOrder is presented ([NPM-1280](https://jiraosl.firmglobal.com/browse/NPM-1280)) ([b22603a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b22603abe1641e5446ddd5a806f8af7d08c0e6a0))

## [13.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.52&sourceBranch=refs/tags/@jotunheim/react-table@13.1.53&targetRepoId=1246) (2023-04-07)

### Bug Fixes

- expand clickable area for sorting table by column to the whole cell ([NPM-1258](https://jiraosl.firmglobal.com/browse/NPM-1258)) ([6a80f9e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6a80f9e52e4c46ee0f2d8beba113308a7ea50819))

## [13.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.52&sourceBranch=refs/tags/@jotunheim/react-table@13.1.52&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.50&sourceBranch=refs/tags/@jotunheim/react-table@13.1.51&targetRepoId=1246) (2023-04-05)

### Bug Fixes

- specify symbol for not match column filter ([NPM-1257](https://jiraosl.firmglobal.com/browse/NPM-1257)) ([4300557](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4300557b30c46fc43c70200645cb73d2c53fe778))
- translate norwegian symbols from english for column filters ([NPM-1257](https://jiraosl.firmglobal.com/browse/NPM-1257)) ([0d2879a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0d2879a1d1e89be0a9410deee25beadca292e786))

## [13.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.49&sourceBranch=refs/tags/@jotunheim/react-table@13.1.50&targetRepoId=1246) (2023-04-03)

### Bug Fixes

- remove value from check if we can apply column filter ([NPM-1292](https://jiraosl.firmglobal.com/browse/NPM-1292)) ([dc99e03](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dc99e037b8d0226e39b8664062c7aa6f9144340f))

## [13.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.48&sourceBranch=refs/tags/@jotunheim/react-table@13.1.49&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.47&sourceBranch=refs/tags/@jotunheim/react-table@13.1.48&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.46&sourceBranch=refs/tags/@jotunheim/react-table@13.1.47&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.45&sourceBranch=refs/tags/@jotunheim/react-table@13.1.46&targetRepoId=1246) (2023-03-29)

### Bug Fixes

- add test ids in Table's components ([NPM-1228](https://jiraosl.firmglobal.com/browse/NPM-1228)) ([6415b6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6415b6ca5b2d547a2c8cf1611d7f7578b8329fe4))

## [13.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.44&sourceBranch=refs/tags/@jotunheim/react-table@13.1.45&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.43&sourceBranch=refs/tags/@jotunheim/react-table@13.1.44&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.42&sourceBranch=refs/tags/@jotunheim/react-table@13.1.43&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.41&sourceBranch=refs/tags/@jotunheim/react-table@13.1.42&targetRepoId=1246) (2023-03-03)

### Bug Fixes

- make optionsMenu.onSelect optional ([NPM-1122](https://jiraosl.firmglobal.com/browse/NPM-1122)) ([7aca94e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7aca94e21675cd864236eca028a1f1a355db3f97))

## [13.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.40&sourceBranch=refs/tags/@jotunheim/react-table@13.1.41&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.39&sourceBranch=refs/tags/@jotunheim/react-table@13.1.40&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- adjust incorrect data attribute ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([59584be](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/59584beed57d2249cfab0158b9b98e9fec903698))
- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [13.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.38&sourceBranch=refs/tags/@jotunheim/react-table@13.1.39&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.37&sourceBranch=refs/tags/@jotunheim/react-table@13.1.38&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.35&sourceBranch=refs/tags/@jotunheim/react-table@13.1.37&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.35&sourceBranch=refs/tags/@jotunheim/react-table@13.1.36&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.34&sourceBranch=refs/tags/@jotunheim/react-table@13.1.35&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.33&sourceBranch=refs/tags/@jotunheim/react-table@13.1.34&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.32&sourceBranch=refs/tags/@jotunheim/react-table@13.1.33&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.31&sourceBranch=refs/tags/@jotunheim/react-table@13.1.32&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.30&sourceBranch=refs/tags/@jotunheim/react-table@13.1.31&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.29&sourceBranch=refs/tags/@jotunheim/react-table@13.1.30&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.28&sourceBranch=refs/tags/@jotunheim/react-table@13.1.29&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add default value for content props ([NPM-1235](https://jiraosl.firmglobal.com/browse/NPM-1235)) ([00a53df](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/00a53dff9213e3d948de77b15726e655a5f65852))

## [13.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.27&sourceBranch=refs/tags/@jotunheim/react-table@13.1.28&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.26&sourceBranch=refs/tags/@jotunheim/react-table@13.1.27&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.25&sourceBranch=refs/tags/@jotunheim/react-table@13.1.26&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.24&sourceBranch=refs/tags/@jotunheim/react-table@13.1.25&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.23&sourceBranch=refs/tags/@jotunheim/react-table@13.1.24&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.22&sourceBranch=refs/tags/@jotunheim/react-table@13.1.23&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.21&sourceBranch=refs/tags/@jotunheim/react-table@13.1.22&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.20&sourceBranch=refs/tags/@jotunheim/react-table@13.1.21&targetRepoId=1246) (2023-01-25)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.19&sourceBranch=refs/tags/@jotunheim/react-table@13.1.20&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.18&sourceBranch=refs/tags/@jotunheim/react-table@13.1.19&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.14&sourceBranch=refs/tags/@jotunheim/react-table@13.1.18&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.14&sourceBranch=refs/tags/@jotunheim/react-table@13.1.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.14&sourceBranch=refs/tags/@jotunheim/react-table@13.1.16&targetRepoId=1246) (2023-01-18)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.14&sourceBranch=refs/tags/@jotunheim/react-table@13.1.15&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.13&sourceBranch=refs/tags/@jotunheim/react-table@13.1.14&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.12&sourceBranch=refs/tags/@jotunheim/react-table@13.1.13&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.11&sourceBranch=refs/tags/@jotunheim/react-table@13.1.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.10&sourceBranch=refs/tags/@jotunheim/react-table@13.1.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.9&sourceBranch=refs/tags/@jotunheim/react-table@13.1.10&targetRepoId=1246) (2022-12-28)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.8&sourceBranch=refs/tags/@jotunheim/react-table@13.1.9&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.7&sourceBranch=refs/tags/@jotunheim/react-table@13.1.8&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.6&sourceBranch=refs/tags/@jotunheim/react-table@13.1.7&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.5&sourceBranch=refs/tags/@jotunheim/react-table@13.1.6&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.4&sourceBranch=refs/tags/@jotunheim/react-table@13.1.5&targetRepoId=1246) (2022-12-20)

### Bug Fixes

- use zIndexStack for loading indicator ([NPM-1127](https://jiraosl.firmglobal.com/browse/NPM-1127)) ([6b290b8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6b290b84880cf43d7373f274c4587fbc9829b237))

## [13.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.3&sourceBranch=refs/tags/@jotunheim/react-table@13.1.4&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.0&sourceBranch=refs/tags/@jotunheim/react-table@13.1.3&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.0&sourceBranch=refs/tags/@jotunheim/react-table@13.1.2&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-table

## [13.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.1.0&sourceBranch=refs/tags/@jotunheim/react-table@13.1.1&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-table

# [13.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.0.4&sourceBranch=refs/tags/@jotunheim/react-table@13.1.0&targetRepoId=1246) (2022-11-23)

### Features

- convert Table to Typescript ([NPM-1102](https://jiraosl.firmglobal.com/browse/NPM-1102)) ([05ef408](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/05ef4083aa3b2532442ec6e26c7109545fe85fa1))

## [13.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.0.3&sourceBranch=refs/tags/@jotunheim/react-table@13.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-table

## [13.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.0.2&sourceBranch=refs/tags/@jotunheim/react-table@13.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-table

## [13.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@13.0.0&sourceBranch=refs/tags/@jotunheim/react-table@13.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-table

# [13.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.17&sourceBranch=refs/tags/@jotunheim/react-table@13.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- use arePropsEqual from utils package ([NPM-1093](https://jiraosl.firmglobal.com/browse/NPM-1093)) ([e0c6570](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e0c6570aa19b69a9e0b7de6b35d13ef1c07214fa))

### Features

- remove className prop of Table ([NPM-955](https://jiraosl.firmglobal.com/browse/NPM-955)) ([7a2692b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7a2692b2a90a9bf5fc61899686a8728adebb08cd))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [12.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.16&sourceBranch=refs/tags/@jotunheim/react-table@12.1.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.15&sourceBranch=refs/tags/@jotunheim/react-table@12.1.16&targetRepoId=1246) (2022-09-30)

### Bug Fixes

- add directory property to Table package.json ([NPM-1089](https://jiraosl.firmglobal.com/browse/NPM-1089)) ([bf7436c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bf7436cb97a4cda88cd6b9f7a4546d57c21a5279))

## [12.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.14&sourceBranch=refs/tags/@jotunheim/react-table@12.1.15&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.13&sourceBranch=refs/tags/@jotunheim/react-table@12.1.14&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.12&sourceBranch=refs/tags/@jotunheim/react-table@12.1.13&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.11&sourceBranch=refs/tags/@jotunheim/react-table@12.1.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.10&sourceBranch=refs/tags/@jotunheim/react-table@12.1.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.9&sourceBranch=refs/tags/@jotunheim/react-table@12.1.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.8&sourceBranch=refs/tags/@jotunheim/react-table@12.1.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.6&sourceBranch=refs/tags/@jotunheim/react-table@12.1.7&targetRepoId=1246) (2022-08-31)

### Bug Fixes

- useOptionsMenu uses incorrect Dropdown item ([NPM-1054](https://jiraosl.firmglobal.com/browse/NPM-1054)) ([34ab7cd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/34ab7cd72284a5bb1f021392a2995eda82e27741))

## [12.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.5&sourceBranch=refs/tags/@jotunheim/react-table@12.1.6&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.2&sourceBranch=refs/tags/@jotunheim/react-table@12.1.3&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.1&sourceBranch=refs/tags/@jotunheim/react-table@12.1.2&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-table

## [12.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.1.0&sourceBranch=refs/tags/@jotunheim/react-table@12.1.1&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-table

# [12.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.0.2&sourceBranch=refs/tags/@jotunheim/react-table@12.1.0&targetRepoId=1246) (2022-07-08)

### Features

- Added searchAutoFocus prop to toolbar ([AM-7653](https://jiraosl.firmglobal.com/browse/AM-7653)) ([59097a7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/59097a72bb8a7104891189b5d1194844d8d2b625))

## [12.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.0.1&sourceBranch=refs/tags/@jotunheim/react-table@12.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-table

## [12.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-table@12.0.0&sourceBranch=refs/tags/@jotunheim/react-table@12.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-table

# 12.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [11.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.12&sourceBranch=refs/tags/@confirmit/react-table@11.1.13&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-table

## [11.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.11&sourceBranch=refs/tags/@confirmit/react-table@11.1.12&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-table

## [11.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.10&sourceBranch=refs/tags/@confirmit/react-table@11.1.11&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- Remove usage of closeOnPortalClick. Only MenuLinkItem if option.href is set. ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([d7e54dd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d7e54dd98227dd629df05b8eeeded7b07618b077))
- update Dropdown.MenuItem usage ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([6ce3219](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ce3219a699823f7081cc01e1ad966abef71ba3b))
- update usages of Dropdown ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([d4788da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d4788da8098f822eef8fc5c6c77489af8836cc00))

## [11.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.9&sourceBranch=refs/tags/@confirmit/react-table@11.1.10&targetRepoId=1246) (2022-05-30)

### Bug Fixes

- don't show prop warning when optionsMenu is not defined ([NPM-1019](https://jiraosl.firmglobal.com/browse/NPM-1019)) ([0b396fc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0b396fcbe503ed895c297daba63d70e2d9fc6e8d))

## [11.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.8&sourceBranch=refs/tags/@confirmit/react-table@11.1.9&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [11.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.7&sourceBranch=refs/tags/@confirmit/react-table@11.1.8&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-table

## [11.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.6&sourceBranch=refs/tags/@confirmit/react-table@11.1.7&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-table

## [11.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.5&sourceBranch=refs/tags/@confirmit/react-table@11.1.6&targetRepoId=1246) (2022-05-11)

### Bug Fixes

- fix Toolbar styles to have min height ([NPM-1009](https://jiraosl.firmglobal.com/browse/NPM-1009)) ([630c333](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/630c33312849e1120fc7511dae05df2aa6dcb2ef))

## [11.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.4&sourceBranch=refs/tags/@confirmit/react-table@11.1.5&targetRepoId=1246) (2022-04-26)

### Bug Fixes

- table proptypes ([NPM-1006](https://jiraosl.firmglobal.com/browse/NPM-1006)) ([e8b3e64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e8b3e64e64a7257e93735f0060ace93d898b7002))

## [11.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.1.3&sourceBranch=refs/tags/@confirmit/react-table@11.1.4&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-table

# [11.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.9&sourceBranch=refs/tags/@confirmit/react-table@11.1.0&targetRepoId=1246) (2022-04-21)

### Features

- Add the ability to show a context menu per row in the Table ([CATI-4150](https://jiraosl.firmglobal.com/browse/CATI-4150)) ([185e9eb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/185e9eb93f0f3c2aaedb093d46a41e0e3a26e5bc))

## [11.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.8&sourceBranch=refs/tags/@confirmit/react-table@11.0.9&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-table

## [11.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.7&sourceBranch=refs/tags/@confirmit/react-table@11.0.8&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-table

## [11.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.6&sourceBranch=refs/tags/@confirmit/react-table@11.0.7&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-table

## [11.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.5&sourceBranch=refs/tags/@confirmit/react-table@11.0.6&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-table

## [11.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.4&sourceBranch=refs/tags/@confirmit/react-table@11.0.5&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-table

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@11.0.3&sourceBranch=refs/tags/@confirmit/react-table@11.0.4&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-table

# [11.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.23&sourceBranch=refs/tags/@confirmit/react-table@11.0.0&targetRepoId=1246) (2022-02-11)

### BREAKING CHANGES

- Remove the deprecated "icon" prop from options menu items (NPM-858)

## [10.4.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.22&sourceBranch=refs/tags/@confirmit/react-table@10.4.23&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.21&sourceBranch=refs/tags/@confirmit/react-table@10.4.22&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.20&sourceBranch=refs/tags/@confirmit/react-table@10.4.21&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/pull-requests/972/overview)

**Bugfix** - Cell now respects `orderOptions`

## [10.4.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.18&sourceBranch=refs/tags/@confirmit/react-table@10.4.19&targetRepoId=1246) (2022-01-26)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.17&sourceBranch=refs/tags/@confirmit/react-table@10.4.18&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.16&sourceBranch=refs/tags/@confirmit/react-table@10.4.17&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.15&sourceBranch=refs/tags/@confirmit/react-table@10.4.16&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.14&sourceBranch=refs/tags/@confirmit/react-table@10.4.15&targetRepoId=1246) (2022-01-14)

### Bug Fixes

- fix filtering on columns in 'Default with knobs' story ([NPM-907](https://jiraosl.firmglobal.com/browse/NPM-907)) ([2a81679](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2a81679f5679532fe87cf734a6040783d9185359))
- fix filtering on Date column when operators are not specified in column settings ([NPM-907](https://jiraosl.firmglobal.com/browse/NPM-907)) ([9d18f36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9d18f36cd64da29a92a79097e7b62bcb3875e87b))

## [10.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.13&sourceBranch=refs/tags/@confirmit/react-table@10.4.14&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.12&sourceBranch=refs/tags/@confirmit/react-table@10.4.13&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.11&sourceBranch=refs/tags/@confirmit/react-table@10.4.12&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.10&sourceBranch=refs/tags/@confirmit/react-table@10.4.11&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.9&sourceBranch=refs/tags/@confirmit/react-table@10.4.10&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.8&sourceBranch=refs/tags/@confirmit/react-table@10.4.9&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.7&sourceBranch=refs/tags/@confirmit/react-table@10.4.8&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.6&sourceBranch=refs/tags/@confirmit/react-table@10.4.7&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.5&sourceBranch=refs/tags/@confirmit/react-table@10.4.6&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.4&sourceBranch=refs/tags/@confirmit/react-table@10.4.5&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [10.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.3&sourceBranch=refs/tags/@confirmit/react-table@10.4.4&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.1&sourceBranch=refs/tags/@confirmit/react-table@10.4.2&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-table

## [10.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.0&sourceBranch=refs/tags/@confirmit/react-table@10.4.1&targetRepoId=1246) (2021-10-01)

### Bug Fixes

- add minimal height for table header cells with no truncation ([NPM-875](https://jiraosl.firmglobal.com/browse/NPM-875)) ([6c4e428](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6c4e428e05f3f06b48a27abc2bbaf9863e49eafe))

# [10.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.13&sourceBranch=refs/tags/@confirmit/react-table@10.4.0&targetRepoId=1246) (2021-09-30)

### Features

- add suppressHeaderTruncation property ([NPM-875](https://jiraosl.firmglobal.com/browse/NPM-875)) ([0137c72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0137c729ad9d28d589371dc7c0d1518c1809b8b6))

## [10.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.12&sourceBranch=refs/tags/@confirmit/react-table@10.3.13&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.11&sourceBranch=refs/tags/@confirmit/react-table@10.3.12&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.10&sourceBranch=refs/tags/@confirmit/react-table@10.3.11&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.9&sourceBranch=refs/tags/@confirmit/react-table@10.3.10&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.8&sourceBranch=refs/tags/@confirmit/react-table@10.3.9&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.7&sourceBranch=refs/tags/@confirmit/react-table@10.3.8&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.6&sourceBranch=refs/tags/@confirmit/react-table@10.3.7&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.5&sourceBranch=refs/tags/@confirmit/react-table@10.3.6&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.4&sourceBranch=refs/tags/@confirmit/react-table@10.3.5&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.3&sourceBranch=refs/tags/@confirmit/react-table@10.3.4&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.2&sourceBranch=refs/tags/@confirmit/react-table@10.3.3&targetRepoId=1246) (2021-09-10)

### Bug Fixes

- sort icon size in table had incorrect size ([NPM-791](https://jiraosl.firmglobal.com/browse/NPM-791)) ([1f97ffb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1f97ffb1e96509d2d8bd33239bd69664d4c5a05d))

## [10.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.1&sourceBranch=refs/tags/@confirmit/react-table@10.3.2&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-table

## [10.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.3.0&sourceBranch=refs/tags/@confirmit/react-table@10.3.1&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-table

# [10.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.8&sourceBranch=refs/tags/@confirmit/react-table@10.3.0&targetRepoId=1246) (2021-08-11)

### Features

- add defaultSelectedRows prop to allow table to start with some initally selected rows ([NPM-841](https://jiraosl.firmglobal.com/browse/NPM-841)) ([202a527](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/202a527f2257d4c4993cc6302a2b6e5191d1138d))

## [10.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.7&sourceBranch=refs/tags/@confirmit/react-table@10.2.8&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.6&sourceBranch=refs/tags/@confirmit/react-table@10.2.7&targetRepoId=1246) (2021-07-30)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.5&sourceBranch=refs/tags/@confirmit/react-table@10.2.6&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.4&sourceBranch=refs/tags/@confirmit/react-table@10.2.5&targetRepoId=1246) (2021-07-27)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.3&sourceBranch=refs/tags/@confirmit/react-table@10.2.4&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.2&sourceBranch=refs/tags/@confirmit/react-table@10.2.3&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.1&sourceBranch=refs/tags/@confirmit/react-table@10.2.2&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-table

## [10.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.2.0&sourceBranch=refs/tags/@confirmit/react-table@10.2.1&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-table

# [10.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.1.0&sourceBranch=refs/tags/@confirmit/react-table@10.2.0&targetRepoId=1246) (2021-07-07)

### Bug Fixes

- fixed clicking row checkbox should not trigger onRowClick when both onRowClick and onSelect are defined ([acf59c1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/acf59c1e201994830bfbc6aec723107bafeca1a3))

### Features

- added icon to batchOperationOptions ([53d7ac4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/53d7ac415f52c9fd8d8423003aa818dd419c6eb6))
- support iconPath in MenuTextItem and MenuLinkItem ([712d6da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/712d6da4629ac78c823ffc5af845e2d33ce61fff))

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.0.2&sourceBranch=refs/tags/@confirmit/react-table@10.0.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-table

## [10.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.0.1&sourceBranch=refs/tags/@confirmit/react-table@10.0.2&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-table

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.0.0&sourceBranch=refs/tags/@confirmit/react-table@10.0.1&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-table

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.11.4&sourceBranch=refs/tags/@confirmit/react-table@10.0.0&targetRepoId=1246) (2021-06-22)

### Features

- added column filtering ([d1da025](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d1da02554329f01a07e5cf7dbc4f07123694d24a))

- **BREAKING:** previously clicking anywhere in the header cell would trigger onSort callback. Now it will only trigger onSort when the header title is clicked.

###

# [9.11.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.9&sourceBranch=refs/tags/@confirmit/react-table@9.11.0&targetRepoId=1246) (2021-06-08)

### Bug Fixes

- not correct appearance row drawers after starting from the second line([NPM-805](https://jiraosl.firmglobal.com/browse/NPM-805))

## [9.11.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.11.1&sourceBranch=refs/tags/@confirmit/react-table@9.11.2&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-table

## [9.11.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.11.0&sourceBranch=refs/tags/@confirmit/react-table@9.11.1&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-table

# [9.11.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.9&sourceBranch=refs/tags/@confirmit/react-table@9.11.0&targetRepoId=1246) (2021-06-08)

### Features

- add getCellBackgroundColor to column option ([1d0ee7f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1d0ee7fc54e77712487bf6ea47cfefa2195d3f90))

## [9.10.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.8&sourceBranch=refs/tags/@confirmit/react-table@9.10.9&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.7&sourceBranch=refs/tags/@confirmit/react-table@9.10.8&targetRepoId=1246) (2021-06-02)

### Features

- improve performance for select/deselect action in selectable rows.

## [9.10.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.6&sourceBranch=refs/tags/@confirmit/react-table@9.10.7&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.5&sourceBranch=refs/tags/@confirmit/react-table@9.10.6&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.4&sourceBranch=refs/tags/@confirmit/react-table@9.10.5&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.3&sourceBranch=refs/tags/@confirmit/react-table@9.10.4&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.2&sourceBranch=refs/tags/@confirmit/react-table@9.10.3&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-table

## [9.10.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.10.1&sourceBranch=refs/tags/@confirmit/react-table@9.10.2&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-table

# [9.10.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.9.0&sourceBranch=refs/tags/@confirmit/react-table@9.10.0&targetRepoId=1246) (2021-05-07)

### Bug Fixes

- only render title when its set ([NPM-779](https://jiraosl.firmglobal.com/browse/NPM-779)) ([f996b3b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f996b3b37831fc1769494cd851c149f595690876))

### Features

- support children in table toolbar ([NPM-779](https://jiraosl.firmglobal.com/browse/NPM-779)) ([df1ea07](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/df1ea07b973dd4c5a8dc76e6de8282bd6adc293b))

## 9.9.0

- ability to turn off min-height for content when showEmptyState is disabled ([NPM-777](https://jiraosl.firmglobal.com/browse/NPM-777)

## [9.8.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.13&sourceBranch=refs/tags/@confirmit/react-table@9.8.14&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.12&sourceBranch=refs/tags/@confirmit/react-table@9.8.13&targetRepoId=1246) (2021-04-22)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.11&sourceBranch=refs/tags/@confirmit/react-table@9.8.12&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.10&sourceBranch=refs/tags/@confirmit/react-table@9.8.11&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.9&sourceBranch=refs/tags/@confirmit/react-table@9.8.10&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.8&sourceBranch=refs/tags/@confirmit/react-table@9.8.9&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [9.8.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.7&sourceBranch=refs/tags/@confirmit/react-table@9.8.8&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.6&sourceBranch=refs/tags/@confirmit/react-table@9.8.7&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.5&sourceBranch=refs/tags/@confirmit/react-table@9.8.6&targetRepoId=1246) (2021-03-26)

### Bug Fixes

- children of Toolbar previously was allowed to shrink, causing issues where the content would flow over other items, f.ex covering the search field ([NPM-750](https://jiraosl.firmglobal.com/browse/NPM-750)) ([fbb2714](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fbb271441d1d424900e16fa865fd4505ca3810a7))

## [9.8.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.4&sourceBranch=refs/tags/@confirmit/react-table@9.8.5&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.3&sourceBranch=refs/tags/@confirmit/react-table@9.8.4&targetRepoId=1246) (2021-03-25)

### Bug Fixes

- dont trigger onSelect or onExpand unless the relevant ids change ([NPM-749](https://jiraosl.firmglobal.com/browse/NPM-749)) ([14da78f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/14da78f1799b34aa6195f2ef97f526d2eedbb2be))

## [9.8.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.2&sourceBranch=refs/tags/@confirmit/react-table@9.8.3&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.1&sourceBranch=refs/tags/@confirmit/react-table@9.8.2&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-table

## [9.8.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.8.0&sourceBranch=refs/tags/@confirmit/react-table@9.8.1&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-table

# [9.8.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.7.4&sourceBranch=refs/tags/@confirmit/react-table@9.8.0&targetRepoId=1246) (2021-03-19)

### Bug Fixes

- use IconButton size=small in table ([NPM-715](https://jiraosl.firmglobal.com/browse/NPM-715)) ([3a375d1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3a375d16dd6e3c4f664b9c5c418047e73f7b5515))

### Features

- ability to turn off cell truncation ([NPM-715](https://jiraosl.firmglobal.com/browse/NPM-715)) ([b22a5d9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b22a5d92a780cbbce952213541d2ddd98084f0c8))

## [9.7.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.7.3&sourceBranch=refs/tags/@confirmit/react-table@9.7.4&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-table

## [9.7.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.7.2&sourceBranch=refs/tags/@confirmit/react-table@9.7.3&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-table

## [9.7.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.7.1&sourceBranch=refs/tags/@confirmit/react-table@9.7.2&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-table

## [9.7.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.7.0&sourceBranch=refs/tags/@confirmit/react-table@9.7.1&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-table

# [9.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.6.0&sourceBranch=refs/tags/@confirmit/react-table@9.7.0&targetRepoId=1246) (2021-03-10)

### Features

- add hasPadding prop to table to easily turn off padding around table component ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([b478b02](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b478b02c217aebd706f4360f76274d924ef32da9))

# [9.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.5.0&sourceBranch=refs/tags/@confirmit/react-table@9.6.0&targetRepoId=1246) (2021-03-09)

### Features

- Background color for selected rows in Table ([NPM-415](https://jiraosl.firmglobal.com/browse/NPM-415)) ([38ff7df](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/38ff7dfe212b53fb3df669bb8d54d2146d7f93cd))

# [9.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.4.1&sourceBranch=refs/tags/@confirmit/react-table@9.5.0&targetRepoId=1246) (2021-03-08)

### Features

- add autoResetExpanded prop ([HUB-7896](https://jiraosl.firmglobal.com/browse/HUB-7896)) ([5e097d1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5e097d199fe43101fd72d060cecdd4d0e279ce58))

## [9.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.4.0&sourceBranch=refs/tags/@confirmit/react-table@9.4.1&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-table

# [9.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.3.5&sourceBranch=refs/tags/@confirmit/react-table@9.4.0&targetRepoId=1246) (2021-03-03)

### Features

- add ability to disable search field ([NPM-735](https://jiraosl.firmglobal.com/browse/NPM-735)) ([f5bb9f8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f5bb9f8b362a59b26958be28cf1e4e7a41dde568))

## [9.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.3.4&sourceBranch=refs/tags/@confirmit/react-table@9.3.5&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-table

## [9.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.3.3&sourceBranch=refs/tags/@confirmit/react-table@9.3.4&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-table

## [9.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.3.2&sourceBranch=refs/tags/@confirmit/react-table@9.3.3&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-table

## [9.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.3.1&sourceBranch=refs/tags/@confirmit/react-table@9.3.2&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-table

# [9.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.2.1&sourceBranch=refs/tags/@confirmit/react-table@9.3.0&targetRepoId=1246) (2021-02-10)

### Bug Fixes

- add back padding on plug-in-columns to avoid IconButtons sticking to the edge of table which does not look good ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([0ee7dd7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0ee7dd75865684a56c26af5f9f301e81c64c9d65))
- align checkbox to cell center, and remove specific rules for first column, as it causes issues depending on which column is first (there should not be any padding at all for expand toggle f.ex) ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([1244f40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1244f40f9a391ac07f2431b06b98f23964c5487c))
- align expand/collapse toggle vertically centered ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([5af335d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5af335dc306c3372a7e48eb90df7d08d20dac7f9))
- empty placeholder option didnt take up any space, and caused a line-break when not the first option ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([0e6d1a4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0e6d1a4b19c77dd6be380d1e5cd30d8bcc64abdd))
- set correct min-height of table contents ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([6073b14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6073b1483b9f06ca1c1789f7e33923fa45e2009a))
- should use IconButton for expand column ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([ba043b2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ba043b2287b1468c1b2c0892abafdd1c79eafd17))
- size and spacing between selection and expand toggle ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([0962bbf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0962bbf24e9ad7245f27b1b6234fd6af1f64285c))
- sticky waitbutton styles ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([e8376bf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e8376bf1a29003cd0e35a6c42c101eb7cc2d852f))
- vertical alignment of checkbox in table was not centered ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([bd555de](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bd555ded3b77a893b96354c8fe1c97768a92e29d))

### Features

- support dropdown-menu for table options ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([4388a31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4388a3168394257df3b94348bb524d84a2189ca3))
- support sub-menus in dropdown ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([2e29b34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2e29b348cfde68fe21e4c7ea6224c7cd0fef5558))

## [9.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.2.0&sourceBranch=refs/tags/@confirmit/react-table@9.2.1&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-table

## [9.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?sourceBranch=refs%2Fheads%2Ffeature%2FNPM-526-table-support-drawer-concept&targetRepoId=1246) (2020-01-29)

### Features

- add drawers: new content props are `renderDrawer` and `isRowExpandable` ([NPM-526](https://jiraosl.firmglobal.com/browse/NPM-526))

## [9.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.16&sourceBranch=refs/tags/@confirmit/react-table@9.1.17&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.15&sourceBranch=refs/tags/@confirmit/react-table@9.1.16&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.14&sourceBranch=refs/tags/@confirmit/react-table@9.1.15&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.13&sourceBranch=refs/tags/@confirmit/react-table@9.1.14&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.11&sourceBranch=refs/tags/@confirmit/react-table@9.1.12&targetRepoId=1246) (2020-12-18)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.10&sourceBranch=refs/tags/@confirmit/react-table@9.1.11&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.9&sourceBranch=refs/tags/@confirmit/react-table@9.1.10&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.8&sourceBranch=refs/tags/@confirmit/react-table@9.1.9&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.7&sourceBranch=refs/tags/@confirmit/react-table@9.1.8&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.6&sourceBranch=refs/tags/@confirmit/react-table@9.1.7&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.5&sourceBranch=refs/tags/@confirmit/react-table@9.1.6&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.4&sourceBranch=refs/tags/@confirmit/react-table@9.1.5&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.3&sourceBranch=refs/tags/@confirmit/react-table@9.1.4&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.1.0&sourceBranch=refs/tags/@confirmit/react-table@9.1.1&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-table

## [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.0.6&sourceBranch=refs/tags/@confirmit/react-table@9.1.0&targetRepoId=1246) (2020-11-24)

### Features

- add disabled property to menu option ([NPM-627](https://jiraosl.firmglobal.com/browse/NPM-627))

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.0.3&sourceBranch=refs/tags/@confirmit/react-table@9.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-table

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.0.2&sourceBranch=refs/tags/@confirmit/react-table@9.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-table

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@9.0.1&sourceBranch=refs/tags/@confirmit/react-table@9.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-table

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@8.1.1&sourceBranch=refs/tags/@confirmit/react-table@9.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@8.1.1&sourceBranch=refs/tags/@confirmit/react-table@9.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@8.1.1&sourceBranch=refs/tags/@confirmit/react-table@8.1.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-table

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@8.0.0&sourceBranch=refs/tags/@confirmit/react-table@8.1.0&targetRepoId=1246) (2020-11-12)

### Bug Fixes

- pass command isntead of name to data-react-table-option attribute ([NPM-604](https://jiraosl.firmglobal.com/browse/NPM-604)) ([64f6352](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/64f63520fe197e3c27c965adcc01c4cb853b99d4))

### Features

- add iconPath property to menu option, deprecate icon property ([NPM-604](https://jiraosl.firmglobal.com/browse/NPM-604)) ([aaa5cf0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/aaa5cf01f1f95d7966ec0149eeefb40c8749c221))

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.2.3&sourceBranch=refs/tags/@confirmit/react-table@8.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.2.2&sourceBranch=refs/tags/@confirmit/react-table@7.2.3&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-table

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.2.1&sourceBranch=refs/tags/@confirmit/react-table@7.2.2&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-table

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.2.0&sourceBranch=refs/tags/@confirmit/react-table@7.2.1&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-table

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.1.4&sourceBranch=refs/tags/@confirmit/react-table@7.2.0&targetRepoId=1246) (2020-10-26)

### Features

- pass row data to callback. add `optionsMenu.optionsOrder` to determine order of available commands ([NPM-578](https://jiraosl.firmglobal.com/browse/NPM-578)) ([724892d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/724892db829594cf8a8a781727a09fd7e2d46f60))

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.1.3&sourceBranch=refs/tags/@confirmit/react-table@7.1.4&targetRepoId=1246) (2020-10-22)

**Note:** Version bump only for package @confirmit/react-table

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.1.2&sourceBranch=refs/tags/@confirmit/react-table@7.1.3&targetRepoId=1246) (2020-10-21)

### Bug Fixes

- add wrapper around load more button ([NPM-574](https://jiraosl.firmglobal.com/browse/NPM-574)) ([8ccd113](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8ccd1131120aac982ec7e2821f96c85738b17db2))

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.1.1&sourceBranch=refs/tags/@confirmit/react-table@7.1.2&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-table

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.13&sourceBranch=refs/tags/@confirmit/react-table@7.1.0&targetRepoId=1246) (2020-10-15)

### Features

- add "href" property to command menu option ([NPM-564](https://jiraosl.firmglobal.com/browse/NPM-564)) ([081d72a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/081d72a19386706936fa2ef67d20e4c555a221a8))

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.11&sourceBranch=refs/tags/@confirmit/react-table@7.0.12&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-table

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.10&sourceBranch=refs/tags/@confirmit/react-table@7.0.11&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-table

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.8&sourceBranch=refs/tags/@confirmit/react-table@7.0.9&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-table

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.3&sourceBranch=refs/tags/@confirmit/react-table@7.0.4&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-table

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.2&sourceBranch=refs/tags/@confirmit/react-table@7.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-table

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@7.0.0&sourceBranch=refs/tags/@confirmit/react-table@7.0.1&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-table

## 7.0.0

- **BREAKING**
  - The "icon" prop returned from getEmptyStateProps() of Table must return a string svg path, not a react component

Features:

- Added "alignment" prop added columns (left, center, right), available via exported CELL_ALIGNMENT. Defaults to left.

Fix:

- Possible to set "icon" or "text" prop via getEmptyStateProps() without having to override both.

## [6.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@6.1.8&sourceBranch=refs/tags/@confirmit/react-table@6.1.9&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-table

## [6.1.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@6.1.5...@confirmit/react-table@6.1.6) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-table

## [6.1.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@6.1.1...@confirmit/react-table@6.1.2) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-table

# [6.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@6.0.2...@confirmit/react-table@6.1.0) (2020-09-01)

### Features

- defaultExpandedRows property in table component ([NPM-510](https://jiraosl.firmglobal.com/browse/NPM-510)) ([d5335aa](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/d5335aa233a915851d5eeb417e49bbd13669bd5d))

## [6.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@5.0.5...@confirmit/react-table@5.1.0) (2020-08-20)

**BREAKING** Added `totalCount` prop and removed `pageCount` prop
**Feat:** Added `pageNumber` property to onPageChange

## [5.0.10](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@5.0.9...@confirmit/react-table@5.0.10) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-table

## [5.0.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@5.0.8...@confirmit/react-table@5.0.9) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-table

## [5.0.5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@5.0.4...@confirmit/react-table@5.0.5) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-table

## [5.0.4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-table@5.0.3...@confirmit/react-table@5.0.4) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-table

## CHANGELOG

### v5.0.0

- **BREAKING**
  - Toolbar props added: `title` and `count`. `children` should not longer be used to display table titles
- Use search options in toolbar SearchField component
- Changed Pager type from page to range
- Added empty state component, with `getEmptyStateProps` and `showEmptyState` props
- Added `types` props for default and mobile tables
- Added `onLoadMore` and `isMoreLoading` props

### v4.0.6

- Fix: row width bug

### v4.0.5

- Fix: selection and expansion column should now be sticky
- Fix: firefox bug with sticky option menu column stacking on top of last column

### v4.0.2

- Remove flex in cells to fix ellipsis
- Reorganized table markup so sticky elements work properly. Now table content is scrollable instead of table container

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Some markup adjustments
- Place options menu in first column in IE11 since it doesn't support sticky
- Fixed FF bug with column sorting

### v3.0.0

- **BREAKING**

  - onSelect now returns a record of selected rows by id
  - UI changes to option menu: now only display kebab menu when there are more than 3 options
  - UI changes to select checkbox column and option menu column: now they are fixed width

- Feature: sub rows support. sub row expand toggle will automatically be displayed if a data row has 'subRows' property.
  An option with 'Expand All' and 'Collapsed All' is displayed in the option menu column header
- Feature: added `onExpand` callback in Content component, it returns a record of expanded rows by id
- Fix: onSelect should not trigger on mount

### v2.2.11

- Fixed problems with markup in Chrome and IE11
- Changed cell side padding to match spec
- Fixed problem with shifted column headers

### v2.2.9

- Fixed problems with markup in IE11

### v2.2.0

- Removing package version in class names.

### v2.1.4

- Fix: Don't show search-field as minimizable

### v2.1.1

- Added suppressHeaderTooltip column properties

### v2.0.0

- **BREAKING**

  - Renamed options menu field from `options` to `getOptions`, which now contains a function that returns an array of options,
    so options can be modified for specific row

- Fixed PropTypes
- Edited searchField max width to match mockup
- Content loading indicator became bigger and now overlays content

### v1.0.0

- **BREAKING**:
  - Major UI changes to @confirmit/react-search-field

### v0.0.1

- Initial release
