# Jotunheim React Table

## Usage

`data` and `columns` props need to be **memoized** with React.useMemo.

### Columns

`columns` prop is list of columns with the shape:

```
{
    // header title or component
    Header: string || element // required
    // accessor by row data key or accessor function
    accessor: string || function // required
    Cell: function // Custom cell render
    // Example: Cell: (cellProps) => <div/>
    columns: column[] // nested columns
    disableSortBy: boolean
    sticky: boolean //
    width: number
    suppressTooltip: boolean
    suppressHeaderTooltip: boolean
    suppressTruncation: boolean
    suppressHeaderTruncation: boolean
    disableSortBy: boolean
    alignment: CellAlignment
    isFixedWidth: boolean
    getCellBackgroundColor: (cellProps) => string
}
```

### Data

`data` prop is list of row data, where each row's cell value can be accessed by the column's accessor property. The
table will render all the rows in this list. Sorting and pagination of this data must be handled outside of this
component.

### Sorting

Sorting is controlled by the following props:

- `onSort` ({sortField, sortOrder}) => void
- `sortField` this is the column identifier of the sorted column
- `sortOrder` either 'asc' or 'desc'

`onSort` needs to be memoized.

Leave `onSort` as `undefined` to disable table sorting. Individual columns can be disabled by flagging `disableSortBy`
as `true` to the column node.

### Pagination

Pagination is controlled by the following props:

- `onPageChange` ({pageIndex}) => void
- `pageCount` number of pages
- `pageSize` number of rows per page
- `pageNumber` current page number

Leave `onPageCount` as `undefined` to hide the pager.

### Row Selection

- `onSelect` (selectedRowsById: Record<string, obj>) => void
- `defaultSelectedRows`: Array\<string\>. Array of row ids which should be selected by default.
- `autoResetSelectedRows`: boolean, false by default. Enabling this will automatically clear the selected rows if data
  is changed. Ref [table docs](https://react-table.tanstack.com/docs/api/useRowSelect#table-options)

`onSelect` needs to be memoized.

Row selection can be controlled via `isSelected` boolean key in row data, and defining onSelect will render a sticky
checkbox column on the left side of the table.

### Row Expansion

- `onExpand` (expandedRowsById: Record<string, obj>) => void
- `defaultExpandedRows`: Array\<string\>. Array of row ids, which should be expanded by default
- `autoResetExpanded`: boolean, equals false by default. When true, the expanded state will automatically reset if data
  is changed

`onExpand` needs to be memoized.

Row expansion can be controlled via `expanded` boolean key in row data.

Expand Toggle column will automatically render if a row has `subRows` property.

Option menu column will also render with a header with 'Expand All' and 'Collapse All' actions.

### Drawer

- `defaultExpandedRows`: Array\<string\>. Array of row ids, which should be expanded by default
- `renderDrawer`: function that accepts row and returns content for drawer
- `isRowExpandable`: function that accepts row and should return boolean. Use it if you do not need drawer for the
  current row
- `autoResetExpanded`: boolean, set to true if renderDrawer function defined.

Expand Toggle column will automatically render if a row has `renderDrawer` property.

If drawer is used, 'Expand All' and 'Collapse All' actions won't be available in a table header.

### Options menu

`optionsMenu` is callback property which should return a particular shape:

- `getOptions` (required) - function that should return an array of available commands per row. Accepts `itemId, item`
  as arguments. Type and amount of commands may be different for each item and can depend on item permission or type for
  example. Command properties:

  - `command` (required) - identifier of command
  - `name` (required) - displayed name of command, shown as tooltip
  - `iconPath` (required) - svg path of icon
  - `icon` (deprecated) - react component which renders an icon
  - `href` (optional) - if assigned buttons are renders as `<a />` element with specified `href`
  - `disabled` (optional) - disables option in MenuItemLink and/or IconButton. Options may sometimes need to be disabled
    based on the row data while still requiring to show a disabled icon to correctly align all options in the column.

- `onSelect` (required) - function that is called when user clicks on the command. Accepts `itemId, item` as arguments.

- `optionsOrder` (optional) - array of command identifiers which determines the visible order of commands. If not
  specified the order is computed from all items and might not be accurate if commands are different in different row.
  Must be considered to use in tables with a big amount of data to skip computation algorithm.

- `enableContextMenu` (optional) - boolean key to show a context menu per row in the Table.

```js
const getOptionsMenu = {
  optionsOrder: ['edit', 'duplicate', 'replace', 'delete'], // Optional parameter to determine the order of items
  onSelect: (itemId, item) => {
    switch (selectedItem) {
      case 'edit':
        startEditing();
        break;
    }
  },
  getOptions: (itemId, item) => [
    {
      command: 'edit',
      name: 'Edit',
      href: '/edit', // Optional "href". If specified command buttons rendered as <a /> elements.
      disabled: true, // Optional
      icon: (props) => <Icon path={pencil} {...props} />,
    },
  ],
};
```

### Column Filtering

Column filtering is controlled by these props in Table.Content:

- `onApplyFilter` (filter: FilterValue) => void. Returns the new/updated filter
- `onToggleFilter`: (filterId: string) => void. Returns the filterId that is toggled to idle or active state.
- `onDeleteFilter`: (filterId: string) => void. Returns the filterId to be deleted.
- `filterValues`: Array\<FilterValue\>. This is used to control the column filters.

where FilterValue is defined to be:

- text/numeric filter type:

```js
{
  id: string; // id of the column header
  operator: string; // One of the OPERATOR
  type: string; // ColumnFilterType.Text | ColumnFilterType.Numeric
  value: string | number; // value of the filter
}
```

- date filter type:

```ts
{
  id: string; // id of the column header
  type: string; // ColumnFilterType.date
  value: {
    optionId: string; // can either be QuickDate or CustomDate
    dateStart? : moment; // moment object of the custom dateStart (only in CUSTOM_DATE_OPTION)
    dateEnd? : moment; // moment object of the custom dateEnd (only in CUSTOM_DATE_OPTION)
  }
}
```

- multiselect filter type:

```js
{
  id: string; // id of the column header
  operator: string; // One of the OPERATOR
  type: string; // COLUMN_FILTER_TYPE.multiselect
  value: [{
    label: string; // label of the selected filter option(s)
    value: string; // value of the selected filter option(s)
  }]
}
```

To configure the filter in each column, add filterSettings prop to your columns prop.

filterSettings is defined to be

```js
filterSettings: {
  type: ColumnFilterType; // ColumnFilterType.Numeric || ColumnFilterType.Text || ColumnFilterType.Date || ColumnFilterType.Multiselect
  operators? : Array < {value: Operator, label: string} > // Optional, for numeric or text filters only. If omitted, a default list of operators is used.
  quickDateOptions? : Array < {id: string, label: string} > // Optional, for date filters only. If omitted, a default list of quick date options is used.
  options? : Array < {value: string; label: string} > // Optional, for multiselect filter only. Populates the select dropdown for these filters.
}
```

For example:

```ts
{
  Header: 'Id',
  accessor:  'id',
  sticky:  true,
  width:  100,
  filterSettings: {
    type: ColumnFilterType.Numeric,
    operators: [
      {value: Operator.LessThan, label: 'Less than'},
      {value: Operator.GreaterThan, label: 'Greater than'},
      {value: Operator.LessThanOrEqual, label: 'Less than or equal'},
    ],
  },
},
{
  Header: 'Last Login Date',
  accessor: 'lastLogin',
  width: 200,
  filterSettings: {
    type: ColumnFilterType.Date,
    quickDateOptions:    [
      {label: 'last 7 dys', id: QuickDate.Last7Days},
      {label: 'last 10 days', id: 'last10days'},
      {label: 'last 5 years', id: 'last5year'},
    ]
  },
},
{
  Header: 'Multiselect',
  accessor: 'multiselect',
  width: 200,
  filterSettings: {
    type: ColumnFilterType.Multiselect,
    options: [
      {label: "Option 1", value: "1"}
      {label: "Option 2", value: "2"}
    ]
  }
}
```

## Custom Plugins

React table uses plugins as React hooks to build a table instance. `useCheckbox`, `useOptionsMenu`, `useSortHeader`
, `useStickyColumns`, `useExpandToggle` are custom plugins used with public react-table plugins. Please review the
documentation at https://react-table.js.org/ to find out more about the plugin pattern.

## Scrolling

If you don't need component-level table scrolling there is a prop `usePageScroll` which disables it and allows browser
page to handle scrolling

## Typescript

Since version 13.1.0 react-table has built-in Typescript support There are no breaking changes in API, but it is
possible that Typescript compiler finds some type inconsistency in application

These are the most common case you might face when if you start using Typescript with that version of library

- `Type 'string' is not assignable to type 'SortOrder'.`
  &ndash; you should convert type of sortOrder field in application to either `SortOrder`

- `Types of parameters '[SOME_TYPE]' and 'original' are incompatible.`
  &ndash; this error may occur on different properties such as `getRowId`, `isRowExpandable`, `renderDrawer`. The cause
  of the error is that type of `data` property does not match with type of `orinal` parameter passed to functions above.
  This is usually happens if when data is formatted before passing to component. Consider using `Cell` property on
  columns to format data instead and pass non-formatted data to table. Or introduce type of formatted data item and use
  it.

- `Types of property 'data' are incompatible.` &ndash; most likely types of columns and data properties could not be
  inferred correctly. Use built-in type for DataItem, assign types on intermediate variables for data and columns if
  needed, it also helps to find errors.

- `Index signature for type 'string' is missing in type '[SOME_TYPE]'`. &ndash; most likely that DataItem type is
  defined as `interface`. Table accept data items that inherits Record<string, unknown>. So you should extend you your
  interface from `Record<string, unknown>` or define it as `type` instead of `interface`

- If you have your own types for columns and filter values, consider importing built-in types.

- There could be errors if you pass non-existent properties to table component
