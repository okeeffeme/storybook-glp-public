import {ColumnInterfaceBasedOnValue} from 'react-table';
import {CELL_ALIGNMENT, COLUMN_FILTER_TYPE} from './constants';
import {
  OPERATOR,
  QUICK_DATE_OPTION,
  CUSTOM_DATE_OPTION,
} from './plugins/useColumnFilters/constants';
import {
  OptionsMenu,
  Option,
  OnSelectHandler,
} from './plugins/useOptionsMenu/types';
import {
  CellAlignment,
  ColumnFilterType,
  BatchOperationOption,
  ColumnOption,
  Language,
  TableType,
  CellAlignmentLiteral,
  ColumnFilterTypeLiteral,
  Column,
} from './types';
import {
  Operator,
  CustomDate,
  QuickDate,
  FilterValue,
} from './plugins/useColumnFilters/types';
import Table from './components/Table';
import Content, {
  ContentProps,
  GetTableEmptyStatePropsFn,
} from './components/Content';
import Toolbar, {ToolbarProps} from './components/Toolbar';
import {SortOrder, SortParams} from './plugins/useSortHeader/types';

export {
  Table,
  Content,
  Toolbar,
  CellAlignment,
  ColumnFilterType,
  Operator,
  QuickDate,
  CustomDate,
  Language,
  TableType,
  SortOrder,
  CELL_ALIGNMENT,
  COLUMN_FILTER_TYPE,
  OPERATOR,
  QUICK_DATE_OPTION,
  CUSTOM_DATE_OPTION,
};

export type {
  ContentProps,
  ToolbarProps,
  BatchOperationOption,
  ColumnOption,
  SortParams,
  ColumnFilterTypeLiteral,
  CellAlignmentLiteral,
  OptionsMenu,
  Option,
  OnSelectHandler,
  FilterValue,
  Column,
  ColumnInterfaceBasedOnValue,
  GetTableEmptyStatePropsFn,
};

export default Table;
