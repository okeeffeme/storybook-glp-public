import {ReactElement, ReactNode} from 'react';
import {
  Column as TableColumn,
  UseFiltersColumnOptions,
  UseGlobalFiltersColumnOptions,
  UseGroupByColumnOptions,
  UseResizeColumnsColumnOptions,
  UseSortByColumnOptions,
} from 'react-table';
import {UseStickyColumnsColumnOptions} from './plugins/useStickyColumns/types';
import {UseColumnFiltersColumnOptions} from './plugins/useColumnFilters/types';

export enum TableType {
  Default = 'default',
  Mobile = 'mobile',
}

export type TableTypeLiteral = 'default' | 'mobile';

export enum CellAlignment {
  Left = 'left',
  Center = 'center',
  Right = 'right',
}

export type CellAlignmentLiteral = 'left' | 'center' | 'right';

export enum ColumnFilterType {
  Text = 'text',
  Numeric = 'numeric',
  Date = 'date',
  Multiselect = 'multiselect',
}

export type ColumnFilterTypeLiteral =
  | 'text'
  | 'numeric'
  | 'date'
  | 'multiselect';

export enum Language {
  EN = 'en',
  NB = 'nb',
}

export type LanguageLiteral = 'en' | 'nb';

export type BatchOperationOption = {
  label: ReactNode;
  value: string;
  iconPath?: string;
};

export type ColumnOption = {
  label: string | number;
  value: string | number;
};

export type TableColumnOptions = {
  suppressHeaderTruncation?: boolean;
  suppressHeaderTooltip?: boolean;
  suppressTruncation?: boolean;
  suppressTooltip?: boolean;
  isFixedWidth?: boolean;
  alignment?: CellAlignment | CellAlignmentLiteral;
  getCellBackgroundColor?: (props: ReactElement['props']) => string;
};

export type TableColumnProps = TableColumnOptions;

export type UseColumnOptions<
  D extends Record<string, unknown> = Record<string, unknown>
> = UseFiltersColumnOptions<D> &
  UseGlobalFiltersColumnOptions<D> &
  UseGroupByColumnOptions<D> &
  UseResizeColumnsColumnOptions<D> &
  UseSortByColumnOptions<D> &
  UseStickyColumnsColumnOptions &
  UseColumnFiltersColumnOptions;

export type Column<D extends Record<string, unknown>> = UseColumnOptions<D> &
  TableColumnOptions &
  TableColumn<D>;
