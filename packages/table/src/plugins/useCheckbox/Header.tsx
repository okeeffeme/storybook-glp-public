import React from 'react';
import {Column, UseRowSelectInstanceProps} from 'react-table';

import Checkbox from './Checkbox';

const Header = <D extends Record<string, unknown>>({
  toggleAllRowsSelected,
  isAllRowsSelected = false,
  column: {id},
}: HeaderProps<D>) => {
  return (
    <div onClick={(e) => e.stopPropagation()}>
      <Checkbox
        id={`column_${id}`}
        data-testid="checkbox_column"
        checked={isAllRowsSelected}
        onChange={toggleAllRowsSelected}
        data-react-table-checkbox-header={true}
      />
    </div>
  );
};

export type HeaderProps<D extends Record<string, unknown>> = {
  column: Column<D>;
} & UseRowSelectInstanceProps<D>;

export default Header;
