import React from 'react';
import {Row} from 'react-table';

import Checkbox from './Checkbox';

function Cell<D extends Record<string, unknown>>({
  row: {id, toggleRowSelected, isSelected = false},
}: CellProps<D>) {
  return (
    <div onClick={(e) => e.stopPropagation()}>
      <Checkbox
        data-testid="checkbox_cell"
        data-react-table-checkbox-cell={isSelected ? 'checked' : 'unchecked'}
        id={`row_${id}`}
        checked={isSelected}
        onChange={toggleRowSelected}
      />
    </div>
  );
}

export type CellProps<D extends Record<string, unknown>> = {
  row: Row<D>;
};

export default Cell;
