import {Hooks} from 'react-table';
import {SELECTION_COLUMN_ID} from '../../constants';
import {CellAlignment} from '../../types';
import Header from './Header';
import Cell from './Cell';

const useCheckbox = <D extends Record<string, unknown>>(hooks: Hooks<D>) => {
  hooks.columns.push((columns) => [
    {
      id: SELECTION_COLUMN_ID,
      width: 0,
      sticky: true,
      suppressHeaderTooltip: true,
      suppressTooltip: true,
      suppressTruncation: true,
      alignment: CellAlignment.Center,
      Header,
      Cell,
    },
    ...columns,
  ]);
};

useCheckbox.pluginName = 'useCheckbox';

export default useCheckbox;
