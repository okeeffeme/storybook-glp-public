import React from 'react';

import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {CheckBox} from '@jotunheim/react-toggle';

import classNames from './UseCheckbox.module.css';

const {block} = bemFactory('comd-table-checkbox', classNames);

const Checkbox = ({checked, onChange, id, ...rest}: CheckboxProps) => {
  return (
    <div className={block()} {...extractDataAriaIdProps(rest)}>
      <CheckBox
        id={`row_${id}`}
        checked={checked}
        onChange={(c) => onChange(c as boolean)}
      />
    </div>
  );
};

export type CheckboxProps = {
  checked: boolean;
  onChange: (isChecked: boolean) => void;
  id: string | number;
} & DataAriaIdAttributes;

export default Checkbox;
