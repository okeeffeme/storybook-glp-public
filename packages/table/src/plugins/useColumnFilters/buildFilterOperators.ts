import translate from '../../utils/translation';
import {OPERATORS} from './constants';
import defaultTextFilterOperators from './filterOperators/textFilterOperators';
import defaultMultiSelectFilterOperators from './filterOperators/multiSelectFilterOperators';
import defaultIntegerFilterOperators from './filterOperators/integerFilterOperators';
import {FilterOperator, Operator, OperatorDefinition} from './types';
import {ColumnFilterType, Language, LanguageLiteral} from '../../types';

const filtersMapper = (
  filters: OperatorDefinition[],
  language: Language | LanguageLiteral
): FilterOperator[] => {
  return filters.map(({label, value, hint, valueDisabled}) => {
    const filter: FilterOperator = {
      name:
        label ||
        translate({key: `columnFilter.operators.${value}.label`, language}),
      hint:
        hint ||
        translate({key: `columnFilter.operators.${value}.hint`, language}),
      operator: value,
      valueDisabled,
    };

    return filter;
  });
};

const VALID_OPERATOR_SET = new Set(Object.values(Operator));

const buildFilterOperators = ({
  filterType,
  language,
  operators,
}: {
  filterType: ColumnFilterType;
  language: Language | LanguageLiteral;
  operators?: OperatorDefinition[];
}): FilterOperator[] => {
  if (operators) {
    const validOperators = operators.filter((operator) => {
      const isValid = VALID_OPERATOR_SET.has(operator.value);
      if (!isValid) {
        console.warn(
          `Operator "${operator.value}" is not valid and will not be displayed in the column filter.`
        );
      }
      return isValid;
    });
    return filtersMapper(validOperators, language);
  }

  let operatorList: Operator[] = [];
  switch (filterType) {
    case ColumnFilterType.Numeric:
      operatorList = defaultIntegerFilterOperators;
      break;
    case ColumnFilterType.Text:
      operatorList = defaultTextFilterOperators;
      break;
    case ColumnFilterType.Multiselect:
      operatorList = defaultMultiSelectFilterOperators;
      break;
    default:
      return [];
  }

  return filtersMapper(
    operatorList.map((operator) => OPERATORS[operator]),
    language
  );
};

export default buildFilterOperators;
