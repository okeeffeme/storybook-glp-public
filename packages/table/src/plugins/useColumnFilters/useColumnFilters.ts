import {Hooks} from 'react-table';
import Filter from './Filter';

const useColumnFilters = <D extends Record<string, unknown>>(
  hooks: Hooks<D>
) => {
  hooks.columns.push((columns) => {
    return [...columns].map((column) => ({
      ...column,
      ...(column.filterSettings ? {Filter} : {disableFilters: true}),
    }));
  });
};

export default useColumnFilters;
