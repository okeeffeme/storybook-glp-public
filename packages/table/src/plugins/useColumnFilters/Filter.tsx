import React, {useCallback, useContext, useMemo, useState} from 'react';
import {Column, ColumnGroup, IdType} from 'react-table';
import cn from 'classnames';

import {Popover} from '@jotunheim/react-popover';
import Icon, {filter as filterIcon} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';
import {bemFactory} from '@jotunheim/react-themes';

import buildFilterOperators from './buildFilterOperators';
import FilterBox from './views/FilterBox';

import TableContext from '../../components/TableContext';
import {ColumnFilterType} from '../../types';
import buildDateQuickOptions from './buildDateQuickOptions';

import {FilteringOptions, FilterValue} from './types';

// eslint-disable-next-line import/no-unresolved
import classNames from './Filter.module.css';

const {element} = bemFactory('comd-table-filters', classNames);

const Filter = <D extends Record<string, unknown>>(props: FilterProps<D>) => {
  const {
    columns,
    filterValues,
    filtering: {onApplyFilter},
  } = props;
  const {language} = useContext(TableContext);

  const column = columns.find(
    (column) => column.id === props.column.id
  ) as Column<D>;

  const {operators, type, quickDateOptions, options} =
    column.filterSettings || {};

  const columnFilter = useMemo(
    () => filterValues?.find((filter) => filter.id === props.column.id),
    [filterValues, props.column.id]
  );

  const [isActive, setActive] = useState(false);

  const iconClass = cn(element('filter-icon'), {
    [element('filter-icon', 'filtered')]: isActive,
  });

  const filterOperators = useMemo(
    () =>
      buildFilterOperators({
        filterType: type as ColumnFilterType,
        operators,
        language,
      }),
    [language, type, operators]
  );

  const dateOptions = useMemo(
    () =>
      type === ColumnFilterType.Date
        ? buildDateQuickOptions({options: quickDateOptions, language})
        : [],
    [type, quickDateOptions, language]
  );

  const handleFilterApply = useCallback(
    ({value, operator}) => {
      onApplyFilter?.({
        value: value,
        operator: operator,
        id: column.id as IdType<D>,
        type: type as ColumnFilterType,
      });

      setActive(false);
    },
    [column.id, onApplyFilter, type]
  );

  const handleCancelFilter = useCallback(() => {
    setActive(false);
  }, []);

  const handleToggle = useCallback(() => {
    setActive(!isActive);
  }, [isActive]);

  return (
    <div
      className={iconClass}
      onClick={(e) => {
        e.stopPropagation();
      }}
      data-testid={`columnFilter-${column.id}`}
      data-react-table-column-filter="">
      <Popover
        contentClassName={element('filter-box')}
        data-testid="filter-popover"
        content={
          <FilterBox
            columnHeader={column.Header}
            columnId={column.id}
            filter={columnFilter}
            filterType={type}
            onApplyFilter={handleFilterApply}
            onCancelFilter={handleCancelFilter}
            allowedFilterOperators={filterOperators}
            quickDateOptions={dateOptions}
            options={options}
          />
        }
        open={isActive}
        noArrow={true}
        placement="top-start"
        data-react-table-filter-dialog-id={column.id}
        closeOnOutsideClick={true}
        closeOnTriggerClick={true}
        onToggle={handleToggle}>
        <IconButton
          size={IconButton.sizes.small}
          data-testid="filter-button"
          data-locator={`filter-trigger-${column.id}`}>
          <Icon path={filterIcon} />
        </IconButton>
      </Popover>
    </div>
  );
};

type FilterProps<D extends Record<string, unknown>> = {
  column: Column<D>;
  columns: ColumnGroup<D>[];
  filtering: FilteringOptions;
  filterValues: FilterValue[];
};

export default Filter;
