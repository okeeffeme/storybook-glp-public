import {ColumnFilterType} from '../../types';
import {COLUMN_FILTER_TYPE} from '../../constants';

export type UseColumnFiltersOptions = {
  disableFilters: boolean;
  filterValues?: FilterValue[];
  filtering: FilteringOptions;
};

export type FilteringOptions = {
  onApplyFilter?: (filterValue: FilterValue) => void;
  onToggleFilter?: (id: string) => void;
  onDeleteFilter?: (id: string) => void;
};

export type FilterOperator = {
  name: string;
  hint?: string;
  valueDisabled?: boolean;
  operator: Operator;
};

export type FilterValue = {
  id: string;
  operator: Operator;
  valueDisabled?: boolean;
  isIdle?: boolean;
} & (
  | {
      value?: DateFilterValue;
      type: ColumnFilterType.Date | 'date' | typeof COLUMN_FILTER_TYPE.date;
    }
  | {
      value?: number;
      type:
        | ColumnFilterType.Numeric
        | 'numeric'
        | typeof COLUMN_FILTER_TYPE.numeric;
    }
  | {
      value?: string;
      type: ColumnFilterType.Text | 'text' | typeof COLUMN_FILTER_TYPE.text;
    }
  | {
      value?: string[];
      type:
        | ColumnFilterType.Multiselect
        | 'multiselect'
        | typeof COLUMN_FILTER_TYPE.multiselect;
    }
);

export type OperatorDefinition = {
  label?: string;
  value: Operator;
  valueDisabled?: boolean;
  hint?: string;
};

export type QuickDateOption = {
  id: QuickDate | QuickDateLiteral | string;
  label: string;
};

export type Option = {
  value: string;
  label: string;
};

export enum Operator {
  Equal = 'equal',
  LessThan = 'lessThan',
  GreaterThan = 'greaterThan',
  LessThanOrEqual = 'lessThanOrEqual',
  GreaterThanOrEqual = 'greaterThanOrEqual',
  InList = 'inList',
  InRange = 'inRange',
  IsNull = 'isNull',
  IsNotNull = 'isNotNull',
  Contains = 'contains',
  StartWith = 'startWith',
  EndWith = 'endWith',
  Match = 'match',
  NotMatch = 'notMatch',
}

export enum QuickDate {
  Last1Hour = 'last1Hour',
  Last24Hours = 'last24Hours',
  Last7Days = 'last7Days',
  Last31Days = 'last31Days',
  Empty = 'empty',
  NotEmpty = 'notEmpty',
}

export type QuickDateLiteral =
  | 'last1Hour'
  | 'last24Hours'
  | 'last7Days'
  | 'last31Days'
  | 'empty'
  | 'notEmpty';

export enum CustomDate {
  Custom = 'custom',
}

export type CustomDateLiteral = 'custom';

export type UseColumnFiltersColumnOptions = {
  filterSettings?: {
    type: ColumnFilterType;
    operators?: OperatorDefinition[];
    quickDateOptions?: QuickDateOption[];
    options?: Option[];
  };
};

export type DateFilterValue = {
  optionId?:
    | QuickDate
    | QuickDateLiteral
    | CustomDate
    | CustomDateLiteral
    | string;
  dateStart?: string;
  dateEnd?: string;
};
