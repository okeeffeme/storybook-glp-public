import {CustomDate, Operator, OperatorDefinition, QuickDate} from './types';

export const OPERATORS: Record<Operator, OperatorDefinition> = {
  [Operator.Equal]: {value: Operator.Equal},
  [Operator.LessThan]: {
    value: Operator.LessThan,
  },
  [Operator.GreaterThan]: {
    value: Operator.GreaterThan,
  },
  [Operator.LessThanOrEqual]: {
    value: Operator.LessThanOrEqual,
  },
  [Operator.GreaterThanOrEqual]: {
    value: Operator.GreaterThanOrEqual,
  },
  [Operator.InList]: {
    value: Operator.InList,
  },
  [Operator.InRange]: {
    value: Operator.InRange,
  },
  [Operator.IsNull]: {
    value: Operator.IsNull,
    valueDisabled: true,
  },
  [Operator.IsNotNull]: {
    value: Operator.IsNotNull,
    valueDisabled: true,
  },
  [Operator.Contains]: {
    value: Operator.Contains,
  },
  [Operator.StartWith]: {
    value: Operator.StartWith,
  },
  [Operator.EndWith]: {
    value: Operator.EndWith,
  },
  [Operator.Match]: {
    value: Operator.Match,
  },
  [Operator.NotMatch]: {
    value: Operator.NotMatch,
  },
};

/**
 * @deprecated OPERATOR is deprecated, use Operator enum instead
 */
export const OPERATOR = Operator;

/**
 * @deprecated CUSTOM_DATE_OPTION is deprecated, use CustomDate enum instead
 */
export const CUSTOM_DATE_OPTION = CustomDate;

/**
 * @deprecated QUICK_DATE_OPTION is deprecated, use QuickDate enum instead
 */
export const QUICK_DATE_OPTION = QuickDate;
