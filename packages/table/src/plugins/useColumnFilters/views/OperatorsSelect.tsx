import React from 'react';
import {Select} from '@jotunheim/react-select';

import {FilterOperator, Operator} from '../types';

const FilterOperators = ({
  currentOperator,
  operators,
  onOperatorChanged,
}: FilterOperatorsProps) => {
  return (
    <Select
      data-locator={'operator'}
      value={currentOperator}
      onChange={onOperatorChanged}
      options={operators.map(({operator, name}) => ({
        value: operator,
        label: name,
      }))}
    />
  );
};

type FilterOperatorsProps = {
  currentOperator: Operator;
  operators: FilterOperator[];
  onOperatorChanged: (value: string) => void;
};

export default FilterOperators;
