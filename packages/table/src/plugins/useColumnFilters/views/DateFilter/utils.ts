import {DEFAULT_TAB, Tab} from './constants';
import {CustomDate} from '../../types';

export const getTabByOptionId = (optionId?: string) => {
  if (!optionId) return DEFAULT_TAB;

  return optionId === CustomDate.Custom ? Tab.Custom : Tab.Quick;
};
