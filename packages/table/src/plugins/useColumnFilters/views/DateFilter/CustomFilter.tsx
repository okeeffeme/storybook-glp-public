import React, {useContext} from 'react';
import moment from 'moment';
import DatePicker from '@jotunheim/react-date-picker';
import Fieldset from '@jotunheim/react-fieldset';

import {CUSTOM_OPTION_FIELD} from './constants';
import TableContext from '../../../../components/TableContext';
import translate from '../../../../utils/translation';
import FilterHint from '../FilterHint';
import {CustomDate} from '../../types';

const CustomFilter = ({
  dateEnd,
  dateStart,
  onSelect,
  isValid,
}: CustomFilterProps) => {
  const {language} = useContext(TableContext);

  const handleOnChange = (id, value) => {
    onSelect({
      id,
      value,
    });
  };

  return (
    <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
      <DatePicker
        showClear={true}
        showLabel={true}
        label={'from'}
        hasError={!isValid}
        date={dateStart ? moment(dateStart) : undefined}
        onChange={(momentDate) => {
          handleOnChange(
            CUSTOM_OPTION_FIELD.DateStart,
            momentDate ? momentDate.format('YYYY-MM-DD') : undefined
          );
        }}
      />
      <DatePicker
        showClear={true}
        showLabel={true}
        label={'to'}
        date={dateEnd ? moment(dateEnd) : undefined}
        hasError={!isValid}
        onChange={(momentDate) => {
          handleOnChange(
            CUSTOM_OPTION_FIELD.DateEnd,
            momentDate ? momentDate.format('YYYY-MM-DD') : undefined
          );
        }}
      />
      <FilterHint
        text={translate({
          key: `columnFilter.operators.${CustomDate.Custom}.hint`,
          language,
        })}
      />
    </Fieldset>
  );
};

type CustomFilterProps = {
  dateStart?: string;
  dateEnd?: string;
  onSelect: (item: {id: string; value: string}) => void;
  isValid: boolean;
};

export default CustomFilter;
