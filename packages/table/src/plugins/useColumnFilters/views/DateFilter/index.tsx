import {bemFactory} from '@jotunheim/react-themes';
import Tabs from '@jotunheim/react-tabs';
import React, {useContext, useEffect, useState} from 'react';
import QuickFilters from './QuickFIlters';
import CustomFilter from './CustomFilter';
import {CUSTOM_OPTION_FIELD, Tab} from './constants';
import {getTabByOptionId} from './utils';
import translate from '../../../../utils/translation';
import TableContext from '../../../../components/TableContext';
import {CustomDate, DateFilterValue, QuickDateOption} from '../../types';

import classNames from '../FilterControls.module.css';

const {block, element} = bemFactory('comd-filter-controls', classNames);

const DateFilter = ({
  filterType,
  onFilterChanged,
  value,
  quickDateOptions,
  isValid,
}: DateFilterProps) => {
  const {language} = useContext(TableContext);

  const [selectedTabId, setSelectedTabId] = useState<string>(() =>
    getTabByOptionId(value?.optionId)
  );
  const [selectedOptionId, setSelectedOptionId] = useState(value?.optionId);
  const [customDateStart, setCustomDateStart] = useState(value?.dateStart);
  const [customDateEnd, setCustomDateEnd] = useState(value?.dateEnd);

  const handleCustomDateSelected = ({id, value}) => {
    setSelectedOptionId(CustomDate.Custom);
    if (id === CUSTOM_OPTION_FIELD.DateStart) {
      setCustomDateStart(value);
    } else {
      setCustomDateEnd(value);
    }
  };

  const handleQuickOptionSelected = (optionId) => {
    setCustomDateStart(undefined);
    setCustomDateEnd(undefined);
    setSelectedOptionId(optionId);
  };

  useEffect(() => {
    onFilterChanged({
      value: {
        optionId: selectedOptionId,
        dateStart: customDateStart,
        dateEnd: customDateEnd,
      },
    });
  }, [selectedOptionId, customDateEnd, customDateStart, onFilterChanged]);

  const quickFilterTitle = translate({
    key: `columnFilter.tabs.${Tab.Quick}.tabTitle`,
    language,
  });

  const customFilterTitle = translate({
    key: `columnFilter.tabs.${Tab.Custom}.tabTitle`,
    language,
  });

  return (
    <div
      className={block()}
      data-react-table-column-filter-popover={filterType}>
      <Tabs
        selectedTabId={selectedTabId}
        onTabClicked={(id) => setSelectedTabId(id)}>
        <Tabs.Tab title={quickFilterTitle} id={Tab.Quick} hasPadding={false}>
          <div className={element('tab-content')}>
            <QuickFilters
              quickDateOptions={quickDateOptions}
              selectedOption={selectedOptionId}
              onSelect={handleQuickOptionSelected}
            />
          </div>
        </Tabs.Tab>
        <Tabs.Tab title={customFilterTitle} id={Tab.Custom} hasPadding={false}>
          <div className={element('tab-content')}>
            <CustomFilter
              onSelect={handleCustomDateSelected}
              dateStart={customDateStart}
              dateEnd={customDateEnd}
              isValid={isValid}
            />
          </div>
        </Tabs.Tab>
      </Tabs>
    </div>
  );
};

type DateFilterProps = {
  filterType: string;
  columnId?: string;
  operator: string;
  value: DateFilterValue;
  isValid: boolean;
  onFilterChanged: (filter: {value: DateFilterValue}) => void;
  quickDateOptions: QuickDateOption[];
};

export default DateFilter;
