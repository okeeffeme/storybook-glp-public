import React from 'react';
import {Radio} from '@jotunheim/react-toggle';
import {QuickDate, QuickDateLiteral, QuickDateOption} from '../../types';

const QuickFilters = ({
  onSelect,
  selectedOption,
  quickDateOptions,
}: QuickFiltersProps) => {
  return (
    <div>
      {quickDateOptions.map(({id, label}) => (
        <div key={id}>
          <Radio
            id={id}
            checked={selectedOption === id}
            onChange={() => onSelect(id)}>
            {label}
          </Radio>
        </div>
      ))}
    </div>
  );
};

type QuickFiltersProps = {
  quickDateOptions: QuickDateOption[];
  onSelect: (id: QuickDate | QuickDateLiteral | string) => void;
  selectedOption?: string;
};

export default QuickFilters;
