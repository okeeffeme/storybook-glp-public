export enum Tab {
  Quick = 'quick',
  Custom = 'custom',
}

export const DEFAULT_TAB = Tab.Quick;

export const CUSTOM_OPTION_FIELD = {
  DateStart: 'dateStart',
  DateEnd: 'dateEnd',
};
