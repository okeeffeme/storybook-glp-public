import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './FilterHint.module.css';

const {block} = bemFactory('comd-filter-hint', classNames);

const FilterHint = ({text}: FilterHintProps) => {
  return <span className={block()}>{text}</span>;
};

type FilterHintProps = {
  text?: string;
};

export default FilterHint;
