import React, {useCallback, useContext} from 'react';

import Select from '@jotunheim/react-select';
import TextField from '@jotunheim/react-text-field';
import {bemFactory} from '@jotunheim/react-themes';
import {Fieldset} from '@jotunheim/react-fieldset';

import OperatorsSelect from './OperatorsSelect';
import FilterHint from './FilterHint';
import translate from '../../../utils/translation';
import TableContext from '../../../components/TableContext';

import classNames from './FilterControls.module.css';
import {ColumnFilterType} from '../../../types';
import {FilterOperator, Operator, Option} from '../types';

const {block} = bemFactory('comd-filter-controls', classNames);

const MultiSelectFilter = (props: MultiSelectFilterProps) => {
  const {
    filterType,
    columnId,
    operator,
    value,
    isValid,
    onFilterChanged,
    operators,
    options,
  } = props;

  const operatorProps = operators.find((x) => x.operator === operator);
  const {valueDisabled, hint} = operatorProps || {};
  const {language} = useContext(TableContext);

  const onOperatorChanged = useCallback(
    (operator) => {
      onFilterChanged({
        operator,
        value,
      });
    },
    [value, onFilterChanged]
  );

  const onValueChanged = useCallback(
    (value) => {
      onFilterChanged({operator, value});
    },
    [operator, onFilterChanged]
  );

  const useSelect =
    operator === Operator.Match || operator === Operator.NotMatch;

  return (
    <div
      className={block()}
      data-react-table-column-filter-popover={filterType}>
      <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
        <OperatorsSelect
          currentOperator={operator}
          operators={operators}
          onOperatorChanged={onOperatorChanged}
        />
        {useSelect ? (
          <Select
            id={`filter-value-${columnId}`}
            value={value}
            onChange={onValueChanged}
            isMulti={true}
            disabled={valueDisabled}
            isCreatable={false}
            placeholder={translate({
              key: `columnFilter.filterBox.placeHolder`,
              language,
            })}
            isClearable={true}
            options={options}
            data-locator={'filter-value'}
            error={!isValid}
          />
        ) : (
          <TextField
            id={`filter-value-${columnId}`}
            placeholder={translate({
              key: `columnFilter.filterBox.placeHolder`,
              language,
            })}
            data-locator={'filter-value'}
            value={(value as string) || ''}
            onChange={onValueChanged}
            error={!isValid}
            disabled={valueDisabled}
          />
        )}
        <FilterHint text={hint} />
      </Fieldset>
    </div>
  );
};

type MultiSelectFilterProps = {
  filterType: ColumnFilterType;
  columnId?: string;
  operator: Operator;
  value: string | string[];
  isValid: boolean;
  onFilterChanged: (filter: {
    operator: Operator;
    value: string | string[];
  }) => void;
  operators: FilterOperator[];
  options: Option[];
};

export default MultiSelectFilter;
