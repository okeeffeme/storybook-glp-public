import React, {useCallback, useContext} from 'react';

import TextField from '@jotunheim/react-text-field';
import {bemFactory} from '@jotunheim/react-themes';
import {Fieldset} from '@jotunheim/react-fieldset';

import OperatorsSelect from './OperatorsSelect';
import FilterHint from './FilterHint';
import translate from '../../../utils/translation';
import TableContext from '../../../components/TableContext';
import {FilterOperator, Operator} from '../types';
// eslint-disable-next-line import/no-unresolved
import classNames from './FilterControls.module.css';

const {block} = bemFactory('comd-filter-controls', classNames);

const TextAndNumberFilter = (props: TextAndNumberFilterProps) => {
  const {
    filterType,
    columnId,
    operator,
    value,
    isValid,
    onFilterChanged,
    operators,
  } = props;
  const operatorProps = operators.find((x) => x.operator === operator);
  const {valueDisabled, hint} = operatorProps || {};
  const {language} = useContext(TableContext);

  const onOperatorChanged = useCallback(
    (operator) => {
      onFilterChanged({
        operator,
        value,
      });
    },
    [value, onFilterChanged]
  );

  const onValueChanged = useCallback(
    (value) => {
      onFilterChanged({operator, value});
    },
    [operator, onFilterChanged]
  );

  return (
    <div
      className={block()}
      data-react-table-column-filter-popover={filterType}
      data-testid="text-and-number-filter"
      data-operator={operator}>
      <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
        <OperatorsSelect
          currentOperator={operator}
          operators={operators}
          onOperatorChanged={onOperatorChanged}
        />
        <TextField
          id={`filter-value-${columnId}`}
          placeholder={translate({
            key: `columnFilter.filterBox.placeHolder`,
            language,
          })}
          data-locator={'filter-value'}
          value={value || ''}
          onChange={onValueChanged}
          error={!isValid}
          disabled={valueDisabled}
        />
        <FilterHint text={hint} />
      </Fieldset>
    </div>
  );
};

type TextAndNumberFilterProps = {
  filterType: string;
  columnId?: string;
  operator: Operator;
  value: string | number;
  isValid: boolean;
  onFilterChanged: (filter: {
    operator: Operator;
    value: string | number;
  }) => void;
  operators: FilterOperator[];
};

export default TextAndNumberFilter;
