import React, {
  ReactNode,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';

import {Button, ButtonRow} from '@jotunheim/react-button';
import {bemFactory} from '@jotunheim/react-themes';

import {enterKeyPressed} from '../../../utils/enterKeyPressed';
import TextAndNumberFilter from './TextAndNumberFilter';
import integerFilterValidation from '../filterValidators/integerFilterValidator';
import DateFilter from './DateFilter';
import MultiSelectFilter from './MultiSelectFilter';
import dateFilterValidator from '../filterValidators/dateFilterValidator';
import TableContext from '../../../components/TableContext';
import translate from '../../../utils/translation';
import {getDefaultOperator} from '../../../utils/getDefaultOperator';
import {ColumnFilterType} from '../../../types';
import {
  DateFilterValue,
  FilterValue,
  FilterOperator,
  Option,
  QuickDateOption,
} from '../types';

// eslint-disable-next-line import/no-unresolved
import classNames from './FilterBox.module.css';

const {block, element} = bemFactory('comd-filter-box', classNames);

const FilterBox = ({
  columnId,
  columnHeader,
  filter,
  filterType,
  onApplyFilter,
  onCancelFilter,
  allowedFilterOperators,
  quickDateOptions,
  options,
}: FilterBoxProps) => {
  const {language} = useContext(TableContext);
  const [value, setValue] = useState(filter?.value);
  const [operator, setOperator] = useState(() => {
    if (filter) return filter.operator;

    return getDefaultOperator(filterType, allowedFilterOperators);
  });
  const [isValid, setIsValid] = useState(true);

  const handleFilterChanged = useCallback(
    ({operator, value}) => {
      setValue(value);
      setOperator(operator);

      let isValid;
      switch (filterType) {
        case ColumnFilterType.Numeric:
          isValid = integerFilterValidation(operator, value);
          break;
        case ColumnFilterType.Date:
          isValid = dateFilterValidator(value);
          break;
        default:
          isValid = true;
      }

      setIsValid(isValid);
    },
    [filterType]
  );

  const handleFilterApplyClick = useCallback(() => {
    if (isValid && filterType) {
      onApplyFilter({value, operator, type: filterType} as FilterValue);
    }
  }, [value, operator, isValid, onApplyFilter, filterType]);

  const handleKeyPress = useCallback(
    (evt) => {
      if (enterKeyPressed(evt)) {
        handleFilterApplyClick();
      }
    },
    [handleFilterApplyClick]
  );

  const isDisabled = useMemo(() => {
    let result = !isValid;
    const allowEmptyValue = allowedFilterOperators.find(
      (filter) => filter.operator === operator
    )?.valueDisabled;
    if (allowEmptyValue) return false;

    if (isValid) {
      const isValidDate =
        filterType === ColumnFilterType.Date &&
        !!(value as DateFilterValue)?.optionId;
      const isValidMatch =
        filterType === ColumnFilterType.Multiselect &&
        Array.isArray(value) &&
        value.length > 0;

      result = !(isValidDate || isValidMatch || !!value);
    }
    return result;
  }, [allowedFilterOperators, isValid, value, operator, filterType]);

  return (
    <div
      data-locator={`column-filter-${columnId}`}
      onKeyPress={handleKeyPress}
      className={block()}>
      <div
        className={element('header')}
        data-locator={`column-filter-header-${columnId}`}>
        {columnHeader}
      </div>
      {filterType === ColumnFilterType.Date && (
        <DateFilter
          filterType={filterType}
          columnId={columnId}
          value={value as DateFilterValue}
          isValid={isValid}
          operator={operator}
          onFilterChanged={handleFilterChanged}
          quickDateOptions={quickDateOptions}
        />
      )}
      {filterType === ColumnFilterType.Numeric && (
        <TextAndNumberFilter
          filterType={filterType}
          columnId={columnId}
          value={value as number}
          isValid={isValid}
          operator={operator}
          onFilterChanged={handleFilterChanged}
          operators={allowedFilterOperators}
        />
      )}
      {filterType === ColumnFilterType.Text && (
        <TextAndNumberFilter
          filterType={filterType}
          columnId={columnId}
          value={value as string}
          isValid={isValid}
          operator={operator}
          onFilterChanged={handleFilterChanged}
          operators={allowedFilterOperators}
        />
      )}
      {filterType === ColumnFilterType.Multiselect && (
        <MultiSelectFilter
          filterType={filterType}
          columnId={columnId}
          value={value as string[]}
          isValid={isValid}
          operator={operator}
          onFilterChanged={handleFilterChanged}
          operators={allowedFilterOperators}
          options={options as Option[]}
        />
      )}
      <div className={element('buttons')}>
        <ButtonRow>
          <Button
            onClick={onCancelFilter}
            data-locator={`cancel-button-${columnId}`}>
            {translate({key: `columnFilter.filterBox.buttonCancel`, language})}
          </Button>
          <Button
            appearance={Button.appearances.primarySuccess}
            onClick={handleFilterApplyClick}
            data-locator={`filter-button-${columnId}`}
            data-testid={'done-button'}
            disabled={isDisabled}>
            {translate({key: `columnFilter.filterBox.buttonApply`, language})}
          </Button>
        </ButtonRow>
      </div>
    </div>
  );
};

type FilterBoxProps = {
  columnHeader: ReactNode;
  columnId?: string;
  filter?: FilterValue;
  filterType?: ColumnFilterType;
  onFilterModified?: (filter: FilterValue) => void;
  onApplyFilter: (filter: FilterValue) => void;
  onCancelFilter: () => void;
  allowedFilterOperators: FilterOperator[];
  quickDateOptions: QuickDateOption[];
  options?: Option[];
};

export default FilterBox;
