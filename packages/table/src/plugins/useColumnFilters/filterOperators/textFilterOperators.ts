import {Operator} from '../types';

const defaultTextFilterOperators = [
  Operator.Equal,
  Operator.Contains,
  Operator.StartWith,
  Operator.EndWith,
  Operator.IsNull,
  Operator.IsNotNull,
];

export default defaultTextFilterOperators;
