import {Operator} from '../types';

const defaultIntegerFilterOperators = [
  Operator.Equal,
  Operator.LessThan,
  Operator.LessThanOrEqual,
  Operator.GreaterThan,
  Operator.GreaterThanOrEqual,
  Operator.InList,
  Operator.InRange,
  Operator.IsNull,
  Operator.IsNotNull,
];

export default defaultIntegerFilterOperators;
