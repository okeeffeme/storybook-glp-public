import {Operator} from '../types';

const defaultMultiSelectFilterOperators = [
  Operator.Match,
  Operator.NotMatch,
  Operator.IsNull,
  Operator.IsNotNull,
];

export default defaultMultiSelectFilterOperators;
