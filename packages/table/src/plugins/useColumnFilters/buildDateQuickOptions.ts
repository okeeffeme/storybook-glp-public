import translate from '../../utils/translation';
import {QuickDate, QuickDateOption} from './types';

const buildDateQuickOptions = ({options, language}): QuickDateOption[] => {
  if (options) {
    return options;
  }

  return Object.values(QuickDate).map((option) => ({
    id: option,
    label: translate({key: `columnFilter.operators.${option}.label`, language}),
  }));
};

export default buildDateQuickOptions;
