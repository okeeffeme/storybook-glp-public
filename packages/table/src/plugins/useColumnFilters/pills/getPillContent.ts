import translate from '../../../utils/translation';
import {getPillText} from '../../../utils/getPillText';
import {ColumnFilterType, Language, LanguageLiteral} from '../../../types';
import {CustomDate} from '../types';

export const getPillContent = (
  filter,
  columnsById,
  language: Language | LanguageLiteral
) => {
  const {value, type, id} = filter;
  const columnInfo = columnsById[id];
  const columnName = columnInfo?.Header;

  switch (type) {
    case ColumnFilterType.Multiselect:
    case ColumnFilterType.Numeric:
    case ColumnFilterType.Text: {
      return getPillText(columnName, filter, language);
    }
    case ColumnFilterType.Date:
      if (value.optionId !== CustomDate.Custom) {
        // Find label from user defined quickDateOptions first; if it doesn't have label, look up locale
        const optionLabel =
          columnInfo.filterSettings.quickDateOptions?.find(
            (option) => option.id === value.optionId
          )?.label ||
          translate({
            key: `columnFilter.operators.${value.optionId}.label`,
            language,
          });
        return `${columnName}: ${optionLabel}`;
      }

      return `${columnName}: ${
        value.dateStart
          ? value.dateStart
          : translate({
              key: `columnFilter.operators.${CustomDate.Custom}.noStartDate`,
              language,
            })
      } : ${
        value.dateEnd
          ? value.dateEnd
          : translate({
              key: `columnFilter.operators.${CustomDate.Custom}.noEndDate`,
              language,
            })
      }`;
  }

  return '';
};
