import React, {useCallback, useContext, useMemo} from 'react';
import {Column} from 'react-table';

import Chip from '@jotunheim/react-chip';
import {Truncate} from '@jotunheim/react-truncate';
import {bemFactory} from '@jotunheim/react-themes';
import {HorizontalScrollWrapper} from '@jotunheim/react-scroll-wrapper';
import {Tooltip} from '@jotunheim/react-tooltip';
import Icon, {eye, eyeOff} from '@jotunheim/react-icons';

import {getPillContent} from './getPillContent';
import TableContext from '../../../components/TableContext';
import translate from '../../../utils/translation';
import {FilteringOptions, FilterValue} from '../types';

// eslint-disable-next-line import/no-unresolved
import classNames from './FilterPills.module.css';

const {block, element} = bemFactory('comd-filter-pills', classNames);

const FilterPills = <D extends Record<string, unknown>>({
  columns,
  filtering: {onDeleteFilter, onToggleFilter},
  filterValues,
}: FilterPillsProps<D>) => {
  const {language} = useContext(TableContext);

  const columnsById = useMemo(
    () =>
      columns.reduce((acc, column) => {
        acc[String(column.accessor)] = column;
        return acc;
      }, {}),
    [columns]
  );

  const handleFilterDelete = useCallback(
    (id) => {
      onDeleteFilter?.(id);
    },
    [onDeleteFilter]
  );

  const handleFilterToggle = useCallback(
    (id) => {
      onToggleFilter?.(id);
    },
    [onToggleFilter]
  );

  if (filterValues.length === 0) return null;

  return (
    <div className={block()}>
      <HorizontalScrollWrapper>
        {filterValues.map((filterValue) => {
          const pillContent = getPillContent(
            filterValue,
            columnsById,
            language
          );
          const {id, isIdle} = filterValue;
          return (
            <Tooltip key={id} content={pillContent}>
              <div
                className={element('pill')}
                data-testid={`filterpill-${id}`}
                data-react-table-filter-pill-id={id}>
                <Chip
                  showDeleteButton={true}
                  isActive={!isIdle}
                  onDelete={() => handleFilterDelete(id)}
                  actions={
                    onToggleFilter
                      ? [
                          {
                            icon: (
                              <Icon
                                path={isIdle ? eye : eyeOff}
                                data-test-deactivate={isIdle}
                              />
                            ),
                            handler: () => handleFilterToggle(id),
                            tooltip: translate({
                              key: `columnFilter.filterPill.${
                                isIdle ? 'activate' : 'deactivate'
                              } `,
                              language,
                            }),
                          },
                        ]
                      : []
                  }>
                  <div className={element('text')}>
                    <Truncate>{pillContent}</Truncate>
                  </div>
                </Chip>
              </div>
            </Tooltip>
          );
        })}
      </HorizontalScrollWrapper>
    </div>
  );
};

type FilterPillsProps<D extends Record<string, unknown>> = {
  columns: Column<D>[];
  filtering: FilteringOptions;
  filterValues: FilterValue[];
};

export default FilterPills;
