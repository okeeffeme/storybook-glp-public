import {CustomDate, DateFilterValue} from '../types';

export default (value: DateFilterValue) => {
  // If one of quick options is selected, then it is always valid
  if (value.optionId !== CustomDate.Custom) return true;

  const {dateStart, dateEnd} = value;
  // If only one of the the 2 date value is set, then it is always valid
  if ((dateStart && !dateEnd) || (!dateStart && dateEnd)) return true;

  const parsedDateStart = Date.parse(dateStart as string);
  const parsedDateEnd = Date.parse(dateEnd as string);
  return parsedDateStart <= parsedDateEnd;
};
