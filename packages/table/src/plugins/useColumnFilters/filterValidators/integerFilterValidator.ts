import {Operator} from '../types';

const allowedListValues = /^$|^-?\d+(\s-?\d+)*$/; // E.g. '-1 0 1 2 3'
const allowedRangeValues = /^$|^(-?\d+)\s*-\s*(-?\d)+$/; // E.g. '-1 - 4', -1-4, -4 - -1, 1 - 4
const allowedOtherValues = /^$|^-?\d+(\.\d+)*$/; // A possibly negative integer

export default (operator, value) => {
  if (!value) return true;

  const trimmedValue = value.trim();

  switch (operator) {
    case Operator.InList:
      return allowedListValues.test(trimmedValue);
    case Operator.InRange:
      return allowedRangeValues.test(trimmedValue);
    case Operator.Equal:
      return allowedOtherValues.test(trimmedValue);
    case Operator.IsNull:
    case Operator.IsNotNull:
      return true;
    default:
      return allowedOtherValues.test(trimmedValue);
  }
};
