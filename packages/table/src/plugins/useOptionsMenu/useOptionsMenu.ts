import React from 'react';
import {
  Column,
  Hooks,
  makePropGetter,
  TableInstance,
  useGetLatest,
} from 'react-table';

import {OPTIONS_MENU_COLUMN_ID} from '../../constants';
import {CellAlignment} from '../../types';
import Header from './Header';
import Cell from './Cell';
import {getAllCommands} from './getAllCommands';
import {isStickyPositionSupported} from '../../utils/isStickyPositionSupported';
import {UseOptionsMenuInstanceProps} from './types';

const useOptionsMenu = <D extends Record<string, unknown>>(hooks: Hooks<D>) => {
  hooks.getOptionsMenuProps = [getOptionsMenuProps];

  const menuCol: Column<D> = {
    id: OPTIONS_MENU_COLUMN_ID,
    width: 0,
    alignment: isStickyPositionSupported()
      ? CellAlignment.Right
      : CellAlignment.Left,
    suppressHeaderTooltip: true,
    suppressTooltip: true,
    suppressTruncation: true,
    Header,
    Cell,
  };

  // For browsers who doesn't support sticky position we put menu on the left
  if (isStickyPositionSupported()) {
    hooks.columns.push((columns) => [...columns, menuCol]);
  } else {
    hooks.columns.push((columns) => [menuCol, ...columns]);
  }
  hooks.useInstance.push(useInstance);
  hooks.getHeaderProps.push(getHeaderProps);
  hooks.getCellProps.push(getCellProps);
};

const useInstance = <D extends Record<string, unknown>>(
  instance: TableInstance<D>
) => {
  const getInstance = useGetLatest(instance);
  const {getOptionsMenuProps, prepareRow} = instance.getHooks();

  // set getOptionsMenuProps for each row
  prepareRow.push((row) => {
    row.getOptionsMenuProps = makePropGetter(getOptionsMenuProps, {
      instance: getInstance(),
      row,
    });
  });

  const {flatRows, optionsMenu} = instance;
  instance.allCommands = React.useMemo(
    () =>
      getAllCommands({
        flatRows,
        optionsMenu,
      }),
    [flatRows, optionsMenu]
  );
};

const optionsMenuContainerStyle = {
  position: 'sticky',
  right: 0,
  zIndex: 3,
};

const getCellProps = (props, {cell: {column}}) => [
  props,
  {
    style: {
      ...props.style,
      ...(column.id === OPTIONS_MENU_COLUMN_ID && optionsMenuContainerStyle),
    },
  },
];

const getHeaderProps = (props, {column}) => {
  return [
    props,
    {
      style: {
        ...props.style,
        // applies to column and column placeholder for header groups
        ...((column.id === OPTIONS_MENU_COLUMN_ID ||
          (column.placeholderOf &&
            column.placeholderOf.id === OPTIONS_MENU_COLUMN_ID)) &&
          optionsMenuContainerStyle),
      },
    },
  ];
};

const getOptionsMenuProps = <D extends Record<string, unknown>>(
  props: UseOptionsMenuInstanceProps<D>,
  {instance}: {instance: TableInstance<D>}
) => {
  if (!instance.optionsMenu) return [props];
  return [
    props,
    {
      getOptions: instance.optionsMenu.getOptions,
      onSelect: instance.optionsMenu.onSelect,
      enableContextMenu: !!instance.optionsMenu.enableContextMenu,
      optionsOrder: instance.optionsMenu.optionsOrder,
    },
  ];
};

useOptionsMenu.pluginName = 'useOptionsMenu';

export default useOptionsMenu;
