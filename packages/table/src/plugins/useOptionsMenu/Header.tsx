import React, {ReactElement} from 'react';
import {Row} from 'react-table';

import {Dropdown} from '@jotunheim/react-dropdown';
import {IconButton} from '@jotunheim/react-button';
import Icon, {dotsVertical} from '@jotunheim/react-icons';

const Header = <D extends Record<string, unknown>>({
  maxRowDepth,
  toggleAllRowsExpanded,
  renderDrawer,
}: HeaderProps<D>) => {
  if (!maxRowDepth || !!renderDrawer) return null;

  return (
    <div
      data-react-table-option-menu-header={true}
      data-testid="table-options-menu-header">
      <Dropdown
        placement="bottom-end"
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuItem
              onClick={() => {
                toggleAllRowsExpanded(false);
              }}
              data-testid="table-collapse-all-rows">
              Collapse All
            </Dropdown.MenuItem>
            <Dropdown.MenuItem
              onClick={() => {
                toggleAllRowsExpanded(true);
              }}
              data-testid="table-expand-all-rows">
              Expand All
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <IconButton
          data-react-table-options-menu={true}
          data-testid="table-options-menu-trigger">
          <Icon path={dotsVertical} />
        </IconButton>
      </Dropdown>
    </div>
  );
};

export type HeaderProps<
  D extends Record<string, unknown> = Record<string, unknown>
> = {
  isAllRowsExpanded: boolean;
  maxRowDepth: number;
  data: D[];
  expandedRows: Row<D>[];
  toggleAllRowsExpanded: (isExpanded: boolean) => void;
  renderDrawer: (original: D) => ReactElement;
};

export default Header;
