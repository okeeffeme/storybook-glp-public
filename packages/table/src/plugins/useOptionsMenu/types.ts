import {ReactElement} from 'react';
import {DropdownMenuProps, DropdownMenu} from '@jotunheim/react-dropdown';

export type UseOptionsMenuOptions<D extends Record<string, unknown>> = {
  // options menu -- to hide, do not define optionsMenu
  optionsMenu?: OptionsMenu<D>;
};

export type UseOptionsMenuInstanceProps<D extends Record<string, unknown>> =
  UseOptionsMenuOptions<D> & {
    allCommands: string[];
  };
export type UseOptionsMenuRowProps<D extends Record<string, unknown>> = {
  getOptionsMenuProps: () => OptionsMenu<D>;
};

export type OptionsMenu<
  D extends Record<string, unknown> = Record<string, unknown>
> = {
  onSelect?: OnSelectHandler<D>;
  getOptions: (id: string | number, original: D) => Option[];
  optionsOrder?: string[];
  enableContextMenu?: boolean;
};

export type OnSelectHandler<D> =
  | ((command: string, original: D) => void)
  | undefined;

export type Option = {
  command: string;
  name: string;
  disabled?: boolean;
  menu?: ReactElement<DropdownMenuProps, typeof DropdownMenu>;
  href?: string;
  iconPath?: string;
};
