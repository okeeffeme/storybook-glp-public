import React, {useContext} from 'react';
import {Row} from 'react-table';

import {Dropdown} from '@jotunheim/react-dropdown';
import {IconButton} from '@jotunheim/react-button';
import Icon, {dotsVertical} from '@jotunheim/react-icons';
import {PlacementTypes} from '@jotunheim/react-portal-trigger';

import TableContext from '../../components/TableContext';
import {TableType} from '../../types';
import OptionButton from './OptionButton';

import {element} from '../../utils/bem.table';

import {OnSelectHandler, Option} from './types';
import {getOrderedOptions} from '../../utils/getOrderedOptions';

const stopClick = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

export const renderIcon = (option) => {
  return option.iconPath ? <Icon path={option.iconPath} /> : null;
};

export const renderMenu = <D extends Record<string, unknown>>(
  options: Option[],
  onSelect: OnSelectHandler<D>,
  original: D
) => (
  <Dropdown.Menu>
    {options.map((option) => {
      if (option.menu) {
        return (
          <Dropdown.SubMenuTriggerItem
            key={option.command}
            disabled={option.disabled}
            menu={option.menu}
            iconPath={option.iconPath}
            data-react-table-dropdown-option={option.command}>
            <div className={element('row-menu-item')}>{option.name}</div>
          </Dropdown.SubMenuTriggerItem>
        );
      }

      return (
        <Dropdown.MenuItem
          key={option.command}
          disabled={option.disabled}
          iconPath={option.iconPath}
          onClick={(e) => {
            e.stopPropagation();
            /* prevent following link if href is not set */
            if (!option.href) {
              e.preventDefault();
            }
            onSelect?.(option.command, original);
          }}
          href={option.href}
          data-react-table-dropdown-option={option.command}>
          <div className={element('row-menu-item')}>{option.name}</div>
        </Dropdown.MenuItem>
      );
    })}
  </Dropdown.Menu>
);

const renderDropdown = <D extends Record<string, unknown>>(
  options: Option[],
  onSelect: OnSelectHandler<D>,
  original: D
) => {
  return (
    <Dropdown
      placement="bottom-end"
      menu={renderMenu(options, onSelect, original)}>
      <IconButton data-react-table-options-menu="" onClick={stopClick}>
        <Icon path={dotsVertical} />
      </IconButton>
    </Dropdown>
  );
};

const renderRestOptions = <D extends Record<string, unknown>>(
  restCommands: string[],
  availableOptions: Option[],
  onSelect: OnSelectHandler<D>,
  original: D
) => {
  if (!restCommands.length) return null;

  const validRestOptions = getOrderedOptions(availableOptions, restCommands);

  if (!validRestOptions.length)
    return (
      <div
        key={'overflow'}
        className={element('empty-option-placeholder')}
        data-react-table-empty-option={'overflow'}
      />
    );

  const shouldDisplayDropdown = restCommands.length >= 2;
  if (shouldDisplayDropdown) {
    return renderDropdown(validRestOptions, onSelect, original);
  }

  const option = validRestOptions[0];
  return (
    <OptionButton option={option} onSelect={onSelect} original={original} />
  );
};

const Cell = <D extends Record<string, unknown>>({
  row,
  allCommands,
}: CellProps<D>) => {
  const {type} = useContext(TableContext);

  const {getOptions, onSelect} = row.getOptionsMenuProps();

  if (!getOptions || !onSelect) return null;

  const availableOptions = getOptions(row.id, row.original);

  if (type === TableType.Mobile) {
    const options = getOrderedOptions(availableOptions, allCommands);
    return renderDropdown(options, onSelect, row.original);
  }

  const firstTwoCommands = allCommands.slice(0, 2);
  const restCommands = allCommands.slice(2);

  return (
    <>
      {firstTwoCommands.map((command) => {
        const option: Option | undefined = availableOptions.find(
          (option) => option.command === command
        );

        if (option) {
          if (option.menu) {
            return (
              <Dropdown
                key={option.name}
                placement={PlacementTypes.bottomEnd}
                data-react-table-option-trigger={option.command}
                menu={option.menu}>
                <IconButton
                  data-react-table-option={option.command}
                  onClick={stopClick}>
                  {renderIcon(option)}
                </IconButton>
              </Dropdown>
            );
          }

          return (
            <OptionButton
              key={option.name}
              option={option}
              onSelect={onSelect}
              original={row.original}
            />
          );
        }

        return (
          <div
            key={command}
            className={element('empty-option-placeholder')}
            data-react-table-empty-option={command}
          />
        );
      })}
      {renderRestOptions(
        restCommands,
        availableOptions,
        onSelect,
        row.original
      )}
    </>
  );
};

type CellProps<D extends Record<string, unknown>> = {
  row: Row<D>;
  allCommands: string[];
};

export default Cell;
