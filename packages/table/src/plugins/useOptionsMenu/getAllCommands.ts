import {Row} from 'react-table';
import {OptionsMenu} from './types';
import {removeDuplicatesFromCommands} from '../../utils/removeDuplicatesFromCommands';

export const getAllCommands = <D extends Record<string, unknown>>({
  flatRows,
  optionsMenu,
}: {
  flatRows: Row<D>[];
  optionsMenu?: OptionsMenu<D>;
}) => {
  if (!optionsMenu || (!optionsMenu.optionsOrder && !optionsMenu.getOptions))
    return [];
  const optionSet = new Set();

  if (Array.isArray(optionsMenu.optionsOrder)) {
    return removeDuplicatesFromCommands(optionsMenu.optionsOrder);
  }

  const allCommands: string[] = []; // return ordered array
  flatRows.forEach((row) => {
    const options = optionsMenu.getOptions(row.id, row.original);
    options.forEach(({command}) => {
      if (!optionSet.has(command)) {
        allCommands.push(command);
        optionSet.add(command);
      }
    });
  });

  return allCommands;
};
