import React from 'react';

import {IconButton} from '@jotunheim/react-button';
import Tooltip from '@jotunheim/react-tooltip';

import {renderIcon} from './Cell';
import {OnSelectHandler, Option} from './types';

function OptionButton<D extends Record<string, unknown>>({
  option,
  onSelect,
  original,
}: OptionButtonProps<D>) {
  return (
    <Tooltip content={option.name}>
      <IconButton
        disabled={option.disabled}
        onClick={(e) => {
          e.stopPropagation();
          return onSelect?.(option.command, original);
        }}
        href={option.href}
        data-react-table-option={option.command}>
        {renderIcon(option)}
      </IconButton>
    </Tooltip>
  );
}

export type OptionButtonProps<D extends Record<string, unknown>> = {
  option: Option;
  onSelect: OnSelectHandler<D>;
  original: D;
};

export default OptionButton;
