import {bemFactory} from '@jotunheim/react-themes';
import Icon, {arrowDown, arrowUp} from '@jotunheim/react-icons';
import React, {ReactNode} from 'react';

import classNames from './UseSortHeader.module.css';
import {ColumnInstance} from 'react-table';
import {SortOptions} from './types';

const {block} = bemFactory({
  baseClassName: 'comd-table-sort-header-icon',
  classNames,
});

const Header = <D extends Record<string, unknown>>(props: HeaderProps<D>) => {
  const {
    column: {canSort, id},
    sorting = {},
    Header,
  } = props;
  const {sortField, sortOrder} = sorting;

  return (
    <>
      {canSort && id === sortField && (
        <div className={block()}>
          <Icon
            path={sortOrder === 'desc' ? arrowDown : arrowUp}
            size={16}
            data-react-table-header-sort={true}
          />
        </div>
      )}
      {typeof Header === 'function' ? Header(props) : Header}
    </>
  );
};

type HeaderProps<D extends Record<string, unknown>> = {
  column: ColumnInstance<D>;
  sorting: SortOptions;
  Header: ReactNode | ((props: HeaderProps<D>) => ReactNode);
};

export default Header;
