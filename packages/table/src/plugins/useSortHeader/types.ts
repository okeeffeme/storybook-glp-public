import {CSSProperties, MouseEvent} from 'react';

export type UseSortHeaderOptions = {
  sorting: SortOptions;
};

export type UseSortHeaderInstanceProps = UseSortHeaderOptions;

export type UseSortHeaderColumnInstance = {
  getSortHeaderProps: () => {
    onSort?: (e: MouseEvent) => void;
    style?: CSSProperties;
  };
};

export type SortParams = {
  sortField: string;
  sortOrder: SortOrder;
};

export type SortOptions = {
  onSort?: (sortOptions: SortParams) => void;
  sortField?: string;
  sortOrder?: SortOrder;
};

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc',
}
