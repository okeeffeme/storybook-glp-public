import React, {MouseEvent} from 'react';
import {
  ColumnInstance,
  Hooks,
  IdType,
  makePropGetter,
  Meta,
  TableHeaderProps,
  TableInstance,
  useGetLatest,
} from 'react-table';

import Header from './Header';
import {SortOrder} from './types';

// this plugin is used to control sorting since useSortBy plugin is uncontrolled
const useSortHeader = <D extends Record<string, unknown>>(hooks: Hooks<D>) => {
  hooks.getSortHeaderProps = [getSortHeaderProps];
  hooks.allColumns.push((columns) => {
    return columns.map((column) => {
      return {
        ...column,
        // eslint-disable-next-line react/display-name
        Header: (props) => <Header {...props} Header={column.Header} />,
      };
    });
  });
  hooks.useInstance.push(useInstance);
};

function useInstance<D extends Record<string, unknown>>(
  instance: TableInstance<D>
) {
  const {flatHeaders} = instance;
  const getInstance = useGetLatest(instance);
  const {getSortHeaderProps} = instance.getHooks();
  flatHeaders.forEach((column) => {
    column.getSortHeaderProps = makePropGetter(getSortHeaderProps, {
      instance: getInstance(),
      column,
    });
  });
}

const getSortOrder = <D extends Record<string, unknown>>(
  sortField: keyof D | undefined,
  sortOrder: SortOrder | string | undefined,
  columnId: IdType<D>
) => {
  if (sortField !== columnId || sortOrder === SortOrder.Desc)
    return SortOrder.Asc;
  if (sortOrder === SortOrder.Asc) return SortOrder.Desc;

  return SortOrder.Asc;
};

const getSortHeaderProps = <D extends Record<string, unknown>>(
  props: TableHeaderProps,
  {instance, column}: Meta<D, {column: ColumnInstance<D>}>
) => {
  if (!instance.sorting || !column.canSort) return [props];
  const {sortField, sortOrder} = instance.sorting;
  const {onClick, style} = column.getSortByToggleProps();
  return [
    props,
    {
      style,
      onSort: (e: MouseEvent) => {
        onClick?.(e);
        instance.sorting?.onSort?.({
          sortField: column.id,
          sortOrder: getSortOrder(sortField, sortOrder, column.id),
        });
      },
      title: undefined,
    },
  ];
};

useSortHeader.pluginName = 'useSortHeader';

export default useSortHeader;
