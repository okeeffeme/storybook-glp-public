import {CSSProperties} from 'react';
import {ColumnInstance, Hooks, TableInstance} from 'react-table';
import {
  EXPAND_COLUMN_ID,
  SELECTION_COLUMN_ID,
  PLUGIN_COL_PADDING,
  EXPAND_ICON_SIZE,
  SELECTION_ICON_SIZE,
} from '../../constants';

const useStickyColumns = <D extends Record<string, unknown>>(
  hooks: Hooks<D>
) => {
  hooks.columns.push((columns) => {
    return [...columns]
      .sort((a, b) => {
        if (a.id === SELECTION_COLUMN_ID) return -1;
        if (a.sticky && b.id === SELECTION_COLUMN_ID) return 1;
        if (a.sticky && b.sticky) return 0;
        if (a.sticky) return -1;
        return 0;
      })
      .map((column) => ({
        ...column,
      }));
  });

  hooks.getHeaderGroupProps.push((props, {instance}) => [
    props,
    {
      style: {
        ...props.style,
        minWidth: `${instance.totalColumnsWidth}px`,
      },
    },
  ]);

  hooks.getHeaderProps.push((props, meta) => {
    const {instance, column} = meta;
    return [
      props,
      {
        style: {
          ...props.style,
          // applies to column and column placeholder for header groups
          ...((column.sticky ||
            (column.placeholderOf && column.placeholderOf.sticky)) &&
            stickyContainerStyle(instance, column)),
        },
      },
    ];
  });

  hooks.getCellProps.push((props, meta) => {
    const {
      instance,
      cell: {column},
    } = meta;
    return [
      props,
      {
        style: {
          ...props.style,
          ...(column.sticky && stickyContainerStyle(instance, column)),
        },
      },
    ];
  });
};

const stickyContainerStyle = <D extends Record<string, unknown>>(
  tableInstance: TableInstance<D>,
  columnInstance: ColumnInstance<D>
) => {
  let left;
  const isSelectionColumnVisible =
    !tableInstance.state.hiddenColumns?.includes(SELECTION_COLUMN_ID);
  const isExpansionColumnVisible =
    !tableInstance.state.hiddenColumns?.includes(EXPAND_COLUMN_ID);
  if (columnInstance.id === SELECTION_COLUMN_ID) {
    left = 0;
  } else if (columnInstance.id === EXPAND_COLUMN_ID) {
    left = isSelectionColumnVisible
      ? SELECTION_ICON_SIZE + PLUGIN_COL_PADDING
      : 0;
  } else {
    left =
      (isSelectionColumnVisible
        ? SELECTION_ICON_SIZE + PLUGIN_COL_PADDING
        : 0) +
      (isExpansionColumnVisible ? EXPAND_ICON_SIZE + PLUGIN_COL_PADDING : 0) +
      columnInstance.totalLeft;
  }

  const cssProperties: CSSProperties = {
    position: 'sticky',
    left,
    zIndex: 3,
  };

  return cssProperties;
};

useStickyColumns.pluginName = 'useStickyColumns';

export default useStickyColumns;
