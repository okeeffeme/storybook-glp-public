export type UseStickyColumnsOptions = {
  sticky?: true;
};

export type UseStickyColumnsInstanceProps = UseStickyColumnsOptions;

export type UseStickyColumnsColumnOptions = UseStickyColumnsOptions;
