import React from 'react';
import {Row} from 'react-table';

import Icon, {menuDown, menuRight} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import {IconButton} from '@jotunheim/react-button';

import {getSubRowPadding} from './utils';

import classNames from '../useCheckbox/UseCheckbox.module.css';

const {block} = bemFactory('comd-table-checkbox', classNames);

const Cell = <D extends Record<string, unknown>>({
  row: {isExpanded, canExpand, depth, getToggleRowExpandedProps, original},
  isRowExpandable,
}: CellProps<D>) => {
  const {onClick, style} = getToggleRowExpandedProps();

  return canExpand || isRowExpandable(original) ? (
    <span
      style={{
        ...style,
        paddingLeft: getSubRowPadding(depth),
      }}
      className={block()}
      data-react-table-expand-toggle-cell={
        isExpanded ? 'expanded' : 'collapsed'
      }
      data-testid="table-expand-toggle-cell">
      <IconButton size={IconButton.sizes.small} onClick={onClick}>
        <Icon path={isExpanded ? menuDown : menuRight} />
      </IconButton>
    </span>
  ) : (
    <span data-testid="table-empty-toggle-cell" />
  );
};

export type CellProps<D extends Record<string, unknown>> = {
  row: Row<D>;
  isRowExpandable: (original: D) => boolean;
};

export default Cell;
