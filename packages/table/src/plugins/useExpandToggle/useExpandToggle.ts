import React from 'react';

import {EXPAND_COLUMN_ID} from '../../constants';
import {getCellWidth, getSubRowPadding} from './utils';
import Cell from './Cell';
import {getMaxDepth} from '../../utils/getMaxDepth';
import {
  Cell as CellInstance,
  ColumnInstance,
  Meta,
  TableCellProps,
  TableHeaderProps,
  TableInstance,
} from 'react-table';

const useExpandToggle = (hooks) => {
  hooks.useInstance.push(useInstance);
  hooks.getCellProps.push(getCellProps);
  hooks.getHeaderProps.push(getHeaderProps);

  hooks.columns.push((columns) => {
    return [
      {
        id: EXPAND_COLUMN_ID,
        width: 0,
        sticky: true,
        suppressHeaderTooltip: true,
        suppressTooltip: true,
        suppressTruncation: true,
        Header: () => null,
        Cell,
      },
      ...columns,
    ];
  });
};

const useInstance = <D extends Record<string, unknown>>(
  instance: TableInstance<D>
) => {
  const {flatRows, isRowExpandable} = instance;
  instance.maxRowDepth = React.useMemo(
    () =>
      Math.max(
        getMaxDepth({
          flatRows,
        }),
        flatRows.filter((row) => isRowExpandable?.(row.original)).length > 0
          ? 1
          : 0
      ),
    [flatRows, isRowExpandable]
  );
};

const getHeaderProps = <D extends Record<string, unknown>>(
  props: TableHeaderProps,
  {instance, column: {id}}: Meta<D, {column: ColumnInstance<D>}>
) => {
  const {maxRowDepth, allCommands, tableType} = instance;
  const width = getCellWidth({
    columnId: id,
    maxRowDepth,
    defaultCellWidth: props.style?.width,
    optionsCount: allCommands.length,
    tableType,
  });

  return [
    props,
    {
      style: {
        ...props.style,
        width,
      },
    },
  ];
};

const getCellProps = <D extends Record<string, unknown>>(
  props: TableCellProps,
  {
    instance: {headers, columns, maxRowDepth, allCommands, tableType},
    cell: {
      row: {depth},
      column: {id},
    },
  }: Meta<D, {cell: CellInstance<D>}>
) => {
  // The sub row cell of the column right after expand toggle needs to be indented.
  const expandColumnIndex = headers.findIndex(
    (header) => header.id === EXPAND_COLUMN_ID
  );
  const nextColumn = columns[expandColumnIndex + 1];
  const isSubRow = depth > 0;
  const isNextColumnCell = nextColumn && nextColumn.id === id;

  const width = getCellWidth({
    columnId: id,
    maxRowDepth,
    defaultCellWidth: props.style?.width,
    optionsCount: allCommands.length,
    tableType,
  });

  return [
    props,
    {
      style: {
        ...props.style,
        width,
        paddingLeft:
          isSubRow && isNextColumnCell ? getSubRowPadding(depth) : undefined,
      },
    },
  ];
};

useExpandToggle.pluginName = 'useExpandToggle';

export default useExpandToggle;
