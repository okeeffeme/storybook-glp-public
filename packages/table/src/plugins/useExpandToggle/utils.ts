import {Cell} from 'react-table';
import {
  EXPAND_COLUMN_ID,
  MAX_OPTION_ICONS,
  OPTIONS_MENU_COLUMN_ID,
  SELECTION_COLUMN_ID,
  OPTION_ICON_SIZE,
  PLUGIN_COL_PADDING,
  EXPAND_ICON_SIZE,
  SELECTION_ICON_SIZE,
} from '../../constants';
import {TableType, TableTypeLiteral} from '../../types';

const CELL_PADDING_LEFT = 12;

export const getCellWidth = ({
  columnId,
  maxRowDepth,
  defaultCellWidth,
  optionsCount,
  tableType = TableType.Default,
}: {
  columnId: string;
  maxRowDepth: number;
  defaultCellWidth?: string | number;
  optionsCount: number;
  tableType: TableType | TableTypeLiteral;
}) => {
  switch (columnId) {
    case EXPAND_COLUMN_ID: {
      return EXPAND_ICON_SIZE * maxRowDepth + PLUGIN_COL_PADDING;
    }

    case SELECTION_COLUMN_ID: {
      return SELECTION_ICON_SIZE + PLUGIN_COL_PADDING;
    }

    case OPTIONS_MENU_COLUMN_ID: {
      /*
        get width needed for header (depending if there are sub rows)
        and width needed for cell (depending the number of options)
        and get the max of the two
       */
      const headerWidth =
        maxRowDepth > 0 ? OPTION_ICON_SIZE + PLUGIN_COL_PADDING : 0;

      const cellWidth =
        tableType === TableType.Mobile
          ? OPTION_ICON_SIZE + PLUGIN_COL_PADDING
          : OPTION_ICON_SIZE * Math.min(optionsCount, MAX_OPTION_ICONS) +
            PLUGIN_COL_PADDING; // Can only have max of 3 icons in cell

      return Math.max(headerWidth, cellWidth);
    }

    default: {
      return defaultCellWidth;
    }
  }
};

export const getSubRowPadding = (depth: number) =>
  `${depth * EXPAND_ICON_SIZE}px`;

export const calculateDrawerPadding = <D extends Record<string, unknown>>(
  cells: Cell<D>[]
) => {
  const amountOfColumnsBeforeExpand = cells.findIndex((cell) => {
    return cell.column.id === EXPAND_COLUMN_ID;
  });

  return `${
    amountOfColumnsBeforeExpand * OPTION_ICON_SIZE +
    EXPAND_ICON_SIZE +
    CELL_PADDING_LEFT +
    PLUGIN_COL_PADDING
  }px`;
};
