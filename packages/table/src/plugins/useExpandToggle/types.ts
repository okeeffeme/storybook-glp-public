import {ReactNode} from 'react';
import {TableType, TableTypeLiteral} from '../../types';

export type UseExpandToggleOptions<D extends Record<string, unknown>> = {
  tableType: TableType | TableTypeLiteral;
  renderDrawer?: (original: D) => ReactNode;
  isRowExpandable?: (original: D) => boolean;
};

export type UseExpandToggleInstanceProps<D extends Record<string, unknown>> =
  UseExpandToggleOptions<D> & {
    maxRowDepth: number;
  };
