import React from 'react';
import {Language, LanguageLiteral, TableType, TableTypeLiteral} from '../types';

export type TableContextValue = {
  type: TableType | TableTypeLiteral;
  usePageScroll: boolean;
  hasToolbar: boolean;
  language: Language | LanguageLiteral;
};

const TableContext = React.createContext<TableContextValue>({
  type: TableType.Default,
  usePageScroll: false,
  hasToolbar: false,
  language: Language.EN,
});

export default TableContext;
