import React, {ReactNode, useContext, useEffect, useMemo, useRef} from 'react';
import {
  useTable,
  useFilters,
  useSortBy,
  useFlexLayout,
  useExpanded,
  useRowSelect,
  Column,
  Row as TableRow,
} from 'react-table';
import cn from 'classnames';

import Pager, {PagerTypes} from '@jotunheim/react-pager';
import {BusyDots, BusyDotsSizes} from '@jotunheim/react-busy-dots';
import {
  extractDataAriaIdProps,
  arePropsEqual,
  DataAriaIdAttributes,
} from '@jotunheim/react-utils';
import {WaitButton} from '@jotunheim/react-wait-button';

import {
  useOptionsMenu,
  useExpandToggle,
  useCheckbox,
  useSortHeader,
  useStickyColumns,
  useColumnFilters,
} from '../plugins';
import {calculateDrawerPadding} from '../plugins/useExpandToggle/utils';
import {
  SELECTION_COLUMN_ID,
  OPTIONS_MENU_COLUMN_ID,
  EXPAND_COLUMN_ID,
} from '../constants';

import Row from './Row';
import Header from './Header';
import EmptyState, {TableEmptyStateProps} from './EmptyState';
import TableContext from './TableContext';
import {useUpdateCallback} from '../utils/useUpdateCallback';

import FilterPills from '../plugins/useColumnFilters/pills/FilterPills';

import {element} from '../utils/bem.table';
import {UseOptionsMenuOptions} from '../plugins/useOptionsMenu/types';
import {SortOptions} from '../plugins/useSortHeader/types';
import {UseExpandToggleOptions} from '../plugins/useExpandToggle/types';
import {FilteringOptions, FilterValue} from '../plugins/useColumnFilters/types';
import {useZIndexStack} from '@jotunheim/react-contexts';

export function Content<
  D extends Record<string, unknown> & {id?: string | number; subRows?: D[]}
>({
  getRowId = (row, relativeIndex, parent) =>
    parent
      ? [parent.id, relativeIndex].join('.')
      : row.id !== undefined
      ? row.id
      : relativeIndex,
  defaultExpandedRows = [],
  defaultSelectedRows = [],
  autoResetSelectedRows = false,
  getEmptyStateProps = () => ({
    text: undefined, // string
    icon: undefined, // string (svg path)
  }),
  showEmptyState = true,
  loadButtonText = 'Load More',
  columns,
  data,
  optionsMenu,
  onRowClick,
  // sorting
  onSort,
  sortField,
  sortOrder,
  // row selection
  onSelect,
  // row expansion
  onExpand,
  // pagination
  onPageChange,
  totalCount = 1,
  pageNumber = 0,
  pageSize = 1,
  // column filter
  onApplyFilter,
  onToggleFilter,
  onDeleteFilter,
  filterValues,
  // load more
  onLoadMore,
  isMoreLoading,
  isLoading,
  renderDrawer,
  autoResetExpanded,
  isRowExpandable,
  ...rest
}: ContentProps<D>) {
  const containerRef = React.useRef<HTMLDivElement>(null);
  const {type, usePageScroll, hasToolbar, language} = useContext(TableContext);

  if (autoResetExpanded == null) {
    autoResetExpanded = typeof renderDrawer === 'function';
  }

  const filtering = useMemo(
    () => ({
      onApplyFilter,
      onToggleFilter,
      onDeleteFilter,
    }),
    [onApplyFilter, onToggleFilter, onDeleteFilter]
  );

  const hasFilterEnabledColumns = useMemo(
    () => columns.some((column) => !!column.filterSettings),
    [columns]
  );

  const isRowExpandableFn = useMemo(() => {
    return isRowExpandable
      ? isRowExpandable
      : () => typeof renderDrawer === 'function';
  }, [renderDrawer, isRowExpandable]);

  const sorting = useMemo(() => {
    if (!onSort) return {};
    return {onSort, sortField, sortOrder};
  }, [onSort, sortField, sortOrder]);

  const tableProps = useTable<D>(
    {
      initialState: {
        expanded: defaultExpandedRows.reduce(
          (acc: Record<string | number, boolean>, rowId) => {
            acc[rowId] = true;
            return acc;
          },
          {}
        ),
        selectedRowIds: defaultSelectedRows.reduce(
          (acc: Record<string | number, boolean>, rowId) => {
            acc[rowId] = true;
            return acc;
          },
          {}
        ),
      },
      getRowId,
      columns,
      data,
      optionsMenu,

      tableType: type,
      isRowExpandable: isRowExpandableFn,
      renderDrawer,
      // sorting
      manualSortBy: true,
      disableSortRemove: true,
      disableMultiSort: true,
      disableSortBy: !onSort,
      // filtering
      manualFilters: true,
      disableFilters: !filterValues || !onApplyFilter || !onDeleteFilter,
      filterValues,
      filtering,
      language,
      // custom props
      sorting,
      autoResetExpanded,
      autoResetSelectedRows,
      autoResetHiddenColumns: false,
    },
    useFilters,
    useSortBy,
    useFlexLayout,
    useExpanded,
    useRowSelect,
    useExpandToggle,
    useCheckbox, // custom plugin for checkbox component
    useStickyColumns, // custom plugin for sticky horizontal scrolling
    useSortHeader, // custom plugin for sort header styling
    useOptionsMenu, // custom plugin for options menu component
    useColumnFilters // custom plugin for column filters
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows: unpreparedRows,
    prepareRow,
    toggleHideColumn,
    rowsById,
    maxRowDepth,
    disableFilters,
    state: {selectedRowIds: stateSelectedRowIds, expanded: stateExpandedRowIds},
  } = tableProps;

  const rows = useMemo(
    () =>
      unpreparedRows.map((row) => {
        prepareRow(row);
        return row;
      }),
    [unpreparedRows, prepareRow]
  );

  const headerGroupsSuppressTruncation = useMemo(
    () =>
      headerGroups.map(({headers}) => {
        return headers.some((header) => header.suppressHeaderTruncation);
      }),
    [headerGroups]
  );

  let drawerPadding = '0';
  let shouldShowOptionsMenu = false;
  if (optionsMenu) {
    shouldShowOptionsMenu = true;
  } else {
    shouldShowOptionsMenu = !!maxRowDepth && !renderDrawer;
  }

  // effect for toggling checkbox and options menu column from custom plugins
  useEffect(() => {
    toggleHideColumn(SELECTION_COLUMN_ID, !onSelect);
    toggleHideColumn(OPTIONS_MENU_COLUMN_ID, !shouldShowOptionsMenu);
    toggleHideColumn(EXPAND_COLUMN_ID, !maxRowDepth);
  }, [
    toggleHideColumn,
    onSelect,
    onExpand,
    shouldShowOptionsMenu,
    maxRowDepth,
  ]);

  useUpdateCallback({ids: stateSelectedRowIds, callback: onSelect, rowsById});

  useUpdateCallback({ids: stateExpandedRowIds, callback: onExpand, rowsById});

  const isEmptyState = showEmptyState && !isLoading && !rows.length;

  const theadClassNames = cn(element('thead'), {
    [element('thead-page-level-scroll-with-toolbar')]:
      usePageScroll && hasToolbar,
  });

  const contentClassNames = cn(element('content'), {
    [element('content-page-level-scroll')]: usePageScroll,
    [element('content', 'has-toolbar')]: hasToolbar,
  });

  const wrapperClassNames = cn(element('content-wrapper'), {
    [element('content-wrapper', 'no-pagination')]:
      rows.length > 0 && !onLoadMore && !onPageChange && usePageScroll,
  });

  const containerClassNames = cn(element('container'), {
    [element('container--min-height')]: showEmptyState,
  });

  const zIndexStack = useZIndexStack();
  const zIndexRef = useRef<number | undefined>();
  if (!zIndexRef.current) {
    zIndexRef.current = zIndexStack.obtain();
  }

  useEffect(() => {
    return () => {
      if (zIndexRef.current) {
        zIndexStack.release(zIndexRef.current);
      }
    };
  }, [zIndexStack]);

  return (
    <div className={wrapperClassNames}>
      {!!filterValues?.length && (
        <FilterPills
          columns={columns}
          filtering={filtering}
          filterValues={filterValues}
        />
      )}
      {isLoading && (
        <div
          className={element('loading-container')}
          style={{
            zIndex: zIndexRef.current,
          }}
          data-react-table-loading={true}>
          <BusyDots size={BusyDotsSizes.large} />
        </div>
      )}
      <div className={contentClassNames} ref={containerRef}>
        <div className={theadClassNames}>
          {headerGroups.map(({getHeaderGroupProps, headers}, i) => {
            const {key, ...rest} = getHeaderGroupProps();
            return (
              <div
                key={`header-group-${key}`}
                className={cn(element('thead-tr'), {
                  [element('thead-tr', 'with-text-wrap')]:
                    headerGroupsSuppressTruncation[i],
                })}
                {...rest}>
                {headers.map((header, i) => (
                  <Header
                    key={`header-${i}`}
                    {...header}
                    disableFilters={disableFilters}
                    hasFilterEnabledColumns={hasFilterEnabledColumns}
                  />
                ))}
              </div>
            );
          })}
        </div>
        {isEmptyState ? (
          <EmptyState {...getEmptyStateProps()} />
        ) : (
          <div
            className={containerClassNames}
            data-testid="container"
            data-autoResetExpanded={autoResetExpanded}
            {...extractDataAriaIdProps(rest)}
            {...getTableProps()}>
            <div className={element('tbody')}>
              <div {...getTableBodyProps()}>
                {rows.map((rowProps, i) => {
                  if (i === 0) {
                    drawerPadding = calculateDrawerPadding(rowProps.cells);
                  }

                  return (
                    <Row
                      key={rowProps.getRowProps().key}
                      {...rowProps}
                      onRowClick={onRowClick}
                      renderDrawer={renderDrawer}
                      isRowExpandable={isRowExpandableFn}
                      drawerPadding={drawerPadding}
                    />
                  );
                })}
              </div>
            </div>
          </div>
        )}
      </div>
      {onPageChange && rows.length > 0 && (
        <Pager
          type={PagerTypes.range}
          page={pageNumber}
          pageSize={pageSize}
          totalCount={totalCount}
          onPageChanged={(pageNum) => {
            if (!usePageScroll && containerRef.current) {
              containerRef.current.scrollTop = 0;
            }

            return onPageChange({
              pageIndex: pageNum - 1,
              pageSize,
              pageNumber: pageNum,
            });
          }}
        />
      )}
      {onLoadMore && rows.length > 0 && (
        <div
          className={cn(element('wait-button'), {
            [element('wait-button', 'table-scroll')]: !usePageScroll,
          })}>
          <div
            className={cn({
              [element('wait-button-sticky-wrapper')]: usePageScroll,
            })}>
            <WaitButton
              onClick={onLoadMore}
              busy={isMoreLoading}
              appearance={WaitButton.appearances.primaryNeutral}>
              {loadButtonText}
            </WaitButton>
          </div>
        </div>
      )}
    </div>
  );
}

export type GetTableEmptyStatePropsFn = () => TableEmptyStateProps;

export type ContentProps<D extends Record<string, unknown>> = {
  // data prop should always be the rows to render after pagination and sorting actions have been applied
  data: D[];
  columns: Column<D>[];
  onRowClick?: (original: D) => void;
  // accessor function for unique identifier
  getRowId?: (
    row: D,
    relativeIndex: number,
    parent?: TableRow<D>
  ) => string | number;
  // row selection -- to hide, do not define onSelect
  onSelect?: (updatedRowsById: Record<string, TableRow<D>>) => void;
  // row expansion
  onExpand?: (expandedRowsById: Record<string, TableRow<D>>) => void;
  defaultExpandedRows?: (string | number)[];
  defaultSelectedRows?: (string | number)[];
  autoResetSelectedRows?: boolean;
  // pagination -- to hide, do not define onPageChange
  onPageChange?: (options: {
    pageIndex: number;
    pageSize?: number;
    pageNumber: number;
  }) => void;
  totalCount?: number;
  pageNumber?: number;
  pageSize?: number;
  // column filter
  onLoadMore?: () => void;
  loadButtonText?: ReactNode;
  isMoreLoading?: boolean;
  isLoading?: boolean;
  getEmptyStateProps?: GetTableEmptyStatePropsFn;
  showEmptyState?: boolean;
  autoResetExpanded?: boolean;
  filterValues?: FilterValue[];
} & UseOptionsMenuOptions<D> &
  SortOptions &
  Omit<UseExpandToggleOptions<D>, 'tableType'> &
  FilteringOptions &
  DataAriaIdAttributes;

export default React.memo(
  Content,
  arePropsEqual(['onSelect', 'onExpand', 'onSort'])
) as typeof Content;
