import React, {ReactElement} from 'react';
import {Cell as TableCell} from 'react-table';
import cn from 'classnames';
import {Truncate} from '@jotunheim/react-truncate';
import Tooltip from '@jotunheim/react-tooltip';

import {
  EXPAND_COLUMN_ID,
  OPTIONS_MENU_COLUMN_ID,
  SELECTION_COLUMN_ID,
} from '../constants';
import {CellAlignment} from '../types';
import {element} from '../utils/bem.table';

function Cell<D extends Record<string, unknown>>({
  getCellProps,
  render,
  value,
  column,
}: CellProps<D>) {
  const {style, ...restCellProps} = getCellProps();

  const {alignment = CellAlignment.Left, getCellBackgroundColor} = column;

  let cell = render('Cell') as ReactElement;
  const cellProps = cell.props;

  if (!column.suppressTruncation) {
    cell = (
      <Truncate
        data-testid="data-table-cell-truncate"
        data-table-cell-truncate="">
        {cell}
      </Truncate>
    );
  }

  const cellClassNames = cn(
    element('flex-cell'),
    element('flex-cell', `align-${alignment}`),
    {
      [element('flex-cell', 'plug-in-column')]:
        column.id === EXPAND_COLUMN_ID ||
        column.id === SELECTION_COLUMN_ID ||
        column.id === OPTIONS_MENU_COLUMN_ID,
      [element('truncated-cell')]: !column.suppressTruncation,
    }
  );

  return (
    <div
      {...restCellProps}
      style={{
        ...style,
        flex: column.isFixedWidth || !style?.flex ? '0 0 auto' : style?.flex, // react-table doesn't define flex when width is 0, need to add it manually
      }}
      className={element('tbody-td')}
      data-react-table-cell-id={column.id}>
      <div
        className={cellClassNames}
        data-testid={`column-${column.id}`}
        style={
          getCellBackgroundColor && {
            background: getCellBackgroundColor(cellProps),
          }
        }>
        {column.suppressTooltip ? (
          cell
        ) : (
          <Tooltip content={value}>
            <div className={element('tooltip-target')}>{cell}</div>
          </Tooltip>
        )}
      </div>
    </div>
  );
}

export type CellProps<D extends Record<string, unknown>> = TableCell<D>;

export default Cell;
