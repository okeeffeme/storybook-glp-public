import React from 'react';

import {EmptyState} from '@jotunheim/react-empty-state';
import {cancel} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';

// eslint-disable-next-line import/no-unresolved
import classNames from './EmptyState.module.css';

const {block, element} = bemFactory('comd-table-empty-state', classNames);

const TableEmptyState = ({
  icon = cancel,
  text = 'No data found',
}: TableEmptyStateProps) => {
  return (
    <div className={block()}>
      <div
        className={element('wrapper')}
        data-react-table-empty-state={true}
        data-testid="empty-state">
        <EmptyState iconPath={icon} text={text} />
      </div>
    </div>
  );
};

export type TableEmptyStateProps = {
  icon?: string;
  text?: string;
};

export default TableEmptyState;
