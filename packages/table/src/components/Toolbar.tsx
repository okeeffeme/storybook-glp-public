import React, {ReactNode, useContext, useEffect, useState} from 'react';
import cn from 'classnames';
import SearchField from '@jotunheim/react-search-field';
import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
  useDebounceCallback,
} from '@jotunheim/react-utils';
import {BusyDots} from '@jotunheim/react-busy-dots';

import {SEARCH_DEBOUNCE_MS, SEARCH_SELECT_WIDTH} from '../constants';
import {BatchOperationOption, ColumnOption, TableType} from '../types';
import ToolbarDropdown from './ToolbarDropdown';
import TableContext from './TableContext';

import {block, element, modifier} from '../utils/bem.toolbar';

export const Toolbar = ({
  batchOperationOptions,
  onBatchOperationSelect,
  batchOperationPlaceholder = 'Apply to selected',
  columnOptions,
  onSearch,
  isSearchDisabled = false,
  searchPlaceholder,
  searchAutoFocus = true,
  isLoading,
  searchColumn,
  searchValue = '',
  title = '',
  titleChildren,
  count,
  children,
  ...rest
}: ToolbarProps) => {
  const {type, usePageScroll} = useContext(TableContext);

  const [debouncedSearchText, setDebouncedSearchText] = useState(searchValue);
  const [searchText, setSearchText] = useState(searchValue);
  const [searchField, setSearchField] = useState(searchColumn);

  // effect that will run after debounce
  useEffect(() => {
    if (onSearch) {
      onSearch(debouncedSearchText, searchField);
    }
  }, [debouncedSearchText, onSearch, searchField]);

  // effect that allows search to be controlled. necessary when consumer needs to clear search value
  useEffect(() => {
    if (onSearch) {
      setSearchText(searchValue);
    }
  }, [onSearch, searchValue]);

  const handleSearchTextChange = (value) => {
    setSearchText(value);
    debouncedSetSearchText(value);
  };

  const handleSearchOptionChange = (option) => {
    if (option) {
      setSearchField(option);
    }
  };

  // debouncedSearchText should only be used for fetch effect
  const debouncedSetSearchText = useDebounceCallback(
    setDebouncedSearchText,
    SEARCH_DEBOUNCE_MS
  );

  const toolbarClassNames = cn(block(), {
    [modifier('mobile')]: type === TableType.Mobile,
    [modifier('page-level-scroll')]: usePageScroll,
  });

  const searchFieldClassNames = cn(element('search-field'), {
    [element('search-field', 'minimizable')]: type === TableType.Mobile,
  });

  return (
    <div className={toolbarClassNames} {...extractDataAriaIdProps(rest)}>
      {isLoading ? (
        <div className={element('group')}>
          <BusyDots />
        </div>
      ) : (
        <>
          <div
            className={element('title-container')}
            data-testid="title-container">
            {title && <h3 className={element('title')}>{title}</h3>}
            {count !== undefined && (
              <span className={element('count')}>({count})</span>
            )}
            {titleChildren}
          </div>
          <div className={element('group')}>
            {batchOperationOptions && onBatchOperationSelect && (
              <div className={element('batch-dropdown')}>
                <ToolbarDropdown
                  placeholder={batchOperationPlaceholder}
                  onSelect={onBatchOperationSelect}
                  options={batchOperationOptions}
                />
              </div>
            )}
            {children && (
              <div className={element('additional')}>{children}</div>
            )}
            {onSearch && (
              <div className={searchFieldClassNames}>
                <SearchField
                  data-testid="search-field"
                  minimizable={type === TableType.Mobile}
                  onChange={handleSearchTextChange}
                  onSearchOptionChange={handleSearchOptionChange}
                  value={searchText}
                  placeholder={searchPlaceholder}
                  searchOption={searchColumn}
                  searchOptions={columnOptions}
                  searchOptionsWidth={SEARCH_SELECT_WIDTH}
                  disabled={isSearchDisabled}
                  data-react-table-search={true}
                  autoFocus={searchAutoFocus}
                  data-autoFocus={searchAutoFocus}
                />
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export type ToolbarProps = {
  batchOperationOptions?: BatchOperationOption[];
  onBatchOperationSelect?: (value: string) => void;
  batchOperationPlaceholder?: string;
  columnOptions?: ColumnOption[];
  onSearch?: (searchText: string, searchField: string | undefined) => void;
  isSearchDisabled?: boolean;
  searchPlaceholder?: string;
  searchAutoFocus?: boolean;
  children?: ReactNode;
  isLoading?: boolean;
  searchColumn?: string;
  searchValue?: string;
  title?: ReactNode;
  count?: string | number;
  titleChildren?: ReactNode;
} & DataAriaIdAttributes;

export default Toolbar;
