import React, {ReactNode, useRef, useState} from 'react';
import {Row as TableRow} from 'react-table';
import cn from 'classnames';
import {Dropdown} from '@jotunheim/react-dropdown';
import {renderMenu} from '../plugins/useOptionsMenu/Cell';
import {Option} from '../plugins/useOptionsMenu/types';

import {useRowWidth} from '../utils/useRowWidth';
import Cell from './Cell';

import {element} from '../utils/bem.table';
import {getOrderedOptions} from '../utils/getOrderedOptions';
import {removeDuplicatesFromCommands} from '../utils/removeDuplicatesFromCommands';

const Row = <D extends Record<string, unknown>>({
  id,
  cells,
  original,
  getRowProps,
  isExpanded,
  onRowClick,
  renderDrawer,
  isRowExpandable,
  drawerPadding,
  getOptionsMenuProps,
  getToggleRowSelectedProps,
}: RowProps<D>) => {
  const {style, ...restRowProps} = getRowProps();
  const {checked} = getToggleRowSelectedProps();
  const optionsMenu = getOptionsMenuProps();
  const [isHovered, setIsHovered] = useState(false);
  const [contextMenuOpen, setContextMenuOpen] = useState(false);
  const [contextMenuPosition, setContextMenuPosition] = useState({
    top: '0px',
    left: '0px',
  });

  const rowRef = useRef<HTMLDivElement>(null);
  const hasExpandedDrawer = isExpanded && isRowExpandable(original);
  const parentWidth = useRowWidth(rowRef, hasExpandedDrawer);

  const handleHoverEnter = () => setIsHovered(true);
  const handleHoverLeave = () => setIsHovered(false);
  const handleRowClick = () => {
    onRowClick?.(original);
  };

  const showContextMenu = optionsMenu?.enableContextMenu;
  const handleContextMenu = (e) => {
    if (!showContextMenu) return;

    setContextMenuPosition({
      top: e.pageY + 'px',
      left: e.pageX + 'px',
    });
    setContextMenuOpen(true);

    e.preventDefault();
  };

  const getMenuOptions = (): Option[] => {
    const availableOptions = optionsMenu.getOptions(id, original);
    if (optionsMenu.optionsOrder) {
      return getOrderedOptions(
        availableOptions,
        removeDuplicatesFromCommands(optionsMenu.optionsOrder)
      );
    }
    return availableOptions;
  };

  const rowClassName = cn(element('tbody-tr'), {
    [element('tbody-tr', 'clickable')]: !!onRowClick,
    [element('tbody-tr', 'hover')]: isHovered,
    [element('tbody-tr', 'selected')]: checked,
  });

  return (
    <>
      {showContextMenu && (
        <div
          data-testid="react-table-context-menu"
          data-react-table-context-menu-id={`${id}`}
          style={{
            top: contextMenuPosition.top,
            left: contextMenuPosition.left,
            position: 'fixed',
            display: contextMenuOpen ? 'block' : 'none',
          }}>
          <Dropdown
            open={contextMenuOpen}
            onToggle={setContextMenuOpen}
            menu={renderMenu(getMenuOptions(), optionsMenu.onSelect, original)}>
            <span />
          </Dropdown>
        </div>
      )}
      <div
        onMouseEnter={handleHoverEnter}
        onMouseLeave={handleHoverLeave}
        className={rowClassName}
        style={{
          ...style,
          display: 'inline-flex', // Overriding this from flex to inline-flex to work with FireFox
          minWidth: '100%', // Overriding this from 0px to 100% so it displays normal when content isn't filling full width
          borderBottom: hasExpandedDrawer ? 'none' : undefined,
        }}
        {...restRowProps}
        onClick={handleRowClick}
        onContextMenu={handleContextMenu}
        ref={rowRef}
        data-testid="data-react-table-row"
        data-react-table-row={true}
        data-react-table-row-id={`${id}`}>
        {cells.map((props) => (
          <Cell key={props.getCellProps().key} {...props} />
        ))}
      </div>
      {hasExpandedDrawer && (
        <div
          onMouseEnter={handleHoverEnter}
          onMouseLeave={handleHoverLeave}
          style={{
            paddingLeft: drawerPadding,
            width: parentWidth,
          }}
          className={cn(element('tbody-tr'), element('tbody-tr', 'drawer'), {
            [element('tbody-tr', 'hover')]: isHovered,
            [element('tbody-tr', 'selected')]: checked,
          })}
          data-testid="expanded-drawer"
          data-react-table-sub-row-id={`${id}`}>
          {renderDrawer?.(original)}
        </div>
      )}
    </>
  );
};

type RowProps<D extends Record<string, unknown>> = {
  onRowClick?: (original: D) => void;
  renderDrawer?: (original: D) => ReactNode;
  isRowExpandable: (original: D) => boolean;
  drawerPadding?: string | number;
} & TableRow<D>;

/* We cast to an original component type to keep generic parameter accessible for consumers */
export default React.memo(Row) as typeof Row;
