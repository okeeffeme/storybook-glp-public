import React, {MouseEventHandler} from 'react';
import {HeaderGroup} from 'react-table';
import cn from 'classnames';
import {Truncate} from '@jotunheim/react-truncate';
import Tooltip from '@jotunheim/react-tooltip';

import {
  EXPAND_COLUMN_ID,
  OPTIONS_MENU_COLUMN_ID,
  SELECTION_COLUMN_ID,
} from '../constants';
import {CellAlignment} from '../types';

import {element} from '../utils/bem.table';

const Header = <D extends Record<string, unknown>>({
  getHeaderProps,
  render,
  getSortHeaderProps,
  id,
  filterSettings,
  disableFilters,
  hasFilterEnabledColumns,
  suppressHeaderTooltip,
  suppressHeaderTruncation,
  alignment = CellAlignment.Left,
  isFixedWidth,
}: HeaderProps<D>) => {
  const header = render('Header');

  const isPluginColumn =
    id === EXPAND_COLUMN_ID ||
    id === SELECTION_COLUMN_ID ||
    id === OPTIONS_MENU_COLUMN_ID;

  const cellClassNames = cn(
    element('flex-cell'),
    element('flex-cell', `align-${alignment}`),
    {
      [element('flex-cell', 'plug-in-column')]: isPluginColumn,
    }
  );
  const {style, ...restHeaderProps} = getHeaderProps();
  const {style: sortHeaderStyle, onSort} = getSortHeaderProps();
  const handleClick: MouseEventHandler = (e) => {
    onSort?.(e);
  };

  const titleClassName = cn(element('cell-title'), {
    [element('tooltip-target')]: !suppressHeaderTooltip,
    [element('clickable')]: !!onSort,
  });

  const HeaderTitle = (
    <div
      className={titleClassName}
      data-react-table-header-title={true}
      data-testid="table-header">
      {suppressHeaderTruncation ? header : <Truncate>{header}</Truncate>}
    </div>
  );

  return (
    <div
      className={cn(element('thead-th'), {
        [element('thead-th', 'with-divider')]:
          !disableFilters && hasFilterEnabledColumns && !isPluginColumn,
        [element('thead-th', 'with-text-wrap')]: suppressHeaderTruncation,
      })}
      data-react-table-header={true}
      data-react-table-header-id={id}
      data-testid={`header-${id}`}
      onClick={handleClick}
      {...restHeaderProps}
      style={{
        ...style,
        ...sortHeaderStyle,
        flex: isFixedWidth || !style?.flex ? '0 0 auto' : style.flex, // react-table doesn't define flex when width is 0, need to add it manually
      }}>
      <div className={cellClassNames} data-testid={`headertitle-${id}`}>
        {suppressHeaderTooltip ? (
          HeaderTitle
        ) : (
          <Tooltip content={header}>{HeaderTitle}</Tooltip>
        )}
        {!disableFilters && filterSettings ? (
          <div className={element('filter-icon')}>{render('Filter')}</div>
        ) : null}
      </div>
    </div>
  );
};

type HeaderProps<D extends Record<string, unknown>> = {
  disableFilters?: boolean;
  hasFilterEnabledColumns?: boolean;
} & HeaderGroup<D>;

export default Header;
