import React, {ReactNode} from 'react';

import {Dropdown} from '@jotunheim/react-dropdown';
import Icon, {menuDown} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

import {element} from '../utils/bem.toolbar.dropdown';

export const ToolbarDropdown = ({
  onSelect,
  options = [],
  placeholder,
}: ToolbarDropdownProps) => {
  return (
    <Dropdown
      placement="bottom-end"
      menu={
        <Dropdown.Menu>
          {options.map((option) => {
            const handleSelect = () => {
              onSelect?.(option.value);
            };

            return (
              <Dropdown.MenuItem
                data-react-table-dropdown-menu-item={option.value}
                onClick={handleSelect}
                iconPath={option.iconPath}
                key={option.value}>
                {option.label}
              </Dropdown.MenuItem>
            );
          })}
        </Dropdown.Menu>
      }>
      <div
        className={element('dropdown-toggle')}
        data-react-table-dropdown-toggle={true}>
        {placeholder}
        <div className={element('dropdown-toggle-button')}>
          <IconButton>
            <Icon path={menuDown} />
          </IconButton>
        </div>
      </div>
    </Dropdown>
  );
};

export type ToolbarDropdownOptionProps = {
  label: ReactNode;
  value: string;
  iconPath?: string;
};

export type ToolbarDropdownProps = {
  options?: ToolbarDropdownOptionProps[];
  onSelect?: (value: string) => void;
  placeholder?: string;
};

export default ToolbarDropdown;
