import React, {ReactNode, useMemo} from 'react';
import cn from 'classnames';
import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';

import Toolbar from './Toolbar';
import Content from './Content';
import TableContext from './TableContext';
import {Language, LanguageLiteral, TableType, TableTypeLiteral} from '../types';

import {block, modifier} from '../utils/bem.table';
import {DEFAULT_LANGUAGE, TABLE_TYPES} from '../constants';

export const Table: React.FC<TableProps> & {
  Content: typeof Content;
  Toolbar: typeof Toolbar;
  TableType: typeof TableType;
  types: typeof TABLE_TYPES;
} = ({
  toolbar,
  content,
  type = TableType.Default,
  hasPadding = true,
  usePageScroll = false,
  language = DEFAULT_LANGUAGE,
  ...rest
}) => {
  usePageScroll = usePageScroll && type === TableType.Default;
  const hasToolbar = !!toolbar;

  const tableClassNames = cn(block(), {
    [modifier('mobile')]: type === TableType.Mobile,
    [modifier('page-level-scroll')]: usePageScroll,
    [modifier('padding')]: hasPadding,
  });

  const tableContextValue = useMemo(
    () => ({
      type,
      usePageScroll,
      hasToolbar,
      language,
    }),
    [type, usePageScroll, hasToolbar, language]
  );

  return (
    <TableContext.Provider value={tableContextValue}>
      <div
        className={tableClassNames}
        {...extractDataAriaIdProps(rest)}
        data-testid="table">
        {toolbar}
        {content}
      </div>
    </TableContext.Provider>
  );
};

type TableProps = {
  toolbar?: ReactNode;
  content?: ReactNode;
  type?: TableType | TableTypeLiteral;
  usePageScroll?: boolean;
  hasPadding?: boolean;
  language?: Language | LanguageLiteral;
} & DataAriaIdAttributes;

Table.Toolbar = Toolbar;
Table.Content = Content;
Table.TableType = TableType;
/**
 * @deprecated Table.types is deprecated, use Table.TableType or TableType enum instead
 */
Table.types = TABLE_TYPES;

export default Table;
