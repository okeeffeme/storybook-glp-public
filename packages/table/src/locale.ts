import {Language} from './types';
import {Tab} from './plugins/useColumnFilters/views/DateFilter/constants';
import {
  CustomDate,
  Operator,
  QuickDate,
} from './plugins/useColumnFilters/types';

export default {
  [Language.EN]: {
    columnFilter: {
      operators: {
        [Operator.Equal]: {
          label: 'Equal',
          symbol: '=',
        },
        [Operator.GreaterThan]: {
          label: 'Greater than',
          symbol: '>',
        },
        [Operator.LessThanOrEqual]: {
          label: 'Less than or equal',
          symbol: '≤',
        },
        [Operator.LessThan]: {
          label: 'Less than',
          symbol: '<',
        },
        [Operator.GreaterThanOrEqual]: {
          label: 'Greater than or equal',
          symbol: '≥',
        },
        [Operator.InList]: {
          label: 'In list',
          symbol: ':',
          hint: 'Use space to separate numbers in the list',
        },
        [Operator.InRange]: {
          label: 'In range',
          symbol: ':',
          hint: 'Use - to separate range ends',
        },
        [Operator.IsNull]: {
          label: 'Is empty',
          symbol: ': is empty',
        },
        [Operator.IsNotNull]: {
          label: 'Is not empty',
          symbol: ': is not empty',
        },
        [Operator.Contains]: {
          label: 'Contains',
          symbol: ':',
        },
        [Operator.StartWith]: {
          label: 'Start with',
          symbol: ': starts with',
        },
        [Operator.EndWith]: {
          label: 'End with',
          symbol: ': ends with',
        },
        [Operator.Match]: {
          label: 'Matches',
          symbol: ':',
        },
        [Operator.NotMatch]: {
          label: "Doesn't match",
          symbol: ' not:',
        },
        [QuickDate.Last1Hour]: {
          label: 'Last 1 hour',
        },
        [QuickDate.Last24Hours]: {
          label: 'Last 24 hours',
        },
        [QuickDate.Last7Days]: {
          label: 'last 7 days',
        },
        [QuickDate.Last31Days]: {
          label: 'Last 31 days',
        },
        [QuickDate.Empty]: {
          label: 'Empty',
        },
        [QuickDate.NotEmpty]: {
          label: 'Not empty',
        },
        [CustomDate.Custom]: {
          noStartDate: 'No start date',
          noEndDate: 'No end date',
          hint: 'Leave any field empty for open ended range.',
        },
      },
      tabs: {
        [Tab.Custom]: {
          tabTitle: 'Custom Filter',
        },
        [Tab.Quick]: {
          tabTitle: 'Quick Filter',
        },
      },
      filterPill: {
        activate: 'activate',
        deactivate: 'deactivate',
      },
      filterBox: {
        placeHolder: 'Value',
        buttonApply: 'done',
        buttonCancel: 'cancel',
      },
    },
  },
  [Language.NB]: {
    columnFilter: {
      operators: {
        [Operator.Equal]: {
          label: 'Er lik',
          symbol: '=',
        },
        [Operator.GreaterThan]: {
          label: 'Større enn',
          symbol: '>',
        },
        [Operator.LessThanOrEqual]: {
          label: 'Mindre enn eller lik',
          symbol: '≤',
        },
        [Operator.LessThan]: {
          label: 'Mindre enn',
          symbol: '<',
        },
        [Operator.GreaterThanOrEqual]: {
          label: 'Større enn eller lik',
          symbol: '≥',
        },
        [Operator.InList]: {
          label: 'I listen',
          symbol: ':',
          hint: 'Bruk mellomrom for å skille nummer i listen',
        },
        [Operator.InRange]: {
          label: 'I området',
          symbol: ':',
          hint: 'Bruk - for å skille endene',
        },
        [Operator.IsNull]: {
          label: 'Er tom',
          symbol: ': er tom',
        },
        [Operator.IsNotNull]: {
          label: 'Er ikke tom',
          symbol: ': er ikke tom',
        },
        [Operator.Contains]: {
          label: 'Inneholder',
          symbol: ':',
        },
        [Operator.StartWith]: {
          label: 'Starter med',
          symbol: ': begynner med',
        },
        [Operator.EndWith]: {
          label: 'Slutter med',
          symbol: ': slutter med',
        },
        [Operator.Match]: {
          label: 'Er lik',
          symbol: ':',
        },
        [Operator.NotMatch]: {
          label: 'Er ulik',
          symbol: ' ikke:',
        },
        [QuickDate.Last1Hour]: {
          label: 'Siste 1 time',
        },
        [QuickDate.Last24Hours]: {
          label: 'Siste 24 timer',
        },
        [QuickDate.Last7Days]: {
          label: 'Siste 7 dager',
        },
        [QuickDate.Last31Days]: {
          label: 'Siste 31 dager',
        },
        [QuickDate.Empty]: {
          label: 'Tom',
        },
        [QuickDate.NotEmpty]: {
          label: 'Ikke tom',
        },
        [CustomDate.Custom]: {
          noStartDate: 'Ingen startdato',
          noEndDate: 'Ingen sluttdato',
          hint: 'La et hvilket som helst felt stå tomt for åpent område.',
        },
      },
      tabs: {
        [Tab.Custom]: {
          tabTitle: 'Egendefinert Filter',
        },
        [Tab.Quick]: {
          tabTitle: 'Hurtigfilter',
        },
      },
      filterPill: {
        activate: 'aktiver',
        deactivate: 'deaktiver',
      },
      filterBox: {
        placeHolder: 'Verdi',
        buttonApply: 'ferdig',
        buttonCancel: 'avbryt',
      },
    },
  },
};
