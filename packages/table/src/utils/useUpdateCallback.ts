import React from 'react';
import {useUpdateEffect} from 'react-use';
import {IdType, Row} from 'react-table';

export const useUpdateCallback = <D extends Record<string, unknown>>({
  ids,
  callback,
  rowsById,
}: {
  ids: Record<IdType<D>, boolean>;
  callback?: (updatedRowsById: Record<string, Row<D>>) => void;
  rowsById: Record<IdType<D>, Row<D>>;
}) => {
  const previous = React.useRef(ids);

  useUpdateEffect(() => {
    if (callback && ids && ids !== previous.current) {
      const updatedRowsById = Object.keys(ids).reduce((acc, id) => {
        acc[id] = rowsById[id];
        return acc;
      }, {});

      callback(updatedRowsById);
    }

    previous.current = ids;
  }, [callback, ids, rowsById]);
};
