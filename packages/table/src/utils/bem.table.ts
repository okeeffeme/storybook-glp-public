import classNames from '../components/Table.module.css';
import {bemFactory} from '@jotunheim/react-themes';

const {block, element, modifier} = bemFactory('comd-table', classNames);

export {block, element, modifier};
