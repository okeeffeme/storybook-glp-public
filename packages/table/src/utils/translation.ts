import translations from '../locale';
import {DEFAULT_LANGUAGE} from '../constants';

const get = (obj, path) =>
  path.split('.').reduce((acc, val) => acc && acc[val], obj);

export const translate = ({key, language}) =>
  get(translations[language], key) ?? get(translations[DEFAULT_LANGUAGE], key);

export default translate;
