import {Operator} from '../plugins/useColumnFilters/types';
import translate from './translation';

const DEFAULT_OPERATOR_SYMBOL = '';

const formatPillText = (filterValue, operator) => {
  const isMatchOperator =
    operator === Operator.Match || operator === Operator.NotMatch;

  return isMatchOperator
    ? filterValue?.map((item) => item.label).join(', ') || ''
    : filterValue;
};

export const getPillText = (columnName, filter, language) => {
  const {value, valueDisabled, operator} = filter;
  const text = formatPillText(value, operator);

  const symbol = translate({
    key: `columnFilter.operators.${operator}.symbol`,
    language,
  });

  return `${columnName}${symbol || DEFAULT_OPERATOR_SYMBOL}${
    valueDisabled ? '' : ` ${text}`
  }`;
};
