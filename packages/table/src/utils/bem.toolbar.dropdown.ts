import {bemFactory} from '@jotunheim/react-themes';
import classNames from '../components/ToolbarDropdown.module.css';

const {block, element, modifier} = bemFactory(
  'comd-table-toolbar-dropdown',
  classNames
);

export {block, element, modifier};
