export const removeDuplicatesFromCommands = (commands: string[]): string[] => {
  const commandsWithoutDuplicates = new Set(commands);
  return [...commandsWithoutDuplicates];
};
