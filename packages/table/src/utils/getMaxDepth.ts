import {Row} from 'react-table';

export const getMaxDepth = <D extends Record<string, unknown>>({
  flatRows,
}: {
  flatRows: Row<D>[];
}) => {
  if (flatRows.length) {
    return Math.max(...flatRows.map((row) => row.depth));
  } else {
    return 0;
  }
};
