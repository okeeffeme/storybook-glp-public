import {ColumnFilterType} from '../types';
import {FilterOperator, Operator} from '../plugins/useColumnFilters/types';

export const getDefaultOperator = (
  filterType: ColumnFilterType | undefined,
  allowedFilterOperators: FilterOperator[]
) => {
  let defaultOperator;
  switch (filterType) {
    case ColumnFilterType.Numeric:
      defaultOperator = Operator.Equal;
      break;
    case ColumnFilterType.Multiselect:
      defaultOperator = Operator.Match;
      break;
    default:
      defaultOperator = Operator.Contains;
      break;
  }

  // return first operator in list if custom operators
  // do not include default operator for the filterType
  return allowedFilterOperators.find(
    (filter) => filter.operator === defaultOperator
  )
    ? defaultOperator
    : allowedFilterOperators[0]?.operator;
};
