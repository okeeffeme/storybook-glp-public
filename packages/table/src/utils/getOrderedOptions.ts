import {Option} from '../plugins/useOptionsMenu/types';

export const getOrderedOptions = (
  options: Option[],
  commandsOrder: string[]
): Option[] => {
  const optionsByCommand = options.reduce((acc, option) => {
    acc[option.command] = option;
    return acc;
  }, {});
  return commandsOrder
    .map((command) => optionsByCommand[command])
    .filter(Boolean);
};
