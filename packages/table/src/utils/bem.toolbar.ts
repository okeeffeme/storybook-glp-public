import classNames from '../components/Toolbar.module.css';
import {bemFactory} from '@jotunheim/react-themes';

const {block, element, modifier} = bemFactory('comd-table-toolbar', classNames);

export {block, element, modifier};
