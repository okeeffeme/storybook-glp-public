export const isStickyPositionSupported = (() => {
  const el = document.createElement('div');
  el.style.cssText = 'position: -webkit-sticky; position: sticky;';
  const _isStickyPositionSupported = el.style.cssText.includes('sticky');

  return () => _isStickyPositionSupported;
})();
