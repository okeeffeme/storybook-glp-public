import {RefObject, useEffect, useState} from 'react';

export const useRowWidth = (
  nodeRef: RefObject<HTMLDivElement>,
  hasExpandedDrawer: boolean
) => {
  const [rowWidth, setRowWidth] = useState('100%');

  useEffect(() => {
    const node = nodeRef.current;

    if (!hasExpandedDrawer || !window.ResizeObserver || !node) {
      return;
    }

    const ro = new window.ResizeObserver((entries) => {
      entries.forEach((entry) => {
        const {width} = entry.contentRect;
        setRowWidth(`${width}px`);
      });
    });

    ro.observe(node);

    return () => {
      ro.unobserve(node);
    };
  }, [nodeRef, hasExpandedDrawer]);

  return rowWidth;
};
