import {ENTER_KEY} from '../constants';

export const enterKeyPressed = ({
  key,
  keyCode,
}: {
  key?: string;
  keyCode?: number;
}) => key === 'Enter' || keyCode === ENTER_KEY;
