import {CellAlignment, ColumnFilterType, Language} from './types';

export const SELECTION_COLUMN_ID = 'selection-column';
export const EXPAND_COLUMN_ID = 'expand-column';
export const OPTIONS_MENU_COLUMN_ID = 'options-menu-column';
export const SEARCH_DEBOUNCE_MS = 300;
export const MAX_OPTION_ICONS = 3;
export const OPTION_ICON_SIZE = 40;
export const SEARCH_SELECT_WIDTH = '130px';
export const ENTER_KEY = 13;
export const PLUGIN_COL_PADDING = 16;
export const EXPAND_ICON_SIZE = 24;
export const SELECTION_ICON_SIZE = 24;

export const DEFAULT_LANGUAGE = Language.EN;

/**
 * @deprecated TABLE_TYPES is deprecated, use TableType enum instead
 */
export const TABLE_TYPES = {
  default: 'default',
  mobile: 'mobile',
};

/**
 * @deprecated CELL_ALIGNMENT is deprecated, use CellAlignment enum instead
 */
export const CELL_ALIGNMENT = {
  left: CellAlignment.Left,
  center: CellAlignment.Center,
  right: CellAlignment.Right,
};

/**
 * @deprecated COLUMN_FILTER_TYPE is deprecated, use ColumnFilterType enum instead
 */
export const COLUMN_FILTER_TYPE = {
  text: ColumnFilterType.Text,
  numeric: ColumnFilterType.Numeric,
  date: ColumnFilterType.Date,
  multiselect: ColumnFilterType.Multiselect,
};
