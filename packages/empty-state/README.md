# Jotunheim React Empty State

[Changelog](./CHANGELOG.md)

A component to show empty states - an icon and some text
