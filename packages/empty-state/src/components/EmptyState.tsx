import React, {AriaAttributes} from 'react';
import {Icon} from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {colors} from '@jotunheim/global-styles';

import classNames from './EmptyState.module.css';

const {block, element} = bemFactory({
  baseClassName: 'comd-empty-state',
  classNames,
});

// Use never type and union type definition to express that its valid to pass either `imageSrc` OR `iconPath`, but not both.
type ImageSrcType = {
  imageSrc?: string;
  iconPath?: never;
};

type IconPathType = {
  imageSrc?: never;
  iconPath?: string;
};

type EmptyStateProps = (ImageSrcType | IconPathType) & {
  text: string;
} & AriaAttributes;

export const EmptyState = ({
  iconPath,
  text,
  imageSrc,
  ...rest
}: EmptyStateProps) => {
  return (
    <div
      className={block()}
      data-testid="empty-state"
      {...extractDataAriaIdProps(rest)}>
      <div>
        {iconPath && !imageSrc && (
          <Icon path={iconPath} size={48} fill={colors.TextDisabled} />
        )}
        {imageSrc && !iconPath && (
          <img
            className={element('img')}
            data-test-empty-state-img=""
            src={imageSrc}
            alt={text}
          />
        )}
      </div>
      <div data-test-empty-state-text="">{text}</div>
    </div>
  );
};

export default EmptyState;
