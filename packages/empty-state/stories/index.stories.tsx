import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import EmptyState from '../src';
import {briefcase} from '../../icons/src';

storiesOf('Components/empty-state', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('With text only', () => <EmptyState text={'No results found'} />)
  .add('With iconPath', () => (
    <EmptyState text={'No results found'} iconPath={briefcase} />
  ))
  .add('With imageSrc', () => (
    <EmptyState
      text="Sorry we couldn't find any results for that."
      imageSrc="magnifier.svg"
    />
  ));
