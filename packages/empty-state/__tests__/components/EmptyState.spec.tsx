import React from 'react';
import {render, screen} from '@testing-library/react';
import {EmptyState} from '../../src/components/EmptyState';

import {cancel} from '../../../icons/src';

describe('Jotunheim React Empty State :: ', () => {
  it('should render with just text', () => {
    const text = 'No results';
    render(<EmptyState text={text} />);
    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.queryByRole('img')).not.toBeInTheDocument();
    expect(screen.getByText(text)).toHaveTextContent(text);
  });

  it('should render with iconPath', () => {
    const text = 'No results';
    render(<EmptyState iconPath={cancel} text={text} />);

    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      cancel
    );
    expect(screen.getByText(text)).toHaveTextContent(text);
    expect(screen.queryByRole('img')).not.toBeInTheDocument();
  });

  it('should render with imageSrc', () => {
    const text = 'No results';
    render(<EmptyState imageSrc="image.svg" text={text} />);
    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.getByRole('img')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', 'image.svg');
    expect(screen.getByText(text)).toHaveTextContent(text);
  });

  it('should not render icon or img when both iconPath and imageSrc is passed', () => {
    const text = 'No results';
    render(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore - break the contract of props to be able to test this scenario
      <EmptyState text={text} iconPath={cancel} imageSrc="image.svg" />
    );

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.queryByRole('img')).not.toBeInTheDocument();
    expect(screen.getByText(text)).toHaveTextContent(text);
  });
});
