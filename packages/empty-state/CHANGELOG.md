# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.36&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.36&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.34&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.35&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.33&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.34&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.32&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.33&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.31&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.32&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.30&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.31&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.29&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.30&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.28&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.29&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.27&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.28&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.26&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.27&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.25&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.26&targetRepoId=1246) (2023-01-30)

### Bug Fixes

- adding data-testid EmptyState component ([NPM-1188](https://jiraosl.firmglobal.com/browse/NPM-1188)) ([b2d24ad](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b2d24ad91dd6bbb762e3523477bacee701250d5c))
- removing data-testids EmptyState component ([e145212](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e145212e2de0f44d4860d3e9b56b3f4848a11412))

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.24&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.25&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.22&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.24&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- adding data-testid EmptyState component ([NPM-1188](https://jiraosl.firmglobal.com/browse/NPM-1188)) ([4302d70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4302d70904a36a962ec91e8ca53405b122e18cad))

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.22&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.23&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- adding data-testid EmptyState component ([NPM-1188](https://jiraosl.firmglobal.com/browse/NPM-1188)) ([4302d70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4302d70904a36a962ec91e8ca53405b122e18cad))

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.21&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.22&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.20&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.21&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.18&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.20&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.18&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.19&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.15&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.18&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.15&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.17&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.15&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.16&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.14&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.15&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.13&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.14&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.11&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.12&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.10&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.11&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.9&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.10&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.8&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.9&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.7&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.8&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.5&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.6&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.2&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.3&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.1&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.2&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-empty-state

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-empty-state@2.0.0&sourceBranch=refs/tags/@jotunheim/react-empty-state@2.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-empty-state

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@1.0.3&sourceBranch=refs/tags/@confirmit/react-empty-state@1.0.4&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-empty-state

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@1.0.2&sourceBranch=refs/tags/@confirmit/react-empty-state@1.0.3&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-empty-state

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@1.0.1&sourceBranch=refs/tags/@confirmit/react-empty-state@1.0.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@1.0.0&sourceBranch=refs/tags/@confirmit/react-empty-state@1.0.1&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-empty-state

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.18&sourceBranch=refs/tags/@confirmit/react-empty-state@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Release version 1.0.0 ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.17&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.18&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.16&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.17&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.15&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.16&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.14&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.15&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.13&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.14&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.11&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.12&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.10&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.11&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.9&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.10&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.8&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.9&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.7&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.8&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.6&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.7&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.5&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.6&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.3&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.4&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.2&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.3&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.1&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.2&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-empty-state

## [0.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-empty-state@0.1.0&sourceBranch=refs/tags/@confirmit/react-empty-state@0.1.1&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-empty-state

# 0.1.0 (2021-07-30)

### Features

- add empty state component ([NPM-799](https://jiraosl.firmglobal.com/browse/NPM-799)) ([a5afd2d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a5afd2dbba58609e7d74bc58c573f67127a5b2e7))

## CHANGELOG

### v0.0.1

- Initial version
