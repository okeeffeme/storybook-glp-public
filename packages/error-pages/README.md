# Jotunheim React Error Pages

## Usage

403 errors should not surface to the client because this will tell the user that something exists, but you dont have access. This could be exploited if a vulnerability is found. Because of this, you should treat all 403 as 404. A good way to solve this is to translate any 403 to 404 in your gateway before the response is returned to the client.
