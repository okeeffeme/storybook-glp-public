import React from 'react';
import {storiesOf} from '@storybook/react';
import {text, object} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Error404, Error500, ErrorClient} from '../src/index';

const links = [
  {link: '#', title: 'title1'},
  {link: '#', title: 'title2'},
];

storiesOf('Components/error-pages', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('404 Page', () => (
    <Error404
      links={object('links', links)}
      homeLinkUrl={text('homeLinkUrl (empty to hide)', '#home')}
      homeLinkText={text('homeLinkText', 'You can also start over from')}
    />
  ))
  .add('500 Page', () => <Error500 />)
  .add('Client error', () => <ErrorClient />);
