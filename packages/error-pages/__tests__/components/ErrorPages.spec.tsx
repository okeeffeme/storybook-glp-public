import React from 'react';
import {render, screen} from '@testing-library/react';
import {Error404, Error500, ErrorClient} from '../../src/index';

describe('Jotunheim React Error Pages :: ', () => {
  describe('404 :: ', () => {
    it('should show link to home if homeLinkUrl is defined', () => {
      render(<Error404 homeLinkUrl="#" />);
      expect(screen.getByTestId('home-link-text')).toBeInTheDocument();
      expect(screen.getByRole('link')).toBeInTheDocument();
    });

    it('should not show link to home if homeLinkUrl is not defined', () => {
      render(<Error404 />);
      expect(screen.queryByTestId('home-link-text')).not.toBeInTheDocument();
      expect(screen.queryByRole('link')).not.toBeInTheDocument();
    });

    it('should show list of links', () => {
      render(
        <Error404
          links={[
            {link: '#', title: 'someLink'},
            {link: '#2', title: 'someLink2'},
          ]}
        />
      );
      expect(screen.getAllByRole('link').length).toBe(2);
    });

    it('should not show list of links if none provided', () => {
      render(<Error404 />);
      expect(screen.queryAllByRole('link').length).toBe(0);
    });
  });

  describe('500 :: ', () => {
    it('renders', () => {
      render(<Error500 />);

      expect(screen.getAllByRole('heading').length).toBe(1);
      expect(screen.getAllByTestId('error500-text').length).toBe(2);
    });
  });

  describe('ClientError :: ', () => {
    it('renders', () => {
      render(<ErrorClient />);
      expect(screen.getAllByRole('heading').length).toBe(1);
      expect(screen.getAllByTestId('error-client-text').length).toBe(2);
      expect(screen.getAllByRole('link').length).toBe(1);
    });
  });
});
