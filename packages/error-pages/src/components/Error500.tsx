import React from 'react';
import cn from 'classnames';

import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import Icon500 from './svg/500.svg';

import classNames from './ErrorPages.module.css';

const {block, element} = bemFactory('comd-error-pages', classNames);

export const Error500 = ({...rest}: DataAriaIdAttributes) => {
  return (
    <div
      className={block()}
      data-testid="error-500"
      {...extractDataAriaIdProps(rest)}>
      <div data-testid="content" className={cn(element('content'))}>
        <h1 className={cn(element('heading'))}>We are so sorry...</h1>
        <p data-testid="error500-text" className={cn(element('text'))}>
          If the problem persist please contact <br /> support and provide this
          information:
        </p>
        <div className={cn(element('icon'))}>
          <Icon500 />
        </div>
        <p
          data-testid="error500-text"
          className={cn(element('text'), element('text', 'type'))}>
          <b>500: Internal server error</b>
        </p>
      </div>
    </div>
  );
};

export default Error500;
