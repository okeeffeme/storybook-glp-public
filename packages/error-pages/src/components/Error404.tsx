import React from 'react';
import cn from 'classnames';

import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import SimpleLink from '@jotunheim/react-simple-link';

import classNames from './ErrorPages.module.css';

const {block, element} = bemFactory('comd-error-pages', classNames);

import Icon404 from './svg/404.svg';

export const Error404 = ({
  links,
  homeLinkUrl,
  homeLinkText = 'You can also start over from',
  ...rest
}: Error404Props) => {
  return (
    <div
      className={block()}
      data-testid="error-404"
      {...extractDataAriaIdProps(rest)}>
      <div className={element('content')}>
        <h1 className={element('heading')}>Sorry about that...</h1>
        <p className={element('error404-text')}>
          That page does not exist <br /> or you don't have permission to view
          it.
        </p>
        <div className={element('icon')}>
          <Icon404 />
        </div>
        <div className={element('link-block')}>
          {links && links.length > 0 && (
            <ul className={element('link-list')}>
              {links.map((obj, i) => (
                <li key={i}>
                  <SimpleLink className={element('link')} href={obj.link}>
                    {obj.title}
                  </SimpleLink>
                </li>
              ))}
            </ul>
          )}
          {homeLinkUrl && (
            <p className={element('error404-text')}>
              <span
                data-testid="home-link-text"
                className={element('link-home-text')}>
                {homeLinkText}
              </span>
              <SimpleLink className={element('link-home')} href={homeLinkUrl}>
                Horizons Home
              </SimpleLink>
            </p>
          )}
        </div>
        <p className={cn(element('text'), element('text', 'type'))}>
          <b>404: Page not found</b>
        </p>
      </div>
    </div>
  );
};

type Error404Props = {
  links?: {
    link?: string;
    title?: string;
  }[];
  homeLinkUrl?: string;
  homeLinkText?: string;
} & DataAriaIdAttributes;

export default Error404;
