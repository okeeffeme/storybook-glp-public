import React from 'react';
import cn from 'classnames';

import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import IconClient from './svg/client.svg';

import classNames from './ErrorPages.module.css';

const {block, element} = bemFactory('comd-error-pages', classNames);

export const ErrorClient = ({...rest}: DataAriaIdAttributes) => {
  return (
    <div
      className={block()}
      data-testid="error-client"
      {...extractDataAriaIdProps(rest)}>
      <div className={cn(element('content'))}>
        <h1 className={cn(element('heading'))}>Oops! Something went wrong</h1>
        <p className={cn(element('text'))} data-testid="error-client-text">
          Reloading the page tends to sort it out...
        </p>
        <div className={cn(element('icon'))}>
          <IconClient />
        </div>
        <div className={element('link-block')}>
          <p className={element('text')} data-testid="error-client-text">
            <a
              className={element('link')}
              href=""
              onClick={() => window.location.reload()}>
              RELOAD
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default ErrorClient;
