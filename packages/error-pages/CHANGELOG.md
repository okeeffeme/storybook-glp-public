# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@8.1.2&sourceBranch=refs/tags/@jotunheim/react-error-pages@8.1.3&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-error-pages

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@8.1.1&sourceBranch=refs/tags/@jotunheim/react-error-pages@8.1.2&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@8.1.0&sourceBranch=refs/tags/@jotunheim/react-error-pages@8.1.1&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- add test ids to error-pages' components ([NPM-1189](https://jiraosl.firmglobal.com/browse/NPM-1189)) ([362ef21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/362ef21e7fadba755a8b763bf8e5d40aa2af15c1))

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@8.0.0&sourceBranch=refs/tags/@jotunheim/react-error-pages@8.1.0&targetRepoId=1246) (2022-10-13)

### Features

- convert ErrorPages to Typescript ([NPM-1098](https://jiraosl.firmglobal.com/browse/NPM-1098)) ([9d42884](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9d4288486b3e4eb574a3bb58c87952123c28ce10))

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@7.0.2&sourceBranch=refs/tags/@jotunheim/react-error-pages@8.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of ErrorPages ([NPM-1079](https://jiraosl.firmglobal.com/browse/NPM-1079)) ([f272f5d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f272f5d468a6ea85c2380804876c97d846158427))
- remove default theme from ErrorPages ([NPM-1070](https://jiraosl.firmglobal.com/browse/NPM-1070)) ([7b92ad6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7b92ad6a4ed1f27eac195a506641f760862f5b75))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support

feat: remove default theme from ErrorPages (NPM-1062)

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@7.0.1&sourceBranch=refs/tags/@jotunheim/react-error-pages@7.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-error-pages

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-error-pages@7.0.0&sourceBranch=refs/tags/@jotunheim/react-error-pages@7.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-error-pages

# 7.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.8&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.9&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.7&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.8&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-error-pages

## [6.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.5&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.6&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-error-pages

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.4&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.5&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.3&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.4&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-error-pages

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.2&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.1&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.2&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-error-pages

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@6.0.0&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-error-pages

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@5.0.3&sourceBranch=refs/tags/@confirmit/react-error-pages@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([70c6fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70c6fc66e591517275e65be695c787ec215a9841))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-error-pages@5.0.2&sourceBranch=refs/tags/@confirmit/react-error-pages@5.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-error-pages

## [5.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-error-pages@5.0.1...@confirmit/react-error-pages@5.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-error-pages

## CHANGELOG

## v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-error-pages`

### v4.1.0

- Removing package version in class names.

### v4.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v3.0.0

- **BREAKING**
  - Remove deprecated 401 page, as this should not be used
  - prop `arrayOfLinks` renamed to `links`
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- fix: Add missing @babel/runtime 7.x peerDependency.
- feat: support for themes

### v2.0.1

- fix: Add missing dependencies

### v2.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation
- Deprecated: 401 errors should redirect to the login page, so a separate 401 client error page is not needed..

### v1.5.0

- Add Client error Screen

### v1.4.6

- Add 401 Permissions Screen

### v1.4.0

- Screen will be mobile-friendly

### v1.3.0

- Support to add or hide horizons home link

### v1.2.1

- Fix: scoping issue in css caused box-sizing to affect global styles

### v1.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **Breaking change** React 16.2 peer dependency
- **Breaking change** Package provides ES modules only

### v0.0.6

- support to add links on error page

### v0.0.5

- design change

### v0.0.1

- initial version
