import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import InlineEdit from '../src/components/InlineEdit';
import Section from '../../section/src';

const label = {
  marginTop: 60,
  paddingBottom: 10,
  fontWeight: '120%',
};

storiesOf('Components/inline-edit', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [defaultValue, setDefaultValue] = React.useState('');
    const [hasError, setHasError] = React.useState(true);

    return (
      <div
        style={{
          padding: '50px',
          backgroundColor: '#f1f1f1',
        }}>
        <div style={{paddingLeft: 20, paddingRight: 20, width: 400}}>
          <div style={label}>Default</div>
          <InlineEdit value={defaultValue} onChange={setDefaultValue} />

          <div style={label}>Open for editing by default</div>
          <InlineEdit
            value="Some value"
            autoSelect={true}
            defaultIsEditing={true}
          />

          <div style={label}>Required</div>
          <InlineEdit placeholder="Please enter a name" required={true} />

          <div style={label}>Disabled</div>
          <InlineEdit value="Disabled" disabled={true} />

          <div style={label}>Inherited styles</div>
          <h4>
            <InlineEdit placeholder="I'm a header tag" />
          </h4>

          <div style={label}>Disabled</div>
          <InlineEdit value="Disabled" disabled={true} />

          <div style={label}>With value and autoSelect</div>
          <InlineEdit value="Some value" autoSelect={true} />

          <div style={label}>Read Only</div>
          <InlineEdit value="Read Only" readOnly={true} />

          <div style={label}>With value and autoSelect</div>
          <InlineEdit value="Some value" autoSelect={true} />

          <div style={label}>Show as link</div>
          <InlineEdit value="https://material-ui.com/" showAsLink={true} />

          <div style={label}>With error</div>
          <InlineEdit
            helperText={
              hasError ? 'Please remove invalid characters' : undefined
            }
            error={hasError}
            value="contains <script> tag"
            onChanging={(v) => {
              setHasError(v.includes('<'));
            }}
          />

          <div style={label}>Show pencil on hover</div>
          <InlineEdit onlyShowPencilOnHover={true} />
        </div>
      </div>
    );
  })
  .add('With Knobs', () => {
    const [value, setValue] = React.useState('Some value');

    return (
      <div style={{paddingLeft: 20, paddingRight: 20, width: 400}}>
        <div style={label}>Default</div>
        <InlineEdit
          value={value}
          onChange={setValue}
          autoSelect={boolean('autoSelect', true)}
          defaultIsEditing={boolean('defaultIsEditing', true)}
          required={boolean('required', true)}
          disabled={boolean('disabled', false)}
          showAsLink={boolean('Show as link', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          onlyShowPencilOnHover={boolean('Show pencil on hover', false)}
          readOnly={boolean('Read only', false)}
        />
      </div>
    );
  })
  .add('Default value when clearing input', () => {
    const initialState = {
      0: 'Initial value',
      1: 'Initial value',
      2: 'Initial value',
    };
    const [state, setState] = useState(initialState);
    const getOnChange = (index) => (v) => {
      setState((s) => ({...s, [index]: v || 'default value'}));
    };
    return (
      <div style={{paddingLeft: 20, paddingRight: 20, width: 400}}>
        <div style={label}>Try to clear input value several times</div>
        <InlineEdit
          value={state[0]}
          onChange={getOnChange(0)}
          autoSelect={boolean('autoSelect', true)}
          defaultIsEditing={boolean('defaultIsEditing', true)}
          required={boolean('required', true)}
          disabled={boolean('disabled', false)}
          showAsLink={boolean('Show as link', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          onlyShowPencilOnHover={boolean('Show pencil on hover', false)}
          readOnly={boolean('Read only', false)}
        />
        <div style={label}>with defaultIsEditing</div>
        <InlineEdit
          value={state[1]}
          onChange={getOnChange(1)}
          autoSelect={boolean('autoSelect', true)}
          defaultIsEditing={boolean('defaultIsEditing', true)}
          required={boolean('required', true)}
          disabled={boolean('disabled', false)}
          showAsLink={boolean('Show as link', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          onlyShowPencilOnHover={boolean('Show pencil on hover', false)}
          readOnly={boolean('Read only', false)}
        />
        <div style={label}>With empty initial value</div>
        <InlineEdit
          value={state[2]}
          onChange={getOnChange(2)}
          autoSelect={boolean('autoSelect', true)}
          defaultIsEditing={boolean('defaultIsEditing', true)}
          required={boolean('required', true)}
          disabled={boolean('disabled', false)}
          showAsLink={boolean('Show as link', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          onlyShowPencilOnHover={boolean('Show pencil on hover', false)}
          readOnly={boolean('Read only', false)}
        />
      </div>
    );
  })
  .add('With validation', () => {
    const [state, setState] = React.useState({error: false, value: 'valid'});
    return (
      <div style={{padding: 20, width: 400}}>
        <>
          <InlineEdit
            helperText={
              state.error ? 'Please remove invalid characters' : undefined
            }
            error={state.error}
            value={state.value}
            onChange={(v) =>
              setState((s) => ({
                ...s,
                value: v,
              }))
            }
            onChanging={(v) => {
              setState((s) => ({
                ...s,
                error: v.includes('<'),
              }));
            }}
          />
          <div style={{marginTop: 60}}>
            <b>Controlled value:</b>
            <span>{state.value}</span>
          </div>
        </>
      </div>
    );
  })
  .add('With async validation', () => {
    const [state, setState] = React.useState({error: false, value: 'valid'});

    return (
      <div style={{padding: 20, width: 400}}>
        <>
          <InlineEdit
            helperText={
              state.error ? 'Please remove invalid characters' : undefined
            }
            error={state.error}
            value={state.value}
            onChange={(v) =>
              setState((s) => ({
                ...s,
                value: v,
              }))
            }
            onChanging={(v) => {
              setTimeout(() => {
                setState((s) => ({
                  ...s,
                  error: v.includes('<'),
                }));
              }, 1000);
            }}
          />
          <div style={{marginTop: 60}}>
            <b>Controlled value:</b>
            <span>{state.value}</span>
          </div>
        </>
      </div>
    );
  })
  .add('Example within sectionHeader', () => (
    <Section collapsible={true}>
      <Section.Header
        title={
          <InlineEdit
            placeholder="Please enter a name"
            required={true}
            helperText={'Help Text Test'}
          />
        }
      />
      <Section.Collapsible>
        <Section.Body>
          <div style={{padding: '8px 16px 16px'}}>Example Content</div>
        </Section.Body>
      </Section.Collapsible>
    </Section>
  ));
