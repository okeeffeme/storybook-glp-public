# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [8.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.32&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.33&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.32&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.32&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.30&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.31&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.29&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.30&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.28&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.29&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.27&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.28&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.26&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.27&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.25&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.26&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [8.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.24&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.25&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.23&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.24&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.22&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.23&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.21&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.22&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.20&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.21&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.19&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.20&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.18&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.19&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.17&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.18&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.16&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.17&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.14&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.14&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.13&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.12&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.13&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.11&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.10&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.9&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.8&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.7&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.8&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.6&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.7&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.3&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.6&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.3&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.5&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.3&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.4&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.2&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.3&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.1.1&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.2&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-inline-edit

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@8.0.0&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.1.0&targetRepoId=1246) (2022-10-13)

### Features

- convert InlineEdit to Typescript ([NPM-1100](https://jiraosl.firmglobal.com/browse/NPM-1100)) ([24d9975](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/24d9975b0ae821dc2080778b531e60b888b05363))

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.17&sourceBranch=refs/tags/@jotunheim/react-inline-edit@8.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of InlineEdit ([NPM-942](https://jiraosl.firmglobal.com/browse/NPM-942)) ([1b4cb99](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1b4cb9932ccfa36e84e6804c6913b8f55e78a65a))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.16&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.15&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.14&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.13&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.12&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.11&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.10&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.9&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.7&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.4&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.3&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.2&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.1&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-inline-edit

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inline-edit@7.0.0&sourceBranch=refs/tags/@jotunheim/react-inline-edit@7.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-inline-edit

# 7.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [6.0.87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.86&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.87&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.86](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.85&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.86&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.84&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.85&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [6.0.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.83&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.84&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.83](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.82&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.83&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.81&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.82&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.79&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.80&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.78&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.79&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.77&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.78&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.76&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.77&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.75&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.76&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.74&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.75&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.71&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.72&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.70&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.71&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.69&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.70&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.68&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.69&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.67&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.68&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.66&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.67&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.65&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.66&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.64&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.65&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.63&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.64&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.61&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.62&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.60&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.61&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.59&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.60&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.58&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.59&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.57&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.58&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.56&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.57&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.55&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.56&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.54&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.55&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.53&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.54&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.52&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.53&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.51&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.52&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.50&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.51&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.49&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.50&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.48&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.49&targetRepoId=1246) (2021-07-01)

### Bug Fixes

- Change capitalization in import statment - to match actual file name ([NPM-813](https://jiraosl.firmglobal.com/browse/NPM-813)) ([4080e89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4080e89a5c6bae5fdc1b70e36b7ea2e756deef0f))

## [6.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.47&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.48&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.46&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.47&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.45&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.46&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.44&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.45&targetRepoId=1246) (2021-06-04)

### Bug Fixes

- fix import of css variable in js by using default import instead of named import, and remove usage of useTheme since this component only supports 1 theme ([NPM-801](https://jiraosl.firmglobal.com/browse/NPM-801)) ([614d8e3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/614d8e36df2bbfe9d32ab376f784b8db06000e38))

## [6.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.43&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.44&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.42&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.43&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.41&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.42&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.40&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.41&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.39&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.40&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.38&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.39&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.37&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.38&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.35&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.36&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.34&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.35&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.33&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.34&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.32&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.33&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [6.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.31&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.32&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.30&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.31&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.29&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.30&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.28&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.29&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.27&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.28&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.26&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.27&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.25&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.23&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.24&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.22&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.23&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.21&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.22&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.20&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.21&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.19&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.20&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.18&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.19&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.17&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.18&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.16&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.17&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.15&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.16&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.14&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.13&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.12&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.13&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.11&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.12&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.8&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.9&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.3&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.2&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@6.0.1&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-inline-edit

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@5.0.1&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@5.0.1&sourceBranch=refs/tags/@confirmit/react-inline-edit@6.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@5.0.1&sourceBranch=refs/tags/@confirmit/react-inline-edit@5.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-inline-edit

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.38&sourceBranch=refs/tags/@confirmit/react-inline-edit@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.37&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.38&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([d423b61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d423b618e8b60d112f9ce3edc85539e0fbdbf39b))

## [4.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.36&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.37&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.35&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.36&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.34&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.35&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.32&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.33&targetRepoId=1246) (2020-10-15)

### Bug Fixes

- correcting flex for ie11 support ([SVD-1043](https://jiraosl.firmglobal.com/browse/SVD-1043)) ([010ad99](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/010ad99b42f09d640c274f89978cf53c5ff702d6))

## [4.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.30&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.31&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.29&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.30&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-inline-edit

# 4.1.29

### Fix

- Stop event propagation onClick for IconButton and Label

## [4.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.27&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.28&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.24&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.25&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.23&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.24&targetRepoId=1246) (2020-09-28)

### Bug Fixes

- change fallback positions for helperTexts on Input fields, to avoid issues where right position is blocked, and it will fallback to bottom, potentially covering the Menu options in the Select component. Changed positions for all Inputs for consistency. ([NPM-542](https://jiraosl.firmglobal.com/browse/NPM-542)) ([c173049](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1730499cdae33acfdc4be8c7c813616f08ad29d)

## [4.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.21&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.22&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inline-edit@4.1.19&sourceBranch=refs/tags/@confirmit/react-inline-edit@4.1.20&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inline-edit@4.1.17...@confirmit/react-inline-edit@4.1.18) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.16](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inline-edit@4.1.15...@confirmit/react-inline-edit@4.1.16) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.10](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inline-edit@4.1.9...@confirmit/react-inline-edit@4.1.10) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-inline-edit

## [4.1.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inline-edit@4.1.8...@confirmit/react-inline-edit@4.1.9) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-inline-edit

## CHANGELOG

### v4.1.0

- Feat: Visual changes

  - Removing border on hover
  - Removing padding
  - Adding inherit for font
  - Adding height to wrapper

- Feat: Adding animation for width - width will now grow from with auto width of the label to full allotted width for the input (in edit state).
- Feat: Adding readOnly prop
- Fix: Adding check if inputRef is null for useEffect

### v4.0.8

- Fix: Inputs should have white background

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.3.0

- Feat: Helper text is moved to inside a tooltip

### v3.2.0

- Removing package version in class names.

### v3.1.0

- Feat: When "error" is set to true, the field is always displayed in edit-mode

### v3.0.0

- **BREAKING**:
  - Updated to follow new DS specs by virtue of TextField updates.

### v2.0.0

- **BREAKING**:
  - Default theme now is the same as Material theme
- Fix: Pencil does not take up any space when `onlyShowPencilOnHover` is true, and component is not hovered. This makes more space available for the label

### v1.2.0

- Feat: adding defaultIsEditing prop

### v1.1.4

- Fix: Trigger onChange when clicking Escape to reset

### v1.1.3

- Fix: Don't reset to an "invalid" value (error=true)

### v1.1.1

- Fix: Exception on click when autoSelect and disabled used together

### v1.1.0

- Feat: Adding disabled support

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.1

- Initial version
