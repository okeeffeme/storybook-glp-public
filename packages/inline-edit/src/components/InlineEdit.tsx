import React, {ReactNode} from 'react';
import cn from 'classnames';

import {
  extractDataAriaIdProps,
  useRequestAnimationFrames,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {TextField} from '@jotunheim/react-text-field';
import {IconButton} from '@jotunheim/react-button';
import Icon, {pencil} from '@jotunheim/react-icons';
import Tooltip, {
  StrictModifiers,
  PopperRefInstance,
} from '@jotunheim/react-tooltip';

import Label from './Label';

import classNames from './InlineEdit.module.css';
import variables from './Variables.css';

const modifiers: StrictModifiers[] = [
  {
    name: 'flip',
    options: {fallbackPlacements: ['right', 'left', 'top', 'bottom']},
  },
];

const KEYS = {
  enter: 13,
  esc: 27,
};

const {block, element, modifier} = bemFactory('comd-inline-edit', classNames);

export function InlineEdit<V extends string | number = string>({
  disabled,
  value,
  error = false,
  maxLength = 64,
  helperText = '',
  placeholder = 'Click to edit...',
  required = false,
  autoSelect = false,
  showAsLink = false,
  editFromLabel = true,
  defaultIsEditing = false,
  onlyShowPencilOnHover = false,
  readOnly = false,
  onPaste = () => {},
  onChange = () => {},
  onChanging = () => {},
  ...rest
}: InlineEditProps<V>) {
  const [newValue, setNewValue] = React.useState(value);
  const [isEditing, setIsEditing] = React.useState(defaultIsEditing);
  const [isHovering, setIsHovering] = React.useState(false);
  const inputRef = React.useRef<HTMLInputElement>(null);

  // Need to store the original value, in case the component is controlled and re-renders with in invalid value.
  // This happens in SD when we want to trigger save (and hence validation),
  // whenever the component loses focus, regardless of whether it is valid or not.
  const originalValue = React.useRef(value);

  placeholder = `${placeholder}${required ? ' *' : ''}`;

  const classes = cn(block(), {
    [modifier('editing')]: isEditing,
    [modifier('disabled')]: disabled,
    [modifier('edit-from-label')]: editFromLabel,
    [modifier('pencil-on-hover')]: onlyShowPencilOnHover,
  });

  const onElementKeyUp = (e) => {
    if (e.which === KEYS.enter) {
      if (!isEditing) {
        e.stopPropagation();
        setIsEditing(true);
      }
    }
  };

  const onLabelClick = (e) => {
    if (editFromLabel && !disabled && !readOnly) {
      e.stopPropagation();
      setIsEditing(true);
    }
  };

  const onInputKeyUp = (e) => {
    switch (e.which) {
      case KEYS.enter:
        e.stopPropagation();

        onInputBlur();
        break;

      case KEYS.esc:
        e.stopPropagation();

        // reset back to original value
        triggerChanging(originalValue.current);
        checkHasChanged(originalValue.current);

        setIsEditing(false);
        break;
    }
  };

  const triggerChanging = (v) => {
    onChanging(v);

    setNewValue(v);
  };

  const checkHasChanged = (v) => {
    if (value !== v) {
      onChange(v);
    }
  };

  const onInputBlur = () => {
    checkHasChanged(newValue);

    setTimeout(() => {
      if (isEditing && error === false) {
        setIsEditing(false);
        originalValue.current = newValue;
      }
    }, 0);
  };

  React.useEffect(() => {
    if (!isEditing) {
      setNewValue(value);
    }
  }, [value, isEditing]);

  React.useEffect(() => {
    if (error) {
      setIsEditing(true);
    }
  }, [error]);

  React.useEffect(() => {
    if (autoSelect && isEditing && inputRef.current !== null) {
      inputRef.current.select();
    }
  }, [autoSelect, isEditing]);

  /* https://popper.js.org/docs/v2/modifiers/event-listeners/
   * As the content of .field-container is dynamically changed depending on isEditing,
   * therefore it needs to adjust popper position manually.
   */
  const popperRef = React.useRef<PopperRefInstance>(null);
  /* eslint-disable react-hooks/exhaustive-deps */
  const forcePopperUpdate = React.useCallback(() => {
    if (popperRef.current) {
      popperRef.current.forceUpdate();
    }
  }, [isEditing]); // isEditing is implicit dependency, if isEditing is changed then component animation has been started.
  /* eslint-enable react-hooks/exhaustive-deps */

  useRequestAnimationFrames(
    forcePopperUpdate,
    Number(variables.transitionDuration)
  );

  return (
    <div
      data-testid="inline-edit"
      tabIndex={disabled ? -1 : 0}
      className={classes}
      onKeyUp={onElementKeyUp}
      {...extractDataAriaIdProps(rest)}>
      <Tooltip
        popperRef={popperRef}
        open={error || isEditing || isHovering}
        content={
          helperText && (
            <span data-inline-edit-helper-text={true}>{helperText}</span>
          )
        }
        type={error ? 'error' : 'info'}
        placement={error ? 'bottom' : 'right'}
        modifiers={modifiers}
        onToggle={setIsHovering}>
        <div className={element('field-container')}>
          {isEditing && !disabled && !readOnly ? (
            <TextField
              ref={inputRef}
              onBlur={onInputBlur}
              onKeyUp={onInputKeyUp}
              onChange={triggerChanging}
              error={error}
              value={newValue ?? undefined}
              autoFocus={!error}
              maxLength={maxLength}
              placeholder={placeholder}
              onPaste={onPaste}
            />
          ) : (
            <>
              <Label
                editFromLabel={editFromLabel}
                value={newValue}
                placeholder={placeholder}
                showAsLink={showAsLink}
                onClick={onLabelClick}
              />

              {!readOnly ? (
                <span className={cn(element('pencil'))}>
                  <IconButton
                    disabled={disabled}
                    onClick={(e) => {
                      e.stopPropagation();
                      setIsEditing(true);
                    }}
                    data-testid="inline-edit-pencil">
                    <Icon path={pencil} />
                  </IconButton>
                </span>
              ) : null}
            </>
          )}
        </div>
      </Tooltip>
    </div>
  );
}

export type InlineEditProps<V extends string | number = string> = {
  value?: V | null;
  maxLength?: number;
  disabled?: boolean;
  error?: boolean;
  required?: boolean;
  showAsLink?: boolean;
  autoSelect?: boolean;
  helperText?: ReactNode;
  placeholder?: string;
  onPaste?: () => void;
  onChange?: (value: V) => void;
  onChanging?: (value: V) => void;
  editFromLabel?: boolean;
  defaultIsEditing?: boolean;
  onlyShowPencilOnHover?: boolean;
  readOnly?: boolean;
};

export default InlineEdit;
