import React, {MouseEventHandler} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';

import classNames from './InlineEdit.module.css';

const {element} = bemFactory('comd-inline-edit', classNames);

export const Label = ({
  value,
  onClick,
  placeholder,
  showAsLink,
  editFromLabel,
}: LabelProps) => {
  const labelClassName = cn(element('label'), {
    [element('label', 'show-as-link')]: showAsLink,
    [element('label', 'edit-from-label')]: editFromLabel,
  });

  const hasValue = value != null && String(value).length > 0;

  if (hasValue) {
    if (showAsLink) {
      return (
        <a
          target="#"
          href={String(value)}
          data-inline-edit-label=""
          className={labelClassName}>
          {value}
        </a>
      );
    }
    return (
      <label
        className={labelClassName}
        data-inline-edit-label=""
        onClick={onClick}>
        {value}
      </label>
    );
  }

  return (
    <label
      className={cn(element('placeholder'))}
      data-inline-edit-placeholder=""
      onClick={onClick}>
      {placeholder}
    </label>
  );
};

type LabelProps = {
  value?: string | number | null;
  onClick?: MouseEventHandler;
  hasValue?: boolean;
  isEditing?: boolean;
  showAsLink?: boolean;
  placeholder?: string;
  editFromLabel?: boolean;
};

export default Label;
