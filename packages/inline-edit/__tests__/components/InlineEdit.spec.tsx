import React from 'react';
import {
  fireEvent,
  render as renderRTL,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import user from '@testing-library/user-event';
import {act} from 'react-dom/test-utils';

import {InlineEdit, InlineEditProps} from '../../src/components/InlineEdit';

const defaultProps: InlineEditProps = {};

const render = (props: Partial<InlineEditProps> = {}) =>
  renderRTL(<InlineEdit {...props} {...defaultProps} />);

const renderWithRTL = (props: Partial<InlineEditProps> = {}) =>
  renderRTL(<InlineEdit {...props} {...defaultProps} />);

describe('Jotunheim React Inline Edit :: ', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should render label instead of placeholder when value is set', () => {
    render({
      value: 'test',
    });

    expect(screen.getByText('test')).toHaveAttribute('data-inline-edit-label');
    expect(screen.getByText('test')).not.toHaveAttribute(
      'data-inline-edit-placeholder'
    );
  });

  it('should render placeholder instead of label when value is not set', () => {
    render();
    expect(screen.getByText('Click to edit...')).not.toHaveAttribute(
      'data-inline-edit-label'
    );
    expect(screen.getByText('Click to edit...')).toHaveAttribute(
      'data-inline-edit-placeholder'
    );
  });

  it('should render placeholder when value is null', () => {
    render({
      value: null,
    });
    expect(screen.getByText('Click to edit...')).toHaveAttribute(
      'data-inline-edit-placeholder'
    );
  });

  it('should render placeholder when value is undefined', () => {
    render({
      value: undefined,
    });

    expect(screen.getByText('Click to edit...')).toHaveAttribute(
      'data-inline-edit-placeholder'
    );
  });

  it('should render label as link when showAsLink is on', () => {
    render({
      value: '#',
      showAsLink: true,
    });
    expect(screen.getByRole('link')).toHaveAttribute('data-inline-edit-label');
  });

  it('should not render label as link when showAsLink is off', () => {
    render({
      value: '#',
      showAsLink: false,
    });
    expect(screen.getByText('#')).toHaveAttribute('data-inline-edit-label');
    expect(screen.queryByRole('link')).not.toBeInTheDocument();
  });

  it('should toggle text-field when button is clicked', (done) => {
    act(() => {
      render();
      expect(screen.queryByRole('textbox')).not.toBeInTheDocument();
      user.click(screen.getByRole('button'));

      setTimeout(() => {
        renderWithRTL();
        expect(screen.queryByRole('textbox')).toBeInTheDocument();

        done();
      }, 100);
    });
  });

  it('should toggle text-field when label is clicked', async () => {
    const {getByText, queryByRole, queryByText} = renderWithRTL();

    expect(queryByRole('textbox')).not.toBeInTheDocument();

    user.click(getByText('Click to edit...'));
    expect(queryByText('Click to edit...')).not.toBeInTheDocument();
    expect(queryByRole('textbox')).toBeInTheDocument();
  });

  it('should render component with correct value after onChange is called with empty string', async () => {
    const onChange = jest.fn(() => {});
    const defaultValue = 'not empty';
    const {getByTestId, getByRole, queryByText} = renderWithRTL({
      value: defaultValue,
      onChange,
    });

    expect(queryByText(defaultValue)).toBeInTheDocument();
    expect(queryByText('Click to edit')).not.toBeInTheDocument();

    user.click(getByTestId('inline-edit-pencil'));
    user.clear(getByRole('textbox'));
    fireEvent.blur(getByRole('textbox'));
    await waitForElementToBeRemoved(getByRole('textbox'));
    expect(queryByText(defaultValue)).toBeInTheDocument();
    expect(queryByText('Click to edit')).not.toBeInTheDocument();

    user.click(getByTestId('inline-edit-pencil'));
    user.type(getByRole('textbox'), defaultValue);
    fireEvent.blur(getByRole('textbox'));
    await waitForElementToBeRemoved(getByRole('textbox'));
    expect(queryByText(defaultValue)).toBeInTheDocument();
    expect(queryByText('Click to edit')).not.toBeInTheDocument();

    expect(onChange).toBeCalledWith('');
  });

  it('should not reset to invalid value', (done) => {
    const onChange = jest.fn(() => {});
    const onChanging = jest.fn(() => {});
    render({value: 'valid', onChanging, onChange});

    user.click(screen.getByRole('button'), {});

    setTimeout(() => {
      render({value: 'invalid', error: true});

      expect(onChanging).not.toHaveBeenCalled();
      expect(onChange).not.toHaveBeenCalled();

      fireEvent.keyUp(screen.getAllByTestId('simple-text-field')[1], {
        which: 27,
      });

      done();
    }, 100);
  });

  it('should be closed by default', () => {
    render();
    expect(screen.queryByRole('textbox')).not.toBeInTheDocument();
  });

  it('should be open when defaultIsEditing is set', () => {
    render({defaultIsEditing: true});

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('should be open when error is set', () => {
    const {queryByRole} = renderWithRTL({error: true});

    expect(queryByRole('textbox')).toBeInTheDocument();
  });

  describe('Disabled state', () => {
    it('should not toggle to TextField on clicking the label', () => {
      const {getByText, queryByRole, queryByText} = renderWithRTL({
        disabled: true,
        autoSelect: true,
      });
      expect(queryByRole('textbox')).not.toBeInTheDocument();

      user.click(getByText('Click to edit...'));
      expect(queryByText('Click to edit...')).toBeInTheDocument();
      expect(queryByRole('textbox')).not.toBeInTheDocument();
    });

    it('should not toggle to TextField on clicking the Icon', () => {
      const {getByTestId, queryByRole, queryByText} = renderWithRTL({
        disabled: true,
        autoSelect: true,
      });

      expect(queryByRole('textbox')).not.toBeInTheDocument();

      user.click(getByTestId('inline-edit-pencil'));
      expect(queryByText('Click to edit...')).toBeInTheDocument();
      expect(queryByRole('textbox')).not.toBeInTheDocument();
    });
  });
});
