import React from 'react';
import {render, screen} from '@testing-library/react';
import HorizontalScrollWrapper from '../../src/components/HorizontalScrollWrapper';

describe('Jotunheim React Scroll Wrapper :: ', () => {
  it('renders', () => {
    render(
      <HorizontalScrollWrapper>
        <span>hello world</span>
        <span>hello world</span>
        <span>hello world</span>
      </HorizontalScrollWrapper>
    );
    expect(screen.getByTestId('io-first')).toBeInTheDocument();
    expect(screen.getByTestId('io-last')).toBeInTheDocument();
    expect(screen.getAllByText('hello world')).toHaveLength(3);
  });
});
