import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {HorizontalScrollWrapper} from '../src';
import Button from '../../button/src';

storiesOf('Components/scroll-wrapper', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Horizontal scroll', () => {
    const count = 15;
    return (
      <HorizontalScrollWrapper>
        {Array.from(Array(count)).map((_, index) => (
          <Button key={index}>Scroll</Button>
        ))}
      </HorizontalScrollWrapper>
    );
  });
