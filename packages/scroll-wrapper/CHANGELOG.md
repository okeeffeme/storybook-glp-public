# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.39&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.39&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.37&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.38&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.36&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.37&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.35&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.36&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.34&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.35&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.33&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.34&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.32&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.33&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.31&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.32&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.30&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.31&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.29&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.30&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- adding data-testid to HorizontalScrollWrapper ([NPM-1206](https://jiraosl.firmglobal.com/browse/NPM-1206)) ([f3ae0b9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f3ae0b9e6362b5e7c08984c7bbbc20a4d782f6fe))
- fixing ScrollWrapper components ([NPM-1206](https://jiraosl.firmglobal.com/browse/NPM-1206)) ([14350d3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/14350d3f18f68aa9fd6e9832a13d58cc337d8d73))
- fixing ScrollWrapper components ([NPM-1206](https://jiraosl.firmglobal.com/browse/NPM-1206)) ([50c3eda](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/50c3eda55d7427ceeeb5850f677bf3c5618f5cbd))

## [1.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.28&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.29&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.27&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.28&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.26&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.27&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.25&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.26&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.23&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.25&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.23&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.24&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.22&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.23&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.21&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.22&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.19&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.21&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.19&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.20&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.16&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.19&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.16&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.18&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.16&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.17&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.15&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.16&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.14&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.15&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.12&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.13&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.11&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.12&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of IconButton from ScrollWrapper ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([d5f3cbb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d5f3cbbd00dddfd9714c66749e075784c663e0cb))

## [1.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.10&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.9&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.8&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.6&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.3&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.2&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.1&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.0&sourceBranch=refs/tags/@jotunheim/react-scroll-wrapper@1.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-scroll-wrapper

# 1.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [0.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.33&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.34&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.32&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.33&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.31&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.32&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [0.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.30&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.31&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.29&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.30&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.27&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.28&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.26&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.27&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.25&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.26&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.24&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.25&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.23&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.24&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.22&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.23&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.19&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.20&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.18&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.19&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.17&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.18&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.16&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.17&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.15&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.16&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.14&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.15&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.13&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.14&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.12&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.13&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.10&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.11&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.9&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.10&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.8&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.9&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.7&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.8&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.6&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.7&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.5&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.6&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.4&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.5&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.3&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.4&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.2&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.1&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.2&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

## [0.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.0&sourceBranch=refs/tags/@confirmit/react-scroll-wrapper@0.1.1&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-scroll-wrapper

# 0.1.0 (2021-06-22)

### Features

- added Horizontal Scroll Wrapper ([7944032](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/794403280e7e274c9ff88a3cd5fd0942220cdfe0))

# 0.1.0 (2021-06-21)

### Features

- added Horizontal Scroll Wrapper ([7944032](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/794403280e7e274c9ff88a3cd5fd0942220cdfe0))

## CHANGELOG

### v0.0.1

- Initial version
