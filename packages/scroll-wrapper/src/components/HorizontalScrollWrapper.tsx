import React, {useRef, useEffect, useState} from 'react';
import ScrollButton from './ScrollButton';
import cn from 'classnames';
import classNames from './HorizontalScrollWrapper.module.css';

import {bemFactory} from '@jotunheim/react-themes';

type HorizontalScrollWrapperProps = {
  selectedElementId?: string;
  children: React.ReactElement[];
};

const INTERSECTION_THRESHOLD = 0.9;

const {block, element} = bemFactory({
  baseClassName: 'comd-horizontal-scroll-wrapper',
  classNames,
});

const HorizontalScrollWrapper = ({
  selectedElementId,
  children,
}: HorizontalScrollWrapperProps) => {
  const [showScrollLeft, setShowScrollLeft] = useState(false);
  const [showScrollRight, setShowScrollRight] = useState(false);
  const firstElement = useRef<HTMLDivElement | null>(null);
  const lastElement = useRef<HTMLDivElement | null>(null);
  const elementsScrollContainer = useRef<HTMLDivElement | null>(null);
  const elementsContainer = useRef<HTMLDivElement | null>(null);
  const selectedElement = useRef<HTMLElement | null>(null);
  const prevSelectedElementId = useRef<string | null | undefined>(null);

  const childrenArray = React.Children.toArray(children);

  if (
    !childrenArray.find(
      (child) => (child as React.ReactElement).props.id === selectedElementId
    )
  ) {
    selectedElementId = children[0]?.props?.id;
  }

  const getScrollDistance = () => {
    // To avoid scrolling past any items, only scroll 1/3 of the container width
    if (elementsScrollContainer.current) {
      return elementsScrollContainer.current.clientWidth / 3;
    }
    return 0;
  };

  const handleScrollLeftClick = (e) => {
    e.preventDefault();

    if (elementsScrollContainer.current) {
      elementsScrollContainer.current.scrollLeft -= getScrollDistance();
    }
  };

  const handleScrollRightClick = (e) => {
    e.preventDefault();
    if (elementsScrollContainer.current) {
      elementsScrollContainer.current.scrollLeft += getScrollDistance();
    }
  };

  const scrollToCurrentTab = () => {
    if (!elementsContainer.current) return;

    const scrollButton = elementsContainer.current.querySelector(
      '[data-scroll-button-container]'
    );
    if (!scrollButton) {
      return;
    }

    // subtract width of scroll button to avoid start of tab being hidden below scroll button
    if (elementsScrollContainer.current && selectedElement.current) {
      elementsScrollContainer.current.scrollLeft =
        selectedElement.current.offsetLeft - scrollButton.clientWidth;
    }
  };

  useEffect(() => {
    if (window.IntersectionObserver && elementsScrollContainer.current) {
      const isScrollable =
        elementsScrollContainer.current.scrollWidth >
        elementsScrollContainer.current.offsetWidth;

      if (!isScrollable) {
        setShowScrollLeft(false);
        setShowScrollRight(false);
      }

      const onIntersection = (entries) => {
        entries.forEach((entry) => {
          switch (entry.target) {
            case firstElement.current:
              setShowScrollLeft(
                !entry.isIntersecting ||
                  entry.intersectionRatio < INTERSECTION_THRESHOLD
              );
              break;

            case lastElement.current:
              setShowScrollRight(
                !entry.isIntersecting ||
                  entry.intersectionRatio < INTERSECTION_THRESHOLD
              );
              break;

            case selectedElement.current:
              if (
                !entry.isIntersecting &&
                prevSelectedElementId.current !== selectedElementId
              ) {
                // only auto-scroll if selectedTabId changed, otherwise you cannot use the left/right
                // buttons to scroll manually
                scrollToCurrentTab();
              }
              break;
          }
        });
        prevSelectedElementId.current = selectedElementId;
      };

      const intersectionObserver = new IntersectionObserver(onIntersection, {
        root: elementsScrollContainer.current,
        threshold: INTERSECTION_THRESHOLD,
      });

      [firstElement, lastElement, selectedElement].forEach(
        (el) => el.current && intersectionObserver.observe(el.current)
      );

      return () => intersectionObserver.disconnect();
    }
  }, [selectedElementId, children]);

  return (
    <div className={block()} ref={elementsContainer}>
      {showScrollLeft && (
        <ScrollButton direction="left" onClick={handleScrollLeftClick} />
      )}
      <div
        className={cn(element('scroll-container'), {
          [element('scroll-container--no-intersectionobserver')]:
            !window.IntersectionObserver,
        })}
        ref={elementsScrollContainer}>
        <div
          className={cn(element('io'), element('io', 'first'))}
          ref={firstElement}
          data-testid={'io-first'}
          data-test-id={'io-first'}
        />
        <div className={element('scroll-content')}>
          {React.Children.map(children, (child) => {
            return React.cloneElement(child, {
              ref: (el) => {
                if (child === children[0]) {
                  firstElement.current = el;
                  selectedElement.current = el;
                } else if (child === children[children.length - 1]) {
                  lastElement.current = el;
                }
              },
            });
          })}
        </div>
        <div
          className={cn(element('io'), element('io', 'last'))}
          ref={lastElement}
          data-testid={'io-last'}
          data-test-id={'io-first'}
        />
      </div>
      {showScrollRight && (
        <ScrollButton direction="right" onClick={handleScrollRightClick} />
      )}
    </div>
  );
};

export default HorizontalScrollWrapper;
