import React from 'react';
import cn from 'classnames';

import {IconButton} from '@jotunheim/react-button';
import Icon, {chevronRight, chevronLeft} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './ScrollButton.module.css';

type ScrollButtonProps = {
  direction: 'left' | 'right';
  onClick: (event: React.MouseEvent) => void;
};

const {element} = bemFactory({
  baseClassName: 'comd-scroll-button',
  classNames,
});

export const ScrollButton = ({direction, onClick}: ScrollButtonProps) => {
  return (
    <div
      data-scroll-button-container
      className={cn(element('container'), element('container', direction))}>
      <div className={element('scroll-icon-button')}>
        <IconButton onClick={onClick}>
          <Icon path={direction === 'right' ? chevronRight : chevronLeft} />
        </IconButton>
      </div>
    </div>
  );
};

export default ScrollButton;
