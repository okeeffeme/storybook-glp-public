import {findMessageNodes} from '../../src/state/utils';
import {messageGroupData} from '../utils';

describe('Jotunheim React Inbox :: state.utils ::', () => {
  describe('findMessageNodes', () => {
    it('findMessageNodes should return previousMessage, nextMessage, and message based on the node', () => {
      messageGroupData.forEach((groupNode, i) => {
        groupNode.messages.forEach((messageNode, n) => {
          const state = {
            selectedMessagePath: [i, n],
            transitioningMessagePath: [],
            groupDeleteTransitionIndex: undefined,
            contentTransitionDirection: undefined,
          };

          const {previousMessage, nextMessage, message} = findMessageNodes({
            state,
            messageGroups: messageGroupData,
          });

          if (i === 0 && n === 0) {
            expect(previousMessage).toBe(null);
          } else if (n > 0) {
            expect(previousMessage?.id).toBe(groupNode.messages[n - 1].id);
          } else {
            expect(previousMessage?.id).toBe(
              messageGroupData[i - 1].messages[
                messageGroupData[i - 1].messages.length - 1
              ].id
            );
          }

          if (
            i === messageGroupData.length - 1 &&
            n === groupNode.messages.length - 1
          ) {
            expect(nextMessage).toBe(null);
          } else if (n < groupNode.messages.length - 1) {
            expect(nextMessage?.id).toBe(groupNode.messages[n + 1].id);
          } else {
            expect(nextMessage?.id).toBe(
              messageGroupData[i + 1].messages[0].id
            );
          }

          expect(message.id).toBe(messageNode.id);
        });
      });
    });
  });
});
