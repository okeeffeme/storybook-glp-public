export const messageGroupData = [
  {
    title: 'Today',
    messages: [
      {
        id: 107,
        title: 'Received 9:10am PT',
        isRead: false,
        text: 'message 107',
      },
      {
        id: 106,
        title: 'Received 6:49am PT',
        isRead: false,
        text: 'message 106',
      },
    ],
  },
  {
    title: 'Yesterday',
    messages: [
      {
        id: 105,
        title: 'Received 10:30pm PT',
        isRead: true,
        text: 'message 105',
      },
      {
        id: 104,
        title: 'Received 7:52pm PT',
        isRead: false,
        text: 'message 104',
      },
      {
        id: 103,
        title: 'Received 12:02pm PT',
        isRead: true,
        text: 'message 103',
      },
      {
        id: 102,
        title: 'Received 10:23am PT',
        isRead: true,
        text: 'message 102',
      },
    ],
  },
  {
    title: 'This Week',
    messages: [
      {
        id: 101,
        title: 'Received 11:22pm PT',
        isRead: false,
        text: 'message 101',
      },
      {
        id: 100,
        title: 'Received 5:32pm PT',
        isRead: false,
        text: 'message 100',
      },
    ],
  },
];
