import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {messageGroupData} from '../utils';

import Inbox from '../../src';

const selectors = {
  closeButton: 'inbox-list-close-button',
  messagesPlaceholder: 'inbox-messages-placeholder',
  messageGroups: 'inbox-message-group',
  messageGroupTitle: '[data-react-inbox-message-group-title]',
  message: '[data-react-inbox-message]',
  listViewActive: 'inbox-list-view-active',
  messageViewActive: 'inbox-message-view-active',
  messageText:
    '[data-react-inbox-active-message] [data-react-inbox-message-text]',
  deleteMessageButton: 'inbox-delete-message-button',
  previousNavigationButton: 'inbox-previous-navigation-button',
  nextNavigationButton: '[data-react-inbox-next-navigation-button] button',
  deleteMessageGroupButton:
    '[data-react-inbox-delete-message-group-button] button',
};

describe('Jotunheim React Inbox :: Inbox ::', () => {
  it('should render messages placeholder if there are no messages', () => {
    render(<Inbox messageGroups={[]} />);

    expect(screen.getAllByTestId('inbox-messages-placeholder')).toHaveLength(1);
  });

  it('should render closeButton when onClose is defined', () => {
    render(<Inbox onClose={() => {}} />);

    expect(screen.getByTestId('inbox-list-close-button')).toBeInTheDocument();
  });

  it('should not render closeButton when onClose is not defined', () => {
    render(<Inbox />);

    expect(
      screen.queryByTestId('inbox-list-close-button')
    ).not.toBeInTheDocument();
  });

  it('should render message groups and its messages by default if there are messages', () => {
    render(<Inbox messageGroups={messageGroupData} />);

    const messageGroups = screen.getAllByTestId('inbox-message-group');

    messageGroups.forEach((group, i) => {
      expect(
        group.querySelector('[data-react-inbox-message-group-title]')
          ?.textContent
      ).toEqual(messageGroupData[i].title);

      const messages = group.querySelectorAll('[data-react-inbox-message]');

      messages.forEach((message, n) => {
        expect(message.textContent).toEqual(
          messageGroupData[i].messages[n].text
        );
      });

      expect(messages.length).toBe(messageGroupData[i].messages.length);
    });

    expect(messageGroups.length).toBe(messageGroupData.length);
  });

  it('should call onSelect with correct args and display message content when message is clicked', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    const messageData = messageGroupData[0].messages[0];
    const firstMessage = screen.getAllByTestId('more-button')[0];

    userEvent.click(firstMessage);

    expect(handleSelect).toHaveBeenCalledTimes(2);
    expect(handleSelect).toHaveBeenCalledWith(messageData.id);
    expect(screen.getAllByText(messageData.text)).toHaveLength(3);
  });

  it('list view should be active by default and not active when message is clicked', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    expect(screen.getByTestId('inbox-list-view-active')).toHaveAttribute(
      'data-react-inbox-list-view-active',
      'true'
    );
    expect(screen.getByTestId('inbox-message-view-active')).toHaveAttribute(
      'data-react-inbox-message-view-active',
      'false'
    );

    userEvent.click(screen.getAllByTestId('more-button')[0]);

    expect(screen.getByTestId(selectors.listViewActive)).toHaveAttribute(
      'data-react-inbox-list-view-active',
      'false'
    );
    expect(screen.getByTestId(selectors.messageViewActive)).toHaveAttribute(
      'data-react-inbox-message-view-active',
      'true'
    );
  });

  it('should call onDelete with correct args and list view should be displayed when message is deleted', () => {
    const handleSelect = jest.fn();
    const handleDelete = jest.fn();

    render(
      <Inbox
        messageGroups={messageGroupData}
        onSelect={handleSelect}
        onDelete={handleDelete}
      />
    );

    userEvent.click(screen.getAllByTestId('more-button')[0]);
    userEvent.click(screen.getAllByTestId('inbox-delete-message-button')[0]);

    expect(screen.getAllByTestId('inbox-list-view-active')).toHaveLength(1);
    expect(handleDelete).toHaveBeenCalledTimes(1);
    expect(handleDelete).toHaveBeenCalledWith([
      messageGroupData[0].messages[0].id,
    ]);
  });

  it('should call onSelect with correct args and display correct message content when message is navigated', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    userEvent.click(screen.getAllByTestId('more-button')[0]);
    userEvent.click(screen.getByTestId('inbox-next-navigation-button'));

    expect(handleSelect).toHaveBeenCalledTimes(3);
    expect(handleSelect).toHaveBeenCalledWith(
      messageGroupData[0].messages[1].id
    );

    userEvent.click(screen.getByTestId('inbox-previous-navigation-button'));

    expect(handleSelect).toHaveBeenCalledTimes(4);
    expect(handleSelect).toHaveBeenCalledWith(
      messageGroupData[0].messages[0].id
    );
  });

  it('previous navigation button should not be rendered for the first message', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    userEvent.click(screen.getAllByTestId('more-button')[0]);

    expect(
      screen.queryAllByTestId('inbox-previous-navigation-button')
    ).toHaveLength(0);
    expect(screen.getAllByTestId('inbox-next-navigation-button')).toHaveLength(
      1
    );
  });

  it('next navigation button should not be rendered for the last message', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    const messageCount = messageGroupData.reduce((acc, cur) => {
      acc += cur.messages.length;
      return acc;
    }, 0);

    userEvent.click(screen.getAllByTestId('more-button')[messageCount - 1]);

    expect(
      screen.getAllByTestId('inbox-previous-navigation-button')
    ).toHaveLength(1);
    expect(
      screen.queryAllByTestId('inbox-next-navigation-button')
    ).toHaveLength(0);
  });

  it('close navigation button should be rendered for message when onClose is defined', () => {
    const handleSelect = jest.fn();
    const handleClose = jest.fn();

    render(
      <Inbox
        messageGroups={messageGroupData}
        onSelect={handleSelect}
        onClose={handleClose}
      />
    );

    expect(
      screen.queryByTestId('inbox-message-close-button')
    ).not.toBeInTheDocument();

    userEvent.click(screen.getAllByTestId('inbox-message')[0]);

    expect(
      screen.queryByTestId('inbox-message-close-button')
    ).toBeInTheDocument();
  });

  it('close navigation button should not be rendered for message when onClose is not defined', () => {
    const handleSelect = jest.fn();

    render(<Inbox messageGroups={messageGroupData} onSelect={handleSelect} />);

    expect(
      screen.queryByTestId('inbox-message-close-button')
    ).not.toBeInTheDocument();

    userEvent.click(screen.getAllByTestId('inbox-message')[0]);

    expect(
      screen.queryByTestId('inbox-message-close-button')
    ).not.toBeInTheDocument();
  });
});
