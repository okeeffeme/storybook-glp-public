import React, {useCallback, useReducer, useEffect} from 'react';
import {storiesOf} from '@storybook/react';
import {produce} from 'immer';

import Inbox from '../src/components/inbox';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {messageData, moreMessages} from './message-data';
import {Button} from '../../button/src';
import {trashBin} from '../../icons/src';

storiesOf('Components/inbox', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const optionsMenuItems = [
      {
        command: 'markAllAsRead',
        name: `Mark All as 'Read'`,
      },
      {
        command: 'deleteAll',
        name: 'Delete All Messages',
        iconPath: trashBin,
      },
    ];

    const initialState = {
      messageGroups: [],
      isLoading: false,
      isMoreLoading: false,
      hasMore: true,
    };

    const reducer = produce((draft, {type, payload}) => {
      switch (type) {
        case 'fetch':
          draft.messageGroups = payload;
          draft.isLoading = false;
          break;

        case 'select':
          draft.messageGroups.forEach((group) => {
            group.messages.forEach((message) => {
              if (message.id === payload) {
                message.isRead = true;
              }
            });
          });
          break;

        case 'delete':
          draft.messageGroups.forEach((group, i) => {
            for (let n = 0; n < group.messages.length; n++) {
              const message = group.messages[n];
              if (payload.includes(message.id)) {
                if (payload.length > 1 || group.messages.length === 1) {
                  draft.messageGroups.splice(i, 1);
                  break;
                } else {
                  draft.messageGroups[i].messages.splice(n, 1);
                }
              }
            }
          });
          break;

        case 'markAllAsRead':
          draft.messageGroups.forEach((group) => {
            group.messages.forEach((message) => {
              message.isRead = true;
            });
          });
          break;

        case 'deleteAll':
          draft.messageGroups = [];
          draft.hasMore = false;
          break;

        case 'loadMore': {
          const lastGroup = draft.messageGroups.length
            ? {
                ...draft.messageGroups[draft.messageGroups.length - 1],
              }
            : {
                title: 'This Week',
                messages: [],
              };
          draft.messageGroups[
            draft.messageGroups.length ? draft.messageGroups.length - 1 : 0
          ] = {
            ...lastGroup,
            messages: [
              ...lastGroup.messages,
              ...moreMessages.map((message, i) => ({
                ...message,
                id:
                  i +
                  (lastGroup.messages.length
                    ? lastGroup.messages[lastGroup.messages.length - 1].id
                    : 0) +
                  1,
              })),
            ],
          };
          draft.hasMore = false;
          draft.isLoading = false;
          draft.isMoreLoading = false;
          break;
        }

        case 'setLoading': {
          draft.isLoading = true;
          if (payload === 'loadMore') {
            draft.isMoreLoading = true;
          }
          break;
        }
      }
      return draft;
    });

    const [toggled, setToggled] = React.useState(false);

    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
      if (toggled) {
        dispatch({type: 'setLoading', payload: 'fetch'});
        // mock async initial fetch
        setTimeout(() => dispatch({type: 'fetch', payload: messageData}), 1000);
      }
    }, [dispatch, toggled]);

    const handleOptionSelect = useCallback(
      (command) => dispatch({type: command}),
      [dispatch]
    );

    const handleSelect = useCallback(
      (id) => dispatch({type: 'select', payload: id}),
      [dispatch]
    );

    const handleDelete = useCallback(
      (ids) => dispatch({type: 'delete', payload: ids}),
      [dispatch]
    );

    const handleLoadMore = useCallback(() => {
      dispatch({type: 'setLoading', payload: 'loadMore'});
      // mock async fetch
      setTimeout(() => dispatch({type: 'loadMore'}), 1000);
    }, [dispatch]);

    if (!toggled) {
      return <Button onClick={() => setToggled(true)}>Inbox</Button>;
    }
    return (
      <div
        style={{
          right: 0,
          background: 'white',
          height: 800,
          width: 600,
        }}>
        <Inbox
          optionsMenuItems={optionsMenuItems}
          onOptionSelect={handleOptionSelect}
          onSelect={handleSelect}
          onDelete={handleDelete}
          onLoadMore={handleLoadMore}
          messageGroups={state.messageGroups}
          isLoading={state.isLoading}
          isMoreLoading={state.isMoreLoading}
          hasMore={state.hasMore}
          onClose={() => setToggled(false)}
        />
      </div>
    );
  });
