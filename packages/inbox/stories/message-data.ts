export const messageData = [
  {
    title: 'Today',
    messages: [
      {
        id: 99,
        title: 'Received 9:10am PT',
        isRead: false,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis aliquam faucibus purus in massa. Nunc congue nisi vitae suscipit. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Nibh venenatis cras sed felis eget velit. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Accumsan lacus vel facilisis volutpat est velit egestas. Erat nam at lectus urna duis convallis convallis tellus id. Tincidunt eget nullam non nisi est sit amet facilisis magna. Erat nam at lectus urna duis convallis. In nibh mauris cursus mattis molestie a iaculis at erat.',
      },
      {
        id: 98,
        title: 'Received 6:49am PT',
        isRead: false,
        text: 'Ullamcorper a lacus vestibulum sed arcu non odio euismod. In metus vulputate eu scelerisque. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Volutpat odio facilisis mauris sit amet massa. Euismod in pellentesque massa placerat duis ultricies lacus sed. Morbi quis commodo odio aenean sed adipiscing diam donec. Cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Imperdiet massa tincidunt nunc pulvinar sapien. Ac turpis egestas sed tempus urna et. Bibendum enim facilisis gravida neque convallis a cras. Feugiat nibh sed pulvinar proin gravida hendrerit lectus. Cursus sit amet dictum sit amet justo. Magnis dis parturient montes nascetur ridiculus mus. Risus nec feugiat in fermentum posuere urna nec tincidunt. Neque volutpat ac tincidunt vitae semper. Nunc congue nisi vitae suscipit tellus. Mollis aliquam ut porttitor leo a. Donec et odio pellentesque diam. Sed velit dignissim sodales ut eu sem.',
      },
    ],
  },
  {
    title: 'Yesterday',
    messages: [
      {
        id: 89,
        title: 'Received 10:30pm PT',
        isRead: true,
        text: 'Eleifend donec pretium vulputate sapien. Dignissim enim sit amet venenatis urna cursus eget. Amet consectetur adipiscing elit ut aliquam purus sit. Feugiat in ante metus dictum. Scelerisque felis imperdiet proin fermentum leo vel. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at. Pharetra et ultrices neque ornare. Nisl tincidunt eget nullam non nisi est sit. Malesuada fames ac turpis egestas maecenas pharetra convallis posuere. Est ullamcorper eget nulla facilisi etiam. Pellentesque elit ullamcorper dignissim cras. Dui nunc mattis enim ut tellus elementum. Amet nisl purus in mollis nunc sed id semper risus. Suscipit tellus mauris a diam maecenas. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare.',
      },
      {
        id: 88,
        title: 'Received 7:52pm PT',
        isRead: false,
        text: 'Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Commodo viverra maecenas accumsan lacus vel facilisis. Scelerisque in dictum non consectetur a erat nam at. Blandit aliquam etiam erat velit scelerisque in dictum. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque. Eget gravida cum sociis natoque penatibus et magnis dis parturient. Urna nec tincidunt praesent semper feugiat nibh sed. Scelerisque felis imperdiet proin fermentum leo. Aliquet bibendum enim facilisis gravida neque convallis a cras semper. Egestas integer eget aliquet nibh praesent tristique. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu. Diam in arcu cursus euismod quis viverra nibh cras pulvinar.',
      },
      {
        id: 87,
        title: 'Received 12:02pm PT',
        isRead: true,
        text: 'Commodo ullamcorper a lacus vestibulum sed arcu. Rhoncus aenean vel elit scelerisque mauris. Magna fermentum iaculis eu non diam phasellus vestibulum. Hendrerit dolor magna eget est lorem ipsum. In fermentum posuere urna nec tincidunt praesent semper feugiat nibh. Dolor sit amet consectetur adipiscing elit pellentesque habitant. Tristique et egestas quis ipsum suspendisse ultrices. Neque sodales ut etiam sit amet nisl. Eget nullam non nisi est sit amet facilisis. Platea dictumst quisque sagittis purus sit. Diam quam nulla porttitor massa. Malesuada fames ac turpis egestas integer. Mi sit amet mauris commodo quis. Quis blandit turpis cursus in hac habitasse platea dictumst quisque. Ornare suspendisse sed nisi lacus sed viverra tellus in hac. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Feugiat in fermentum posuere urna nec tincidunt. Sit amet est placerat in egestas erat imperdiet sed euismod.',
      },
      {
        id: 86,
        title: 'Received 10:23am PT',
        isRead: true,
        text: 'Aliquam ultrices sagittis orci a. Duis at tellus at urna condimentum mattis pellentesque. Risus pretium quam vulputate dignissim suspendisse in est. Ut pharetra sit amet aliquam. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Proin sed libero enim sed faucibus. Faucibus scelerisque eleifend donec pretium vulputate. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis. Sit amet justo donec enim diam vulputate ut pharetra sit. Sit amet est placerat in. Orci sagittis eu volutpat odio facilisis. Lacinia quis vel eros donec. Faucibus in ornare quam viverra orci sagittis eu volutpat odio. Sem nulla pharetra diam sit. Netus et malesuada fames ac turpis egestas sed tempus urna. Risus viverra adipiscing at in tellus integer feugiat scelerisque. Placerat in egestas erat imperdiet sed euismod nisi porta lorem. Nulla facilisi cras fermentum odio eu feugiat pretium. Et ultrices neque ornare aenean euismod elementum nisi. Faucibus ornare suspendisse sed nisi lacus sed viverra.',
      },
    ],
  },
  {
    title: 'This Week',
    messages: [
      {
        id: 79,
        title: 'Received 11:22pm PT',
        isRead: false,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis aliquam faucibus purus in massa. Nunc congue nisi vitae suscipit. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Nibh venenatis cras sed felis eget velit. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Accumsan lacus vel facilisis volutpat est velit egestas. Erat nam at lectus urna duis convallis convallis tellus id. Tincidunt eget nullam non nisi est sit amet facilisis magna. Erat nam at lectus urna duis convallis. In nibh mauris cursus mattis molestie a iaculis at erat.',
      },
      {
        id: 78,
        title: 'Received 5:32pm PT',
        isRead: false,
        text: 'Ullamcorper a lacus vestibulum sed arcu non odio euismod. In metus vulputate eu scelerisque. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Volutpat odio facilisis mauris sit amet massa. Euismod in pellentesque massa placerat duis ultricies lacus sed. Morbi quis commodo odio aenean sed adipiscing diam donec. Cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Imperdiet massa tincidunt nunc pulvinar sapien. Ac turpis egestas sed tempus urna et. Bibendum enim facilisis gravida neque convallis a cras. Feugiat nibh sed pulvinar proin gravida hendrerit lectus. Cursus sit amet dictum sit amet justo. Magnis dis parturient montes nascetur ridiculus mus. Risus nec feugiat in fermentum posuere urna nec tincidunt. Neque volutpat ac tincidunt vitae semper. Nunc congue nisi vitae suscipit tellus. Mollis aliquam ut porttitor leo a. Donec et odio pellentesque diam. Sed velit dignissim sodales ut eu sem.',
      },
    ],
  },
];

export const moreMessages = [
  {
    id: 77,
    title: 'Received 11:22pm PT',
    isRead: true,
    text: 'Aliquam ultrices sagittis orci a. Duis at tellus at urna condimentum mattis pellentesque. Risus pretium quam vulputate dignissim suspendisse in est. Ut pharetra sit amet aliquam. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Proin sed libero enim sed faucibus. Faucibus scelerisque eleifend donec pretium vulputate. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis. Sit amet justo donec enim diam vulputate ut pharetra sit. Sit amet est placerat in. Orci sagittis eu volutpat odio facilisis. Lacinia quis vel eros donec. Faucibus in ornare quam viverra orci sagittis eu volutpat odio. Sem nulla pharetra diam sit. Netus et malesuada fames ac turpis egestas sed tempus urna. Risus viverra adipiscing at in tellus integer feugiat scelerisque. Placerat in egestas erat imperdiet sed euismod nisi porta lorem. Nulla facilisi cras fermentum odio eu feugiat pretium. Et ultrices neque ornare aenean euismod elementum nisi. Faucibus ornare suspendisse sed nisi lacus sed viverra.',
  },
  {
    id: 76,
    title: 'Received 5:32pm PT',
    isRead: true,
    text: 'Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Commodo viverra maecenas accumsan lacus vel facilisis. Scelerisque in dictum non consectetur a erat nam at. Blandit aliquam etiam erat velit scelerisque in dictum. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque. Eget gravida cum sociis natoque penatibus et magnis dis parturient. Urna nec tincidunt praesent semper feugiat nibh sed. Scelerisque felis imperdiet proin fermentum leo. Aliquet bibendum enim facilisis gravida neque convallis a cras semper. Egestas integer eget aliquet nibh praesent tristique. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu. Diam in arcu cursus euismod quis viverra nibh cras pulvinar.',
  },
  {
    id: 75,
    title: 'Received 3:01pm PT',
    isRead: false,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis aliquam faucibus purus in massa. Nunc congue nisi vitae suscipit. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Nibh venenatis cras sed felis eget velit. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec. Accumsan lacus vel facilisis volutpat est velit egestas. Erat nam at lectus urna duis convallis convallis tellus id. Tincidunt eget nullam non nisi est sit amet facilisis magna. Erat nam at lectus urna duis convallis. In nibh mauris cursus mattis molestie a iaculis at erat.',
  },
  {
    id: 74,
    title: 'Received 1:45pm PT',
    isRead: false,
    text: 'Ullamcorper a lacus vestibulum sed arcu non odio euismod. In metus vulputate eu scelerisque. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Volutpat odio facilisis mauris sit amet massa. Euismod in pellentesque massa placerat duis ultricies lacus sed. Morbi quis commodo odio aenean sed adipiscing diam donec. Cursus risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Imperdiet massa tincidunt nunc pulvinar sapien. Ac turpis egestas sed tempus urna et. Bibendum enim facilisis gravida neque convallis a cras. Feugiat nibh sed pulvinar proin gravida hendrerit lectus. Cursus sit amet dictum sit amet justo. Magnis dis parturient montes nascetur ridiculus mus. Risus nec feugiat in fermentum posuere urna nec tincidunt. Neque volutpat ac tincidunt vitae semper. Nunc congue nisi vitae suscipit tellus. Mollis aliquam ut porttitor leo a. Donec et odio pellentesque diam. Sed velit dignissim sodales ut eu sem.',
  },
];
