# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.12&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.13&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.12&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.12&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.10&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.11&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.9&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.10&targetRepoId=1246) (2023-03-30)

### Bug Fixes

- accept both string and number as message id ([NPM-1290](https://jiraosl.firmglobal.com/browse/NPM-1290)) ([dc7fe38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dc7fe38912e1e9c662077036afa8e89a606b6a9e))

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.8&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.9&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.7&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.8&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.6&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.7&targetRepoId=1246) (2023-03-24)

### Bug Fixes

- add classes on Truncated via data- attributes ([NPM-1269](https://jiraosl.firmglobal.com/browse/NPM-1269)) ([a1302fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a1302fa10532b5008c4d5c0c30adf0ebb63352d1))

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.5&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.6&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.4&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.5&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.3&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.4&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.2&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.1&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.2&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.1.0&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.1&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-inbox

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.25&sourceBranch=refs/tags/@jotunheim/react-inbox@5.1.0&targetRepoId=1246) (2023-02-09)

### Features

- convert inbox package to TypeScript ([NPM-1243](https://jiraosl.firmglobal.com/browse/NPM-1243)) ([e1fa25f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e1fa25f7c6073809149206e3fe04b9c1afe2f7a0))

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.24&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.25&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.23&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.24&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.22&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.23&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.21&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.22&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.20&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.21&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.19&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.20&targetRepoId=1246) (2023-01-27)

### Bug Fixes

- adding data-testids to Inbox components ([NPM-1196](https://jiraosl.firmglobal.com/browse/NPM-1196)) ([54d2156](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/54d2156105f9b74d820f2bd422a688bbbedb0c3d))

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.18&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.19&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.17&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.18&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.16&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.17&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.14&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.14&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.13&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.12&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.11&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.10&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.8&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.8&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.5&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.5&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.5&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.4&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.3&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.2&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-inbox

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@5.0.0&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-inbox

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.14&sourceBranch=refs/tags/@jotunheim/react-inbox@5.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Inbox ([NPM-941](https://jiraosl.firmglobal.com/browse/NPM-941)) ([6497d91](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6497d91964f85bc41063b84cc7cd26bc4c133aff))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.13&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.14&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- accept ReactNode in title ([AUT-6213](https://jiraosl.firmglobal.com/browse/AUT-6213)) ([c1099a9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1099a977f546954c4bac3e0794b05d69e07f9f0))
- only show Close button if onClose is defined ([AUT-6213](https://jiraosl.firmglobal.com/browse/AUT-6213)) ([e801d50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e801d50d3e81cb3286ec6274e91f1b42a4625231))
- remove theme support ([NPM-1032](https://jiraosl.firmglobal.com/browse/NPM-1032)) ([7b50f28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7b50f2862f92cce0db2371182f906697725f3b9e))

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.12&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.11&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.10&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.9&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.8&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.6&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.3&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.2&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.1&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-inbox

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-inbox@4.0.0&sourceBranch=refs/tags/@jotunheim/react-inbox@4.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-inbox

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.15&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.16&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.14&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.15&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.13&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.14&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- update usages of Dropdown ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([d4788da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d4788da8098f822eef8fc5c6c77489af8836cc00))

## [3.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.12&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.13&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.11&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.12&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.10&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.11&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.8&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.9&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.7&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.8&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.6&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.7&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.5&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.6&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.4&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.5&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-inbox

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@3.0.3&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.4&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-inbox

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.14&sourceBranch=refs/tags/@confirmit/react-inbox@3.0.0&targetRepoId=1246) (2022-02-11)

### BREAKING CHANGES

- Remove deprecated icon prop in options menu (NPM-858)

## [2.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.13&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.14&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.12&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.13&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.11&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.12&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.10&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.11&targetRepoId=1246) (2022-01-13)

### Bug Fixes

- Adding preventDefault to onClick in inbox options-menu ([STUD-3182](https://jiraosl.firmglobal.com/browse/STUD-3182)) ([58757ee](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/58757ee7d0977e3913150c4c4fd6b3a5d4b33c9e))

## [2.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.9&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.10&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.8&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.9&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.7&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.8&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.6&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.7&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.5&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.6&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [2.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.4&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.5&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.2&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.1&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.2&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.1.0&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-inbox

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.56&sourceBranch=refs/tags/@confirmit/react-inbox@2.1.0&targetRepoId=1246) (2021-09-13)

### Features

- support iconPath on optionsMenu for Inbox component. This deprecates previous icon property, and icon property will be removed in the future ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([b4bab31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b4bab3176ad71373bf7145432ab975ac36c2ecfc))

## [2.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.55&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.56&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.54&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.55&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.53&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.54&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.52&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.53&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.51&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.52&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.50&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.51&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.49&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.50&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.48&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.49&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.47&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.48&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.46&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.47&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.45&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.46&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.44&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.45&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.43&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.44&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.42&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.43&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.41&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.42&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.40&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.41&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.39&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.40&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.38&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.39&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.37&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.38&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.36&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.37&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.35&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.36&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.33&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.34&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.32&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.33&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.31&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.32&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.30&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.31&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.29&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.30&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.28&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.29&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.27&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.28&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.26&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.27&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.25&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.24&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.22&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.23&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.21&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.20&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.19&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.18&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.17&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.18&targetRepoId=1246) (2020-12-18)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.16&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.15&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.14&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.13&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.12&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.11&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.10&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.7&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.4&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.3&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-inbox

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@2.0.2&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-inbox

# [2.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.39&sourceBranch=refs/tags/@confirmit/react-inbox@2.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [1.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.38&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.39&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.37&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.38&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.36&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.37&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.33&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.34&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.32&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.33&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.31&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.32&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.28&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.29&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.26&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.27&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-inbox@1.0.24&sourceBranch=refs/tags/@confirmit/react-inbox@1.0.25&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inbox@1.0.21...@confirmit/react-inbox@1.0.22) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inbox@1.0.16...@confirmit/react-inbox@1.0.17) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-inbox

## [1.0.16](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-inbox@1.0.15...@confirmit/react-inbox@1.0.16) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-inbox

## CHANGELOG

### v1.0.13

- Refactoring: Adding `@confirmit/react-dot` to dependencies

### v1.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v0.0.1

- Initial version
