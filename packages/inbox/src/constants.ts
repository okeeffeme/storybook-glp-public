export const DEFAULT_ANIMATION_MS = 300;

export enum BaseClassNames {
  inbox = 'comd-inbox',
  messageGroup = 'comd-inbox-message-group',
  messageContent = 'comd-inbox-message-content',
}
