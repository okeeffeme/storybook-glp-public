import {createContext} from 'react';
import {initialState, State} from './state/reducer';
import type {Direction, MessageId, View} from './utils';

interface InboxDispatchState {
  onSelect: (id: MessageId, path: number[]) => void;
  onNavigate: (
    id: MessageId | undefined,
    direction: Direction,
    path: number[]
  ) => void;
  onDelete: (id: MessageId, path: number[]) => void;
  onDeleteGroup: (ids: MessageId[]) => void;
  onReturn: (path: number[]) => void;
  onSetListTransition: (view: View, path: number[]) => void;
  onSetContentTransition: (direction: Direction, path: number[]) => void;
  onSetGroupDeleteTransition: (index: number) => void;
}

export const InboxStateContext = createContext<State>(initialState);
export const InboxDispatchContext = createContext<InboxDispatchState>({
  onSetListTransition: () => null,
  onSelect: () => null,
  onNavigate: () => null,
  onReturn: () => null,
  onDeleteGroup: () => null,
  onSetGroupDeleteTransition: () => null,
  onSetContentTransition: () => null,
  onDelete: () => null,
});
