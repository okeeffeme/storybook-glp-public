import React, {FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';
import type {OptionMenuItem} from '../utils';

import {Dropdown} from '@jotunheim/react-dropdown';
import {IconButton} from '@jotunheim/react-button';
import Icon, {dotsVertical} from '@jotunheim/react-icons';

import classNames from './css/inbox.module.css';

interface Options extends OptionMenuItem {
  iconPath?: string;
}

interface OptionsMenuProps {
  options?: Options[];
  onSelect?: (command: string) => void;
}

const OptionsMenu: FC<OptionsMenuProps> = ({options = [], onSelect}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.inbox,
    classNames,
  });

  return (
    <Dropdown
      placement="bottom-end"
      menu={
        <Dropdown.Menu>
          {options.map((item) => {
            return (
              <Dropdown.MenuItem
                onClick={(e) => {
                  e.preventDefault();
                  onSelect && onSelect(item.command);
                }}
                iconPath={item.iconPath}
                data-react-inbox-menu-item={item.command}
                key={item.command}>
                <div
                  className={element('menu-item')}
                  data-react-inbox-menu-item={item.command}>
                  {item.name}
                </div>
              </Dropdown.MenuItem>
            );
          })}
        </Dropdown.Menu>
      }>
      <IconButton data-react-inbox-options-menu={true}>
        <Icon path={dotsVertical} />
      </IconButton>
    </Dropdown>
  );
};

export default OptionsMenu;
