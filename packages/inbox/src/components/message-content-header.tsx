import React, {useContext, FC, ReactNode} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';
import {InboxDispatchContext, InboxStateContext} from '../context';
import {getSelectedMessagePath} from '../state/selectors';

import Icon, {chevronLeft, close} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

import classNames from './css/inbox.module.css';

interface MessageContentHeaderProps {
  returnText: ReactNode;
  onClose?: () => void;
}

export const MessageContentHeader: FC<MessageContentHeaderProps> = ({
  returnText,
  onClose,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.messageContent,
    classNames,
  });

  const state = useContext(InboxStateContext);
  const {onReturn} = useContext(InboxDispatchContext);

  const selectedMessagePath = getSelectedMessagePath(state);

  return (
    <div className={element('header')}>
      <IconButton
        onClick={() => onReturn(selectedMessagePath)}
        data-react-inbox-return-button={true}>
        <Icon path={chevronLeft} />
      </IconButton>
      <div className={element('header-title')}>{returnText}</div>
      {onClose && (
        <IconButton
          onClick={onClose}
          data-testid="inbox-message-close-button"
          data-react-inbox-message-close-button={true}>
          <Icon path={close} />
        </IconButton>
      )}
    </div>
  );
};

export default MessageContentHeader;
