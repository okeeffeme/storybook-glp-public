import React, {useReducer, useMemo, FC, ReactNode} from 'react';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';
import type {
  Direction,
  IMessageGroup,
  OptionMenuItem,
  View,
  MessageId,
} from '../utils';
import {InboxStateContext, InboxDispatchContext} from '../context';
import reducer, {initialState} from '../state/reducer';
import {
  setListTransition,
  setGroupDeleteTransition,
  setContentTransition,
} from '../state/action-creators';

import ListView from './list-view';
import MessageView from './message-view';

import classNames from './css/inbox.module.css';

interface InboxProps {
  messageGroups?: IMessageGroup[];
  onDelete?: (ids: MessageId[]) => void;
  onSelect?: (id: MessageId) => void;
  onLoadMore?: () => void;
  onClose?: () => void;
  title?: ReactNode;
  messagesPlaceholderText?: ReactNode;
  loadButtonText?: ReactNode;
  returnText?: ReactNode;
  previousText?: ReactNode;
  nextText?: ReactNode;
  optionsMenuItems?: OptionMenuItem[];
  onOptionSelect?: (command: string) => void;
  hasMore?: boolean;
  isLoading?: boolean;
  isMoreLoading?: boolean;
}

export const Inbox: FC<InboxProps> = ({
  messageGroups = [],
  onDelete,
  onSelect,
  onLoadMore,
  onClose,
  title,
  loadButtonText = 'See More',
  returnText = 'Back to Messages',
  previousText = 'Previous',
  nextText = 'Next',
  messagesPlaceholderText = 'You have no messages',
  optionsMenuItems = [],
  onOptionSelect,
  hasMore,
  isLoading,
  isMoreLoading,
  ...rest
}) => {
  const {block} = bemFactory({baseClassName: BaseClassNames.inbox, classNames});

  const [state, dispatch] = useReducer(reducer, initialState);

  const handlers = useMemo(() => {
    return {
      onSelect: (id: MessageId, path: number[]) => {
        dispatch(setListTransition('message', path));
        onSelect?.(id);
      },
      onNavigate: (
        id: MessageId | undefined,
        direction: Direction,
        path: number[]
      ) => {
        dispatch(setContentTransition(direction, path));
        id && onSelect?.(id);
      },
      onDelete: (id: MessageId, path: number[]) => {
        dispatch(setListTransition('list', path));
        onDelete?.([id]);
      },
      onDeleteGroup: (ids: MessageId[]) => {
        dispatch(setGroupDeleteTransition(undefined));
        onDelete?.(ids);
      },
      onReturn: (path: number[]) => dispatch(setListTransition('list', path)),
      onSetListTransition: (view: View, path: number[]) =>
        dispatch(setListTransition(view, path)),
      onSetContentTransition: (direction: Direction, path: number[]) =>
        dispatch(setContentTransition(direction, path)),
      onSetGroupDeleteTransition: (index: number) =>
        dispatch(setGroupDeleteTransition(index)),
    };
  }, [onSelect, onDelete]);

  return (
    <InboxDispatchContext.Provider value={handlers}>
      <InboxStateContext.Provider value={state}>
        <div
          className={block()}
          data-testid="inbox"
          {...extractDataAriaIdProps(rest)}>
          <ListView
            title={title}
            onClose={onClose}
            optionsMenuItems={optionsMenuItems}
            onOptionSelect={onOptionSelect}
            hasMore={hasMore}
            isLoading={isLoading}
            isMoreLoading={isMoreLoading}
            onLoadMore={onLoadMore}
            loadButtonText={loadButtonText}
            messagesPlaceholderText={messagesPlaceholderText}
            messageGroups={messageGroups}
          />

          <MessageView
            onClose={onClose}
            returnText={returnText}
            previousText={previousText}
            nextText={nextText}
            messageGroups={messageGroups}
          />
        </div>
      </InboxStateContext.Provider>
    </InboxDispatchContext.Provider>
  );
};

export default Inbox;
