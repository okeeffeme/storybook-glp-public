import React, {ReactNode, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';

import Icon, {notifications} from '@jotunheim/react-icons';

import classNames from './css/inbox.module.css';

interface MessagesPlaceholderProps {
  text?: ReactNode;
}

const MessagesPlaceholder: FC<MessagesPlaceholderProps> = ({text}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.inbox,
    classNames,
  });

  return (
    <div
      className={element('messages-placeholder')}
      data-testid="inbox-messages-placeholder">
      <Icon path={notifications} size="42px" />
      <span className={element('messages-placeholder-text')}>{text}</span>
    </div>
  );
};

export default MessagesPlaceholder;
