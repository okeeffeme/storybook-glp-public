import React, {FC, MouseEventHandler, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';
import type {OutputMessage} from '../state/utils';
import type {Direction} from '../utils';

import Icon, {chevronLeft, chevronRight} from '@jotunheim/react-icons';
import {Button} from '@jotunheim/react-button';

import classNames from './css/inbox.module.css';

interface MessageContentFooterProps {
  onNavigate?: (direction: Direction) => void;
  previous?: OutputMessage | null;
  next?: OutputMessage | null;
  previousText?: ReactNode;
  nextText?: ReactNode;
}

export const MessageContentFooter: FC<MessageContentFooterProps> = ({
  previous,
  next,
  onNavigate,
  previousText,
  nextText,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.messageContent,
    classNames,
  });

  const footerClasses = cn(element('footer'), {
    [element('footer', 'left-only')]: !next && !!previous,
    [element('footer', 'right-only')]: !previous && !!next,
  });

  const handleButtonClick =
    (direction: 'left' | 'right'): MouseEventHandler =>
    (e) => {
      if (!onNavigate) {
        return;
      }

      e.preventDefault();
      e.stopPropagation();
      onNavigate(direction);
    };

  return (
    <div className={footerClasses}>
      {previous && (
        <Button
          prefix={<Icon path={chevronLeft} />}
          onClick={handleButtonClick('left')}
          appearance={Button.appearances.secondaryNeutral}
          data-testid="inbox-previous-navigation-button"
          data-react-inbox-previous-navigation-button={true}>
          {previousText}
        </Button>
      )}
      {next && (
        <Button
          suffix={<Icon path={chevronRight} />}
          onClick={handleButtonClick('right')}
          appearance={Button.appearances.secondaryNeutral}
          data-testid="inbox-next-navigation-button">
          {nextText}
        </Button>
      )}
    </div>
  );
};

export default MessageContentFooter;
