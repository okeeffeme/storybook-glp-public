import React, {useContext, FC, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import Icon, {trashBin} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

import {InboxDispatchContext, InboxStateContext} from '../context';
import {getSelectedMessagePath} from '../state/selectors';
import {BaseClassNames} from '../constants';
import type {MessageId} from '../utils';

import classNames from './css/inbox.module.css';

interface MessageContentBodyProps {
  className?: string;
  id?: MessageId;
  title?: ReactNode;
  text?: ReactNode;
}

export const MessageContentBody: FC<MessageContentBodyProps> = ({
  className,
  id,
  title,
  text,
  ...rest
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.messageContent,
    classNames,
  });

  const state = useContext(InboxStateContext);
  const {onDelete} = useContext(InboxDispatchContext);

  const selectedMessagePath = getSelectedMessagePath(state);

  return (
    <div
      className={cn(element('message'), className)}
      {...extractDataAriaIdProps(rest)}>
      <div className={element('message-header')}>
        {title && (
          <div
            className={element('message-title')}
            data-react-inbox-message-title={true}>
            {title}
          </div>
        )}
        <IconButton
          onClick={() => id && onDelete(id, selectedMessagePath)}
          data-testid="inbox-delete-message-button"
          data-react-inbox-delete-message-button={true}>
          <Icon path={trashBin} />
        </IconButton>
      </div>
      <div className={element('body')} data-react-inbox-message-text={true}>
        {text}
      </div>
    </div>
  );
};

export default MessageContentBody;
