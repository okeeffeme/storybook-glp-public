import React, {useContext, useCallback, ReactNode, FC} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {InboxDispatchContext, InboxStateContext} from '../context';
import {
  getSelectedMessagePath,
  getContentTransitionDirection,
  getTransitioningMessagePath,
} from '../state/selectors';
import {BaseClassNames} from '../constants';
import type {Direction, IMessage, IMessageGroup} from '../utils';

import {findMessageNodes, OutputMessage} from '../state/utils';
import useTransitionTimeoutCbHook from './use-transition-timeout-cb-hook';
import MessageContentHeader from './message-content-header';
import MessageContentBody from './message-content-body';
import MessageContentFooter from './message-content-footer';

import classNames from './css/inbox.module.css';

interface MessageViewProps {
  message?: IMessage;
  previous?: OutputMessage;
  next?: OutputMessage;
  messageGroups?: IMessageGroup[];
  onClose?: () => void;
  returnText?: ReactNode;
  previousText?: ReactNode;
  nextText?: ReactNode;
}

export const MessageView: FC<MessageViewProps> = ({
  messageGroups = [],
  onClose,
  returnText,
  previousText,
  nextText,
}) => {
  const {block, element} = bemFactory({
    baseClassName: BaseClassNames.messageContent,
    classNames,
  });

  const state = useContext(InboxStateContext);
  const {onNavigate, onSetContentTransition} = useContext(InboxDispatchContext);

  const selectedMessagePath = getSelectedMessagePath(state);
  const contentTransitionDirection = getContentTransitionDirection(state);
  const transitioningMessagePath = getTransitioningMessagePath(state);

  const shouldStart = () => !!contentTransitionDirection;
  useTransitionTimeoutCbHook(shouldStart, onSetContentTransition, [
    contentTransitionDirection,
    onSetContentTransition,
  ]);

  const {previousMessage, nextMessage, message} = findMessageNodes({
    state,
    messageGroups,
  });

  const handleNavigate = useCallback(
    (direction: Direction) => {
      const message = direction === 'left' ? previousMessage : nextMessage;
      onNavigate(message?.id, direction, message?.path ?? []);
    },
    [onNavigate, previousMessage, nextMessage]
  );

  const messageTransitionClassname = cn({
    [element('message-transition', 'enter')]:
      selectedMessagePath.length && !transitioningMessagePath.length,
    [element('message-transition', 'enter-active')]:
      selectedMessagePath.length && !!transitioningMessagePath.length,
    [element('message-transition', 'exit-active')]:
      !selectedMessagePath.length && !!transitioningMessagePath.length,
    [element('message-transition', 'exit')]:
      !selectedMessagePath.length && !transitioningMessagePath.length,
  });

  const overlapMessageTransitionClassname = cn({
    [element('overlap-transition')]: !contentTransitionDirection,
    [element('left-transition', 'enter-active')]:
      contentTransitionDirection === 'left',
    [element('right-transition', 'enter-active')]:
      contentTransitionDirection === 'right',
  });

  const overlappingMessage =
    contentTransitionDirection === 'left'
      ? nextMessage
      : contentTransitionDirection === 'right'
      ? previousMessage
      : message;

  return (
    <div
      className={cn(block(), messageTransitionClassname)}
      data-testid="inbox-message-view-active"
      data-react-inbox-message-view-active={!!selectedMessagePath.length}>
      {(!!selectedMessagePath.length || !!transitioningMessagePath.length) && (
        <>
          <MessageContentHeader onClose={onClose} returnText={returnText} />
          <div className={element('message-container')}>
            {message && (
              <MessageContentBody
                id={message.id}
                title={message.title}
                text={message.text}
              />
            )}
            {overlappingMessage && (
              <MessageContentBody
                className={overlapMessageTransitionClassname}
                id={overlappingMessage.id}
                title={overlappingMessage.title}
                text={overlappingMessage.text}
                data-react-inbox-active-message={true}
              />
            )}
          </div>
          <MessageContentFooter
            onNavigate={handleNavigate}
            previous={previousMessage}
            next={nextMessage}
            previousText={previousText}
            nextText={nextText}
          />
        </>
      )}
    </div>
  );
};

export default MessageView;
