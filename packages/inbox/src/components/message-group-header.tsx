import React, {FC, ReactNode} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';

import Icon, {trashBin} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

import classNames from './css/inbox.module.css';

interface MessageGroupHeaderProps {
  title?: ReactNode;
  onDelete?: () => void;
}

export const MessageGroupHeader: FC<MessageGroupHeaderProps> = ({
  title,
  onDelete,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.messageGroup,
    classNames,
  });

  return (
    <div className={element(`header`)}>
      <div
        className={element(`title`)}
        data-react-inbox-message-group-title={true}
        data-testid="message-group-title">
        {title}
      </div>
      <div className={element('action-buttons')}>
        {onDelete && (
          <IconButton
            onClick={onDelete}
            data-react-inbox-delete-message-group-button={true}>
            <Icon path={trashBin} />
          </IconButton>
        )}
      </div>
    </div>
  );
};

export default MessageGroupHeader;
