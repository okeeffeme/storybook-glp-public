import {useEffect, useRef} from 'react';
import {DEFAULT_ANIMATION_MS} from '../constants';

const useTransitionTimeoutCbHook = <T,>(
  shouldStart: () => boolean,
  cb: (...args) => void,
  [deps]: Array<T>
) => {
  const timeout = useRef<NodeJS.Timeout | number>(0);

  useEffect(() => {
    if (shouldStart()) {
      clearTimeout(timeout.current as NodeJS.Timeout);
      timeout.current = setTimeout(() => {
        cb();
      }, DEFAULT_ANIMATION_MS);
    }

    return () => clearTimeout(timeout.current as NodeJS.Timeout);
  }, [shouldStart, deps, cb]);
};

export default useTransitionTimeoutCbHook;
