import React, {FC, ReactNode, useContext} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {InboxStateContext, InboxDispatchContext} from '../context';
import {
  getTransitioningMessagePath,
  getSelectedMessagePath,
  getGroupDeleteTransitionIndex,
} from '../state/selectors';
import useTransitionTimeoutCbHook from './use-transition-timeout-cb-hook';
import {BaseClassNames} from '../constants';
import type {IMessageGroup, OptionMenuItem} from '../utils';

import {BusyDots} from '@jotunheim/react-busy-dots';
import {WaitButton} from '@jotunheim/react-wait-button';
import Header from './header';
import MessageGroup from './message-group';
import MessagesPlaceholder from './messages-placeholder';

import classNames from './css/inbox.module.css';

interface ListViewProps {
  optionsMenuItems: OptionMenuItem[];
  messageGroups?: IMessageGroup[];
  onLoadMore?: () => void;
  onClose?: () => void;
  title?: ReactNode;
  messagesPlaceholderText?: ReactNode;
  loadButtonText?: ReactNode;
  onOptionSelect?: (command: string) => void;
  hasMore?: boolean;
  isLoading?: boolean;
  isMoreLoading?: boolean;
}

const ListView: FC<ListViewProps> = ({
  title,
  messageGroups = [],
  onClose,
  optionsMenuItems,
  onOptionSelect,
  messagesPlaceholderText,
  hasMore,
  isLoading,
  isMoreLoading,
  onLoadMore,
  loadButtonText,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.inbox,
    classNames,
  });

  const state = useContext(InboxStateContext);
  const {onSetListTransition} = useContext(InboxDispatchContext);

  const transitioningMessagePath = getTransitioningMessagePath(state);
  const selectedMessagePath = getSelectedMessagePath(state);
  const groupDeleteTransitionIndex = getGroupDeleteTransitionIndex(state);

  const shouldStart = () =>
    !!transitioningMessagePath.length && !selectedMessagePath.length;
  useTransitionTimeoutCbHook(shouldStart, onSetListTransition, [
    transitioningMessagePath,
    onSetListTransition,
  ]);

  const listTransitionClassname = cn({
    [element('list-transition', 'enter')]:
      !selectedMessagePath.length && !transitioningMessagePath.length,
    [element('list-transition', 'enter-active')]:
      !selectedMessagePath.length && !!transitioningMessagePath.length,
    [element('list-transition', 'exit-active')]:
      selectedMessagePath.length && !!transitioningMessagePath.length,
    [element('list-transition', 'exit')]:
      selectedMessagePath.length && !transitioningMessagePath.length,
  });

  const isMessagesEmpty =
    !isLoading && !isMoreLoading && !hasMore && !messageGroups.length;

  return (
    <div
      className={cn(element('container'), listTransitionClassname)}
      data-testid="inbox-list-view-active"
      data-react-inbox-list-view-active={!selectedMessagePath.length}>
      <Header
        title={title}
        onClose={onClose}
        optionsMenuItems={optionsMenuItems}
        onOptionSelect={onOptionSelect}
      />
      <div className={element('scroll-container')}>
        {isMessagesEmpty ? (
          <MessagesPlaceholder text={messagesPlaceholderText} />
        ) : (
          <>
            {messageGroups.map((group, i) => (
              <MessageGroup
                key={i}
                index={i}
                title={group.title}
                messages={group.messages}
                groupDeleteTransitionIndex={groupDeleteTransitionIndex}
              />
            ))}
            {isLoading && !isMoreLoading && (
              <div className={element('loading-container')}>
                <BusyDots />
              </div>
            )}
            {hasMore && (
              <div
                className={element('wait-button')}
                data-react-inbox-see-more-button={true}>
                <WaitButton
                  onClick={onLoadMore}
                  busy={isMoreLoading}
                  appearance={WaitButton.appearances.secondaryNeutral}>
                  {loadButtonText}
                </WaitButton>
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default ListView;
