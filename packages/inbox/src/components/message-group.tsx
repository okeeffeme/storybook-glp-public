import React, {useContext, useCallback, ReactNode, FC} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import type {IMessage} from '../utils';
import {BaseClassNames} from '../constants';
import {InboxDispatchContext} from '../context';
import useTransitionTimeoutCbHook from './use-transition-timeout-cb-hook';

import MessageGroupHeader from './message-group-header';
import Message from './message';

import classNames from './css/inbox.module.css';

interface MessageGroupProps {
  index: number;
  title: ReactNode;
  messages: IMessage[];
  className?: string;
  groupDeleteTransitionIndex?: number;
}

export const MessageGroup: FC<MessageGroupProps> = ({
  className,
  title,
  index,
  messages,
  groupDeleteTransitionIndex,
}) => {
  const {block, element} = bemFactory({
    baseClassName: BaseClassNames.messageGroup,
    classNames,
  });

  const {onDeleteGroup, onSetGroupDeleteTransition} =
    useContext(InboxDispatchContext);

  const handleTransitionTimeout = useCallback(() => {
    const messageIds = messages.map((message) => message.id as number);
    onDeleteGroup(messageIds);
  }, [onDeleteGroup, messages]);

  const shouldStart = () => index === groupDeleteTransitionIndex;
  useTransitionTimeoutCbHook(shouldStart, handleTransitionTimeout, [
    groupDeleteTransitionIndex,
    handleTransitionTimeout,
  ]);

  const handleDelete = useCallback(
    () => onSetGroupDeleteTransition(index),
    [index, onSetGroupDeleteTransition]
  );

  const groupTransitionClassname = cn({
    [element('group-transition')]: true,
    [element('group-transition', 'enter-active')]:
      groupDeleteTransitionIndex === index,
  });

  return (
    <ul className={cn(block(), className)} data-testid="inbox-message-group">
      <li className={groupTransitionClassname}>
        <MessageGroupHeader title={title} onDelete={handleDelete} />
        <ul>
          {messages.map((message, messageIndex) => (
            <Message
              key={messageIndex}
              message={message}
              parentIndex={index}
              messageIndex={messageIndex}
            />
          ))}
        </ul>
      </li>
    </ul>
  );
};

export default React.memo(MessageGroup);
