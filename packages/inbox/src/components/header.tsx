import React, {ReactNode, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {BaseClassNames} from '../constants';
import type {OptionMenuItem} from '../utils';

import Icon, {close, bellCircle} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';
import OptionsMenu from './options-menu';

import classNames from './css/inbox.module.css';

interface HeaderProps {
  optionsMenuItems: OptionMenuItem[];
  title?: ReactNode;
  onClose?: () => void;
  onOptionSelect?: (command: string) => void;
}

export const Header: FC<HeaderProps> = ({
  title,
  onClose,
  optionsMenuItems,
  onOptionSelect,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.inbox,
    classNames,
  });

  return (
    <div className={element('header')}>
      <Icon size="48px" path={bellCircle} />
      <div className={element('header-title')}>{title}</div>
      <div className={element('header-buttons')}>
        {onOptionSelect && optionsMenuItems && (
          <OptionsMenu onSelect={onOptionSelect} options={optionsMenuItems} />
        )}
        {onClose && (
          <IconButton
            onClick={onClose}
            data-testid="inbox-list-close-button"
            data-react-inbox-list-close-button={true}>
            <Icon path={close} />
          </IconButton>
        )}
      </div>
    </div>
  );
};

export default Header;
