import React, {FC, useContext} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {InboxDispatchContext} from '../context';
import {BaseClassNames} from '../constants';
import type {IMessage} from '../utils';

import Dot from '@jotunheim/react-dot';
import Icon, {chevronRight} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';
import {Truncate} from '@jotunheim/react-truncate';

import classNames from './css/inbox.module.css';

interface MessageProps {
  message: IMessage;
  parentIndex: number;
  messageIndex: number;
}

export const Message: FC<MessageProps> = ({
  message: {id, text, isRead},
  parentIndex,
  messageIndex,
}) => {
  const {element} = bemFactory({
    baseClassName: BaseClassNames.messageGroup,
    classNames,
  });

  const {onSelect} = useContext(InboxDispatchContext);

  const handleClick = () => {
    id && onSelect(id, [parentIndex, messageIndex]);
  };

  return (
    <li
      className={cn(element('preview'), {
        [element('preview', 'unread')]: !isRead,
      })}
      onClick={handleClick}
      data-testid="inbox-message"
      data-react-inbox-message-id={id}
      data-react-inbox-message={true}>
      {!isRead && <Dot data-react-inbox-unread-badge={true} />}
      <Truncate data-testid="preview-text">{text}</Truncate>
      <IconButton data-testid="more-button" onClick={handleClick}>
        <Icon path={chevronRight} />
      </IconButton>
    </li>
  );
};

export default React.memo(Message);
