import type {ReactNode} from 'react';

export type MessageId = string | number;

export interface IMessage {
  id?: MessageId;
  title?: ReactNode;
  text?: ReactNode;
  isRead?: boolean;
}

export interface IMessageGroup {
  title: ReactNode;
  messages: IMessage[];
}

export interface OptionMenuItem {
  command: string;
  name: string;
  icon?: () => void;
}

export type View = 'message' | 'list';

export type Direction = 'left' | 'right';
