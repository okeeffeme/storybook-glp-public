import * as selectors from './selectors';
import type {State} from './reducer';
import type {IMessage, IMessageGroup} from '../utils';

interface FindMessageNodes {
  state: State;
  messageGroups: IMessageGroup[];
}

export interface OutputMessage extends IMessage {
  path?: number[];
}

interface Output {
  previousMessage: OutputMessage | null;
  nextMessage: OutputMessage | null;
  message: IMessage;
}

export const findMessageNodes = ({state, messageGroups}: FindMessageNodes) => {
  const transitioningMessagePath = selectors.getTransitioningMessagePath(state);
  const selectedMessagePath = selectors.getSelectedMessagePath(state);

  const path = transitioningMessagePath.length
    ? transitioningMessagePath
    : selectedMessagePath;

  const output: Output = {
    previousMessage: null,
    nextMessage: null,
    message: {},
  };

  if (!path.length) return output;

  const [messageGroupIndex, messageIndex] = path;

  const currentGroup = messageGroups[messageGroupIndex]
    ? messageGroups[messageGroupIndex].messages
    : [];

  output.message = currentGroup[messageIndex];

  // message group before exists and is first message in group
  if (messageGroupIndex !== 0 && messageIndex === 0) {
    const previousGroup = messageGroups[messageGroupIndex - 1].messages;
    output.previousMessage = {
      ...previousGroup[previousGroup.length - 1],
      path: [messageGroupIndex - 1, previousGroup.length - 1],
    };

    // message before exists in group
  } else if (messageIndex !== 0) {
    output.previousMessage = {
      ...currentGroup[messageIndex - 1],
      path: [messageGroupIndex, messageIndex - 1],
    };
  }

  // message group after exists and is last message in group
  if (
    messageGroupIndex !== messageGroups.length - 1 &&
    messageIndex === currentGroup.length - 1
  ) {
    const nextGroup = messageGroups[messageGroupIndex + 1].messages;
    output.nextMessage = {
      ...nextGroup[0],
      path: [messageGroupIndex + 1, 0],
    };
    // message after exists in group
  } else if (messageIndex !== currentGroup.length - 1) {
    output.nextMessage = {
      ...currentGroup[messageIndex + 1],
      path: [messageGroupIndex, messageIndex + 1],
    };
  }

  return output;
};
