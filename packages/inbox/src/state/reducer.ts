import {ActionType} from './action-types';
import type {
  SetGroupDeleteTransitionAction,
  SetContentTransitionAction,
  SetListTransitionAction,
} from './action-creators';

export type State = {
  selectedMessagePath: number[];
  transitioningMessagePath: number[];
  groupDeleteTransitionIndex: number | undefined;
  contentTransitionDirection: 'left' | 'right' | undefined;
};

export const initialState: State = {
  selectedMessagePath: [],
  transitioningMessagePath: [],
  groupDeleteTransitionIndex: undefined,
  contentTransitionDirection: undefined,
};

const reducer = (
  state: State,
  action:
    | SetContentTransitionAction
    | SetGroupDeleteTransitionAction
    | SetListTransitionAction
) => {
  switch (action.type) {
    case ActionType.SetListTransition: {
      const {view, path} = action.payload;

      if (view === 'message') {
        return {
          ...state,
          transitioningMessagePath: path,
          selectedMessagePath: path,
        };
      } else if (view === 'list') {
        return {
          ...state,
          transitioningMessagePath: path,
          selectedMessagePath: [],
        };
      }
      return {
        ...state,
        transitioningMessagePath: [],
      };
    }

    case ActionType.SetContentTransition: {
      const {direction, path} = action.payload;
      if (direction) {
        return {
          ...state,
          contentTransitionDirection: direction,
          transitioningMessagePath: path,
          selectedMessagePath: path,
        };
      }
      return {
        ...state,
        contentTransitionDirection: undefined,
        transitioningMessagePath: [],
      };
    }

    case ActionType.SetGroupDeleteTransition:
      return {
        ...state,
        groupDeleteTransitionIndex: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
