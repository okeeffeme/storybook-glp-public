import type {State} from './reducer';

export const getSelectedMessagePath = (state: State) =>
  state.selectedMessagePath;
export const getTransitioningMessagePath = (state: State) =>
  state.transitioningMessagePath;
export const getGroupDeleteTransitionIndex = (state: State) =>
  state.groupDeleteTransitionIndex;
export const getContentTransitionDirection = (state: State) =>
  state.contentTransitionDirection;
