export enum ActionType {
  SetListTransition = 'ri:setListTransition',
  SetGroupDeleteTransition = 'ri:setGroupDeleteTransition',
  SetContentTransition = 'ri:setContentTransition',
}
