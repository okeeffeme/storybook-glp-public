import {ActionType} from './action-types';
import type {Direction, View} from '../utils';

export interface SetListTransitionAction {
  type: ActionType.SetListTransition;
  payload: {view: View; path: number[]};
}

export interface SetGroupDeleteTransitionAction {
  type: ActionType.SetGroupDeleteTransition;
  payload: number | undefined;
}

export interface SetContentTransitionAction {
  type: ActionType.SetContentTransition;
  payload: {direction: Direction; path: number[]};
}

export const setListTransition = (
  view: View,
  path: number[] = []
): SetListTransitionAction => ({
  type: ActionType.SetListTransition,
  payload: {
    view,
    path,
  },
});

export const setGroupDeleteTransition = (
  index: number | undefined
): SetGroupDeleteTransitionAction => ({
  type: ActionType.SetGroupDeleteTransition,
  payload: index,
});

export const setContentTransition = (
  direction: Direction,
  path: number[] = []
): SetContentTransitionAction => ({
  type: ActionType.SetContentTransition,
  payload: {
    direction,
    path,
  },
});
