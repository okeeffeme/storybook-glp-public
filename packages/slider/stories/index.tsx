import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {boolean, number, text} from '@storybook/addon-knobs';

import Slider from '../src/components/slider';

storiesOf('Components/slider', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('default', () => {
    const [value, setValue] = React.useState(30);

    return (
      <Slider
        label={text('label', 'Slider')}
        value={value}
        min={number('min', 0)}
        max={number('max', 100)}
        disabled={boolean('disabled', false)}
        showInputField={boolean('show input field', false)}
        onChange={(value) => setValue(value)}
        stepSize={number('stepSize', 1)}
        showMarkers={boolean('showMarkers', true)}
        maxDecimals={number('max decimals', 2)}
      />
    );
  });
