# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.12&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.13&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.12&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.12&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.10&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.11&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.9&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.10&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.8&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.9&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.7&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.8&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.6&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.7&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.5&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.6&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [9.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.4&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.5&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.3&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.4&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.2&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.3&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.1&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.2&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.1.0&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.1&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-slider

# [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.21&sourceBranch=refs/tags/@jotunheim/react-slider@9.1.0&targetRepoId=1246) (2023-02-06)

### Features

- Convert slider package to TypeScript ([NPM-1241](https://jiraosl.firmglobal.com/browse/NPM-1241)) ([78773cf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/78773cfec179b083fffc7d60c35b282241227503))

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.20&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.21&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.19&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.20&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.18&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.19&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.17&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.18&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.15&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.15&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.14&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.15&targetRepoId=1246) (2023-01-11)

### Bug Fixes

- adding data-testid ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([7d9b113](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7d9b113c661fd9cb0f5b6665ffd902716635fdce))

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.13&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.14&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.12&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.11&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.10&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.9&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.8&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.7&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.4&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.4&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.4&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.3&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.2&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-slider

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@9.0.0&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-slider

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.17&sourceBranch=refs/tags/@jotunheim/react-slider@9.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Slider ([NPM-952](https://jiraosl.firmglobal.com/browse/NPM-952)) ([7e801f9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7e801f99954b9c14d82c6c9f177b73fabc2cfad4))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.16&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.15&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.14&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.13&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.12&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.11&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.10&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.9&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.7&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.4&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.3&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.2&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.1&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-slider

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-slider@8.0.0&sourceBranch=refs/tags/@jotunheim/react-slider@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-slider

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.57&sourceBranch=refs/tags/@confirmit/react-slider@7.0.58&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.56&sourceBranch=refs/tags/@confirmit/react-slider@7.0.57&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.55&sourceBranch=refs/tags/@confirmit/react-slider@7.0.56&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.54&sourceBranch=refs/tags/@confirmit/react-slider@7.0.55&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.53&sourceBranch=refs/tags/@confirmit/react-slider@7.0.54&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.52&sourceBranch=refs/tags/@confirmit/react-slider@7.0.53&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.50&sourceBranch=refs/tags/@confirmit/react-slider@7.0.51&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.49&sourceBranch=refs/tags/@confirmit/react-slider@7.0.50&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.48&sourceBranch=refs/tags/@confirmit/react-slider@7.0.49&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.47&sourceBranch=refs/tags/@confirmit/react-slider@7.0.48&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.46&sourceBranch=refs/tags/@confirmit/react-slider@7.0.47&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.45&sourceBranch=refs/tags/@confirmit/react-slider@7.0.46&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.42&sourceBranch=refs/tags/@confirmit/react-slider@7.0.43&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.41&sourceBranch=refs/tags/@confirmit/react-slider@7.0.42&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.40&sourceBranch=refs/tags/@confirmit/react-slider@7.0.41&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.39&sourceBranch=refs/tags/@confirmit/react-slider@7.0.40&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.38&sourceBranch=refs/tags/@confirmit/react-slider@7.0.39&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.37&sourceBranch=refs/tags/@confirmit/react-slider@7.0.38&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.36&sourceBranch=refs/tags/@confirmit/react-slider@7.0.37&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.35&sourceBranch=refs/tags/@confirmit/react-slider@7.0.36&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.34&sourceBranch=refs/tags/@confirmit/react-slider@7.0.35&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.32&sourceBranch=refs/tags/@confirmit/react-slider@7.0.33&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.31&sourceBranch=refs/tags/@confirmit/react-slider@7.0.32&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.30&sourceBranch=refs/tags/@confirmit/react-slider@7.0.31&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.29&sourceBranch=refs/tags/@confirmit/react-slider@7.0.30&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.28&sourceBranch=refs/tags/@confirmit/react-slider@7.0.29&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.27&sourceBranch=refs/tags/@confirmit/react-slider@7.0.28&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.26&sourceBranch=refs/tags/@confirmit/react-slider@7.0.27&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.25&sourceBranch=refs/tags/@confirmit/react-slider@7.0.26&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.24&sourceBranch=refs/tags/@confirmit/react-slider@7.0.25&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.23&sourceBranch=refs/tags/@confirmit/react-slider@7.0.24&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.22&sourceBranch=refs/tags/@confirmit/react-slider@7.0.23&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.21&sourceBranch=refs/tags/@confirmit/react-slider@7.0.22&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.20&sourceBranch=refs/tags/@confirmit/react-slider@7.0.21&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.19&sourceBranch=refs/tags/@confirmit/react-slider@7.0.20&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.18&sourceBranch=refs/tags/@confirmit/react-slider@7.0.19&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.17&sourceBranch=refs/tags/@confirmit/react-slider@7.0.18&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.16&sourceBranch=refs/tags/@confirmit/react-slider@7.0.17&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.15&sourceBranch=refs/tags/@confirmit/react-slider@7.0.16&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.14&sourceBranch=refs/tags/@confirmit/react-slider@7.0.15&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.13&sourceBranch=refs/tags/@confirmit/react-slider@7.0.14&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.12&sourceBranch=refs/tags/@confirmit/react-slider@7.0.13&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.11&sourceBranch=refs/tags/@confirmit/react-slider@7.0.12&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.10&sourceBranch=refs/tags/@confirmit/react-slider@7.0.11&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.9&sourceBranch=refs/tags/@confirmit/react-slider@7.0.10&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.7&sourceBranch=refs/tags/@confirmit/react-slider@7.0.8&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.6&sourceBranch=refs/tags/@confirmit/react-slider@7.0.7&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.5&sourceBranch=refs/tags/@confirmit/react-slider@7.0.6&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.4&sourceBranch=refs/tags/@confirmit/react-slider@7.0.5&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.3&sourceBranch=refs/tags/@confirmit/react-slider@7.0.4&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.2&sourceBranch=refs/tags/@confirmit/react-slider@7.0.3&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.1&sourceBranch=refs/tags/@confirmit/react-slider@7.0.2&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@7.0.0&sourceBranch=refs/tags/@confirmit/react-slider@7.0.1&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-slider

## [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.27&sourceBranch=refs/tags/@confirmit/react-slider@7.0.0&targetRepoId=1246) (2021-03-15)

### Features

- add `stepSize` and `showMarkers` properties instead of `steps` property

### BREAKING CHANGES

- remove `steps` property

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.26&sourceBranch=refs/tags/@confirmit/react-slider@6.0.27&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.25&sourceBranch=refs/tags/@confirmit/react-slider@6.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.24&sourceBranch=refs/tags/@confirmit/react-slider@6.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.22&sourceBranch=refs/tags/@confirmit/react-slider@6.0.23&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.21&sourceBranch=refs/tags/@confirmit/react-slider@6.0.22&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.20&sourceBranch=refs/tags/@confirmit/react-slider@6.0.21&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.19&sourceBranch=refs/tags/@confirmit/react-slider@6.0.20&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.18&sourceBranch=refs/tags/@confirmit/react-slider@6.0.19&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.17&sourceBranch=refs/tags/@confirmit/react-slider@6.0.18&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.16&sourceBranch=refs/tags/@confirmit/react-slider@6.0.17&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.15&sourceBranch=refs/tags/@confirmit/react-slider@6.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.14&sourceBranch=refs/tags/@confirmit/react-slider@6.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.13&sourceBranch=refs/tags/@confirmit/react-slider@6.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.12&sourceBranch=refs/tags/@confirmit/react-slider@6.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.11&sourceBranch=refs/tags/@confirmit/react-slider@6.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.10&sourceBranch=refs/tags/@confirmit/react-slider@6.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.7&sourceBranch=refs/tags/@confirmit/react-slider@6.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.4&sourceBranch=refs/tags/@confirmit/react-slider@6.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.3&sourceBranch=refs/tags/@confirmit/react-slider@6.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.2&sourceBranch=refs/tags/@confirmit/react-slider@6.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-slider

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@6.0.1&sourceBranch=refs/tags/@confirmit/react-slider@6.0.2&targetRepoId=1246) (2020-11-19)

### Bug Fixes

- change width to min-width of selected value. ([NPM-606](https://jiraosl.firmglobal.com/browse/NPM-606)) ([be14f3b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/be14f3b5235f780aa8a4fed12f3631e2caf5e5b4))
- remove width from slider input. add margin-left to selected value. ([NPM-606](https://jiraosl.firmglobal.com/browse/NPM-606)) ([2cf995e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2cf995e2518bf8718290d2144b2613eb11bce281))

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@5.0.1&sourceBranch=refs/tags/@confirmit/react-slider@6.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@5.0.1&sourceBranch=refs/tags/@confirmit/react-slider@6.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@5.0.1&sourceBranch=refs/tags/@confirmit/react-slider@5.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-slider

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.45&sourceBranch=refs/tags/@confirmit/react-slider@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.44&sourceBranch=refs/tags/@confirmit/react-slider@4.0.45&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.43&sourceBranch=refs/tags/@confirmit/react-slider@4.0.44&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.42&sourceBranch=refs/tags/@confirmit/react-slider@4.0.43&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.41&sourceBranch=refs/tags/@confirmit/react-slider@4.0.42&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.38&sourceBranch=refs/tags/@confirmit/react-slider@4.0.39&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.37&sourceBranch=refs/tags/@confirmit/react-slider@4.0.38&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.35&sourceBranch=refs/tags/@confirmit/react-slider@4.0.36&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.32&sourceBranch=refs/tags/@confirmit/react-slider@4.0.33&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.31&sourceBranch=refs/tags/@confirmit/react-slider@4.0.32&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.29&sourceBranch=refs/tags/@confirmit/react-slider@4.0.30&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-slider@4.0.27&sourceBranch=refs/tags/@confirmit/react-slider@4.0.28&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.26](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-slider@4.0.25...@confirmit/react-slider@4.0.26) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-slider@4.0.23...@confirmit/react-slider@4.0.24) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-slider@4.0.17...@confirmit/react-slider@4.0.18) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-slider

## [4.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-slider@4.0.16...@confirmit/react-slider@4.0.17) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-slider

## CHANGELOG

## v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-slider`

### v3.1.0

- Removing package version in class names.

### v3.0.12

- fix: removed slider fixed width
- fix: input field width

### v3.0.8

- fix: slider labels alignment

### v3.0.2

- fix: slider rail opacity from percentage to number

### v3.0.0

- **BREAKING**:

  - Major UI changes to @confirmit/react-text-field

### v2.0.0

- **BREAKING**:
  - Removed SliderInput export component
  - Removed defaultValue, handlerClassName, trackClassName and railClassName props
  - Feat:
    - Added label, disabled, steps, showInputField, maxDecimals props

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.4.0

- Remove hideLabel from being passed in to TextField prop as it's no longer needed

### v0.3.0

- **BREAKING**
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v0.2.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v0.0.1

- Feat: Slider and SliderInput components
