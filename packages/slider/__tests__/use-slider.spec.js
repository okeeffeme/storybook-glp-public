import {
  getClosestStep,
  getIntervals,
  getNextStep,
  getPercent,
  useSlider,
} from '../src/components/use-slider';
import {renderHook, act} from '@testing-library/react-hooks';

describe('Confirmit-slider/use-slider ::', () => {
  const steps = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

  describe('useSlider', () => {
    it('can handle float values', async () => {
      const onChange = jest.fn();

      const {result} = renderHook(() =>
        useSlider({
          min: 0,
          max: 100,
          value: 30,
          stepSize: 0.01,
          disabled: false,
          onChange: onChange,
          maxDecimals: 2,
        })
      );

      expect(result.current.percent).toEqual(0.3);

      // Mocking refs
      result.current.trackRef.current = {
        getBoundingClientRect: () => ({left: 0}),
        clientWidth: 100,
      };
      result.current.handlerRef.current = {
        focus: () => void 0,
      };

      act(() => {
        result.current.handleMouseDown({
          preventDefault: () => void 0,
          clientX: 40.55,
        });
      });

      expect(onChange).toHaveBeenCalledWith(40.55);
    });
  });

  describe('getClosestStep', () => {
    it('should get the closest step', () => {
      let target = 54;
      expect(getClosestStep(steps, target)).toBe(50);

      target = 55;
      expect(getClosestStep(steps, target)).toBe(50);

      target = 56;
      expect(getClosestStep(steps, target)).toBe(60);
    });
  });

  describe('getPercent', () => {
    it('should get percent', () => {
      const min = 0;
      const max = 100;

      expect(getPercent(40, min, max)).toBe(0.4);
      expect(getPercent(-10, min, max)).toBe(-0.1);
      expect(getPercent(110, min, max)).toBe(1.1);
    });
  });

  describe('getNextStep', () => {
    it('should get next step', () => {
      expect(getNextStep(steps, 30, 1)).toBe(40);
      expect(getNextStep(steps, 0, -1)).toBe(0);
      expect(getNextStep(steps, 0, 1)).toBe(10);
      expect(getNextStep(steps, 100, -1)).toBe(90);
      expect(getNextStep(steps, 100, 1)).toBe(100);
    });
  });

  describe('getIntervals', () => {
    it('should get correct intervals from stepSize', () => {
      const min = 0;
      const max = 10;
      const stepSize = 2;

      const result = getIntervals({min, max, stepSize});
      expect(result).toEqual([0, 2, 4, 6, 8, 10]);
    });

    it("should get correct intervals from stepSize that doesn't divide the scale without remainder", () => {
      const min = 0;
      const max = 10;
      const stepSize = 2.3;

      const result = getIntervals({min, max, stepSize, maxDecimals: 1});
      expect(result).toEqual([0, 2.3, 4.6, 6.9, 9.2, 10]);
    });
  });
});
