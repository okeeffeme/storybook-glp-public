import {mount} from 'enzyme';
import React from 'react';
import StepMarks from '../src/components/step-marks';

const render = (props) => mount(<StepMarks {...props} />);

describe('Confirmit-slider/Mark', () => {
  it('should render with correct amount of marks', () => {
    const stepIntervals = [1, 2, 3, 4, 5];

    const wrapper = render({stepIntervals});
    expect(wrapper.find('[data-slider-mark]')).toHaveLength(5);
  });
});
