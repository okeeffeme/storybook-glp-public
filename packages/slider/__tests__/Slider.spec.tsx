import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Slider from '../src/components/slider';

const defaultProps = {
  disabled: false,
  label: 'Test',
  onChange: jest.fn(),
};

describe('Confirmit-slider ::', () => {
  it('should render with correct value', () => {
    render(<Slider {...defaultProps} value={50} />);

    expect(screen.getByText(/50/i)).toHaveAttribute('data-slider-value', '50');
  });

  it('should render with slider rail width', () => {
    render(<Slider {...defaultProps} value={75} />);
    expect(screen.getByTestId('dataslidertrack')).toHaveStyle('width: 75%');
  });

  it('should not render label when label is not provided', () => {
    render(<Slider {...defaultProps} value={50} />);
    expect(screen.getByText(/50/i)).not.toHaveClass('data-slider-form-label');
  });

  it('should render form value and not text field when showInputField is false', () => {
    render(<Slider {...defaultProps} value={50} />);
    expect(screen.getByText(/50/i)).toHaveAttribute('data-slider-value');
    expect(screen.getByText(/50/i)).not.toHaveAttribute(
      'data-slider-input-field'
    );
  });

  it('should render with correct label when provided label', () => {
    render(<Slider {...defaultProps} value={50} label="slider label" />);

    expect(screen.getByText(/slider label/i)).toBeInTheDocument();
  });

  it('should render with correct min/max label', () => {
    render(<Slider {...defaultProps} value={50} min={5} max={50} />);
    expect(screen.getByTestId('min')).toHaveTextContent('5');
    expect(screen.getByTestId('max')).toHaveTextContent('50');
  });

  it('should trigger onChange when input field is updated', () => {
    const onChange = jest.fn();
    const newValue = '70';

    render(
      <Slider
        {...defaultProps}
        value={50}
        showInputField={true}
        onChange={onChange}
      />
    );
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {
        value: newValue,
      },
    });

    expect(onChange).toHaveBeenCalledWith(newValue);
  });
});
