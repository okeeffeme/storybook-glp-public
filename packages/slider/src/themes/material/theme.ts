import sliderClassNames from './slider.module.css';
import sliderWidthInputClassNames from './slider-input.module.css';

export default {
  slider: {
    baseClassName: 'comd-slider',
    classNames: sliderClassNames,
  },
  sliderInput: {
    baseClassName: 'comd-slider-input',
    classNames: sliderWidthInputClassNames,
  },
};
