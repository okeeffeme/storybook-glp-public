import React, {useState, FC} from 'react';
import cn from 'classnames';

import {bemFactory, useTheme} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {useSlider} from './use-slider';
import componentThemes from '../themes';

import {TextField} from '@jotunheim/react-text-field';
import {Tooltip} from '@jotunheim/react-tooltip';
import StepMarks from './step-marks';

interface SliderProps {
  onChange: (value: number) => void;
  label: string;
  disabled: boolean;
  min?: number;
  max?: number;
  value?: number;
  showInputField?: boolean;
  maxDecimals?: number;
  stepSize?: number;
  showMarkers?: boolean;
}

const Slider: FC<SliderProps> = ({
  min = 0,
  max = 100,
  value = 0,
  onChange,
  label,
  disabled,
  maxDecimals = 2,
  showInputField = false,
  stepSize = 1,
  showMarkers = stepSize > 1,
  ...rest
}) => {
  const {baseClassName, classNames} = useTheme('slider', componentThemes);
  const [handlerHover, setHandlerHover] = useState(false);

  const {
    mouseDown,
    handleMouseDown,
    handleKeyDown,
    handleInputField,
    percent,
    stepIntervals,
    trackRef,
    handlerRef,
  } = useSlider({min, max, value, stepSize, disabled, onChange, maxDecimals});

  const {block, element} = bemFactory({baseClassName, classNames});
  const sliderClassNames = block();
  const labelClassNames = cn(element('form-label'), {
    [element('form-label', 'disabled')]: disabled,
  });
  const formValueClassNames = cn(element('form-value'), {
    [element('form-value', 'disabled')]: disabled,
  });
  const trackClassNames = cn(element('track'), {
    [element('track', 'disabled')]: disabled,
  });
  const railClassNames = cn(element('rail'), {
    [element('rail', 'disabled')]: disabled,
  });
  const handlerClassNames = cn(element('handler'), {
    [element('ripple')]: !disabled,
    [element('handler', 'disabled')]: disabled,
  });
  const sliderWrapperClassNames = cn(element('slider-wrapper'), {
    [element('slider-wrapper', 'has-input-field')]: showInputField,
  });

  const roundMultiplier = Math.pow(10, maxDecimals);
  const roundedValue = Math.round(value * roundMultiplier) / roundMultiplier;

  return (
    <div
      className={sliderClassNames}
      data-slider=""
      data-testid="slider"
      {...extractDataAndAriaProps(rest)}>
      {label && (
        <span className={labelClassNames} data-slider-form-label="">
          {label}
        </span>
      )}
      <div className={element('slider-group')}>
        <div className={sliderWrapperClassNames}>
          <span
            className={element('range-label')}
            data-slider-min-label=""
            data-testid="min">
            {min}
          </span>
          <div
            ref={trackRef}
            className={element('slider-container')}
            onKeyDown={handleKeyDown}
            onMouseDown={handleMouseDown}>
            <div data-slider-rail="" className={railClassNames} />
            <div
              data-slider-track=""
              data-testid="dataslidertrack"
              className={trackClassNames}
              style={{width: `${percent * 100}%`}}
            />
            {showMarkers && <StepMarks stepIntervals={stepIntervals} />}
            <Tooltip
              ref={handlerRef}
              content={`${value}`}
              open={mouseDown || handlerHover}
              placement={'top'}>
              <div
                data-slider-handler=""
                tabIndex={0}
                className={handlerClassNames}
                style={{left: `${percent * 100}%`}}
                onMouseOver={() => setHandlerHover(true)}
                onMouseLeave={() => setHandlerHover(false)}
              />
            </Tooltip>
          </div>
          <span
            className={element('range-label')}
            data-slider-max-label=""
            data-testid="max">
            {max}
          </span>
        </div>
        {showInputField ? (
          <div className={element('input-field-wrapper')}>
            <TextField
              type={'number'}
              value={roundedValue}
              min={min}
              max={max}
              step={stepSize}
              onChange={handleInputField}
              disabled={disabled}
              data-slider-input-field
            />
          </div>
        ) : (
          <span
            className={formValueClassNames}
            data-slider-value={roundedValue}>
            {roundedValue}
          </span>
        )}
      </div>
    </div>
  );
};

export default Slider;
