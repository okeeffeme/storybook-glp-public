import {
  useEffect,
  useState,
  useCallback,
  useMemo,
  useRef,
  KeyboardEventHandler,
  MouseEventHandler,
} from 'react';

interface ValueIsAtMinOrMax {
  nextIndex: number;
  steps: number[];
}

interface GetIntervals {
  min: number;
  max: number;
  stepSize: number;
  maxDecimals: number;
}

interface UseSlider {
  min: number;
  max: number;
  value: number;
  stepSize: number;
  disabled: boolean;
  onChange: (nextValue: number) => void;
  maxDecimals: number;
}

const keyCodes = {
  ArrowUp: 38,
  ArrowDown: 40,
  ArrowLeft: 37,
  ArrowRight: 39,
  Home: 36,
  End: 35,
};

export const getClosestStep = (array: number[], target: number) =>
  array.reduce((prev, curr) => {
    return Math.abs(curr - target) < Math.abs(prev - target) ? curr : prev;
  });

export const getPercent = (value: number, min: number, max: number) => {
  let percent = (value - min) / (max - min);
  if (isNaN(percent)) {
    percent = 0;
  }

  return percent;
};

const valueIsAtMinOrMax = ({nextIndex, steps}: ValueIsAtMinOrMax) =>
  nextIndex < 0 || nextIndex > steps.length - 1;

export const getNextStep = (
  steps: number[],
  value: number,
  increment: number
) => {
  const index = steps.findIndex((element) => element === value);

  const nextIndex = index + increment;
  if (valueIsAtMinOrMax({nextIndex, steps})) return value;

  return steps[nextIndex];
};

const roundNumber = (num: number, maxDecimals: number) =>
  maxDecimals === undefined ? num : Number(num.toFixed(maxDecimals));

export const getIntervals = ({
  min,
  max,
  stepSize,
  maxDecimals,
}: GetIntervals) => {
  const steps = Math.ceil(Math.abs((max - min) / stepSize));
  return Array(steps + 1)
    .fill(undefined)
    .map((step, index, array) => {
      return index === array.length - 1
        ? max
        : roundNumber(min + stepSize * index, maxDecimals);
    });
};

export const useSlider = ({
  min,
  max,
  value,
  stepSize,
  disabled,
  onChange,
  maxDecimals,
}: UseSlider) => {
  const trackRef = useRef<HTMLDivElement | null>(null);
  const handlerRef = useRef<HTMLElement | null>(null);
  const [mouseDown, setMouseDown] = useState(false);

  const intervals = useMemo(
    () => getIntervals({min, max, stepSize, maxDecimals}),
    [stepSize, min, max, maxDecimals]
  );
  const getTrackOffset = () => {
    return trackRef.current?.getBoundingClientRect().left;
  };

  const resolveValue = useCallback(
    (value) => {
      if (value === undefined) return min;

      if (value > max) return max;

      if (value < min) return min;

      return value;
    },
    [min, max]
  );

  const setValueFromPosition = useCallback(
    (position) => {
      const positionMax = trackRef.current?.clientWidth;

      if (!positionMax) {
        return;
      }

      let nextValue;

      if (position <= 0) {
        nextValue = min;
      } else if (position >= positionMax) {
        nextValue = max;
      } else {
        nextValue =
          roundNumber((position / positionMax) * (max - min), maxDecimals) +
          min;
      }

      nextValue = intervals
        ? getClosestStep(intervals, resolveValue(nextValue))
        : resolveValue(nextValue);
      if (nextValue !== value && onChange) {
        onChange(nextValue);
      }
    },
    [onChange, min, max, intervals, value, resolveValue, maxDecimals]
  );

  const handleMouseDown: MouseEventHandler<HTMLDivElement> = (event) => {
    event.preventDefault();
    if (disabled) return;
    setMouseDown(true);

    const position = event.clientX - (getTrackOffset() ?? 0);
    setValueFromPosition(position);

    // Set focus manually since we called preventDefault()
    handlerRef.current?.focus();
  };

  const handleDragMouseMove = useCallback(
    (e: MouseEvent) => {
      requestAnimationFrame(() => {
        const position = e.clientX - (getTrackOffset() ?? 0);
        setValueFromPosition(position);
      });
    },
    [setValueFromPosition]
  );

  const handleMouseEnd = useCallback(() => {
    setMouseDown(false);
  }, [setMouseDown]);

  useEffect(() => {
    if (mouseDown && !disabled) {
      document.addEventListener('mousemove', handleDragMouseMove);
      document.addEventListener('mouseup', handleMouseEnd);
    }

    return () => {
      document.removeEventListener('mousemove', handleDragMouseMove);
      document.removeEventListener('mouseup', handleMouseEnd);
    };
  }, [handleDragMouseMove, handleMouseEnd, mouseDown, disabled]);

  const handleKeyDown: KeyboardEventHandler<HTMLDivElement> = (event) => {
    event.preventDefault();

    let nextValue;

    switch (event.keyCode) {
      case keyCodes.ArrowDown:
      case keyCodes.ArrowLeft:
        nextValue = intervals ? getNextStep(intervals, value, -1) : value - 1;
        break;
      case keyCodes.ArrowUp:
      case keyCodes.ArrowRight:
        nextValue = intervals ? getNextStep(intervals, value, 1) : value + 1;
        break;
      case keyCodes.Home:
        nextValue = max;
        break;
      case keyCodes.End:
        nextValue = min;
        break;

      default:
        return;
    }

    nextValue = resolveValue(nextValue);

    if (nextValue !== value && onChange) {
      onChange(nextValue);
    }
  };

  const handleInputField = (newValue: string) => {
    const nextValue = resolveValue(newValue);
    if (nextValue !== value && onChange) {
      onChange(nextValue);
    }
  };

  const stepIntervals = useMemo(
    () =>
      (intervals || []).slice(1, -1).map((mark) => getPercent(mark, min, max)), // remove first and last marks
    [intervals, min, max]
  );
  const percent = getPercent(resolveValue(value), min, max);

  return {
    trackRef,
    handlerRef,
    mouseDown,
    percent,
    stepIntervals,
    handleKeyDown,
    handleMouseDown,
    handleInputField,
  };
};
