import React, {memo, FC} from 'react';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import componentThemes from '../themes';
interface StepsMarksProps {
  stepIntervals?: number[];
}
const StepMarks: FC<StepsMarksProps> = ({stepIntervals = []}) => {
  const {baseClassName, classNames} = useTheme('slider', componentThemes);
  const {element} = bemFactory({baseClassName, classNames});

  return (
    <div className={element('marks-container')}>
      {stepIntervals.map((stepInterval) => (
        <div
          key={stepInterval}
          className={element('mark')}
          style={{left: `${stepInterval * 100}%`}}
          data-slider-mark={stepInterval}
        />
      ))}
    </div>
  );
};

export default memo(StepMarks);
