# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.46&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.47&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.46&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.46&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.44&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.45&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.43&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.44&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.42&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.43&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.41&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.42&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.40&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.41&targetRepoId=1246) (2023-03-02)

### Bug Fixes

- use math.div for division in sass ([NPM-991](https://jiraosl.firmglobal.com/browse/NPM-991)) ([ca0312f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ca0312f62bff2fc51a2fed9b226819a264fc9d8a))

## [9.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.39&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.40&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [9.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.38&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.39&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.36&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.38&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- import types in separate type import for pager, switch and textarea package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([28a09c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/28a09c2cd05cbcd3745b6e0c8fa1ff7ba861ccff))

## [9.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.36&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.37&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- import types in separate type import for pager, switch and textarea package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([9c1b367](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9c1b3674319da58da589a71e1d3eea1769cc4718))

## [9.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.35&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.36&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.34&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.35&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.33&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.34&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- export switch interface ([NPM-1237](https://jiraosl.firmglobal.com/browse/NPM-1237)) ([e63e94f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e63e94f67c6793077a3f974c5d772bbc82989cc0))

## [9.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.32&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.33&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.31&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.32&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.30&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.31&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.29&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.30&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.27&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.27&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.28&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.26&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.27&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.25&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.26&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.24&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.25&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.23&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.24&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.21&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.23&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.21&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.18&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.18&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.18&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.17&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.16&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.14&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.13&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.14&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.12&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.11&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.10&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.9&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.8&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.6&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.3&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.2&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.1&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-switch

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-switch@9.0.0&sourceBranch=refs/tags/@jotunheim/react-switch@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-switch

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@8.0.1&sourceBranch=refs/tags/@confirmit/react-switch@8.0.2&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-switch

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@8.0.0&sourceBranch=refs/tags/@confirmit/react-switch@8.0.1&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-switch

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.3.3&sourceBranch=refs/tags/@confirmit/react-switch@8.0.0&targetRepoId=1246) (2022-06-02)

### Bug Fixes

- Remove support for "className" prop ([NPM-1003](https://jiraosl.firmglobal.com/browse/NPM-1003)) ([60ae67f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/60ae67f248a19efb3c35f5fa6c187d87cfeaec04))

### BREAKING CHANGES

- Remove support for "className" prop (NPM-1003)

## [7.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.3.2&sourceBranch=refs/tags/@confirmit/react-switch@7.3.3&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.3.1&sourceBranch=refs/tags/@confirmit/react-switch@7.3.2&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-switch

## [7.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.3.0&sourceBranch=refs/tags/@confirmit/react-switch@7.3.1&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-switch

# [7.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.49&sourceBranch=refs/tags/@confirmit/react-switch@7.3.0&targetRepoId=1246) (2022-04-06)

### Features

- Make id property optional in Switch, Toggle and ToggleButton components ([NPM-994](https://jiraosl.firmglobal.com/browse/NPM-994)) ([b3d2356](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b3d23568079a5e7f4d1e813436a0141858e6a54e))

## [7.2.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.48&sourceBranch=refs/tags/@confirmit/react-switch@7.2.49&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.47&sourceBranch=refs/tags/@confirmit/react-switch@7.2.48&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.46&sourceBranch=refs/tags/@confirmit/react-switch@7.2.47&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.44&sourceBranch=refs/tags/@confirmit/react-switch@7.2.45&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.43&sourceBranch=refs/tags/@confirmit/react-switch@7.2.44&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.42&sourceBranch=refs/tags/@confirmit/react-switch@7.2.43&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.41&sourceBranch=refs/tags/@confirmit/react-switch@7.2.42&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.40&sourceBranch=refs/tags/@confirmit/react-switch@7.2.41&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.39&sourceBranch=refs/tags/@confirmit/react-switch@7.2.40&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.38&sourceBranch=refs/tags/@confirmit/react-switch@7.2.39&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.37&sourceBranch=refs/tags/@confirmit/react-switch@7.2.38&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.35&sourceBranch=refs/tags/@confirmit/react-switch@7.2.36&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.34&sourceBranch=refs/tags/@confirmit/react-switch@7.2.35&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.33&sourceBranch=refs/tags/@confirmit/react-switch@7.2.34&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.32&sourceBranch=refs/tags/@confirmit/react-switch@7.2.33&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.31&sourceBranch=refs/tags/@confirmit/react-switch@7.2.32&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.30&sourceBranch=refs/tags/@confirmit/react-switch@7.2.31&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.29&sourceBranch=refs/tags/@confirmit/react-switch@7.2.30&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.28&sourceBranch=refs/tags/@confirmit/react-switch@7.2.29&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.27&sourceBranch=refs/tags/@confirmit/react-switch@7.2.28&targetRepoId=1246) (2021-07-23)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.26&sourceBranch=refs/tags/@confirmit/react-switch@7.2.27&targetRepoId=1246) (2021-07-22)

### Bug Fixes

- helper text icon should have default cursor style instead of pointer ([08e0203](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/08e0203e5bf1ed618b0b2dab5074561f7bbb6f56))

## [7.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.25&sourceBranch=refs/tags/@confirmit/react-switch@7.2.26&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.24&sourceBranch=refs/tags/@confirmit/react-switch@7.2.25&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.23&sourceBranch=refs/tags/@confirmit/react-switch@7.2.24&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.22&sourceBranch=refs/tags/@confirmit/react-switch@7.2.23&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.21&sourceBranch=refs/tags/@confirmit/react-switch@7.2.22&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.20&sourceBranch=refs/tags/@confirmit/react-switch@7.2.21&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.19&sourceBranch=refs/tags/@confirmit/react-switch@7.2.20&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.18&sourceBranch=refs/tags/@confirmit/react-switch@7.2.19&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.17&sourceBranch=refs/tags/@confirmit/react-switch@7.2.18&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.16&sourceBranch=refs/tags/@confirmit/react-switch@7.2.17&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.15&sourceBranch=refs/tags/@confirmit/react-switch@7.2.16&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.13&sourceBranch=refs/tags/@confirmit/react-switch@7.2.14&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.12&sourceBranch=refs/tags/@confirmit/react-switch@7.2.13&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.11&sourceBranch=refs/tags/@confirmit/react-switch@7.2.12&targetRepoId=1246) (2021-04-14)

### Bug Fixes

- remove Title Case on labels and titles ([NPM-761](https://jiraosl.firmglobal.com/browse/NPM-761)) ([0213c34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0213c343d273533335a3a64b1c736f4b3823e2d1))

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.10&sourceBranch=refs/tags/@confirmit/react-switch@7.2.11&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.9&sourceBranch=refs/tags/@confirmit/react-switch@7.2.10&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.8&sourceBranch=refs/tags/@confirmit/react-switch@7.2.9&targetRepoId=1246) (2021-03-22)

### Bug Fixes

- don't toggle switch when clicking help text ([NPM-746](https://jiraosl.firmglobal.com/browse/NPM-746)) ([1ea3b20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1ea3b20f3ee31448a7b1f8c49892256b29cf0647))
- dont add margins unless label is set ([NPM-746](https://jiraosl.firmglobal.com/browse/NPM-746)) ([694c959](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/694c959fa305a7d515d501bcf4907913ce9b6bff))

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.7&sourceBranch=refs/tags/@confirmit/react-switch@7.2.8&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.6&sourceBranch=refs/tags/@confirmit/react-switch@7.2.7&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.5&sourceBranch=refs/tags/@confirmit/react-switch@7.2.6&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.4&sourceBranch=refs/tags/@confirmit/react-switch@7.2.5&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.3&sourceBranch=refs/tags/@confirmit/react-switch@7.2.4&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.1&sourceBranch=refs/tags/@confirmit/react-switch@7.2.2&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-switch

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.2.0&sourceBranch=refs/tags/@confirmit/react-switch@7.2.1&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-switch

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.1.3&sourceBranch=refs/tags/@confirmit/react-switch@7.2.0&targetRepoId=1246) (2021-01-21)

### Features

- update test ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([dfd9e30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dfd9e303c8b1709ee736a6a0460bc16a0b276d07))
- use InformationIcon in components, and style cleanup ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([5436ea1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5436ea15a84f20bcdbecbe74724026d532ed41de))

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.1.2&sourceBranch=refs/tags/@confirmit/react-switch@7.1.3&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-switch

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.1.1&sourceBranch=refs/tags/@confirmit/react-switch@7.1.2&targetRepoId=1246) (2020-12-11)

### Bug Fixes

- avoid switch help icon to wrap to a new line by itself, since it looks weird ([NPM-653](https://jiraosl.firmglobal.com/browse/NPM-653)) ([4e25213](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4e252139a228191075aa59bfac9acc2dc4fc695a))
- remove incorrect margin-bottom on Switch, and adjust position of handle since it looks off-center ([NPM-653](https://jiraosl.firmglobal.com/browse/NPM-653)) ([30d34ed](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/30d34edc3ee3b6956dba53c909ec7b13b32a5cbb))
- switch now correctly uses text-transform: uppercase on label ([NPM-653](https://jiraosl.firmglobal.com/browse/NPM-653)) ([1a4de1e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1a4de1e36286487fb487c2a7e19c024783e865f2))

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.1.0&sourceBranch=refs/tags/@confirmit/react-switch@7.1.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-switch

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.13&sourceBranch=refs/tags/@confirmit/react-switch@7.1.0&targetRepoId=1246) (2020-12-11)

### Features

- support wrapper tooltip for switch ([NPM-644](https://jiraosl.firmglobal.com/browse/NPM-644)) ([1fbb727](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1fbb7276ce47f7663fc5011ffc04258f0bdef6dc))

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.12&sourceBranch=refs/tags/@confirmit/react-switch@7.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.11&sourceBranch=refs/tags/@confirmit/react-switch@7.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.10&sourceBranch=refs/tags/@confirmit/react-switch@7.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.9&sourceBranch=refs/tags/@confirmit/react-switch@7.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.6&sourceBranch=refs/tags/@confirmit/react-switch@7.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.3&sourceBranch=refs/tags/@confirmit/react-switch@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.2&sourceBranch=refs/tags/@confirmit/react-switch@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-switch

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@7.0.1&sourceBranch=refs/tags/@confirmit/react-switch@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-switch

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@6.0.0&sourceBranch=refs/tags/@confirmit/react-switch@7.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@5.0.4&sourceBranch=refs/tags/@confirmit/react-switch@6.0.0&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- Minor adjustments to the look, and is now up to date with the Design System spec. (NPM-597)

### Features

- New `helperText` prop, allows you to add a info icon with a help text in a tooltip. ([NPM-597](https://jiraosl.firmglobal.com/browse/NPM-597)) ([103c6f4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/103c6f406c1dfb02400ee7a7885e7007f53f0f61))

### BREAKING CHANGES

- Switch component used to have margin top/bottom, but this is removed. If you do not rely on these margins, you should be able to upgrade without issues. If you do rely on these margins, you can add a wrapper around that has margin top and bottom. (NPM-597)
- Previously this component also included a default theme, which looked identical to the material theme. The default theme has now been removed completely. (NPM-597)

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-switch@5.0.3&sourceBranch=refs/tags/@confirmit/react-switch@5.0.4&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-switch

## [5.0.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-switch@5.0.2...@confirmit/react-switch@5.0.3) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-switch

## CHANGELOG

### v5.0.2

- Fix: Incorrect label color.

### v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v4.1.0

- Removing package version in class names.

### v4.0.0

- **BREAKING**: - Renamed to react-switch

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.0

- **BREAKING** - Removed props: baseClassName, classNames

### v1.0.0

- BREAKING CHANGE: Removing prop `type`
- Major change: Replacing default styling with material styling.
- Correcting CSS styling of material theme to allow keyboard navigation
- Enabling label via prop `label` , along with left and right options
- Changing storybook stories

### v0.0.4

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
