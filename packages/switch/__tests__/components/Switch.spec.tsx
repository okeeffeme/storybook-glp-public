import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {Switch} from '../../src';

describe('Switch', () => {
  it('should render with default props', () => {
    render(<Switch />);

    expect(screen.getByRole('checkbox')).toBeInTheDocument();
  });

  it('should render label and wrapper when only label is set', () => {
    render(<Switch label="My label" />);

    expect(screen.getByText(/my label/i)).toBeInTheDocument();
  });

  it('should render helpText and wrapper when only helpText is set', () => {
    const {container} = render(<Switch id="toggle" helperText="My help" />);

    expect(
      container.querySelector('.comd-button-toggle__label-help-wrapper')
    ).toBeTruthy();
    expect(
      container.querySelector('.comd-button-toggle__toggle-text-label')
    ).toBeFalsy();
    expect(
      container.querySelector('.comd-button-toggle__help-icon-wrapper')
    ).toBeTruthy();
  });

  it('should not render label or wrapper when not set', () => {
    const {container} = render(<Switch />);

    expect(
      container.querySelector('.comd-button-toggle__label-help-wrapper')
    ).toBeFalsy();
    expect(
      container.querySelector('.comd-button-toggle__toggle-text-label')
    ).toBeFalsy();
    expect(
      container.querySelector('.comd-button-toggle__help-icon-wrapper')
    ).toBeFalsy();
  });

  it('should render with defaultChecked = true', () => {
    render(<Switch defaultChecked={true} />);

    expect(screen.getByRole('checkbox')).toHaveAttribute('checked');
  });

  it('should render with checked = true', () => {
    const onChange = jest.fn();

    render(<Switch checked onChange={onChange} />);

    expect(screen.getByRole('checkbox')).toHaveAttribute('checked');
  });

  it('should render disabled toggle', () => {
    render(<Switch disabled />);

    expect(screen.getByRole('checkbox')).toHaveAttribute('disabled');
  });

  it('should show info icon if helperText is provided', () => {
    const {container} = render(<Switch helperText="help text" />);

    expect(
      container.querySelector('.comd-button-toggle__help-icon-wrapper')
        ?.children[0]
    ).toBe(container.querySelector('svg'));
  });

  it('should not show info icon if helperText is not provided', () => {
    const {container} = render(<Switch />);

    expect(container.querySelector('svg')).not.toBeInTheDocument();
  });

  it('should call onChange if input is changed', () => {
    const change = jest.fn();

    render(<Switch id="toggle" onChange={change} />);

    fireEvent.click(screen.getByRole('checkbox'));

    expect(change).toBeCalled();
  });
});
