import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Switch} from '../src';

/* eslint-disable-next-line */
const Pad = ({children}) => {
  return (
    <div
      style={{
        padding: '24px',
        display: 'grid',
        gridAutoRows: 'auto',
        gap: '12px',
      }}>
      {children}
    </div>
  );
};

storiesOf('Components/switch', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('default', () => (
    <Pad>
      <Switch id="1" label="Switch Label" helperText="help text" />
      <Switch
        id="2"
        label="Switch with long labelSwitch with long labelSwitch with long labelSwitch with long labelSwitch with long labelSwitch with long labelSwitch with long labelSwitch with long label"
        helperText="help text"
      />
      <Switch
        id="3"
        defaultChecked={true}
        labelOnLeft
        label="Switch Label on left"
        helperText="help text"
      />
    </Pad>
  ))
  .add('Controlled', () => {
    const [isChecked, setIsChecked] = React.useState(false);

    return (
      <Pad>
        <Switch
          id="controlled"
          label="Label"
          checked={isChecked}
          onChange={(e) => {
            setIsChecked(e.target.checked);
          }}
          helperText="Some help text"
        />
      </Pad>
    );
  })
  .add('With Knobs', () => {
    return (
      <Pad>
        <Switch
          id={text('id', 'with-knobs')}
          label={text('label', 'Label')}
          labelOnLeft={boolean('labelOnLeft', false)}
          checked={boolean('checked', false)}
          disabled={boolean('disabled', false)}
          helperText={text('helperText', 'Some help text')}
          onChange={() => {}} // avoid console warning from about missing handler
        />
      </Pad>
    );
  });
