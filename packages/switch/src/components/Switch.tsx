import React from 'react';
import cn from 'classnames';

import {
  extractDataAndAriaProps,
  extractOnEventProps,
  useUuid,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Tooltip from '@jotunheim/react-tooltip';
import {InformationIcon} from '@jotunheim/react-icons';

import LabelHelpWrapper from './LabelHelpWrapper';

import classNames from './Switch.module.css';

// This is used for easier alignment of content that has no text.
// Adding this character makes the element behave as it has text content.
// This has to be implemented as a Fragment, to avoid React escaping the values.
const ZeroWidthSpaceChar = () => <>&#8203;</>;

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-button-toggle',
  classNames,
});

export interface SwitchProps {
  id?: string;
  disabled?: boolean;
  checked?: boolean;
  defaultChecked?: boolean;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  label?: string;
  labelOnLeft?: boolean;
  helperText?: React.ReactNode;
}

export const Switch = React.forwardRef<HTMLDivElement, SwitchProps>(
  (
    {
      id,
      label,
      checked,
      onChange,
      helperText,
      defaultChecked,
      disabled = false,
      labelOnLeft = false,
      ...rest
    },
    ref
  ) => {
    const uuid = useUuid();
    id = id ?? uuid;

    const classes = cn(block(), {
      [modifier('disabled')]: disabled,
      [modifier('left')]: labelOnLeft,
    });

    return (
      <div
        data-testid="switch"
        ref={ref}
        className={classes}
        {...extractOnEventProps(rest)}
        {...extractDataAndAriaProps(rest)}>
        <input
          id={id}
          type="checkbox"
          className={element('checkbox')}
          defaultChecked={defaultChecked}
          checked={checked}
          disabled={disabled}
          onChange={onChange}
        />
        <label htmlFor={id} className={element('label')}>
          {!labelOnLeft && <span className={element('toggle-element')} />}

          <LabelHelpWrapper>
            {!!label && (
              <span className={element('toggle-text-label')}>{label}</span>
            )}
            {helperText && (
              <>
                <span className={element('nbsp')}>&nbsp;</span>
                <span
                  className={element('help-icon-wrapper')}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                  }}>
                  <ZeroWidthSpaceChar />
                  <Tooltip content={helperText}>
                    <InformationIcon />
                  </Tooltip>
                </span>
              </>
            )}
          </LabelHelpWrapper>

          {labelOnLeft && <span className={element('toggle-element')} />}
        </label>
      </div>
    );
  }
);

Switch.displayName = 'Switch';

export default Switch;
