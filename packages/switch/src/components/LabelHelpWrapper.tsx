import React, {ReactNode} from 'react';

import {hasVisibleChildren} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Switch.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-button-toggle',
  classNames,
});

export const LabelHelpWrapper = ({children}: {children: ReactNode}) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return <span className={element('label-help-wrapper')}>{children}</span>;
};

export default LabelHelpWrapper;
