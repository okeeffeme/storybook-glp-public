import Switch from './components/Switch';
import type {SwitchProps} from './components/Switch';

export {Switch, SwitchProps};

export default Switch;
