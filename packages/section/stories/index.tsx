import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {boolean, select, text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Icon, {pencil, plus, trashBin} from '../../icons/src/index';
import {TextField} from '../../text-field/src/index';
import {Button} from '../../button/src/index';
import {Switch} from '../../switch/src';
import {Tooltip} from '../../tooltip/src/index';
import Dialog from '../../dialog/src/index';

import Section, {
  SectionList,
  SectionListHeader,
  SectionDraggableList,
  PaddingSize,
} from '../src';
import {SectionBackgroundColor} from '../src/types';

const actions = {
  onAction: action('selectAction'),
  // eslint-disable-next-line react/display-name
  getActions: () => [
    {
      command: 'edit',
      icon: <Icon path={pencil} />, // icon can either be function or react element
      tooltip: 'Edit thing',
    },
    {
      command: 'delete',
      icon: <Icon path={trashBin} />, // icon can either be function or react element
      tooltip: undefined,
    },
  ],
};

const optionsMenu = {
  onSelect: () => action('selectMenuItem'),
  getOptions: () => [
    {
      command: 'edit',
      name: 'Edit',
      iconPath: pencil,
    },
    {
      command: 'delete',
      name: 'Delete',
      iconPath: trashBin,
      disabled: true,
    },
  ],
};

const optionsMenuWithHelperText = {
  ...optionsMenu,
  getOptions: () => [
    {
      command: 'delete',
      name: 'Delete',
      helperText: 'This is a helper text.',
    },
  ],
};

const optionsMenuWithDisabledOption = {
  ...optionsMenu,
  getOptions: () => [
    {
      command: 'delete',
      name: 'Delete',
      disabled: true,
    },
  ],
};

const optionsMenuWithIcon = {
  ...optionsMenu,
  iconPath: plus,
};

const sectionList = [
  {
    id: '1',
    title: 'Item 1',
    draggable: true,
    collapsible: true,
    defaultCollapsed: false,
    paddingSize: PaddingSize.Medium,
    bodyHasPadding: true,
    content: (
      <TextField
        onChange={() => {}}
        label="Text Field"
        placeholder="Text input"
        type="text"
      />
    ),
    optionsMenu,
  },
  {
    id: '2',
    title: 'Item 2',
    draggable: true,
    collapsible: true,
    paddingSize: PaddingSize.Medium,
    error: true,
    errorText: 'error text',
    bodyHasPadding: true,
    content: (
      <TextField
        onChange={() => {}}
        label="Text Field"
        placeholder="Text input"
        type="text"
      />
    ),
    optionsMenu,
  },
  {
    id: '3',
    title: 'Item 3',
    draggable: true,
    collapsible: true,
    paddingSize: PaddingSize.Medium,
    bodyHasPadding: true,
    content: (
      <TextField
        onChange={() => {}}
        label="Text Field"
        placeholder="Text input"
        type="text"
      />
    ),
    optionsMenu,
  },
];

storiesOf('Components/section/basic', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('basic', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}
      error={boolean('error', false)}
      errorText={text('errorText', '')}>
      <Section.Header title="Basic" />
      <Section.Body hasPadding={boolean('Section.Body hasPadding', false)}>
        Section content
      </Section.Body>
    </Section>
  ))
  .add('with footer', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}>
      <Section.Header title="With Footer" />
      <Section.Body hasPadding={boolean('Section.Body hasPadding', false)}>
        Section content
      </Section.Body>
      <Section.Footer>
        <Button>Cancel</Button>
        <Button appearance={Button.appearances.primarySuccess}>Apply</Button>
      </Section.Footer>
    </Section>
  ))
  .add('with actions and options menu', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}>
      <Section.Header
        title="With actions and options menu"
        actions={actions}
        optionsMenu={optionsMenu}
      />
      <Section.Body hasPadding={boolean('Section.Body hasPadding', false)}>
        Section content
      </Section.Body>
    </Section>
  ))
  .add('with actions and options menu with dynamic icon', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}>
      <Section.Header
        title="With actions and options menu with dynamic icon"
        actions={actions}
        optionsMenu={optionsMenuWithIcon}
      />
      <Section.Body hasPadding={boolean('Section.Body hasPadding', false)}>
        Section content
      </Section.Body>
    </Section>
  ))
  .add('with header children', () => {
    const [checked, setChecked] = React.useState(false);

    return (
      <Section
        collapsible={true}
        error={boolean('error', false)}
        errorText={text('errorText', '')}>
        <Section.Header title="With header children">
          <Tooltip content={'Enable or disable some stuff'}>
            <Switch
              id={`toggle_scenario`}
              label={'Enable Scenario'}
              checked={checked}
              onChange={(e) => {
                e.stopPropagation();
                setChecked((c) => !c);
              }}
            />
          </Tooltip>
          <Tooltip content={'Add or remove some stuff'}>
            <Button
              onClick={(e) => {
                e.stopPropagation();
              }}
              appearance={Button.appearances.secondaryNeutral}>
              Add/Remove
            </Button>
          </Tooltip>
        </Section.Header>
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
    );
  })
  .add('without paddings', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}
      error={boolean('error', false)}
      errorText={text('errorText', '')}>
      <Section.Header
        title="Without padding"
        hasPadding={boolean('Section.Header hasPadding', false)}
      />
      <Section.Body hasPadding={boolean('Section.Body hasPadding', false)}>
        Section body
      </Section.Body>
      <Section.Footer hasPadding={boolean('Section.Footer hasPadding', false)}>
        Footer
      </Section.Footer>
    </Section>
  ))
  .add('with background color', () => (
    <Section
      paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}
      backgroundColor={select(
        'backgroundColor',
        SectionBackgroundColor,
        SectionBackgroundColor.AltPrimary
      )}>
      <Section.Header title="With Alt Primary background" />
      <Section.Body hasPadding={true}>Section body</Section.Body>
      <Section.Footer>Footer</Section.Footer>
    </Section>
  ));

storiesOf('Components/section/collapsible', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('collapsible', () => (
    <Section collapsible={true}>
      <Section.Header title="Collapsible" optionsMenu={optionsMenu} />
      <Section.Collapsible>
        <Section.Body>Section content</Section.Body>
      </Section.Collapsible>
    </Section>
  ))
  .add('default collapsed', () => (
    <Section collapsible={true} defaultCollapsed={true}>
      <Section.Header title="Default collapsed" />
      <Section.Collapsible>
        <Section.Body>Section content</Section.Body>
      </Section.Collapsible>
    </Section>
  ))
  .add('Controlled collapsed', () => {
    const [collapsed, setCollapsed] = React.useState(true);

    return (
      <div style={{margin: 16}}>
        <div style={{marginBottom: 16}}>
          <Switch
            id="collapsed"
            label="Collapsed"
            checked={collapsed}
            onChange={() => setCollapsed(!collapsed)}
          />
        </div>
        <Section
          collapsible={true}
          collapsed={collapsed}
          onToggle={() => {
            setCollapsed(!collapsed);
          }}>
          <Section.Header title="Controllable collapsed" />
          <Section.Collapsible>
            <Section.Body>Section content</Section.Body>
          </Section.Collapsible>
        </Section>
      </div>
    );
  })
  .add('with actions and options menu', () => (
    <Section collapsible={true}>
      <Section.Header
        title="With actions and options menu"
        actions={actions}
        optionsMenu={optionsMenu}
      />
      <Section.Collapsible>
        <Section.Body>Section content</Section.Body>
      </Section.Collapsible>
    </Section>
  ));

storiesOf('Components/section/list', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('section list', () => (
    <SectionList spaced={false}>
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header title="Section 1" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header title="Section 2" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
      <Section collapsible={true} defaultCollapsed={true} active={true}>
        <Section.Header title="Section 3 (active state)" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
    </SectionList>
  ))
  .add('with spacing', () => (
    <SectionList>
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header title="Section 1" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header title="Section 2" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header title="Section 3" />
        <Section.Collapsible>
          <Section.Body>Section content</Section.Body>
        </Section.Collapsible>
      </Section>
    </SectionList>
  ))
  .add('draggable list inside section', () => {
    const [list, setList] = React.useState(sectionList);
    return (
      <SectionList spaced={false}>
        <Section collapsible={true}>
          <Section.Header title="Section 1" />
          <Section.Collapsible>
            <Section.Body hasPadding={true}>
              <SectionDraggableList
                enableDragAndDrop={true}
                onDrop={(result) => {
                  if (result.destination) {
                    const item = list[result.source.index];
                    const newOrder = [...list];
                    newOrder.splice(result.source.index, 1);
                    newOrder.splice(result.destination.index, 0, item);
                    setList(newOrder);
                  }
                }}
                sections={list.map((section) => ({
                  ...section,
                  collapsible: false,
                }))}
              />
            </Section.Body>
          </Section.Collapsible>
        </Section>
      </SectionList>
    );
  })
  .add('draggable list (use in dialog)', () => {
    const [list, setList] = React.useState(sectionList);
    const [showModal, _toggleModal] = React.useState(true);
    const toggleModal = () => _toggleModal((prev) => !prev);

    return (
      <>
        <Button onClick={toggleModal}>Open Dialog</Button>
        <Dialog open={showModal}>
          <Dialog.Header
            title={'render dragged element in portal'}
            onCloseButtonClick={toggleModal}
          />
          <Dialog.Body>
            <SectionDraggableList
              usePortal={true}
              enableDragAndDrop={true}
              onDrop={(result) => {
                if (result.destination) {
                  const item = list[result.source.index];
                  const newOrder = [...list];
                  newOrder.splice(result.source.index, 1);
                  newOrder.splice(result.destination.index, 0, item);
                  setList(newOrder);
                }
              }}
              sections={list.map((section) => ({
                ...section,
                collapsible: false,
              }))}
            />
          </Dialog.Body>
        </Dialog>
      </>
    );
  })
  .add('draggable list with collapsible sections', () => {
    const [list, setList] = React.useState(sectionList);
    return (
      <>
        <SectionList spaced={false}>
          <Section collapsible={true}>
            <Section.Header
              title="Section 1"
              actions={actions}
              optionsMenu={optionsMenu}
            />
            <Section.Collapsible>
              <Section.Body hasPadding={true}>
                <TextField
                  onChange={() => {}}
                  label="Text Field"
                  placeholder="Text input"
                  type="text"
                />
              </Section.Body>
            </Section.Collapsible>
          </Section>
          <Section collapsible={true} defaultCollapsed={true}>
            <Section.Header title="Section 2" />
            <Section.Collapsible>
              <Section.Body hasPadding={true}>
                <TextField
                  onChange={() => {}}
                  label="Text Field"
                  placeholder="Text input"
                  type="text"
                />
              </Section.Body>
            </Section.Collapsible>
          </Section>
          <Section collapsible={true} defaultCollapsed={true}>
            <Section.Header title="Section 3" />
            <Section.Collapsible>
              <Section.Body hasPadding={true}>
                <TextField
                  onChange={() => {}}
                  label="Text Field"
                  placeholder="Text input"
                  type="text"
                />
              </Section.Body>
            </Section.Collapsible>
          </Section>
        </SectionList>
        <div style={{background: '#f4f4f5'}}>
          <SectionListHeader
            title="Items"
            paddingSize={select('paddingSize', PaddingSize, PaddingSize.Medium)}
            onSearch={() => action('search')}
            onAdd={() => {
              const newList = [...list];
              newList.push({
                id: `${list.length + 1}`,
                title: `Item ${list.length + 1}`,
                draggable: true,
                collapsible: true,
                paddingSize: PaddingSize.Medium,
                bodyHasPadding: true,
                content: (
                  <TextField
                    onChange={() => {}}
                    label="Text Field"
                    placeholder="Text input"
                    type="text"
                  />
                ),
                optionsMenu,
              });
              setList(newList);
            }}
          />
          <div style={{padding: '0 16px 16px'}}>
            <SectionDraggableList
              enableDragAndDrop={true}
              onDrop={(result) => {
                if (result.destination) {
                  const item = list[result.source.index];
                  const newOrder = [...list];
                  newOrder.splice(result.source.index, 1);
                  newOrder.splice(result.destination.index, 0, item);
                  setList(newOrder);
                }
              }}
              sections={list}
            />
          </div>
        </div>
      </>
    );
  })
  .add('draggable list with controllably collapsible sections', () => {
    const controllableSectionIndex = 1;
    const [collapsed, setCollapsed] = React.useState(false);
    const [list, setList] = React.useState(sectionList);

    return (
      <div style={{margin: 16}}>
        <div style={{marginBottom: 16}}>
          <Switch
            id="collapsed"
            label="Collapsed"
            checked={collapsed}
            onChange={() => setCollapsed(!collapsed)}
          />
        </div>
        <SectionDraggableList
          enableDragAndDrop={true}
          onDrop={(result) => {
            if (result.destination) {
              const item = list[result.source.index];
              const newOrder = [...list];
              newOrder.splice(result.source.index, 1);
              newOrder.splice(result.destination.index, 0, item);
              setList(newOrder);
            }
          }}
          sections={list.map((section, index) => ({
            ...section,
            collapsible: false,
            collapsed:
              index === controllableSectionIndex ? collapsed : undefined,
            onToggle:
              index === controllableSectionIndex
                ? () => {
                    setCollapsed(!collapsed);
                  }
                : undefined,
          }))}
        />
      </div>
    );
  });

storiesOf('Components/section/list header', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add(
    'section list header with custom actions, default search, add and options menu',
    () => (
      <SectionListHeader
        title="Header"
        onSearch={() => action('search')}
        onAdd={() => {}}
        onAction={action('selectAction')}
        getActions={() => [
          {
            command: 'edit',
            icon: <Icon path={pencil} />, // icon can either be function or react element
          },
        ]}
        optionsMenu={optionsMenu}
      />
    )
  )
  .add('section list header with options menu', () => (
    <SectionListHeader title="Header" optionsMenu={optionsMenu} />
  ))
  .add('section list header with options menu dynamic icon', () => (
    <SectionListHeader title="Header" optionsMenu={optionsMenuWithIcon} />
  ))
  .add(
    'section list header with options menu having helper text at menu item',
    () => (
      <SectionListHeader
        title="Header"
        optionsMenu={optionsMenuWithHelperText}
      />
    )
  )
  .add(
    'section list header with options menu having menu item disabled',
    () => (
      <SectionListHeader
        title="Header"
        optionsMenu={optionsMenuWithDisabledOption}
      />
    )
  )
  .add('section list header only with custom actions', () => (
    <SectionListHeader
      title="Header"
      onAction={action('selectAction')}
      getActions={() => [
        {
          command: 'edit',
          icon: <Icon path={pencil} />, // icon can either be function or react element
        },
      ]}
    />
  ));
