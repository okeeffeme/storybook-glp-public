import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Section, {SectionBackgroundColor} from '../../src';

describe('Section :: ', () => {
  it('should render with a header, body, footer', () => {
    render(
      <Section>
        <Section.Header title="title" />
        <Section.Body />
        <Section.Footer />
      </Section>
    );

    expect(screen.getByTestId('header-section')).toBeInTheDocument();
    expect(screen.getByTestId('section-body')).toBeInTheDocument();
    expect(screen.getByTestId('section-footer')).toBeInTheDocument();
    expect(screen.getByTestId('header-section')).toHaveTextContent('title');
  });

  it('renders collapsed section when defaultCollapsed', () => {
    render(
      <Section collapsible={true} defaultCollapsed={true}>
        <Section.Header />
        <Section.Collapsible>
          <Section.Body />
        </Section.Collapsible>
      </Section>
    );

    expect(screen.getByTestId('collapse-area-section')).toHaveStyle({
      height: '0',
    });
  });

  it('should collapse and expand Section.Collapsible when toggle is clicked for uncontrolled', () => {
    // arrange
    const handleToggle = jest.fn();

    render(
      <Section
        collapsible={true}
        onToggle={handleToggle}
        defaultCollapsed={false}>
        <Section.Header title="title" />
        <Section.Collapsible>
          <Section.Body />
        </Section.Collapsible>
      </Section>
    );
    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveStyle({
      height: 'auto',
    });
    expect(screen.getAllByTestId('collapse-area-section')[0]).not.toHaveClass(
      'comd-section__collapse-area--collapsed'
    );

    userEvent.click(screen.getByRole('button'));

    expect(handleToggle).lastCalledWith(true);
    expect(handleToggle).toHaveBeenCalledTimes(1);
    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveStyle({
      height: '0px',
    });
    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveClass(
      'comd-section__collapse-area--collapsed'
    );

    userEvent.click(screen.getByRole('button'));

    expect(handleToggle).lastCalledWith(false);
    expect(handleToggle).toHaveBeenCalledTimes(2);
    expect(screen.getAllByTestId('collapse-area-section')[0]).not.toHaveClass(
      'comd-section__collapse-area--collapsed'
    );
    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveStyle({
      height: '0px',
    });
  });

  it('should collapse when new controlled prop is given', () => {
    const handleToggle = jest.fn();

    const {rerender} = render(
      <Section collapsible={true} onToggle={handleToggle} collapsed={false}>
        <Section.Header title="title" />
        <Section.Collapsible>
          <Section.Body />
        </Section.Collapsible>
      </Section>
    );

    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveStyle({
      height: 'auto',
    });

    userEvent.click(screen.getByRole('button'));
    rerender(
      <Section collapsible={true} onToggle={handleToggle} collapsed={true}>
        <Section.Header title="title" />
        <Section.Collapsible>
          <Section.Body />
        </Section.Collapsible>
      </Section>
    );

    expect(screen.getAllByTestId('collapse-area-section')[0]).toHaveStyle({
      height: '0px',
    });
    expect(handleToggle).toHaveBeenCalled();
  });

  it('should render active modifier class when active is enabled', () => {
    render(
      <Section active={true}>
        <Section.Body />
      </Section>
    );
    expect(screen.getByTestId('internal-section')).toHaveClass(
      'comd-section--active'
    );
  });

  it('should not render active modifier class when active is disabled', () => {
    render(
      <Section active={false}>
        <Section.Body />
      </Section>
    );

    expect(screen.getByTestId('internal-section')).not.toHaveClass(
      'comd-section--active'
    );
  });

  it('should render "contrast low" background modifier by default', () => {
    render(
      <Section>
        <Section.Body />
      </Section>
    );
    expect(screen.getByTestId('internal-section')).toHaveClass(
      'comd-section--background-contrast-low'
    );
  });

  it('should render correct background modifier, if backgroundColor is specified', () => {
    render(
      <Section backgroundColor={SectionBackgroundColor.AltPrimary}>
        <Section.Body />
      </Section>
    );
    expect(screen.getByTestId('internal-section')).toHaveClass(
      'comd-section--background-alt-primary'
    );
  });

  describe('Error state :: ', () => {
    it('should set tooltip open to true when error is true', () => {
      render(
        <Section errorText="error text" error={true}>
          <Section.Body />
        </Section>
      );
      expect(screen.getByTestId('tooltipinner')).toHaveClass(
        'comd-tooltip__inner--error'
      );
    });

    it('should set tooltip open to false when error is false', () => {
      render(
        <Section errorText="error text" error={false}>
          <Section.Body />
        </Section>
      );

      expect(screen.queryByTestId('tooltipinner')).not.toBeInTheDocument();
    });

    it('should get error modifier class when error is set to true', () => {
      render(
        <Section error={true}>
          <Section.Body />
        </Section>
      );

      expect(screen.getByTestId('internal-section')).toHaveClass(
        'comd-section--error'
      );
    });

    it('should not get error modifier class when error is false', () => {
      render(
        <Section error={false}>
          <Section.Body />
        </Section>
      );

      expect(screen.getByTestId('internal-section')).not.toHaveClass(
        'comd-section--error'
      );
    });
  });
});
