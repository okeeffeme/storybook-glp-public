import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import {SectionDraggable} from '../../src/components/SectionDraggable';

const selectors = {
  optionsMenuToggle: '[data-section="options-menu-toggle"]',
  collapseArea: '[data-section="collapse-area"]',
  toggleButton: '[data-section-toggle="icon-button"]',
};

const optionName = 'myName';

type GetProps = {
  defaultCollapsed?: boolean;
};

const getProps = (props?: GetProps) => ({
  id: 'myId',
  draggableIndex: 1,
  collapsible: true,
  defaultCollapsed: props?.defaultCollapsed ?? false,
  content: <div>My content</div>,
  usePortal: false,
  optionsMenu: {
    onSelect: () => {},
    getOptions: () => [
      {
        command: 'myCommand',
        name: optionName,
      },
    ],
  },
});

const isCollapsed = (container: HTMLElement): boolean => {
  const collapseArea = container.querySelector(selectors.collapseArea)!;
  return collapseArea.className.includes('collapsed');
};

describe('SectionDraggable ', () => {
  describe('should not collapse expanded body, ', () => {
    const props = getProps();

    it('if click on options menu button in header', () => {
      const {container} = render(<SectionDraggable {...props} />);
      const optionsMenuButton = container.querySelector(
        selectors.optionsMenuToggle
      )!;

      userEvent.click(optionsMenuButton);

      expect(isCollapsed(container)).toBeFalsy();
    });

    it('if click on option in header', () => {
      const {container, getByText} = render(<SectionDraggable {...props} />);
      const optionsMenuButton = container.querySelector(
        selectors.optionsMenuToggle
      )!;

      userEvent.click(optionsMenuButton);
      const option = getByText(optionName);
      userEvent.click(option);

      expect(isCollapsed(container)).toBeFalsy();
    });
  });

  describe('should not expand collapsed body, ', () => {
    const props = getProps({defaultCollapsed: true});

    it('if click on options menu button in header', () => {
      const {container} = render(<SectionDraggable {...props} />);
      const optionsMenuButton = container.querySelector(
        selectors.optionsMenuToggle
      )!;

      userEvent.click(optionsMenuButton);

      expect(isCollapsed(container)).toBeTruthy();
    });

    it('if click on option in header', () => {
      const {container, getByText} = render(<SectionDraggable {...props} />);
      const optionsMenuButton = container.querySelector(
        selectors.optionsMenuToggle
      )!;

      userEvent.click(optionsMenuButton);
      const option = getByText(optionName);
      userEvent.click(option);

      expect(isCollapsed(container)).toBeTruthy();
    });
  });

  describe('when toggle button is clicked, ', () => {
    it('collapses expanded body', () => {
      const props = getProps();
      const {container} = render(<SectionDraggable {...props} />);
      const toggleButton = container.querySelector(selectors.toggleButton)!;

      userEvent.click(toggleButton);

      expect(isCollapsed(container)).toBeTruthy();
    });

    it('expands collapsed body', () => {
      const props = getProps({defaultCollapsed: true});
      const {container} = render(<SectionDraggable {...props} />);
      const toggleButton = container.querySelector(selectors.toggleButton)!;

      userEvent.click(toggleButton);

      expect(isCollapsed(container)).toBeFalsy();
    });
  });
});
