import React from 'react';
import {render} from '@testing-library/react';

import {InternalSection} from '../../src/components/InternalSection';

const selectors = {
  portal: '[class="co-portal"]',
  content: '[data-section="section"]',
};

const contentText = 'My content';

const getProps = (props) => ({
  collapsible: false,
  defaultCollapsed: props?.defaultCollapsed ?? false,
  children: <div>{contentText}</div>,
  usePortal: false,
  snapshot: {
    isDragging: false,
  },
  ...props,
});

describe('InternalSection ', () => {
  describe('render in portal, ', () => {
    it('it should render in portal', () => {
      const props = getProps({usePortal: true, snapshot: {isDragging: true}});
      const {container} = render(<InternalSection {...props} />, {
        container: document.body,
      });
      const portal = container.querySelector(selectors.portal);

      expect(portal?.textContent).toBe(contentText);
    });
    it('it should not render in portal when it is not dragging', () => {
      const props = getProps({usePortal: true, snapshot: {isDragging: false}});
      const {container} = render(<InternalSection {...props} />, {
        container: document.body,
      });
      const portal = container.querySelector(selectors.portal);
      const content = container.querySelector(selectors.content);

      expect(portal).toBeFalsy();
      expect(content?.textContent).toBe(contentText);
    });
    it('it should not render in portal when usePortal is false', () => {
      const props = getProps({usePortal: false, snapshot: {isDragging: true}});
      const {container} = render(<InternalSection {...props} />, {
        container: document.body,
      });
      const portal = container.querySelector(selectors.portal);
      const content = container.querySelector(selectors.content);

      expect(portal).toBeFalsy();
      expect(content?.textContent).toBe(contentText);
    });
  });
});
