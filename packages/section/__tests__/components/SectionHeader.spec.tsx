import React from 'react';
import {render, screen} from '@testing-library/react';
import Section, {PaddingSize} from '../../src';
import Icon, {pencil, chevronDown, chevronUp} from '../../../icons/src/index';
import userEvent from '@testing-library/user-event';

describe('Section Header :: ', () => {
  it('should render options menu when optionsMenu is defined', () => {
    render(
      <Section.Header
        optionsMenu={{
          onSelect: jest.fn(),
          // eslint-disable-next-line react/display-name
          getOptions: () => [
            {
              iconPath: pencil,
              command: 'edit',
              name: 'Edit',
            },
          ],
        }}
      />
    );
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should not render options menu when optionsMenu is not defined', () => {
    render(
      <Section>
        <Section.Header />
      </Section>
    );

    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should add padding-medium class modifier by default', () => {
    render(
      <Section>
        <Section.Header />
      </Section>
    );

    expect(screen.getByTestId('header-section')).toHaveClass(
      'comd-section__header-container--padding-medium'
    );
    expect(screen.getByTestId('header-section')).not.toHaveClass(
      'comd-section__header-container--padding-large'
    );
  });

  it('should add padding-large class modifier when paddingSize large is set', () => {
    render(
      <Section paddingSize={PaddingSize.Large}>
        <Section.Header />
      </Section>
    );
    expect(screen.getByTestId('header-section')).not.toHaveClass(
      'comd-section__header-container--padding-medium'
    );
    expect(screen.getByTestId('header-section')).toHaveClass(
      'comd-section__header-container--padding-large'
    );
  });

  it('should have no padding class modifier when hasPadding is false', () => {
    render(
      <Section>
        <Section.Header hasPadding={false} />
      </Section>
    );

    const header = screen.getByTestId('header-section');

    expect(header).not.toHaveClass(
      'comd-section__header-container--padding-medium'
    );
    expect(header).not.toHaveClass(
      'comd-section__header-container--padding-large'
    );
  });

  it('should render actions when actions is defined', () => {
    render(
      <Section collapsible={true}>
        <Section.Header
          actions={{
            onAction: jest.fn(),
            // eslint-disable-next-line react/display-name
            getActions: () => [
              {
                icon: <Icon path={pencil} />,
                command: 'edit',
                name: 'Edit',
              },
            ],
          }}
        />
      </Section>
    );

    expect(screen.getAllByRole('button')[0]).toHaveAttribute(
      'data-section-action',
      'edit'
    );
  });

  it('should call actions.onSelect when action is clicked', () => {
    const handleAction = jest.fn();

    render(
      <Section collapsible={true}>
        <Section.Header
          actions={{
            onAction: handleAction,
            // eslint-disable-next-line react/display-name
            getActions: () => [
              {
                icon: <Icon path={pencil} />,
                command: 'edit',
                name: 'Edit',
              },
            ],
          }}
        />
      </Section>
    );
    const actionButton = screen.getAllByRole('button')[0];
    userEvent.click(actionButton);

    expect(handleAction).toHaveBeenCalledWith('edit');
  });

  it('should render children in header', () => {
    render(
      <Section>
        <Section.Header>
          <div data-testid="header-child" />
        </Section.Header>
      </Section>
    );
    expect(screen.getByTestId('header-child')).toBeInTheDocument();
  });

  describe('Collapsible :: ', () => {
    it('should render a toggle button when collapsible is true', () => {
      render(
        <Section collapsible={true}>
          <Section.Header />
        </Section>
      );

      expect(screen.getByRole('button')).toHaveAttribute(
        'data-section-toggle',
        'icon-button'
      );
    });

    it('should not render a toggle button when collapsible is not set', () => {
      render(
        <Section>
          <Section.Header />
        </Section>
      );

      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should not add collapsed class modifier when collapsed is set', () => {
      render(
        <Section>
          <Section.Header />
        </Section>
      );
      expect(screen.getByTestId('header-section')).not.toHaveClass(
        'comd-section__header-container--collapsed'
      );
    });

    it('should add collapsed class modifier when collapsed true is set', () => {
      render(
        <Section collapsed={true}>
          <Section.Header />
        </Section>
      );
      expect(screen.getByTestId('header-section')).toHaveClass(
        'comd-section__header-container--collapsed'
      );
    });

    it('should render chevronDown when collapsed=true and collapsible=true', () => {
      render(
        <Section collapsed={true} collapsible={true}>
          <Section.Header />
        </Section>
      );

      expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
        'd',
        chevronDown
      );
    });

    it('should render chevronDown when collapsed=false and collapsible=true', () => {
      render(
        <Section collapsed={false} collapsible={true}>
          <Section.Header />
        </Section>
      );
      expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
        'd',
        chevronUp
      );
    });
  });
});
