import React from 'react';
import {render, screen} from '@testing-library/react';
import Section, {PaddingSize} from '../../src';

describe('Section Body :: ', () => {
  it('should not add padding class modifiers by default', () => {
    render(
      <Section>
        <Section.Body />
      </Section>
    );

    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-medium'
    );
    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-large'
    );
  });

  it('should not add header-present modifier class when no header is present', () => {
    render(
      <Section>
        <Section.Body />
      </Section>
    );

    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--header-present'
    );
  });

  it('should add header-present modifier class when header is present', () => {
    render(
      <Section>
        <Section.Header />
        <Section.Body />
      </Section>
    );
    expect(screen.getByTestId('section-body')).toHaveClass(
      'comd-section__body--header-present'
    );
  });

  it('should add header-present modifier class when header is present, and wrapped in a container', () => {
    render(
      <Section>
        <div>
          <Section.Header />
        </div>
        <Section.Body />
      </Section>
    );

    expect(screen.getByTestId('section-body')).toHaveClass(
      'comd-section__body--header-present'
    );
  });

  it('should add default padding class modifier when hasPadding is true', () => {
    render(
      <Section>
        <Section.Body hasPadding={true} />
      </Section>
    );

    expect(screen.getByTestId('section-body')).toHaveClass(
      'comd-section__body--padding-medium'
    );
    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-large'
    );
  });

  it('should add large padding class modifier when hasPadding is true and paddingSize large is set on Section', () => {
    render(
      <Section paddingSize={PaddingSize.Large}>
        <Section.Body hasPadding={true} />
      </Section>
    );
    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-medium'
    );
    expect(screen.getByTestId('section-body')).toHaveClass(
      'comd-section__body--padding-large'
    );
  });

  it('should not add large padding class modifier when hasPadding is false and paddingSize large is set on Section', () => {
    render(
      <Section paddingSize={PaddingSize.Large}>
        <Section.Body hasPadding={false} />
      </Section>
    );
    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-medium'
    );
    expect(screen.getByTestId('section-body')).not.toHaveClass(
      'comd-section__body--padding-large'
    );
  });
});
