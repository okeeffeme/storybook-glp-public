import React from 'react';
import {render, screen} from '@testing-library/react';
import Section, {SectionList} from '../../src';

describe('Section List component', () => {
  it('should render sections', () => {
    render(
      <SectionList>
        <Section />
      </SectionList>
    );
    expect(screen.getByTestId('section-list')).toBeInTheDocument();
    expect(screen.getByTestId('internal-section')).toBeInTheDocument();
  });
  it('should render sections with array.map', () => {
    const items = ['1', '2', '3', '4'];
    const panels = items.map((i) => <Section key={i} />);
    render(<SectionList>{panels}</SectionList>);
    expect(screen.getByTestId('section-list')).toBeInTheDocument();
    expect(screen.getAllByTestId('internal-section')[0]).toBeInTheDocument();
  });
});
