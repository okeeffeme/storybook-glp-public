import React from 'react';
import {render, screen} from '@testing-library/react';
import {SectionListHeader, PaddingSize} from '../../src';
import Icon, {pencil} from '../../../icons/src';
import user from '@testing-library/user-event';

describe('Section ListHeader component', () => {
  const getActions = () => [
    {
      command: 'edit',
      icon: <Icon path={pencil} />,
    },
  ];

  it('should render list header', () => {
    render(<SectionListHeader title="title" />);
    const header = screen.getByTestId('header-section');
    expect(header).toHaveTextContent('title');
  });

  it('should render add button when onAdd is defined', () => {
    render(<SectionListHeader title="title" onAdd={jest.fn()} />);
    const addButton = screen.getByRole('button');
    expect(addButton).toHaveAttribute('data-section-list-action', 'add');
  });

  it('should not render header-icons-container if no actions are passed', () => {
    render(<SectionListHeader title="title" />);
    const addButton = screen.queryByRole('button');
    expect(addButton).not.toBeInTheDocument();
  });

  it('should call onAdd when add button is clicked', () => {
    const handleAdd = jest.fn();
    render(<SectionListHeader title="title" onAdd={handleAdd} />);

    const addButton = screen.getByRole('button');
    user.click(addButton);
    expect(handleAdd).toHaveBeenCalled();
  });

  it('should render add button when onAdd is defined', () => {
    render(<SectionListHeader title="title" onSearch={jest.fn()} />);

    const searchButton = screen.getByRole('button');
    expect(searchButton).toHaveAttribute('data-search-toggle-icon', 'true');
  });

  it('should render optionsMenu when optionsMenu is defined', () => {
    render(
      <SectionListHeader
        title="title"
        onSearch={jest.fn()}
        optionsMenu={{
          onSelect: () => console.log('selectMenuItem'),
          getOptions: () => [
            {
              command: 'delete',
              name: 'Delete',
            },
          ],
        }}
      />
    );
    expect(screen.getByTestId('options-menu')).toBeInTheDocument();
  });

  it('should not render optionsMenu when optionsMenu is not defined', () => {
    render(<SectionListHeader title="title" onSearch={jest.fn()} />);

    expect(screen.queryByTestId('options-menu')).not.toBeInTheDocument();
  });

  it('should render edit button when getActions and onAction are defined', () => {
    render(
      <SectionListHeader
        title="title"
        onAction={jest.fn}
        getActions={getActions}
      />
    );
    expect(screen.getByRole('button')).toHaveAttribute(
      'data-section-action',
      'edit'
    );
  });

  it('should render edit button and onAdd when onAdd, getActions, onAction are defined', () => {
    render(
      <SectionListHeader
        title="title"
        onAdd={jest.fn()}
        onAction={jest.fn}
        getActions={getActions}
      />
    );
    const button = screen.getAllByRole('button');
    expect(button[0]).toHaveAttribute('data-section-action', 'edit');
    expect(button[1]).toHaveAttribute('data-section-list-action', 'add');
  });

  it('should call edit button when custom actions provide it', () => {
    const handleEdit = jest.fn();
    render(
      <SectionListHeader
        title="title"
        onAction={handleEdit}
        getActions={getActions}
      />
    );

    const addButton = screen.getByRole('button');
    user.click(addButton);
    expect(handleEdit).toHaveBeenCalled();
  });

  it('should add padding-medium modifier when paddingSize is not set', () => {
    render(<SectionListHeader />);
    const header = screen.getByTestId('header-section');
    expect(header).toHaveClass(
      'comd-section__header-container--padding-medium'
    );
    expect(header).not.toHaveClass(
      'comd-section__header-container--padding-large'
    );
  });

  it('should add padding-large modifier when paddingSize is set to large', () => {
    render(<SectionListHeader paddingSize={PaddingSize.Large} />);
    const header = screen.getByTestId('header-section');
    expect(header).not.toHaveClass(
      'comd-section__header-container--padding-medium'
    );
    expect(header).toHaveClass('comd-section__header-container--padding-large');
  });
});
