import React from 'react';
import {render, screen} from '@testing-library/react';
import Section, {PaddingSize} from '../../src';

describe('Section Footer :: ', () => {
  it('should render children', () => {
    render(
      <Section>
        <Section.Footer>
          <div data-testid="footer-children" />
        </Section.Footer>
      </Section>
    );
    expect(screen.getByTestId('footer-children')).toBeInTheDocument();
  });

  it('should add padding-medium class modifier by default', () => {
    render(
      <Section>
        <Section.Footer />
      </Section>
    );

    expect(screen.getByTestId('section-footer')).toHaveClass(
      'comd-section__footer--padding-medium'
    );
    expect(screen.getByTestId('section-footer')).not.toHaveClass(
      'comd-section__footer--padding-large'
    );
  });

  it('should add padding-large class modifier when paddingSize large is set on context', () => {
    render(
      <Section paddingSize={PaddingSize.Large}>
        <Section.Footer />
      </Section>
    );

    expect(screen.getByTestId('section-footer')).not.toHaveClass(
      'comd-section__footer--padding-medium'
    );
    expect(screen.getByTestId('section-footer')).toHaveClass(
      'comd-section__footer--padding-large'
    );
  });

  it('should have no padding class modifier when hasPadding is false', () => {
    render(
      <Section>
        <Section.Footer hasPadding={false} />
      </Section>
    );

    const header = screen.getByTestId('section-footer');

    expect(header).not.toHaveClass(
      'comd-section__footer-container--padding-medium'
    );
    expect(header).not.toHaveClass(
      'comd-section__footer-container--padding-large'
    );
  });
});
