import React from 'react';
import {action} from '@storybook/addon-actions';
import {render, screen} from '@testing-library/react';
import OptionsMenu from '../../src/components/SectionOptionsMenu';
import {dotsVertical, plus, trashBin} from '../../../icons/src';

describe('Section Options Menu component', () => {
  const optionsMenuProps = {
    onSelect: action('selectMenuItem'),
    getOptions: () => [
      {
        command: 'delete',
        name: 'Delete',
        iconPath: trashBin,
      },
    ],
  };

  it('should render options menu', () => {
    render(<OptionsMenu {...optionsMenuProps} />);
    expect(screen.getByTestId('options-menu')).toBeInTheDocument();
    expect(screen.getByRole('button')).toHaveAttribute(
      'data-section',
      'options-menu-toggle'
    );
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('should render default iconPath', () => {
    render(<OptionsMenu {...optionsMenuProps} />);
    const icon = screen.getByTestId('icon');
    expect(icon).toBeInTheDocument();
    expect(icon.querySelector('path')).toHaveAttribute('d', dotsVertical);
  });

  it('should render iconPath if passed in prop', () => {
    const newProps = {
      ...optionsMenuProps,
      iconPath: plus,
    };
    render(<OptionsMenu {...newProps} />);
    const icon = screen.getByTestId('icon');
    expect(icon).toBeInTheDocument();
    expect(icon.querySelector('path')).toHaveAttribute('d', plus);
  });
});
