import React from 'react';
import {render, screen} from '@testing-library/react';
import {SectionDraggableList, PaddingSize} from '../../src';

const sectionList = [
  {
    id: '1',
    title: 'Item 1',
    draggable: false,
    collapsible: true,
    defaultCollapsed: false,
    content: <div>content</div>,
  },
  {
    id: '2',
    title: 'Item 2',
    draggable: false,
    collapsible: true,
    content: <div>content</div>,
  },
  {
    id: '3',
    title: 'Item 3',
    draggable: false,
    collapsible: true,
    content: <div>content</div>,
  },
];

describe('Section DraggableList :: ', () => {
  it('should render sections', () => {
    render(<SectionDraggableList onDrop={() => {}} sections={sectionList} />);
    expect(screen.getByTestId('section-list')).toBeInTheDocument();
    expect(screen.getAllByTestId('internal-section')).toHaveLength(
      sectionList.length
    );
  });

  describe('Error state :: ', () => {
    it('should get error modifier class when error is set to true', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          error: true,
          errorText: 'error text',
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);

      const section = screen.getByTestId('internal-section');
      expect(section).toHaveClass('comd-section--error');
    });

    it('should not get error modifier class when error is false', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          error: false,
          errorText: 'error text',
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);

      const section = screen.getByTestId('internal-section');
      expect(section).not.toHaveClass('comd-section--error');
    });

    it('should set tooltip open to false when error is false', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          error: false,
          errorText: 'error text',
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);
      expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
    });

    it('should set tooltip open to true when error is true', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          error: true,
          errorText: 'error text',
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);
      expect(screen.getByTestId('tooltipinner')).toBeInTheDocument();
    });
  });

  describe('paddingSize and bodyHasPadding :: ', () => {
    it('should add padding-medium class modifier on Section Header by default', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);

      const header = screen.getByTestId('header-section');

      expect(header).toHaveClass(
        'comd-section__header-container--padding-medium'
      );
      expect(header).not.toHaveClass(
        'comd-section__header-container--padding-large'
      );
    });

    it('should add padding-medium class modifier on Section Header and Body when bodyHasPadding is true', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          bodyHasPadding: true,
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);

      const header = screen.getByTestId('header-section');
      const body = screen.getByTestId('section-body');

      expect(header).toHaveClass(
        'comd-section__header-container--padding-medium'
      );
      expect(header).not.toHaveClass(
        'comd-section__header-container--padding-large'
      );

      expect(body).toHaveClass('comd-section__body--padding-medium');
      expect(body).not.toHaveClass('comd-section__body--padding-large');
    });

    it('should add padding-large class modifier on Section Header and Body when bodyHasPadding is true and paddingSize is large', () => {
      const list = [
        {
          id: '1',
          title: 'Item 1',
          draggable: false,
          collapsible: true,
          defaultCollapsed: false,
          paddingSize: PaddingSize.Large,
          bodyHasPadding: true,
          content: <div>content</div>,
        },
      ];
      render(<SectionDraggableList onDrop={() => {}} sections={list} />);

      const header = screen.getByTestId('header-section');
      const body = screen.getByTestId('section-body');

      expect(header).not.toHaveClass(
        'comd-section__header-container--padding-medium'
      );
      expect(header).toHaveClass(
        'comd-section__header-container--padding-large'
      );

      expect(body).not.toHaveClass('comd-section__body--padding-medium');
      expect(body).toHaveClass('comd-section__body--padding-large');
    });
  });
});
