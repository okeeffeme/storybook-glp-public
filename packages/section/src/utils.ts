import {deprecationNotice} from '@jotunheim/react-utils';

export const deprecateClassName = (className?: string) => {
  if (className) {
    deprecationNotice(
      `@confirmit/react-section: "className" prop is deprecated, and will be removed in the future. "className" value was "${className}". If needed for spacing/positioning, move class to a wrapper instead.`
    );
  }
};

export const baseClassName = 'comd-section';
