import {ReactNode} from 'react';
import {DraggableId} from 'react-beautiful-dnd';

export type Action = {
  command: string;
  icon?: ReactNode;
  tooltip?: string;
};

export type Actions = {
  onAction: (command: string) => void;
  getActions: () => Array<Action>;
};

export type SectionOptionProps = {
  command: string;
  name: string;
  disabled?: boolean;
  iconPath?: string;
  helperText?: ReactNode;
};

export type SectionOptionsMenuProps = {
  onSelect: (command: string) => void;
  getOptions: () => Array<SectionOptionProps>;
  iconPath?: string;
};

export type SectionProps = {
  defaultCollapsed?: boolean;
  collapsible?: boolean;
  paddingSize?: PaddingSize;
  backgroundColor?: SectionBackgroundColor;
  error?: boolean;
  errorText?: ReactNode;
  children?: ReactNode;
  collapsed?: boolean;
  onToggle?: (isCollapsed: boolean) => void;
  active?: boolean;
};

export type SectionDraggableProps = {
  disableDrag?: boolean;
  draggable?: boolean;
} & SectionProps & {
    id: DraggableId;
    bodyHasPadding?: boolean;
    content?: ReactNode;
    name?: string;
    title?: ReactNode;
    actions?: Actions;
    optionsMenu?: SectionOptionsMenuProps;
  };

export enum PaddingSize {
  Medium = 'medium',
  Large = 'large',
}

export enum SectionBackgroundColor {
  AltPrimary = 'alt-primary',
  ContrastLow = 'contrast-low',
}
