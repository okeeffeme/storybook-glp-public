import React, {ReactNode} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import SectionContext from './SectionContext';
import {baseClassName} from '../utils';

import classNames from './Section.module.css';

const {element} = bemFactory({baseClassName, classNames});

type SectionBodyProps = {
  children?: ReactNode;
  hasPadding?: boolean;
};

export const SectionBody = ({
  children,
  hasPadding = false,
  ...rest
}: SectionBodyProps) => {
  const {paddingSize, hasHeader} = React.useContext(SectionContext);

  return (
    <div
      className={cn(element('body'), {
        [element('body', `padding-${paddingSize}`)]: hasPadding,
        [element('body', `header-present`)]: hasHeader,
      })}
      data-section="body"
      data-testid="section-body"
      {...extractDataAndAriaProps(rest)}>
      {children}
    </div>
  );
};

export default SectionBody;
