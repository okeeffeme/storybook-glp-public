import React, {MouseEvent} from 'react';

import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton} from '@jotunheim/react-button';

import {Actions, Action} from '../types';

const stopClick = (e: MouseEvent) => {
  e.preventDefault();
  e.stopPropagation();
};

type SectionActionProps = Action & {onAction?: Actions['onAction']};

export const SectionAction = ({
  command,
  icon,
  tooltip,
  onAction,
}: SectionActionProps) => {
  const handleClick = (e: MouseEvent) => {
    stopClick(e);
    onAction && onAction(command);
  };

  return (
    <Tooltip content={tooltip}>
      <IconButton onClick={handleClick} data-section-action={command}>
        {icon}
      </IconButton>
    </Tooltip>
  );
};
