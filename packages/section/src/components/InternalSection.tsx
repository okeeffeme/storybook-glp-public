import React, {useMemo, ReactNode, useEffect} from 'react';
import {useUncontrolledProp} from 'uncontrollable';
import cn from 'classnames';
import {
  DraggableProvidedDraggableProps,
  DraggableStateSnapshot,
} from 'react-beautiful-dnd';

import {usePortal} from '@jotunheim/react-contexts';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Tooltip} from '@jotunheim/react-tooltip';
import {PopperRefInstance, TriggerRef} from '@jotunheim/react-portal-trigger';
import {transitions} from '@jotunheim/global-styles';

import {baseClassName} from '../utils';
import {PaddingSize, SectionBackgroundColor, SectionProps} from '../types';
import SectionContext from './SectionContext';
import SectionHeader from './SectionHeader';

import classNames from './Section.module.css';

type InternalSectionProps = SectionProps & {
  innerRef?: TriggerRef;
  draggableProps?: DraggableProvidedDraggableProps | Record<string, never>;
  usePortal?: boolean;
  snapshot?: DraggableStateSnapshot;
};

const checkIfHeaderIsPresent = (children: ReactNode) => {
  return React.Children.toArray(children).some((child) => {
    if (React.isValidElement(child)) {
      if (child.type === SectionHeader) {
        return true;
      }

      if (child.props?.children) {
        return checkIfHeaderIsPresent(child.props.children);
      }
    }

    return false;
  });
};

const {block, modifier} = bemFactory({baseClassName, classNames});

export const InternalSection = ({
  defaultCollapsed,
  collapsible,
  paddingSize = PaddingSize.Medium,
  backgroundColor = SectionBackgroundColor.ContrastLow,
  error = false,
  errorText,
  innerRef = undefined,
  draggableProps = {},
  children,
  active,
  snapshot,
  usePortal: _usePortal = false,
  ...rest
}: InternalSectionProps) => {
  const popperRef = React.useRef<PopperRefInstance>(null);

  const isDragging = !!snapshot?.isDragging;
  const renderInPortal = _usePortal && isDragging;
  const {
    renderInPortal: render,
    mountPortalElement,
    unmountPortalElement,
  } = usePortal();

  useEffect(() => {
    if (renderInPortal) {
      mountPortalElement();
      return () => {
        unmountPortalElement();
      };
    }
  }, [renderInPortal, mountPortalElement, unmountPortalElement]);

  const [collapsed, onToggle] = useUncontrolledProp(
    rest.collapsed,
    defaultCollapsed,
    rest.onToggle
  );

  const sectionContext = useMemo(
    () => ({
      paddingSize,
      collapsed,
      collapsible,
      hasHeader: checkIfHeaderIsPresent(children),
      onToggle: () => {
        onToggle(!collapsed);
      },
    }),
    [paddingSize, collapsible, collapsed, onToggle, children]
  );

  React.useLayoutEffect(() => {
    // This will make sure the tooltip is re-rendered when section is moved
    // There is a transition of 200ms during expand/collapse, so need to wait before updating position
    setTimeout(() => {
      popperRef?.current?.forceUpdate();
    }, transitions.EnterDuration);
  });

  const classes = cn(block(), {
    [modifier('active')]: active,
    [modifier('collapsed')]: collapsed,
    [modifier('error')]: error,
    [modifier(`background-${backgroundColor}`)]: true,
  });

  const child = (
    <SectionContext.Provider value={sectionContext}>
      <Tooltip
        ref={innerRef}
        popperRef={popperRef}
        content={errorText}
        open={Boolean(error)}
        data-testid="internal-section-tooltip"
        type={'error'}>
        <div
          className={classes}
          data-testid="internal-section"
          data-section="section"
          data-section-active={Boolean(active)}
          data-section-collapsed={Boolean(collapsed)}
          data-section-error={Boolean(error)}
          {...draggableProps}
          {...extractDataAriaIdProps(rest)}>
          {children}
        </div>
      </Tooltip>
    </SectionContext.Provider>
  );

  if (!renderInPortal) {
    return child;
  }

  return render(child);
};
