import React, {useContext, useRef, ReactNode} from 'react';
import cn from 'classnames';
import {Transition} from 'react-transition-group';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import SectionContext from './SectionContext';

import {baseClassName} from '../utils';

import classNames from './Section.module.css';

const {element} = bemFactory({baseClassName, classNames});

const COLLAPSED_HEIGHT = '0';
const EXPANDED_HEIGHT = 'auto';

type SectionCollapsibleProps = {
  children?: ReactNode;
};

const SectionCollapsible = ({children, ...rest}: SectionCollapsibleProps) => {
  const {collapsed} = useContext(SectionContext);

  const collapseArea = useRef<HTMLDivElement>(null);
  const [height, setHeight] = React.useState(
    collapsed ? COLLAPSED_HEIGHT : EXPANDED_HEIGHT
  );

  const classes = cn(element('collapse-area'), {
    [element('collapse-area', 'collapsed')]: collapsed,
  });

  const setAutoHeight = () => {
    setHeight(EXPANDED_HEIGHT);
  };

  const setZeroHeight = () => {
    // Add a small timeout to avoid batch update of state
    setTimeout(() => {
      setHeight(COLLAPSED_HEIGHT);
    });
  };

  const setScrollHeight = () => {
    // Set the height to what the calculated height of the panel would be, to be able to transition current height to a new height
    if (collapseArea.current) {
      setHeight(`${collapseArea.current.scrollHeight}px`);
    }
  };

  return (
    <Transition
      nodeRef={collapseArea}
      in={!collapsed}
      onEnter={setScrollHeight}
      onEntered={setAutoHeight}
      onExit={setScrollHeight}
      onExiting={setZeroHeight}
      addEndListener={(node: HTMLElement | (() => void), done?: () => void) => {
        /*
        Backwards compatibility with react-transition-group < 4.4.0.
        If react-transition-group < 4.4.0 is installed the node is passed as a first argument here.
        If react-transition-group >= 4.4.0 is installed only the done argument is passed when setting a nodeRef.
         */
        if (typeof node === 'function') {
          done = node;
          /* eslint-disable-next-line */
          /* @ts-ignore TS will complain about this, but as mentioned above it is needed for backwards compatibility. */
          node = collapseArea.current;
        }

        // use the css transitionend event to mark the finish of a transition
        (node as HTMLElement).addEventListener(
          'transitionend',
          function _done() {
            done?.();
            (node as HTMLElement).removeEventListener(
              'transitionend',
              _done,
              false
            );
          },
          false
        );
      }}>
      <div
        ref={collapseArea}
        className={classes}
        style={{height}}
        data-section="collapse-area"
        data-testid="collapse-area-section"
        {...extractDataAndAriaProps(rest)}>
        {children}
      </div>
    </Transition>
  );
};

export default SectionCollapsible;
