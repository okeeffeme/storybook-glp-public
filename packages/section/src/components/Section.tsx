import React from 'react';

import Header from './SectionHeader';
import Collapsible from './SectionCollapsible';
import Body from './SectionBody';
import Footer from './SectionFooter';
import {InternalSection} from './InternalSection';

import {SectionProps} from '../types';

const Section = ({
  defaultCollapsed,
  collapsible,
  paddingSize,
  backgroundColor,
  error,
  errorText,
  children,
  ...rest
}: SectionProps) => {
  return (
    <InternalSection
      defaultCollapsed={defaultCollapsed}
      collapsible={collapsible}
      paddingSize={paddingSize}
      backgroundColor={backgroundColor}
      error={error}
      errorText={errorText}
      {...rest}>
      {children}
    </InternalSection>
  );
};

// DraggableList is not exported to allow tree shaking when react-beautiful-dnd is not to be used

type Module = typeof Section & {
  Header: typeof Header;
  Body: typeof Body;
  Footer: typeof Footer;
  Collapsible: typeof Collapsible;
};

const module: Module = Section as Module;

module.Header = Header;
module.Body = Body;
module.Footer = Footer;
module.Collapsible = Collapsible;

export default module;
