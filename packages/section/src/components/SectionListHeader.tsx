import React, {useMemo, ReactNode} from 'react';

import {IconButton} from '@jotunheim/react-button';
import Icon, {plus} from '@jotunheim/react-icons';
import Search from '@jotunheim/react-search-field';

import SectionContext from './SectionContext';
import {SectionHeader} from './SectionHeader';
import {SectionAction} from './SectionAction';
import {PaddingSize, SectionOptionsMenuProps, Actions} from '../types';

type SectionListHeaderProps = {
  title?: ReactNode;
  onAdd?: () => void;
  onSearch?: () => void;
  searchValue?: string;
  onAction?: Actions['onAction'];
  getActions?: Actions['getActions'];
  optionsMenu?: SectionOptionsMenuProps;
  paddingSize?: PaddingSize;
};

export const SectionListHeader = ({
  title,
  onAdd,
  onSearch,
  searchValue = '',
  onAction,
  getActions,
  optionsMenu,
  paddingSize = PaddingSize.Medium,
  ...rest
}: SectionListHeaderProps) => {
  const sectionContext = useMemo(
    () => ({
      paddingSize,
      onToggle: () => {},
      hasHeader: true,
    }),
    [paddingSize]
  );

  const actions = getActions && getActions();

  return (
    <SectionContext.Provider value={sectionContext}>
      <SectionHeader
        title={title}
        data-section="list-header"
        optionsMenu={optionsMenu}
        {...rest}>
        <>
          {onAction &&
            // actions should really be passed with the actions prop to SectionHeader, but
            // because onSearch and onAdd should be between actions and options menu,
            // we have to do it this way, until we can make a breaking change and reorder it (Need UX verification).
            // If making a breaking change for this, could also consider to align the API for actions between the two headers
            // actions prop with onAction and getActions methods, vs separate onAction and getAction props.
            actions &&
            actions.map((action) => {
              return (
                <SectionAction
                  key={action.command}
                  command={action.command}
                  icon={action.icon}
                  tooltip={action.tooltip}
                  onAction={onAction}
                />
              );
            })}
          {onSearch && (
            <Search
              value={searchValue}
              onChange={onSearch}
              data-section="search"
            />
          )}
          {onAdd && (
            <IconButton onClick={onAdd} data-section-list-action="add">
              <Icon path={plus} />
            </IconButton>
          )}
        </>
      </SectionHeader>
    </SectionContext.Provider>
  );
};

export default SectionListHeader;
