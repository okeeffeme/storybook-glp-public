import React from 'react';

import {Dropdown} from '@jotunheim/react-dropdown';
import {IconButton} from '@jotunheim/react-button';
import Icon, {dotsVertical} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import {Tooltip, PlacementTypes} from '@jotunheim/react-tooltip';

import {SectionOptionsMenuProps} from '../types';
import {baseClassName} from '../utils';

import classNames from './Section.module.css';

const {element} = bemFactory({baseClassName, classNames});

const stopClick = (e) => {
  //Stop propagation to prevent the onClick handler from being triggered in SectionHeader
  e.stopPropagation();
};

export const SectionOptionsMenu = ({
  onSelect,
  getOptions,
  iconPath = dotsVertical,
}: SectionOptionsMenuProps) => {
  if (!getOptions || !onSelect) return null;

  const options = getOptions();

  return (
    <Dropdown
      placement="bottom-end"
      data-testid="options-menu"
      menu={
        <Dropdown.Menu data-section="options-menu">
          {options.map((option) => {
            const handleSelect = (e) => {
              onSelect(option.command);
              stopClick(e);
            };

            return (
              <Dropdown.MenuItem
                data-section-menu-item={option.command}
                onClick={handleSelect}
                key={option.command}
                iconPath={option.iconPath}
                disabled={!!option.disabled}>
                <div className={element('menu-item')}>
                  <Tooltip
                    content={option.helperText}
                    placement={PlacementTypes.auto}>
                    <span>{option.name}</span>
                  </Tooltip>
                </div>
              </Dropdown.MenuItem>
            );
          })}
        </Dropdown.Menu>
      }>
      <IconButton onClick={stopClick} data-section="options-menu-toggle">
        <Icon path={iconPath} />
      </IconButton>
    </Dropdown>
  );
};

export default SectionOptionsMenu;
