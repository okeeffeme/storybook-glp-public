import {createContext} from 'react';

import {PaddingSize} from '../types';

type SectionContext = {
  collapsed?: boolean;
  collapsible?: boolean;
  hasHeader: boolean;
  paddingSize: PaddingSize;
  onToggle: () => void;
};

export default createContext<SectionContext>({
  collapsed: false,
  collapsible: false,
  hasHeader: false,
  paddingSize: PaddingSize.Medium,
  onToggle() {},
});
