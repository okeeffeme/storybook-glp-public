import React from 'react';
import {Draggable} from 'react-beautiful-dnd';

import {bemFactory} from '@jotunheim/react-themes';
import Icon, {dragVertical} from '@jotunheim/react-icons';

import {InternalSection} from './InternalSection';
import Header from './SectionHeader';
import Collapsible from './SectionCollapsible';
import Body from './SectionBody';
import {baseClassName} from '../utils';

import {SectionDraggableProps} from '../types';

import classNames from './Section.module.css';

type SectionDraggableInternalProps = SectionDraggableProps & {
  draggableIndex: number;
  usePortal?: boolean;
  zIndex?: number;
};

const {element} = bemFactory({baseClassName, classNames});

export const SectionDraggable = ({
  draggableIndex,
  draggable,
  name,
  id,
  collapsible,
  defaultCollapsed,
  paddingSize,
  error,
  errorText,
  content,
  bodyHasPadding,
  title,
  actions,
  optionsMenu,
  zIndex,
  ...rest
}: SectionDraggableInternalProps) => {
  const commonProps = {
    ['data-section-name']: name || id,
    defaultCollapsed: collapsible && defaultCollapsed !== false,
    paddingSize: paddingSize,
    error: error,
    errorText: errorText,
    collapsible: collapsible,
  };

  const renderBody = () => {
    if (content) {
      return (
        <Collapsible>
          <Body hasPadding={bodyHasPadding}>{content}</Body>
        </Collapsible>
      );
    }
    return null;
  };

  if (draggable) {
    return (
      <Draggable draggableId={id} index={draggableIndex}>
        {(provided, snapshot) => {
          if (
            snapshot.isDragging &&
            provided.draggableProps.style &&
            'position' in provided.draggableProps.style &&
            zIndex
          ) {
            provided.draggableProps.style.zIndex = zIndex;
          }

          return (
            <InternalSection
              innerRef={provided.innerRef}
              draggableProps={provided.draggableProps}
              snapshot={snapshot}
              {...commonProps}
              {...rest}>
              <Header
                title={
                  <div className={element('title-flex')}>
                    <div
                      className={element('draggable-icon')}
                      {...provided.dragHandleProps}>
                      <Icon path={dragVertical} />
                    </div>
                    <div className={element('header-title')}>{title}</div>
                  </div>
                }
                actions={actions}
                optionsMenu={optionsMenu}
              />
              {renderBody()}
            </InternalSection>
          );
        }}
      </Draggable>
    );
  }

  return (
    <InternalSection {...commonProps} {...rest}>
      <Header title={title} actions={actions} optionsMenu={optionsMenu} />
      {renderBody()}
    </InternalSection>
  );
};

export default SectionDraggable;
