import React, {ReactNode} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import SectionContext from './SectionContext';
import {baseClassName} from '../utils';

import classNames from './Section.module.css';

type SectionFooterProps = {
  children?: ReactNode;
  hasPadding?: boolean;
};

const {element} = bemFactory({baseClassName, classNames});

export const SectionFooter = ({
  children,
  hasPadding = true,
  ...rest
}: SectionFooterProps) => {
  const {paddingSize} = React.useContext(SectionContext);

  return (
    <footer
      className={cn(element('footer'), {
        [element('footer', `padding-${paddingSize}`)]: hasPadding,
      })}
      data-section="footer"
      data-testid="section-footer"
      {...extractDataAndAriaProps(rest)}>
      {children}
    </footer>
  );
};

export default SectionFooter;
