import React, {ReactNode} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import {baseClassName} from '../utils';

import classNames from './Section.module.css';

type SectionListProps = {
  children?: ReactNode;
  spaced?: boolean;
};

const {element} = bemFactory({baseClassName, classNames});

export const SectionList = ({
  children,
  spaced = true,
  ...rest
}: SectionListProps) => {
  const classes = cn(element('list-container'), {
    [element('list-container', 'spaced')]: spaced,
  });

  return (
    <div
      data-testid="section-list"
      className={classes}
      {...extractDataAndAriaProps(rest)}>
      {children}
    </div>
  );
};

export default SectionList;
