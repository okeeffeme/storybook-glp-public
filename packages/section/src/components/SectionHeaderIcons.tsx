import React, {ReactNode} from 'react';
import {hasVisibleChildren} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {baseClassName} from '../utils';

import classNames from './Section.module.css';

type SectionHeaderIconsProps = {
  children?: ReactNode;
};

const {element} = bemFactory({baseClassName, classNames});

const SectionHeaderIcons = ({children}: SectionHeaderIconsProps) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return <div className={element('header-icons-container')}>{children}</div>;
};

export default SectionHeaderIcons;
