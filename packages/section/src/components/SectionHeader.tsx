import React, {ReactNode, useContext} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {IconButton} from '@jotunheim/react-button';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import Icon, {chevronDown, chevronUp} from '@jotunheim/react-icons';

import {Actions, SectionOptionsMenuProps} from '../types';
import SectionContext from './SectionContext';
import {SectionAction} from './SectionAction';
import SectionOptionsMenu from './SectionOptionsMenu';
import HeaderIcons from './SectionHeaderIcons';

import {baseClassName} from '../utils';

import classNames from './Section.module.css';

type SectionHeaderProps = {
  title?: ReactNode;
  actions?: Actions;
  optionsMenu?: SectionOptionsMenuProps;
  children?: ReactNode;
  hasPadding?: boolean;
};

const {element} = bemFactory({baseClassName, classNames});

export const SectionHeader = ({
  title,
  actions,
  optionsMenu,
  children,
  hasPadding = true,
  ...rest
}: SectionHeaderProps) => {
  const {collapsible, collapsed, onToggle, paddingSize} =
    useContext(SectionContext);

  const classes = cn(element('header-container'), {
    [element('header-container', `padding-${paddingSize}`)]: hasPadding,
    [element('header-container', 'collapsed')]: collapsed,
  });

  const actualActions = actions?.getActions();

  return (
    <div
      className={classes}
      onClick={collapsible ? onToggle : undefined}
      data-section="header"
      data-testid="header-section"
      {...extractDataAndAriaProps(rest)}>
      <header>
        <div role="heading" className={element('header-title')}>
          {title}
        </div>
        <HeaderIcons>
          {children}
          {actualActions &&
            actions?.onAction &&
            actualActions.map((action) => {
              return (
                <SectionAction
                  key={action.command}
                  command={action.command}
                  icon={action.icon}
                  tooltip={action.tooltip}
                  onAction={actions.onAction}
                />
              );
            })}
          {optionsMenu && <SectionOptionsMenu {...optionsMenu} />}
          {collapsible && (
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                onToggle();
              }}
              data-section-toggle="icon-button">
              {collapsed ? (
                <Icon path={chevronDown} />
              ) : (
                <Icon path={chevronUp} />
              )}
            </IconButton>
          )}
        </HeaderIcons>
      </header>
    </div>
  );
};

export default SectionHeader;
