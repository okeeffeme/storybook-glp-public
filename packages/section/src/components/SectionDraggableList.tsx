import React, {useRef} from 'react';
import {
  DragDropContext,
  Droppable,
  DropResult,
  ResponderProvided,
} from 'react-beautiful-dnd';

import {useZIndexStack} from '@jotunheim/react-contexts';

import SectionList from './SectionList';
import SectionDraggable from './SectionDraggable';
import {SectionDraggableProps} from '../types';

type SectionDraggableListProps = {
  enableDragAndDrop?: boolean;
  droppableId?: string;
  onDrop?: (result: DropResult, provided: ResponderProvided) => void;
  spaced?: boolean;
  sections: Array<SectionDraggableProps>;
  usePortal?: boolean;
};

export const SectionDraggableList = ({
  enableDragAndDrop = true,
  droppableId = 'section-list',
  onDrop = () => {},
  sections,
  spaced,
  usePortal = false,
  ...rest
}: SectionDraggableListProps) => {
  const zIndexStack = useZIndexStack();
  const zIndexRef = useRef<number | undefined>();

  return (
    <SectionList spaced={spaced} {...rest}>
      <DragDropContext
        onBeforeDragStart={() => {
          zIndexRef.current = zIndexStack.obtain();
        }}
        onDragEnd={(result: DropResult, provided: ResponderProvided) => {
          if (zIndexRef.current) {
            zIndexStack.release(zIndexRef.current);
            zIndexRef.current = undefined;
          }
          onDrop?.(result, provided);
        }}>
        <Droppable droppableId={droppableId}>
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {sections.map((section, i) => {
                return (
                  <SectionDraggable
                    key={section.id}
                    draggableIndex={i}
                    draggable={section.draggable || enableDragAndDrop}
                    usePortal={usePortal}
                    zIndex={zIndexRef.current}
                    {...section}
                  />
                );
              })}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </SectionList>
  );
};

export default SectionDraggableList;
