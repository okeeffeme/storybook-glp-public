# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.12&sourceBranch=refs/tags/@jotunheim/react-section@10.3.13&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.12&sourceBranch=refs/tags/@jotunheim/react-section@10.3.12&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.10&sourceBranch=refs/tags/@jotunheim/react-section@10.3.11&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.9&sourceBranch=refs/tags/@jotunheim/react-section@10.3.10&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.8&sourceBranch=refs/tags/@jotunheim/react-section@10.3.9&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.7&sourceBranch=refs/tags/@jotunheim/react-section@10.3.8&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.6&sourceBranch=refs/tags/@jotunheim/react-section@10.3.7&targetRepoId=1246) (2023-03-22)

### Bug Fixes

- Remove dependencies ([NPM-1126](https://jiraosl.firmglobal.com/browse/NPM-1126)) ([d672807](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d672807c182f7aa6b102a92bfc6dc651149257f5))
- use zIndexStack only in onDragStart and onDragEnd ([NPM-1126](https://jiraosl.firmglobal.com/browse/NPM-1126)) ([d285e15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d285e15d0475950b0d1993148a1a118b2b667a9d))

## [10.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.5&sourceBranch=refs/tags/@jotunheim/react-section@10.3.6&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.4&sourceBranch=refs/tags/@jotunheim/react-section@10.3.5&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.3&sourceBranch=refs/tags/@jotunheim/react-section@10.3.4&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.2&sourceBranch=refs/tags/@jotunheim/react-section@10.3.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [10.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.1&sourceBranch=refs/tags/@jotunheim/react-section@10.3.2&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-section

## [10.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.3.0&sourceBranch=refs/tags/@jotunheim/react-section@10.3.1&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-section

# [10.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.2.3&sourceBranch=refs/tags/@jotunheim/react-section@10.3.0&targetRepoId=1246) (2023-02-20)

### Features

- support background color property of section component ([NPM-1251](https://jiraosl.firmglobal.com/browse/NPM-1251)) ([dc38559](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dc38559286ea06fe99861f8159fd431ff0c679db))

## [10.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.2.2&sourceBranch=refs/tags/@jotunheim/react-section@10.2.3&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-section

## [10.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.2.1&sourceBranch=refs/tags/@jotunheim/react-section@10.2.2&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-section

## [10.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.2.0&sourceBranch=refs/tags/@jotunheim/react-section@10.2.1&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- adding data-testids to Section components ([NPM-1208](https://jiraosl.firmglobal.com/browse/NPM-1208)) ([e187b8f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e187b8f02cb28133c46965543ab4963a7d0fdf8f))
- renaming data-testids in Section components ([NPM-1208](https://jiraosl.firmglobal.com/browse/NPM-1208)) ([3ff4908](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3ff49086fcb1a21c3971896d527be1e80249bc79))

# [10.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.29&sourceBranch=refs/tags/@jotunheim/react-section@10.2.0&targetRepoId=1246) (2023-02-07)

### Features

- Add ability to disable padding for Section header and footer ([NPM-1249](https://jiraosl.firmglobal.com/browse/NPM-1249)) ([20d3443](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/20d3443a154e4c1e5ceecaeff5d9712e4bbe19d1))

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.29&sourceBranch=refs/tags/@jotunheim/react-section@10.1.0&targetRepoId=1246) (2023-02-07)

### Features

- Add ability to disable padding for Section header and footer ([NPM-1249](https://jiraosl.firmglobal.com/browse/NPM-1249)) ([20d3443](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/20d3443a154e4c1e5ceecaeff5d9712e4bbe19d1))

## [10.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.28&sourceBranch=refs/tags/@jotunheim/react-section@10.0.29&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.27&sourceBranch=refs/tags/@jotunheim/react-section@10.0.28&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.26&sourceBranch=refs/tags/@jotunheim/react-section@10.0.27&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.25&sourceBranch=refs/tags/@jotunheim/react-section@10.0.26&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.24&sourceBranch=refs/tags/@jotunheim/react-section@10.0.25&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.23&sourceBranch=refs/tags/@jotunheim/react-section@10.0.24&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.22&sourceBranch=refs/tags/@jotunheim/react-section@10.0.23&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.21&sourceBranch=refs/tags/@jotunheim/react-section@10.0.22&targetRepoId=1246) (2023-01-30)

### Bug Fixes

- use the same version (13.1.1) of react-beautiful-dnd among all packages; bump the version of @types/react-beautiful-dnd ([HUB-9705](https://jiraosl.firmglobal.com/browse/HUB-9705)) ([f65c6d4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f65c6d42d89506ed47af76d313fffbc139244ea2))

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.20&sourceBranch=refs/tags/@jotunheim/react-section@10.0.21&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.19&sourceBranch=refs/tags/@jotunheim/react-section@10.0.20&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.17&sourceBranch=refs/tags/@jotunheim/react-section@10.0.19&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.17&sourceBranch=refs/tags/@jotunheim/react-section@10.0.18&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.16&sourceBranch=refs/tags/@jotunheim/react-section@10.0.17&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.15&sourceBranch=refs/tags/@jotunheim/react-section@10.0.16&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.14&sourceBranch=refs/tags/@jotunheim/react-section@10.0.15&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.13&sourceBranch=refs/tags/@jotunheim/react-section@10.0.14&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.12&sourceBranch=refs/tags/@jotunheim/react-section@10.0.13&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.11&sourceBranch=refs/tags/@jotunheim/react-section@10.0.12&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.10&sourceBranch=refs/tags/@jotunheim/react-section@10.0.11&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.9&sourceBranch=refs/tags/@jotunheim/react-section@10.0.10&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.8&sourceBranch=refs/tags/@jotunheim/react-section@10.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.5&sourceBranch=refs/tags/@jotunheim/react-section@10.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.5&sourceBranch=refs/tags/@jotunheim/react-section@10.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.5&sourceBranch=refs/tags/@jotunheim/react-section@10.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.4&sourceBranch=refs/tags/@jotunheim/react-section@10.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.3&sourceBranch=refs/tags/@jotunheim/react-section@10.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.2&sourceBranch=refs/tags/@jotunheim/react-section@10.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-section

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@10.0.0&sourceBranch=refs/tags/@jotunheim/react-section@10.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- fix SectionCollapsible types ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([994e27d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/994e27d32dfded56be66822c34c5a73dfe06b73b))

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.17&sourceBranch=refs/tags/@jotunheim/react-section@10.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Section ([NPM-926](https://jiraosl.firmglobal.com/browse/NPM-926)) ([ae53d6b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ae53d6b617cafc0f614e5539c1b95478e8a8b015))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

feat: remove className prop of Section (NPM-925)

- As part of NPM-925 we remove className props from components.

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.16&sourceBranch=refs/tags/@jotunheim/react-section@9.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.15&sourceBranch=refs/tags/@jotunheim/react-section@9.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.14&sourceBranch=refs/tags/@jotunheim/react-section@9.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.13&sourceBranch=refs/tags/@jotunheim/react-section@9.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.12&sourceBranch=refs/tags/@jotunheim/react-section@9.0.13&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.11&sourceBranch=refs/tags/@jotunheim/react-section@9.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.10&sourceBranch=refs/tags/@jotunheim/react-section@9.0.11&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.9&sourceBranch=refs/tags/@jotunheim/react-section@9.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.7&sourceBranch=refs/tags/@jotunheim/react-section@9.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.4&sourceBranch=refs/tags/@jotunheim/react-section@9.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.3&sourceBranch=refs/tags/@jotunheim/react-section@9.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.2&sourceBranch=refs/tags/@jotunheim/react-section@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.1&sourceBranch=refs/tags/@jotunheim/react-section@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-section

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-section@9.0.0&sourceBranch=refs/tags/@jotunheim/react-section@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-section

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.11&sourceBranch=refs/tags/@confirmit/react-section@8.1.12&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.10&sourceBranch=refs/tags/@confirmit/react-section@8.1.11&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.9&sourceBranch=refs/tags/@confirmit/react-section@8.1.10&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- could not control collapsing on SectionDraggable ([NPM-1022](https://jiraosl.firmglobal.com/browse/NPM-1022)) ([56c84b1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/56c84b140f07173cb53b4e7ebc36a74e2e6353db))
- Removed unused variables and added a comment to ESLint setting ignoreRestSiblings ([NPM-1022](https://jiraosl.firmglobal.com/browse/NPM-1022)) ([85c2453](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85c24532ffeaaa2229b753caf7510ad367d9998e))

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.8&sourceBranch=refs/tags/@confirmit/react-section@8.1.9&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- update Dropdown.MenuItem usage ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([6ce3219](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ce3219a699823f7081cc01e1ad966abef71ba3b))

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.7&sourceBranch=refs/tags/@confirmit/react-section@8.1.8&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.6&sourceBranch=refs/tags/@confirmit/react-section@8.1.7&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.5&sourceBranch=refs/tags/@confirmit/react-section@8.1.6&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.4&sourceBranch=refs/tags/@confirmit/react-section@8.1.5&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.2&sourceBranch=refs/tags/@confirmit/react-section@8.1.3&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.1&sourceBranch=refs/tags/@confirmit/react-section@8.1.2&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-section

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.1.0&sourceBranch=refs/tags/@confirmit/react-section@8.1.1&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-section

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.8&sourceBranch=refs/tags/@confirmit/react-section@8.1.0&targetRepoId=1246) (2022-03-22)

### Features

- Introduce ability to render the dragged element in a portal ([AM-7459](https://jiraosl.firmglobal.com/browse/AM-7459)) ([4539921](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/45399216515251118bbab2ad8deaf0905aeedbc1))

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.7&sourceBranch=refs/tags/@confirmit/react-section@8.0.8&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-section

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.6&sourceBranch=refs/tags/@confirmit/react-section@8.0.7&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-section

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.5&sourceBranch=refs/tags/@confirmit/react-section@8.0.6&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-section

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.2&sourceBranch=refs/tags/@confirmit/react-section@8.0.3&targetRepoId=1246) (2022-02-18)

### Bug Fixes

- Casting properties to sidestep type-easure when assigning properties to a function ([NPM-904](https://jiraosl.firmglobal.com/browse/NPM-904)) ([1e2cc61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1e2cc61de5f0ec1ce2e4cfb026b3ae398b31cfef))

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.1&sourceBranch=refs/tags/@confirmit/react-section@8.0.2&targetRepoId=1246) (2022-02-18)

### Bug Fixes

- don't collapse when clicking option items ([SVD-1881](https://jiraosl.firmglobal.com/browse/SVD-1881)) ([758a45f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/758a45fb31562dd9ec9779e0bd8e90826432c202))
- removed preventDefault from stopClick in SectionOptionsMenu ([SVD-1881](https://jiraosl.firmglobal.com/browse/SVD-1881)) ([0947b93](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0947b938890133e9047b81a77b0edf9b689dec48))
- stop propagation when click on dropdown options menu ([SVD-1881](https://jiraosl.firmglobal.com/browse/SVD-1881)) ([c1d68b4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1d68b488f565fb5cd84f002ad01d9f87b6e1d56))

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@8.0.0&sourceBranch=refs/tags/@confirmit/react-section@8.0.1&targetRepoId=1246) (2022-02-16)

### Bug Fixes

- Add more tests before migration to new component ([NPM-852](https://jiraosl.firmglobal.com/browse/NPM-852)) ([7c7f9e7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7c7f9e72db8183f1ebb85983184d8fa9f20ebb7b))

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.14&sourceBranch=refs/tags/@confirmit/react-section@8.0.0&targetRepoId=1246) (2022-02-03)

### Bug Fixes

- remove theme support ([92c365d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/92c365dbe5b1b837bb6165653c468e39b1fa3391))
- Use MenuTextItem instead, so that options menu doesn't automatically close when disabled opion is clicked. Cursor for disabled items also changed to browser default. ([NPM-923](https://jiraosl.firmglobal.com/browse/NPM-923)) ([4147bce](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4147bce5ed6ac2ab19ae7cc2475342ffb303dec1))

### Features

- Show helperText in Tooltip instead of InformationIcon ([NPM-923](https://jiraosl.firmglobal.com/browse/NPM-923)) ([5b49325](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5b493257cb6e962bc933caf6db56b12bc41cabe6))

### BREAKING CHANGES

- remove theme support
- Remove the "icon" prop from options, consumers should use "iconPath" instead.

## [7.8.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.13&sourceBranch=refs/tags/@confirmit/react-section@7.8.14&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.12&sourceBranch=refs/tags/@confirmit/react-section@7.8.13&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.11&sourceBranch=refs/tags/@confirmit/react-section@7.8.12&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.10&sourceBranch=refs/tags/@confirmit/react-section@7.8.11&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.9&sourceBranch=refs/tags/@confirmit/react-section@7.8.10&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.8&sourceBranch=refs/tags/@confirmit/react-section@7.8.9&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.7&sourceBranch=refs/tags/@confirmit/react-section@7.8.8&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.6&sourceBranch=refs/tags/@confirmit/react-section@7.8.7&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.5&sourceBranch=refs/tags/@confirmit/react-section@7.8.6&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.4&sourceBranch=refs/tags/@confirmit/react-section@7.8.5&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.8.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.3&sourceBranch=refs/tags/@confirmit/react-section@7.8.4&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.1&sourceBranch=refs/tags/@confirmit/react-section@7.8.2&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-section

## [7.8.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.8.0&sourceBranch=refs/tags/@confirmit/react-section@7.8.1&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-section

# [7.8.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.7.3&sourceBranch=refs/tags/@confirmit/react-section@7.8.0&targetRepoId=1246) (2021-09-23)

### Features

- add active prop ([NPM-530](https://jiraosl.firmglobal.com/browse/NPM-530)) ([c4bf87b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c4bf87ba404d2993afa066027ac6dee44cbb1120))

## [7.7.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.7.2&sourceBranch=refs/tags/@confirmit/react-section@7.7.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-section

## [7.7.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.7.1&sourceBranch=refs/tags/@confirmit/react-section@7.7.2&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-section

## [7.7.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.7.0&sourceBranch=refs/tags/@confirmit/react-section@7.7.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-section

# [7.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.5&sourceBranch=refs/tags/@confirmit/react-section@7.7.0&targetRepoId=1246) (2021-09-13)

### Features

- support iconPath on SectionOptionsMenu. This deprecates previous icon property, and it will be removed in the future ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([468f5f3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/468f5f3b5066631c8a254def95bc8f24c9861aea))

## [7.6.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.4&sourceBranch=refs/tags/@confirmit/react-section@7.6.5&targetRepoId=1246) (2021-09-13)

### Bug Fixes

- add correct padding on Section.Body when no Section.Header is present ([NPM-781](https://jiraosl.firmglobal.com/browse/NPM-781)) ([bbe2f10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bbe2f102d065e8c10f6c8abbf9ab8124c0abb74b))

## [7.6.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.3&sourceBranch=refs/tags/@confirmit/react-section@7.6.4&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-section

## [7.6.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.2&sourceBranch=refs/tags/@confirmit/react-section@7.6.3&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.6.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.1&sourceBranch=refs/tags/@confirmit/react-section@7.6.2&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-section

## [7.6.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.6.0&sourceBranch=refs/tags/@confirmit/react-section@7.6.1&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-section

# [7.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.5.2&sourceBranch=refs/tags/@confirmit/react-section@7.6.0&targetRepoId=1246) (2021-08-09)

### Features

- rewrite rest of Section to TypeScript, add deprecation warnings for className prop, rewrite some internal components to reduce duplication ([NPM-837](https://jiraosl.firmglobal.com/browse/NPM-837)) ([b15f949](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b15f949d7210cfa8054691f065327e5903be732d))

## [7.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.5.1&sourceBranch=refs/tags/@confirmit/react-section@7.5.2&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-section

## [7.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.5.0&sourceBranch=refs/tags/@confirmit/react-section@7.5.1&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-section

# [7.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.33&sourceBranch=refs/tags/@confirmit/react-section@7.5.0&targetRepoId=1246) (2021-07-26)

### Features

- support error state with tooltip for Section ([NPM-831](https://jiraosl.firmglobal.com/browse/NPM-831)) ([f7ce184](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f7ce184e8959f8deb337fe7fb949a6bef8a95b83))

## [7.4.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.32&sourceBranch=refs/tags/@confirmit/react-section@7.4.33&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.31&sourceBranch=refs/tags/@confirmit/react-section@7.4.32&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.30&sourceBranch=refs/tags/@confirmit/react-section@7.4.31&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.29&sourceBranch=refs/tags/@confirmit/react-section@7.4.30&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.27&sourceBranch=refs/tags/@confirmit/react-section@7.4.28&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.26&sourceBranch=refs/tags/@confirmit/react-section@7.4.27&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.25&sourceBranch=refs/tags/@confirmit/react-section@7.4.26&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.24&sourceBranch=refs/tags/@confirmit/react-section@7.4.25&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.23&sourceBranch=refs/tags/@confirmit/react-section@7.4.24&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.22&sourceBranch=refs/tags/@confirmit/react-section@7.4.23&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.21&sourceBranch=refs/tags/@confirmit/react-section@7.4.22&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.20&sourceBranch=refs/tags/@confirmit/react-section@7.4.21&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.19&sourceBranch=refs/tags/@confirmit/react-section@7.4.20&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.18&sourceBranch=refs/tags/@confirmit/react-section@7.4.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.17&sourceBranch=refs/tags/@confirmit/react-section@7.4.18&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.16&sourceBranch=refs/tags/@confirmit/react-section@7.4.17&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.14&sourceBranch=refs/tags/@confirmit/react-section@7.4.15&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.13&sourceBranch=refs/tags/@confirmit/react-section@7.4.14&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.12&sourceBranch=refs/tags/@confirmit/react-section@7.4.13&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.11&sourceBranch=refs/tags/@confirmit/react-section@7.4.12&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.10&sourceBranch=refs/tags/@confirmit/react-section@7.4.11&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.9&sourceBranch=refs/tags/@confirmit/react-section@7.4.10&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.8&sourceBranch=refs/tags/@confirmit/react-section@7.4.9&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.7&sourceBranch=refs/tags/@confirmit/react-section@7.4.8&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.6&sourceBranch=refs/tags/@confirmit/react-section@7.4.7&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.5&sourceBranch=refs/tags/@confirmit/react-section@7.4.6&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.4&sourceBranch=refs/tags/@confirmit/react-section@7.4.5&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.3&sourceBranch=refs/tags/@confirmit/react-section@7.4.4&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.2&sourceBranch=refs/tags/@confirmit/react-section@7.4.3&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.1&sourceBranch=refs/tags/@confirmit/react-section@7.4.2&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-section

## [7.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.4.0&sourceBranch=refs/tags/@confirmit/react-section@7.4.1&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-section

# [7.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.3.0&sourceBranch=refs/tags/@confirmit/react-section@7.4.0&targetRepoId=1246) (2021-03-15)

### Features

- Add disabled and helperText property to optionsmenu in section ([NPM-742](https://jiraosl.firmglobal.com/browse/NPM-742)) ([24877c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/24877c24e0fd737fbd0e0db76b9dc7d135fbc2ef))

# [7.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.17&sourceBranch=refs/tags/@confirmit/react-section@7.3.0&targetRepoId=1246) (2021-03-10)

### Bug Fixes

- remove incorrect margin-bottom on sectionlist header ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([5e7db72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5e7db722d4b7e777f81cf903a2a4f3b50f33ad66))

### Features

- add paddingSize prop to Section and hasPadding prop to Section.Body ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([bf627d3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bf627d3a000205609ad1ed92293f1216440ca1d1))

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.16&sourceBranch=refs/tags/@confirmit/react-section@7.2.17&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.15&sourceBranch=refs/tags/@confirmit/react-section@7.2.16&targetRepoId=1246) (2021-03-03)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.14&sourceBranch=refs/tags/@confirmit/react-section@7.2.15&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.13&sourceBranch=refs/tags/@confirmit/react-section@7.2.14&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.12&sourceBranch=refs/tags/@confirmit/react-section@7.2.13&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.11&sourceBranch=refs/tags/@confirmit/react-section@7.2.12&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.9&sourceBranch=refs/tags/@confirmit/react-section@7.2.10&targetRepoId=1246) (2021-02-11)

### Features

- Upgrade Section.Collapsible to supporting react-transition-group 4, to support React strict mode

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.9&sourceBranch=refs/tags/@confirmit/react-section@7.2.10&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.8&sourceBranch=refs/tags/@confirmit/react-section@7.2.9&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.7&sourceBranch=refs/tags/@confirmit/react-section@7.2.8&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.6&sourceBranch=refs/tags/@confirmit/react-section@7.2.7&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.5&sourceBranch=refs/tags/@confirmit/react-section@7.2.6&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.4&sourceBranch=refs/tags/@confirmit/react-section@7.2.5&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.3&sourceBranch=refs/tags/@confirmit/react-section@7.2.4&targetRepoId=1246) (2020-12-16)

### Bug Fixes

- use min-height on header, to allow it to grow ([NPM-664](https://jiraosl.firmglobal.com/browse/NPM-664)) ([48cae48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/48cae48bbdb5e0852720498ff0868d7e91245220))

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.2&sourceBranch=refs/tags/@confirmit/react-section@7.2.3&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.1&sourceBranch=refs/tags/@confirmit/react-section@7.2.2&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-section

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.2.0&sourceBranch=refs/tags/@confirmit/react-section@7.2.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-section

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.9&sourceBranch=refs/tags/@confirmit/react-section@7.2.0&targetRepoId=1246) (2020-12-11)

### Features

- support children in Section.Header ([NPM-644](https://jiraosl.firmglobal.com/browse/NPM-644)) ([6162c9c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6162c9c4c353956dae31e2dd72eb515555fc94e3))

## [7.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.8&sourceBranch=refs/tags/@confirmit/react-section@7.1.9&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.7&sourceBranch=refs/tags/@confirmit/react-section@7.1.8&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.6&sourceBranch=refs/tags/@confirmit/react-section@7.1.7&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.5&sourceBranch=refs/tags/@confirmit/react-section@7.1.6&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.4&sourceBranch=refs/tags/@confirmit/react-section@7.1.5&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.1&sourceBranch=refs/tags/@confirmit/react-section@7.1.2&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-section

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.1.0&sourceBranch=refs/tags/@confirmit/react-section@7.1.1&targetRepoId=1246) (2020-11-25)

### Bug Fixes

- Uncontrolled collapsible section no longer worked ([NPM-633](https://jiraosl.firmglobal.com/browse/NPM-633)) ([ae3853a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ae3853a5ff3df7dc815a47d80db21001ba57e68f))

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.0.6&sourceBranch=refs/tags/@confirmit/react-section@7.1.0&targetRepoId=1246) (2020-11-24)

### Features

- Section should support controlled "collapsed" and "onToggle" props. Also supports tooltips for header actions. ([NPM-486](https://jiraosl.firmglobal.com/browse/NPM-486)) ([c38127d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c38127d49cbd09f528711ae8ad59342322ea25dd))

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.0.3&sourceBranch=refs/tags/@confirmit/react-section@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-section

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.0.2&sourceBranch=refs/tags/@confirmit/react-section@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-section

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@7.0.1&sourceBranch=refs/tags/@confirmit/react-section@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-section

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@6.0.1&sourceBranch=refs/tags/@confirmit/react-section@7.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@6.0.1&sourceBranch=refs/tags/@confirmit/react-section@7.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@6.0.1&sourceBranch=refs/tags/@confirmit/react-section@6.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-section

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.2.3&sourceBranch=refs/tags/@confirmit/react-section@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [5.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.2.2&sourceBranch=refs/tags/@confirmit/react-section@5.2.3&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-section

## [5.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.2.1&sourceBranch=refs/tags/@confirmit/react-section@5.2.2&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-section

## [5.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.2.0&sourceBranch=refs/tags/@confirmit/react-section@5.2.1&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-section

### [5.2.0]

- options menu with dynamic icon
- options menu added to section list header

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.1.0&sourceBranch=refs/tags/@confirmit/react-section@5.1.1&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-section

## [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.40&sourceBranch=refs/tags/@confirmit/react-section@5.1.0&targetRepoId=1246) (2020-10-15)

- **Changes:**
  - Fix: Only by drag icon it is possible to drag and drop sections in SectionDraggableList
  - Feat: SectionListHeader accepts getActions and onAction functions in props

## [5.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.38&sourceBranch=refs/tags/@confirmit/react-section@5.0.39&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.37&sourceBranch=refs/tags/@confirmit/react-section@5.0.38&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.35&sourceBranch=refs/tags/@confirmit/react-section@5.0.36&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.30&sourceBranch=refs/tags/@confirmit/react-section@5.0.31&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.29&sourceBranch=refs/tags/@confirmit/react-section@5.0.30&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.27&sourceBranch=refs/tags/@confirmit/react-section@5.0.28&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-section@5.0.25&sourceBranch=refs/tags/@confirmit/react-section@5.0.26&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-section@5.0.23...@confirmit/react-section@5.0.24) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-section@5.0.21...@confirmit/react-section@5.0.22) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.19](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-section@5.0.18...@confirmit/react-section@5.0.19) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.15](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-section@5.0.14...@confirmit/react-section@5.0.15) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-section

## [5.0.14](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-section@5.0.13...@confirmit/react-section@5.0.14) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-section

## CHANGELOG

### v5.0.0

- **BREAKING**:
  - Renamed package name from `@confirmit/react-panel` to `@confrmit/react-section`
  - Renamed Panel to Section
  - Renamed PanelSection to SectionList
  - Removed Title, Toggle, and Actions components Section
  - Removed Section props: `onToggle`, `collapsed`, `type`, `dropShadow`, `border`
  - Removed support for `uncontrollable` and added internal implementation of uncontrolled collapse. Sections cannot be controlled anymore
  - Removed padding from Body
  - Added Section props: `collapsible` and `defaultCollapsed`
  - Added Header props: `title`, `actions`, and `optionsMenu`
- Added SectionListHeader and SectionDraggableList components
- Added support for drag and drop sections using `react-beautiful-dnd`
- Updated Section and SectionList margin and padding spacing
- Added SectionList prop: `spaced` which defaults to `true`

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Removing package version in class names.

### v3.0.13

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v3.0.3

- Fix: Adding overflow:hidden to panel\_\_header, panel\_\_header--title and panel\_\_footer for material-theme

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.0

- **BREAKING**
  - Removed props `baseClassName`, `classNames`, `collapsedIcon`, `expandedIcon` as these were only meant to be internal
  - Use same icons for default and material theme
- Refactor: remove `children` propType validation for `PanelSection` since it does not work child is wrapped in HOC etc
- Refactor: use new icon package for expand/collapse icon
- Refactor: change to useTheme
- Chore: remove unused styles

### v1.0.0

- **BREAKING**: Adding `IconButton` to `Panel.Toggle`, adds additional padding to toggle
- Feat: Matching MaterialUI for multiple collapsible panels in material-theme
- Feat: Adding `PanelSection` wrapper for multiple collapsible panels

### v0.0.1

- Initial version
