# Jotunheim React Main Application List

A specialized version of the Table component, which should be used to represent the "main" objects that an application deals with,
like dashboards, surveys, hierarchies, cases, etc.

Enforces the following behavior:

- Uses page level scroll, pagination not allowed, only load more button should be used.
- Needs to be sorted by default, ideally on a Created Date field if applicable.
- For convenience, it is possible to mark columns with either isNameColumn or isStatusColumn, to achieve a consistent look of these (see stories for examples).
- The "Name" column has a slightly bigger font than usual.
  - The NameLink component is exported separately if custom rendering of the Name column is needed.

[Changelog](./CHANGELOG.md)

## Typescript

Since version 2.1.x it has built-in Typescript support.
Please check to [Table Readme.MD](./../table/README.md#Typescript) if you have any errors caused by types.
