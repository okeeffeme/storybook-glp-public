# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.56&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.57&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.55&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.56&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.54&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.55&targetRepoId=1246) (2023-04-07)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.54&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.54&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.52&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.53&targetRepoId=1246) (2023-04-05)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.51&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.52&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.50&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.51&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.49&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.50&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.48&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.49&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.47&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.48&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.46&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.47&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.45&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.46&targetRepoId=1246) (2023-03-24)

### Bug Fixes

- Main App List is short-circuiting optimation due to always recreating "columns" array ([NPM-1281](https://jiraosl.firmglobal.com/browse/NPM-1281)) ([7e9088e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7e9088ecfa4355cb603ae99827a358213c626b87))

## [2.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.44&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.45&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.43&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.44&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.42&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.43&targetRepoId=1246) (2023-03-03)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.41&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.42&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.40&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.41&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- remove redundant data-usePageScroll attribute on main app list ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([ae1c321](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ae1c3210f81cddabe5947a3b5221d8e349a7fde2))

## [2.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.39&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.40&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.38&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.39&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.36&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.38&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.36&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.37&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.35&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.36&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.34&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.35&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.33&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.34&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.32&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.33&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.31&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.32&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.30&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.31&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.29&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.30&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.28&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.29&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.27&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.28&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.26&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.27&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.25&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.26&targetRepoId=1246) (2023-02-06)

### Bug Fixes

- adding data-testid in Main Application List component ([NPM-1198](https://jiraosl.firmglobal.com/browse/NPM-1198)) ([e91ab62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e91ab62fe7d650226585c74c7e0ac7c8ec9b1ead))
- adding data-testid to Main-application-list components ([NPM-1198](https://jiraosl.firmglobal.com/browse/NPM-1198)) ([4a96c87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4a96c87fd377797754f8731a5ddb81be1c028b67))

## [2.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.24&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.25&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.23&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.24&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.22&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.23&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.21&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.22&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.20&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.21&targetRepoId=1246) (2023-01-25)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.19&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.20&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.18&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.19&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.14&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.18&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.14&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.14&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.16&targetRepoId=1246) (2023-01-18)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.14&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.15&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.13&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.14&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.12&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.13&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.11&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.10&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.9&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.10&targetRepoId=1246) (2022-12-28)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.8&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.9&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.7&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.8&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.6&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.7&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.5&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.6&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.4&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.5&targetRepoId=1246) (2022-12-20)

### Bug Fixes

- use zIndexStack for loading indicator ([NPM-1127](https://jiraosl.firmglobal.com/browse/NPM-1127)) ([6b290b8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6b290b84880cf43d7373f274c4587fbc9829b237))

## [2.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.3&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.4&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.0&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.3&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.0&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.2&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.1.0&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.1&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-main-application-list

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.25&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.1.0&targetRepoId=1246) (2022-11-23)

### Features

- convert MainApplicationList to Typescript ([NPM-1102](https://jiraosl.firmglobal.com/browse/NPM-1102)) ([a0982da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a0982da877ff77f58912469bd7df520d4e54085a))

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.24&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.25&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.23&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.24&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.21&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.22&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.20&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.21&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.19&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.20&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.18&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.19&targetRepoId=1246) (2022-09-30)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.17&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.18&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.16&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.17&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.15&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.16&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.14&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.15&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.13&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.14&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.12&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.11&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.12&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.9&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.10&targetRepoId=1246) (2022-08-31)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.8&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.9&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.5&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.6&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.4&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.5&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.3&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.4&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.2&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.3&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.1&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-main-application-list

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-main-application-list@2.0.0&sourceBranch=refs/tags/@jotunheim/react-main-application-list@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-main-application-list

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.23&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.24&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.22&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.23&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.21&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.22&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.20&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.21&targetRepoId=1246) (2022-05-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.19&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.20&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.18&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.19&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.17&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.18&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.16&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.17&targetRepoId=1246) (2022-05-11)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.15&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.16&targetRepoId=1246) (2022-04-26)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.14&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.15&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.9&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.10&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.8&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.9&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.7&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.8&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.6&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.7&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.5&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.6&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.4&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.5&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@1.0.3&sourceBranch=refs/tags/@confirmit/react-main-application-list@1.0.4&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-table@10.4.23&sourceBranch=refs/tags/@confirmit/react-table@11.0.0&targetRepoId=1246) (2022-02-11)

### BREAKING CHANGES

- react-table changed: Remove the deprecated "icon" prop from options menu items (NPM-858)

## [0.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.17&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.18&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.16&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.17&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.15&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.16&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.13&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.14&targetRepoId=1246) (2022-01-26)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.12&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.13&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.11&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.12&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.10&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.11&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.9&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.10&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.8&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.9&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.7&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.8&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.6&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.7&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.5&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.6&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.4&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.5&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.3&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.4&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.2&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.3&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.1&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.2&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.2.0&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.1&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-main-application-list

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.76&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.2.0&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

### Features

- update to new forsta logo in app-header ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([0b59376](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0b59376c70ae3baa2fb8f76acfad6911dd61d866))

## [0.1.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.75&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.76&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.73&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.74&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.72&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.73&targetRepoId=1246) (2021-10-01)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.71&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.72&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.70&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.71&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.69&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.70&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.68&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.69&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.67&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.68&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.66&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.67&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.65&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.66&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.64&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.65&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.63&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.64&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.62&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.63&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.61&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.62&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.60&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.61&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.59&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.60&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.58&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.59&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.57&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.58&targetRepoId=1246) (2021-08-11)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.56&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.57&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.55&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.56&targetRepoId=1246) (2021-07-30)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.54&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.55&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.53&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.54&targetRepoId=1246) (2021-07-27)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.52&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.53&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.51&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.52&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.50&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.51&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.49&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.50&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.48&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.49&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.45&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.46&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.44&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.45&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.43&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.44&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.42&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.43&targetRepoId=1246) (2021-06-22)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.41&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.42&targetRepoId=1246) (2021-06-21)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.39&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.40&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.38&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.39&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.37&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.38&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.36&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.37&targetRepoId=1246) (2021-06-08)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.35&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.36&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.33&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.34&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.32&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.33&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.31&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.32&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.30&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.31&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.29&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.30&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.28&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.29&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.26&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.27&targetRepoId=1246) (2021-05-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.24&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.25&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.23&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.24&targetRepoId=1246) (2021-04-22)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.22&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.23&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.21&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.22&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.20&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.21&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.19&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.20&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.18&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.19&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.17&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.18&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.16&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.17&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.15&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.16&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.14&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.15&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.13&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.14&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.12&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.13&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.11&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.12&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.10&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.11&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.9&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.10&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.8&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.9&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.7&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.8&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.6&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.7&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.5&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.6&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.4&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.5&targetRepoId=1246) (2021-03-09)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.3&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.4&targetRepoId=1246) (2021-03-08)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.2&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.3&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.1&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.2&targetRepoId=1246) (2021-03-03)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.1.0&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.1.1&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-main-application-list

## 0.1.0 (2021-02-17)

**Breaking:** makeHref renamed to getHref and now provides row object instead of value
**Breaking:** getNameText is now used to get value for name row
**Breaking:** getStatusAppearance and getStatusText also provide row object instead of value

[0.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.0.5&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.0.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.0.4&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.0.5&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-main-application-list

## [0.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-main-application-list@0.0.3&sourceBranch=refs/tags/@confirmit/react-main-application-list@0.0.4&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-main-application-list

## 0.0.2 (2021-02-10)

### Bug Fixes

- add grid wrapper to allow background to expand and not break layout when horizontal scrolling occurs ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([32c6384](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/32c6384b96be5891acb2b38411d07ca00155f46c))

## CHANGELOG

### v0.0.1

- Initial version
