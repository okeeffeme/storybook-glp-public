import React from 'react';
import {storiesOf} from '@storybook/react';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import MainApplicationList, {NameLink} from '../src';
import Icon, {
  viewColumn,
  contentDuplicate,
  pencil,
  printer,
  trashBin,
} from '../../icons/src';
import {IconButton} from '../../button/src';
import AppHeader from '../../app-header/src';
import {Tooltip} from '../../tooltip/src';
import {Truncate} from '../../truncate/src';
import {Appearance} from '../../tag/src';
import {MainApplicationListColumn} from '../src/components/MainApplicationList';
import {Row} from 'react-table';

const appHeaderProps = {
  oidcEnabled: false,
  title: 'App Title',
  user: {
    firstName: 'FirstName',
    lastName: 'LastName',
  },
};

const getHref = (row) => `http://google.com/search?q=${encodeURI(row.name)}`;

const renderDrawer = (dataItem: DataItem) => (
  <div style={{backgroundColor: '#ccc'}}>
    <div>{dataItem.name}</div>
    <div>Extra info goes here</div>
  </div>
);

type DataItem = {
  name: string;
  status: string;
  header2: string;
  header3: string;
  header4: string;
  header5?: string;
};

const columns: MainApplicationListColumn<DataItem>[] = [
  {
    Header: 'Name',
    accessor: 'name',
    isNameColumn: true,
    getHref,
  },
  {
    Header: 'Status',
    isStatusColumn: true,
    accessor: 'status',
    getStatusText: (row) => {
      return row.status === 'live' ? 'published' : 'in design';
    },
    getStatusAppearance: (row) => {
      return row.status === 'live' ? Appearance.Safe : Appearance.Warning;
    },
  },
  {
    Header: 'Header 3',
    accessor: 'header3',
  },
  {
    Header: 'Header 4',
    accessor: 'header4',
  },
  {
    Header: 'Header 5',
    accessor: 'header5',
  },
];

const customColumns: MainApplicationListColumn<DataItem>[] = [
  {
    Header: 'Name',
    suppressTooltip: true,
    Cell: function Cell({row}: {row: Row<DataItem>}) {
      const {name, header2, header3} = row.original;
      return (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '40px 0',
          }}>
          <div style={{marginRight: 20}}>
            <Truncate lines={1}>
              <Tooltip content={name}>
                <NameLink href={getHref({name})}>{name}</NameLink>
              </Tooltip>
            </Truncate>
          </div>
          <div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                width: '100px',
              }}>
              <div>
                <strong>Header 2</strong>
              </div>
              <div>{header2}</div>
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                width: '100px',
              }}>
              <div>
                <strong>Header 3</strong>
              </div>
              <div>{header3}</div>
            </div>
          </div>
        </div>
      );
    },
  },
  {
    Header: 'Header 4',
    accessor: 'header4',
  },
  {
    Header: 'Header 5',
    accessor: 'header5',
  },
];
/* eslint-enable react/prop-types */
/* eslint-enable react/display-name */

const items = Array.from(Array(20).keys()).map((item, index) => ({
  name: `Item ${++index}`,
  status: index % 2 === 0 ? 'design' : 'live',
  header2: 'Item 2',
  header3: 'Item 3',
  header4: 'Item 4',
}));

const moreItems = Array.from(Array(5).keys()).map((item, index) => ({
  name: `Item ${++index + 20}`,
  status: 'live',
  header2: 'Item 2',
  header3: 'Item 3',
  header4: 'Item 4',
}));

const optionsMenu = {
  onSelect: () => null,
  getOptions: () => {
    return [
      {
        command: 'duplicate',
        name: 'Duplicate',
        iconPath: contentDuplicate,
        href: '/duplicate',
      },
      {
        command: 'print',
        name: 'Print',
        iconPath: printer,
      },
      {
        command: 'edit',
        name: 'Edit',
        iconPath: pencil,
        href: '/edit',
      },
      {
        command: 'delete',
        name: 'Delete',
        disabled: true,
        iconPath: trashBin,
      },
    ];
  },
};

/* eslint-disable react/prop-types */
const AppWrapper = ({children}) => {
  // The grid on this AppWrapper is needed to avoid background being cropped off when horizontal scrolling occurs
  // Each app needs to add something like this to their AppWrapper
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: '1fr',
        gridTemplateRows: 'minmax(100vh, auto)',
      }}>
      <div>
        <AppHeader {...appHeaderProps} />
        <div style={{margin: 0, padding: 20, backgroundColor: '#e1e4e9'}}>
          {children}
        </div>
      </div>
    </div>
  );
};

storiesOf('Components/main-application-list', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [data, setData] = React.useState<DataItem[]>([]);
    const [isMoreLoading, setIsMoreLoading] = React.useState(false);

    React.useEffect(() => {
      setTimeout(() => {
        setData(items);
      }, 1000);
    }, []);

    const onLoadMore = React.useCallback(() => {
      setIsMoreLoading(true);

      setTimeout(() => {
        setData((items) => items.concat(moreItems));
        setIsMoreLoading(false);
      }, 1000);
    }, []);

    return (
      <AppWrapper>
        <MainApplicationList
          toolbarProps={{
            title: 'Table',
            count: 999,
            batchOperationOptions: [
              {
                value: 'edit',
                label: 'Edit',
              },
              {
                value: 'delete',
                label: 'Delete',
              },
            ],
            onBatchOperationSelect: () => alert('Batch operation select'),
            batchOperationPlaceholder: 'Apply to selected',
            columnOptions: columns.map((column) => ({
              value: column.accessor as string,
              label: column.Header as string,
            })),
            onSearch: () => null,
            searchValue: '',
            searchColumn: '',
            searchPlaceholder: 'Search',
            children: (
              <IconButton>
                <Icon path={viewColumn} />
              </IconButton>
            ),
          }}
          contentProps={{
            data,
            columns,
            optionsMenu,
            isMoreLoading,
            isLoading: data.length === 0,
            onLoadMore: data.length > 20 ? undefined : onLoadMore,
          }}
        />
      </AppWrapper>
    );
  })
  .add('With drawer and custom name column', () => {
    return (
      <AppWrapper>
        <MainApplicationList
          toolbarProps={{
            title: 'Table',
          }}
          contentProps={{
            data: items,
            columns: customColumns,
            renderDrawer,
          }}
        />
      </AppWrapper>
    );
  });
