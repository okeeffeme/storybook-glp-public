import React from 'react';
import {render, screen} from '@testing-library/react';
import MainApplicationList, {
  MainApplicationListColumn,
  TagAppearance,
} from '../../src';

type TestDataItem = {
  name?: string;
  status?: string;
};

describe('Jotunheim React Main Application List :: ', () => {
  it('Inner table component should have usePageScroll enabled', () => {
    render(<MainApplicationList contentProps={{columns: [], data: []}} />);

    expect(screen.getByTestId('table')).toHaveClass(
      'comd-table comd-table--page-level-scroll comd-table--padding'
    );
  });

  it('When getNameText specified in name column should get value from it', () => {
    const mockColumns: MainApplicationListColumn<TestDataItem>[] = [
      {
        Header: 'Name',
        accessor: 'name',
        isNameColumn: true,
        getHref: () => 'href',
        getNameText: () => 'Text from getNameText',
      },
    ];

    const mockData = [
      {
        name: 'Test name',
      },
    ];

    render(
      <MainApplicationList
        contentProps={{
          columns: mockColumns,
          data: mockData,
        }}
      />
    );
    expect(screen.getByRole('link')).toHaveTextContent('Text from getNameText');
  });

  it('When getNameText not specified in name column should get value from data object', () => {
    const mockColumns: MainApplicationListColumn<TestDataItem>[] = [
      {
        Header: 'Name',
        accessor: 'name',
        isNameColumn: true,
        getHref: () => 'href',
      },
    ];

    const mockData = [
      {
        name: 'Test name',
      },
    ];

    render(
      <MainApplicationList
        contentProps={{
          columns: mockColumns,
          data: mockData,
        }}
      />
    );

    expect(screen.getByRole('link')).toHaveTextContent('Test name');
  });

  it('When getStatusText specified in status column should get value from it', () => {
    const mockColumns: MainApplicationListColumn<TestDataItem>[] = [
      {
        Header: 'Status',
        accessor: 'status',
        isStatusColumn: true,
        getStatusAppearance: () => TagAppearance.Danger,
        getStatusText: () => 'Text from getNameText',
      },
    ];

    const mockData = [
      {
        status: 'Test status',
      },
    ];

    render(
      <MainApplicationList
        contentProps={{
          columns: mockColumns,
          data: mockData,
        }}
      />
    );
    expect(screen.getByTestId('main-application-list-tag')).toHaveTextContent(
      'Text from getNameText'
    );
  });

  it('When getStatusText not specified in status column should get value from data object', () => {
    const mockColumns: MainApplicationListColumn<TestDataItem>[] = [
      {
        Header: 'Status',
        accessor: 'status',
        isStatusColumn: true,
        getStatusAppearance: () => TagAppearance.Primary,
      },
    ];

    const mockData = [
      {
        status: 'Test status',
      },
    ];

    render(
      <MainApplicationList
        contentProps={{
          columns: mockColumns,
          data: mockData,
        }}
      />
    );

    expect(screen.getByTestId('main-application-list-tag')).toHaveTextContent(
      'Test status'
    );
  });
});
