import React, {MouseEventHandler, ReactNode, ForwardedRef} from 'react';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Link, {LinkAppearances} from '@jotunheim/react-link';

import classNames from './MainApplicationList.module.css';

const NameLink = React.forwardRef(
  (
    {children, href, target, onClick, ...rest}: NameLinkProps,
    ref: ForwardedRef<HTMLSpanElement>
  ) => {
    const {element} = bemFactory({
      baseClassName: 'comd-main-application-list',
      classNames,
    });

    return (
      <span
        ref={ref}
        className={element('name-link')}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}>
        <Link
          href={href}
          target={target}
          onClick={onClick}
          appearance={LinkAppearances.table}>
          {children}
        </Link>
      </span>
    );
  }
);

export type NameLinkProps = {
  children: ReactNode;
  href: string;
  target?: string;
  onClick?: MouseEventHandler;
};

export default NameLink;
