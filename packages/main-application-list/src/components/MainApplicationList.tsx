import React, {
  PropsWithChildren,
  ReactElement,
  ReactNode,
  useMemo,
} from 'react';
import Table, {
  ToolbarProps,
  ContentProps,
  Column,
  ColumnInterfaceBasedOnValue,
} from '@jotunheim/react-table';
import {Tag, Type, Appearance} from '@jotunheim/react-tag';
import {
  DataAriaIdAttributes,
  developerNotice,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';

import NameLink from './NameLink';

export const MainApplicationList: (<T extends Record<string, unknown>>(
  props: PropsWithChildren<MainApplicationListProps<T>>
) => ReactElement<MainApplicationListProps<T>> | null) & {
  NameLink: typeof NameLink;
  TagAppearance: typeof Appearance;
} = <T extends Record<string, unknown>>({
  toolbarProps,
  contentProps,
  ...rest
}: PropsWithChildren<MainApplicationListProps<T>>) => {
  const columns = useMemo(() => {
    return contentProps.columns.map((item) => {
      if (item.isNameColumn) {
        (item as ColumnInterfaceBasedOnValue<T>).Cell = function Cell({cell}) {
          return (
            <NameLink href={item.getHref(cell.row.original)}>
              {item.getNameText
                ? item.getNameText(cell.row.original)
                : cell.value}
            </NameLink>
          );
        };
      }

      if (item.isStatusColumn) {
        if (item.suppressTooltip == null) {
          // covers both null and undefined
          item.suppressTooltip = true;
        }

        (item as ColumnInterfaceBasedOnValue<T>).Cell = function Cell({cell}) {
          return (
            <Tag
              type={Type.Document}
              appearance={item.getStatusAppearance?.(cell.row.original)}
              data-document-status={cell.value}
              data-testid="main-application-list-tag">
              {item.getStatusText
                ? item.getStatusText?.(cell.row.original)
                : cell.value}
            </Tag>
          );
        };
      }

      return item;
    });
  }, [contentProps.columns]);

  if (process.env.NODE_ENV !== 'production') {
    const usedContentProps = Object.keys(contentProps);
    const invalidContentProps = [
      'onPageChange',
      'pageNumber',
      'pageSize',
      'showEmptyState',
    ];

    invalidContentProps.forEach((prop) => {
      if (usedContentProps.includes(prop)) {
        developerNotice(
          `Content prop "${prop}" is not valid for Main Application List.`
        );
      }
    });
    if (!usedContentProps.includes('sortField')) {
      developerNotice(
        `Content prop "sortField" must be set, should sort by Created Date by default.`
      );
    }
  }

  return (
    <Table
      usePageScroll={true}
      data-testid="main-application-list-table"
      {...extractDataAriaIdProps(rest)}
      toolbar={<Table.Toolbar {...toolbarProps} />}
      content={
        <Table.Content
          {...contentProps}
          columns={columns}
          showEmptyState={true}
        />
      }
    />
  );
};

type MainApplicationListColumnProps<D extends Record<string, unknown>> =
  | {isNameColumn?: undefined; isStatusColumn?: undefined}
  | {
      isNameColumn: true;
      getHref: (original: D) => string;
      getNameText?: (original: D) => ReactNode;
      isStatusColumn?: undefined;
    }
  | {
      isNameColumn?: undefined;
      isStatusColumn: true;
      getStatusText?: (original: D) => string;
      getStatusAppearance?: (original: D) => Appearance;
    };

export type MainApplicationListColumn<
  D extends Record<string, unknown> = Record<string, unknown>
> = MainApplicationListColumnProps<D> & Column<D>;

export type MainApplicationListProps<
  D extends Record<string, unknown> = Record<string, unknown>
> = {
  toolbarProps?: ToolbarProps;
  contentProps: {
    columns: MainApplicationListColumn<D>[];
  } & ContentProps<D>;
} & DataAriaIdAttributes;

MainApplicationList.NameLink = NameLink;
MainApplicationList.TagAppearance = Appearance;

export default MainApplicationList;
