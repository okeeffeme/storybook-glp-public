# Jotunheim React Simple Select

### **This component is for internal use (inside this repo) ONLY**

## DESCRIPTION

React Simple Select aims to be the building block of present and future select components;
providing both efficiency and a basis for consistent styling (eventually).
