import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {flattenNestedOptions} from '../../src/components/simple-select';
import {theme, ThemeContext} from '../../../themes/src';

import Select from '../../src';

const defaultOptions = [
  {value: '1', label: 'Option 1'},
  {value: '2', label: 'Option 2'},
  {value: '3', label: 'Option 3'},
];

const groupedOptions = [
  {
    label: 'Header 1',
    options: [
      {
        label: 'Option 1',
        value: '1',
      },
      {
        label: 'Option 2',
        value: '2',
      },
    ],
  },
  {
    label: 'Header 2',
    options: [
      {
        label: 'Option 3',
        value: '3',
      },
    ],
  },
];

const defaultProps = {
  value: 0,
  options: [],
  onChange() {},
};

const render = (props) =>
  renderRTL(<Select {...defaultProps} {...props} />, {
    wrapper: ({children}) => (
      <ThemeContext.Provider value={theme.themeNames.material}>
        {children}
      </ThemeContext.Provider>
    ),
  });

describe('Jotunheim React Simple Select :: ', () => {
  describe('Single select', () => {
    it('should render with options prop', () => {
      render({options: defaultOptions});

      userEvent.click(screen.getByTestId('simple-select-dropdown-indicator'));

      expect(screen.getAllByText(/option/i).length).toBe(3);
    });

    it('should render with children options', () => {
      const customOptions = [
        {
          label: 'Option 1',
          value: '1',
        },
        {
          label: 'Option 2',
          value: '2',
        },
      ];

      render({
        options: customOptions,
      });

      userEvent.click(screen.getByTestId('simple-select-dropdown-indicator'));

      const options = screen.getAllByText(/option/i);

      expect(options.length).toBe(2);
      expect(options[0].textContent).toBe('Option 1');
      expect(options[1].textContent).toBe('Option 2');
    });

    it('should trigger onChange with a single option value', () => {
      const onChange = jest.fn();

      render({options: defaultOptions, onChange});

      userEvent.click(screen.getByTestId('simple-select-dropdown-indicator'));
      userEvent.click(screen.getByText('Option 2'));

      expect(onChange).toHaveBeenCalledWith('2');
    });

    it('should set correct value on inner ReactSelect with a nested set of options', () => {
      render({
        options: [{label: 'group', options: defaultOptions}],
        value: '2',
      });

      expect(screen.getByText('Option 2')).toBeInTheDocument();
    });

    it('should render with grouped children options', () => {
      render({
        options: groupedOptions,
      });

      userEvent.click(screen.getByTestId('simple-select-dropdown-indicator'));

      expect(screen.getByText(groupedOptions[0].label)).toBeInTheDocument();
      expect(screen.getByText(groupedOptions[1].label)).toBeInTheDocument();
      expect(screen.getAllByText(/option/i).length).toBe(3);
    });

    it('should set selected value of inner ReactSelect to option matching current value', () => {
      render({
        options: defaultOptions,
        value: '2',
      });

      const options = screen.getAllByText(/option/i);

      expect(options.length).toBe(1);
      expect(options[0].textContent).toBe('Option 2');
    });

    it('should set selected value of inner ReactSelect to option matching current value if value is zero', () => {
      const customOptions = [
        {value: 0, label: 'Option 0'},
        {value: 1, label: 'Option 1'},
      ];

      render({options: customOptions, value: 0});

      const options = screen.getAllByText(/option/i);

      expect(options.length).toBe(1);
      expect(options[0].textContent).toBe('Option 0');
    });

    it('should set selected value of inner ReactSelect to option matching current value when using grouped options', () => {
      render({
        options: groupedOptions,
        value: '3',
      });

      const options = screen.getAllByText(/option/i);

      expect(options.length).toBe(1);
      expect(options[0].textContent).toBe('Option 3');
    });

    it('should not have bidirectional override tag if truncation is from the right', () => {
      render({
        options: defaultOptions,
        value: '1',
      });

      expect(screen.getByText('Option 1')).not.toHaveAttribute('dir', 'ltr');
    });

    it('should have bidirectional override tag if truncation is from the left', () => {
      render({
        options: defaultOptions,
        value: '1',
        truncateValueFromLeft: true,
      });

      expect(screen.getByText('Option 1')).toHaveAttribute('dir', 'ltr');
    });
  });

  describe('Multiple Select', () => {
    it('should trigger onChange with array of options', () => {
      let selectedOption = null;
      const onChange = jest.fn((opt) => (selectedOption = opt));

      render({
        options: defaultOptions,
        isMulti: true,
        value: [],
        onChange,
      });

      userEvent.click(screen.getByTestId('simple-select-dropdown-indicator'));

      userEvent.click(screen.getByText(defaultOptions[0].label));

      expect(onChange).toHaveBeenCalledWith(selectedOption);

      userEvent.click(screen.getByText(/select/i));

      userEvent.click(screen.getByText(defaultOptions[1].label));

      expect(onChange).toHaveBeenCalledWith(selectedOption);
    });

    it('should set selected value of inner ReactSelect to option to null value if undefined value is passed in', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: undefined,
      });

      expect(screen.queryByTestId('multi-value')).not.toBeInTheDocument();
    });

    it('should set selected value of inner ReactSelect to option to null value that is passed in', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: null,
      });

      expect(screen.queryByTestId('multi-value')).not.toBeInTheDocument();
    });

    it('should set selected value of inner ReactSelect to option to array value that is passed in', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[1], defaultOptions[2]],
      });

      const selectedValues = screen.getAllByTestId('multi-value');

      expect(selectedValues.length).toBe(2);
      expect(selectedValues[0].textContent).toBe(defaultOptions[1].label);
      expect(selectedValues[1].textContent).toBe(defaultOptions[2].label);
    });

    it('should set Chip prop isActive to true for selected items in multiselect', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[1]],
      });

      expect(screen.getByTestId('chip')).toBeInTheDocument();
    });

    it('should have multi value class name if value truncation from the left is not set', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[0]],
      });

      const selectedValue = screen.getByTestId('multi-value');

      expect(
        selectedValue.classList.contains('comd-select-multi-value__content')
      ).toBe(true);
      expect(
        selectedValue.classList.contains('comd-select-multi-value__content-rtl')
      ).toBe(false);
    });

    it('should have multi value class for left truncation if value truncation from the left is set', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[0]],
        truncateValueFromLeft: true,
      });

      const selectedValue = screen.getByTestId('multi-value');

      expect(
        selectedValue.classList.contains('comd-select-multi-value__content')
      ).toBe(true);
      expect(
        selectedValue.classList.contains(
          'comd-select-multi-value__content--rtl'
        )
      ).toBe(true);
    });

    it('should not have bidirectional override tag if truncation is from the right', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[0]],
      });

      // expect(wrapper.find('bdo').length).toBe(0);
    });
    //
    it('should have bidirectional override tag if truncation is from the left', () => {
      render({
        isMulti: true,
        options: defaultOptions,
        value: [defaultOptions[0]],
        truncateValueFromLeft: true,
      });

      expect(screen.getByText(/option/i)).toHaveAttribute('dir', 'ltr');
    });
    it('should not disable dropdown indicator when Select is disabled', () => {
      render({isDisabled: true});

      expect(
        screen.getByTestId('simple-select-dropdown-indicator')
      ).toHaveProperty('disabled');
    });
  });

  describe('flattenNestedOptions', () => {
    it('should flatten nested options object', () => {
      const options = [
        {
          label: 1,
          value: 1,
        },
        {
          label: 2,
          value: 2,
          options: [
            {
              label: 3,
              value: 3,
            },
            {
              label: 4,
              value: 4,
            },
          ],
        },
      ];

      expect(flattenNestedOptions(options)).toEqual([
        {
          label: 1,
          value: 1,
        },
        {
          label: 2,
          value: 2,
          options: [
            {
              label: 3,
              value: 3,
            },
            {
              label: 4,
              value: 4,
            },
          ],
        },
        {
          label: 3,
          value: 3,
        },
        {
          label: 4,
          value: 4,
        },
      ]);
    });
  });
});
