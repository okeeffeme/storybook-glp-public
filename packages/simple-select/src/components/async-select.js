import React from 'react';
import PropTypes from 'prop-types';
import AsyncPaginate, {reduceGroupedOptions} from 'react-select-async-paginate';

import SimpleSelect from './simple-select';

const defaultAdditionalAsync = {
  page: 1,
};

const AsyncSelect = ({
  loadOptions,
  reduceOptions,
  debounceTimeout = 250,
  additional = defaultAdditionalAsync,
  ...rest
}) => {
  return (
    <AsyncPaginate
      {...rest}
      additional={additional}
      debounceTimeout={debounceTimeout}
      loadOptions={loadOptions}
      reduceOptions={reduceOptions}
      SelectComponent={SimpleSelect}
    />
  );
};

AsyncSelect.propTypes = {
  additional: PropTypes.any,
  loadOptions: PropTypes.func,
  reduceOptions: PropTypes.func,
  debounceTimeout: PropTypes.number,
};

AsyncSelect.reduceGroupedOptions = reduceGroupedOptions;

export default AsyncSelect;
