import React from 'react';
import PropTypes from 'prop-types';
import {IconButton} from '@jotunheim/react-button';
import Icon, {close} from '@jotunheim/react-icons';

const ClearIndicator = ({innerProps}) => (
  <IconButton {...innerProps} data-test-simple-select-clear-indicator>
    <Icon path={close} />
  </IconButton>
);

ClearIndicator.propTypes = {
  innerProps: PropTypes.object,
};

export default ClearIndicator;
