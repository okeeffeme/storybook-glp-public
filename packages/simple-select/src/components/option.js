import React from 'react';
import {components} from 'react-select';
import PropTypes from 'prop-types';

const Option = ({children, innerProps, ...props}) => {
  return (
    <components.Option
      innerProps={{...innerProps, 'data-test-id': 'simple-select-option'}}
      {...props}>
      {children}
    </components.Option>
  );
};

Option.propTypes = {
  children: PropTypes.node,
  innerProps: PropTypes.object,
};

export default Option;
