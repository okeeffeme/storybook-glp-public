import React from 'react';
import {components} from 'react-select';
import PropTypes from 'prop-types';

const SingleValue = ({children, ...props}) => {
  const {
    selectProps: {truncateValueFromLeft},
  } = props;

  return (
    <components.SingleValue {...props}>
      {truncateValueFromLeft ? (
        <bdo
          data-testid="bidirectional-override-tag"
          data-truncatevaluefromleft="true"
          dir="ltr">
          {children}
        </bdo>
      ) : (
        children
      )}
    </components.SingleValue>
  );
};

SingleValue.propTypes = {
  children: PropTypes.node,
  selectProps: PropTypes.object,
};

export default SingleValue;
