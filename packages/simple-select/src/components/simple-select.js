import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import CreatableSelect from 'react-select/creatable';

import {useZIndexStack} from '@jotunheim/react-contexts';

import MultiValue from './multi-value';
import SingleValue from './single-value';
import Option from './option';
import ClearIndicator from './clear-indicator';
import DropdownIndicator from './dropdown-indicator';
import MenuPortal from './menu-portal';
import SelectContainer from './select-container';

import styles from './styles';

const noop = () => {};
const noopFalse = () => false;
const noopNoResults = () => 'No results found';

export const flattenNestedOptions = (options) => {
  return options.reduce((acc, option) => {
    acc.push(option);
    if (option.options) {
      acc.push(...flattenNestedOptions(option.options));
    }
    return acc;
  }, []);
};

const getSelectedValue = (options, value, isAsync) => {
  if (
    value == null ||
    Array.isArray(value) ||
    typeof value === 'object' ||
    isAsync
  ) {
    // casting undefined to null due to unexpected rendering behaviour from react-select node_module when assigning undefined value
    // issue link: https://github.com/JedWatson/react-select/issues/3066
    return value === undefined ? null : value;
  }

  // create a flat list of potentially nested options, to more easily find selected
  const allOptions = flattenNestedOptions(options);

  return allOptions.find((option) => option.value === value);
};

export const SimpleSelect = ({
  classNamePrefix = 'comd-react-select',
  value,
  isMulti = false,
  disabled = false,
  readOnly = false,
  autoFocus = false,
  isLoading = false,
  isCreatable = false,
  isClearable = false,
  isSearchable = true,
  filterOption,
  placeholder,
  isValidNewOption,
  isOptionDisabled = noopFalse,
  noOptionsMessage = noopNoResults,
  formatGroupLabel,
  formatOptionLabel,
  menuShouldScrollIntoView,
  onFocus = noop,
  onBlur = noop,
  onChange,
  onCreateOption,
  formatCreateLabel,
  components,
  options = [],
  onMenuOpen = noop,
  onMenuClose = noop,
  truncateValueFromLeft = false,
  ...rest
}) => {
  const Component = isCreatable ? CreatableSelect : ReactSelect;

  // react-select-async-paginate passes down a shouldLoadMore function, we use this to determine whether async is enabled
  const isAsync = typeof rest.shouldLoadMore === 'function';
  const selectedValue = getSelectedValue(options, value, isAsync);

  const zIndex = useZIndexStack();

  const [menuZIndex, setMenuZIndex] = React.useState(null);
  const reactSelectStyles = React.useMemo(
    () => styles({menuZIndex, truncateValueFromLeft}),
    [menuZIndex, truncateValueFromLeft]
  );

  const handleMenuOpen = () => {
    onMenuOpen();
    setMenuZIndex(zIndex.obtain());
  };

  const handleMenuClose = () => {
    onMenuClose();
    setMenuZIndex(null);
  };

  React.useEffect(() => {
    return () => zIndex.release(menuZIndex);
  }, [zIndex, menuZIndex]);

  const checkIsOptionDisabled = (option) => {
    if (option.__isNew__ && option.value === '') {
      return true;
    }

    return isOptionDisabled(option);
  };

  const handleChange = (option) => {
    if (isMulti) {
      onChange(Array.isArray(option) ? option : []);
    } else if (isAsync) {
      onChange(option);
    } else {
      onChange(option ? option.value : undefined);
    }
  };

  if (process.env.NODE_ENV !== 'production') {
    if (isMulti && value && !Array.isArray(value)) {
      /* eslint-disable no-console*/
      console.warn(
        'Please provide an array of selected values via "value" prop'
      );
      return null;
    }
  }

  return (
    <Component
      onMenuOpen={handleMenuOpen}
      onMenuClose={handleMenuClose}
      data-react-select
      classNamePrefix={classNamePrefix}
      styles={reactSelectStyles}
      isValidNewOption={isValidNewOption}
      isOptionDisabled={checkIsOptionDisabled}
      formatOptionLabel={formatOptionLabel}
      onFocus={onFocus}
      onBlur={onBlur}
      closeMenuOnSelect={!isMulti}
      isClearable={isClearable}
      isSearchable={isSearchable}
      isDisabled={disabled || readOnly}
      readOnly={readOnly}
      isMulti={isMulti}
      menuPortalTarget={document.body}
      onChange={handleChange}
      onCreateOption={onCreateOption}
      options={options}
      placeholder={placeholder}
      autoFocus={autoFocus}
      hideSelectedOptions={isMulti}
      noOptionsMessage={noOptionsMessage}
      formatCreateLabel={formatCreateLabel}
      formatGroupLabel={formatGroupLabel}
      value={selectedValue}
      isLoading={isLoading}
      filterOption={filterOption}
      menuShouldScrollIntoView={menuShouldScrollIntoView}
      menuPlacement="auto"
      createOptionPosition="first"
      truncateValueFromLeft={truncateValueFromLeft}
      components={{
        MenuPortal,
        MultiValue,
        SingleValue,
        ClearIndicator,
        Option,
        DropdownIndicator,
        SelectContainer,
        IndicatorSeparator: null,
        ...components,
      }}
      data-testid="react-select"
      {...rest}
    />
  );
};

const OptionShapeBase = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
};

const ValueShape = PropTypes.shape({
  ...OptionShapeBase,
  value: OptionShapeBase.value.isRequired,
});

const OptionShape = PropTypes.shape({
  ...OptionShapeBase,
  options: PropTypes.arrayOf(PropTypes.shape(OptionShapeBase)),
});

SimpleSelect.propTypes = {
  classNamePrefix: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    ValueShape,
    PropTypes.arrayOf(ValueShape),
  ]),
  isMulti: PropTypes.bool,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  autoFocus: PropTypes.bool,
  isLoading: PropTypes.bool,
  isCreatable: PropTypes.bool,
  isClearable: PropTypes.bool,
  isSearchable: PropTypes.bool,
  filterOption: PropTypes.func,
  placeholder: PropTypes.string,
  isValidNewOption: PropTypes.func,
  isOptionDisabled: PropTypes.func,
  noOptionsMessage: PropTypes.func,
  formatGroupLabel: PropTypes.func,
  formatOptionLabel: PropTypes.func,
  menuShouldScrollIntoView: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onCreateOption: PropTypes.func,
  formatCreateLabel: PropTypes.func,
  components: PropTypes.object,
  children: PropTypes.node,
  options: PropTypes.arrayOf(OptionShape),
  onMenuOpen: PropTypes.func,
  onMenuClose: PropTypes.func,
  truncateValueFromLeft: PropTypes.bool,
};

export default SimpleSelect;
