import {colors} from '@jotunheim/global-styles';

const {Background1, Disabled, TextDisabled, TextSecondary, Interactive} =
  colors;

const getOptionBackgroundColor = (option) => {
  if (!option.isDisabled) {
    if (option.isSelected) {
      return Interactive;
    }

    if (option.isFocused) {
      return Background1;
    }
  }

  return null;
};

const getActiveOptionBackgroundColor = (option) => {
  if (option.isDisabled) return null;

  return option.isSelected ? option.data.color : Disabled;
};

let minWidth = '112px';

export default ({menuZIndex, truncateValueFromLeft}) => ({
  option: (baseStyles, option) => {
    return {
      ...baseStyles,
      height: 32,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      paddingLeft: 24,
      color: option.isDisabled ? TextDisabled : 'currentColor',
      backgroundColor: getOptionBackgroundColor(option),
      ':active': {
        ...baseStyles[':active'],
        backgroundColor: getActiveOptionBackgroundColor(option),
      },
    };
  },
  control: (baseStyles) => {
    return {
      ...baseStyles,
      borderRadius: '0',
      border: 'none',
      boxShadow: null,
      backgroundColor: 'transparent',
    };
  },
  input: (baseStyles) => ({
    ...baseStyles,
    padding: 0,
    margin: 0,
  }),
  container: (baseStyles) => ({
    ...baseStyles,
    marginTop: 'auto',
    width: '100%',
  }),
  indicatorsContainer: (baseStyles) => ({
    ...baseStyles,
    marginTop: 'auto',
  }),
  clearIndicator: (baseStyles) => ({
    ...baseStyles,
    padding: '0 8px',
  }),
  dropdownIndicator: (baseStyles) => ({
    ...baseStyles,
    padding: '0 8px',
  }),
  loadingIndicator: (baseStyles) => ({
    ...baseStyles,
    display: 'flex',
    alignItems: 'center',
    height: 40,
  }),
  valueContainer: (
    baseStyles,
    {isMulti, hasValue, selectProps: {readOnly}}
  ) => {
    // Since Chips are a bit big, we need to have smaller padding
    const xPadding = readOnly ? '0' : '12px';
    const padding = isMulti && hasValue ? `4px ${xPadding}` : `8px ${xPadding}`;
    return {
      ...baseStyles,
      padding,
      alignSelf: 'stretch',
    };
  },
  placeholder: (baseStyles) => ({
    ...baseStyles,
    color: TextSecondary,
  }),
  singleValue: (baseStyles, {isDisabled, selectProps: {readOnly}}) => ({
    ...baseStyles,
    width: 'calc(100% - 16px)', // subtract padding * 2
    fontSize: '1em',
    marginLeft: 0,
    color: isDisabled && !readOnly ? TextDisabled : 'currentColor',
    textAlign: 'left',
    direction: truncateValueFromLeft ? 'rtl' : 'ltr',
  }),
  menuPortal: (baseStyles) => {
    minWidth = baseStyles.width;

    return {
      ...baseStyles,
      zIndex: menuZIndex,
    };
  },
  menu: (baseStyles) => ({
    ...baseStyles,

    marginTop: 4,
    marginBottom: 0,
    position: 'static',
    width: 'auto',
    minWidth,
  }),
  menuList: (baseStyles) => ({...baseStyles, paddingTop: 8}),
});
