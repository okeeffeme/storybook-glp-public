import React from 'react';
import PropTypes from 'prop-types';
import {useTheme, bemFactory} from '@jotunheim/react-themes';
import {Chip} from '@jotunheim/react-chip';

import componentThemes from '../themes';
import cn from 'classnames';

const MultiValue = ({
  removeProps,
  children,
  isDisabled,
  selectProps: {readOnly, truncateValueFromLeft},
}) => {
  const {classNames, baseClassName} = useTheme('multiValue', componentThemes);
  const {block, element} = bemFactory({baseClassName, classNames});

  const multiClassName = cn(element('content'), {
    [element('content', 'rtl')]: truncateValueFromLeft,
  });

  return (
    // Need to capture mouseDown and stop propagation to avoid dropdown being opened when removing/clicking on items
    <span onMouseDown={(e) => e.stopPropagation()} style={{maxWidth: '98%'}}>
      <Chip
        className={block()}
        showDeleteButton={true}
        disabled={isDisabled}
        isActive={true}
        readOnly={readOnly}
        onDelete={removeProps.onClick}>
        <div className={multiClassName} data-testid="multi-value">
          {truncateValueFromLeft ? (
            <bdo
              data-testid="simple-select-bidirectional-override-tag"
              data-truncatevaluefromleft="true"
              dir="ltr">
              {children}
            </bdo>
          ) : (
            children
          )}
        </div>
      </Chip>
    </span>
  );
};

MultiValue.propTypes = {
  index: PropTypes.number,
  children: PropTypes.node,
  removeProps: PropTypes.object,
  isDisabled: PropTypes.bool,
  selectProps: PropTypes.object,
};

export default MultiValue;
