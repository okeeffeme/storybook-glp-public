import React from 'react';
import PortalTrigger from '@jotunheim/react-portal-trigger';
import PropTypes from 'prop-types';

const setSize = ({state}) => {
  state.elements.popper.style.minWidth = `${state.elements.reference.offsetWidth}px`;
  state.elements.popper.style.maxWidth = `100vw`;
};

const modifiers = [
  {
    name: 'minMaxWidth',
    enabled: true,
    phase: 'beforeWrite',
    requires: ['computeStyles'],
    fn: setSize,
    effect: setSize,
  },
];

const MenuPortal = ({children}) => {
  const popperRef = React.useRef(null);

  React.useLayoutEffect(() => {
    if (popperRef.current) {
      popperRef.current.forceUpdate();
    }
  });

  return (
    <PortalTrigger
      popperRef={popperRef}
      placement={'bottom-start'}
      open={true}
      modifiers={modifiers}
      overlay={<>{children}</>}
      transitionClassNames="someTransition">
      <div
        style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0}}
        data-testid="react-select-menu-portal"
      />
    </PortalTrigger>
  );
};

MenuPortal.propTypes = {
  children: PropTypes.node,
};

export default MenuPortal;
