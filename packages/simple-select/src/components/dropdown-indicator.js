import React from 'react';
import PropTypes from 'prop-types';
import Icon, {menuDown} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

const DropdownIndicator = ({
  innerProps,
  selectProps: {readOnly, isDisabled},
}) => {
  if (readOnly) return null;

  return (
    <IconButton
      {...innerProps}
      disabled={isDisabled}
      data-test-simple-select-dropdown-indicator
      data-testid="simple-select-dropdown-indicator">
      <Icon path={menuDown} />
    </IconButton>
  );
};

DropdownIndicator.propTypes = {
  innerProps: PropTypes.object,
  selectProps: PropTypes.object,
};

export default DropdownIndicator;
