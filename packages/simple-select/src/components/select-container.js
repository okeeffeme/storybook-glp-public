import React from 'react';
import {components} from 'react-select';

const SelectContainer = ({...props}) => {
  /* eslint-disable react/prop-types */
  return (
    <components.SelectContainer
      {...props}
      innerProps={{
        ...props.innerProps,
        [`data-testid`]: 'react-select',
      }}
    />
  );
  /* eslint-enable react/prop-types */
};

export default SelectContainer;
