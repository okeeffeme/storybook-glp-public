import classNames from './css/simple-select.module.css';

export default {
  multiValue: {
    baseClassName: 'comd-select-multi-value',
    classNames,
  },
};
