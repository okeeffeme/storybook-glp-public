# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.50&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.51&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.50&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.50&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.48&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.49&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.47&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.48&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.46&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.47&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.45&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.46&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.44&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.45&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.43&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.44&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.42&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.43&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.41&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.42&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.40&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.41&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.39&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.40&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.38&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.39&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.37&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.38&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.36&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.37&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add test ids to Select component ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([95c84ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/95c84ffde7f2dfa167a161913f5694a757e49c43))
- adding data-testid to Select components ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([eb34582](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eb3458226e6455f57d36fc2ce57ec6d23b1e4085))

## [4.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.35&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.36&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.34&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.35&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.33&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.34&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.32&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.33&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.31&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.32&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.29&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.31&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.29&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.30&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.28&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.29&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.27&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.28&targetRepoId=1246) (2023-01-10)

### Bug Fixes

- add test id for multi-value component within simple-select package ([NPM-1213](https://jiraosl.firmglobal.com/browse/NPM-1213)) ([b8ea5b3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b8ea5b39b2e17537ecc64612f011475a709710d4))

## [4.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.26&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.25&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.24&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.22&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.22&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.23&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.19&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.22&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.19&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.21&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.19&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.20&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.18&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.19&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.17&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.18&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.15&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.16&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.14&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.15&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.13&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.14&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.12&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.13&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.11&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.12&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.10&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.9&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.8&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.6&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.3&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.2&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.1&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-simple-select

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-select@4.0.0&sourceBranch=refs/tags/@jotunheim/react-simple-select@4.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-simple-select

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.4.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.22&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.23&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.21&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.22&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.20&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.21&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.4.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.19&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.20&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.18&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.19&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.16&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.17&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.15&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.16&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.14&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.15&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.13&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.14&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.12&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.13&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.11&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.12&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.8&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.9&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.7&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.8&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.6&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.7&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.5&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.6&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.4&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.5&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.3&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.4&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.2&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.3&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [3.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.4.1&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.2&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-simple-select

# [3.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.30&sourceBranch=refs/tags/@confirmit/react-simple-select@3.4.0&targetRepoId=1246) (2021-10-07)

### Features

- add data-test-simple-select-option attribute to Option component ([NPM-879](https://jiraosl.firmglobal.com/browse/NPM-879)) ([d0ee576](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d0ee57650e468f0bb6897c00ab265f4f1da16dbb))

## [3.3.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.29&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.30&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.28&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.29&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.27&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.28&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.26&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.27&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.25&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.26&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.24&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.25&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.23&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.24&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.22&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.23&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.21&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.22&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.20&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.21&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.19&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.20&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.18&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.19&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.17&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.18&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.16&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.17&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.15&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.16&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.14&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.15&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.13&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.14&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.12&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.13&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.11&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.12&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.10&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.11&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.9&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.10&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.8&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.9&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.7&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.8&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.5&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.6&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.4&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.5&targetRepoId=1246) (2021-04-20)

### Bug Fixes

- should disable dropdown indicator when Select is disabled ([NPM-654](https://jiraosl.firmglobal.com/browse/NPM-654)) ([b0c85cc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b0c85cc85b12bfa1b4211b4fc6d09c87645a8182))

## [3.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.3&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.4&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.2&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [3.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.1&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.2&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.3.0&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.1&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-simple-select

# [3.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.2.1&sourceBranch=refs/tags/@confirmit/react-simple-select@3.3.0&targetRepoId=1246) (2021-03-26)

### Features

- expose NonceProvider so apps can set a custom cachekey for emotion to avoid conflicting styling issues for React-Select ([NPM-751](https://jiraosl.firmglobal.com/browse/NPM-751)) ([a3cc910](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a3cc9100c0b19d24eefadeca960183214df7cfeb))

## [3.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.2.0&sourceBranch=refs/tags/@confirmit/react-simple-select@3.2.1&targetRepoId=1246) (2021-03-25)

### Bug Fixes

- revert emotion cache key and remove NonceProvider from simple-select, as this approach causes styling conflicts, styles are not cleaned up after unmount, and does not entirely fix the original issue ([NPM-751](https://jiraosl.firmglobal.com/browse/NPM-751)) ([d0bf1fc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d0bf1fcc77112efca3d637003fac8dde1b9227cf))

# [3.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.1.0&sourceBranch=refs/tags/@confirmit/react-simple-select@3.2.0&targetRepoId=1246) (2021-03-24)

### Features

- added cacheKey ([6834614](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6834614a7dbd6692635b1c10f4235e808816912d))

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.32&sourceBranch=refs/tags/@confirmit/react-simple-select@3.1.0&targetRepoId=1246) (2021-03-19)

### Features

- Added the option for setting the truncation direction of the selected value ([NPM-738](https://jiraosl.firmglobal.com/browse/NPM-738)) ([66a1abc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/66a1abc9faff029655d3c53264efc154900bb859))

## [3.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.31&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.32&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.30&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.31&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.29&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.30&targetRepoId=1246) (2021-03-16)

### Bug Fixes

- data attributes for simple select support buttons ([HUB-7921](https://jiraosl.firmglobal.com/browse/HUB-7921)) ([a25b50b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a25b50be4a6ed8df5e1d429da942e9bbef4cd319))

## [3.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.28&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.29&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.27&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.28&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.26&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.27&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.25&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.26&targetRepoId=1246) (2021-02-18)

### Bug Fixes

- fixed string type value is not mapped to nested options correctly ([922caaa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/922caaa26436734f4436a134e0503615eb1712a5))

## [3.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.24&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.23&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.22&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.23&targetRepoId=1246) (2021-02-12)

### Bug Fixes

- style of Chip in multi-select ([NPM-706](https://jiraosl.firmglobal.com/browse/NPM-706)) ([a73c369](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a73c369f147e76ebfda605d0d3dcd5d918939b2a))

## [3.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.20&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.21&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.19&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.20&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.18&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.19&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.17&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.18&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.16&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.15&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.14&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.13&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.12&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.11&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.10&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.7&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.4&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.3&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-simple-select

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@3.0.2&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-simple-select

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.8&sourceBranch=refs/tags/@confirmit/react-simple-select@3.0.0&targetRepoId=1246) (2020-11-11)

### Features

- confirmit/react-z-index peer is no more required ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([1c6b3d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1c6b3d84112c9ecd50d485adb1ba087e8b97001c))

### BREAKING CHANGES

- @confirmit/react-contexts is new peer dependency

## [2.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.7&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.8&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.6&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.7&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.5&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.6&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.2&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.3&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.1&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.2&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.2.0&sourceBranch=refs/tags/@confirmit/react-simple-select@2.2.1&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs%2Ftags%2F%40confirmit%2Freact-simple-select%402.1.9&sourceBranch=refs%2Ftags%2F%40confirmit%2Freact-simple-select%402.2.0&targetRepoId=1246) (2020-10-6)

### Features

- Use custom menu portal for simple select

## [2.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.1.11&sourceBranch=refs/tags/@confirmit/react-simple-select@2.1.12&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.1.9&sourceBranch=refs/tags/@confirmit/react-simple-select@2.1.10&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-select@2.1.7&sourceBranch=refs/tags/@confirmit/react-simple-select@2.1.8&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.1.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.1.5...@confirmit/react-simple-select@2.1.6) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.1.4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.1.3...@confirmit/react-simple-select@2.1.4) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.1.0...@confirmit/react-simple-select@2.1.1) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-simple-select

# [2.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.0.21...@confirmit/react-simple-select@2.1.0) (2020-08-16)

### Features

- Added `readOnly` prop

## [2.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.0.21...@confirmit/react-simple-select@2.0.22) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-simple-select

## [2.0.21](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-select@2.0.20...@confirmit/react-simple-select@2.0.21) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-simple-select

## CHANGELOG

### v2.0.11

- Make the menu grow automatically (up to a maxWidth) when text is wider than select

### v2.0.7

- Make it possible to show ellipsis in selected chips

### v2.0.6

- Fix: obtain zIndex from context instead of global zIndexStack

### v2.0.1

- Fix: translate undefined value to null since react-select nodule module doesn't clear internal state when assigning undefined value.
  Issue link: https://github.com/JedWatson/react-select/issues/3066

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v1.3.5

- Fix: z-index is properly allocated for the async version.

### v1.3.4

- Fix: value container spacing so it is centred

### v1.3.2

- Fix: z-index for the select menu is now allocated on open/close the menu.

### v1.3.1

- Refactor: Update path to import css variables

### v1.3.0

- Removing package version in class names.

### v1.2.2

- Remove usage of PortalRootContext for the menu portal target as it breaks the menu positioning

### v1.2.0

- Change styles
- Export 'components' from react-select package to allow custom overriding
- Use PortalRootContext for the menu portal target from '@jotunheim/react-transition-portal'

### v1.0.0

- **BREAKING**:
  - Tweaked styles to work with our new InputWrapper component

### v0.0.1

- Initial version
