import React, {AriaAttributes} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import formatValue from '../utils/formatValue';

import classNames from './Counter.module.css';

const {block, modifier} = bemFactory({
  baseClassName: 'comd-counter',
  classNames,
});

export const Counter = ({
  value,
  appearance = CounterAppearances.default,
  ...rest
}: CounterProps) => {
  const classes = cn(block(), modifier(appearance));
  if (value > 0) {
    return (
      <div
        className={classes}
        data-testid="data-counter"
        data-react-counter={true}
        {...extractDataAriaIdProps(rest)}>
        <span>{formatValue(value)}</span>
      </div>
    );
  }

  return null;
};

type CounterProps = {
  value: number;
  appearance?: CounterAppearances;
} & AriaAttributes;

export enum CounterAppearances {
  default = 'default',
  error = 'error',
  reference = 'reference',
  step = 'step',
}

Counter.Appearances = CounterAppearances;

export default Counter;
