import Counter, {CounterAppearances} from './components/Counter';

export {Counter, CounterAppearances};
export default Counter;
