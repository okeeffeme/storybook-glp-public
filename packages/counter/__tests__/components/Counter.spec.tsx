import React from 'react';
import {render, screen} from '@testing-library/react';
import {Counter} from '../../src/components/Counter';
import {BADGE_MAX_VALUE} from '../../src/utils/constants';

describe('Jotunheim React Counter :: ', () => {
  it('should not render counter if value is 0', () => {
    render(<Counter value={0} />);

    expect(screen.queryByTestId('counter')).not.toBeInTheDocument();
  });
  it('should render badge with text of 9', () => {
    const maxValue = BADGE_MAX_VALUE;
    const expectedString = `${BADGE_MAX_VALUE}`;
    render(<Counter value={maxValue} />);
    expect(screen.queryByTestId('data-counter')).toHaveTextContent(
      expectedString
    );
  });

  it('should render badge with text of 9+', () => {
    const exceedsMaxValue = BADGE_MAX_VALUE + 1;
    const expectedString = `${BADGE_MAX_VALUE}+`;
    render(<Counter value={exceedsMaxValue} />);

    expect(screen.queryByTestId('data-counter')).toHaveTextContent(
      expectedString
    );
  });
});
