# Jotunheim React Counter

[Changelog](./CHANGELOG.md)

Counter badges display numeric counts in different contexts. These badges share the size of a 24 by 24px Icon. These badges have a maximum value of 9, and higher counts are represented as '9+'. When the count is 0, the counter is not displayed.

The fill colour of the circle changes depending upon its usage: orange for references, red for errors, blue for steps.

A counter badge can be part of an Icon Button to create a Counter Badge Icon Button.
