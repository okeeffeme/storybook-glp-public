import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {IconButton} from '../../button/src';
import {select, number} from '@storybook/addon-knobs';

import Counter from '../src';

/* eslint-disable-next-line */
const Container = ({children}) => {
  return <div style={{padding: '24'}}>{children}</div>;
};

storiesOf('Components/Counter', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default with Knobs', () => (
    <Container>
      <Counter
        value={number('value', 4)}
        appearance={select(
          'appearance',
          Counter.Appearances,
          Counter.Appearances[0]
        )}
      />
    </Container>
  ))
  .add('IconButton Usage with Knobs', () => (
    <Container>
      <IconButton>
        <Counter
          value={number('value', 4)}
          appearance={select(
            'appearance',
            Counter.Appearances,
            Counter.Appearances[0]
          )}
        />
      </IconButton>
    </Container>
  ));
