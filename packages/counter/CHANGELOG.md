# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.1.2&sourceBranch=refs/tags/@jotunheim/react-counter@2.1.3&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-counter

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.1.1&sourceBranch=refs/tags/@jotunheim/react-counter@2.1.2&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- adding data-testid Counter ([NPM-1182](https://jiraosl.firmglobal.com/browse/NPM-1182)) ([376d85f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/376d85fd57d234f2eadedc98fe516941f631797d))
- adding data-testid to Counter ([NPM-1182](https://jiraosl.firmglobal.com/browse/NPM-1182)) ([402c5c5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/402c5c59ffab2686c167c063291eb595dec638f8))

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.1.0&sourceBranch=refs/tags/@jotunheim/react-counter@2.1.1&targetRepoId=1246) (2023-01-10)

### Bug Fixes

- add test id for Counter component within counter package ([NPM-1215](https://jiraosl.firmglobal.com/browse/NPM-1215)) ([693c9b1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/693c9b12c2c236631031bbb004400af6858a43cc))

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.0.2&sourceBranch=refs/tags/@jotunheim/react-counter@2.1.0&targetRepoId=1246) (2022-10-13)

### Features

- convert Counter to TypeScript ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([a8275f2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a8275f25f17ee03329377834f4bf9fe17b12b6a9))

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.0.1&sourceBranch=refs/tags/@jotunheim/react-counter@2.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-counter

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-counter@2.0.0&sourceBranch=refs/tags/@jotunheim/react-counter@2.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-counter

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@1.0.0&sourceBranch=refs/tags/@confirmit/react-counter@1.0.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@0.1.2&sourceBranch=refs/tags/@confirmit/react-counter@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Release version 1.0.0 ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@0.1.1&sourceBranch=refs/tags/@confirmit/react-counter@0.1.2&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-counter

## [0.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@0.1.0&sourceBranch=refs/tags/@confirmit/react-counter@0.1.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [0.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@0.0.4&sourceBranch=refs/tags/@confirmit/react-counter@0.1.0&targetRepoId=1246) (2020-11-06)

### Features

- add explicit default appearance for counter, and expose appearances const ([NPM-595](https://jiraosl.firmglobal.com/browse/NPM-595)) ([c0f3473](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c0f3473a5604c3f3bcda2dab67c79a004db8bdac))

## [0.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-counter@0.0.3&sourceBranch=refs/tags/@confirmit/react-counter@0.0.4&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-counter

## [0.0.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-counter@0.0.2...@confirmit/react-counter@0.0.3) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-counter

## CHANGELOG

### v0.0.1

- Initial version
