import React, {useCallback, useEffect, useState} from 'react';
import moment, {Moment} from 'moment';
import cn from 'classnames';
import {useUncontrolledProp} from 'uncontrollable';

import {extractDataAndAriaProps, extractProps} from '@jotunheim/react-utils';
import Icon, {calendar} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';
import {TextField} from '@jotunheim/react-text-field';
import Calendar from '@jotunheim/react-calendar';
import {bemFactory} from '@jotunheim/react-themes';

import {DefaultDateFormat} from '../constants';
import classNames from './DatePicker.module.css';

const {block, element} = bemFactory({
  baseClassName: 'comd-date-picker',
  classNames,
});

type DatePickerProps = {
  defaultDate?: Moment | null;
  date?: Moment | null;
  onChange?: (date: Moment | null) => void;
  portalZIndex?: number;
  id?: string;
  name?: string;
  minDate?: Moment;
  maxDate?: Moment;
  format?: string;
  disabled?: boolean;
  readOnly?: boolean;
  hasError?: boolean;
  errorText?: string;
  placeholder?: string;
  label?: string;
  showLabel?: boolean;
  showClear?: boolean;
};

export default function DatePicker(props: DatePickerProps) {
  const {
    id,
    name,
    format = DefaultDateFormat,
    minDate,
    maxDate,
    portalZIndex,
    disabled,
    readOnly,
    hasError = false,
    errorText,
    placeholder,
    label,
    showLabel = false,
    showClear,
    ...rest
  } = props;

  const [date, onChange] = useUncontrolledProp(
    props.date,
    props.defaultDate,
    props.onChange
  );

  const [open, setOpen] = useState(false);
  const [formattedDate, setFormattedDate] = useState('');

  const handleInputChange = (value: string) => {
    setFormattedDate(value);

    const mValue = value
      ? moment(value, [format, DefaultDateFormat], true)
      : null;

    onChange(mValue);
  };

  const handleSelect = (date: Moment) => {
    const mDate = moment(date);
    const formattedDate = mDate.format(format);
    setFormattedDate(formattedDate);

    // We recreate moment object from formatted date, to make it similar if it could have been created from user input
    // This affects moment.creationData() result
    const mValue = moment(formattedDate, [format], true);
    onChange(mValue);

    setOpen(false);
  };

  const handleToggle = useCallback(
    (open: boolean) => {
      if (!disabled && !readOnly) setOpen(open);
    },
    [disabled, readOnly, setOpen]
  );

  /* istanbul ignore next */
  const handleInputKeyUp = (e) => {
    if (
      e.nativeEvent.keyCode === 13 ||
      (open && e.nativeEvent.keyCode === 27)
    ) {
      e.nativeEvent.stopImmediatePropagation();
      setOpen(false);
    }
  };

  /* Clear formatted date if date is null.
   * Needed when date is invalid and is cleared via props. */
  useEffect(() => {
    if (!date) {
      setFormattedDate('');
    }
  }, [date, setFormattedDate]);

  const value = date && date.isValid() ? date.format(format) : formattedDate;

  const classes = cn(block(), {
    open: open,
  });
  const toDate = (mDate) => {
    return mDate && mDate.isValid() ? mDate.toDate() : undefined;
  };

  const calendarAriaAndDataProps = extractProps(rest, /^calendar-(aria|data)-/);
  for (const propName in calendarAriaAndDataProps) {
    // remove calendar prefix from prop names
    const nextPropName = propName.substring('calendar-'.length);
    calendarAriaAndDataProps[nextPropName] = calendarAriaAndDataProps[propName];
    delete calendarAriaAndDataProps[propName];
  }

  const popoverContent = (
    <Calendar
      date={toDate(date)}
      minDate={toDate(minDate)}
      maxDate={toDate(maxDate)}
      onSelect={handleSelect}
      data-testid="date-picker"
      {...calendarAriaAndDataProps}
    />
  );

  const textFieldProps = {
    placeholder,
    label: !showLabel ? '' : label,
  };

  return (
    <Popover
      portalZIndex={portalZIndex}
      className={element('popover')}
      contentClassName={element('popover-content')}
      hasPadding={false}
      content={popoverContent}
      closeOnTriggerClick={false}
      placement="bottom-start"
      open={open}
      noArrow={true}
      onToggle={handleToggle}>
      <div className={classes} {...extractDataAndAriaProps(rest)}>
        <TextField
          error={hasError}
          helperText={hasError ? errorText : ''}
          value={value}
          prefix={<Icon path={calendar} />}
          id={id}
          name={name}
          onChange={handleInputChange}
          onKeyUp={handleInputKeyUp}
          disabled={disabled}
          readOnly={readOnly}
          showClear={showClear}
          {...textFieldProps}
        />
      </div>
    </Popover>
  );
}
