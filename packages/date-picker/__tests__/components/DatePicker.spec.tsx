import React, {ReactElement} from 'react';
import {shallow} from 'enzyme';
import moment from 'moment';

import DatePicker from '../../src/components/DatePicker';
import {TextField} from '../../../text-field/src/index';
import {Popover} from '../../../popover/src';

describe('Date Picker', () => {
  const date = moment().year(2012).month(11).date(12).startOf('day');

  it('should render with no date', () => {
    const wrapper = shallow(<DatePicker />);

    expect(wrapper.find(TextField).props().value).toEqual('');
  });

  it('should render with default date format', () => {
    const wrapper = shallow(<DatePicker date={date} />);

    expect(wrapper.find(TextField).props().value).toEqual('12 Dec 2012');
  });

  it('should render with custom format', () => {
    const wrapper = shallow(<DatePicker date={date} format="LLL" />);

    expect(wrapper.find(TextField).props().value).toEqual(
      'December 12, 2012 12:00 AM'
    );
  });

  it('should change date by calendar', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<DatePicker onChange={onChange} />);
    const popover = wrapper.find(Popover);
    const calendar = shallow(popover.prop('content') as ReactElement);

    const nextDate = moment()
      .year(2016)
      .month(4)
      .date(4)
      .startOf('day')
      .toDate();

    calendar.simulate('select', nextDate);

    expect(onChange.mock.calls[0][0].isSame(nextDate)).toBe(true);
  });

  it('should select date by input', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<DatePicker onChange={onChange} />);
    const calendar = wrapper.find(TextField);
    const nextDate = '06 Jul 2015';

    const mDate = moment()
      .year(2015)
      .month(6)
      .date(6)
      .hours(0)
      .minutes(0)
      .seconds(0)
      .milliseconds(0);

    calendar.simulate('change', nextDate);

    expect(onChange.mock.calls[0][0].isSame(mDate)).toBe(true);
  });

  it('should select empty date by input', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<DatePicker onChange={onChange} />);
    const calendar = wrapper.find(TextField);
    const nextDate = '';

    calendar.simulate('change', nextDate);

    expect(onChange.mock.calls[0][0]).toBeNull();
  });

  it('should select error by input', () => {
    const onChange = jest.fn();
    const wrapper = shallow(<DatePicker onChange={onChange} />);
    const calendar = wrapper.find(TextField);
    const nextDate = '0s7/0s6/2s01s5';

    calendar.simulate('change', nextDate);

    expect(onChange.mock.calls[0][0].isValid()).toBe(false);
  });

  it('should render data- and aria- props', () => {
    const wrapper = shallow(
      <DatePicker data-test="datepicker-id" aria-role="date" />
    );

    expect(wrapper.find('[data-test="datepicker-id"]').exists()).toBe(true);
    expect(wrapper.find('[aria-role="date"]').exists()).toBe(true);
  });

  it('should pass calendar- prefixed props to calendar', () => {
    const wrapper = shallow(
      <DatePicker
        calendar-data-test="calendar-id"
        calendar-aria-role="calendar"
      />
    );

    const popoverContent = shallow(wrapper.props().content);

    expect(popoverContent.find('[data-test="calendar-id"]').exists()).toBe(
      true
    );
    expect(popoverContent.find('[aria-role="calendar"]').exists()).toBe(true);
  });
});
