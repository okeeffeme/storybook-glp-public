# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [18.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.34&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.35&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.34&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.34&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.32&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.33&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.31&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.32&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.30&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.31&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.29&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.30&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.28&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.29&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.27&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.28&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [18.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.26&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.27&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.25&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.26&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.24&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.25&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.23&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.24&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.22&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.23&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.21&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.22&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.20&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.21&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.19&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.20&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.18&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.19&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.17&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.18&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.15&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.15&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.14&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.15&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.13&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.14&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.12&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.11&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.10&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.9&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.8&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.7&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.4&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.4&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.4&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.3&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.2&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [18.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@18.0.0&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-date-picker

# [18.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.17&sourceBranch=refs/tags/@jotunheim/react-date-picker@18.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of DatePicker ([NPM-934](https://jiraosl.firmglobal.com/browse/NPM-934)) ([7565beb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7565bebcdfa92329a3ef77239e637996a9b44cb8))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [17.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.16&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.15&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.14&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.13&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.12&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.13&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [17.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.11&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.10&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.11&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [17.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.9&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.7&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.4&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.3&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.2&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.1&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-date-picker

## [17.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-date-picker@17.0.0&sourceBranch=refs/tags/@jotunheim/react-date-picker@17.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-date-picker

# 17.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [16.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.33&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.34&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.32&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.33&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.31&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.32&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [16.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.30&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.31&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.29&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.30&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.28&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.29&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.26&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.27&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.25&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.26&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.24&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.25&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.23&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.24&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.22&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.23&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.21&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.22&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.18&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.19&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.17&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.18&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.16&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.17&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.15&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.16&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.14&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.15&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.13&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.14&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.12&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.13&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.11&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.12&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [16.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.10&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.11&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.8&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.9&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.7&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.8&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.6&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.7&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.5&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.6&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.4&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.5&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.3&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.4&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.2&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.3&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.1&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.2&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-date-picker

## [16.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@16.0.0&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.1&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-date-picker

# [16.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.52&sourceBranch=refs/tags/@confirmit/react-date-picker@16.0.0&targetRepoId=1246) (2021-07-27)

### Bug Fixes

- default date format has been changed to DD MMM YYYY to be aligned with the Design System spec. ([NPM-497](https://jiraosl.firmglobal.com/browse/NPM-497)) ([b5ba00c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b5ba00ceb0cc812c02cd8335957b171131e14335))

### Features

- expose DatePicker default date format as named export DefaultDateFormat ([NPM-497](https://jiraosl.firmglobal.com/browse/NPM-497)) ([cf52079](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cf52079296da54193936ae827bea90949214bbf7))

### BREAKING CHANGES

- The default date format has changed from L to DD MMM YYYY. This could be problematic f.ex in tests that expect the date format to be a certain way.

## [15.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.51&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.52&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.50&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.51&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.49&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.50&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.48&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.49&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.47&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.48&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.46&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.47&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.45&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.46&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.44&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.45&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.43&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.44&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.42&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.43&targetRepoId=1246) (2021-06-02)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.41&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.42&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.40&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.41&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.39&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.40&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.38&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.39&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.37&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.38&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.36&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.37&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.34&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.35&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.33&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.34&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.32&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.33&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.31&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.32&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [15.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.30&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.31&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.29&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.30&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.28&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.29&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.27&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.28&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.26&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.27&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.25&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.26&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.24&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.23&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.21&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.20&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.19&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.18&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.17&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.18&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.16&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.15&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.14&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.13&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.12&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.11&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.10&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.9&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.6&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.3&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.2&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-date-picker

## [15.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@15.0.1&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-date-picker

# [15.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@14.0.1&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [15.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@14.0.1&sourceBranch=refs/tags/@confirmit/react-date-picker@15.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [14.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@14.0.1&sourceBranch=refs/tags/@confirmit/react-date-picker@14.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-date-picker

# [14.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.28&sourceBranch=refs/tags/@confirmit/react-date-picker@14.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [13.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.27&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.28&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.26&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.27&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.25&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.26&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.24&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.25&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.21&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.22&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.20&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.21&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.18&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.19&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.15&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.16&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.14&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.15&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.12&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.13&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-date-picker@13.0.10&sourceBranch=refs/tags/@confirmit/react-date-picker@13.0.11&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-date-picker@13.0.8...@confirmit/react-date-picker@13.0.9) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.7](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-date-picker@13.0.6...@confirmit/react-date-picker@13.0.7) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-date-picker

## [13.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-date-picker@13.0.0...@confirmit/react-date-picker@13.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-date-picker

# [13.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-date-picker@12.0.16...@confirmit/react-date-picker@13.0.0) (2020-08-12)

### Features

- Typescript support ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([b74232b](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/b74232b330d778c7960d68d535b8acfb8471cd89))

### BREAKING CHANGES

- popperModifiers property has been removed

## CHANGELOG

### v12.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-date-picker`

### v11.2.0

- Feat: Error text is moved to inside a tooltip

### v11.1.0

- Removing package version in class names.

### v11.0.6

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v11.0.0

- **BREAKING**
  - Remove inputClassName from being passed in to TextField as it's no longer used.
  - Updated to follow new DS specs by virtue of TextField updates.
  - Added prop: `showClear`
  - Removed default theme styling

### v10.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v9.0.0

- **BREAKING**
  - Date picker will now take 100% width, to be similar to our other input fields
  - Remove `icon` prop as it was not meant to be exposed

### v8.0.0

- **BREAKING**
  - removing confirmit-icons, potentially breaking for default-theme (increases height by 4px)
- Feat: using @confirmit/react-icons for default- and material-theme

### v7.0.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.

### v6.0.0

- **BREAKING**
  - Removed iconClassName prop: the icon will now be styled by TextField component.
- Update TextField prop usage: pass in CalendarIcon as prop instead of manually placing it.

### v5.1.0

- Update TextField prop usage: remove hideLabel from being passed in, instead only pass in label if showLabel is provided.

### v5.0.0

- **BREAKING**
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v4.1.2

- Fix: (default theme) - adjust calendar icon placement

### v4.1.0

- feat: new props `hasError` and `errorText` can be used to show a validation message when an invalid date is entered/selected.
- feat: new props `label`, `placeholder` and `showLabel`.
- feat: refactor to re-use TextField.

### v4.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v3.0.0

- ?

### v2.5.17

- It is now possible to set a class on the icon shown in the input field.

### v2.5.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

### v.2.4.0

Added CSS modules for scoping the CSS to the packages. Read more about the changes and how to adopt them in your project here: [How to modify the webpack config file](../../docs/CssModules.md)

### v.2.3.0

- Feat: Pass down data- and aria- attributes
- Feat: Pass down calendar-data- and calendar-aria- attributes to Calendar

### v2.1.0

- Added disable option

### v.2.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only
- **BREAKING** MomentPicker is now default export and renamed to DatePicker. Previous DatePicker is removed.
- **BREAKING** Calendar popover is now rendered in portal. This change may break some end2end tests.
- **BREAKING** Theming support is added. Consumer application has to import at least one theme

```js
// entry.js or application.js
import 'confirmit-date-picker/themes/default';
```
