import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {boolean, text} from '@storybook/addon-knobs';

import moment, {Moment} from 'moment';

import DatePicker from '../src/components/DatePicker';
import Popover from '../../popover/src';
import Button from '../../button/src';
import Calendar from '../../calendar/src';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{paddingLeft: '100px', paddingTop: '100px', width: '300px'}}>
    {children}
  </div>
);

const Overlay = ({children}) => (
  <div style={{height: '100px', width: '500px'}}>{children}</div>
);

/* eslint-disable  react/prop-types */
const InTheMiddleOfTallContent = ({children}) => (
  <div
    style={{
      height: '2000px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    {children}
  </div>
);
/* eslint-enable react/prop-types */

storiesOf('Components/date-picker', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <div style={{padding: '50px', height: '400px', backgroundColor: '#f1f1f1'}}>
      <Container>
        <DatePicker
          defaultDate={moment()}
          hasError={boolean('hasError', false)}
          errorText={text('errorText', 'some error message')}
          placeholder={text('placeholder', '')}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          showLabel={boolean('showLabel', true)}
          showClear={boolean('showClear', false)}
          label={text('label', 'Start Date')}
        />
      </Container>
    </div>
  ))
  .add('basic', () => (
    <Container>
      <DatePicker defaultDate={moment()} />
    </Container>
  ))
  .add('no date', () => (
    <Container>
      <DatePicker />
    </Container>
  ))
  .add('reset date', () => {
    const [date, setDate] = useState<Moment | null>(moment());
    return (
      <Container>
        <DatePicker date={date} onChange={setDate} />
        <Button onClick={() => setDate(null)}>Reset</Button>
      </Container>
    );
  })
  .add('custom format', () => (
    <Container>
      <DatePicker defaultDate={moment()} format={text('format', 'L')} />
    </Container>
  ))
  .add('in popover', () => (
    <InTheMiddleOfTallContent>
      <Popover
        content={
          <Overlay>
            <DatePicker defaultDate={moment()} />
          </Overlay>
        }>
        <Button>Trigger!</Button>
      </Popover>
    </InTheMiddleOfTallContent>
  ))
  .add('in calendar popover', () => {
    return (
      <InTheMiddleOfTallContent>
        <Calendar
          renderDayContent={({day, className}) => {
            return (
              <Popover
                content={
                  <Overlay>
                    <DatePicker defaultDate={moment()} />
                  </Overlay>
                }>
                <div className={className}>{day.date()}</div>
              </Popover>
            );
          }}
        />
      </InTheMiddleOfTallContent>
    );
  });
