# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.7&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.8&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.6&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.7&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.5&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.6&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.4&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.5&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.3&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.4&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.2&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.3&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [10.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.1&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.2&targetRepoId=1246) (2023-01-11)

### Bug Fixes

- adding data-testid TooltipStatic ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([dada85f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dada85f630e7b9523e96ef3922aa0cf2d976b943))

## [10.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.1.0&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.1&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-tooltip

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@10.0.0&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.1.0&targetRepoId=1246) (2022-10-13)

### Features

- re-export types from portal trigger ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([89a7b62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/89a7b62d6c0a8a7995c0a5c81692948123856d4f))

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.6&sourceBranch=refs/tags/@jotunheim/react-tooltip@10.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Tooltip ([NPM-960](https://jiraosl.firmglobal.com/browse/NPM-960)) ([b2ba1d0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b2ba1d0cc33bb53cd16e9fc726d8ce2fffe27ced))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.5&sourceBranch=refs/tags/@jotunheim/react-tooltip@9.0.6&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.4&sourceBranch=refs/tags/@jotunheim/react-tooltip@9.0.5&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.2&sourceBranch=refs/tags/@jotunheim/react-tooltip@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.1&sourceBranch=refs/tags/@jotunheim/react-tooltip@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-tooltip

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tooltip@9.0.0&sourceBranch=refs/tags/@jotunheim/react-tooltip@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-tooltip

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.11&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.12&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.10&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.11&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.9&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.10&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.8&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.9&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.7&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.8&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.6&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.7&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.5&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.6&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.4&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.5&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.3&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.4&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.2&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.3&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.1&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.1.0&sourceBranch=refs/tags/@confirmit/react-tooltip@8.1.1&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.0.4&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.5&targetRepoId=1246) (2021-02-11)

### Features

- TooltipStatic now supports a ref (now using forwardRef)

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.0.4&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.5&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.0.3&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.4&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.0.2&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.3&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-tooltip

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@8.0.0&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.1&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- remove theming support for transition timeouts, should just use defaults ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([2d341ca](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2d341cafe665db536ef1109991f7773a557d7f70))

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@7.0.0&sourceBranch=refs/tags/@confirmit/react-tooltip@8.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@6.0.3&sourceBranch=refs/tags/@confirmit/react-tooltip@7.0.0&targetRepoId=1246) (2020-10-26)

### Features

- bind portal position to viewport instead of scroll parent ([NPM-581](https://jiraosl.firmglobal.com/browse/NPM-581)) ([f7e2dd3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f7e2dd37ddd8e9b0f9682b34ed96b0167b0fe19a))

### BREAKING CHANGES

- There are no api changes, but change of how portal trigger tooltip is rendered.
  It now uses viewport instead scroll parent to determine its position, which in most cases a desired behaviour.
  If you need to use the old behaviour you can add:

```
const flipModifier: StrictModifiers = {
  name: 'flip',
  options: {
    altBoundary: true,
  },
};
const modifiers = useMemo(() => { return [flipModifier]}, [flipModifier])
return <Tooltip modifiers={modifiers} />
```

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@6.0.2&sourceBranch=refs/tags/@confirmit/react-tooltip@6.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-tooltip

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tooltip@6.0.1&sourceBranch=refs/tags/@confirmit/react-tooltip@6.0.2&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-tooltip

## [6.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tooltip@6.0.0...@confirmit/react-tooltip@6.0.1) (2020-09-08)

### Bug Fixes

- let PortalTrigger to handle empty content ([NPM-514](https://jiraosl.firmglobal.com/browse/NPM-514)) ([727bafe](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/727bafea9aaaf74d80e3f9cf41e5f267bd8530e9))

# [6.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tooltip@5.0.0...@confirmit/react-tooltip@6.0.0) (2020-08-12)

### Features

- rework Tooltip according to PortalTrigger changes ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([df5e3f4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/df5e3f4d0af70ca6f31dbead6f581fd99c26f905))

### BREAKING CHANGES

- - `Tooltip.popperModifiers` replaced with `Tooltip.modifiers` property. See
    https://popper.js.org/docs/v2/modifiers/.

* `Tooltip.Conditional` has been removed. `Tooltip` doesn't render `PortalTrigger`
  anymore if there are no portal content.
* Trigger element (child) must support ref and onMouseEnter, onMouseLeave properties.
* `Tooltip` support material theme only.

## CHANGELOG

### v5.0.0

- **BREAKING**: Tooltip is now always using viewport as boundaryElement.
  In case you need older behavior consider using `popperModifiers` property.

```
preventOverflow: {
  boundariesElement: 'scrollParent'
}
```

### v4.0.1

- Fix: Styles are rendered properly if popper changes its original placement cause of flip.

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.2.0

- Feat: Add `error` type style for Material tooltip.
- Fix: Ajust styles to match Design System spec.
  - Less padding
  - Solid background color
  - Font size

### v3.1.0

- Removing package version in class names.

### v3.0.7

- Bug fix: prevent text cut off in a tooltip with long text without space

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.2

- Fix: Arrow placement now correctly placed even when auto-placement takes over.

### v2.0.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.
- Removed peerDependencies
  - `hoist-non-react-statics`
  - `uncontrollable`

### v1.1.2

- Fix: (missing default prop for "hover", so previous version didn't work as intended)

### v1.1.0

- Feature: Tooltips are always slightly delayed when using "hover" trigger

### v1.0.0

- Feature: Tooltip.Conditional component, which only wraps the children in a tooltip if the tooltip is defined. If not it will just render the children.

### v0.1.2

- Bug fix: fix falling out arrow

### v0.0.1

- Initial version
