# Jotunheim React Tooltip

`Tooltip` is based on `PortalTrigger` component, so check its [README](../portal-trigger/README.md).

### Changelog

[Changelog](./CHANGELOG.md)

### Custom Trigger Element

There are few rules to follow if you would like to wrap custom trigger element with Tooltip.

```jsx
<Tooltip>
  <SomeCustomElement />
</Tooltip>
```

- SomeCustomElement should support ref. If this is functional component,
  then you should wrap it with forwardRef and assign ref to inner element.
- SomeCustomElement should pass onMouseEnter and onMouseLeave events to the ref element
  in case of "hover" trigger. This is necessary as PortalTrigger assigns those callbacks
  to the trigger element.

```jsx
import {forwardRef} from 'react';
import {extractOnEventProps} from '@jotunheim/react-utils';
forwardRef(function SomeCustomElement(props, ref) {
  return <div ref={ref} {...extractOnEventProps(props)} />;
});
```
