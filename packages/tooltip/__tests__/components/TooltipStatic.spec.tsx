import React from 'react';
import {render, screen} from '@testing-library/react';
import {TooltipStatic} from '../../src/components/TooltipStatic';

describe('TooltipStatic', () => {
  it('should render static tooltip', () => {
    render(<TooltipStatic>Content</TooltipStatic>);
    expect(screen.getByTestId('tooltipinner')).toHaveTextContent('Content');
  });
});
