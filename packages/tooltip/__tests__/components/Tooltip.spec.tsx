import React from 'react';
import {render, screen} from '@testing-library/react';
import Tooltip from '../../src/components/Tooltip';

describe('Tooltip', () => {
  it('should render error tooltip', () => {
    render(
      <Tooltip open={true} type={Tooltip.Types.error} content="Error!">
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.getByTestId('transition-portal');
    const tooltipContent = tooltip.querySelector('[data-tooltip-content]');

    expect(tooltip.querySelectorAll('[data-tooltip]').length).toBe(1);
    expect(screen.getByText('Trigger').textContent).toBe('Trigger');
    expect(tooltipContent).toHaveAttribute('data-tooltip-content-error');
    expect(tooltipContent?.textContent).toBe('Error!');
  });

  it('should render tooltip content when open', () => {
    render(
      <Tooltip open={true} content="Content!">
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.getByTestId('transition-portal');
    const tooltipContent = tooltip.querySelector('[data-tooltip-content]');

    expect(tooltip.querySelectorAll('[data-tooltip]').length).toBe(1);
    expect(screen.getByText('Trigger').textContent).toBe('Trigger');
    expect(tooltipContent).not.toHaveAttribute('data-tooltip-content-error');
    expect(tooltipContent?.textContent).toBe('Content!');
  });

  it('should not render tooltip content when not open', () => {
    render(
      <Tooltip defaultOpen={false} open={false} content="Content!">
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.queryByTestId('transition-portal');

    expect(tooltip).not.toBeInTheDocument();
  });

  it('should not render tooltip if content is undefined', () => {
    render(
      <Tooltip open content={undefined}>
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.queryByTestId('transition-portal');

    expect(tooltip).not.toBeInTheDocument();
  });

  it('should not render tooltip if content is null', () => {
    render(
      <Tooltip open={true} content={null}>
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.queryByTestId('transition-portal');

    expect(tooltip).not.toBeInTheDocument();
  });

  it('should not render tooltip if content is empty string', () => {
    render(
      <Tooltip open content={''}>
        <div className="trigger">Trigger</div>
      </Tooltip>
    );

    const tooltip = screen.queryByTestId('transition-portal');

    expect(tooltip).not.toBeInTheDocument();
  });
});
