import Tooltip from './components/Tooltip';
import TooltipStatic from './components/TooltipStatic';
import {
  StrictModifiers,
  Placement,
  PlacementTypes,
  TriggerTypes,
  Trigger,
  PopperRefInstance,
  TriggerRef,
  PopperRef,
  TriggerElement,
  TriggerElementProps,
} from '@jotunheim/react-portal-trigger';

export {Tooltip, TooltipStatic, PlacementTypes, TriggerTypes};
export type {
  StrictModifiers,
  Placement,
  Trigger,
  PopperRefInstance,
  TriggerRef,
  PopperRef,
  TriggerElement,
  TriggerElementProps,
};
export default Tooltip;
