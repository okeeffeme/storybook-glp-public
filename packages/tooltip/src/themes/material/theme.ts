import classNames from './css/tooltip.module.css';

export default {
  tooltip: {
    baseClassName: 'comd-tooltip',
    classNames,
  },
};
