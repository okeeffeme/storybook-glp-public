import {theme} from '@jotunheim/react-themes';

import materialTheme from './material/theme';

export default {
  [theme.themeNames.default]: materialTheme,
  [theme.themeNames.material]: materialTheme,
};
