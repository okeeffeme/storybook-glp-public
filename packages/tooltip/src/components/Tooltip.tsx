import React, {ReactNode, useState, forwardRef} from 'react';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import {
  PortalTrigger,
  Placement,
  PlacementTypes,
  Trigger,
  TriggerTypes,
  TriggerElement,
  StrictModifiers,
  PopperRef,
  TriggerRef,
} from '@jotunheim/react-portal-trigger';
import TooltipStatic from './TooltipStatic';

import componentThemes from '../themes';
import {TooltipTypes, Types} from './TooltipTypes';

const TOOLTIP_DELAY = 400; // https://ux.stackexchange.com/a/119975

type TooltipProps = {
  defaultOpen?: boolean;
  open?: boolean;
  placement?: Placement;
  children: TriggerElement;
  trigger?: Trigger;
  content?: ReactNode;
  type?: Types;
  positionFixed?: boolean;
  onToggle?: (open) => void;
  animation?: boolean;
  modifiers?: StrictModifiers[];
  popperRef?: PopperRef;
  portalZIndex?: number;
};

type ForwardRefTooltip = React.ForwardRefExoticComponent<
  TooltipProps & React.RefAttributes<HTMLElement | null>
> & {
  triggerTypes: typeof TriggerTypes;
  placementTypes: typeof PlacementTypes;
  Types: typeof TooltipTypes;
};

export const Tooltip: ForwardRefTooltip = forwardRef(function Tooltip(
  props: TooltipProps,
  ref: TriggerRef
) {
  const {
    defaultOpen,
    open,
    content,
    placement = PlacementTypes.bottom,
    children,
    type = TooltipTypes.info,
    trigger = TriggerTypes.hover,
    positionFixed,
    onToggle,
    animation,
    modifiers,
    popperRef,
    portalZIndex,
  } = props;

  const {baseClassName, classNames} = useTheme('tooltip', componentThemes);

  const {modifier} = bemFactory({baseClassName, classNames});
  const [popperPlacement, setPopperPlacement] = useState(placement);

  const transitionClassNames = {
    appear: modifier('appear'),
    appearActive: modifier('appear-active'),
    enter: modifier('enter'),
    enterActive: modifier('enter-active'),
    exit: modifier('leave'),
    exitActive: modifier('leave-active'),
  };

  return (
    <PortalTrigger
      ref={ref}
      popperRef={popperRef}
      defaultOpen={defaultOpen}
      open={open}
      onToggle={onToggle}
      animation={animation}
      positionFixed={positionFixed}
      modifiers={modifiers}
      portalZIndex={portalZIndex}
      overlay={
        content ? (
          <TooltipStatic
            staticPlacement={false}
            placement={popperPlacement}
            type={type}>
            {content}
          </TooltipStatic>
        ) : undefined
      }
      placement={placement}
      onPlacementChange={setPopperPlacement}
      trigger={trigger}
      delay={
        trigger === PortalTrigger.triggerTypes.hover ? TOOLTIP_DELAY : undefined
      }
      transitionClassNames={transitionClassNames}>
      {children}
    </PortalTrigger>
  );
}) as ForwardRefTooltip;

Tooltip.placementTypes = PortalTrigger.placementTypes;
Tooltip.triggerTypes = PortalTrigger.triggerTypes;
Tooltip.Types = TooltipTypes;
Tooltip.displayName = 'Tooltip';

export default Tooltip;
