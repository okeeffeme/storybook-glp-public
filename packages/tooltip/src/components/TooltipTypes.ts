export enum TooltipTypes {
  info = 'info',
  error = 'error',
}

export type Types = TooltipTypes | 'error' | 'info';
