import React, {ReactNode, Ref} from 'react';
import cn from 'classnames';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import {PlacementTypes, Placement} from '@jotunheim/react-portal-trigger';

import componentThemes from '../themes';
import {TooltipTypes, Types} from './TooltipTypes';

type TooltipStaticProps = {
  placement?: Placement;
  children: ReactNode;
  staticPlacement?: boolean;
  type?: Types;
};

export const TooltipStatic: ForwardRefTooltipStatic = React.forwardRef(
  (props: TooltipStaticProps, ref: Ref<HTMLDivElement>) => {
    const {
      children,
      placement = PlacementTypes.bottom,
      staticPlacement = true,
      type = TooltipTypes.info,
    } = props;

    const {baseClassName, classNames} = useTheme('tooltip', componentThemes);
    const {block, element, modifier} = bemFactory({baseClassName, classNames});

    const classes = cn(`${block()} ${modifier(placement)}`, {
      [modifier('static-placement')]: staticPlacement,
    });

    const contentDataAttributes = {
      'data-tooltip-content': '',
      ...(type === TooltipTypes.error && {'data-tooltip-content-error': ''}),
    };

    return (
      <div className={classes} data-tooltip="" ref={ref}>
        <div
          data-popper-arrow=""
          className={cn(element('arrow'), {
            [element('arrow', 'error')]: type === TooltipTypes.error,
          })}
        />
        <div
          className={cn(element('inner'), {
            [element('inner', 'error')]: type === TooltipTypes.error,
          })}
          {...contentDataAttributes}
          data-testid="tooltipinner">
          {children}
        </div>
      </div>
    );
  }
) as ForwardRefTooltipStatic;

type ForwardRefTooltipStatic = React.ForwardRefExoticComponent<
  TooltipStaticProps & React.RefAttributes<HTMLDivElement>
> & {
  types: typeof TooltipTypes;
};

export default TooltipStatic;
