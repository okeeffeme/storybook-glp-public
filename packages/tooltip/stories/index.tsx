import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Tooltip, TooltipStatic} from '../src';
import Button from '../../button/src';

storiesOf('Components/tooltip', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('static', () => (
    <div>
      <TooltipStatic>Bottom</TooltipStatic>
      <TooltipStatic placement="top">Top</TooltipStatic>
      <TooltipStatic placement="left">Left</TooltipStatic>
      <TooltipStatic placement="right">Right</TooltipStatic>
    </div>
  ))
  .add('hover', () => (
    <div style={{padding: '100px'}}>
      <Tooltip content={<div>Tooltip text</div>}>
        <Button>Hover me!</Button>
      </Tooltip>
    </div>
  ))
  .add('hover, with nested trigger content', () => (
    <Tooltip content={<div>Tooltip text</div>}>
      <div style={{margin: '10px', border: '1px solid #ccc'}}>
        <Button>Hover me!</Button>
      </div>
    </Tooltip>
  ))
  .add('with scrolling', () => (
    <div style={{marginTop: '400px', marginBottom: '400px'}}>
      <Tooltip content={<div>Tooltip text</div>}>
        <Button>Bottom!</Button>
      </Tooltip>
      <Tooltip content={<div>Tooltip text</div>} placement="top">
        <Button>Top!</Button>
      </Tooltip>
      <Tooltip content={<div>Tooltip text</div>} placement="left">
        <Button>Left!</Button>
      </Tooltip>
      <Tooltip content={<div>Tooltip text</div>} placement="right">
        <Button>Right!</Button>
      </Tooltip>
    </div>
  ))
  .add('placements', () => (
    <div style={{padding: '100px'}}>
      <div>
        <h1>Bottom/Top/Left/Right</h1>
        <Tooltip content={<div>Tooltip text</div>} defaultOpen={true}>
          <Button>Bottom!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="top"
          defaultOpen={true}>
          <Button>Top!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="left"
          defaultOpen={true}>
          <Button>Left!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="right"
          defaultOpen={true}>
          <Button>Right!</Button>
        </Tooltip>
      </div>
      <div>
        <h1>Bottom-start/Top-start/Left-start/Right-start</h1>
        <Tooltip
          content={<div>Tooltip text</div>}
          defaultOpen={true}
          placement="bottom-start">
          <Button>Bottom-start!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="top-start"
          defaultOpen={true}>
          <Button>Top-start!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="left-start"
          defaultOpen={true}>
          <Button>Left-start!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="right-start"
          defaultOpen={true}>
          <Button>Right-start!</Button>
        </Tooltip>
      </div>
      <div>
        <h1>Bottom-end/Top-end/Left-end/Right-end</h1>
        <Tooltip
          content={<div>Tooltip text</div>}
          defaultOpen={true}
          placement="bottom-end">
          <Button>Bottom-end!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="top-end"
          defaultOpen={true}>
          <Button>Top-end!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="left-end"
          defaultOpen={true}>
          <Button>Left-end!</Button>
        </Tooltip>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="right-end"
          defaultOpen={true}>
          <Button>Right-end!</Button>
        </Tooltip>
      </div>
      <div>
        <h1>Default placement</h1>
        <Tooltip content={<div>Tooltip text</div>} defaultOpen={true}>
          <Button>Default!</Button>
        </Tooltip>
      </div>
      <div>
        <h1>Auto</h1>
        <Tooltip
          content={<div>Tooltip text</div>}
          placement="auto"
          defaultOpen={true}>
          <Button>Auto!</Button>
        </Tooltip>
      </div>
    </div>
  ))
  .add('click', () => (
    <div style={{padding: '100px'}}>
      <Tooltip content={<div>Tooltip text</div>} trigger="click">
        <Button>Click me!</Button>
      </Tooltip>
    </div>
  ))
  .add('open by default', () => (
    <Tooltip
      content={<div>Tooltip text</div>}
      trigger="click"
      defaultOpen={true}>
      <Button>Click me!</Button>
    </Tooltip>
  ))
  .add('hover with no animation', () => (
    <Tooltip animation={false} content={<div>Tooltip text</div>}>
      <Button>Hover me!</Button>
    </Tooltip>
  ))
  .add('click with no animation', () => (
    <Tooltip
      animation={false}
      content={<div>Tooltip text</div>}
      trigger="click">
      <Button>Click me!</Button>
    </Tooltip>
  ))
  .add('no content', () => (
    <Tooltip>
      <Button>No need to click</Button>
    </Tooltip>
  ))
  .add('always open', () => (
    <Tooltip
      open
      trigger={Tooltip.triggerTypes.none}
      content={<div>Tooltip text</div>}>
      <Button>No need to click</Button>
    </Tooltip>
  ))
  .add('advanced tooltip', () => (
    <Tooltip
      trigger="click"
      content={
        <div>
          <Tooltip content={'Hello!'}>
            <Button appearance={Button.appearances.primaryDanger}>
              Button inside tooltip
            </Button>
          </Tooltip>
        </div>
      }>
      <Button appearance={Button.appearances.primarySuccess}>
        There is a button
      </Button>
    </Tooltip>
  ))
  .add('long tooltip content', () => (
    <Tooltip
      content={
        <div>
          Super long! Super long! Super long! Super long! Super long! Super
          long! Super long!
        </div>
      }>
      <Button appearance={Button.appearances.primarySuccess}>Super Long</Button>
    </Tooltip>
  ))
  .add('long tooltip content with no spaces', () => (
    <Tooltip
      content={
        <div>
          Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!Superlong!
        </div>
      }>
      <Button appearance={Button.appearances.primarySuccess}>Super Long</Button>
    </Tooltip>
  ))
  .add('validation error tooltip', () => (
    <Tooltip
      content={<div>Validation error!</div>}
      open
      type={Tooltip.Types.error}>
      <input type="text" defaultValue="!" />
    </Tooltip>
  ));
