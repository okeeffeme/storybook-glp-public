import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Popover from '../../src/components/Popover';
import {TriggerTypes} from '../../../portal-trigger/src';

jest.useFakeTimers();

describe('Popover', () => {
  it('should render popover with positionFixed', () => {
    render(
      <Popover content={<span>Content!</span>} positionFixed={true}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.getByTestId('transition-portal').parentElement).toHaveStyle(
      'position: fixed;'
    );
  });

  it('should render popover with no positionFixed by default', () => {
    render(
      <Popover content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.getByTestId('transition-portal').parentElement).toHaveStyle(
      'position: absolute;'
    );
  });

  it('should render popover with animation disabled by default', () => {
    render(
      <Popover content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('transition-portal')).toHaveAttribute(
      'data-animation',
      'false'
    );
  });

  it('should render popover with animation disabled if trigger is click', () => {
    render(
      <Popover trigger={TriggerTypes.click} content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('transition-portal')).toHaveAttribute(
      'data-animation',
      'false'
    );
  });

  it('should render popover with animation enabled if trigger is hover', () => {
    render(
      <Popover trigger={TriggerTypes.hover} content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    jest.runAllTimers();

    expect(screen.queryByTestId('transition-portal')).toHaveAttribute(
      'data-animation',
      'true'
    );
  });

  it('should change arrow styles based on PortalTrigger placement', () => {
    const {rerender} = render(
      <Popover content={<span>Content!</span>} placement="bottom">
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.getByTestId('popover')).toHaveAttribute(
      'data-position',
      'bottom'
    );

    userEvent.click(screen.getByText(/trigger/i));

    rerender(
      <Popover content={<span>Content!</span>} placement="top">
        <span>Trigger</span>
      </Popover>
    );
  });

  it('should render arrow', () => {
    render(
      <Popover content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.getByTestId('popper-arrow')).toBeInTheDocument();
  });

  it('should not render arrow', () => {
    render(
      <Popover noArrow={true} content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('popper-arrow')).not.toBeInTheDocument();
  });

  it('should pass undefined content to PortalTrigger if content is empty string', () => {
    render(
      <Popover content={''}>
        <span data-trigger="">Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('popover-content')).not.toBeInTheDocument();
  });

  it('should pass undefined content to PortalTrigger if content is null', () => {
    render(
      <Popover content={null}>
        <span data-trigger="">Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('popover-content')).not.toBeInTheDocument();
  });

  it('should pass undefined content to PortalTrigger if content is undefined', () => {
    render(
      <Popover content={undefined}>
        <span data-trigger="">Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(screen.queryByTestId('popover-content')).not.toBeInTheDocument();
  });

  it('should add padding class modifier by default', () => {
    render(
      <Popover content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(
      screen
        .getByText(/content/i)
        .parentElement?.classList.contains('comd-popover__content--padding')
    ).toBe(true);
  });

  it('should not add padding class modifier when hasPadding is false', () => {
    render(
      <Popover hasPadding={false} content={<span>Content!</span>}>
        <span>Trigger</span>
      </Popover>
    );

    userEvent.click(screen.getByText(/trigger/i));

    expect(
      screen
        .getByText(/content/i)
        .parentElement?.classList.contains('comd-popover__content--padding')
    ).toBe(false);
  });
});
