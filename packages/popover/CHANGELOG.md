# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.6&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.7&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.5&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.6&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.4&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.5&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.3&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.4&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [9.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.2&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.3&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.1&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.2&targetRepoId=1246) (2023-01-20)

### Bug Fixes

- add test id for Popover component ([NPM-1203](https://jiraosl.firmglobal.com/browse/NPM-1203)) ([b47fc25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b47fc2582c269aba002876998054d38a3d9e5c87))

## [9.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.1.0&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.1&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-popover

# [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.7&sourceBranch=refs/tags/@jotunheim/react-popover@9.1.0&targetRepoId=1246) (2022-10-13)

### Features

- re-export types from portal trigger ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([bab599b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bab599b85d10b12dd7ba03c9cbf82040235d5d62))

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.6&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.7&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.5&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.6&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.4&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.5&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.2&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.1&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-popover

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-popover@9.0.0&sourceBranch=refs/tags/@jotunheim/react-popover@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-popover

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.11&sourceBranch=refs/tags/@confirmit/react-popover@8.0.12&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.10&sourceBranch=refs/tags/@confirmit/react-popover@8.0.11&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.9&sourceBranch=refs/tags/@confirmit/react-popover@8.0.10&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.8&sourceBranch=refs/tags/@confirmit/react-popover@8.0.9&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.7&sourceBranch=refs/tags/@confirmit/react-popover@8.0.8&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.6&sourceBranch=refs/tags/@confirmit/react-popover@8.0.7&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.5&sourceBranch=refs/tags/@confirmit/react-popover@8.0.6&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.4&sourceBranch=refs/tags/@confirmit/react-popover@8.0.5&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.3&sourceBranch=refs/tags/@confirmit/react-popover@8.0.4&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.2&sourceBranch=refs/tags/@confirmit/react-popover@8.0.3&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-popover

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.1&sourceBranch=refs/tags/@confirmit/react-popover@8.0.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@8.0.0&sourceBranch=refs/tags/@confirmit/react-popover@8.0.1&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-popover

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@7.1.4&sourceBranch=refs/tags/@confirmit/react-popover@8.0.0&targetRepoId=1246) (2021-03-10)

### Features

- hasPadding prop can be set to false to turn off internal padding in popover content ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([b7ff3b5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b7ff3b5fdac875a8504bdc3a617c13b720c013aa))

### BREAKING CHANGES

- As part of this, the padding on popover content has been increased to 16px to align with Design System specs. Previously it was 3px top/bottom and 8px left/right.
- hasPadding prop can be set to false to turn off internal padding in popover content (NPM-632)

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@7.1.2&sourceBranch=refs/tags/@confirmit/react-popover@7.1.3&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-popover

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@7.1.1&sourceBranch=refs/tags/@confirmit/react-popover@7.1.2&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-popover

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@7.1.0&sourceBranch=refs/tags/@confirmit/react-popover@7.1.1&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-popover

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@7.0.0&sourceBranch=refs/tags/@confirmit/react-popover@7.1.0&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- use globals for transition duration ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([b297fbe](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b297fbe098cab97eb623c0eb272e87139cd54f46))
- use same transition timeout for leave as for enter ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([a14391a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a14391a2166e332d09dcbdee075e7f128d12521b))

### Features

- add a slight delay when trigger is set to hover ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([bc7d893](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bc7d893b2838ca4c01a67258fa9c77182fda7723))

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@6.0.0&sourceBranch=refs/tags/@confirmit/react-popover@7.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@5.0.3&sourceBranch=refs/tags/@confirmit/react-popover@6.0.0&targetRepoId=1246) (2020-10-26)

### Features

- bind portal position to viewport instead of scroll parent ([NPM-581](https://jiraosl.firmglobal.com/browse/NPM-581)) ([f7e2dd3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f7e2dd37ddd8e9b0f9682b34ed96b0167b0fe19a))

### BREAKING CHANGES

- There are no api changes, but change of how portal trigger tooltip is rendered.
  It now uses viewport instead scroll parent to determine its position, which in most cases a desired behaviour.
  If you need to use the old behaviour you can add:

```
const flipModifier: StrictModifiers = {
  name: 'flip',
  options: {
    altBoundary: true,
  },
};
const modifiers = useMemo(() => { return [flipModifier]}, [flipModifier])
return <Popover modifiers={modifiers} />
```

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@5.0.2&sourceBranch=refs/tags/@confirmit/react-popover@5.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-popover

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-popover@5.0.1&sourceBranch=refs/tags/@confirmit/react-popover@5.0.2&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-popover

## [5.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-popover@5.0.0...@confirmit/react-popover@5.0.1) (2020-09-08)

### Bug Fixes

- let PortalTrigger to handle empty content ([NPM-514](https://jiraosl.firmglobal.com/browse/NPM-514)) ([dafdd44](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/dafdd44fe47b966c62d274f44d31bba77e0863d6))

# [5.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-popover@4.0.3...@confirmit/react-popover@5.0.0) (2020-08-12)

### Features

- rework Popover according to changes in PortalTrigger ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([1219c97](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/1219c9792f9b5a5d49273072d617d0e3c5e61809))

### BREAKING CHANGES

- - Popover.popperModifiers`replaced with`Popover.modifiers` property.
    See https://popper.js.org/docs/v2/modifiers/.

* Trigger element (child) must support ref and onMouseEnter, onMouseLeave properties.
* `Popover` support material theme only.

## CHANGELOG

### v4.0.1

- Fix: Styles are rendered properly if popper changes its original placement cause of flip.

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.2.0

- Removing package version in class names.

### v3.1.0

- Feature: Add portalZIndex to Popover component

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.7

- Fix: Add extractDataAriaIdProps to Popover Overlay component and modify PortalTrigger rest props to use excludeDataAndAriaProps.

### v2.0.3

- Fix: Remove `baseClassName` default prop. This was incorrectly passed as rest to PortalTrigger, causing the classname `co-popper` to be added to the wrapped child of popper. This class has some styles attached to it, which caused the wrapped child to have unexpected styles attached to it.

### v2.0.1

- Fix: Arrow placement now correctly placed even when auto-placement takes over.

### v2.0.0

- **BREAKING** - changed to function component (no longer supports ref) - Removed props: baseClassName, classNames

### v1.0.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.
- Removed peerDependencies
  - `hoist-non-react-statics`
  - `uncontrollable`

### v0.2.0

- Bug fix: fix falling out arrow
- Feature: new prop "positionFixed" which it false by default

### v0.0.1

- Initial version
