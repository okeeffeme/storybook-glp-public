import React, {forwardRef, ReactNode, useState} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {
  PortalTrigger,
  Placement,
  PlacementTypes,
  Trigger,
  TriggerTypes,
  TriggerElement,
  PopperRef,
  TriggerRef,
  StrictModifiers,
} from '@jotunheim/react-portal-trigger';

import classNames from './Popover.module.css';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-popover',
  classNames,
});

type PopoverProps = {
  defaultOpen?: boolean;
  open?: boolean;
  onToggle?: (open: boolean) => void;
  placement?: Placement;
  positionFixed?: boolean;
  children: TriggerElement;
  content?: ReactNode;
  className?: string;
  arrowClassName?: string;
  contentClassName?: string;
  trigger?: Trigger;
  noArrow?: boolean;
  portalZIndex?: number;
  closeOnOutsideClick?: boolean;
  closeOnTriggerClick?: boolean;
  closeOnPortalClick?: boolean;
  hasPadding?: boolean;
  popperRef?: PopperRef;
  modifiers?: StrictModifiers[];
};

const TRIGGER_DELAY = 400; // https://ux.stackexchange.com/a/119975

type ForwardRefPopover = React.ForwardRefExoticComponent<
  PopoverProps & React.RefAttributes<HTMLElement | null>
> & {
  triggerTypes: typeof TriggerTypes;
  placementTypes: typeof PlacementTypes;
};

const Popover: ForwardRefPopover = forwardRef(
  (props: PopoverProps, ref: TriggerRef) => {
    const {
      popperRef,
      closeOnTriggerClick,
      closeOnOutsideClick,
      closeOnPortalClick = false,
      defaultOpen,
      open,
      onToggle,
      noArrow = false,
      portalZIndex,
      content,
      placement = PlacementTypes.bottom,
      positionFixed = false,
      children,
      className,
      arrowClassName,
      contentClassName,
      modifiers,
      hasPadding = true,
      trigger = TriggerTypes.click,
      ...rest
    } = props;

    const [actualPlacement, setActualPlacement] = useState(placement);
    const classes = cn(
      block(),
      {
        [modifier(actualPlacement)]: true,
        [modifier('no-arrow')]: noArrow,
      },
      className
    );

    const transitionClassNames = {
      appear: modifier('appear'),
      appearActive: modifier('appear-active'),
      enter: modifier('enter'),
      enterActive: modifier('enter-active'),
      exit: modifier('leave'),
      exitActive: modifier('leave-active'),
    };

    const arrowClasses = cn(element('arrow'), arrowClassName);
    const contentClasses = cn(element('content'), contentClassName, {
      [element('content', 'padding')]: hasPadding,
    });

    const overlay = content ? (
      <div
        className={classes}
        data-popover=""
        data-testid="popover"
        data-position={actualPlacement}
        {...extractDataAriaIdProps(rest)}>
        {!noArrow && (
          <div
            className={arrowClasses}
            data-popper-arrow=""
            data-testid="popper-arrow"
          />
        )}
        <div className={contentClasses} data-test="popover-content">
          {content}
        </div>
      </div>
    ) : undefined;

    const animation = trigger === PortalTrigger.triggerTypes.hover;

    return (
      <PortalTrigger
        ref={ref}
        modifiers={modifiers}
        popperRef={popperRef}
        defaultOpen={defaultOpen}
        open={open}
        onToggle={onToggle}
        trigger={trigger}
        delay={animation ? TRIGGER_DELAY : undefined}
        animation={animation}
        closeOnPortalClick={closeOnPortalClick}
        closeOnOutsideClick={closeOnOutsideClick}
        closeOnTriggerClick={closeOnTriggerClick}
        overlay={overlay}
        placement={placement}
        onPlacementChange={setActualPlacement}
        positionFixed={positionFixed}
        portalZIndex={portalZIndex}
        transitionClassNames={transitionClassNames}>
        {children}
      </PortalTrigger>
    );
  }
) as ForwardRefPopover;

Popover.placementTypes = PortalTrigger.placementTypes;
Popover.triggerTypes = PortalTrigger.triggerTypes;
Popover.displayName = 'Popover';

export default Popover;
