import Popover from './components/Popover';
import {
  StrictModifiers,
  Placement,
  PlacementTypes,
  TriggerTypes,
  Trigger,
  TriggerElement,
  TriggerElementProps,
  TriggerRef,
  PopperRefInstance,
  PopperRef,
} from '@jotunheim/react-portal-trigger';

export {Popover, PlacementTypes, TriggerTypes};
export type {
  StrictModifiers,
  Placement,
  Trigger,
  TriggerElement,
  TriggerElementProps,
  TriggerRef,
  PopperRefInstance,
  PopperRef,
};
export default Popover;
