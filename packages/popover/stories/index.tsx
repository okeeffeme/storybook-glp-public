import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Popover, TriggerTypes} from '../src';
import Button from '../../button/src';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{padding: '200px'}}>{children}</div>
);

/* eslint-disable-next-line */
const Overlay = ({children}) => <div style={{margin: '30px'}}>{children}</div>;

/* eslint-disable react/prop-types */
const OverlayContent = ({children, onSave, onCancel}) => (
  <Overlay>
    <div>{children}</div>
    <Button appearance={Button.appearances.primarySuccess} onClick={onSave}>
      Save
    </Button>
    <Button onClick={onCancel}>Cancel</Button>
  </Overlay>
);

storiesOf('Components/popover', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('uncontrolled', () => (
    <Container>
      <Popover content={<span>Use this information with care</span>}>
        <Button>Trigger!</Button>
      </Popover>
    </Container>
  ))
  .add('uncontrolled / trigger on hover', () => (
    <Container>
      <Popover
        trigger={TriggerTypes.hover}
        content={<span>Use this information with care</span>}>
        <Button>Hover to trigger</Button>
      </Popover>
    </Container>
  ))
  .add('uncontrolled / no content', () => (
    <Container>
      <Popover>
        <Button>Trigger!</Button>
      </Popover>
    </Container>
  ))
  .add('uncontrolled / open by default', () => (
    <Container>
      <Popover
        defaultOpen
        content={<span>Use this information with care</span>}>
        <Button>Trigger!</Button>
      </Popover>
    </Container>
  ))
  .add('uncontrolled / placements', () => (
    <div
      style={{
        paddingLeft: '300px',
        paddingRight: '300px',
      }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '200px',
        }}>
        <Popover
          defaultOpen
          placement="bottom-start"
          content={<span>Use this information with care</span>}>
          <Button>bottom-start</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="bottom"
          content={<span>Use this information with care</span>}>
          <Button>bottom</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="bottom-end"
          content={<span>Use this information with care</span>}>
          <Button>bottom-end</Button>
        </Popover>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '200px',
        }}>
        <Popover
          defaultOpen
          placement="top-start"
          content={<span>Use this information with care</span>}>
          <Button>top-start</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="top"
          content={<span>Use this information with care</span>}>
          <Button>top</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="top-end"
          content={<span>Use this information with care</span>}>
          <Button>top-end</Button>
        </Popover>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '200px',
        }}>
        <Popover
          defaultOpen
          placement="left-start"
          content={<span>Use this information with care</span>}>
          <Button>left-start</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="left"
          content={<span>Use this information with care</span>}>
          <Button>left</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="left-end"
          content={<span>Use this information with care</span>}>
          <Button>left-end</Button>
        </Popover>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '200px',
        }}>
        <Popover
          defaultOpen
          placement="right-start"
          content={<span>Use this information with care</span>}>
          <Button>right-start</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="right"
          content={<span>Use this information with care</span>}>
          <Button>right</Button>
        </Popover>
        <Popover
          defaultOpen
          placement="right-end"
          content={<span>Use this information with care</span>}>
          <Button>right-end</Button>
        </Popover>
      </div>
    </div>
  ))
  .add('controlled', () => {
    const [open, setIsOpen] = React.useState(false);

    const content = (
      <OverlayContent
        onSave={() => setIsOpen(false)}
        onCancel={() => setIsOpen(false)}>
        Based on this content please decide to click either cancel or save
      </OverlayContent>
    );

    return (
      <Container>
        <Popover
          open={open}
          content={content}
          onToggle={(newState) => setIsOpen(newState)}>
          <Button>Trigger!</Button>
        </Popover>
      </Container>
    );
  })
  .add('controlled / always open', () => (
    <Container>
      <Popover
        open
        trigger={Popover.triggerTypes.none}
        content={<span>Use this information with care</span>}>
        <Button>Trigger!</Button>
      </Popover>
    </Container>
  ))
  .add('controlled / no close click outside', () => {
    const [open, setIsOpen] = React.useState(false);

    const content = (
      <OverlayContent
        onSave={() => setIsOpen(false)}
        onCancel={() => setIsOpen(false)}>
        Based on this content please decide to click either cancel or save
      </OverlayContent>
    );
    return (
      <Container>
        <Popover
          open={open}
          content={content}
          onToggle={(newState) => setIsOpen(newState)}
          closeOnOutsideClick={false}>
          <Button>Trigger!</Button>
        </Popover>
      </Container>
    );
  })
  .add('nested popovers', () => (
    <Container>
      <Popover
        defaultOpen={false}
        content={
          <Popover
            defaultOpen={false}
            content={
              <Popover
                defaultOpen={false}
                content={'If you click here popovers must not be closed'}>
                <Button>Grand Child Trigger</Button>
              </Popover>
            }>
            <Button>Child Trigger!</Button>
          </Popover>
        }>
        <Button>Trigger!</Button>
      </Popover>
    </Container>
  ));
