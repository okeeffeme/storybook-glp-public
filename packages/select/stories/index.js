import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {boolean, text} from '@storybook/addon-knobs';
import TextField from '../../text-field/src/index';
import Popover from '../../popover/src';
import Button from '../../button/src/';
import {Fieldset} from '../../fieldset/src';

import Select, {DrillDown, NonceProvider} from '../src/index';
import {LayoutContext, InputWidth} from '../../contexts/src';

const label = {
  marginTop: 60,
  paddingBottom: 10,
  fontWeight: 'bold',
};

const drillDownOptions = [
  {
    value: 1,
    label: 'Node 1 abc',
  },
  {
    value: 2,
    label: 'Node 2 def',
  },
  {
    value: 3,
    label: 'Node 3 klm',
  },
  {
    value: 4,
    label: 'Node 4',
    options: [
      {
        value: 5,
        label: 'Node 5 abc asdvasd vasdv sadv sadv sadv sadv sadv dvad',
      },
      {
        value: 6,
        label: 'Node 6sdv asdv asdv sdavsadvasdv asdv sadv asdv sad vsad sdav!',
        options: [
          {
            value: 7,
            label:
              'Node 7 xyz asdv asdv sadv asdv sadv sadv asdv asdv asdv adv',
          },
          {
            value: 8,
            label: 'Node 8 def',
          },
        ],
      },
    ],
  },
];

storiesOf('Components/select', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Select', () => {
    const [withSearchValue, setWithSearchValue] = React.useState('1');
    const [withOptionsArrayValue, setWithOptionsArrayValue] =
      React.useState('1');
    const [withoutSelection, setWithoutSelection] = React.useState();
    const [multipleSelectValue, setMultipleSelectValue] = React.useState([]);
    const [multipleSearchSelectValue, setMultipleSearchSelectValue] =
      React.useState([]);
    const [
      multipleSearchSelectCustomValue,
      setMultipleSearchSelectCustomValue,
    ] = React.useState([]);
    return (
      <NonceProvider cacheKey="lowercasealpha">
        <div
          style={{
            padding: '100px',
            paddingBottom: '400px',
            backgroundColor: '#f1f1f1',
          }}>
          <div style={label}>Select with Search</div>
          <Select
            value={withSearchValue}
            onChange={setWithSearchValue}
            label="Source"
            required={true}
            isOptionDisabled={(option) => option.value === '3'}>
            <Select.Option value="1">Option 1</Select.Option>
            <Select.Option value="2">Option 2</Select.Option>
            <Select.Option value="3">Option 3 (disabled)</Select.Option>
            <Select.Option value="4">Option 4</Select.Option>
            <Select.Option value="5">Option 5</Select.Option>
            <Select.Option value="6">Option 6</Select.Option>
            <Select.Option value="7">
              <div style={{backgroundColor: 'lightsteelblue'}}>
                Option 7 (with markup)
              </div>
            </Select.Option>
          </Select>

          <div style={label}>Select with options array</div>
          <Select
            value={withOptionsArrayValue}
            label="Source"
            required={true}
            onChange={setWithOptionsArrayValue}
            options={[
              {value: 1, label: 'Option 1'},
              {value: 2, label: 'Option 2'},
              {value: 3, isDisabled: true, label: 'Option 3'},
            ]}
          />

          <div style={label}>Select without selection and without search</div>
          <Select
            value={withoutSelection}
            label="Source"
            onChange={setWithoutSelection}
            isSearchable={false}
            options={[
              {value: 1, label: 'Option 1'},
              {value: 2, label: 'Option 2'},
              {value: 3, label: 'Option 3'},
            ]}
          />

          <div style={label}>Multiple Select</div>
          <Select
            value={multipleSelectValue}
            onChange={setMultipleSelectValue}
            isMulti={true}
            isSearchable={false}
            isClearable={true}
            label="Label">
            <Select.Option value="1">Option 1</Select.Option>
            <Select.Option value="2">Option 2</Select.Option>
            <Select.Option value="3">Option 3</Select.Option>
            <Select.Option value="4">Option 4</Select.Option>
            <Select.Option value="5">Option 5</Select.Option>
            <Select.Option value="6">Option 6</Select.Option>
          </Select>

          <div style={label}>Multiple Searchable Select</div>
          <Select
            value={multipleSearchSelectValue}
            onChange={setMultipleSearchSelectValue}
            label="Languages"
            isMulti={true}
            isSearchable={true}
            isClearable={true}>
            <Select.Option value="1">English</Select.Option>
            <Select.Option value="2">Norwegian</Select.Option>
            <Select.Option value="3">Indian</Select.Option>
            <Select.Option value="4">Greek</Select.Option>
            <Select.Option value="5">Canadian</Select.Option>
            <Select.Option value="6">Russian</Select.Option>
          </Select>

          <div style={label}>
            Multiple searchable select with custom item/tag creation
          </div>
          <Select
            value={multipleSearchSelectCustomValue}
            onChange={setMultipleSearchSelectCustomValue}
            label="Keywords"
            isMulti={true}
            isCreatable={true}
            isValidNewOption={(inputValue /*, selectValue, selectOptions*/) => {
              return inputValue !== 'invalid';
            }}
            helperText="Select system keywords from list, or type to enter custom keywords"
            createPrefix="Create custom keyword: ">
            <Select.Option value="Benchmark">Benchmark</Select.Option>
            <Select.Option value="Library">Library</Select.Option>
            <Select.Option value="Registration survey">
              Registration survey
            </Select.Option>
            <Select.Option value="Update profile survey">
              Update profile survey
            </Select.Option>
          </Select>
        </div>
      </NonceProvider>
    );
  })
  .add('Drill Down - Multi Select', () => {
    const [value, setValue] = React.useState([
      drillDownOptions[0],
      drillDownOptions[3].options[1],
    ]);
    return (
      <div style={{padding: '100px'}}>
        <DrillDown
          value={value}
          onChange={setValue}
          error={boolean('error', false)}
          isMulti={boolean('isMulti', true)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          required={boolean('required', false)}
          isCreatable={boolean('isCreatable', false)}
          isClearable={boolean('isClearable', true)}
          helperText={text('helperText', '')}
          placeholder={text('placeholder', '')}
          options={drillDownOptions}
          label="Select levels"
        />
      </div>
    );
  })
  .add('Drill Down - Single Select', () => {
    const [value, setValue] = React.useState(drillDownOptions[0].value);
    return (
      <div style={{padding: '100px'}}>
        <DrillDown
          value={value}
          onChange={setValue}
          error={boolean('error', false)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          required={boolean('required', false)}
          isCreatable={boolean('isCreatable', false)}
          isClearable={boolean('isClearable', true)}
          helperText={text('helperText', '')}
          placeholder={text('placeholder', '')}
          options={drillDownOptions}
          label="Select levels"
        />
      </div>
    );
  })
  .add('With knobs', () => {
    const [options, setOptions] = React.useState([
      {value: '1', label: 'Option 1'},
      {value: '2', label: 'Option 2'},
      {value: '3', label: 'Option 3 is longer than others!'},
    ]);

    const [singleValue, setSingleValue] = React.useState(options[0].value);
    const [multiValue, setMultiValue] = React.useState([options[0]]);

    const isMulti = boolean('isMulti', false);

    const onChange = (value) => {
      if (isMulti) {
        setMultiValue(value);
      } else {
        setSingleValue(value);
      }
    };

    const value = isMulti ? multiValue : singleValue;

    return (
      <div style={{padding: '100px'}}>
        <Select
          error={boolean('error', false)}
          isMulti={isMulti}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          required={boolean('required', false)}
          isCreatable={boolean('isCreatable', false)}
          isClearable={boolean('isClearable', false)}
          label={text('label', 'Label')}
          helperText={text('helperText', '')}
          placeholder={text('placeholder', '')}
          truncateValueFromLeft={boolean('truncateValueFromLeft', false)}
          value={value}
          onChange={onChange}
          options={options}
          onCreateOption={(text) => {
            const newOption = {value: text, label: text};

            if (
              !options.find((o) => {
                return o.value === text;
              })
            ) {
              setOptions([newOption, ...options]);
            }

            onChange(newOption);
          }}
        />
      </div>
    );
  })
  .add('with label', () => {
    const [value, setValue] = React.useState('1');

    return (
      <div style={{padding: '100px'}}>
        <Select
          value={value}
          onChange={setValue}
          isClearable={true}
          label="Choose a number">
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">
            Option 2 - very very very long label that should make the menu
            options container wider than the select itself
          </Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('with React.node as helperText', () => {
    const [value, setValue] = useState('1');
    return (
      <div style={{padding: '100px'}}>
        <Select
          helperText={
            <div>
              <ul>
                <li>This is the first thing</li>
                <li>This is the second thing</li>
                <li>This is the third thing</li>
              </ul>
            </div>
          }
          value={value}
          onChange={setValue}
          isClearable={true}
          label="Choose a number">
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">
            Option 2 - very very very long label that should make the menu
            options container wider than the select itself
          </Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('Multi with several selected values', () => {
    const options = [
      {value: '1', label: 'Value 1'},
      {value: '2', label: 'Value 2'},
      {value: '3', label: 'Value 3'},
    ];
    const [value, setValue] = React.useState([options[0], options[2]]);

    return (
      <div style={{padding: '100px'}}>
        <Select
          value={value}
          onChange={setValue}
          isMulti={true}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          options={options}
        />
      </div>
    );
  })
  .add('With isClearable', () => {
    const [value, setValue] = React.useState('1');

    return (
      <div style={{padding: '100px'}}>
        <Select isClearable={true} value={value} onChange={setValue}>
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">Option 2</Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('With autoFocus and label', () => {
    const [value, setValue] = React.useState('1');

    return (
      <div style={{padding: '100px'}}>
        <Select
          autoFocus={true}
          value={value}
          onChange={setValue}
          label="Select one">
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">Option 2</Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('With defaultText', () => {
    const [value, setValue] = React.useState();

    return (
      <div style={{padding: '100px'}}>
        <Select
          value={value}
          onChange={setValue}
          defaultText="Please select a value">
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">Option 2</Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('With header and no value', () => {
    const [value, setValue] = React.useState('2');

    return (
      <div style={{padding: '100px'}}>
        <Select value={value} onChange={setValue}>
          <Select.Option header>List of Option</Select.Option>
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">Option 2</Select.Option>
          <Select.Option header>Last Option</Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
      </div>
    );
  })
  .add('With many options and headers with value', () => {
    const options = [];

    for (let i = 1; i <= 200; i++) {
      if ((i - 1) % 10 === 0) {
        options.push(
          <Select.Option header key={`header_${i}`} value={`header_${i}`}>
            Options {i}-{i + 9}
          </Select.Option>
        );
      }
      options.push(
        <Select.Option key={i} value={i}>
          Option {i}
        </Select.Option>
      );
    }
    const [value, setValue] = React.useState(1);

    return (
      <div style={{padding: '100px'}}>
        <Select value={value} onChange={setValue}>
          {options}
        </Select>
      </div>
    );
  })
  .add('Next to text-field', () => {
    const [value1, setValue1] = React.useState();
    const [value2, setValue2] = React.useState('Option 1');

    return (
      <div style={{padding: '100px', display: 'flex'}}>
        <Select
          label="Label"
          isClearable={true}
          value={value1}
          onChange={setValue1}>
          <Select.Option value="1">Option 1</Select.Option>
          <Select.Option value="2">Option 2</Select.Option>
          <Select.Option value="3">Option 3</Select.Option>
        </Select>
        <TextField
          label="Label"
          value={value2}
          onChange={setValue2}
          showClear={true}
        />
      </div>
    );
  })
  .add('With zero value', () => {
    const [value, setValue] = React.useState(0);

    return (
      <div style={{padding: '100px'}}>
        <Select
          value={value}
          onChange={setValue}
          isClearable={true}
          label="Choose a number">
          <Select.Option value={0}>Option 0</Select.Option>
          <Select.Option value={1}>Option 1</Select.Option>
          <Select.Option value={2}>Option 2</Select.Option>
        </Select>
      </div>
    );
  })
  .add('Async', () => {
    const delay = (waitMs) =>
      new Promise((resolve) => {
        setTimeout(resolve, waitMs);
      });

    const types = ['Even', 'Odd'];
    const typeOptions = types.map((t) => ({value: t, label: t}));

    const [typeOption, setTypeOption] = useState(null);
    const [digitOption, setDigitOption] = useState(null);

    const options = new Array(287)
      .fill(null)
      .map((v, idx) => ({label: `Item ${idx + 1}`, value: idx}));

    const loadOptions = React.useCallback(
      async (inputValue, previousOptions, {page}) => {
        await delay(1500);

        // very naive pagination implementation, please don't copy this...
        const PAGE_SIZE = 50;
        const start = (page - 1) * PAGE_SIZE;
        const newOptions = options
          .filter((o) => o.label.indexOf(inputValue) > -1)
          .slice(start, start + PAGE_SIZE);

        return {
          options: newOptions,
          hasMore: newOptions.length > 0,
          additional: {
            page: page + 1,
          },
        };
      },
      [options]
    );

    const loadGroupedOptions = React.useCallback(
      async (inputValue, previousOptions, {page}) => {
        await delay(1500);

        // very naive pagination implementation, please don't copy this...
        const PAGE_SIZE = 10;
        const start = (page - 1) * PAGE_SIZE;
        const newOptions = options
          .filter((o) => o.label.indexOf(inputValue) > -1)
          .slice(start, start + PAGE_SIZE);

        return {
          options: [
            {
              label: 'Group 1',
              options: newOptions,
            },
            {
              label: 'Group 2',
              options: newOptions.slice(),
            },
          ],
          hasMore: newOptions.length > 0,
          additional: {
            page: page + 1,
          },
        };
      },
      [options]
    );

    const loadDigitOptions = React.useCallback(async () => {
      const options = {
        Odd: [1, 3, 5].map((digit) => ({value: digit, label: digit})),
        Even: [2, 4, 6].map((digit) => ({value: digit, label: digit})),
      };

      await delay(1500);
      return {options: options[typeOption], hasMore: false};
    }, [typeOption]);

    const [value1, setValue1] = React.useState({value: 67, label: 'Item 67'});
    const [value2, setValue2] = React.useState(null);

    return (
      <div style={{padding: '100px'}}>
        <div style={label}>With knobs</div>
        <Select
          value={value1}
          onChange={setValue1}
          loadOptions={loadOptions}
          error={boolean('error', false)}
          isMulti={boolean('isMulti', false)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          required={boolean('required', false)}
          isCreatable={boolean('isCreatable', false)}
          isClearable={boolean('isClearable', false)}
          label={text('label', 'Search for a value')}
          helperText={text('helperText', '')}
          placeholder={text('placeholder', '')}
        />

        <div style={label}>With grouping of options</div>
        <Select
          value={value2}
          onChange={setValue2}
          loadOptions={loadGroupedOptions}
          reduceOptions={(prevOptions, loadedOptions) => {
            /*
                Group new items as well by the label of the group,
                adding them to the same group if the label has already been loaded.
                The built-in "reduceGroupedOptions" method will group by label, but you can override this to add your
                own grouping logic.
                */
            return Select.reduceGroupedOptions(prevOptions, loadedOptions);
          }}
        />

        <div style={label}>With uncached options</div>
        <Select
          value={typeOption}
          onChange={(value) => {
            setTypeOption(value);
            setDigitOption(null);
          }}
          options={typeOptions}
        />
        <Select
          value={digitOption}
          onChange={setDigitOption}
          loadOptions={loadDigitOptions}
          disabled={!typeOption}
          varyCacheBy={[typeOption]}
        />
      </div>
    );
  })
  .add('Inside popover', () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState('');

    const content = (
      <div style={{padding: 20, width: 200, height: 200}}>
        <Select
          value={value}
          onChange={setValue}
          options={[
            {value: '1', label: 'Options 1'},
            {value: '2', label: 'Options 2'},
            {value: '3', label: 'Options 3'},
          ]}
        />
      </div>
    );

    return (
      <div style={{padding: '100px'}}>
        <Popover
          open={open}
          content={content}
          onToggle={(open) => setOpen(open)}>
          <Button>Open popover</Button>
        </Popover>
      </div>
    );
  })
  .add('Inside popover: Async', () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState('');

    const content = (
      <div style={{padding: 20, width: 200, height: 200}}>
        <Select
          value={value}
          onChange={setValue}
          loadOptions={() => ({
            options: [
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ],
            hasMore: false,
            additional: {page: 1},
          })}
        />
      </div>
    );
    return (
      <div style={{padding: '100px'}}>
        <Popover
          open={open}
          content={content}
          onToggle={(open) => setOpen(open)}>
          <Button>Open popover</Button>
        </Popover>
      </div>
    );
  })
  .add('With Layout Wrapper (max600min80), showing max width', () => {
    const [value, setValue] = useState('');

    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 800, padding: 40, background: '#f1f1f1'}}>
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
        </div>
      </LayoutContext.Provider>
    );
  })
  .add('With Layout Wrapper (max600min80), showing min width', () => {
    const [value, setValue] = useState('');

    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 60, padding: 40, background: '#f1f1f1'}}>
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
          <Select
            value={value}
            onChange={setValue}
            options={[
              {value: '1', label: 'Options 1'},
              {value: '2', label: 'Options 2'},
              {value: '3', label: 'Options 3'},
            ]}
          />
        </div>
      </LayoutContext.Provider>
    );
  })
  .add('Overriding with CSS Variables', () => {
    const [multiValue, setMultiValue] = useState('');
    const [singleValue, setSingleValue] = useState('');
    const [bgColor, setBgColor] = useState('');
    const [textColor, setTextColor] = useState('');

    return (
      <div
        style={{
          padding: '10px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-around',
          height: '40vh',
        }}>
        <h3>
          We've added CSS variables to meet Studio Specific business needs,
          please do not use these without consulting the DS team.
        </h3>
        <p>
          You can check out the global-styles README{' '}
          <b>Use CSS variables in your sass files</b> section for more
          information.
        </p>
        <br />
        <p>Enter custom colors to update the CSS variables</p>
        <div
          style={{
            '--ds-color-base-contrast-low': bgColor,
            '--ds-color-base': textColor,
          }}>
          <Fieldset>
            <Select
              value={multiValue}
              onChange={setMultiValue}
              isMulti={true}
              isSearchable={true}
              isClearable={true}
              label="Multi Select">
              <Select.Option value="1">Option 1</Select.Option>
              <Select.Option value="2">Option 2</Select.Option>
              <Select.Option value="3">Option 3</Select.Option>
            </Select>
            <Select
              value={singleValue}
              onChange={setSingleValue}
              isSearchable={true}
              isClearable={true}
              label="Single Select">
              <Select.Option value="1">Option 1</Select.Option>
              <Select.Option value="2">Option 2</Select.Option>
              <Select.Option value="3">Option 3</Select.Option>
            </Select>
          </Fieldset>
        </div>
        <Fieldset>
          <TextField
            label="Background Color"
            value={bgColor}
            onChange={setBgColor}
            showClear={true}
          />
          <TextField
            label="Text Color"
            value={textColor}
            onChange={setTextColor}
            showClear={true}
          />
        </Fieldset>
      </div>
    );
  });
