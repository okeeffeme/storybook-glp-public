# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [7.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.61&sourceBranch=refs/tags/@jotunheim/react-select@7.0.62&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.61&sourceBranch=refs/tags/@jotunheim/react-select@7.0.61&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.59&sourceBranch=refs/tags/@jotunheim/react-select@7.0.60&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.58&sourceBranch=refs/tags/@jotunheim/react-select@7.0.59&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.57&sourceBranch=refs/tags/@jotunheim/react-select@7.0.58&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.56&sourceBranch=refs/tags/@jotunheim/react-select@7.0.57&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.55&sourceBranch=refs/tags/@jotunheim/react-select@7.0.56&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.54&sourceBranch=refs/tags/@jotunheim/react-select@7.0.55&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.53&sourceBranch=refs/tags/@jotunheim/react-select@7.0.54&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.52&sourceBranch=refs/tags/@jotunheim/react-select@7.0.53&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.51&sourceBranch=refs/tags/@jotunheim/react-select@7.0.52&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.50&sourceBranch=refs/tags/@jotunheim/react-select@7.0.51&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.49&sourceBranch=refs/tags/@jotunheim/react-select@7.0.50&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.48&sourceBranch=refs/tags/@jotunheim/react-select@7.0.49&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.47&sourceBranch=refs/tags/@jotunheim/react-select@7.0.48&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.46&sourceBranch=refs/tags/@jotunheim/react-select@7.0.47&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- export select types ([NPM-1237](https://jiraosl.firmglobal.com/browse/NPM-1237)) ([bb3fb0a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb3fb0a8a73145fda3d033e697040186f13d84fa))

## [7.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.45&sourceBranch=refs/tags/@jotunheim/react-select@7.0.46&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.44&sourceBranch=refs/tags/@jotunheim/react-select@7.0.45&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add test ids to Select component ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([95c84ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/95c84ffde7f2dfa167a161913f5694a757e49c43))
- adding data-testid to Select component ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([5c8c66b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5c8c66b4f9ac11570ef0a8f69c443ca6a123a9c8))
- adding props in Select components ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([600f0d9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/600f0d9d75f06860c521ef9229fd2006d68aa44d))

## [7.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.43&sourceBranch=refs/tags/@jotunheim/react-select@7.0.44&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.42&sourceBranch=refs/tags/@jotunheim/react-select@7.0.43&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.41&sourceBranch=refs/tags/@jotunheim/react-select@7.0.42&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.40&sourceBranch=refs/tags/@jotunheim/react-select@7.0.41&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.39&sourceBranch=refs/tags/@jotunheim/react-select@7.0.40&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.38&sourceBranch=refs/tags/@jotunheim/react-select@7.0.39&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.34&sourceBranch=refs/tags/@jotunheim/react-select@7.0.38&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.34&sourceBranch=refs/tags/@jotunheim/react-select@7.0.37&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.34&sourceBranch=refs/tags/@jotunheim/react-select@7.0.36&targetRepoId=1246) (2023-01-18)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.34&sourceBranch=refs/tags/@jotunheim/react-select@7.0.35&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.33&sourceBranch=refs/tags/@jotunheim/react-select@7.0.34&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.32&sourceBranch=refs/tags/@jotunheim/react-select@7.0.33&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.31&sourceBranch=refs/tags/@jotunheim/react-select@7.0.32&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.30&sourceBranch=refs/tags/@jotunheim/react-select@7.0.31&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.29&sourceBranch=refs/tags/@jotunheim/react-select@7.0.30&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.28&sourceBranch=refs/tags/@jotunheim/react-select@7.0.29&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.27&sourceBranch=refs/tags/@jotunheim/react-select@7.0.28&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.26&sourceBranch=refs/tags/@jotunheim/react-select@7.0.27&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.23&sourceBranch=refs/tags/@jotunheim/react-select@7.0.26&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.23&sourceBranch=refs/tags/@jotunheim/react-select@7.0.25&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.23&sourceBranch=refs/tags/@jotunheim/react-select@7.0.24&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.22&sourceBranch=refs/tags/@jotunheim/react-select@7.0.23&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.21&sourceBranch=refs/tags/@jotunheim/react-select@7.0.22&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.20&sourceBranch=refs/tags/@jotunheim/react-select@7.0.21&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.18&sourceBranch=refs/tags/@jotunheim/react-select@7.0.19&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.17&sourceBranch=refs/tags/@jotunheim/react-select@7.0.18&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.16&sourceBranch=refs/tags/@jotunheim/react-select@7.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.15&sourceBranch=refs/tags/@jotunheim/react-select@7.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.14&sourceBranch=refs/tags/@jotunheim/react-select@7.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.13&sourceBranch=refs/tags/@jotunheim/react-select@7.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.12&sourceBranch=refs/tags/@jotunheim/react-select@7.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.11&sourceBranch=refs/tags/@jotunheim/react-select@7.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.10&sourceBranch=refs/tags/@jotunheim/react-select@7.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.9&sourceBranch=refs/tags/@jotunheim/react-select@7.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.7&sourceBranch=refs/tags/@jotunheim/react-select@7.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.4&sourceBranch=refs/tags/@jotunheim/react-select@7.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.3&sourceBranch=refs/tags/@jotunheim/react-select@7.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.2&sourceBranch=refs/tags/@jotunheim/react-select@7.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.1&sourceBranch=refs/tags/@jotunheim/react-select@7.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-select

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-select@7.0.0&sourceBranch=refs/tags/@jotunheim/react-select@7.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-select

# 7.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [6.5.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.76&sourceBranch=refs/tags/@confirmit/react-select@6.5.77&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.75&sourceBranch=refs/tags/@confirmit/react-select@6.5.76&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.74&sourceBranch=refs/tags/@confirmit/react-select@6.5.75&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.73&sourceBranch=refs/tags/@confirmit/react-select@6.5.74&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [6.5.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.72&sourceBranch=refs/tags/@confirmit/react-select@6.5.73&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.71&sourceBranch=refs/tags/@confirmit/react-select@6.5.72&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.70&sourceBranch=refs/tags/@confirmit/react-select@6.5.71&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.68&sourceBranch=refs/tags/@confirmit/react-select@6.5.69&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.67&sourceBranch=refs/tags/@confirmit/react-select@6.5.68&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.66&sourceBranch=refs/tags/@confirmit/react-select@6.5.67&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.65&sourceBranch=refs/tags/@confirmit/react-select@6.5.66&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.64&sourceBranch=refs/tags/@confirmit/react-select@6.5.65&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.63&sourceBranch=refs/tags/@confirmit/react-select@6.5.64&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.58&sourceBranch=refs/tags/@confirmit/react-select@6.5.59&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.57&sourceBranch=refs/tags/@confirmit/react-select@6.5.58&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.56&sourceBranch=refs/tags/@confirmit/react-select@6.5.57&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.55&sourceBranch=refs/tags/@confirmit/react-select@6.5.56&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.54&sourceBranch=refs/tags/@confirmit/react-select@6.5.55&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.53&sourceBranch=refs/tags/@confirmit/react-select@6.5.54&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.52&sourceBranch=refs/tags/@confirmit/react-select@6.5.53&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.51&sourceBranch=refs/tags/@confirmit/react-select@6.5.52&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.50&sourceBranch=refs/tags/@confirmit/react-select@6.5.51&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.49&sourceBranch=refs/tags/@confirmit/react-select@6.5.50&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.48&sourceBranch=refs/tags/@confirmit/react-select@6.5.49&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.47&sourceBranch=refs/tags/@confirmit/react-select@6.5.48&targetRepoId=1246) (2021-12-07)

### Bug Fixes

- drilldown menulist component no longer renders ([NPM-896](https://jiraosl.firmglobal.com/browse/NPM-896)) ([bb1505f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb1505f9a03d5c23abd7e9458730716718a22629))

## [6.5.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.46&sourceBranch=refs/tags/@confirmit/react-select@6.5.47&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.45&sourceBranch=refs/tags/@confirmit/react-select@6.5.46&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.44&sourceBranch=refs/tags/@confirmit/react-select@6.5.45&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.43&sourceBranch=refs/tags/@confirmit/react-select@6.5.44&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.5.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.42&sourceBranch=refs/tags/@confirmit/react-select@6.5.43&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.40&sourceBranch=refs/tags/@confirmit/react-select@6.5.41&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.39&sourceBranch=refs/tags/@confirmit/react-select@6.5.40&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.38&sourceBranch=refs/tags/@confirmit/react-select@6.5.39&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.37&sourceBranch=refs/tags/@confirmit/react-select@6.5.38&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.36&sourceBranch=refs/tags/@confirmit/react-select@6.5.37&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.35&sourceBranch=refs/tags/@confirmit/react-select@6.5.36&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.34&sourceBranch=refs/tags/@confirmit/react-select@6.5.35&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.33&sourceBranch=refs/tags/@confirmit/react-select@6.5.34&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.32&sourceBranch=refs/tags/@confirmit/react-select@6.5.33&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.31&sourceBranch=refs/tags/@confirmit/react-select@6.5.32&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.30&sourceBranch=refs/tags/@confirmit/react-select@6.5.31&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.29&sourceBranch=refs/tags/@confirmit/react-select@6.5.30&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.28&sourceBranch=refs/tags/@confirmit/react-select@6.5.29&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.27&sourceBranch=refs/tags/@confirmit/react-select@6.5.28&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.26&sourceBranch=refs/tags/@confirmit/react-select@6.5.27&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.25&sourceBranch=refs/tags/@confirmit/react-select@6.5.26&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.24&sourceBranch=refs/tags/@confirmit/react-select@6.5.25&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.23&sourceBranch=refs/tags/@confirmit/react-select@6.5.24&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.22&sourceBranch=refs/tags/@confirmit/react-select@6.5.23&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.21&sourceBranch=refs/tags/@confirmit/react-select@6.5.22&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.20&sourceBranch=refs/tags/@confirmit/react-select@6.5.21&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.19&sourceBranch=refs/tags/@confirmit/react-select@6.5.20&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.18&sourceBranch=refs/tags/@confirmit/react-select@6.5.19&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.17&sourceBranch=refs/tags/@confirmit/react-select@6.5.18&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.16&sourceBranch=refs/tags/@confirmit/react-select@6.5.17&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.15&sourceBranch=refs/tags/@confirmit/react-select@6.5.16&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.14&sourceBranch=refs/tags/@confirmit/react-select@6.5.15&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.13&sourceBranch=refs/tags/@confirmit/react-select@6.5.14&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.12&sourceBranch=refs/tags/@confirmit/react-select@6.5.13&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.11&sourceBranch=refs/tags/@confirmit/react-select@6.5.12&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.10&sourceBranch=refs/tags/@confirmit/react-select@6.5.11&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.9&sourceBranch=refs/tags/@confirmit/react-select@6.5.10&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.7&sourceBranch=refs/tags/@confirmit/react-select@6.5.8&targetRepoId=1246) (2021-05-05)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.6&sourceBranch=refs/tags/@confirmit/react-select@6.5.7&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.5&sourceBranch=refs/tags/@confirmit/react-select@6.5.6&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.4&sourceBranch=refs/tags/@confirmit/react-select@6.5.5&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.3&sourceBranch=refs/tags/@confirmit/react-select@6.5.4&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.2&sourceBranch=refs/tags/@confirmit/react-select@6.5.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [6.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.1&sourceBranch=refs/tags/@confirmit/react-select@6.5.2&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-select

## [6.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.5.0&sourceBranch=refs/tags/@confirmit/react-select@6.5.1&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-select

# [6.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.4.2&sourceBranch=refs/tags/@confirmit/react-select@6.5.0&targetRepoId=1246) (2021-03-26)

### Features

- expose NonceProvider so apps can set a custom cachekey for emotion to avoid conflicting styling issues for React-Select ([NPM-751](https://jiraosl.firmglobal.com/browse/NPM-751)) ([a3cc910](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a3cc9100c0b19d24eefadeca960183214df7cfeb))

## [6.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.4.1&sourceBranch=refs/tags/@confirmit/react-select@6.4.2&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-select

## [6.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.4.0&sourceBranch=refs/tags/@confirmit/react-select@6.4.1&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-select

# [6.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.3.0&sourceBranch=refs/tags/@confirmit/react-select@6.4.0&targetRepoId=1246) (2021-03-24)

### Features

- single Drilldown has parent nodes open for the selected node ([NPM-736](https://jiraosl.firmglobal.com/browse/NPM-736)) ([86ddbb1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/86ddbb172cb2435783fefe360604934516d023c6))

# [6.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.5&sourceBranch=refs/tags/@confirmit/react-select@6.3.0&targetRepoId=1246) (2021-03-19)

### Features

- Added the option for setting the truncation direction of the selected value ([NPM-738](https://jiraosl.firmglobal.com/browse/NPM-738)) ([66a1abc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/66a1abc9faff029655d3c53264efc154900bb859))

## [6.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.4&sourceBranch=refs/tags/@confirmit/react-select@6.2.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-select

## [6.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.3&sourceBranch=refs/tags/@confirmit/react-select@6.2.4&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-select

## [6.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.2&sourceBranch=refs/tags/@confirmit/react-select@6.2.3&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-select

## [6.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.1&sourceBranch=refs/tags/@confirmit/react-select@6.2.2&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.2.0&sourceBranch=refs/tags/@confirmit/react-select@6.2.1&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-select

# [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.1.4&sourceBranch=refs/tags/@confirmit/react-select@6.2.0&targetRepoId=1246) (2021-03-09)

### Features

- Select/unselect node by clicking the node row for Drilldown component ([NPM-737](https://jiraosl.firmglobal.com/browse/NPM-737)) ([91a1c5b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/91a1c5b1d1ef1656881878ad6ec288a6e64bad4c))

## [6.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.1.3&sourceBranch=refs/tags/@confirmit/react-select@6.1.4&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-select

## [6.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.1.2&sourceBranch=refs/tags/@confirmit/react-select@6.1.3&targetRepoId=1246) (2021-02-22)

**Note:** Version bump only for package @confirmit/react-select

## [6.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.1.1&sourceBranch=refs/tags/@confirmit/react-select@6.1.2&targetRepoId=1246) (2021-02-19)

**Note:** Version bump only for package @confirmit/react-select

# [6.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.29&sourceBranch=refs/tags/@confirmit/react-select@6.1.0&targetRepoId=1246) (2021-02-18)

### Bug Fixes

- fixed string type value is not mapped to nested options correctly ([922caaa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/922caaa26436734f4436a134e0503615eb1712a5))

### Features

- add support for single-select drilldown ([24a3951](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/24a3951465b9f0159e48cf2eb75b138b6679cd6f))

## [6.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.28&sourceBranch=refs/tags/@confirmit/react-select@6.0.29&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.26&sourceBranch=refs/tags/@confirmit/react-select@6.0.27&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.25&sourceBranch=refs/tags/@confirmit/react-select@6.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.24&sourceBranch=refs/tags/@confirmit/react-select@6.0.25&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.22&sourceBranch=refs/tags/@confirmit/react-select@6.0.23&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.21&sourceBranch=refs/tags/@confirmit/react-select@6.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.20&sourceBranch=refs/tags/@confirmit/react-select@6.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.19&sourceBranch=refs/tags/@confirmit/react-select@6.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.18&sourceBranch=refs/tags/@confirmit/react-select@6.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.17&sourceBranch=refs/tags/@confirmit/react-select@6.0.18&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.16&sourceBranch=refs/tags/@confirmit/react-select@6.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.15&sourceBranch=refs/tags/@confirmit/react-select@6.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.14&sourceBranch=refs/tags/@confirmit/react-select@6.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.13&sourceBranch=refs/tags/@confirmit/react-select@6.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.12&sourceBranch=refs/tags/@confirmit/react-select@6.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.11&sourceBranch=refs/tags/@confirmit/react-select@6.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.10&sourceBranch=refs/tags/@confirmit/react-select@6.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.9&sourceBranch=refs/tags/@confirmit/react-select@6.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.6&sourceBranch=refs/tags/@confirmit/react-select@6.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.3&sourceBranch=refs/tags/@confirmit/react-select@6.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.2&sourceBranch=refs/tags/@confirmit/react-select@6.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-select

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@6.0.1&sourceBranch=refs/tags/@confirmit/react-select@6.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-select

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@5.0.1&sourceBranch=refs/tags/@confirmit/react-select@6.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@5.0.1&sourceBranch=refs/tags/@confirmit/react-select@6.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@5.0.1&sourceBranch=refs/tags/@confirmit/react-select@5.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-select

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.10&sourceBranch=refs/tags/@confirmit/react-select@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.9&sourceBranch=refs/tags/@confirmit/react-select@4.3.10&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([c688338](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c6883386087e63a6d480d9a9a3ba982a9e30926b))

## [4.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.8&sourceBranch=refs/tags/@confirmit/react-select@4.3.9&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.6&sourceBranch=refs/tags/@confirmit/react-select@4.3.7&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.5&sourceBranch=refs/tags/@confirmit/react-select@4.3.6&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.2&sourceBranch=refs/tags/@confirmit/react-select@4.3.3&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.1&sourceBranch=refs/tags/@confirmit/react-select@4.3.2&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.3.0&sourceBranch=refs/tags/@confirmit/react-select@4.3.1&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-select

## [4.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs%2Ftags%2F%40confirmit%2Freact-select%404.2.9&sourceBranch=refs%2Ftags%2F%40confirmit%2Freact-select%404.3.0&targetRepoId=1246) (2020-10-6)

### Features

- Support select in popover

## [4.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.2.15&sourceBranch=refs/tags/@confirmit/react-select@4.2.16&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.2.14&sourceBranch=refs/tags/@confirmit/react-select@4.2.15&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.2.12&sourceBranch=refs/tags/@confirmit/react-select@4.2.13&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-select@4.2.10&sourceBranch=refs/tags/@confirmit/react-select@4.2.11&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.8](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.2.7...@confirmit/react-select@4.2.8) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.2.5...@confirmit/react-select@4.2.6) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.2.3...@confirmit/react-select@4.2.4) (2020-08-27)

**Note:** Version bump only for package @confirmit/react-select

## [4.2.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.2.0...@confirmit/react-select@4.2.1) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-select

# [4.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.1.17...@confirmit/react-select@4.2.0) (2020-08-16)

### Features

- Added `ReadOnly` prop

## [4.1.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.1.16...@confirmit/react-select@4.1.17) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-select

## [4.1.15](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-select@4.1.14...@confirmit/react-select@4.1.15) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-select

## CHANGELOG

### v4.1.6

- Fix: Inputs should have white background

### v4.1.0

- Feat: Added new DrillDown component
  - Supports nested options

### v4.0.3

- Fix: Remove prop-type warning for option.label; allow react nodes

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.3.0

- Feat: Helper text is moved to inside a tooltip

### v3.2.8

- Fix: a hash was added into the middle of the classNames because of the property `classNamePrefix` passed to the react-simple-select. Now the hash is not added in the middle anymore.

### v3.2.1

- Fix: Make sure class-names created by react-select using the classNamePrefix are set

### v3.2.0

- Removing package version in class names.

### v3.1.0

- Feature: Now supports async loading via new props "loadOptions" and "reduceOptions" (refer to stories for instructions)

### v3.0.2

- Fix: Remove unused styled and internal components

### v3.0.1

- Fix: Remove some redundant styles, leftover from transition to new DS spec.

### v3.0.0

- **BREAKING**:
  - Updated to follow new DS specs by virtue of using the new InputWrapper component.

### v2.0.4

- Using new simple select base component

### v2.0.3

- Fix: More strict check for invalid value, only null and undefined are treated as invalid now.
  0 number is valid value.

### v2.0.0

- **BREAKING**:

  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.4.2

- Fix: No longer requires react-dropdown (hence not relying on react-transition-portal either)

### v1.4.0

- Feat: Now using transform transition for label animation
- Fix: Better match text-field styling

### v1.3.0

- Add "className" prop to select component, which will be rendered on root div element

### v1.2.3

- Fix: Reduce size to be same height as TextField to align when displayed next to eachother
- Fix: Add missing hover effect

### v1.2.1

- Move data-attributes and aria attributes to the root div element (ReactSelect doesn't render them)

### v1.2.0

- Add disabled chip for multi-select, determined by the `disabled` prop from select component

### v1.1.0

- new version of Chip

### v1.0.0

- **BREAKING**:

  - No longer supports "default" theme, will render with material theme regardless
  - Completely rewritten to use 3rd party React Select under the hood
  - Removed className props: `dropdownClassName`, `menuClassName`, `textClassName`, `toggleClassName`, `labelClassName`
  - Removed theme related props: `baseClassName`, `classNames`, `BorderRenderer`
  - Removed prop: `popperModifiers`
  - Deprecated `defaultText`, use `placeholder` instead
  - `Select.Option` rewritten to function component, no longer supports ref
  - No longer supports `uncontrollable`:

    - `onChange` handler must be provided
    - Removed props: `open`, `onToggle` (not possible to control open/closed state from the outside)

  - New features:
    - `isMulti` prop, for multi-select
      - `value` prop can be an array of objects
      - Menu will stay open when selecting items
    - `isCreatable` prop to support adding items that are not in the list
    - `isValidNewOption` callback to determine if new value is valid
    - `isClearable` to show button for clearing selected value
    - `isSearchable` for searching for values (defaults to true)
    - `filterOption` callback for implementing custom filtering logic

### v0.3.1

- Feat: Adding disabled styling for material-theme

### v0.3.0

- Feat: Add `popperModifiers` prop

### v0.2.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.

### v0.1.18

- Add styling on hover to align with other BorderRenderers which was missing

### v0.0.1

- Initial version
