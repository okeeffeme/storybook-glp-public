import PropTypes from 'prop-types';

import {
  getDisplayName,
  deprecateRestProps,
  excludeDataAndAriaProps,
} from '@jotunheim/react-utils';

/*
 This component is never rendered, it only serves as a placeholder to make sure we keep backwards compatibility for
 the consumers that create the options via markup as opposed to via the "options" prop on the select.
 */
export const Option = (props) => {
  deprecateRestProps(
    getDisplayName(Option),
    Object.keys(excludeDataAndAriaProps(props)),
    Object.keys(Option.propTypes)
  );

  return null;
};

Option.propTypes = {
  header: PropTypes.bool,
  text: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,
  onSelect: PropTypes.func,
};

export default Option;
