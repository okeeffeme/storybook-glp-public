import React from 'react';
import Tooltip from '@jotunheim/react-tooltip';
import SimpleSelect from '@jotunheim/react-simple-select';

import Select from './select';
import Wrapper from './wrapper';
import DrillDownMenuList from './drilldown-menulist';

import {createNodePrefix, flattenOptions} from '../utils';

const DrillDown = ({
  label,
  value,
  options = [],
  error = false,
  required = false,
  disabled = false,
  readOnly = false,
  isMulti,
  autoFocus,
  isLoading,
  isClearable,
  isSearchable,
  onBlur,
  onFocus,
  onChange,
  className,
  helperText,
  placeholder: placeholderProp,
  noOptionsMessage,
  /*
  loadOptions,
  isCreatable,
  filterOption,
  reduceOptions,
  onCreateOption,
  isValidNewOption,
  isOptionDisabled = noopFalse,
  menuShouldScrollIntoView,
   */
  ...rest
}) => {
  const flattenedOptions = React.useMemo(() => {
    return flattenOptions(options);
  }, [options]);

  const formatOptionLabel = React.useCallback(
    (opt, info) => {
      if (info.context === 'value') {
        const option = flattenedOptions.find((o) => o.value === opt.value);
        const label = `${createNodePrefix(option)}${option.label}`;

        if (isMulti) {
          return (
            <Tooltip content={label}>
              <span
                data-testid="multi-drilldown-value"
                data-drilldown-value={opt.value}>
                {label}
              </span>
            </Tooltip>
          );
        }

        return (
          <span data-testid="drilldown-value" data-drilldown-value={opt.value}>
            {label}
          </span>
        );
      }
    },
    [flattenedOptions, isMulti]
  );

  return (
    <Wrapper
      error={error}
      label={label}
      value={value}
      onBlur={onBlur}
      onFocus={onFocus}
      disabled={disabled}
      readOnly={readOnly}
      required={required}
      className={className}
      helperText={helperText}
      placeholder={placeholderProp}
      {...rest}>
      {({handleFocus, handleBlur, placeholder}) => (
        <SimpleSelect
          components={{
            MenuList: DrillDownMenuList,
          }}
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder={placeholder}
          flattenedOptions={flattenedOptions}
          formatOptionLabel={formatOptionLabel}
          label={label}
          value={value}
          error={error}
          options={options}
          isMulti={isMulti}
          required={required}
          disabled={disabled}
          readOnly={readOnly}
          onChange={onChange}
          autoFocus={autoFocus}
          className={className}
          isLoading={isLoading}
          helperText={helperText}
          isClearable={isClearable}
          isSearchable={isSearchable}
          noOptionsMessage={noOptionsMessage}
          truncateValueFromLeft={true}
        />
      )}
    </Wrapper>
  );
};

DrillDown.propTypes = Select.propTypes;

export default DrillDown;
