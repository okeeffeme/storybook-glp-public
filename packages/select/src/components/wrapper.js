import React from 'react';
import cn from 'classnames';
import InputWrapper from '@jotunheim/react-input-wrapper';
import {useTheme, bemFactory} from '@jotunheim/react-themes';
import {
  deprecateRestProps,
  excludeDataAndAriaProps,
  extractDataAndAriaProps,
  getDisplayName,
} from '@jotunheim/react-utils';

import componentThemes from '../themes';

import Select from './select';

const noop = () => {};

// Internal helper to share some logic between Select and DrillDown

/* eslint-disable react/prop-types */
const Wrapper = ({
  label,
  value,
  options,
  isAsync,
  error = false,
  onBlur = noop,
  onFocus = noop,
  required = false,
  children,
  disabled,
  readOnly = false,
  className,
  helperText,
  placeholder = '',
  ...rest
}) => {
  const {classNames, baseClassName} = useTheme('select', componentThemes);
  const {block} = bemFactory({baseClassName, classNames});
  const hasValue = Array.isArray(value) ? value.length > 0 : value != null;
  const [isActive, setIsActive] = React.useState(false);

  if (process.env.NODE_ENV !== 'production') {
    deprecateRestProps(
      getDisplayName(Select),
      Object.keys(excludeDataAndAriaProps(rest)),
      Object.keys(Select.propTypes)
    );
  }

  // revert to defaultText, for back compat with previous version
  placeholder = !isActive && !!label ? '' : placeholder || rest.defaultText;

  const handleFocus = React.useCallback(() => {
    setIsActive(true);
    onFocus();
  }, [onFocus]);

  const handleBlur = React.useCallback(() => {
    setIsActive(false);
    onBlur();
  }, [onBlur]);

  return (
    <InputWrapper
      data-react-select
      data-testid="wrapper-select"
      data-options={options ? options.length : null}
      data-isasync={isAsync}
      error={error}
      value={value}
      data-value={value}
      label={label}
      active={isActive}
      required={required}
      disabled={disabled}
      readOnly={readOnly}
      hasValue={hasValue}
      className={cn(block(), className)}
      helperText={helperText}
      {...extractDataAndAriaProps(rest)}>
      {children({handleFocus, handleBlur, placeholder})}
    </InputWrapper>
  );
};

export default Wrapper;
