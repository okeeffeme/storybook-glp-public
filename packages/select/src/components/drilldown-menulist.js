import React from 'react';
import HighlightTerm from '@jotunheim/react-highlight-term';
import {createFilter} from '@jotunheim/react-simple-select';
import TreeView, {NodeSelectionMode} from '@jotunheim/react-tree-view';

import {createNodes, createNodePrefix, discoverParentsChain} from '../utils';

const toggleArrayValue = (arr, index, value) => {
  if (index === -1) {
    return [...arr, value];
  }

  return [...arr.slice(0, index), ...arr.slice(index + 1)];
};

const filterOption = createFilter();

/* eslint-disable react/prop-types */
const DrillDownMenuList = ({selectProps, ...rest}) => {
  const {options, flattenedOptions, isMulti, value, inputValue} = selectProps;
  const {innerRef, maxHeight, setValue} = rest;
  const hasFilter = !!inputValue;

  const [expandedNodeIds, setExpandedNodeIds] = React.useState(() => {
    if (isMulti) {
      return [];
    }

    return discoverParentsChain(options, value);
  });

  const selectedNodeIds = React.useMemo(() => {
    if (isMulti) {
      return value.map((option) => option.value);
    }

    // singe-select value is null when nothing is selected, but TreeView is still expecting an array
    return value ? [value.value] : [];
  }, [isMulti, value]);

  const getNodeSelectionMode = React.useCallback(() => {
    return isMulti ? NodeSelectionMode.multi : NodeSelectionMode.single;
  }, [isMulti]);

  const nodes = React.useMemo(
    () => createNodes(options, expandedNodeIds),
    [options, expandedNodeIds]
  );

  const filteredNodes = React.useMemo(() => {
    const matchedOptions = flattenedOptions.filter((o) =>
      filterOption(o, inputValue)
    );

    return createNodes(matchedOptions);
  }, [inputValue, flattenedOptions]);

  const renderNodeContents = React.useCallback(
    (node) => {
      if (inputValue) {
        return (
          <>
            <span>{createNodePrefix(node)}</span>
            <HighlightTerm text={node.label} searchTerm={inputValue} />
          </>
        );
      }

      return node.label;
    },
    [inputValue]
  );

  // toggle node checked/unchecked
  const onNodeSelectionToggled = React.useCallback(
    (node) => {
      const nodeValue = {
        value: node.id,
        label: node.label,
      };

      if (isMulti) {
        const index = value.findIndex((n) => n.value === node.id);
        setValue(toggleArrayValue(value, index, nodeValue));
      } else {
        setValue(nodeValue);
      }
    },
    [isMulti, value, setValue]
  );

  // keep track of which nodes are expanded
  const onToggleNode = React.useCallback(
    (node) => {
      const index = expandedNodeIds.indexOf(node.id);

      setExpandedNodeIds(toggleArrayValue(expandedNodeIds, index, node.id));
    },
    [expandedNodeIds, setExpandedNodeIds]
  );

  const nodesToRender = hasFilter ? filteredNodes : nodes;

  if (nodesToRender.length === 0) {
    return rest.children;
  }

  return (
    <div
      ref={innerRef}
      data-drilldown-menulist
      style={{maxHeight, overflow: 'auto'}}>
      <TreeView
        nodes={nodesToRender}
        onToggleNode={onToggleNode}
        selectedNodeIds={selectedNodeIds}
        renderNodeContents={renderNodeContents}
        getNodeSelectionMode={getNodeSelectionMode}
        onNodeSelectionToggled={onNodeSelectionToggled}
        onNodeClicked={onNodeSelectionToggled}
      />
    </div>
  );
};

export default DrillDownMenuList;
