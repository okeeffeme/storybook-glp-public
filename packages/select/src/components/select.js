import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import {useTheme, bemFactory} from '@jotunheim/react-themes';
import SimpleSelect, {AsyncSelect} from '@jotunheim/react-simple-select';

import Wrapper from './wrapper';

import componentThemes from '../themes';
import {getOptionsFromChildren} from '../utils';

export const Select = ({
  label,
  value,
  options: optionsProp = [],
  error = false,
  required = false,
  disabled = false,
  readOnly = false,
  isMulti,
  autoFocus,
  isLoading,
  isCreatable,
  isClearable,
  isSearchable,
  children,
  className,
  helperText,
  filterOption,
  placeholder: placeholderProp,
  createPrefix = 'Create: ',
  isValidNewOption,
  isOptionDisabled,
  noOptionsMessage,
  formatGroupLabel,
  formatOptionLabel,
  menuShouldScrollIntoView,
  onFocus,
  onBlur,
  onChange,
  onCreateOption,
  loadOptions,
  reduceOptions,
  varyCacheBy = null,
  truncateValueFromLeft = false,
  ...rest
}) => {
  const {classNames, baseClassName} = useTheme('select', componentThemes);
  const {element} = bemFactory({baseClassName, classNames});

  const childOptions = getOptionsFromChildren(children);

  if (process.env.NODE_ENV !== 'production') {
    if (optionsProp.length && childOptions.length) {
      /* eslint-disable no-console*/
      console.warn(
        'Please specify either options array or options children, not both'
      );
      return null;
    }
  }

  const options = optionsProp.length ? optionsProp : childOptions;

  const formatCreateLabel = (inputValue) => (
    <span
      className={cn(element('create-option'), {
        [element('create-option', 'empty')]: !inputValue,
      })}>
      {inputValue ? `${createPrefix}'${inputValue}'` : helperText}
    </span>
  );

  const isAsync = typeof loadOptions === 'function';
  const Component = isAsync === true ? AsyncSelect : SimpleSelect;

  return (
    <Wrapper
      error={error}
      label={label}
      isAsync={isAsync}
      value={value}
      onBlur={onBlur}
      options={options}
      onFocus={onFocus}
      disabled={disabled}
      readOnly={readOnly}
      required={required}
      className={className}
      helperText={helperText}
      placeholder={placeholderProp}
      {...rest}>
      {({handleFocus, handleBlur, placeholder}) => (
        <Component
          isValidNewOption={isValidNewOption}
          isOptionDisabled={isOptionDisabled}
          formatOptionLabel={formatOptionLabel}
          onFocus={handleFocus}
          onBlur={handleBlur}
          isCreatable={isCreatable}
          isClearable={isClearable}
          isSearchable={isSearchable}
          disabled={disabled}
          readOnly={readOnly}
          isMulti={isMulti}
          onChange={onChange}
          onCreateOption={onCreateOption}
          options={options}
          placeholder={placeholder}
          autoFocus={autoFocus}
          noOptionsMessage={noOptionsMessage}
          formatCreateLabel={formatCreateLabel}
          formatGroupLabel={formatGroupLabel}
          value={value}
          isLoading={isLoading}
          filterOption={filterOption}
          menuShouldScrollIntoView={menuShouldScrollIntoView}
          loadOptions={loadOptions}
          reduceOptions={reduceOptions}
          cacheUniq={varyCacheBy}
          truncateValueFromLeft={truncateValueFromLeft}
        />
      )}
    </Wrapper>
  );
};

const OptionShapeBase = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export const OptionShape = PropTypes.shape({
  ...OptionShapeBase,
  options: PropTypes.arrayOf(PropTypes.shape(OptionShapeBase)),
});

const ValueShape = PropTypes.shape({
  ...OptionShapeBase,
  value: OptionShapeBase.value.isRequired,
});

Select.propTypes = {
  isLoading: PropTypes.bool,
  onCreateOption: PropTypes.func,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  noOptionsMessage: PropTypes.func,
  formatGroupLabel: PropTypes.func,
  formatOptionLabel: PropTypes.func,
  filterOption: PropTypes.func,
  createPrefix: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  helperText: PropTypes.node,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    ValueShape,
    PropTypes.arrayOf(ValueShape),
  ]),
  options: PropTypes.arrayOf(OptionShape),
  autoFocus: PropTypes.bool,
  isClearable: PropTypes.bool,
  isMulti: PropTypes.bool,
  required: PropTypes.bool,
  isCreatable: PropTypes.bool,
  isValidNewOption: PropTypes.func,
  isOptionDisabled: PropTypes.func,
  error: PropTypes.bool,
  label: PropTypes.string,
  isSearchable: PropTypes.bool,
  menuShouldScrollIntoView: PropTypes.bool,
  loadOptions: PropTypes.func, // async (inputValue, previousOptions, additionalOptions) => {}
  reduceOptions: PropTypes.func, // (prevOptions, loadedOptions) => {}
  varyCacheBy: PropTypes.array, // changing of varyCacheBy deps leads to options reloading on menu open
  truncateValueFromLeft: PropTypes.bool, // sets the side which is truncated if selected value is longer than available space. Works only if value element is displayed as inline, not block.
};

Select.reduceGroupedOptions = AsyncSelect.reduceGroupedOptions;

export default Select;
