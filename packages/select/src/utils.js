import React from 'react';

import Option from './components/option';

export const hasChildOptions = (option) =>
  Array.isArray(option.options) && !!option.options.length;

export const createNodes = (options, expandedNodeIds = []) => {
  return options.map((option) => {
    const hasChildren = hasChildOptions(option);

    return {
      id: option.value,
      label: option.label,
      parent: option.parent,
      hasChildren,
      isExpanded: expandedNodeIds.includes(option.value),
      children: hasChildren
        ? createNodes(option.options, expandedNodeIds)
        : undefined,
    };
  });
};

export const flattenOptions = (
  options,
  onlyLeafs = false,
  result = [],
  parent
) => {
  return options.reduce((result, current) => {
    const option = {
      ...current,
      parent,
      options: undefined,
    };

    if (hasChildOptions(current)) {
      if (!onlyLeafs) {
        result.push(option);
      }

      flattenOptions(current.options, onlyLeafs, result, option);
    } else {
      result.push(option);
    }

    return result;
  }, result);
};

export const createNodePrefix = (node) => {
  let prefix = '';
  let n = node;

  while (n.parent) {
    prefix = `${n.parent.label} / ${prefix}`;
    n = n.parent;
  }

  return prefix;
};

export const getOptionsFromChildren = (children) => {
  const options = [];
  let currentOptions = options;

  React.Children.toArray(children)
    .filter((child) => child.type === Option)
    .forEach((child) => {
      if (child.props.header) {
        const group = {
          label: child.props.text || child.props.children,
          options: [],
        };
        options.push(group);
        currentOptions = group.options;
      } else {
        currentOptions.push({
          label: child.props.text || child.props.children,
          value: child.props.value,
        });
      }
    });

  return options;
};

const getParentsChainAsArray = (parents, value) => {
  let current = value;
  const result = [];

  while (parents[current]) {
    result.push(parents[current]);
    current = parents[current];
  }

  return result;
};

// Depth-first search approach for traverse nested options as a tree.
const depthFirstSearchForOptionParents = (options, value) => {
  const stack = [...options];
  const parents = {};

  while (stack.length) {
    let parent = stack.pop();

    if (parent.value === value) {
      // Node with given value is found
      return getParentsChainAsArray(parents, value);
    }

    if (parent.options && parent.options.length) {
      // If there are nested options the id of the parent option should be saved in parents dictionary and all nested options should be added to the end of the stack
      for (const option of parent.options) {
        parents[option.value] = parent.value;
        stack.push(option);
      }
    }
  }

  return [];
};

export const discoverParentsChain = (options, value) => {
  if (
    !options ||
    !options.length ||
    !value ||
    value.value === undefined ||
    value.value === null
  ) {
    return [];
  }

  return depthFirstSearchForOptionParents(options, value.value);
};
