import classNames from './css/select.module.css';

export default {
  select: {
    baseClassName: 'comd-react-select',
    classNames,
  },
};
