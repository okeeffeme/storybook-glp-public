# Jotunheim React Select

## NonceProvider

A `<NonceProvider cacheKey="key">` needs to be added in the app in cases where there is a chance of multiple competing React-Select instances/versions. An example of this is Studio and Action Management.

The `key` should consist only of lowercase letters in the a-z range.
