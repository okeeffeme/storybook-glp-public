import React from 'react';
import {render, screen} from '@testing-library/react';
import {ThemeContext, theme} from '../../../themes/src/index';

import {DrillDown} from '../../src';

const drillDownOptions = [
  {
    value: 1,
    label: 'Node 1 abc',
  },
  {
    value: 2,
    label: 'Node 2 def',
  },
  {
    value: 3,
    label: 'Node 3 klm',
  },
  {
    value: 4,
    label: 'Node 4',
    options: [
      {
        value: 5,
        label: 'Node 5',
      },
      {
        value: 6,
        label: 'Node 6',
        options: [
          {
            value: 7,
            label: 'Node 7',
          },
          {
            value: 8,
            label: 'Node 8 def',
          },
        ],
      },
    ],
  },
];

const defaultProps = {
  value: [],
  options: [],
  onChange() {},
};

const renderDrillDown = (props) =>
  render(<DrillDown {...defaultProps} {...props} />, {
    wrappingComponent: ThemeContext.Provider,
    wrappingComponentProps: {
      value: theme.themeNames.material,
    },
  });

describe('DrillDown component', () => {
  describe('Multiple Select', () => {
    it('should render label when defined', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
      });
      expect(screen.getByTestId('input-wrapper-label')).toHaveTextContent(
        'Please select'
      );
    });

    it('should not render label markup when label not provided', () => {
      renderDrillDown({isMulti: true});

      expect(
        screen.queryByTestId('input-wrapper-label')
      ).not.toBeInTheDocument();
    });

    it('should render label as visible when non-empty array is provided', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
        options: drillDownOptions,
        value: [drillDownOptions[0], drillDownOptions[3].options[1].options[0]],
      });
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should not render label as visible when empty array is provided as value', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
        value: [],
      });

      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should not render if multi-select does not receive a value as an array', () => {
      let didWarn = false;
      /* eslint-disable no-console*/
      console.warn = jest.fn((warn) => {
        didWarn = true;
        expect(warn).toBe(
          'Please provide an array of selected values via "value" prop'
        );
      });

      renderDrillDown({
        isMulti: true,
        options: drillDownOptions,
        value: '1',
      });
      expect(screen.queryByTestId('tree-view')).not.toBeInTheDocument();
      expect(didWarn).toBe(true);
    });

    it('should render custom values for each selected option', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
        options: drillDownOptions,
        value: [drillDownOptions[0], drillDownOptions[3].options[1].options[0]],
      });
      expect(screen.getAllByTestId('multi-drilldown-value')).toHaveLength(2);
    });

    it('should render no custom values when no options are selected', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
        options: drillDownOptions,
      });

      expect(screen.queryAllByTestId('drilldown-value')).toHaveLength(0);
    });

    it('should set prop for value truncation from the left by default', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: true,
        options: drillDownOptions,
        value: [drillDownOptions[0]],
      });
      expect(
        screen.getByTestId('simple-select-bidirectional-override-tag')
      ).toHaveAttribute('data-truncatevaluefromleft', 'true');
    });
  });

  describe('Single Select', () => {
    it('should render selected option when value is string', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: false,
        options: drillDownOptions,
        value: drillDownOptions[3].options[0].value,
      });

      const labelText = `${drillDownOptions[3].label} / ${drillDownOptions[3].options[0].label}`;

      expect(screen.getByTestId('drilldown-value')).toHaveTextContent(
        labelText
      );
    });

    it('should render selected option when value is object', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: false,
        options: drillDownOptions,
        value: drillDownOptions[3].options[0],
      });

      const labelText = `${drillDownOptions[3].label} / ${drillDownOptions[3].options[0].label}`;

      expect(screen.getByTestId('drilldown-value')).toHaveTextContent(
        labelText
      );
    });

    it('should not have drilldown-value class name', () => {
      renderDrillDown({
        label: 'Please select',
        isMulti: false,
        options: drillDownOptions,
        value: drillDownOptions[0],
      });
      expect(screen.getByTestId('drilldown-value')).not.toHaveClass(
        'comd-react-select__drilldown-value'
      );
    });
  });
});
