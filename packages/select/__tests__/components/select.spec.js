import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {ThemeContext, theme} from '../../../themes/src/index';

import Select from '../../src';

const defaultOptions = [
  {value: '1', label: 'Option 1'},
  {value: '2', label: 'Option 2'},
  {value: '3', label: 'Option 3'},
];

const convertOptions = (options) =>
  options.map((option) => (
    <Select.Option key={option.value} value={option.value}>
      {option.label}
    </Select.Option>
  ));

const defaultProps = {
  value: 0,
  options: defaultOptions,
  onChange() {},
};

const renderRTL = (props) =>
  render(<Select {...defaultProps} {...props} />, {
    wrappingComponent: ThemeContext.Provider,
    wrappingComponentProps: {
      value: theme.themeNames.material,
    },
  });

/* eslint-disable react/prop-types */
const UncontrolledSelect = (props) => {
  const [value, onChange] = React.useState(props.value);

  return <Select {...props} value={value} onChange={onChange} />;
};
/* eslint-enable react/prop-types */

const renderSelect = (props) => {
  return render(<UncontrolledSelect {...defaultProps} {...props} />);
};

describe('Select component', () => {
  describe('Single select', () => {
    it('should render label when defined', () => {
      renderRTL({label: 'Please select'});

      expect(screen.getByTestId('input-wrapper-label')).toHaveTextContent(
        'Please select'
      );
    });

    it('should render with className when defined', () => {
      const props = {className: 'my-custom-select'};
      render(
        <div>
          <Select {...defaultProps} {...props} />
        </div>
      );
      expect(screen.getByTestId('wrapper-select')).toHaveClass(
        'my-custom-select'
      );
      expect(screen.getByTestId('wrapper-select')).not.toHaveClass(
        'some-other-class'
      );
    });

    it('should not render label markup when label not provided', () => {
      renderRTL();
      expect(screen.queryAllByTestId('input-wrapper-label')).toHaveLength(0);
    });

    it('should render label as visible when non-empty value is provided', () => {
      renderRTL({label: 'Please select', value: '1'});
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render label as visible when empty string is provided', () => {
      renderRTL({label: 'Please select', value: ''});
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should not render label as visible when null value is provided', () => {
      renderRTL({label: 'Please select', value: null});
      expect(screen.queryByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should not render label as visible when undefined value is provided', () => {
      renderRTL({
        label: 'Please select',
        value: undefined,
      });
      expect(screen.queryByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render with options prop', () => {
      renderRTL({options: defaultOptions});
      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-options',
        defaultOptions.length.toString()
      );
    });

    it('should render with children options', () => {
      const children = [
        <Select.Option key={1} value="1">
          Option 1
        </Select.Option>,
        <Select.Option key={2} value="2">
          Option 2
        </Select.Option>,
      ];

      renderRTL({
        options: undefined,
        children,
      });
      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-options',
        children.length.toString()
      );
    });

    it('should set correct value on inner ReactSelect with a nested set of options', () => {
      renderRTL({
        options: [{label: 'group', options: defaultOptions}],
        value: '2',
      });
      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-value',
        '2'
      );
    });

    it('should render with grouped children options', () => {
      const children = [
        <Select.Option key={1} header>
          Header 1
        </Select.Option>,
        <Select.Option key={2} value="1">
          Option 1
        </Select.Option>,
        <Select.Option key={3} value="2">
          Option 2
        </Select.Option>,
        <Select.Option key={4} header>
          Header 2
        </Select.Option>,
        <Select.Option key={5} value="3">
          Option 3
        </Select.Option>,
      ];

      renderRTL({
        options: undefined,
        children,
      });
      let groupedOptions = children.filter((child) => child.props.header);
      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-options',
        groupedOptions.length.toString()
      );
    });

    it('should not render when both options and children are set', () => {
      console.warn = jest.fn((warn) => {
        expect(warn).toBe(
          'Please specify either options array or options children, not both'
        );
      });

      renderRTL({
        options: defaultOptions,
        children: convertOptions(defaultOptions),
        value: [],
      });
      expect(screen.queryByTestId('wrapper-select')).not.toBeInTheDocument();
    });

    it('should render AsyncSelect when loadOptions is defined', () => {
      const loadOptions = async () => {
        return defaultOptions;
      };

      renderRTL({
        loadOptions,
        value: {
          label: 'Option 3',
          value: '3',
        },
      });
      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-isasync',
        'true'
      );
      expect(
        screen.getByTestId('simple-select-dropdown-indicator')
      ).toBeInTheDocument();
    });

    it('should not render AsyncSelect when loadOptions is not defined', () => {
      renderRTL({
        options: defaultOptions,
        value: '3',
      });

      expect(screen.getByTestId('wrapper-select')).toHaveAttribute(
        'data-isasync',
        'false'
      );
      expect(
        screen.getByTestId('simple-select-dropdown-indicator')
      ).toBeInTheDocument();
    });

    it('should add readOnly class to container', () => {
      renderRTL({
        label: 'Please select',
        value: '3',
        readOnly: true,
      });
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--readOnly'
      );
    });

    it('should add only readOnly class to container when both disabled and readOnly are true', () => {
      renderRTL({
        label: 'Please select',
        value: '3',
        readOnly: true,
        disabled: true,
      });
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--readOnly'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
    });
  });

  describe('Multiple Select', () => {
    it('should render label when defined', () => {
      const props = {
        label: 'Please select',
        isMulti: true,
        value: [],
      };
      const {getByTestId} = renderSelect(props);

      expect(getByTestId('input-wrapper-label').innerHTML).toEqual(props.label);
    });

    it('should not render label markup when label not provided', () => {
      const props = {
        isMulti: true,
        value: [],
      };
      const {queryByTestId} = renderSelect(props);

      expect(queryByTestId('input-wrapper-label')).not.toBeInTheDocument();
    });

    it('should render label as visible when non-empty array is provided', () => {
      const props = {
        label: 'Please select',
        isMulti: true,
        value: [defaultOptions[0]],
      };
      const {getByTestId} = renderSelect(props);

      expect(
        getByTestId('input-wrapper-label').classList.contains(
          'comd-input-wrapper__label--shrink'
        )
      ).toBe(true);
    });

    it('should not render label as visible when empty array is provided', () => {
      const props = {
        label: 'Please select',
        isMulti: true,
        value: [],
      };
      const {getByTestId} = renderSelect(props);

      expect(
        getByTestId('input-wrapper-label').classList.contains(
          'comd-input-wrapper__label--shrink'
        )
      ).toBe(false);
    });

    it('should not render if multi-select does not receive a value as an array', () => {
      let didWarn = false;
      /* eslint-disable no-console*/
      console.warn = jest.fn((warn) => {
        didWarn = true;
        expect(warn).toBe(
          'Please provide an array of selected values via "value" prop'
        );
      });

      const props = {
        isMulti: true,
        options: defaultOptions,
        value: '1',
      };
      const {queryByTestId} = renderSelect(props);
      expect(queryByTestId('react-select')).not.toBeInTheDocument();
      expect(didWarn).toBe(true);
    });

    it('should select options one by one', () => {
      const props = {
        label: 'Please select',
        isMulti: true,
        value: [],
      };

      const {queryByText, queryByTestId} = renderSelect(props);

      // Check that there are no selected options
      expect(queryByText('Option 1')).not.toBeInTheDocument();
      expect(queryByText('Option 2')).not.toBeInTheDocument();
      expect(queryByText('Option 3')).not.toBeInTheDocument();

      // Select Option 1
      userEvent.click(queryByTestId('simple-select-dropdown-indicator'));
      userEvent.click(queryByText('Option 1'));
      userEvent.click(queryByTestId('simple-select-dropdown-indicator'));

      // Select Option 3
      userEvent.click(queryByTestId('simple-select-dropdown-indicator'));
      userEvent.click(queryByText('Option 3'));
      userEvent.click(queryByTestId('simple-select-dropdown-indicator'));

      // Check selected options
      expect(queryByText('Option 1')).toBeInTheDocument();
      expect(queryByText('Option 2')).not.toBeInTheDocument();
      expect(queryByText('Option 3')).toBeInTheDocument();
    });
  });
});
