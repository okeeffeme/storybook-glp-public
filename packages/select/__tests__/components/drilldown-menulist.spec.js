import React from 'react';
import {render, screen} from '@testing-library/react';
import {ThemeContext, theme} from '../../../themes/src/index';

import DrillDownMenuList from '../../src/components/drilldown-menulist';
import {flattenOptions} from '../../src/utils';

const drillDownOptions = [
  {
    value: 1,
    label: 'Node 1 abc',
  },
  {
    value: 2,
    label: 'Node 2 def',
  },
  {
    value: 3,
    label: 'Node 3 klm',
  },
  {
    value: 4,
    label: 'Node 4',
    options: [
      {
        value: 5,
        label: 'Node 5',
      },
      {
        value: 6,
        label: 'Node 6',
        options: [
          {
            value: 7,
            label: 'Node 7',
          },
          {
            value: 8,
            label: 'Node 8 def',
          },
        ],
      },
    ],
  },
];

const defaultProps = {
  selectProps: {
    options: drillDownOptions,
    flattenedOptions: flattenOptions(drillDownOptions),
    isMulti: true,
    value: [1],
    inputValue: '',
  },
  innerRef: React.createRef(),
  maxHeight: 100,
  setValue() {},
};

const renderDrillDownMenuList = (props) =>
  render(<DrillDownMenuList {...defaultProps} {...props} />, {
    wrappingComponent: ThemeContext.Provider,
    wrappingComponentProps: {
      value: theme.themeNames.material,
    },
  });

describe('DrillDown MenuList component', () => {
  describe('Multiple Select', () => {
    it('should render', () => {
      renderDrillDownMenuList({
        label: 'Please select',
        isMulti: true,
      });

      expect(screen.getByTestId('tree-view')).toBeInTheDocument();
    });
  });
});
