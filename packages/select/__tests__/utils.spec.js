import {discoverParentsChain} from '../src/utils';

const drillDownOptions = [
  {
    value: 1,
    label: 'Node 1 abc',
  },
  {
    value: 2,
    label: 'Node 2 def',
  },
  {
    value: 3,
    label: 'Node 3 klm',
  },
  {
    value: 4,
    label: 'Node 4',
    options: [
      {
        value: 5,
        label: 'Node 5',
        options: [
          {
            value: 9,
            label: 'Node 9',
            options: [
              {
                value: 11,
                label: 'Node 11',
              },
              {
                value: 12,
                label: 'Node 12 def',
              },
            ],
          },
          {
            value: 10,
            label: 'Node 10 def',
          },
        ],
      },
      {
        value: 6,
        label: 'Node 6',
        options: [
          {
            value: 7,
            label: 'Node 7',
            options: [
              {
                value: 0,
                label: 'Node 0 zero',
              },
            ],
          },
          {
            value: 8,
            label: 'Node 8 def',
          },
        ],
      },
    ],
  },
];

describe('Discover parent chain of the selected option', () => {
  it('should return an empty array if options are not specified', () => {
    expect(discoverParentsChain(null, {value: 1})).toEqual([]);
  });

  it('should return an empty array if value is not specified', () => {
    expect(discoverParentsChain([], undefined)).toEqual([]);
    expect(discoverParentsChain([], null)).toEqual([]);
    expect(discoverParentsChain([], {})).toEqual([]);
  });

  it('should return an empty array if options are empty', () => {
    expect(discoverParentsChain([], {value: 1})).toEqual([]);
  });

  it('should return find node with value 0', () => {
    expect(discoverParentsChain(drillDownOptions, {value: 0})).toEqual([
      7,
      6,
      4,
    ]);
  });

  it('should return an empty array if no node with provided id is in options', () => {
    expect(discoverParentsChain(drillDownOptions, {value: 100})).toEqual([]);
  });

  it('should return empty array if node with value is on the top level', () => {
    expect(discoverParentsChain(drillDownOptions, {value: 1})).toEqual([]);
  });

  it('should return array of all parents if node is not on the top level', () => {
    expect(discoverParentsChain(drillDownOptions, {value: 8})).toEqual([6, 4]);
    expect(discoverParentsChain(drillDownOptions, {value: 9})).toEqual([5, 4]);
    expect(discoverParentsChain(drillDownOptions, {value: 10})).toEqual([5, 4]);
    expect(discoverParentsChain(drillDownOptions, {value: 11})).toEqual([
      9,
      5,
      4,
    ]);
  });
});
