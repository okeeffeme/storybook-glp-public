import React from 'react';
import {render, screen} from '@testing-library/react';
import Tag from '../../src/components/Tag';
import {thumbUp} from '../../../icons/src';

describe('Tag', () => {
  it('should render children', () => {
    render(<Tag>Example</Tag>);

    expect(screen.getByText(/example/i).textContent).toEqual('Example');
  });

  it('should render with no children', () => {
    render(<Tag data-testid="tag" />);

    expect(screen.getByTestId('tag').textContent).toBeFalsy();
  });

  it('should add default and tag modifiers by default', () => {
    const {container} = render(<Tag />);

    expect(
      container.querySelector('.comd-tag--default.comd-tag--default-tag')
    ).toBeTruthy();
  });

  it('should add appearance modifier', () => {
    const {container} = render(<Tag appearance={Tag.Appearance.Primary} />);

    expect(container.querySelectorAll('.comd-tag--default').length).toBe(0);
    expect(container.querySelectorAll('.comd-tag--primary').length).toBe(1);
  });

  it('should add type modifier', () => {
    const {container} = render(<Tag type={Tag.Type.Document} />);

    expect(container.querySelectorAll('.comd-tag--default-tag').length).toBe(0);
    expect(container.querySelectorAll('.comd-tag--document-tag').length).toBe(
      1
    );
  });

  it('should render icon', () => {
    render(<Tag iconPath={thumbUp} />);

    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      thumbUp
    );
  });

  it('should render without icon', () => {
    render(<Tag />);

    expect(screen.queryByTestId('icon')).toBeFalsy();
  });
});
