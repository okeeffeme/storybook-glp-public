import Tag from './components/Tag';
import {Appearance} from './utils/appearance';
import {Type} from './utils/type';

export {Tag, Appearance, Type};
export default Tag;
