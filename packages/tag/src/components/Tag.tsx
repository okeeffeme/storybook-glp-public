import React, {ReactNode} from 'react';
import cn from 'classnames';
import Icon from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {Appearance} from '../utils/appearance';
import {Type} from '../utils/type';
import classNames from '../styles/Tag.module.css';

export const Tag = ({
  appearance = Appearance.Default,
  type = Type.Default,
  children,
  iconPath,
  ...rest
}: TagProps) => {
  const {block, element, modifier} = bemFactory('comd-tag', classNames);
  const classes = cn(block(), modifier(appearance), modifier(type), {
    [modifier('icon')]: !!iconPath,
  });

  return (
    <div
      className={classes}
      data-testid="tag"
      {...extractDataAriaIdProps(rest)}
      data-tag-component-text="">
      {iconPath && (
        <div className={element('icon')} data-tag-icon="">
          <Icon path={iconPath} size={12} />
        </div>
      )}
      {children}
    </div>
  );
};

export type TagProps = {
  appearance?: Appearance;
  type?: Type;
  children?: ReactNode;
  iconPath?: string;
};

Tag.Appearance = Appearance;
Tag.Type = Type;

export default Tag;
