export enum Appearance {
  Default = 'default',
  Primary = 'primary',
  Danger = 'danger',
  Safe = 'safe',
  Warning = 'warning',
  Promoter = 'promoter',
  Passive = 'passive',
  Detractor = 'detractor',
}
