import React from 'react';
import {storiesOf} from '@storybook/react';
import {text, select} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {thumbDown, thumbUp, thumbsUpDown} from '../../icons/src';

import {Tag, Appearance, Type} from '../src';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div
    style={{
      margin: '24px',
      display: 'flex',
      flexDirection: 'column',
      rowGap: '10px',
    }}>
    {children}
  </div>
);

/* eslint-disable-next-line */
const Row = ({children}) => (
  <div style={{display: 'flex', justifyContent: 'space-evenly'}}>
    {children}
  </div>
);

storiesOf('Components/tag', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Tag', () => (
    <Container>
      <h4>Tag</h4>
      <Row>
        <Tag appearance={Appearance.Default}>default</Tag>
        <Tag appearance={Appearance.Primary}>primary</Tag>
        <Tag appearance={Appearance.Danger}>danger</Tag>
        <Tag appearance={Appearance.Safe}>safe</Tag>
        <Tag appearance={Appearance.Warning}>warning</Tag>
      </Row>
      <h4>Status Tag</h4>
      <Row>
        <Tag type={Type.Document} appearance={Appearance.Default}>
          default
        </Tag>
        <Tag type={Type.Document} appearance={Appearance.Primary}>
          primary
        </Tag>
        <Tag type={Type.Document} appearance={Appearance.Danger}>
          danger
        </Tag>
        <Tag type={Type.Document} appearance={Appearance.Safe}>
          safe
        </Tag>
        <Tag type={Type.Document} appearance={Appearance.Warning}>
          warning
        </Tag>
      </Row>
      <h4>Metatags in Text Analytics</h4>
      <Row>
        <Tag appearance={Appearance.Promoter} iconPath={thumbUp}>
          Support /&nbsp;<b>I like this</b>
        </Tag>
        <Tag appearance={Appearance.Passive} iconPath={thumbsUpDown}>
          Product attributes /&nbsp;<b>Neutral</b>
        </Tag>
        <Tag appearance={Appearance.Detractor} iconPath={thumbDown}>
          <b>I don't like this</b>
        </Tag>
      </Row>
    </Container>
  ))
  .add('With Knobs', () => (
    <Container>
      <div>
        <Tag
          appearance={select('Appearance', Appearance, Appearance.Default)}
          type={select('Type', Type, Type.Default)}>
          {text('Text', 'With Knobs')}
        </Tag>
      </div>
    </Container>
  ));
