import React, {ReactNode} from 'react';
import {render as renderRTL, screen} from '@testing-library/react';
import Dialog, {DialogProps} from '../../src/components/Dialog';
import {DialogHeaderProps} from '../../src/components/DialogHeader';
import {DialogPositions, DialogSizes} from '../../src';
import userEvent from '@testing-library/user-event';

const render = ({
  props,
  headerProps,
  children,
}: {
  props?: Partial<DialogProps>;
  headerProps?: Partial<DialogHeaderProps>;
  children?: ReactNode;
} = {}) => {
  return renderRTL(
    <Dialog open {...props} data-test-dialog data-testid="dialog">
      <Dialog.Header title="Title" {...headerProps} />
      <Dialog.Body>Body!</Dialog.Body>
      {children}
    </Dialog>
  );
};

describe('Dialog :: ', () => {
  it('should render no children if not open', () => {
    render({
      props: {
        open: false,
      },
    });

    expect(screen.queryByTestId('dialog-body')).not.toBeInTheDocument();
  });

  it('should render dialog with header and body with backdrop', () => {
    render();

    expect(screen.getByTestId('dialog-header')).toBeInTheDocument();
    expect(screen.getByTestId('dialog-body')).toBeInTheDocument();
    expect(screen.getByTestId('dialog-backdrop')).toBeInTheDocument();
  });

  it('should render dialog with header, body and footer', () => {
    render({
      children: <Dialog.Footer data-custom-footer="">Footer!</Dialog.Footer>,
    });
    expect(screen.getByTestId('dialog-header')).toBeInTheDocument();
    expect(screen.getByTestId('dialog-body')).toBeInTheDocument();
    expect(screen.getByTestId('dialog-footer')).toBeInTheDocument();
  });

  it('should render dialog with header and body without backdrop', () => {
    render({props: {isModal: false}});
    expect(screen.queryByTestId('dialog-backdrop')).not.toBeInTheDocument();
  });

  it('should render dialog header with closeButton', () => {
    render();

    expect(screen.getByTestId('dialog-header')).toBeInTheDocument();
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should render dialog header without closeButton', () => {
    render({headerProps: {closeButton: false}});

    expect(screen.getByTestId('dialog-header')).toBeInTheDocument();
    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should render dialog with "medium" size by default', () => {
    render();
    expect(screen.getByTestId('dialog-bodywrapper')).toHaveClass(
      'comd-dialog__body-wrapper--medium'
    );
  });

  it('should render dialog with specific size', () => {
    render({props: {size: DialogSizes.large}});

    expect(screen.getByTestId('dialog-bodywrapper')).toHaveClass(
      'comd-dialog__body-wrapper--large'
    );
  });

  it('should render dialog with "fixed" position by default', () => {
    render();
    expect(screen.getByTestId('dialog')).toHaveClass('comd-dialog--fixed');
    expect(screen.queryByTestId('dialog-backdrop')).toHaveClass(
      'comd-dialog-backdrop--fixed'
    );
  });

  it('should render dialog with specific position', () => {
    render({props: {position: DialogPositions.absolute}});

    expect(screen.getByTestId('dialog')).toHaveClass('comd-dialog--absolute');
    expect(screen.queryByTestId('dialog-backdrop')).toHaveClass(
      'comd-dialog-backdrop--absolute'
    );
  });

  it('should call onHide if clicked on backdrop', () => {
    const onHide = jest.fn();
    render({props: {onHide}});

    userEvent.click(screen.getByTestId('dialog-backdrop'));

    expect(onHide).toBeCalled();
  });

  it('should not call onHide if dialog body wrapper is clicked inside dialog body wrapper', () => {
    const onHide = jest.fn();
    render({props: {onHide}});

    userEvent.click(screen.getByTestId('dialog-bodywrapper'));

    expect(onHide).not.toBeCalled();
  });

  it('should not call onHide if clicked on backdrop and hideOnBackdropClick is false', () => {
    const onHide = jest.fn();
    render({props: {onHide, hideOnBackdropClick: false}});

    userEvent.click(screen.getByTestId('dialog-backdrop'));

    expect(onHide).not.toBeCalled();
  });
});
