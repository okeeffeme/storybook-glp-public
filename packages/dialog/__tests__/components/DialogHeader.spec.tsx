import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';
import Icon, {eye, pencil} from '../../../icons/src';
import {ToggleButton, ToggleButtonGroup} from '../../../toggle-button/src';

import DialogHeader from '../../src/components/DialogHeader';

const render = (props) => renderRTL(<DialogHeader {...props} />);

describe('DialogHeader', () => {
  it('should render dialog header', () => {
    render({className: 'my-dialog'});
    expect(screen.getByTestId('dialog-header')).toBeInTheDocument();
  });

  it('should render dialog title when defined', () => {
    render({title: 'my title', titleClassName: 'my-title'});

    expect(screen.getByRole('heading')).toHaveTextContent('my title');
  });

  it('should not render dialog title when title is not set', () => {
    render({titleClassName: 'my-title'});
    expect(screen.queryByRole('heading')).not.toBeInTheDocument();
  });

  it('should render dialog header with children', () => {
    render({children: <span data-child>Another title</span>});

    expect(screen.getByTestId('dialog-header')).toHaveTextContent(
      'Another title'
    );
  });

  it('should render dialog header with closeButton', () => {
    render({closeButton: true});
    expect(screen.getByRole('button')).toHaveAttribute(
      'data-close-dialog-button'
    );
  });

  it('should render dialog header without closeButton', () => {
    render({closeButton: false});

    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should render dialog actions when actions are defined', () => {
    render({
      actions: [
        {
          icon: <div>A</div>,
          tooltip: 'Action 1',
          handler: () => {},
        },
        {
          icon: <div>B</div>,
          tooltip: 'Action 2',
          handler: () => {},
        },
      ],
      closeButton: false,
    });
    expect(screen.getByTestId('dialog-header-actions')).toBeInTheDocument();
    expect(screen.getAllByRole('button')).toHaveLength(2);
  });

  it('should render dialog actions when custom actions are defined', () => {
    render({
      actions: (
        <ToggleButtonGroup>
          <ToggleButton
            id={'edit'}
            text={'Edit'}
            onChange={() => {}}
            tooltipText={'Edit thing'}
            icon={<Icon path={pencil} />}
          />
          <ToggleButton
            id={'preview'}
            text={'Preview'}
            onChange={() => {}}
            tooltipText={'Preview thing'}
            icon={<Icon path={eye} />}
          />
        </ToggleButtonGroup>
      ),
      closeButton: false,
    });
    expect(screen.getByTestId('dialog-header-actions')).toBeInTheDocument();
    expect(screen.getAllByRole('checkbox')).toHaveLength(2);
  });

  it('should render dialog actions when closeButton is set', () => {
    render({actions: [], closeButton: true});

    expect(screen.getByTestId('dialog-header-actions')).toBeInTheDocument();
  });

  it('should not render dialog actions when actions and closeButton is not defined', () => {
    render({actions: [], closeButton: false});

    expect(
      screen.queryByTestId('dialog-header-actions')
    ).not.toBeInTheDocument();
  });
});
