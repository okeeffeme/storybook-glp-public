import React from 'react';
import {render, screen} from '@testing-library/react';
import DialogFooter from '../../src/components/DialogFooter';

describe('DialogFooter', () => {
  it('should render dialog footer', () => {
    render(<DialogFooter>Footer Content</DialogFooter>);

    expect(screen.getByTestId('dialog-footer')).toMatchSnapshot();
  });
});
