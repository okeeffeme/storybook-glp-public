import React from 'react';
import {render, screen} from '@testing-library/react';
import DialogBodyWrapper from '../../src/components/DialogBodyWrapper';

describe('DialogBodyWrapper', () => {
  it('should render dialog body wrapper', () => {
    render(<DialogBodyWrapper>Dialog content</DialogBodyWrapper>);

    expect(screen.getByTestId('dialog-bodywrapper')).toMatchSnapshot();
  });

  it('should render dialog body wrapper with size', () => {
    render(<DialogBodyWrapper size="large">Dialog content</DialogBodyWrapper>);

    expect(screen.getByTestId('dialog-bodywrapper')).toMatchSnapshot();
  });
});
