import React from 'react';
import {render, screen} from '@testing-library/react';
import DialogBody from '../../src/components/DialogBody';

describe('DialogBody', () => {
  it('should add padding class modifier by default', () => {
    render(<DialogBody>Body Content</DialogBody>);
    expect(screen.getByTestId('dialog-body')).toHaveClass(
      'comd-dialog__body--padding'
    );
  });

  it('should not add padding class modifier when hasPadding is false', () => {
    render(<DialogBody hasPadding={false}>Body Content</DialogBody>);

    expect(screen.getByTestId('dialog-body')).not.toHaveClass(
      'comd-dialog__body--padding'
    );
  });
});
