import {createContext} from 'react';
import {DialogSizes} from './DialogSizes';

export type DialogContextType = {
  hide: () => void;
  size: DialogSizes | string;
};

export default createContext<DialogContextType>({
  hide() {},
  size: DialogSizes.medium,
});
