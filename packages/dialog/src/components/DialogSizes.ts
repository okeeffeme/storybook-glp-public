export enum DialogSizes {
  small = 'small',
  medium = 'medium',
  large = 'large',
  huge = 'huge',
}
