import React, {ReactNode} from 'react';

import {hasVisibleChildren} from '@jotunheim/react-utils';
import {dialogBem} from './bem';

export type DialogHeaderActionsProps = {
  children?: ReactNode;
};

const {element} = dialogBem;

function DialogHeaderActions(props: DialogHeaderActionsProps) {
  const {children} = props;

  if (!hasVisibleChildren(children)) {
    return null;
  }

  return (
    <div
      className={element('header-actions')}
      data-testid="dialog-header-actions"
      data-dialog-header-actions="">
      {children}
    </div>
  );
}

export default DialogHeaderActions;
