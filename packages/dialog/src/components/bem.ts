import {bemFactory} from '@jotunheim/react-themes';

import classNames from '../css/Dialog.module.css';

export const baseClassName = 'comd-dialog';

export const dialogBem = bemFactory({baseClassName, classNames});

export const dialogOpenBem = bemFactory({
  baseClassName: `${baseClassName}-open`,
  classNames,
});

export const dialogBackdropBem = bemFactory({
  baseClassName: `${baseClassName}-backdrop`,
  classNames,
});
