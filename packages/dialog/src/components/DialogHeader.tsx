import React, {
  MouseEventHandler,
  ReactNode,
  useContext,
  useEffect,
  useRef,
} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton} from '@jotunheim/react-button';

import Icon, {close} from '@jotunheim/react-icons';

import DialogContext from './DialogContext';
import DialogHeaderActions from './DialogHeaderActions';
import {dialogBem} from './bem';

type DialogHeaderAction = {
  icon: ReactNode;
  handler: MouseEventHandler;
  tooltip?: string;
};

export type DialogHeaderProps = {
  children?: ReactNode;
  title?: ReactNode;
  onCloseButtonClick?: MouseEventHandler;
  autoFocusCloseButton?: boolean;
  closeButton?: boolean;
  titleClassName?: string;
  actions?: DialogHeaderAction[] | ReactNode;
};

const {element} = dialogBem;

function DialogHeader(props: DialogHeaderProps) {
  const {
    title,
    children,
    closeButton = true,
    titleClassName,
    autoFocusCloseButton = false,
    onCloseButtonClick,
    actions,
    ...rest
  } = props;

  const closeButtonRef = useRef<HTMLInputElement | null>(null);
  const {hide} = useContext(DialogContext);

  const handleCloseButtonClick = (e) => {
    if (onCloseButtonClick) {
      onCloseButtonClick(e);
    } else {
      e.preventDefault();
      hide();
    }
  };
  //
  useEffect(() => {
    if (autoFocusCloseButton) {
      //Need the timeout because could conflict when other elements on the page get focus as well
      setTimeout(() => {
        if (closeButtonRef.current !== null) {
          closeButtonRef.current.focus();
        }
      }, 0);
    }
  }, [autoFocusCloseButton]);

  const classes = cn(element('header'));

  return (
    <header
      className={classes}
      data-dialog-header=""
      data-drag-handle=""
      data-testid="dialog-header"
      {...extractDataAriaIdProps(rest)}>
      <div className={element('header-content')}>
        {title && (
          <h4 className={cn(element('title'), titleClassName)}>{title}</h4>
        )}
        {children}
      </div>
      <DialogHeaderActions>
        {Array.isArray(actions)
          ? (actions as DialogHeaderAction[]).map((action, idx) => (
              <Tooltip content={action.tooltip} key={idx}>
                <IconButton data-dialog-header-action onClick={action.handler}>
                  {action.icon}
                </IconButton>
              </Tooltip>
            ))
          : actions}

        {closeButton && (
          <IconButton
            data-close-dialog-button
            onClick={handleCloseButtonClick}
            ref={closeButtonRef}>
            <Icon path={close} />
          </IconButton>
        )}
      </DialogHeaderActions>
    </header>
  );
}

export default DialogHeader;
