import React, {ReactNode} from 'react';
import cn from 'classnames';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {dialogBem} from './bem';

type DialogFooterProps = {
  children?: ReactNode;
};

const {element} = dialogBem;

export const DialogFooter = ({children, ...rest}: DialogFooterProps) => {
  const classes = cn({
    [element('footer')]: true,
  });

  return (
    <footer
      className={classes}
      data-dialog-footer=""
      data-testid="dialog-footer"
      {...extractDataAndAriaProps(rest)}>
      {children}
    </footer>
  );
};

export default DialogFooter;
