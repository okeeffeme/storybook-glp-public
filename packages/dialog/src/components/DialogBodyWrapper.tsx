import React, {ComponentClass, FunctionComponent, ReactNode} from 'react';
import cn from 'classnames';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import Draggable from 'react-draggable';
import {DialogSizes} from './DialogSizes';
import {dialogBem} from './bem';

const {element} = dialogBem;

type BodyWrapperProps = {
  size?: DialogSizes | string;
  contentComponentClass?:
    | FunctionComponent<{className?: string}>
    | ComponentClass<{className?: string}>
    | string;
  draggable?: boolean;
  children?: ReactNode;
};

export const DialogBodyWrapper = ({
  children,
  size = 'medium',
  contentComponentClass,
  draggable = false,
  ...rest
}: BodyWrapperProps) => {
  const bodyWrapperClasses = cn(element('body-wrapper'), {
    [size && element('body-wrapper', size)]: size,
    [element('body-wrapper', 'draggable')]: draggable,
  });

  const DialogContent = contentComponentClass || 'div';

  const nodeRef = React.useRef(null);
  const renderDraggable = (component) => {
    if (draggable) {
      const draggableChild = React.cloneElement(
        React.Children.only(component),
        {
          ref: nodeRef,
        }
      );
      return (
        <Draggable nodeRef={nodeRef} handle="[data-drag-handle]">
          {draggableChild}
        </Draggable>
      );
    }
    return component;
  };

  return (
    <div
      className={bodyWrapperClasses}
      {...extractDataAndAriaProps(rest)}
      data-dialog-body-wrapper=""
      data-testid="dialog-bodywrapper">
      {renderDraggable(
        <DialogContent className={element('content')}>{children}</DialogContent>
      )}
    </div>
  );
};

export default DialogBodyWrapper;
