import React, {ReactNode, useContext} from 'react';
import cn from 'classnames';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import DialogContext from './DialogContext';
import {dialogBem} from './bem';

type DialogBodyProps = {
  children?: ReactNode;
  hasPadding?: boolean;
};

const {element} = dialogBem;

export const DialogBody = ({
  children,
  hasPadding = true,
  ...rest
}: DialogBodyProps) => {
  const {size} = useContext(DialogContext);

  const classes = cn(element('body'), element('body', size), {
    [element('body', 'padding')]: hasPadding,
  });

  return (
    <div
      className={classes}
      data-dialog-body=""
      data-testid="dialog-body"
      {...extractDataAndAriaProps(rest)}>
      {children}
    </div>
  );
};

export default DialogBody;
