import React, {
  ComponentType,
  CSSProperties,
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import cn from 'classnames';

import {usePortal, useZIndexStack} from '@jotunheim/react-contexts';
import {
  extractDataAndAriaProps,
  addClassNameToDOMNode,
  removeClassNameFromDOMNode,
  DataAriaIdAttributes,
} from '@jotunheim/react-utils';

import Header from './DialogHeader';
import Body from './DialogBody';
import Footer from './DialogFooter';
import BodyWrapper from './DialogBodyWrapper';
import DialogContext from './DialogContext';

import {DialogSizes} from './DialogSizes';
import {DialogPositions} from './DialogPositions';
import {dialogBackdropBem, dialogBem, dialogOpenBem} from './bem';

let numberOfDialogs = 0;
let numberOfBackdrops = 0;

export type DialogProps = {
  open?: boolean;
  size?: DialogSizes | string;
  children: ReactNode;
  isModal?: boolean;
  componentClass?:
    | ComponentType<
        {
          style: CSSProperties;
          className: string;
        } & DataAriaIdAttributes
      >
    | string;
  bodyWrapperComponentClass?: ComponentType | string;
  contentComponentClass?: ComponentType | string;
  portalZIndex?: number;
  draggable?: boolean;
  hideOnBackdropClick?: boolean;
  keyboard?: boolean;
  onHide?: () => void;
  position?: DialogPositions;
};

const getScrollbarWidth = () => {
  const scrollDiv = document.createElement('div');
  scrollDiv.style.cssText =
    'position: absolute; z-index: -1; opacity: 0; width: 50px; height: 50px; overflow: scroll;';
  document.body.appendChild(scrollDiv);

  const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);

  return scrollbarWidth;
};

const isBodyOverflowing = () => {
  return document.body.scrollHeight > document.documentElement.clientHeight;
};

function Dialog(props: DialogProps) {
  const {
    open,
    size = 'medium',
    children,
    isModal = true,
    componentClass = 'div',
    bodyWrapperComponentClass,
    contentComponentClass,
    portalZIndex,
    draggable = false,
    hideOnBackdropClick = true,
    onHide = () => {},
    keyboard = true,
    position = DialogPositions.fixed,
    ...rest
  } = props;

  const zIndexStack = useZIndexStack();
  const {
    renderInPortal,
    isPortalElementInDom,
    mountPortalElement,
    unmountPortalElement,
  } = usePortal();

  const [zIndex, setZIndex] = useState(0);
  const [number, setNumber] = useState(0);

  const resolvedZIndex = portalZIndex || zIndex;

  const dialogClasses = cn(
    dialogBem.block(),
    dialogBem.modifier(size),
    dialogBem.modifier(position)
  );
  const dialogStyle = {
    zIndex: resolvedZIndex + 1,
  };

  const backdropClasses = cn(
    dialogBackdropBem.block(),
    dialogBackdropBem.modifier(position)
  );
  const backdropStyle = {
    zIndex: resolvedZIndex,
  };

  const handleKeyUp = useCallback(
    (e) => {
      const isTopDialog = numberOfDialogs - 1 === number;
      if (keyboard && e.keyCode === 27 && isTopDialog) {
        onHide();
      }
    },
    [onHide, keyboard, number]
  );

  useEffect(() => {
    if (open) {
      document.addEventListener('keyup', handleKeyUp);
      return () => {
        document.removeEventListener('keyup', handleKeyUp);
      };
    }
  }, [open, handleKeyUp]);

  const setScrollbar = useCallback(() => {
    addClassNameToDOMNode(document.body, dialogOpenBem.block());

    if (isBodyOverflowing()) {
      document.body.style.paddingRight = `${getScrollbarWidth()}px`;
    }
  }, []);

  const resetScrollbar = useCallback(() => {
    document.body.style.paddingRight = '';
    removeClassNameFromDOMNode(document.body, dialogOpenBem.block());
  }, []);

  const handleBackdropClick = () => {
    if (hideOnBackdropClick) {
      onHide();
    }
  };

  /* Separate effect to calculate number of dialogs and backdrops and set number of current dialog.
   * If any of backdrop is visible it sets scrollbar.
   */
  useEffect(() => {
    if (open) {
      setNumber(numberOfDialogs++);

      if (isModal) {
        numberOfBackdrops += 1;
      }

      if (numberOfBackdrops > 0) {
        setScrollbar();
      }

      return () => {
        numberOfDialogs -= 1;
        if (isModal) {
          numberOfBackdrops -= 1;
        }
        if (numberOfBackdrops === 0) {
          resetScrollbar();
        }
        setNumber(0);
      };
    }
  }, [open, isModal, setNumber, setScrollbar, resetScrollbar]);

  /* Separate effect ot obtain and release zIndex */
  useEffect(() => {
    // Every dialog has 1050 z-index gap to show other portal like dropdowns, popovers and tooltips
    // in between
    const zIndex = zIndexStack.obtain(1050);
    setZIndex(zIndex);

    return () => {
      zIndexStack.release(zIndex);
    };
  }, [open, zIndexStack]);

  /* Separate effect on mounting/unmounting portal to DOM */
  useEffect(() => {
    if (open) {
      mountPortalElement();
      return () => {
        unmountPortalElement();
      };
    }
  }, [open, mountPortalElement, unmountPortalElement]);

  const dialogContextValue = useMemo(
    () => ({
      hide: onHide,
      size,
    }),
    [onHide, size]
  );

  if (!isPortalElementInDom) {
    return null;
  }

  const DialogBodyWrapper = bodyWrapperComponentClass || BodyWrapper;

  const dialog = React.createElement(
    componentClass,
    {
      className: dialogClasses,
      style: dialogStyle,
      ...extractDataAndAriaProps(rest),
    },
    <DialogBodyWrapper
      size={size}
      contentComponentClass={contentComponentClass}
      draggable={draggable}>
      {children}
    </DialogBodyWrapper>
  );

  return renderInPortal(
    <DialogContext.Provider value={dialogContextValue}>
      {dialog}
      {isModal && (
        <div
          data-dialog-backdrop=""
          style={backdropStyle}
          data-testid="dialog-backdrop"
          onClick={handleBackdropClick}
          className={backdropClasses}
        />
      )}
    </DialogContext.Provider>
  );
}

Dialog.Header = Header;
Dialog.Body = Body;
Dialog.Footer = Footer;
Dialog.BodyWrapper = BodyWrapper;

export default Dialog;
