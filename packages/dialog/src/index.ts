import Dialog from './components/Dialog';
import DialogContext from './components/DialogContext';
import DialogHeader from './components/DialogHeader';
import DialogBody from './components/DialogBody';
import DialogBodyWrapper from './components/DialogBodyWrapper';
import DialogFooter from './components/DialogFooter';
import {DialogSizes} from './components/DialogSizes';
import {DialogPositions} from './components/DialogPositions';

export {
  Dialog,
  DialogContext,
  DialogHeader,
  DialogBody,
  DialogBodyWrapper,
  DialogFooter,
  DialogSizes,
  DialogPositions,
};

export default Dialog;
