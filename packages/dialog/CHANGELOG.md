# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.31&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.32&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.31&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.31&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.29&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.30&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.28&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.29&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.27&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.28&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.26&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.27&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.25&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.26&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.24&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.25&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [9.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.23&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.24&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.22&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.23&targetRepoId=1246) (2023-02-20)

### Bug Fixes

- adding data-testids to Dialog ([NPM-1184](https://jiraosl.firmglobal.com/browse/NPM-1184)) ([32fde73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/32fde7376a2bab64b49624d75e262969029e60fc))
- adding data-testids to Dialog components ([NPM-1184](https://jiraosl.firmglobal.com/browse/NPM-1184)) ([176eb2a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/176eb2a10532d766931eaacef90cdd602a20e70f))

## [9.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.21&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.22&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.20&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.21&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.19&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.20&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.18&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.19&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.17&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.18&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.16&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.17&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.15&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.16&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.13&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.13&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.14&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.12&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.13&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.11&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.10&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.9&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.7&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.7&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.4&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.4&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.4&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.3&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.2&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-dialog

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@9.0.0&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- add generic parameter componentClass type ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([5136e56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5136e565e9db8d92c45df0e3417b040ec0e08e37))

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.13&sourceBranch=refs/tags/@jotunheim/react-dialog@9.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of IconButton from Dialog ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([5512c4c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5512c4c9ca0a9e02db6c3a820aaaf1bbc9eb2a81))

### Features

- remove className prop of Dialog ([NPM-935](https://jiraosl.firmglobal.com/browse/NPM-935)) ([d90972a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d90972a42ec2a3bc07cbb1f3a3c50cd6dbed2e7a))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.12&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.11&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.10&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.9&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.8&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.6&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.3&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.2&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.1&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-dialog

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dialog@8.0.0&sourceBranch=refs/tags/@jotunheim/react-dialog@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-dialog

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.20&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.21&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.19&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.20&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.18&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.19&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.17&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.18&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.16&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.17&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.14&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.15&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.13&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.14&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.12&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.13&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.11&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.12&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.10&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.11&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.9&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.10&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.6&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.7&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.5&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.6&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.4&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.5&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.3&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.4&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.2&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.3&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.1&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.2&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-dialog

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dialog@7.0.0&sourceBranch=refs/tags/@confirmit/react-dialog@7.0.1&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

# 7.0.0 (2021-10-20)

### Features

- @confirmit/react-modal package is renamed to @confirmit/react-dialog ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([fb08b42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb08b42d84a18d40b0cb0388cbbaa446eecba97e))
- add DialogPositions and position property to Dialog component ([NPM-878](https://jiraosl.firmglobal.com/browse/NPM-878)) ([e031036](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e031036bccf68c2bc334a2b7dab087b3677c57f7))
- Change data attributes: data-close-modal-button -> data-close-dialog-button ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([a73796b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a73796bc329a299da3871644c7d340d918d89db4))
- Change data attributes: data-modal-backdrop -> data-dialog-backdrop ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([fd7e2be](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fd7e2be35cc86319d42f8a3b193985e8405879fe))
- Change data attributes: data-modal-body -> data-dialog-body ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([e10c7ab](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e10c7ab6e2521871f471e7afb92d28c4cfdb29bc))
- Change data attributes: data-modal-dialog -> data-dialog-body-wrapper ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([e9e6217](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e9e6217be5910e2ab980e9cd078f39d2a1e1e0b0))
- Change data attributes: data-modal-footer -> data-dialog-footer ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([998c6b4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/998c6b42ff2bc8a9358576cd376716160de76520))
- Change data attributes: data-modal-header -> data-dialog-header ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([ca6dce0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ca6dce0daa52b697a096e1309aa7cef70f3d8720))
- Change data attributes: data-modal-header-action -> data-dialog-header-action ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([a961432](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a9614328092ec401db2e7ca10e518b5ed13885a0))
- Change data attributes: data-modal-header-actions -> data-dialog-header-actions ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([ffb10bc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ffb10bc52be5d07378c0e9e9a33a7b8d71f35aac))
- Rename Modal to Dialog ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([4219fbf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4219fbff367c308af88eebc532b55dce49e937c6))
- Rename Modal.Body to Dialog.Body ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([61b0968](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/61b0968ed81c23b9aee130eb49a0623671d9236f))
- Rename Modal.Dialog to Dialog.BodyWrapper ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([18355dc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/18355dc1772469f889a521b34deaf49104cefb4f))
- Rename Modal.Footer to Dialog.Footer ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([cf9e979](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cf9e9796b3d8f9b05230fd7d389a049b7a8c1929))
- Rename Modal.Header to Dialog.Header ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([29ec863](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/29ec86341fb6336516323b326e17ab42bdb886aa))
- Rename ModalContext to DialogContext ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([5e1725f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5e1725fcbfd4439a4c9828429615ff269b6eb66c))
- Rename ModalSizes to DialogSizes ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([61da97e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/61da97e0ac0037e2e4e81c09902d71c92555a243))
- Rename the property of Modal component: backdrop -> isModal ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([82e4b29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/82e4b29d143154b4452d8a93901111df34328554))
- Rename the property of Modal component: dialogClassName -> bodyWrapperClassName ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([0842b23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0842b2349a99f7aa591ce1e4546362cc20b74ab6))
- Rename the property of Modal component: dialogComponentClass -> bodyWrapperComponentClass ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([eee3d51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eee3d51cb520ab8a43c0d58baf8abccd6a7b22e2))

### BREAKING CHANGES

- Rename the property of Modal component: backdrop -> isModal (NPM-347)
- Change data attributes: data-modal-backdrop -> data-dialog-backdrop (NPM-347)
- Rename Modal to Dialog (NPM-347)
- Rename ModalSizes to DialogSizes (NPM-347)
- Rename ModalContext to DialogContext (NPM-347)
- Change data attributes: data-modal-header-action -> data-dialog-header-action (NPM-347)
- Change data attributes: data-modal-header-actions -> data-dialog-header-actions (NPM-347)
- Change data attributes: data-close-modal-button -> data-close-dialog-button (NPM-347)
- Change data attributes: data-modal-header -> data-dialog-header (NPM-347)
- Rename Modal.Header to Dialog.Header (NPM-347)
- Change data attributes: data-modal-footer -> data-dialog-footer (NPM-347)
- Rename Modal.Footer to Dialog.Footer (NPM-347)
- Change data attributes: data-modal-body -> data-dialog-body (NPM-347)
- Rename Modal.Body to Dialog.Body (NPM-347)
- Rename the property of Modal component: dialogComponentClass -> bodyWrapperComponentClass (NPM-347)
- Rename the property of Modal component: dialogClassName -> bodyWrapperClassName (NPM-347)
- Change data attributes: data-modal-dialog -> data-dialog-body-wrapper (NPM-347)
- Rename Modal.Dialog to Dialog.BodyWrapper (NPM-347)
- @confirmit/react-modal package is renamed to @confirmit/react-dialog (NPM-347)

## [6.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.30&sourceBranch=refs/tags/@confirmit/react-modal@6.2.31&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.29&sourceBranch=refs/tags/@confirmit/react-modal@6.2.30&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.28&sourceBranch=refs/tags/@confirmit/react-modal@6.2.29&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.27&sourceBranch=refs/tags/@confirmit/react-modal@6.2.28&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.26&sourceBranch=refs/tags/@confirmit/react-modal@6.2.27&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.25&sourceBranch=refs/tags/@confirmit/react-modal@6.2.26&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.24&sourceBranch=refs/tags/@confirmit/react-modal@6.2.25&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.23&sourceBranch=refs/tags/@confirmit/react-modal@6.2.24&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.22&sourceBranch=refs/tags/@confirmit/react-modal@6.2.23&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.21&sourceBranch=refs/tags/@confirmit/react-modal@6.2.22&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.20&sourceBranch=refs/tags/@confirmit/react-modal@6.2.21&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.19&sourceBranch=refs/tags/@confirmit/react-modal@6.2.20&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.18&sourceBranch=refs/tags/@confirmit/react-modal@6.2.19&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.17&sourceBranch=refs/tags/@confirmit/react-modal@6.2.18&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.16&sourceBranch=refs/tags/@confirmit/react-modal@6.2.17&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.15&sourceBranch=refs/tags/@confirmit/react-modal@6.2.16&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.14&sourceBranch=refs/tags/@confirmit/react-modal@6.2.15&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.13&sourceBranch=refs/tags/@confirmit/react-modal@6.2.14&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.12&sourceBranch=refs/tags/@confirmit/react-modal@6.2.13&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.11&sourceBranch=refs/tags/@confirmit/react-modal@6.2.12&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.10&sourceBranch=refs/tags/@confirmit/react-modal@6.2.11&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.9&sourceBranch=refs/tags/@confirmit/react-modal@6.2.10&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.7&sourceBranch=refs/tags/@confirmit/react-modal@6.2.8&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.6&sourceBranch=refs/tags/@confirmit/react-modal@6.2.7&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.5&sourceBranch=refs/tags/@confirmit/react-modal@6.2.6&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [6.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.4&sourceBranch=refs/tags/@confirmit/react-modal@6.2.5&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.3&sourceBranch=refs/tags/@confirmit/react-modal@6.2.4&targetRepoId=1246) (2021-03-25)

### Bug Fixes

- add min-height values to modal header and footer, to avoid them being too small when close button is not present ([NPM-747](https://jiraosl.firmglobal.com/browse/NPM-747)) ([57c1c83](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/57c1c8386c8f8ae3d0112309d0029221c604b6f6))

## [6.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.2&sourceBranch=refs/tags/@confirmit/react-modal@6.2.3&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.1&sourceBranch=refs/tags/@confirmit/react-modal@6.2.2&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.2.0&sourceBranch=refs/tags/@confirmit/react-modal@6.2.1&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-modal

# [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.7&sourceBranch=refs/tags/@confirmit/react-modal@6.2.0&targetRepoId=1246) (2021-03-10)

### Features

- add hasPadding prop on Modal.Body so content that needs to stretch edge-to-edge can easily be added. ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([520c3cc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/520c3ccde0b92ab0bf02d2cccd2937c630135ebd))

## [6.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.6&sourceBranch=refs/tags/@confirmit/react-modal@6.1.7&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-modal

## [6.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.5&sourceBranch=refs/tags/@confirmit/react-modal@6.1.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-modal

## [6.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.4&sourceBranch=refs/tags/@confirmit/react-modal@6.1.5&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-modal

## [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.2&sourceBranch=refs/tags/@confirmit/react-modal@6.1.3&targetRepoId=1246) (2021-02-11)

### Features

- Change Modal.Dialog to use a nodeRef when draggable, to support React strict mode

## [6.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.2&sourceBranch=refs/tags/@confirmit/react-modal@6.1.3&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-modal

## [6.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.1&sourceBranch=refs/tags/@confirmit/react-modal@6.1.2&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-modal

## [6.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.1.0&sourceBranch=refs/tags/@confirmit/react-modal@6.1.1&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-modal

# [6.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.19&sourceBranch=refs/tags/@confirmit/react-modal@6.1.0&targetRepoId=1246) (2021-01-20)

### Features

- Add support for anything within Modal Header Actions (actions prop can now alternatively take a ReactNode) ([NPM-677](https://jiraosl.firmglobal.com/browse/NPM-677)) ([22189d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/22189d808e353cc3a7fa1a30e3da8b7cc2110c04))

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.18&sourceBranch=refs/tags/@confirmit/react-modal@6.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.18]

Internal refactoring of export statements

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.16&sourceBranch=refs/tags/@confirmit/react-modal@6.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.15&sourceBranch=refs/tags/@confirmit/react-modal@6.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.14&sourceBranch=refs/tags/@confirmit/react-modal@6.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.13&sourceBranch=refs/tags/@confirmit/react-modal@6.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.12&sourceBranch=refs/tags/@confirmit/react-modal@6.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.11&sourceBranch=refs/tags/@confirmit/react-modal@6.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.10&sourceBranch=refs/tags/@confirmit/react-modal@6.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.7&sourceBranch=refs/tags/@confirmit/react-modal@6.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.4&sourceBranch=refs/tags/@confirmit/react-modal@6.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.3&sourceBranch=refs/tags/@confirmit/react-modal@6.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-modal

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@6.0.2&sourceBranch=refs/tags/@confirmit/react-modal@6.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-modal

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.25&sourceBranch=refs/tags/@confirmit/react-modal@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- confirmit/react-z-index peer is no more required ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([1c6b3d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1c6b3d84112c9ecd50d485adb1ba087e8b97001c))
- import usePortal from react-contexts instead of react-transition-portal ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([85d2356](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d23565811d1ec72c9dd7b99603eb24a8f7be93))

### BREAKING CHANGES

- @confirmit/react-contexts is new peer dependency
- @confirmit/react-contexts is new peer dependency

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.24&sourceBranch=refs/tags/@confirmit/react-modal@5.0.25&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.23&sourceBranch=refs/tags/@confirmit/react-modal@5.0.24&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.22&sourceBranch=refs/tags/@confirmit/react-modal@5.0.23&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.19&sourceBranch=refs/tags/@confirmit/react-modal@5.0.20&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.18&sourceBranch=refs/tags/@confirmit/react-modal@5.0.19&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.17&sourceBranch=refs/tags/@confirmit/react-modal@5.0.18&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.14&sourceBranch=refs/tags/@confirmit/react-modal@5.0.15&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.12&sourceBranch=refs/tags/@confirmit/react-modal@5.0.13&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-modal@5.0.10&sourceBranch=refs/tags/@confirmit/react-modal@5.0.11&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-modal@5.0.8...@confirmit/react-modal@5.0.9) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.7](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-modal@5.0.6...@confirmit/react-modal@5.0.7) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-modal

## [5.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-modal@5.0.0...@confirmit/react-modal@5.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-modal

# [5.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-modal@4.1.0...@confirmit/react-modal@5.0.0) (2020-08-12)

### Bug Fixes

- obtain/release z-index on "open" property change only ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([6d85fca](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/6d85fcab260c919805b419fcf92d0686e6c254f1))

### BREAKING CHANGES

- peerDependency `@confirmit/react-transition-portal": "^1.3.0"` is required

## CHANGELOG

### v4.1.0

- Feat: draggable ModalDialog now requires ModalHeader as its dragHandle. Added cursor styling to ModalHeader when draggable.

### v4.0.3

- Fix: revert `keyboard` value to be `true` by default
  (it was set to `undefined` during typescript transition unintentionally)

### v4.0.0

- **BREAKING**: ModalHeader and ModalFooter have fixed height now.
  If there is more content in a header, consider to move content to the top of ModalBody.
- Feat: Modal has maximum height and always within viewport boundaries.
- Feat: ModalBody is scrollable now.
- Fix: ModalSizes.huge is now properly applied to ModalDialog, it is now 90vw.
- Fix: ModalBody bottom margin is reduced from 32px to 24px according to DS spec.

### v3.2.0

- Feat: Typescript support
- Feat: Modal is functional component now.

### v3.1.0

- Feat: Add draggable support to Modal / Modal.Dialog

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v2.1.0

- Removing package version in class names.

### v2.0.12

- Fix: Modal title overflow, truncate characters and use ellipsis to prevent overflow.

### v2.0.7

- Refactor: Use close icon from `@confirmit/react-icons` package
- Refactor: Use same icon for both themes
- Fix: Remove custom opacity styles for close button in material theme

### v2.0.0

- **BREAKING**:
  - Require a new peerDependency `@confirmit/react-transition-portal`
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.4.0

- Feat: Added a new "actions" prop on ModalHeader, to allow more actions next to the closeButton.

### v1.3.0

- Feat: Added prop `hideOnBackdropClick`. The purpose is to set if modal should close on backdrop click or not.

### v1.2.0

- Use responsive helpers from confirmit-global-styles-material

### v1.1.0

- Update ModalHeader title styling to match mockups (\$comd-h3-v3)

### v1.0.0

- Breaking: Animation support is removed. This should fix any autoFocus issues.
- These props are removed:
  - animation
  - dialogTransitionEnterTimeout
  - dialogTransitionLeaveTimeout
  - backdropTransitionEnterTimeout
  - backdropTransitionLeaveTimeout
  - onEnter
  - onEntering
  - onEntered
  - onLeave
  - onLeaving
  - onLeft

### v0.2.2

- `withZIndexStack` wrapper added to provide `zindexStack` prop

### v0.2.0

- Update breakpoints for responsive
- Update transitions

### v0.1.0

- Feat: new prop `titleClassName`

### v0.0.1

- Initial version
