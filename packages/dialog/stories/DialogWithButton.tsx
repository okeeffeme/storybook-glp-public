import React, {forwardRef, ReactNode, Ref, useState} from 'react';
import {Dialog, DialogSizes} from '../src';
import {Button} from '../../button/src';

type DialogWithButtonProps = {
  isModal?: boolean;
  hideOnBackdropClick?: boolean;
  keyboard?: boolean;
  className?: string;
  buttonText?: ReactNode;
  children: ReactNode;
  onToggle?: () => void;
  size?: DialogSizes;
  portalZIndex?: number;
  draggable?: boolean;
};

type ForwardRefDialogButton = React.ForwardRefExoticComponent<
  DialogWithButtonProps & React.RefAttributes<HTMLDivElement>
> & {
  Header: typeof Dialog.Header;
  Body: typeof Dialog.Body;
  Footer: typeof Dialog.Footer;
};

const DialogWithButton: ForwardRefDialogButton = forwardRef(
  function DialogWithButton(
    {
      buttonText,
      children,
      isModal,
      draggable,
      keyboard,
      size,
      className,
      portalZIndex,
      hideOnBackdropClick,
      ...rest
    }: DialogWithButtonProps,
    ref: Ref<HTMLDivElement>
  ) {
    const [open, onToggle] = useState(false);

    return (
      <div
        style={{display: 'inline-block'}}
        className={className}
        {...rest}
        ref={ref}>
        <Button onClick={() => onToggle(true)}>{buttonText}</Button>
        <Dialog
          open={open}
          onHide={() => {
            onToggle(false);
          }}
          isModal={isModal}
          hideOnBackdropClick={hideOnBackdropClick}
          keyboard={keyboard}
          size={size}
          portalZIndex={portalZIndex}
          draggable={draggable}>
          {children}
        </Dialog>
      </div>
    );
  }
) as ForwardRefDialogButton;

DialogWithButton.Header = Dialog.Header;
DialogWithButton.Body = Dialog.Body;
DialogWithButton.Footer = Dialog.Footer;

export default DialogWithButton;
