import React, {useRef, useState} from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, select} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Button, ButtonRow} from '../../button/src';
import {Tooltip} from '../../tooltip/src';
import {Popover} from '../../popover/src';
import SearchField from '../../search-field/src';
import Select from '../../select/src';
import Icon, {eye, pencil} from '../../icons/src';
import {ToggleButton, ToggleButtonGroup} from '../../toggle-button/src';
import {PortalRootContext} from '../../contexts/src';

import {Dialog, DialogPositions, DialogSizes} from '../src';

import DialogWithButton from './DialogWithButton';

storiesOf('Components/dialog', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('plain dialog', () => {
    const [open, onToggle] = useState(true);

    return (
      <>
        <Button onClick={() => onToggle(true)}>Open</Button>

        <Dialog
          open={open}
          onHide={() => {
            onToggle(false);
          }}
          draggable={boolean('draggable', false)}
          isModal={boolean('isModal', false)}
          hideOnBackdropClick={boolean('hideOnBackdropClick', false)}
          keyboard={boolean('keyboard', false)}
          size={select('size', DialogSizes, DialogSizes.medium)}>
          <Dialog.Header
            title="Title"
            onCloseButtonClick={() => console.log('close dialog')}
          />
          <Dialog.Body hasPadding={boolean('Dialog.Body hasPadding', true)}>
            <div>Dialog Body!</div>
          </Dialog.Body>
          <Dialog.Footer>Dialog Footer!</Dialog.Footer>
        </Dialog>
      </>
    );
  })
  .add('with fixed/absolute position', () => {
    const [openFixed, onToggleFixed] = useState(false);
    const [openAbsolute, onToggleAbsolute] = useState(false);
    const containerRef = useRef<HTMLDivElement>(null);

    return (
      <>
        <Button onClick={() => onToggleFixed(true)}>
          Open dialog with fixed position
        </Button>
        <Button onClick={() => onToggleAbsolute(true)}>
          Open Dialog with absolute position
        </Button>

        <div
          ref={containerRef}
          style={{
            position: 'relative',
            border: '1px solid black',
            width: 600,
            height: 600,
          }}>
          <PortalRootContext.Provider value={containerRef}>
            <Dialog
              open={openFixed}
              onHide={() => {
                onToggleFixed(false);
              }}
              size={DialogSizes.medium}>
              <Dialog.Body>
                <div>Fixed dialog</div>
              </Dialog.Body>
            </Dialog>
            <Dialog
              position={DialogPositions.absolute}
              open={openAbsolute}
              onHide={() => {
                onToggleAbsolute(false);
              }}
              size={DialogSizes.medium}>
              <Dialog.Body>
                <div>Absolute dialog</div>
              </Dialog.Body>
            </Dialog>
          </PortalRootContext.Provider>
        </div>
      </>
    );
  })
  .add('with header actions', () => (
    <Dialog open={true} draggable={boolean('draggable', false)}>
      <Dialog.Header
        title="Title"
        actions={[
          {
            icon: <Icon path={pencil} />,
            tooltip: 'Action 1',
            handler: () => {},
          },
          {
            icon: <Icon path={eye} />,
            tooltip: 'Action 2',
            handler: () => {},
          },
        ]}
      />
      <Dialog.Body hasPadding={boolean('Dialog.Body hasPadding', true)}>
        Dialog Body!
      </Dialog.Body>
      <Dialog.Footer>Dialog Footer!</Dialog.Footer>
    </Dialog>
  ))
  .add('with custom header actions', () => {
    const [mode, setMode] = React.useState('edit');

    return (
      <Dialog open={true} draggable={boolean('draggable', false)}>
        <Dialog.Header
          title="Title"
          actions={
            <ToggleButtonGroup>
              <ToggleButton
                id={'edit'}
                checked={mode === 'edit'}
                onChange={() => {
                  setMode('edit');
                }}
                text={'Edit'}
                tooltipText={'Edit thing'}
                icon={<Icon path={pencil} />}
              />
              <ToggleButton
                id={'preview'}
                checked={mode === 'preview'}
                onChange={() => {
                  setMode('preview');
                }}
                text={'Preview'}
                tooltipText={'Preview thing'}
                icon={<Icon path={eye} />}
              />
            </ToggleButtonGroup>
          }
        />
        <Dialog.Body hasPadding={boolean('Dialog.Body hasPadding', true)}>
          Dialog Body!
        </Dialog.Body>
        <Dialog.Footer>Dialog Footer!</Dialog.Footer>
      </Dialog>
    );
  })
  .add('with very tall contents', () => {
    return (
      <DialogWithButton
        buttonText="dialog with tall contents"
        draggable={boolean('draggable', false)}>
        <DialogWithButton.Header title="Title" />
        <DialogWithButton.Body>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam commodo
          accumsan enim ut lobortis. Aenean tempus eros mauris, in scelerisque
          nibh pulvinar sit amet. Nullam facilisis efficitur rhoncus. Nam a
          ornare lectus, ac hendrerit leo. Sed commodo efficitur arcu, fringilla
          ornare libero facilisis nec. Nulla velit justo, vehicula in nunc non,
          interdum pretium tellus. Aenean placerat, mauris sit amet lacinia
          convallis, metus metus venenatis erat, at sollicitudin enim nisl vel
          elit. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Cras posuere at tortor mattis
          imperdiet. Nam tristique, tellus in iaculis dapibus, magna magna
          commodo dolor, quis varius justo dui non nibh. Donec ligula augue,
          elementum eu gravida a, molestie a odio. Nam rhoncus metus id risus
          molestie luctus. Nam interdum, quam eu dictum mollis, dolor ex gravida
          est, ac placerat leo diam ut nibh. Sed dictum tellus quam, et congue
          nisl finibus non. Sed efficitur fringilla tellus, tristique congue
          massa viverra et. Nullam nec massa et lorem pellentesque accumsan.
          Integer id dapibus velit. Praesent est turpis, molestie a dui
          pharetra, laoreet porta quam. Suspendisse non urna orci. Vestibulum
          ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
          Curae; Aliquam ipsum enim, fringilla vel arcu eget, dapibus hendrerit
          leo. Duis aliquet tincidunt nunc a fringilla. Quisque vitae sapien
          commodo, pellentesque nisl vitae, consectetur massa. Ut in enim
          sollicitudin, varius magna in, mattis ante. Class aptent taciti
          sociosqu ad litora torquent per conubia nostra, per inceptos
          himenaeos. Duis in tincidunt diam. In congue ligula sit amet urna
          bibendum, eu cursus dui malesuada. Nulla eu velit vitae justo
          fringilla rhoncus a at nibh. Nullam id justo magna. Pellentesque a
          finibus elit. Phasellus quam diam, facilisis sed lorem ut, pharetra
          pulvinar eros. Pellentesque et tincidunt erat. Sed justo nisl, mattis
          ac accumsan quis, ultrices pretium enim. Phasellus sodales libero id
          arcu suscipit pretium. In a placerat ligula. Pellentesque volutpat,
          orci at commodo dictum, felis mi gravida lorem, et fermentum neque
          justo sed nisi. Aenean dapibus dignissim urna, a condimentum nulla
          ornare at. Mauris nec sem et eros molestie cursus at vitae ante. Nulla
          blandit laoreet felis, in porta nunc suscipit ut. Suspendisse
          efficitur viverra ligula a efficitur. Morbi eros ligula, fermentum non
          nisi non, faucibus hendrerit lacus. Nullam facilisis mauris eu
          pellentesque semper. Integer fermentum sed lorem nec viverra. Vivamus
          tincidunt lacus eget tempus efficitur. Integer luctus dapibus felis, a
          lacinia lectus semper sit amet. Nullam at nibh rhoncus, finibus augue
          sit amet, faucibus nibh. Aenean sollicitudin turpis tortor, sit amet
          blandit neque iaculis lobortis. Proin efficitur metus a dolor varius
          viverra. Integer quis facilisis erat. Sed quis mi tincidunt,
          vestibulum quam non, iaculis velit. Curabitur efficitur eros eget odio
          auctor euismod. Integer consequat odio in fermentum commodo. Curabitur
          tempor a nibh ac vehicula. Ut in sem ac tellus egestas tristique.
          Aenean purus turpis, semper sed lorem eget, euismod convallis massa.
          Nam vitae orci vel elit pretium ullamcorper.
        </DialogWithButton.Body>
        <DialogWithButton.Footer>
          <Button>Cancel</Button>
        </DialogWithButton.Footer>
      </DialogWithButton>
    );
  })
  .add('basic with autoFocusCloseButton', () => (
    <DialogWithButton
      buttonText="dialog autoFocusCloseButton"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title" autoFocusCloseButton={true} />
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
    </DialogWithButton>
  ))
  .add('custom header', () => (
    <DialogWithButton
      buttonText="basic dialog"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title">
        <span>Another Title</span>
      </DialogWithButton.Header>
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
    </DialogWithButton>
  ))
  .add('with footer', () => (
    <DialogWithButton
      buttonText="with footer"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
      <DialogWithButton.Footer>
        <ButtonRow>
          <Button>Cancel</Button>
          <Button appearance={Button.appearances.primarySuccess}>Ok</Button>
        </ButtonRow>
      </DialogWithButton.Footer>
    </DialogWithButton>
  ))
  .add('fully customized dialog', () => (
    <DialogWithButton
      buttonText="basic dialog"
      draggable={boolean('draggable', false)}>
      <header>Custom header</header>
      <div>Custom body</div>
      <footer>Custom footer</footer>
    </DialogWithButton>
  ))
  .add('no close button', () => (
    <DialogWithButton
      buttonText="no close button"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title" closeButton={false} />
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
    </DialogWithButton>
  ))
  .add('with tooltip', () => (
    <DialogWithButton
      buttonText="basic dialog"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>
        <div>Scroll down to buttons!</div>
        <div
          style={{
            width: '1500px',
            height: '1500px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
          <div>
            Click on button and scroll to check "flip" and "visibility" of
            tooltip
          </div>
          <Tooltip content={<div>Dialog Tooltip</div>} trigger="click">
            <Button>Bottom To Top</Button>
          </Tooltip>
          <Tooltip
            content={<div>Dialog Tooltip</div>}
            trigger="click"
            placement="left">
            <Button>Left To Right</Button>
          </Tooltip>
        </div>
      </DialogWithButton.Body>
    </DialogWithButton>
  ))
  .add('with popover', () => (
    <DialogWithButton
      buttonText="basic dialog"
      draggable={boolean('draggable', false)}>
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>
        <div>Scroll down to buttons!</div>
        <div
          style={{
            width: '1500px',
            height: '1500px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
          <div>
            Click on button and scroll to check "flip" and "visibility" of
            popover
          </div>
          <Popover content={<div>Dialog Popover</div>} trigger="click">
            <Button>Bottom To Top</Button>
          </Popover>
          <Popover
            content={<div>Dialog Popover</div>}
            trigger="click"
            placement="left">
            <Button>Left To Right</Button>
          </Popover>
        </div>
      </DialogWithButton.Body>
    </DialogWithButton>
  ))
  .add('with SearchField and Select in header', () => {
    const [value, setValue] = useState<string>('');
    return (
      <>
        <DialogWithButton buttonText="open dialog with SearchField">
          <DialogWithButton.Header>
            <SearchField
              minimizable={false}
              value={value}
              onChange={(value) => {
                setValue(value as string);
              }}
            />
          </DialogWithButton.Header>
          <DialogWithButton.Body />
        </DialogWithButton>
        <DialogWithButton buttonText="open dialog with Select">
          <DialogWithButton.Header>
            <div style={{width: '300px'}}>
              <Select onChange={() => {}} />
            </div>
          </DialogWithButton.Header>
          <DialogWithButton.Body />
        </DialogWithButton>
      </>
    );
  })
  .add('do no close by backdrop click', () => (
    <DialogWithButton buttonText="open dialog" hideOnBackdropClick={false}>
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
    </DialogWithButton>
  ));

storiesOf('Components/dialog/Nested', module)
  .add('nested dialog', () => (
    <div style={{height: '2000px'}}>
      <DialogWithButton buttonText="nested dialog">
        <DialogWithButton.Header title="Title" />
        <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
        <DialogWithButton.Footer>
          <DialogWithButton buttonText="Open me!">
            <DialogWithButton.Header title="Title" />
            <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
          </DialogWithButton>
        </DialogWithButton.Footer>
      </DialogWithButton>
    </div>
  ))
  .add('nested dialog with custom z-index', () => (
    <DialogWithButton buttonText="nested dialog" portalZIndex={100}>
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
      <DialogWithButton.Footer>
        <DialogWithButton buttonText="Open me!" portalZIndex={200}>
          <DialogWithButton.Header title="Title" />
          <DialogWithButton.Body>Dialog Body!</DialogWithButton.Body>
        </DialogWithButton>
      </DialogWithButton.Footer>
    </DialogWithButton>
  ))
  .add('nested with tooltip', () => (
    <DialogWithButton buttonText="basic dialog">
      <DialogWithButton.Header title="Title" />
      <DialogWithButton.Body>
        <Tooltip content={<div>Dialog Tooltip</div>} trigger="click">
          <Button>Click me!</Button>
        </Tooltip>
        <br />
        <br />
        <Tooltip content={<div>Dialog Tooltip</div>}>
          <DialogWithButton buttonText="basic dialog">
            <DialogWithButton.Header title="Title" />
            <DialogWithButton.Body>
              <Tooltip content={<div>Dialog Tooltip</div>} trigger="click">
                <Button>Click me!</Button>
              </Tooltip>
            </DialogWithButton.Body>
          </DialogWithButton>
        </Tooltip>
      </DialogWithButton.Body>
    </DialogWithButton>
  ));

storiesOf('Components/dialog/Dialog-Modeless', module)
  .add('Modeless (no backdrop)', () => (
    <Tooltip content={<div>Dialog Tooltip</div>}>
      <DialogWithButton
        buttonText="Open dialog"
        isModal={false}
        draggable={boolean('draggable', false)}>
        <DialogWithButton.Header title="Title" />
        <DialogWithButton.Body>
          Tooltip on "open dialog"-button should still work, since you can
          interact with the background items when no backdrop is present
        </DialogWithButton.Body>
      </DialogWithButton>
    </Tooltip>
  ))
  .add('Dialog (default, backdrop)', () => (
    <Tooltip content={<div>Dialog Tooltip</div>}>
      <DialogWithButton
        buttonText="Open dialog"
        draggable={boolean('draggable', false)}>
        <DialogWithButton.Header title="Title" />
        <DialogWithButton.Body>
          Tooltip on "Open dialog"-button should not work, since you cannot
          interact with the background items when backdrop is present
        </DialogWithButton.Body>
      </DialogWithButton>
    </Tooltip>
  ));

storiesOf('Components/dialog/Draggable', module).add(
  'Draggable wrapper with draggable',
  () => (
    <Dialog.BodyWrapper draggable={true}>
      <Dialog.Header
        title="Title"
        onCloseButtonClick={(e) => {
          e.preventDefault();
          alert('nice try!');
        }}
      />
      <Dialog.Body hasPadding={boolean('Dialog.Body hasPadding', true)}>
        Dialog Body!
      </Dialog.Body>
      <Dialog.Footer>Dialog Footer!</Dialog.Footer>
    </Dialog.BodyWrapper>
  )
);
