# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-highlight-term@4.1.1&sourceBranch=refs/tags/@jotunheim/react-highlight-term@4.1.2&targetRepoId=1246) (2023-03-30)

### Bug Fixes

- add missing props ([NPM-1201](https://jiraosl.firmglobal.com/browse/NPM-1201)) ([d96d1bc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d96d1bcddeae4f40da6c08cb107f5e11a248a06e))

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-highlight-term@4.1.0&sourceBranch=refs/tags/@jotunheim/react-highlight-term@4.1.1&targetRepoId=1246) (2022-12-22)

### Bug Fixes

- add testing id for HighlightTerm component ([NPM-1173](https://jiraosl.firmglobal.com/browse/NPM-1173)) ([759e3b1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/759e3b1bfb0f80817eaf0e5e23ab3e7fb676821d))

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-highlight-term@4.0.0&sourceBranch=refs/tags/@jotunheim/react-highlight-term@4.1.0&targetRepoId=1246) (2022-10-13)

### Features

- convert HighlightTerm to Typescript ([NPM-1099](https://jiraosl.firmglobal.com/browse/NPM-1099)) ([35b926d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/35b926d2b36d3a082e0762a21e6c411409f8234a))

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-highlight-term@3.0.1&sourceBranch=refs/tags/@jotunheim/react-highlight-term@4.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of HighlightTerm ([NPM-939](https://jiraosl.firmglobal.com/browse/NPM-939)) ([f81e890](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f81e890a47406ae3e04f8aaabbc8cde4cf8fc01d))
- remove default theme from HighlightTerm ([NPM-1071](https://jiraosl.firmglobal.com/browse/NPM-1071)) ([fa67f82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fa67f82ae440f68429ab17671a530d78efc6f679))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-highlight-term@3.0.0&sourceBranch=refs/tags/@jotunheim/react-highlight-term@3.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-highlight-term

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-highlight-term@2.1.1&sourceBranch=refs/tags/@confirmit/react-highlight-term@2.1.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-highlight-term@2.1.0&sourceBranch=refs/tags/@confirmit/react-highlight-term@2.1.1&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-highlight-term

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-highlight-term@2.0.4&sourceBranch=refs/tags/@confirmit/react-highlight-term@2.1.0&targetRepoId=1246) (2021-12-10)

### Features

- added highlightBackgroundColor prop to style highlighted text ([AUT-6140](https://jiraosl.firmglobal.com/browse/AUT-6140)) ([28a71ee](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/28a71eea778b5df056b2f76d62058c11b2a3338b))

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-highlight-term@2.0.3&sourceBranch=refs/tags/@confirmit/react-highlight-term@2.0.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-highlight-term@2.0.2&sourceBranch=refs/tags/@confirmit/react-highlight-term@2.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-highlight-term

## [2.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-highlight-term@2.0.1...@confirmit/react-highlight-term@2.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-highlight-term

## CHANGELOG

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v1.1.0

- Removing package version in class names.

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.1

- Initial version
