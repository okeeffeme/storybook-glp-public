import React from 'react';
import cn from 'classnames';
import Highlighter from 'react-highlight-words';

import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './HighlightTerm.module.css';

const {block, element} = bemFactory('comd-highlight-term', classNames);

export const HighlightTerm = ({
  highlightBackgroundColor,
  searchTerm,
  text,
  ...rest
}: HighlightTermProps) => {
  const highlightTermClassNames = cn(block());
  const highlightStyle = highlightBackgroundColor
    ? {backgroundColor: highlightBackgroundColor}
    : undefined;

  return (
    <Highlighter
      data-testid="highlight-term"
      autoEscape={true}
      className={highlightTermClassNames}
      highlightClassName={element('highlight')}
      highlightStyle={highlightStyle}
      searchWords={[searchTerm]}
      textToHighlight={text}
      {...extractDataAriaIdProps(rest)}
    />
  );
};

type HighlightTermProps = {
  highlightBackgroundColor?: string;
  searchTerm?: string;
  text: string;
} & DataAriaIdAttributes;

export default HighlightTerm;
