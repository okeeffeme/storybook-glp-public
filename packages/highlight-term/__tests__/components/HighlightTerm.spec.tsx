import React from 'react';
import {render, screen} from '@testing-library/react';
import {HighlightTerm} from '../../src';

describe('Jotunheim React Highlight Term :: ', () => {
  it('renders without highlighted text', () => {
    render(<HighlightTerm text="No search term to highlight." />);

    expect(
      screen
        .getByTestId('highlight-term')
        .querySelector('.comd-highlight-term__highlight')
    ).toBe(null);
  });

  it('renders correct highlighted text', () => {
    render(
      <HighlightTerm
        searchTerm={'pizza'}
        text={'Text to highlight is "pizza"'}
      />
    );

    expect(screen.getByText('pizza')).toBeInTheDocument();
  });

  it('should render highlightStyle with correct color when highlightBackgroundColor is magenta', () => {
    render(
      <HighlightTerm
        searchTerm={'pizza'}
        text={'Text to highlight is "pizza"'}
        highlightBackgroundColor="magenta"
      />
    );

    expect(
      screen.getByTestId('highlight-term').querySelector('mark')
    ).toHaveStyle('background-color: magenta;');
  });

  it('should not render highlightStyle when highlightBackgroundColor is not specified', () => {
    render(
      <HighlightTerm
        searchTerm={'pizza'}
        text={'Text to highlight is "pizza"'}
      />
    );

    expect(
      screen.getByTestId('highlight-term').querySelector('mark')
    ).not.toHaveStyle('background-color: magenta;');
  });
});
