import React from 'react';
import {storiesOf} from '@storybook/react';
import {text} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import HighlightTerm from '../src/components/HighlightTerm';
import {TextField} from '../../text-field/src';

storiesOf('Components/highlight-term', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [value, setValue] = React.useState('');

    return (
      <div style={{width: 400, margin: 50}}>
        <TextField
          label={'Search to Highlight'}
          placeholder={'Search Text'}
          onChange={setValue}
          value={value}
        />
        <HighlightTerm
          searchTerm={value}
          text={`Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}
        />
      </div>
    );
  })
  .add('Configurable text', () => {
    const [value, setValue] = React.useState('');

    return (
      <div style={{width: 400, margin: 50}}>
        <TextField
          label={'Search to Highlight'}
          placeholder={'Search Text'}
          onChange={setValue}
          value={value}
        />
        <HighlightTerm
          searchTerm={value}
          text={text('Text to highlight', 'Configure text using knobs.')}
        />
      </div>
    );
  })
  .add('Custom highlight style', () => {
    const [value, setValue] = React.useState('ipsum');

    return (
      <div style={{width: 400, margin: 50}}>
        <TextField
          label={'Search to Highlight'}
          placeholder={'Search Text'}
          onChange={setValue}
          value={value}
        />
        <HighlightTerm
          searchTerm={value}
          text={`Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}
          highlightBackgroundColor="magenta"
        />
      </div>
    );
  });
