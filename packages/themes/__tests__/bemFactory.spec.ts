import {bemFactory} from '../src/bemFactory';

describe('Bem Factory', () => {
  const version = 'v3-2-1';
  const baseClassName = 'block-class';
  const classNames = {
    'block-class': 'block-class___module',
    'block-class--modifier': 'block-class--modifier___module',
    'block-class__element': 'block-class__element___module',
    'block-class__element--modifier': 'block-class__element--modifier___module',
  };
  const classNamesWithVersion = {
    'block-class_v3-2-1': 'block-class_v3-2-1___module',
    'block-class--modifier_v3-2-1': 'block-class--modifier_v3-2-1___module',
    'block-class__element_v3-2-1': 'block-class__element_v3-2-1___module',
    'block-class__element--modifier_v3-2-1':
      'block-class__element--modifier_v3-2-1___module',
  };

  const otherClassNames = {
    'a-banana': 'a-banana',
    'a-banana__peel': 'a-banana__peel',
  };

  describe('arguments as object', () => {
    it('should generate classes according to baseClassName and classNames', () => {
      const {block, element, modifier} = bemFactory({
        baseClassName,
        classNames,
      });

      expect(block()).toBe('block-class___module');
      expect(modifier('modifier')).toBe('block-class--modifier___module');
      expect(element('element')).toBe('block-class__element___module');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier___module'
      );
    });

    it('should generate classes according to baseClassName and classNames when classNames contains a mix of classes starting with baseClassName and other classes', () => {
      const {block, element, modifier} = bemFactory({
        baseClassName,
        classNames: {
          ...otherClassNames,
          ...classNames,
        },
      });

      expect(block()).toBe('block-class___module');
      expect(modifier('modifier')).toBe('block-class--modifier___module');
      expect(element('element')).toBe('block-class__element___module');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier___module'
      );
    });

    it('should generate classes according to baseClassName only', () => {
      const {block, element, modifier} = bemFactory({baseClassName});

      expect(block()).toBe('block-class');
      expect(modifier('modifier')).toBe('block-class--modifier');
      expect(element('element')).toBe('block-class__element');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier'
      );
    });

    it('should generate classes according to baseClassName when classNames does not contain any classes starting with baseClassName', () => {
      const {block, element, modifier} = bemFactory({
        baseClassName,
        classNames: otherClassNames,
      });

      expect(block()).toBe('block-class');
      expect(modifier('modifier')).toBe('block-class--modifier');
      expect(element('element')).toBe('block-class__element');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier'
      );
    });

    it('should generate classes according to baseClassName and version', () => {
      const {block, element, modifier} = bemFactory({baseClassName, version});

      expect(block()).toBe('block-class_v3-2-1');
      expect(modifier('modifier')).toBe('block-class--modifier_v3-2-1');
      expect(element('element')).toBe('block-class__element_v3-2-1');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier_v3-2-1'
      );
    });

    it('should generate classes according to baseClassName, classNames and version', () => {
      const {block, element, modifier} = bemFactory({
        baseClassName,
        classNames: classNamesWithVersion,
        version,
      });

      expect(block()).toBe('block-class_v3-2-1___module');
      expect(modifier('modifier')).toBe(
        'block-class--modifier_v3-2-1___module'
      );
      expect(element('element')).toBe('block-class__element_v3-2-1___module');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier_v3-2-1___module'
      );
    });
  });

  describe('arguments as arguments', () => {
    it('should generate classes according to baseClassName and classNames', () => {
      const {block, element, modifier} = bemFactory(baseClassName, classNames);

      expect(block()).toBe('block-class___module');
      expect(modifier('modifier')).toBe('block-class--modifier___module');
      expect(element('element')).toBe('block-class__element___module');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier___module'
      );
    });

    it('should generate classes according to baseClassName only', () => {
      const {block, element, modifier} = bemFactory(baseClassName);

      expect(block()).toBe('block-class');
      expect(modifier('modifier')).toBe('block-class--modifier');
      expect(element('element')).toBe('block-class__element');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier'
      );
    });

    it('should generate classes according to baseClassName and version', () => {
      const {block, element, modifier} = bemFactory(
        baseClassName,
        undefined,
        version
      );

      expect(block()).toBe('block-class_v3-2-1');
      expect(modifier('modifier')).toBe('block-class--modifier_v3-2-1');
      expect(element('element')).toBe('block-class__element_v3-2-1');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier_v3-2-1'
      );
    });

    it('should generate classes according to baseClassName, classNames and version', () => {
      const {block, element, modifier} = bemFactory(
        baseClassName,
        classNamesWithVersion,
        version
      );

      expect(block()).toBe('block-class_v3-2-1___module');
      expect(modifier('modifier')).toBe(
        'block-class--modifier_v3-2-1___module'
      );
      expect(element('element')).toBe('block-class__element_v3-2-1___module');
      expect(element('element', 'modifier')).toBe(
        'block-class__element--modifier_v3-2-1___module'
      );
    });
  });
});
