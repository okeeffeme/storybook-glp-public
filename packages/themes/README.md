# Jotunheim React Themes

This package helps to add theming support to components.
It uses the new React Context API to set the theme, which also makes it possible to set a different theme for different parts of an app.

## Example

### v3 ->

```jsx
import {theme, ThemeContext} from '@jotunheim/react-themes';

<ThemeContext.Provider value={theme.themeNames.material}>
  <App />
</ThemeContext.Provider>;
```
