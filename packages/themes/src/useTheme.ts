import {useContext} from 'react';

import {getTheme, themeNames} from './theme';
import {ThemeContext} from './ThemeContext';

type MapThemeToProps<TThemeProps> = (
  props: TThemeProps
) => Record<string, TThemeProps>;

const defaultMapThemeToProps = <TThemeProps>(
  componentNames: string[]
): MapThemeToProps<TThemeProps> => {
  return (theme: TThemeProps): Record<string, TThemeProps> => {
    if (!theme) return {};

    return componentNames.reduce((acc, next) => {
      return {
        ...acc,
        ...theme[next],
      };
    }, {});
  };
};

export const useTheme = <TThemeProps>(
  componentName: string | string[] | MapThemeToProps<TThemeProps>,
  themes: Record<themeNames, Record<string, TThemeProps>>
): TThemeProps => {
  const themeName = useContext(ThemeContext);
  const theme = getTheme(themeName, themes);

  if (!componentName) {
    throw new Error('Must specify a component name');
  }

  let mapThemeToProps;
  if (typeof componentName === 'function') {
    mapThemeToProps = componentName;
  } else if (Array.isArray(componentName)) {
    mapThemeToProps = defaultMapThemeToProps(componentName);
  } else {
    mapThemeToProps = defaultMapThemeToProps([componentName]);
  }

  return mapThemeToProps(theme);
};
