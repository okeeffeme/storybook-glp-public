import React from 'react';
import hoistNonReactStatics from 'hoist-non-react-statics';
import {getDisplayName} from '@jotunheim/react-utils';

import {useTheme} from './useTheme';

export const withTheme = (componentName, themes) => (WrappedComponent) => {
  if (!componentName) return WrappedComponent;

  const ThemedComponent = (props) => {
    const themeProps = useTheme(componentName, themes);

    return <WrappedComponent {...themeProps} {...props} />;
  };

  // use the same displayName as WrappedComponent, so snapshot won't rely on implementation
  ThemedComponent.displayName = getDisplayName(WrappedComponent);

  hoistNonReactStatics(ThemedComponent, WrappedComponent);

  return ThemedComponent;
};
