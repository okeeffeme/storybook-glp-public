export {withTheme} from './withTheme';
export {bemFactory} from './bemFactory';
export {ThemeContext} from './ThemeContext';
export {useTheme} from './useTheme';

import * as _theme from './theme';
export {_theme as theme};
