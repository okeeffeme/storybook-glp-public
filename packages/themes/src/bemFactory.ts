/*
  bem factory can be used either for css modules approach or regular css classes.
  In case of css modules non-empty classnames obj should be passed.
 */

type BemFactory = {
  block: () => string;
  element: (elementName: string, modifierClassName?: string) => string;
  modifier: (modifierName: string) => string;
};

type BemFactoryOptions = {
  baseClassName: string;
  classNames?: Record<string, string>;
  version?: string;
};

const classesThatStartWithBaseClassName = ({classNames, baseClassName}) => {
  if (!classNames) {
    return [];
  }

  return Object.keys(classNames).reduce((accumulator, current) => {
    if (current.startsWith(baseClassName as string)) {
      return [...accumulator, current];
    }
    return accumulator;
  }, [] as string[]);
};

export const bemFactory = (
  baseClassName: string | BemFactoryOptions,
  classNames?: Record<string, string>,
  version?: string
): BemFactory => {
  if (typeof baseClassName !== 'string') {
    version = baseClassName.version;
    classNames = baseClassName.classNames;
    baseClassName = baseClassName.baseClassName;
  }

  const useHash =
    classesThatStartWithBaseClassName({classNames, baseClassName}).length > 0;
  const getClassName = classNameKey => {
    if (version) {
      classNameKey = `${classNameKey}_${version}`;
    }

    if (classNames && useHash) {
      return classNames[classNameKey];
    }

    return classNameKey;
  };

  return {
    block() {
      return getClassName(baseClassName) || '';
    },

    element(elementName, modifierName) {
      const className = modifierName
        ? `${baseClassName}__${elementName}--${modifierName}`
        : `${baseClassName}__${elementName}`;

      return getClassName(className) || '';
    },

    modifier(modifierName) {
      const className = `${baseClassName}--${modifierName}`;
      return getClassName(className) || '';
    },
  };
};
