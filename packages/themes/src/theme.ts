export enum themeNames {
  default = 'default',
  material = 'material',
}

const _storedThemes: Record<themeNames, unknown> = {
  [themeNames.default]: {},
  [themeNames.material]: {},
};

export const configureTheme = (themeName: themeNames, themeProps: unknown) => {
  _storedThemes[themeName] = _storedThemes[themeName] || {};

  Object.assign(_storedThemes[themeName], themeProps);
};

export const getTheme = (
  themeName: themeNames,
  themes: Record<themeNames, unknown> = _storedThemes
) => {
  return themes[themeName];
};
