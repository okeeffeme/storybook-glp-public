import {createContext} from 'react';

import {themeNames} from './theme';

export const ThemeContext = createContext<themeNames>(themeNames.material);
