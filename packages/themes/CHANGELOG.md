# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-themes@6.0.3&sourceBranch=refs/tags/@jotunheim/react-themes@6.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-themes

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-themes@6.0.2&sourceBranch=refs/tags/@jotunheim/react-themes@6.0.3&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-themes

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-themes@6.0.1&sourceBranch=refs/tags/@jotunheim/react-themes@6.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-themes

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-themes@6.0.0&sourceBranch=refs/tags/@jotunheim/react-themes@6.0.1&targetRepoId=1246) (2022-07-08)

### Bug Fixes

- change the default value of ThemeContext to material ([ac163c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ac163c28205291e57ea27de6ee2bc59378f904c4))

## 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-themes@5.0.5&sourceBranch=refs/tags/@confirmit/react-themes@5.0.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-themes@5.0.4&sourceBranch=refs/tags/@confirmit/react-themes@5.0.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-themes

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-themes@5.0.3&sourceBranch=refs/tags/@confirmit/react-themes@5.0.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-themes@5.0.2&sourceBranch=refs/tags/@confirmit/react-themes@5.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-themes

## [5.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-themes@5.0.1...@confirmit/react-themes@5.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-themes

## CHANGELOG

### v5.0.0

- Refactor: new package name: `@confirmit/react-themes`

### v4.0.3

- Fix: Do not consider classNames that does not start with baseClassName when deciding to use hash in classes. This fixes the issue when using css modules :export feature to be able to import css variables in JS.

### v4.0.2

- **BREAKING**: Do not output version in classname

### v4.0.0

- **BREAKING**
  - Update types of bemFactory to allow us to use classnames the way we normally use it with inlining objects

### v3.3.0

- Typescript support

### v3.2.0

- Feat: useTheme hooks now supports a second argument for the theme styles,
  instead of relying on components having called configureTheme first
- Feat: bemFactory has ability to set className with version on blocks and elements. This is meant or internal usage only.

### v3.1.0

- Feat: useTheme hook is now the preferred way of getting the theme props inside a component

### v3.0.5

- Fix: Move confirmit-react-utils to `dependencies`

### v3.0.4

- `getDisplayName` is taken from confirmit-react-utils package

### v3.0.1

- Newer version of hoist-react-non-statics

### v3.0.0

- **BREAKING**
  - Removed ThemeProvider, not in use.
  - theme.useTheme method no longer exists, theme should be set via the new Context API, use the ThemeContext component.
  - Also removed theme.getThemeName, use ThemeContext to read current theme.
  - Removed addThemeChangeListener and removeThemeChangeListener methods, no longer needed when using new Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v2.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v1.1.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

### v1.0.1

- Fix: Remove disabled prop of ThemeProvider as it was not supposed to be released at all.

### v1.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only
