import React from 'react';
import {storiesOf} from '@storybook/react';
import {select, boolean, object} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Button} from '../../button/src';
import ConfirmationDialog, {
  ConfirmationMode,
  DialogSizes,
} from '../src/components/ConfirmationDialog';

const Container = ({children}) => (
  <div style={{padding: 20, margin: 40, border: '1px solid #ccc'}}>
    {children}
  </div>
);

storiesOf('Components/confirmation-dialog', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default - with knobs', () => {
    const [isOpen, setIsOpen] = React.useState(false);

    const onConfirm = React.useCallback(() => {
      window.alert('Confirm was clicked');
      setIsOpen(false);
    }, []);

    const onClose = React.useCallback(() => {
      setIsOpen(false);
    }, []);

    return (
      <Container>
        <Button onClick={() => setIsOpen(true)}>Confirm</Button>

        <ConfirmationDialog
          open={isOpen}
          mode={select('mode', ConfirmationMode, ConfirmationMode.Default)}
          isWaiting={boolean('isWaiting', false)}
          isConfirmDisabled={boolean('isConfirmDisabled', false)}
          autoFocusCloseButton={boolean('autoFocusCloseButton', false)}
          size={select('size', DialogSizes, DialogSizes.medium)}
          texts={object('texts', {
            ok: 'Remove',
            waiting: 'Removing',
          })}
          onConfirm={onConfirm}
          onClose={onClose}
          title={'Remove thing'}>
          <div>Are you sure?</div>
        </ConfirmationDialog>
      </Container>
    );
  })
  .add('With extra verification', () => {
    const [isOpen, setIsOpen] = React.useState(false);

    const onConfirm = React.useCallback(() => {
      window.alert('Confirm was clicked');
      setIsOpen(false);
    }, []);

    const onClose = React.useCallback(() => {
      setIsOpen(false);
    }, []);

    return (
      <Container>
        <Button onClick={() => setIsOpen(true)}>Delete</Button>

        <ConfirmationDialog
          open={isOpen}
          texts={object('texts', {
            ok: 'Delete forever',
          })}
          mode={ConfirmationMode.DangerWithVerification}
          title={'Delete something'}
          autoFocusCloseButton={true}
          onConfirm={onConfirm}
          onClose={onClose}>
          <div>Are you sure? This is not reversible.</div>
        </ConfirmationDialog>
      </Container>
    );
  });
