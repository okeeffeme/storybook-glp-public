# Jotunheim React Confirmation Dialog

[Changelog](./CHANGELOG.md)

A more specialized modal/dialog for confirming user actions, similar to the native browser window.confirm utility.

Supports a "mode" prop, with three different alternatives:

- Default: Will show the Confirm button as a green "primarySuccess" button. Should be used when the action to confirm is not destructive, or not in any way drastic or potentially has serious ramifications.A

- Danger: Will show the Confirm button as a red "primaryDanger" button. Should be used when action is destructive or potentially a bit drastic.

- DangerWithVerification: Will also show the Confirm button as a red "primaryDanger" button. Should be used when the action is destructive and permanent, like deleting a Live survey, or erasing all data in a database.

Check with UX if in doubt about which mode to use.
