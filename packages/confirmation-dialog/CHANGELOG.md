# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.5.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.27&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.28&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.27&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.27&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.25&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.26&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.24&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.25&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.23&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.24&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.23&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.21&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.22&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.20&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.21&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([59eb409](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/59eb409c260da66b1ce7244f398a4f3a3d26b32c))

## [2.5.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.19&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.20&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.18&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.19&targetRepoId=1246) (2023-02-20)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.17&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.18&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.16&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.17&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.15&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.16&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.14&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.15&targetRepoId=1246) (2023-02-06)

### Bug Fixes

- adding data-testids to Confirmation-dialog component([NPM-1181](https://jiraosl.firmglobal.com/browse/NPM-1181)) ([94bba46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/94bba4672350b5d2d9c355340a6975dcbe3f5ea6))
- Confirmation-dialog component fixes ([NPM-1181](https://jiraosl.firmglobal.com/browse/NPM-1181)) ([fa3f606](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fa3f60615c12250a50b6bc362e7cb711c616f2bc))
- data-apperance logic in ConfirmationDialog component ([NPM-1181](https://jiraosl.firmglobal.com/browse/NPM-1181)) ([0ca7e81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0ca7e8199b0787ab472ce634448f9576c4b1e9a6))

## [2.5.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.13&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.14&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.12&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.13&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.11&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.12&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.10&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.11&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.9&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.10&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.7&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.9&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.7&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.8&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.6&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.7&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.5&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.6&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.4&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.5&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.3&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.4&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.2&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.3&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.1&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.2&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.0&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.1&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

# [2.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.5.0&targetRepoId=1246) (2022-12-16)

### Features

- add dialog window size parameter support ([NPM-1121](https://jiraosl.firmglobal.com/browse/NPM-1121)) ([16d20fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/16d20facdae85fc87bad9c2b6e447a08b13c5433))

# [2.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.4.0&targetRepoId=1246) (2022-12-15)

### Features

- add dialog window size parameter support ([NPM-1121](https://jiraosl.firmglobal.com/browse/NPM-1121)) ([16d20fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/16d20facdae85fc87bad9c2b6e447a08b13c5433))

# [2.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.3.0&targetRepoId=1246) (2022-12-06)

### Features

- add dialog window size parameter support ([NPM-1121](https://jiraosl.firmglobal.com/browse/NPM-1121)) ([16d20fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/16d20facdae85fc87bad9c2b6e447a08b13c5433))

# [2.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.2.0&targetRepoId=1246) (2022-12-02)

### Features

- add dialog window size parameter support ([NPM-1121](https://jiraosl.firmglobal.com/browse/NPM-1121)) ([16d20fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/16d20facdae85fc87bad9c2b6e447a08b13c5433))

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.1.0&targetRepoId=1246) (2022-11-29)

### Features

- add dialog window size parameter support ([NPM-1121](https://jiraosl.firmglobal.com/browse/NPM-1121)) ([7849c96](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7849c9635e453f1cda226461650458c2e9ef63d4))

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.21&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.22&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.20&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.21&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.18&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.19&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.17&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.18&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.16&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.15&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.14&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.13&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.12&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.11&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.10&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.9&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.7&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.4&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.3&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.2&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.1&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.0&sourceBranch=refs/tags/@jotunheim/react-confirmation-dialog@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-confirmation-dialog

## 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.86](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.85&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.86&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.84&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.85&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.83&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.84&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.83](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.82&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.83&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.81&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.82&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.80&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.81&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.78&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.79&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.77&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.78&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.76&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.77&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.75&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.76&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.74&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.75&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.73&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.74&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.70&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.71&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.69&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.70&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.68&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.69&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.67&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.68&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.66&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.67&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.65&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.66&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.64&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.65&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.63&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.64&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [1.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.62&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.63&targetRepoId=1246) (2021-10-20)

### Bug Fixes

- replace @confirmit/react-modal with @confirmit/react-dialog because of package renaming ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([cb47cac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cb47cac4cebcc454b6e83b9d461f168b5edf5311))

## [1.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.60&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.61&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.59&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.60&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.58&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.59&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.57&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.58&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.56&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.57&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.55&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.56&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.54&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.55&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.53&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.54&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.52&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.53&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.51&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.52&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.50&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.51&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.49&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.50&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.48&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.49&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.47&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.48&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.46&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.47&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.45&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.46&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.44&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.45&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.43&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.44&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.42&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.43&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.41&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.42&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.40&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.41&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.39&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.40&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.38&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.39&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.37&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.38&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.35&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.36&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.34&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.35&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.33&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.34&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.32&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.33&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [1.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.31&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.32&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.30&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.31&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.29&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.30&targetRepoId=1246) (2021-03-17)

### Bug Fixes

- use ButtonRow component to space buttons in footer correctly ([NPM-730](https://jiraosl.firmglobal.com/browse/NPM-730)) ([d825f5d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d825f5d5bd179eb941ce96cf7749d64eda93be7f))

## [1.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.28&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.29&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.27&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.28&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.26&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.27&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.25&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.26&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.24&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.23&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.21&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.20&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.19&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.18&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.19&targetRepoId=1246) (2021-01-20)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.17&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.18&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.16&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.17&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.14&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.15&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.13&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.12&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.13&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.11&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.10&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.9&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.10&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.8&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.9&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.7&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.8&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.4&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.5&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- fix default verification text ([NPM-641](https://jiraosl.firmglobal.com/browse/NPM-641)) ([8fcc232](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8fcc232223ef6e492d80ed7ccbee6cd3ea000805))

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.3&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.4&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.0&sourceBranch=refs/tags/@confirmit/react-confirmation-dialog@1.0.1&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-confirmation-dialog

## CHANGELOG

### v1.0.0

- Initial version
