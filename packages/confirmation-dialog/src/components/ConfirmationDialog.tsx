import React, {AriaAttributes} from 'react';

import Dialog, {DialogSizes} from '@jotunheim/react-dialog';
import Button, {ButtonRow} from '@jotunheim/react-button';
import TextField from '@jotunheim/react-text-field';
import WaitButton from '@jotunheim/react-wait-button';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import classNames from './ConfirmationDialog.module.css';

const {block} = bemFactory('comd-confirmation-dialog-verification', classNames);

export const CONFIRM_DELETE_TEXT = '1234';

export enum ConfirmationMode {
  Default = 'default',
  Danger = 'danger',
  DangerWithVerification = 'dangerWithVerification',
}

export type ConfirmationDialogTexts = {
  ok?: string;
  cancel?: string;
  waiting?: string;
  verificationPlaceholder?: string;
};

export type ConfirmationDialogProps = {
  title: React.ReactNode;
  onClose: () => void;
  onConfirm: () => void;
  children: React.ReactNode;
  mode?: ConfirmationMode;
  open?: boolean;
  isWaiting?: boolean;
  isConfirmDisabled?: boolean;
  texts?: ConfirmationDialogTexts;
  autoFocusCloseButton?: boolean;
  size?: DialogSizes | string;
} & AriaAttributes;

export const defaultTexts = {
  ok: 'OK',
  cancel: `Cancel`,
  waiting: '',
  verificationPlaceholder: `Enter "${CONFIRM_DELETE_TEXT}" here`,
};

export {DialogSizes};

const shouldDisableConfirm = (
  isConfirmDisabled: boolean,
  mode: ConfirmationMode,
  verificationText: string
) => {
  return (
    isConfirmDisabled ||
    (mode === ConfirmationMode.DangerWithVerification &&
      verificationText.toUpperCase() !== CONFIRM_DELETE_TEXT)
  );
};

export const ConfirmationDialog = ({
  mode = ConfirmationMode.Default,
  open = false,
  title,
  children,
  isWaiting = false,
  isConfirmDisabled = false,
  autoFocusCloseButton = false,
  onClose,
  onConfirm,
  texts,
  size = DialogSizes.medium,
  ...rest
}: ConfirmationDialogProps) => {
  texts = {
    ...defaultTexts,
    ...texts,
  };

  const cancelButtonRef = React.useRef<HTMLElement>(null);
  const [verificationText, setVerificationText] = React.useState<string>('');
  const showCloseInHeader = mode === ConfirmationMode.Default;
  const isVerificationDisabled = isWaiting || isConfirmDisabled;
  const appearance =
    mode === ConfirmationMode.Default
      ? WaitButton.appearances.primarySuccess
      : WaitButton.appearances.primaryDanger;

  React.useEffect(() => {
    if (open) {
      setVerificationText('');
    }
  }, [open]);

  React.useEffect(() => {
    // Need the timeout because could conflict when other elements on the page get focus as well
    setTimeout(() => {
      if (open && autoFocusCloseButton && !showCloseInHeader) {
        // need to focus the Cancel button instead
        cancelButtonRef.current?.focus();
      }
    }, 0);
  }, [open, autoFocusCloseButton, showCloseInHeader]);

  return (
    <Dialog
      open={open}
      data-testid="confirmation-dialog"
      onHide={onClose}
      isModal={true}
      keyboard={mode !== ConfirmationMode.DangerWithVerification}
      size={size}
      data-size={size}
      data-test-confirmation-dialog
      {...extractDataAriaIdProps(rest)}>
      <Dialog.Header
        title={title}
        closeButton={showCloseInHeader}
        onCloseButtonClick={onClose}
        autoFocusCloseButton={autoFocusCloseButton}
      />

      <Dialog.Body>
        {children}

        {mode === ConfirmationMode.DangerWithVerification && (
          <div className={block()}>
            <TextField
              value={verificationText}
              onChange={setVerificationText}
              disabled={isVerificationDisabled}
              data-test="confirmation-dialog-verification"
              data-testid="confirmation-dialog-verification"
              placeholder={texts.verificationPlaceholder}
            />
          </div>
        )}
      </Dialog.Body>

      <Dialog.Footer>
        <ButtonRow>
          {isWaiting === false && (
            <Button
              onClick={onClose}
              ref={cancelButtonRef}
              data-test="confirmation-dialog-cancel"
              data-testid="confirmation-dialog-cancel"
              appearance={Button.appearances.secondaryNeutral}>
              {texts.cancel}
            </Button>
          )}
          <WaitButton
            data-test="confirmation-dialog-ok"
            appearance={appearance}
            data-appearance={appearance}
            busyChildren={texts.waiting}
            busy={isWaiting}
            disabled={shouldDisableConfirm(
              isConfirmDisabled,
              mode,
              verificationText
            )}
            onClick={onConfirm}>
            {texts.ok}
          </WaitButton>
        </ButtonRow>
      </Dialog.Footer>
    </Dialog>
  );
};

export default ConfirmationDialog;
