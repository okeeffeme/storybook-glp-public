import ConfirmationDialog, {
  ConfirmationMode,
  CONFIRM_DELETE_TEXT,
  DialogSizes,
} from './components/ConfirmationDialog';

export {ConfirmationMode, ConfirmationDialog, CONFIRM_DELETE_TEXT, DialogSizes};

export default ConfirmationDialog;
