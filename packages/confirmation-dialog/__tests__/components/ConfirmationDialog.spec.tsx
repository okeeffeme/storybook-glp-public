import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {
  ConfirmationDialog,
  ConfirmationMode,
  ConfirmationDialogProps,
  DialogSizes,
} from '../../src/components/ConfirmationDialog';
import WaitButton from '../../../wait-button/src';

const defaultProps = {
  title: 'title',
  okText: 'ok',
  onClose: () => {},
  onConfirm: () => {},
  open: true,
  children: <div />,
};

const renderConfirmationDialog = (props?: ConfirmationDialogProps) => {
  return render(
    <ConfirmationDialog {...defaultProps} {...props}>
      <div />
    </ConfirmationDialog>
  );
};

describe('Jotunheim React Confirmation Dialog :: ', () => {
  it('should show with primarySuccess button, and not verification field, for default mode', () => {
    renderConfirmationDialog();
    expect(screen.getByTestId('wait-button')).toBeInTheDocument();
    expect(
      screen.queryByTestId('confirmation-dialog-verification')
    ).not.toBeInTheDocument();
    expect(screen.getByTestId('wait-button')).toHaveAttribute(
      'data-appearance',
      WaitButton.appearances.primarySuccess
    );
  });

  it('should show with primaryDanger button, and not verification field, for danger mode', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.Danger,
    });

    expect(screen.getByTestId('wait-button')).toBeInTheDocument();
    expect(
      screen.queryByTestId('confirmation-dialog-verification')
    ).not.toBeInTheDocument();
    expect(screen.getByTestId('wait-button')).toHaveAttribute(
      'data-appearance',
      WaitButton.appearances.primaryDanger
    );
  });

  it('should show with primaryDanger button and verification field for dangerWithVerification mode', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
    });

    expect(screen.getByTestId('wait-button')).toBeInTheDocument();
    expect(
      screen.getByTestId('confirmation-dialog-verification')
    ).toBeInTheDocument();
    expect(screen.getByTestId('wait-button')).toHaveAttribute(
      'data-appearance',
      WaitButton.appearances.primaryDanger
    );
  });

  it('should not disable verification field by default', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
    });
    expect(
      screen.getByTestId('confirmation-dialog-verification')
    ).not.toBeDisabled();
  });

  it('should disable verification field while waiting', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
      isWaiting: true,
    });
    expect(screen.getByTestId('simple-text-field')).toBeDisabled();
  });

  it('should disable verification field while confirm is disabled', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
      isConfirmDisabled: true,
    });

    expect(screen.getByTestId('simple-text-field')).toBeDisabled();
  });

  it('should show Cancel button when not waiting', () => {
    renderConfirmationDialog();
    expect(
      screen.getByTestId('confirmation-dialog-cancel')
    ).toBeInTheDocument();
  });

  it('should not show Cancel button when waiting', () => {
    renderConfirmationDialog({
      ...defaultProps,
      isWaiting: true,
    });

    expect(
      screen.queryByTestId('confirmation-dialog-cancel')
    ).not.toBeInTheDocument();
  });

  it('should not disable Ok button when not specified', () => {
    renderConfirmationDialog();
    expect(screen.getByTestId('wait-button')).not.toBeDisabled();
  });

  it('should disable Ok button when specified', () => {
    renderConfirmationDialog({
      ...defaultProps,
      isConfirmDisabled: true,
    });

    expect(screen.getByTestId('wait-button')).toBeDisabled();
  });

  it('should disable Ok button when mode is dangerWithVerification, until 1234 is typed', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
    });

    expect(screen.getByTestId('wait-button')).toBeDisabled();
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: '1234'},
    });
    expect(screen.getByTestId('wait-button')).not.toBeDisabled();
  });

  it('should still disable Ok button when mode is dangerWithVerification, and something else is typed', () => {
    renderConfirmationDialog({
      ...defaultProps,
      mode: ConfirmationMode.DangerWithVerification,
    });

    expect(screen.getByTestId('wait-button')).toBeDisabled();
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: '123'},
    });
    expect(screen.getByTestId('wait-button')).toBeDisabled();
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: '12345'},
    });
    expect(screen.getByTestId('wait-button')).toBeDisabled();
  });

  it('should have medium size dialog window when size parameter is not specified', () => {
    renderConfirmationDialog({
      ...defaultProps,
    });
    expect(screen.getByTestId('confirmation-dialog')).toHaveAttribute(
      'data-size',
      DialogSizes.medium
    );
  });

  it('should pass size parameter to dialog window when the parameter is specified', () => {
    renderConfirmationDialog({
      ...defaultProps,
      size: DialogSizes.large,
    });

    expect(screen.getByTestId('confirmation-dialog')).toHaveAttribute(
      'data-size',
      DialogSizes.large
    );
  });
});
