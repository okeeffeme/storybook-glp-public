import CopyToClipboard from './components/CopyToClipboard';
import {Appearances} from './components/Appearances';

export {CopyToClipboard, Appearances};
export default CopyToClipboard;
