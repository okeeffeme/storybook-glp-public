import React, {AriaAttributes} from 'react';
import {useCopyToClipboard, useTimeoutFn} from 'react-use';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {IconButton} from '@jotunheim/react-button';

import {Appearances} from './Appearances';
import IconButtonCopy from './IconButtonCopy';
import ButtonCopy from './ButtonCopy';
import cn from 'classnames';

import classNames from './CopyToClipboard.module.css';

type CopyToClipboardProps = {
  value: string;
  appearance?: Appearances;
  labels?: {
    copy?: string;
    copied?: string;
  };
  showValue?: boolean;
} & AriaAttributes;

const defaultObj = {};

const defaultLabels = {
  copy: 'Copy',
  copied: 'Copied',
};

const preventDefault = (e: Event) => {
  e.preventDefault();
  e.stopPropagation();
};

const {block, element} = bemFactory('comd-copy-to-clipboard', classNames);

const CopyToClipboard = ({
  value,
  appearance = Appearances.default,
  labels = defaultObj,
  showValue = true,
  ...rest
}: CopyToClipboardProps) => {
  const [isCopied, setIsCopied] = React.useState(false);
  const [, copyToClipboard] = useCopyToClipboard();
  const [, , startTimer] = useTimeoutFn(() => {
    setIsCopied(false);
  }, 1500);

  const handleClick = () => {
    setIsCopied(true);
    copyToClipboard(value);
    startTimer();
  };

  const buttonClassName = cn(element('button'), element('button', appearance), {
    [element('button', `${appearance}-copied`)]: isCopied,
  });

  const props = {
    isCopied: isCopied,
    labels: {...defaultLabels, ...labels},
    onClick: isCopied ? preventDefault : handleClick,
    element,
  };

  const renderComponent = () => {
    switch (appearance) {
      case Appearances.icon:
        return <IconButtonCopy {...props} />;
      case Appearances.iconSmall:
        return <IconButtonCopy {...props} size={IconButton.sizes.small} />;
    }

    return <ButtonCopy className={buttonClassName} {...props} />;
  };

  return (
    <div
      className={block()}
      data-testid="copy-to-clipboard"
      {...extractDataAriaIdProps(rest)}>
      {showValue &&
        appearance !== Appearances.icon &&
        appearance !== Appearances.iconSmall && (
          <div className={element('value')}>{value}</div>
        )}
      {renderComponent()}
    </div>
  );
};

export default CopyToClipboard;
