import React from 'react';
import Icon, {check} from '@jotunheim/react-icons';

type LinkContentProps = {
  element: (el: string, mod?: string) => string;
  isCopied: boolean;
  labels: {
    copy: string;
    copied: string;
  };
};

const ButtonContent = ({
  isCopied,
  element,
  labels: {copy: copyLabel, copied: copiedLabel},
}: LinkContentProps) =>
  isCopied ? (
    <span className={element('button-content')}>
      {copiedLabel}
      <span className={element('decorator')}>
        <Icon path={check} />
      </span>
    </span>
  ) : (
    <span>{copyLabel}</span>
  );

export default ButtonContent;
