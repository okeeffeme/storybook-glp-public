import ButtonContent from './ButtonContent';
import React from 'react';

type TextLinkCopyProps = {
  element: (el: string, mod?: string) => string;
  isCopied: boolean;
  labels: {
    copy: string;
    copied: string;
  };
  onClick: (e) => void;
  className: string;
};

const ButtonCopy = ({
  isCopied,
  element,
  labels,
  onClick,
  className,
}: TextLinkCopyProps) => (
  <button className={className} data-copy-to-clipboard-button onClick={onClick}>
    <ButtonContent isCopied={isCopied} labels={labels} element={element} />
  </button>
);

export default ButtonCopy;
