export enum Appearances {
  default = 'default',
  monochrome = 'monochrome',
  icon = 'icon',
  iconSmall = 'iconSmall',
}
