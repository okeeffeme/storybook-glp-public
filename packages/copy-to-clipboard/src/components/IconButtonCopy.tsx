import React from 'react';

import Icon, {contentCopy, copied} from '@jotunheim/react-icons';
import {IconButton, IconButtonProps} from '@jotunheim/react-button';
import Tooltip from '@jotunheim/react-tooltip';

type IconButtonCopyProps = {
  isCopied: boolean;
  labels: {
    copy: string;
    copied: string;
  };
  onClick: (e) => void;
  size?: IconButtonProps['size'];
};

const IconButtonCopy = ({
  isCopied,
  labels,
  onClick,
  size = IconButton.sizes.standard,
}: IconButtonCopyProps) => {
  return (
    <Tooltip content={isCopied ? labels.copied : labels.copy} placement="top">
      <IconButton data-copy-to-clipboard-button onClick={onClick} size={size}>
        <Icon path={isCopied ? copied : contentCopy} />
      </IconButton>
    </Tooltip>
  );
};

export default IconButtonCopy;
