import React from 'react';
import {render, screen} from '@testing-library/react';

import ButtonContent from '../../src/components/ButtonContent';

describe('Jotunheim React Copy To Clipboard :: Button Content ::', () => {
  const props = {
    element: (el) => `comd-copy-to-clipboard__${el}`,
    labels: {
      copy: 'Copy that',
      copied: 'Copied that',
    },
  };

  it('should show copy label and no icon when value not yet copied', () => {
    render(<ButtonContent {...{...props, isCopied: false}} />);

    expect(screen.getByText(/copy that/i)).toBeInTheDocument();
    expect(screen.queryAllByTestId('icon').length).toBe(0);
  });

  it('should show copied label along with icon when value copied', () => {
    render(<ButtonContent {...{...props, isCopied: true}} />);

    expect(screen.getByText(/copied that/i)).toBeInTheDocument();
    expect(screen.queryAllByTestId('icon').length).toBe(1);
  });
});
