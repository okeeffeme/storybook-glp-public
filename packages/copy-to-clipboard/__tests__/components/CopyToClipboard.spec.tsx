import React from 'react';
import {render, screen} from '@testing-library/react';

import {Appearances} from '../../src';
import CopyToClipboard from '../../src/components/CopyToClipboard';

describe('Jotunheim React Copy To Clipboard :: ', () => {
  it('should show value to copy by default', () => {
    render(<CopyToClipboard value="value to copy" />);

    expect(screen.getAllByText(/value to copy/i).length).toBe(1);
  });

  it('should hide value to copy when explicitly specified', () => {
    render(
      <CopyToClipboard
        data-testid="copy-to-clipboard"
        value="value to copy"
        showValue={false}
      />
    );

    expect(
      screen
        .getByTestId('copy-to-clipboard')
        .querySelectorAll('.comd-copy-to-clipboard__value').length
    ).toBe(0);
  });

  it('renders IconButton and no value when `icon` appearance is specified ', () => {
    render(
      <CopyToClipboard
        data-testid="copy-to-clipboard"
        value="value to copy"
        appearance={Appearances.icon}
      />
    );

    expect(
      screen
        .getByTestId('copy-to-clipboard')
        .querySelectorAll('.comd-copy-to-clipboard__value').length
    ).toBe(0);
  });

  it('renders IconButton with small size and no value when `iconSmall` appearance is specified ', () => {
    render(
      <CopyToClipboard
        data-testid="copy-to-clipboard"
        value="value to copy"
        appearance={Appearances.iconSmall}
      />
    );

    expect(
      screen
        .getByTestId('copy-to-clipboard')
        .querySelectorAll('.comd-copy-to-clipboard__value').length
    ).toBe(0);
  });
});
