# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [11.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.44&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.45&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.44&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.44&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.42&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.43&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.41&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.42&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.40&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.41&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.39&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.40&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.38&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.39&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.37&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.38&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [11.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.36&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.37&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.35&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.36&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.34&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.35&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.33&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.34&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.32&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.33&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.31&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.32&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.30&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.31&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.29&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.30&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.27&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.27&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.28&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.26&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.27&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.25&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.26&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.24&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.25&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.23&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.24&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.21&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.23&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.21&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.18&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.18&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.18&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.17&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.16&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.14&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.13&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.14&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of IconButton from CopyToClipboard ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([6f77e32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6f77e32586fadde0237676fea7c2ab1a36f2e6eb))

## [11.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.12&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.11&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.10&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.9&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.8&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.6&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.3&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.2&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.1&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

## [11.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.0&sourceBranch=refs/tags/@jotunheim/react-copy-to-clipboard@11.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-copy-to-clipboard

# 11.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [10.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.38&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.39&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.37&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.38&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.36&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.37&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [10.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.35&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.36&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.34&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.35&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.32&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.33&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.31&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.32&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.30&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.31&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.29&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.30&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.28&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.29&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.27&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.28&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.24&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.25&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.23&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.24&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.22&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.23&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.21&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.22&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.20&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.21&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.19&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.20&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.18&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.19&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [10.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.17&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.18&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.15&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.16&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.14&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.15&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.13&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.14&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.12&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.13&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.11&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.12&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.10&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.11&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.9&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.10&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.8&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.9&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.7&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.8&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.6&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.7&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.5&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.6&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.4&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.5&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.3&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.4&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.2&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.3&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.1&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.2&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.0&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.1&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.39&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.1.0&targetRepoId=1246) (2021-06-01)

### Bug Fixes

- stop copy-to-clipboard copy button from shrinking, so it wont get cropped when container is too small ([NPM-794](https://jiraosl.firmglobal.com/browse/NPM-794)) ([6e93894](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6e93894998f49de2b4eb2b1d33d51d39c9ee01c6))

### Features

- add iconSmall appearance to copy-to-clipboard ([NPM-794](https://jiraosl.firmglobal.com/browse/NPM-794)) ([95ad431](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/95ad431a1f38fc18cd95fdae7da429f5bbfbd84b))

## [10.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.38&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.39&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.37&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.38&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.36&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.37&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.35&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.36&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.34&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.35&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.33&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.34&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.31&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.32&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.30&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.31&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.29&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.30&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [10.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.28&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.29&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.27&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.28&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.26&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.27&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.25&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.26&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.24&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.25&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.23&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.22&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.23&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.20&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.21&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.19&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.20&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.18&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.19&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.17&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.18&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.16&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.15&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.14&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.13&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.12&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.11&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.10&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.7&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.4&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.3&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.2&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.40&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@10.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [9.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.39&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.40&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.38&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.39&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.37&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.38&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.34&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.35&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.33&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.34&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.32&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.33&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.29&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.30&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.27&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.28&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.25&sourceBranch=refs/tags/@confirmit/react-copy-to-clipboard@9.0.26&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-copy-to-clipboard@9.0.23...@confirmit/react-copy-to-clipboard@9.0.24) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-copy-to-clipboard@9.0.21...@confirmit/react-copy-to-clipboard@9.0.22) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-copy-to-clipboard@9.0.16...@confirmit/react-copy-to-clipboard@9.0.17) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## [9.0.16](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-copy-to-clipboard@9.0.15...@confirmit/react-copy-to-clipboard@9.0.16) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-copy-to-clipboard

## CHANGELOG

### v9.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-copy-to-clipboard`

### v8.0.0

- **BREAKING**
  - Remove default theme
  - Replace `buttonAppearance` prop with `appearance` prop
  - The list of suggested appearances changed
- Feat:
  - Icon button view has been added

### v7.1.0

- Removing package version in class names.

### v7.0.0

- **BREAKING**
  - Rewrite to hooks and useTheme
  - Removed props
    - `buttonType`. Replaced with `buttonAppearance`
    - `baseClassName`
    - `classNames`
    - `buttonClassName`
    - `valueClassName`
  - Use same icon for both themes
  - Change default appearance from `primaryInvert` to `primaryNeutral`
- Feat:
  - Support for Button's `appearanece`, via `buttonAppearance`
  - Add data-attributes for test purposes
- Fix:
  - Set max-width to 100%, and truncate text with ellipsis when overflowing

### v6.0.0

- **BREAKING**
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v5.0.0

- BREAKING: Remove size.extraSmall - may cause issues on default-theme

### v4.0.0

- **BREAKING**: Remove `checkIcon` prop, and check icon

### v3.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v2.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v1.1.0

- Adds support for changing button type, adds styling for material design

### v1.0.0

- Initial version
