import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {boolean, text} from '@storybook/addon-knobs';
import Icon, {account, alert, alarm} from '../../icons/src/index';
import Chip from '../src/components/Chip';

storiesOf('Components/chip', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Chip', () => (
    <div style={{margin: '40px'}}>
      <Chip
        onClick={
          boolean('With onClick', true)
            ? () => window.alert('onClick')
            : undefined
        }
        icon={boolean('With icon', true) ? <Icon path={account} /> : null}
        error={boolean('Error', false)}
        errorText={text('Error Text', 'Error description')}
        disabled={boolean('Disabled', false)}
        readOnly={boolean('ReadOnly', false)}
        showDeleteButton={boolean('Display delete button', true)}
        deleteButtonTooltip={text('Delete button tooltip', 'Clear')}
        isActive={boolean('Active', true)}
        onDelete={() => window.alert('Delete button clicked')}
        actions={[
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: () => window.alert('Alert button clicked'),
          },
          {
            icon: <Icon path={alarm} />,
            tooltip: 'This is alarm icon',
            handler: () => window.alert('Alarm button clicked'),
          },
        ]}
        isDragged={boolean('Is dragged', false)}>
        {text('Children text', 'Children text')}
      </Chip>
    </div>
  ));
