import React, {MouseEventHandler, ReactNode} from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton, IconButtonAppearance} from '@jotunheim/react-button';

import classNames from './Chip.module.css';

const {element} = bemFactory('comd-chip', classNames);

type ActionButtonProps = {
  onClick: MouseEventHandler;
  tooltip?: ReactNode;
  children: ReactNode;
  appearance: IconButtonAppearance;
};

export const ActionButton = ({
  onClick,
  tooltip,
  children,
  appearance,
  ...rest
}: ActionButtonProps) => {
  const onActionClick = (event) => {
    onClick(event);
    event.stopPropagation();
  };

  return (
    <Tooltip content={tooltip}>
      <div className={element('action-button')}>
        <IconButton
          appearance={appearance}
          onClick={onActionClick}
          size="small"
          {...extractDataAndAriaProps(rest)}>
          {children}
        </IconButton>
      </div>
    </Tooltip>
  );
};

export default ActionButton;
