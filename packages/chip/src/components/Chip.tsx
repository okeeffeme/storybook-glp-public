import React, {MouseEventHandler, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import Icon, {closeCircle} from '@jotunheim/react-icons';
import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton} from '@jotunheim/react-button';

import ActionButton from './ActionButton';

import classNames from './Chip.module.css';

const {element, block, modifier} = bemFactory('comd-chip', classNames);

const noOp = () => {};

type Action = {
  icon: ReactNode;
  handler: () => void;
  tooltip?: string;
};

export type ChipProps = {
  onClick?: MouseEventHandler;
  icon?: ReactNode;
  children?: ReactNode;
  error?: boolean;
  errorText?: string;
  disabled?: boolean;
  showDeleteButton?: boolean;
  className?: string;
  onDelete?: MouseEventHandler;
  actions?: Action[];
  isDragged?: boolean;
  readOnly?: boolean;
  isActive?: boolean;
  deleteButtonTooltip?: ReactNode;
};

export const Chip = ({
  icon,
  onClick,
  children,
  disabled,
  isActive,
  readOnly = false,
  error,
  errorText,
  showDeleteButton,
  onDelete = noOp,
  actions = [],
  isDragged,
  deleteButtonTooltip,
  ...rest
}: ChipProps) => {
  const clickable = !!onClick && !error && !disabled && !readOnly;
  const isDeletable = showDeleteButton && !disabled && !readOnly;
  const hasActions = !!actions.length;
  const actionButtonAppearance =
    isActive || error
      ? IconButton.appearances.banner
      : IconButton.appearances.default;

  const chip = (
    <div className={block()}>
      <div
        tabIndex={disabled || error ? -1 : 1}
        data-testid="chip"
        className={cn(element('chip'), {
          [modifier('clickable')]: clickable,
          [modifier('disabled')]: disabled && !readOnly,
          [modifier('error')]: error,
          [modifier('is-dragged')]: isDragged,
          [modifier('activated')]: !error && !disabled && isActive,
        })}
        role={clickable ? 'button' : undefined}
        onClick={clickable ? onClick : undefined}
        {...extractDataAndAriaProps(rest)}>
        {icon && (
          <div className={element('icon')} data-chip-icon="">
            {icon}
          </div>
        )}
        <span
          className={element('label')}
          data-chip-label=""
          data-testid="span">
          {children}
        </span>

        {(isDeletable || hasActions) && (
          <div className={element('actions')} tabIndex={-1}>
            {hasActions && !error && !disabled && !readOnly && (
              <div className={element('hover-actions-wrapper')}>
                <div
                  data-testid="hover-actions"
                  className={cn(element('hover-actions'), {
                    [element('hover-actions', 'activated')]:
                      !disabled && isActive,
                  })}>
                  {actions.map((action, index) => {
                    return (
                      <ActionButton
                        data-testid="action-button"
                        key={index}
                        appearance={actionButtonAppearance}
                        data-test="action-button"
                        data-appearance={actionButtonAppearance}
                        data-input-chip-action-button=""
                        onClick={action.handler}
                        tooltip={action.tooltip}>
                        {action.icon}
                      </ActionButton>
                    );
                  })}
                </div>
              </div>
            )}
            {isDeletable && (
              <ActionButton
                appearance={actionButtonAppearance}
                data-appearance={actionButtonAppearance}
                data-testid="input-chip-delete-button"
                data-input-chip-delete-button=""
                onClick={onDelete}
                tooltip={deleteButtonTooltip}>
                <Icon path={closeCircle} />
              </ActionButton>
            )}
          </div>
        )}
        {clickable && <div className={element('ripple')} />}
      </div>
    </div>
  );

  if (error && errorText) {
    return (
      <Tooltip
        content={errorText}
        open
        onToggle={noOp}
        type={Tooltip.Types.error}>
        {chip}
      </Tooltip>
    );
  }

  return chip;
};

export default Chip;
