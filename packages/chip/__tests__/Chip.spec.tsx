import React, {ReactNode} from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Chip, {ChipProps} from '../src/components/Chip';
import Icon, {alert, alarm, clockOutline} from '../../icons/src';
import {IconButton} from '../../button/src';

const Render = (props: ChipProps, text?: ReactNode) => {
  return render(<Chip {...props}>{text}</Chip>);
};

const CLICK_MODIFIER = 'comd-chip--clickable';

describe('Jotunheim React Input Chip :: ', () => {
  it('should call onClick handler when chip is clicked', () => {
    const callback = jest.fn();
    const props = {
      onClick: callback,
    };
    Render(props);
    userEvent.click(screen.getByRole('button'));
    expect(callback).toHaveBeenCalled();
  });

  it('should have --clickable modifier if onClick is defined', () => {
    const props = {
      onClick: () => {},
    };
    Render(props);
    expect(screen.getByRole('button')).toHaveClass(CLICK_MODIFIER);
  });

  it('should not have --clickable modifier if onClick is not defined', () => {
    const props = {};
    Render(props);
    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should call onDelete handler when delete button is clicked', () => {
    const callback = jest.fn();
    const props = {
      showDeleteButton: true,
      onDelete: callback,
    };

    Render(props);
    userEvent.click(screen.getByRole('button'));
    expect(props.onDelete).toHaveBeenCalled();
  });

  it('should not render delete button when showDeleteButton is false', () => {
    const callback = jest.fn();
    const props = {
      showDeleteButton: false,
      onDelete: callback,
    };
    Render(props);
    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should not render delete button when disabled is true', () => {
    const callback = jest.fn();
    const props = {
      disabled: true,
      showDeleteButton: true,
      onDelete: callback,
    };

    Render(props);

    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should not render delete button when readOnly is true', () => {
    const callback = jest.fn();
    const props = {
      readOnly: true,
      showDeleteButton: true,
      onDelete: callback,
    };

    Render(props);

    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should render text properly', () => {
    const text = 'Test Text';
    Render({}, text);
    expect(screen.getByTestId('span')).toHaveTextContent(text);
  });

  it('should render icon properly', () => {
    const props = {
      icon: <Icon path={clockOutline} />,
    };

    Render(props);
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('should have --is-dragged modifier when isDragged prop is true', () => {
    const props = {
      isDragged: true,
    };

    Render(props);

    expect(screen.getByTestId('chip')).toHaveClass('comd-chip--is-dragged');
  });

  it('should not have --is-dragged modifier when isDragged prop is false', () => {
    const props = {
      isDragged: false,
    };

    Render(props);

    expect(screen.queryByTestId('chip')).not.toHaveClass(
      'comd-chip--is-dragged'
    );
  });

  describe('Hover actions :: ', () => {
    it('should render hover-actions when actions are passed', () => {
      const props = {
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
          {
            icon: <Icon path={alarm} />,
            tooltip: 'This is alarm icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);

      expect(screen.getByTestId('hover-actions')).toHaveClass(
        'comd-chip__hover-actions'
      );
    });

    it('should not render hover-actions when no actions are passed', () => {
      const props = {
        actions: [],
      };

      Render(props);
      expect(screen.queryByTestId('hover-actions')).not.toBeInTheDocument();
    });

    it('should not render hover-actions when error is true', () => {
      const props = {
        error: true,
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
          {
            icon: <Icon path={alarm} />,
            tooltip: 'This is alarm icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);

      expect(screen.queryByTestId('hover-actions')).not.toBeInTheDocument();
    });

    it('should not render hover-actions when disabled is true', () => {
      const props = {
        disabled: true,
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
          {
            icon: <Icon path={alarm} />,
            tooltip: 'This is alarm icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);

      expect(screen.queryByTestId('hover-actions')).not.toBeInTheDocument();
    });

    it('should not render hover-actions when readOnly is true', () => {
      const props = {
        readOnly: true,
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
          {
            icon: <Icon path={alarm} />,
            tooltip: 'This is alarm icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);

      expect(screen.queryByTestId('hover-actions')).not.toBeInTheDocument();
    });

    it('should set appearance on IconButton in ActionButton to default when isActive is false', () => {
      const props = {
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);
      expect(screen.getByRole('button')).toHaveClass(
        'comd-icon-button--' + IconButton.appearances.default
      );
    });

    it('should set appearance on IconButton in ActionButton to banner when isActive is true', () => {
      const props = {
        isActive: true,
        actions: [
          {
            icon: <Icon path={alert} />,
            tooltip: 'This is alert icon',
            handler: jest.fn(),
          },
        ],
      };

      Render(props);
      expect(screen.getByRole('button')).toHaveClass(
        'comd-icon-button--' + IconButton.appearances.banner
      );
    });
  });

  describe('Error state :: ', () => {
    it('should add --error modifier if error is true', () => {
      const props = {
        error: true,
      };

      Render(props);
      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--error');
    });

    it('should not add --error modifier if error is false', () => {
      const props = {
        error: false,
      };

      Render(props);
      expect(screen.queryByTestId('chip')).not.toHaveClass('comd-chip--error');
    });

    it('should be error and deletable', () => {
      const props = {
        error: true,
        showDeleteButton: true,
      };

      Render(props);
      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--error');
      expect(screen.getByRole('button')).toHaveAttribute(
        'data-input-chip-delete-button'
      );
    });

    it('should not show delete button when error and readOnly is true', () => {
      const props = {
        error: true,
        readOnly: true,
        showDeleteButton: true,
      };

      Render(props);

      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--error');
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should not show delete button when error and disabled is true', () => {
      const props = {
        error: true,
        disabled: true,
        showDeleteButton: true,
      };

      Render(props);

      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--error');
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should not have --activated modifier when error and isActive is true', () => {
      const props = {
        isActive: true,
        error: true,
      };

      Render(props);
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--error');
    });
  });

  describe('Active state :: ', () => {
    it('should show delete button when active and deletable is true', () => {
      const props = {
        isActive: true,
        showDeleteButton: true,
      };

      Render(props);

      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--activated');
      expect(screen.getByRole('button')).toHaveAttribute(
        'data-input-chip-delete-button'
      );
    });

    it('should be active when readOnly is true', () => {
      const props = {
        isActive: true,
        showDeleteButton: true,
        readOnly: true,
      };

      Render(props);

      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--activated');
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });
  });

  describe('Disabled state :: ', () => {
    it('should be disabled when disabled is true', () => {
      const props = {
        disabled: true,
      };

      Render(props);
      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--disabled');
    });

    it('should be disabled when isActive is true and disabled is true', () => {
      const props = {
        isActive: true,
        disabled: true,
      };

      Render(props);
      expect(screen.getByTestId('chip')).toHaveClass('comd-chip--disabled');
      expect(screen.queryByTestId('chip')).not.toHaveClass(
        'comd-chip--activated'
      );
    });

    it('should not be disabled disabled when readOnly is true', () => {
      const props = {
        disabled: true,
        readOnly: true,
      };

      Render(props);
      expect(screen.queryByTestId('chip')).not.toHaveClass(
        'comd-chip--disabled'
      );
    });
  });

  describe('Delete button :: ', () => {
    it('should set appearance on delete button to default when isActive is false and error is false', () => {
      const props = {
        error: false,
        isActive: false,
        showDeleteButton: true,
      };

      Render(props);
      expect(screen.getByRole('button')).toHaveClass(
        'comd-icon-button--' + IconButton.appearances.default
      );
    });

    it('should set appearance on delete button to banner when isActive is true', () => {
      const props = {
        isActive: true,
        showDeleteButton: true,
      };

      Render(props);
      expect(screen.getByRole('button')).toHaveClass(
        'comd-icon-button--' + IconButton.appearances.banner
      );
    });

    it('should set appearance on delete button to banner when error is true', () => {
      const props = {
        error: true,
        showDeleteButton: true,
      };

      Render(props);
      expect(screen.getByRole('button')).toHaveClass(
        'comd-icon-button--' + IconButton.appearances.banner
      );
    });
  });
});
