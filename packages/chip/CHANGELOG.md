# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.34&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.35&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.34&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.34&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.32&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.33&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.31&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.32&targetRepoId=1246) (2023-03-29)

### Bug Fixes

- add test ids in Table's components ([NPM-1228](https://jiraosl.firmglobal.com/browse/NPM-1228)) ([6415b6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6415b6ca5b2d547a2c8cf1611d7f7578b8329fe4))

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.30&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.31&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.29&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.30&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.28&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.29&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.27&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.28&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.26&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.27&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([c2c1e7c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c2c1e7c515a3fed3441c28127f07b26d7881a006))

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.25&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.26&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.24&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.25&targetRepoId=1246) (2023-02-27)

### Bug Fixes

- adding data-testid Chip component ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([b7fac62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b7fac62c55b610a8e613b80d5ebb57aa48e4462e))

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.23&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.24&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.22&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.23&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.21&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.22&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add missing prop to Chip component within chip package ([NPM-1235](https://jiraosl.firmglobal.com/browse/NPM-1235)) ([7ac3a38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7ac3a380cb2e47988ee597d3138245ac514e3e96))

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.20&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.21&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.19&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.20&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.18&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.19&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.17&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.18&targetRepoId=1246) (2023-01-23)

### Bug Fixes

- adding data-testid Chip component ([d879393](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d87939378686b3e750c3d6341692b585338b7bd1))
- small code fixes ([NPM-1179](https://jiraosl.firmglobal.com/browse/NPM-1179)) ([ed86764](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ed8676487ca38612dd6b82deab374ee4cbadb03f))

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.16&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.17&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.14&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.14&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.13&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.12&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.13&targetRepoId=1246) (2023-01-10)

### Bug Fixes

- add test id for Chip component within simple-select package ([NPM-1213](https://jiraosl.firmglobal.com/browse/NPM-1213)) ([bf63ff8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bf63ff8c9661df8cc1939fdc79445e2cea947e11))

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.11&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.10&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.9&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.7&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.7&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.4&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.4&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.4&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.3&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.2&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-chip

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@7.0.0&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- remove unused properties from IconButton ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([427f5a4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/427f5a4135cd91ce51c00371c7657f673e11da52))

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.13&sourceBranch=refs/tags/@jotunheim/react-chip@7.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Chip ([NPM-932](https://jiraosl.firmglobal.com/browse/NPM-932)) ([407e410](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/407e4109f303223fe4880a7275f9bc23b61fee71))
- remove default theme from Chip ([NPM-1068](https://jiraosl.firmglobal.com/browse/NPM-1068)) ([7f37485](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7f37485f68721b8711671eaa80641a948eef32d1))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.12&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.11&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.10&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.9&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.8&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.6&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.3&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.2&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.1&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-chip

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-chip@6.0.0&sourceBranch=refs/tags/@jotunheim/react-chip@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-chip

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.1.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.58&sourceBranch=refs/tags/@confirmit/react-chip@5.1.59&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.57&sourceBranch=refs/tags/@confirmit/react-chip@5.1.58&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.56&sourceBranch=refs/tags/@confirmit/react-chip@5.1.57&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.1.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.55&sourceBranch=refs/tags/@confirmit/react-chip@5.1.56&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.54&sourceBranch=refs/tags/@confirmit/react-chip@5.1.55&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.52&sourceBranch=refs/tags/@confirmit/react-chip@5.1.53&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.51&sourceBranch=refs/tags/@confirmit/react-chip@5.1.52&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.50&sourceBranch=refs/tags/@confirmit/react-chip@5.1.51&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.49&sourceBranch=refs/tags/@confirmit/react-chip@5.1.50&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.48&sourceBranch=refs/tags/@confirmit/react-chip@5.1.49&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.47&sourceBranch=refs/tags/@confirmit/react-chip@5.1.48&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.44&sourceBranch=refs/tags/@confirmit/react-chip@5.1.45&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.43&sourceBranch=refs/tags/@confirmit/react-chip@5.1.44&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.42&sourceBranch=refs/tags/@confirmit/react-chip@5.1.43&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.41&sourceBranch=refs/tags/@confirmit/react-chip@5.1.42&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.40&sourceBranch=refs/tags/@confirmit/react-chip@5.1.41&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.39&sourceBranch=refs/tags/@confirmit/react-chip@5.1.40&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.38&sourceBranch=refs/tags/@confirmit/react-chip@5.1.39&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.37&sourceBranch=refs/tags/@confirmit/react-chip@5.1.38&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.35&sourceBranch=refs/tags/@confirmit/react-chip@5.1.36&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.34&sourceBranch=refs/tags/@confirmit/react-chip@5.1.35&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.33&sourceBranch=refs/tags/@confirmit/react-chip@5.1.34&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.32&sourceBranch=refs/tags/@confirmit/react-chip@5.1.33&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.31&sourceBranch=refs/tags/@confirmit/react-chip@5.1.32&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.30&sourceBranch=refs/tags/@confirmit/react-chip@5.1.31&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.29&sourceBranch=refs/tags/@confirmit/react-chip@5.1.30&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.28&sourceBranch=refs/tags/@confirmit/react-chip@5.1.29&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.27&sourceBranch=refs/tags/@confirmit/react-chip@5.1.28&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.26&sourceBranch=refs/tags/@confirmit/react-chip@5.1.27&targetRepoId=1246) (2021-07-12)

### Bug Fixes

- remove hover effect on chip when onClick is not set ([NPM-818](https://jiraosl.firmglobal.com/browse/NPM-818)) ([105e152](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/105e152b48f152024f49694e83fabcaededeaf9d))
- set correct action button appearance when chip is in error state ([NPM-818](https://jiraosl.firmglobal.com/browse/NPM-818)) ([7a77041](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7a77041ba99cf4ac4263e4bdd6744ac02ef03d2d))

## [5.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.25&sourceBranch=refs/tags/@confirmit/react-chip@5.1.26&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.24&sourceBranch=refs/tags/@confirmit/react-chip@5.1.25&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.23&sourceBranch=refs/tags/@confirmit/react-chip@5.1.24&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.22&sourceBranch=refs/tags/@confirmit/react-chip@5.1.23&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.21&sourceBranch=refs/tags/@confirmit/react-chip@5.1.22&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.20&sourceBranch=refs/tags/@confirmit/react-chip@5.1.21&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.19&sourceBranch=refs/tags/@confirmit/react-chip@5.1.20&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.18&sourceBranch=refs/tags/@confirmit/react-chip@5.1.19&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.17&sourceBranch=refs/tags/@confirmit/react-chip@5.1.18&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.16&sourceBranch=refs/tags/@confirmit/react-chip@5.1.17&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.15&sourceBranch=refs/tags/@confirmit/react-chip@5.1.16&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.14&sourceBranch=refs/tags/@confirmit/react-chip@5.1.15&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.13&sourceBranch=refs/tags/@confirmit/react-chip@5.1.14&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.11&sourceBranch=refs/tags/@confirmit/react-chip@5.1.12&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.10&sourceBranch=refs/tags/@confirmit/react-chip@5.1.11&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.9&sourceBranch=refs/tags/@confirmit/react-chip@5.1.10&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.8&sourceBranch=refs/tags/@confirmit/react-chip@5.1.9&targetRepoId=1246) (2021-04-07)

### Bug Fixes

- should not be clickable when onClick handler is not defined ([NPM-731](https://jiraosl.firmglobal.com/browse/NPM-731)) ([fdcf922](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fdcf922870f4f57abda67e003214b48fa32f0379))

## [5.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.7&sourceBranch=refs/tags/@confirmit/react-chip@5.1.8&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.6&sourceBranch=refs/tags/@confirmit/react-chip@5.1.7&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.5&sourceBranch=refs/tags/@confirmit/react-chip@5.1.6&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.4&sourceBranch=refs/tags/@confirmit/react-chip@5.1.5&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.3&sourceBranch=refs/tags/@confirmit/react-chip@5.1.4&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.2&sourceBranch=refs/tags/@confirmit/react-chip@5.1.3&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-chip

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.1.1&sourceBranch=refs/tags/@confirmit/react-chip@5.1.2&targetRepoId=1246) (2021-02-15)

### Bug Fixes

- correct color on action buttons in non-active state, refactored to use IconButton ([NPM-705](https://jiraosl.firmglobal.com/browse/NPM-705)) ([b6fe7a1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b6fe7a15ffdb24d80a46c8dd0c403fd438cd1897))

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.17&sourceBranch=refs/tags/@confirmit/react-chip@5.1.0&targetRepoId=1246) (2021-02-01)

### Features

- Add isActivated, deleteButtonTooltip props ([NPM-352](https://jiraosl.firmglobal.com/browse/NPM-352)) ([5571393](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/55713937007fd52dd269062038fc9990f917b745))
- Change errorText placement ([NPM-352](https://jiraosl.firmglobal.com/browse/NPM-352)) ([0fee5a7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0fee5a7a33a744e83cfbce0129b572b570949e78))

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.16&sourceBranch=refs/tags/@confirmit/react-chip@5.0.17&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.15&sourceBranch=refs/tags/@confirmit/react-chip@5.0.16&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.14&sourceBranch=refs/tags/@confirmit/react-chip@5.0.15&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.13&sourceBranch=refs/tags/@confirmit/react-chip@5.0.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.12&sourceBranch=refs/tags/@confirmit/react-chip@5.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.11&sourceBranch=refs/tags/@confirmit/react-chip@5.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.10&sourceBranch=refs/tags/@confirmit/react-chip@5.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.9&sourceBranch=refs/tags/@confirmit/react-chip@5.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.6&sourceBranch=refs/tags/@confirmit/react-chip@5.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.3&sourceBranch=refs/tags/@confirmit/react-chip@5.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.2&sourceBranch=refs/tags/@confirmit/react-chip@5.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-chip

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@5.0.1&sourceBranch=refs/tags/@confirmit/react-chip@5.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-chip

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.20&sourceBranch=refs/tags/@confirmit/react-chip@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.19&sourceBranch=refs/tags/@confirmit/react-chip@4.2.20&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.18&sourceBranch=refs/tags/@confirmit/react-chip@4.2.19&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.15&sourceBranch=refs/tags/@confirmit/react-chip@4.2.16&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.14&sourceBranch=refs/tags/@confirmit/react-chip@4.2.15&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.13&sourceBranch=refs/tags/@confirmit/react-chip@4.2.14&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.10&sourceBranch=refs/tags/@confirmit/react-chip@4.2.11&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.8&sourceBranch=refs/tags/@confirmit/react-chip@4.2.9&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-chip@4.2.6&sourceBranch=refs/tags/@confirmit/react-chip@4.2.7&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.2.4...@confirmit/react-chip@4.2.5) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-chip

## [4.2.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.2.2...@confirmit/react-chip@4.2.3) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-chip

# [4.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.1.0...@confirmit/react-chip@4.2.0) (2020-08-21)

### Features

- add typescript support ([NPM-493](https://jiraosl.firmglobal.com/browse/NPM-493)) ([36605dc](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/36605dc4ff7870e3c90723845ce4fa38f2ee4433))
- add typescript support ([NPM-493](https://jiraosl.firmglobal.com/browse/NPM-493)) ([de945d3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/de945d34745db31439d8f828be84b9e6f2f43e3a))

# [4.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.0.16...@confirmit/react-chip@4.0.17) (2020-08-16)

### Features

- Added `readOnly` prop

## [4.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.0.16...@confirmit/react-chip@4.0.17) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-chip

## [4.0.16](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-chip@4.0.15...@confirmit/react-chip@4.0.16) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-chip

## CHANGELOG

### v4.0.3

- Make it possible to show ellipsis in chips

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Removing package version in class names.

### v3.0.9

- Fix: Update height to 28px to align with Design System spec, and to get a better fit when used inside `Select`

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.1.4

- Fix: Improve propType validation for "actions"

### v2.1.0

- Fix: hide delete button when `disabled` prop is true

### v2.0.0

- Added `errorText` prop
- Added `actions` prop
- Added ripple click effect
- Added `isDragging` prop
- **BREAKING**
  - InputChip was absorbed by Chip
  - To implement delete use `showDeleteButton` and `onDelete` props,
  - To turn error state on use `error` and `errorText` props,

### v1.0.0

- **BREAKING** - Chip: Removed props: baseClassName, classNames - InputChip: Removed props: baseClassName, classNames, deleteIcon

### v0.0.13

- Fix: use more color variables from global-styles-material package

### v0.0.1

- Initial version
