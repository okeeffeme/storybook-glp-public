# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [8.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.40&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.40&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.38&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.39&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.37&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.38&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.36&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.37&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.35&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.36&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.34&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.35&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.33&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.34&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- convert incorrect "alert-type" attribute to data attribute ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([1286b7b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1286b7ba6818951d4a2e6bee12acca1a209b133d))

## [8.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.32&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.33&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.31&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.32&targetRepoId=1246) (2023-02-21)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.30&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.31&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.29&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.30&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.28&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.29&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.27&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.28&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.26&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.27&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.24&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.26&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.24&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.25&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.23&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.24&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- add test id for Alert component within alert package([NPM-1216](https://jiraosl.firmglobal.com/browse/NPM-1216)) ([1c426e0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1c426e0776c5dc8185ebdcfc86f74fb9e6a6323f))

## [8.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.22&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.23&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.21&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.22&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.19&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.21&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.19&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.20&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.16&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.19&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.16&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.18&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.16&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.17&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.15&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.16&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.14&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.15&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.12&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.13&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.11&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.12&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.10&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.9&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.8&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.6&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.3&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.2&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.1&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.0&sourceBranch=refs/tags/@jotunheim/react-alert@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-alert

## [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@7.0.1&sourceBranch=refs/tags/@jotunheim/react-alert@7.0.2&targetRepoId=1246) (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@7.0.0&sourceBranch=refs/tags/@jotunheim/react-alert@7.0.1&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @jotunheim/react-alert

# 7.0.0 (2022-06-27)

### Features

- switch to "jotunheim" scope ([NPM-1029](https://jiraosl.firmglobal.com/browse/NPM-1029)) ([f029391](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f029391021d40003e7bee146a728131b8225e01e))
- Convert Alert to TypeScript ([NPM-1029](https://jiraosl.firmglobal.com/browse/NPM-1029)) ([c22b6f9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c22b6f94e917d03c027b49c3f5058f1491a9e8cc))
- update browser support ([NPM-1029](https://jiraosl.firmglobal.com/browse/NPM-1029)) ([a2c0027](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2c0027e1eb1a858769fba3fe91d75804a9bb8f7))

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.21&sourceBranch=refs/tags/@confirmit/react-alert@6.0.22&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.20&sourceBranch=refs/tags/@confirmit/react-alert@6.0.21&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.19&sourceBranch=refs/tags/@confirmit/react-alert@6.0.20&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.18&sourceBranch=refs/tags/@confirmit/react-alert@6.0.19&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.16&sourceBranch=refs/tags/@confirmit/react-alert@6.0.17&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.15&sourceBranch=refs/tags/@confirmit/react-alert@6.0.16&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.14&sourceBranch=refs/tags/@confirmit/react-alert@6.0.15&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.13&sourceBranch=refs/tags/@confirmit/react-alert@6.0.14&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.12&sourceBranch=refs/tags/@confirmit/react-alert@6.0.13&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.11&sourceBranch=refs/tags/@confirmit/react-alert@6.0.12&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.8&sourceBranch=refs/tags/@confirmit/react-alert@6.0.9&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.7&sourceBranch=refs/tags/@confirmit/react-alert@6.0.8&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.6&sourceBranch=refs/tags/@confirmit/react-alert@6.0.7&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.5&sourceBranch=refs/tags/@confirmit/react-alert@6.0.6&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.4&sourceBranch=refs/tags/@confirmit/react-alert@6.0.5&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.3&sourceBranch=refs/tags/@confirmit/react-alert@6.0.4&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-alert

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.2&sourceBranch=refs/tags/@confirmit/react-alert@6.0.3&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@6.0.1&sourceBranch=refs/tags/@confirmit/react-alert@6.0.2&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-alert

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.25&sourceBranch=refs/tags/@confirmit/react-alert@6.0.0&targetRepoId=1246) (2021-10-12)

### Bug Fixes

- remove limit of text length in Alert ([NPM-884](https://jiraosl.firmglobal.com/browse/NPM-884)) ([ce96d77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ce96d779696dead7f297064072b7b66cdb538d8d))

### BREAKING CHANGES

- remove limit of text length in Alert (NPM-884)

## [5.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.24&sourceBranch=refs/tags/@confirmit/react-alert@5.1.25&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.23&sourceBranch=refs/tags/@confirmit/react-alert@5.1.24&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.22&sourceBranch=refs/tags/@confirmit/react-alert@5.1.23&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.21&sourceBranch=refs/tags/@confirmit/react-alert@5.1.22&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.20&sourceBranch=refs/tags/@confirmit/react-alert@5.1.21&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.19&sourceBranch=refs/tags/@confirmit/react-alert@5.1.20&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.18&sourceBranch=refs/tags/@confirmit/react-alert@5.1.19&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.17&sourceBranch=refs/tags/@confirmit/react-alert@5.1.18&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.16&sourceBranch=refs/tags/@confirmit/react-alert@5.1.17&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.15&sourceBranch=refs/tags/@confirmit/react-alert@5.1.16&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.14&sourceBranch=refs/tags/@confirmit/react-alert@5.1.15&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.13&sourceBranch=refs/tags/@confirmit/react-alert@5.1.14&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.12&sourceBranch=refs/tags/@confirmit/react-alert@5.1.13&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.11&sourceBranch=refs/tags/@confirmit/react-alert@5.1.12&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.10&sourceBranch=refs/tags/@confirmit/react-alert@5.1.11&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.9&sourceBranch=refs/tags/@confirmit/react-alert@5.1.10&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.8&sourceBranch=refs/tags/@confirmit/react-alert@5.1.9&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.7&sourceBranch=refs/tags/@confirmit/react-alert@5.1.8&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.6&sourceBranch=refs/tags/@confirmit/react-alert@5.1.7&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.5&sourceBranch=refs/tags/@confirmit/react-alert@5.1.6&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.3&sourceBranch=refs/tags/@confirmit/react-alert@5.1.4&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.2&sourceBranch=refs/tags/@confirmit/react-alert@5.1.3&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-alert

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.1&sourceBranch=refs/tags/@confirmit/react-alert@5.1.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.1.0&sourceBranch=refs/tags/@confirmit/react-alert@5.1.1&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-alert

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.26&sourceBranch=refs/tags/@confirmit/react-alert@5.1.0&targetRepoId=1246) (2021-03-17)

### Bug Fixes

- update colors in Alert to match Design System spec, and refactor to use Link component instead of custom styles ([NPM-744](https://jiraosl.firmglobal.com/browse/NPM-744)) ([a9b0757](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a9b0757dfc3f5823f96f239e2d4bfd88959c81dc))

### Features

- introduce actionsChildren prop to render things like Links and Buttons on the right side of Alerts ([NPM-744](https://jiraosl.firmglobal.com/browse/NPM-744)) ([fc43396](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fc43396b844f24dd0b69e5a446a363d459afa431))

## [5.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.25&sourceBranch=refs/tags/@confirmit/react-alert@5.0.26&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.24&sourceBranch=refs/tags/@confirmit/react-alert@5.0.25&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.23&sourceBranch=refs/tags/@confirmit/react-alert@5.0.24&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.22&sourceBranch=refs/tags/@confirmit/react-alert@5.0.23&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.21&sourceBranch=refs/tags/@confirmit/react-alert@5.0.22&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.20&sourceBranch=refs/tags/@confirmit/react-alert@5.0.21&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.19&sourceBranch=refs/tags/@confirmit/react-alert@5.0.20&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.18&sourceBranch=refs/tags/@confirmit/react-alert@5.0.19&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.17&sourceBranch=refs/tags/@confirmit/react-alert@5.0.18&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.16&sourceBranch=refs/tags/@confirmit/react-alert@5.0.17&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.15&sourceBranch=refs/tags/@confirmit/react-alert@5.0.16&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.14&sourceBranch=refs/tags/@confirmit/react-alert@5.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.13&sourceBranch=refs/tags/@confirmit/react-alert@5.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.12&sourceBranch=refs/tags/@confirmit/react-alert@5.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.11&sourceBranch=refs/tags/@confirmit/react-alert@5.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.10&sourceBranch=refs/tags/@confirmit/react-alert@5.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.9&sourceBranch=refs/tags/@confirmit/react-alert@5.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.7&sourceBranch=refs/tags/@confirmit/react-alert@5.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.4&sourceBranch=refs/tags/@confirmit/react-alert@5.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.3&sourceBranch=refs/tags/@confirmit/react-alert@5.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-alert

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@5.0.2&sourceBranch=refs/tags/@confirmit/react-alert@5.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-alert

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.35&sourceBranch=refs/tags/@confirmit/react-alert@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([70c6fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70c6fc66e591517275e65be695c787ec215a9841))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

## [4.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.34&sourceBranch=refs/tags/@confirmit/react-alert@4.0.35&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.33&sourceBranch=refs/tags/@confirmit/react-alert@4.0.34&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.30&sourceBranch=refs/tags/@confirmit/react-alert@4.0.31&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.29&sourceBranch=refs/tags/@confirmit/react-alert@4.0.30&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.28&sourceBranch=refs/tags/@confirmit/react-alert@4.0.29&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.25&sourceBranch=refs/tags/@confirmit/react-alert@4.0.26&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-alert@4.0.23&sourceBranch=refs/tags/@confirmit/react-alert@4.0.24&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.20](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-alert@4.0.19...@confirmit/react-alert@4.0.20) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.15](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-alert@4.0.14...@confirmit/react-alert@4.0.15) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-alert

## [4.0.14](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-alert@4.0.13...@confirmit/react-alert@4.0.14) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-alert

## CHANGELOG

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Removing package version in class names.

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.6

- Fix: Fix styles, Design System specs were misunderstood in previous fix

### v2.0.0

- ** BREAKING**
  - Removed `icon` prop, all icons are now set internally based on the type of alert.
  - `type` now defaults to `info`
  - `showIcon` now defaults to `true`
  - add `align-items: center` to default theme
  - max width set to 600px
  - Use same icons and colors for both default and material theme
- Feat: new props
  - `showDismissButton`: controls wheter the dismiss button should be shown, default is `false`
  - `onDismiss`: callback for when the dismiss button is clicked
  - `href`: href to link to be shown on right edge of alert
  - `hrefText`: link text
- Refactor: Use new icons package

### v1.0.4

- Fix: Adjust colors for Default theme

### v1.0.0

- **BREAKING**
  - Removed props:
    - `baseClassName`
    - `classNames`
    - `infoIcon`
    - `successIcon`
    - `errorIcon`
    - `warningIcon`

### v0.1.2

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.1.1

- **BREAKING**
  - Introduce default icon for the alert. Icon is being wrapped with an extra div
- Feat:
  - Material theme was added
  - New prop `showIcon` to turn on/off the icon. Being set to `true` will show either icon from `icon` prop or default icon from theme.

### v0.0.1

- Initial version
