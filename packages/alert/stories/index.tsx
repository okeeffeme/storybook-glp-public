import React from 'react';
import {storiesOf} from '@storybook/react';
import {radios, boolean, text, object} from '@storybook/addon-knobs';

import {Link, LinkAppearances} from '../../link/src';
import Button from '../../button/src';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Alert from '../src';

storiesOf('Components/alert', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })

  .add('Default', () => {
    return (
      <div style={{margin: '20px'}}>
        <h1>With knobs</h1>
        <div style={{margin: '20px'}}>
          <Alert
            type={radios('type', Alert.types, Alert.types.info)}
            showIcon={boolean('showIcon', true)}
            actionsChildren={object('actionsChildren', null)}
            showDismissButton={boolean('showDismissButton', true)}>
            {text('children', 'This is an alert!')}
          </Alert>
        </div>

        <h1>All alerts</h1>
        {Object.keys(Alert.types).map((alertType) => {
          return (
            <div style={{margin: '20px'}} key={alertType}>
              <Alert
                type={Alert.types[alertType]}
                showIcon={true}
                showDismissButton={true}>
                Alert text
              </Alert>
            </div>
          );
        })}
      </div>
    );
  })
  .add('With link as actionsChildren', () => (
    <div style={{margin: '20px'}}>
      <Alert
        type={radios('type', Alert.types, Alert.types.info)}
        showIcon={true}
        actionsChildren={
          <Link appearance={LinkAppearances.monochrome} href="href">
            Link
          </Link>
        }
        showDismissButton={true}>
        {text('children', 'This is an alert!')}
      </Alert>
    </div>
  ))
  .add('With Button as actionsChildren', () => (
    <div style={{margin: '20px'}}>
      <Alert
        type={radios('type', Alert.types, Alert.types.info)}
        showIcon={true}
        actionsChildren={<Button>Button</Button>}
        showDismissButton={true}>
        {text('children', 'This is an alert!')}
      </Alert>
    </div>
  ));
