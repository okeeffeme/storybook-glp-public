import {Alert} from './components/Alert';
import {AlertType} from './types';

export {Alert, AlertType};

export default Alert;
