export enum AlertType {
  warning = 'warning',
  info = 'info',
  danger = 'danger',
  success = 'success',
}
