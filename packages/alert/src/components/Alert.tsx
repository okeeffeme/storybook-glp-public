import React, {MouseEventHandler, ReactNode} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {IconButton} from '@jotunheim/react-button';

import Icon, {close} from '@jotunheim/react-icons';

import {AlertType} from '../types';
import {getIconByType} from '../getIconByType';

import classNames from './Alert.module.css';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-alert',
  classNames,
});

const noop = () => {};

interface Props {
  children?: ReactNode;
  type?: AlertType;
  className?: string;
  showIcon?: boolean;
  onDismiss?: MouseEventHandler;
  showDismissButton?: boolean;
  actionsChildren?: ReactNode | null;
}

export const Alert = ({
  className,
  children,
  type = AlertType.info,
  showIcon = true,
  onDismiss = noop,
  showDismissButton = false,
  actionsChildren,
  ...rest
}: Props) => {
  const classes = cn(block(), className, modifier(type));

  return (
    <div
      data-testid="alert"
      data-alert-type={type}
      className={classes}
      {...extractDataAndAriaProps(rest)}>
      {showIcon && <div className={element('icon')}>{getIconByType(type)}</div>}
      <div className={element('text')} data-testid="alert-text-container">
        {children}
      </div>
      <div
        className={element('actions-wrapper')}
        data-testid="alert-actions-container">
        {actionsChildren}
        {showDismissButton && (
          <div className={element('dismiss-button')}>
            <IconButton data-test="alert-dismiss-button" onClick={onDismiss}>
              <Icon path={close} />
            </IconButton>
          </div>
        )}
      </div>
    </div>
  );
};

Alert.types = AlertType;

export default Alert;
