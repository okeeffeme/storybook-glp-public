import React from 'react';
import {colors} from '@jotunheim/global-styles';
import Icon, {
  alert,
  checkCircle,
  criticalAlert,
  informationOutline,
} from '@jotunheim/react-icons';

import {AlertType} from './types';

const assertUnreachable = (val: never) => {
  throw new Error(
    `Did not expect to get here: Did you forget to handle the value ${val}?`
  );
};

export const getIconByType = (type: AlertType) => {
  switch (type) {
    case AlertType.info:
      return (
        <Icon
          data-testid="alert-icon"
          fill={colors.PrimaryAccent}
          path={informationOutline}
        />
      );
    case AlertType.warning:
      return (
        <Icon data-testid="alert-icon" fill={colors.Warning} path={alert} />
      );
    case AlertType.danger:
      return (
        <Icon
          data-testid="alert-icon"
          fill={colors.Danger}
          path={criticalAlert}
        />
      );
    case AlertType.success:
      return (
        <Icon data-testid="alert-icon" fill={colors.Safe} path={checkCircle} />
      );
    default:
      assertUnreachable(type);
  }
};
