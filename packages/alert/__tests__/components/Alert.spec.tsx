import React from 'react';
import {render} from '@testing-library/react';

import {Alert} from '../../src';

import {
  alert,
  checkCircle,
  criticalAlert,
  informationOutline,
} from '../../../icons/src';

describe('Alert', () => {
  it('should render with no icon', () => {
    const wrapper = render(<Alert showIcon={false}>No icon</Alert>);
    expect(wrapper.queryByTestId('alert-icon')).not.toBeInTheDocument();
  });

  it('should render with icon and default to info icon', () => {
    const wrapper = render(<Alert>Icon</Alert>);

    expect(wrapper.queryByTestId('alert-icon')).toBeInTheDocument();
    expect(wrapper.container.querySelector('path')?.getAttribute('d')).toEqual(
      informationOutline
    );
  });

  it('should render with icon and info type', () => {
    const wrapper = render(<Alert type={Alert.types.info}>Icon</Alert>);

    expect(wrapper.queryByTestId('alert-icon')).toBeInTheDocument();
    expect(wrapper.container.querySelector('path')?.getAttribute('d')).toEqual(
      informationOutline
    );
  });

  it('should render with icon and warning type', () => {
    const wrapper = render(<Alert type={Alert.types.warning}>Icon</Alert>);

    expect(wrapper.queryByTestId('alert-icon')).toBeInTheDocument();
    expect(wrapper.container.querySelector('path')?.getAttribute('d')).toEqual(
      alert
    );
  });

  it('should render with icon and danger type', () => {
    const wrapper = render(<Alert type={Alert.types.danger}>Icon</Alert>);

    expect(wrapper.queryByTestId('alert-icon')).toBeInTheDocument();
    expect(wrapper.container.querySelector('path')?.getAttribute('d')).toEqual(
      criticalAlert
    );
  });

  it('should render with icon and success type', () => {
    const wrapper = render(<Alert type={Alert.types.success}>Icon</Alert>);

    expect(wrapper.queryByTestId('alert-icon')).toBeInTheDocument();
    expect(wrapper.container.querySelector('path')?.getAttribute('d')).toEqual(
      checkCircle
    );
  });

  it('should render actionsChildren', () => {
    const wrapper = render(
      <Alert actionsChildren={<div data-testid="action-child">test</div>}>
        Icon
      </Alert>
    );

    expect(wrapper.queryByTestId('action-child')).toBeInTheDocument();
  });
});
