import React from 'react';
import {storiesOf} from '@storybook/react';
import {number} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import MarkdownEditor from '../src';
import Dialog from '../../dialog/src';
import MarkdownView from '../../markdown-view/src';

const markdownExample = `Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

There must be at least 3 dashes separating each header cell.
The outer pipes (|) are optional, and you don't need to make the
raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | \`renders\` | **nicely**
1 | 2 | 3
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself].

URLs and URLs in angle brackets will automatically get turned into links.
http://www.example.com or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com`;

storiesOf('Components/markdown-editor', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Basic usage', () => {
    const [value, setValue] = React.useState(markdownExample);

    return (
      <div style={{margin: 16, height: '100%'}}>
        <MarkdownEditor
          onChange={setValue}
          rows={number('rows', 7)}
          value={value}
        />
        <h3>Preview:</h3>
        <MarkdownView markdown={value} />
      </div>
    );
  })
  .add('In a modal', () => {
    const [value, setValue] = React.useState(markdownExample);

    return (
      <Dialog open={true}>
        <Dialog.Body>
          <div style={{height: '90vh', width: '100%'}}>
            <MarkdownEditor
              autoFocus={true}
              onChange={setValue}
              value={value}
            />
          </div>
        </Dialog.Body>
      </Dialog>
    );
  });
