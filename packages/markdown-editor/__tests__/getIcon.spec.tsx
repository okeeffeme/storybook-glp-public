import React from 'react';
import {render, screen} from '@testing-library/react';
import {ExecuteOptions} from 'react-mde';
import {
  formatBold,
  formatCode,
  formatHeading,
  formatItalic,
  formatListBulletedSquare,
  formatListNumbered,
  formatQuote,
  formatStrikethrough,
} from '../../icons/src';
import getIcon from '../src/components/getIcon';
import defaultTexts from '../src/components/defaultTexts';

function IconWrapper({command, opts, texts}) {
  return getIcon(command, opts, texts);
}

describe('Jotunheim React Markdown Editor :: getIcon :: ', () => {
  const opts = {
    initialState: {
      text: '',
      selectedText: '',
      selection: {
        start: 0,
        end: 0,
      },
    },
    textApi: {
      replaceSelection: jest.fn(),
      setSelectionRange: jest.fn(),
      getState: jest.fn(),
    },
  } as ExecuteOptions;

  it('should return LinkPopover', () => {
    render(getIcon('insertLink', opts, defaultTexts));
    expect(screen.getByTestId('link-popover')).toBeInTheDocument();
  });

  it('should return ImagePopover', () => {
    render(getIcon('insertImage', opts, defaultTexts));
    expect(screen.getByTestId('image-popover')).toBeInTheDocument();
  });

  it('should return TablePopover', () => {
    render(getIcon('insertTable', opts, defaultTexts));
    expect(screen.getByTestId('table-popover')).toBeInTheDocument();
  });

  it('should return icon for header command', () => {
    render(<IconWrapper command={'header'} opts={opts} texts={defaultTexts} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatHeading
    );
  });

  it('should return icon for bold command', () => {
    render(<IconWrapper command={'bold'} opts={opts} texts={defaultTexts} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatBold
    );
  });

  it('should return icon for italic command', () => {
    render(<IconWrapper command={'italic'} opts={opts} texts={defaultTexts} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatItalic
    );
  });

  it('should return icon for strikethrough command', () => {
    render(
      <IconWrapper command={'strikethrough'} opts={opts} texts={defaultTexts} />
    );
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatStrikethrough
    );
  });

  it('should return icon for quote command', () => {
    render(<IconWrapper command={'quote'} opts={opts} texts={defaultTexts} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatQuote
    );
  });

  it('should return icon for code command', () => {
    render(<IconWrapper command={'code'} opts={opts} texts={defaultTexts} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatCode
    );
  });

  it('should return icon for unordered-list command', () => {
    render(
      <IconWrapper
        command={'unordered-list'}
        opts={opts}
        texts={defaultTexts}
      />
    );
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatListBulletedSquare
    );
  });

  it('should return icon for ordered-list command', () => {
    render(
      <IconWrapper command={'ordered-list'} opts={opts} texts={defaultTexts} />
    );
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      formatListNumbered
    );
  });
});
