import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {ExecuteOptions} from 'react-mde';
import {LinkPopoverContent} from '../src/components/LinkPopover';
import defaultTexts from '../src/components/defaultTexts';
import user from '@testing-library/user-event';

describe('Jotunheim React Markdown Editor :: LinkPopover :: ', () => {
  it('should render popover content', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 0,
          end: 0,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <LinkPopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    expect(screen.getAllByRole('textbox')).toHaveLength(2);
    expect(screen.getAllByRole('button')).toHaveLength(2);
  });

  it('should add link to markdown editor when clicking confirm', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 0,
          end: 0,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <LinkPopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    const imagePopoverInput = screen.getAllByTestId('simple-text-field');
    const description = imagePopoverInput[0];
    const source = imagePopoverInput[1];
    fireEvent.change(description, {target: {value: 'hello'}});
    fireEvent.change(source, {target: {value: 'https://world.com'}});

    user.click(screen.getByText('Add'));

    expect(opts.textApi.replaceSelection).toHaveBeenCalledWith(
      '[hello](https://world.com)'
    );
    expect(setIsOpen).toHaveBeenCalledWith(false);
  });

  it('should not add link to markdown editor when clicking cancel', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 0,
          end: 0,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <LinkPopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    const imagePopoverInput = screen.getAllByTestId('simple-text-field');
    const description = imagePopoverInput[0];
    const link = imagePopoverInput[1];
    fireEvent.change(description, {target: {value: 'hello'}});
    fireEvent.change(link, {target: {value: 'https://world.com'}});
    user.click(screen.getByText('Cancel'));

    expect(opts.textApi.replaceSelection).not.toHaveBeenCalled();
    expect(setIsOpen).toHaveBeenCalledWith(false);
  });
});
