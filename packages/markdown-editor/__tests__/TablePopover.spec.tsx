import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {ExecuteOptions} from 'react-mde';
import {
  TablePopoverContent,
  makeBody,
  makeHeader,
} from '../src/components/TablePopover';
import defaultTexts from '../src/components/defaultTexts';
import user from '@testing-library/user-event';

describe('Jotunheim React Markdown Editor :: TablePopover :: ', () => {
  it('should render popover content', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 0,
          end: 0,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <TablePopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    expect(screen.getAllByTestId('simple-text-field')).toHaveLength(2);
    expect(screen.getAllByRole('button')).toHaveLength(2);
  });

  it('should add table when clicking confirm', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 3,
          end: 3,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <TablePopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    const imagePopoverInput = screen.getAllByTestId('simple-text-field');
    const description = imagePopoverInput[0];
    const source = imagePopoverInput[1];
    fireEvent.change(description, {target: {value: 3}});
    fireEvent.change(source, {target: {value: 2}});

    user.click(screen.getByText('Add'));

    expect(opts.textApi.replaceSelection).toHaveBeenCalledWith(
      `
|  |  |
| --- | --- |
|  |  |
|  |  |`
    );

    expect(opts.textApi.setSelectionRange).toHaveBeenCalledWith({
      start: opts.initialState.selection.start + 2,
      end: opts.initialState.selection.end + 2,
    });
    expect(setIsOpen).toHaveBeenCalledWith(false);
  });

  it('should not add table when clicking cancel ', () => {
    const opts = {
      initialState: {
        text: '',
        selectedText: '',
        selection: {
          start: 3,
          end: 3,
        },
      },
      textApi: {
        replaceSelection: jest.fn(),
        setSelectionRange: jest.fn(),
        getState: jest.fn(),
      },
    } as ExecuteOptions;
    const setIsOpen = jest.fn();

    render(
      <TablePopoverContent
        setIsOpen={setIsOpen}
        texts={defaultTexts}
        opts={opts}
      />
    );

    const imagePopoverInput = screen.getAllByTestId('simple-text-field');
    const description = imagePopoverInput[0];
    const source = imagePopoverInput[1];
    fireEvent.change(description, {target: {value: 3}});
    fireEvent.change(source, {target: {value: 2}});

    user.click(screen.getByText('Cancel'));

    expect(opts.textApi.replaceSelection).not.toHaveBeenCalled();
    expect(opts.textApi.setSelectionRange).not.toHaveBeenCalled();
    expect(setIsOpen).toHaveBeenCalledWith(false);
  });

  it('makeHeader should create header row', () => {
    expect(makeHeader(0)).toEqual(
      `|
|
`
    );
    expect(makeHeader(1)).toEqual(
      `|  |
| --- |
`
    );
    expect(makeHeader(2)).toEqual(
      `|  |  |
| --- | --- |
`
    );
  });

  it('makeBody should create columns and rows for the table body', () => {
    expect(makeBody(0, 0)).toEqual(``);
    expect(makeBody(2, 2)).toEqual(
      `|  |  |
|  |  |`
    );
    expect(makeBody(1, 4)).toEqual(
      `|  |
|  |
|  |
|  |`
    );
  });
});
