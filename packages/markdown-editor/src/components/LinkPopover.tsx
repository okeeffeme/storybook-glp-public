import React, {useState} from 'react';
import {ExecuteOptions} from 'react-mde';
import cn from 'classnames';

import Button from '@jotunheim/react-button';
import Icon, {link as linkPath} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';
import TextField from '@jotunheim/react-text-field';
import {bemFactory} from '@jotunheim/react-themes';

import {MarkdownEditorTexts} from './defaultTexts';
import {baseClassName} from './constants';
import classNames from './markdown-editor.module.css';

type LinkPopoverProps = {
  opts: ExecuteOptions | undefined;
  texts: MarkdownEditorTexts;
};

type LinkPopoverContentProps = LinkPopoverProps & {
  setIsOpen: (isOpen: boolean) => void;
};

export const LinkPopoverContent = ({
  opts,
  setIsOpen,
  texts,
}: LinkPopoverContentProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [description, setDescription] = useState('');
  const [link, setLink] = useState('https://');

  const handleAdd = () => {
    if (!opts) {
      return;
    }

    const textToAdd = `[${description}](${link})`;
    opts.textApi.replaceSelection(textToAdd);
    resetToDefault();
  };

  const resetToDefault = () => {
    setDescription('');
    setLink('https://');
    setIsOpen(false);
  };

  return (
    <>
      <div className={element('popover-content-row')}>
        <TextField
          label={texts.text}
          onChange={setDescription}
          placeholder={texts.linkDescription}
          value={description}
        />
      </div>
      <div className={element('popover-content-row')}>
        <TextField label={texts.link} onChange={setLink} value={link} />
      </div>
      <div
        className={cn(
          element('popover-content-row'),
          element('popover-content-row', 'buttons')
        )}>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="cancel-link"
            onClick={resetToDefault}>
            {texts.cancel}
          </Button>
        </div>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="confirm-link"
            appearance={Button.appearances.primarySuccess}
            onClick={handleAdd}>
            {texts.add}
          </Button>
        </div>
      </div>
    </>
  );
};

const LinkPopover = ({opts, texts}: LinkPopoverProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Popover
      contentClassName={element('popover-content')}
      content={
        <LinkPopoverContent setIsOpen={setIsOpen} opts={opts} texts={texts} />
      }
      open={isOpen}
      onToggle={setIsOpen}
      closeOnOutsideClick={true}>
      <span className={element('popover-button')} data-testid="link-popover">
        <Icon size={16} path={linkPath} />
      </span>
    </Popover>
  );
};
export default LinkPopover;
