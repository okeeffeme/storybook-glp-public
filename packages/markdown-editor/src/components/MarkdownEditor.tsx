import React, {useMemo, useState} from 'react';
import ReactMarkdownEditor, {Command, ExecuteOptions} from 'react-mde';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import getIcon from './getIcon';
import defaultTexts, {MarkdownEditorTexts} from './defaultTexts';
import {baseClassName} from './constants';
import classNames from './markdown-editor.module.css';

type MarkdownEditorProps = {
  onChange: (markdown: string) => void;
  value: string;
  autoFocus?: boolean;
  rows?: number;
  texts?: MarkdownEditorTexts;
  inputRef?: React.Ref<HTMLInputElement>;
};

const MarkdownEditor = ({
  autoFocus = false,
  onChange,
  rows = 7,
  texts,
  value,
  inputRef,
  ...rest
}: MarkdownEditorProps) => {
  const {block, element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [editorOpts, setEditorOpts] = useState<ExecuteOptions>();

  const mergedTexts = useMemo(
    () => ({...defaultTexts, ...(texts || {})}),
    [texts]
  );

  const insertImage: Command = {
    execute: (opts) => setEditorOpts(opts),
  };

  const insertLink: Command = {
    execute: (opts) => setEditorOpts(opts),
  };

  const insertTable: Command = {
    execute: (opts) => setEditorOpts(opts),
  };

  return (
    <div
      className={block()}
      data-testid="markdown-editor"
      {...extractDataAriaIdProps(rest)}>
      <ReactMarkdownEditor
        childProps={{
          commandButtons: {
            className: element('toolbar-button'),
          },
          textArea: {
            autoFocus,
            rows,
            className: element('textarea'),
            style: {
              height: '100%',
            },
          },
        }}
        classes={{
          textArea: element('input'),
          toolbar: element('toolbar'),
        }}
        commands={{insertLink, insertImage, insertTable}}
        disablePreview={true}
        getIcon={(command) => getIcon(command, editorOpts, mergedTexts)}
        onChange={onChange}
        selectedTab={'write'}
        toolbarCommands={[
          ['header', 'bold', 'italic', 'strikethrough'],
          ['quote', 'code', 'unordered-list', 'ordered-list'],
          ['insertTable', 'insertImage', 'insertLink'],
        ]}
        value={value}
        refs={{
          textarea: inputRef as
            | React.RefObject<HTMLTextAreaElement>
            | undefined,
        }}
      />
    </div>
  );
};

export default React.forwardRef(function MarkdownEditorWrapper(
  props: MarkdownEditorProps,
  ref: React.Ref<HTMLInputElement>
) {
  return <MarkdownEditor {...props} inputRef={ref} />;
});
