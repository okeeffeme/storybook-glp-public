import React from 'react';
import Icon, {
  formatBold,
  formatCode,
  formatHeading,
  formatItalic,
  formatListBulletedSquare,
  formatListNumbered,
  formatQuote,
  formatStrikethrough,
} from '@jotunheim/react-icons';
import {ExecuteOptions} from 'react-mde';

import ImagePopover from './ImagePopover';
import LinkPopover from './LinkPopover';
import TablePopover from './TablePopover';
import {MarkdownEditorTexts} from './defaultTexts';

function getPath(command: string) {
  switch (command) {
    case 'header':
      return formatHeading;
    case 'bold':
      return formatBold;
    case 'italic':
      return formatItalic;
    case 'strikethrough':
      return formatStrikethrough;
    case 'quote':
      return formatQuote;
    case 'code':
      return formatCode;
    case 'unordered-list':
      return formatListBulletedSquare;
    case 'ordered-list':
      return formatListNumbered;
    default:
      return command;
  }
}

function getIcon(
  command: string,
  opts: ExecuteOptions | undefined,
  texts: MarkdownEditorTexts
) {
  if (command === 'insertImage') {
    return <ImagePopover opts={opts} texts={texts} />;
  }

  if (command === 'insertLink') {
    return <LinkPopover opts={opts} texts={texts} />;
  }

  if (command === 'insertTable') {
    return <TablePopover opts={opts} texts={texts} />;
  }

  const path = getPath(command);
  return <Icon size={16} path={path} />;
}

export default getIcon;
