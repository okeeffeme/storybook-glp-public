export type MarkdownEditorTexts = {
  add?: string;
  caption?: string;
  cancel?: string;
  columns?: string;
  imageDescription?: string;
  link?: string;
  linkDescription?: string;
  source?: string;
  rows?: string;
  text?: string;
};

const defaultTexts: MarkdownEditorTexts = {
  add: 'Add',
  caption: 'Caption',
  cancel: 'Cancel',
  columns: 'Columns',
  imageDescription: 'Image Description',
  link: 'Link',
  linkDescription: 'Link Description',
  source: 'Source',
  rows: 'Rows',
  text: 'Text',
};

export default defaultTexts;
