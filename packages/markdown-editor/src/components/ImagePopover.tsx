import React, {useState} from 'react';
import {ExecuteOptions} from 'react-mde';
import cn from 'classnames';

import Button from '@jotunheim/react-button';
import Icon, {imageOutline} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';
import TextField from '@jotunheim/react-text-field';
import {bemFactory} from '@jotunheim/react-themes';

import {MarkdownEditorTexts} from './defaultTexts';
import {baseClassName} from './constants';
import classNames from './markdown-editor.module.css';

type ImagePopoverProps = {
  opts: ExecuteOptions | undefined;
  texts: MarkdownEditorTexts;
};

type ImagePopoverContentProps = ImagePopoverProps & {
  setIsOpen: (isOpen: boolean) => void;
};

export const ImagePopoverContent = ({
  opts,
  setIsOpen,
  texts,
}: ImagePopoverContentProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [description, setDescription] = useState('');
  const [imageLink, setImageLink] = useState('https://');

  const handleAdd = () => {
    if (!opts) {
      return;
    }

    const textToAdd = `![${description}](${imageLink})`;
    opts.textApi.replaceSelection(textToAdd);
    resetToDefault();
  };

  const resetToDefault = () => {
    setDescription('');
    setImageLink('https://');
    setIsOpen(false);
  };

  return (
    <>
      <div className={element('popover-content-row')}>
        <TextField
          label={texts.caption}
          onChange={setDescription}
          placeholder={texts.imageDescription}
          value={description}
        />
      </div>
      <div className={element('popover-content-row')}>
        <TextField
          label={texts.source}
          onChange={setImageLink}
          value={imageLink}
        />
      </div>
      <div
        className={cn(
          element('popover-content-row'),
          element('popover-content-row', 'buttons')
        )}>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="cancel-image"
            onClick={resetToDefault}>
            {texts.cancel}
          </Button>
        </div>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="confirm-image"
            appearance={Button.appearances.primarySuccess}
            onClick={handleAdd}>
            {texts.add}
          </Button>
        </div>
      </div>
    </>
  );
};

const ImagePopover = ({opts, texts}: ImagePopoverProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Popover
      contentClassName={element('popover-content')}
      content={
        <ImagePopoverContent setIsOpen={setIsOpen} opts={opts} texts={texts} />
      }
      open={isOpen}
      onToggle={setIsOpen}
      closeOnOutsideClick={true}>
      <span className={element('popover-button')} data-testid="image-popover">
        <Icon size={16} path={imageOutline} />
      </span>
    </Popover>
  );
};

export default ImagePopover;
