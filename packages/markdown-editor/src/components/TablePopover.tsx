import React, {useState} from 'react';
import cn from 'classnames';
import {ExecuteOptions} from 'react-mde';

import Button from '@jotunheim/react-button';
import Icon, {formatTable} from '@jotunheim/react-icons';
import {Popover} from '@jotunheim/react-popover';
import TextField from '@jotunheim/react-text-field';
import {bemFactory} from '@jotunheim/react-themes';

import {MarkdownEditorTexts} from './defaultTexts';
import {baseClassName} from './constants';
import classNames from './markdown-editor.module.css';

type TablePopoverProps = {
  opts: ExecuteOptions | undefined;
  texts: MarkdownEditorTexts;
};

type TablePopoverContentProps = TablePopoverProps & {
  setIsOpen: (isOpen: boolean) => void;
};

export function makeHeader(col: number) {
  let header = '|';
  let border = '|';

  while (col) {
    header += '  |';
    border += ' --- |';
    col -= 1;
  }

  return `${header}\n${border}\n`;
}

export function makeBody(col: number, row: number) {
  let body = '';

  for (let x = 0; x < row; x++) {
    body += '|';

    for (let y = 0; y < col; y++) {
      body += '  |';
    }

    body += '\n';
  }

  body = body.replace(/\n$/g, '');
  return body;
}

export const TablePopoverContent = ({
  opts,
  setIsOpen,
  texts,
}: TablePopoverContentProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [rows, setRows] = useState(1);
  const [columns, setColumns] = useState(1);

  const handleAdd = () => {
    if (!opts) {
      return;
    }

    const start = opts.initialState.selection.start;
    let table = '\n';
    table += makeHeader(columns);
    table += makeBody(columns, rows - 1);
    opts.textApi.replaceSelection(table);

    // Want to set the cursor in the first header position
    // Which would be 2 indices over from where we started
    // First index would be the new line, second being the first '|' of the table.
    opts.textApi.setSelectionRange({start: start + 2, end: start + 2});

    resetToDefault();
  };

  const resetToDefault = () => {
    setRows(1);
    setColumns(1);
    setIsOpen(false);
  };

  return (
    <>
      <div className={element('popover-content-row')}>
        <div className={element('popover-content-row-item')}>
          <TextField
            label={texts.rows}
            onChange={setRows}
            type={'number'}
            value={rows}
          />
        </div>
        <div className={element('popover-content-row-item')}>
          <TextField
            label={texts.columns}
            onChange={setColumns}
            type={'number'}
            value={columns}
          />
        </div>
      </div>
      <div
        className={cn(
          element('popover-content-row'),
          element('popover-content-row', 'buttons')
        )}>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="cancel-table"
            onClick={resetToDefault}>
            {texts.cancel}
          </Button>
        </div>
        <div className={element('popover-content-row-item')}>
          <Button
            data-markdown-editor-test="confirm-table"
            appearance={Button.appearances.primarySuccess}
            onClick={handleAdd}>
            {texts.add}
          </Button>
        </div>
      </div>
    </>
  );
};

const TablePopover = ({opts, texts}: TablePopoverProps) => {
  const {element} = bemFactory({
    baseClassName,
    classNames,
  });
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Popover
      contentClassName={element('popover-content')}
      content={
        <TablePopoverContent setIsOpen={setIsOpen} opts={opts} texts={texts} />
      }
      open={isOpen}
      onToggle={setIsOpen}
      closeOnOutsideClick={true}>
      <span className={element('popover-button')} data-testid="table-popover">
        <Icon size={16} path={formatTable} />
      </span>
    </Popover>
  );
};

export default TablePopover;
