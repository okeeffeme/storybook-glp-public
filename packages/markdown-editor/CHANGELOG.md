# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.52&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.53&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.52&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.52&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.50&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.51&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.49&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.50&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.48&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.49&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.47&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.48&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.46&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.47&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.45&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.46&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [2.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.44&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.45&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.43&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.44&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.42&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.43&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.41&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.42&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.40&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.41&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.39&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.40&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.38&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.39&targetRepoId=1246) (2023-02-01)

### Bug Fixes

- adding data-testids to Markdown-editor components ([NPM-1199](https://jiraosl.firmglobal.com/browse/NPM-1199)) ([7dca085](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7dca085e22507e1dfa8cdb7320cc6a75e1e02ac2))

## [2.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.37&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.38&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.36&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.37&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.35&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.36&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.33&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.35&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.33&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.34&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.32&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.33&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.31&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.32&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.30&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.31&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.29&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.30&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.28&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.29&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.27&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.28&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.26&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.27&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.25&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.26&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.22&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.25&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.22&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.24&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.22&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.23&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.21&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.22&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.20&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.21&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.18&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.19&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.17&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.18&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.16&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.15&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.14&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.13&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.12&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.11&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.10&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.9&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.7&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.4&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.3&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.2&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.1&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.0&sourceBranch=refs/tags/@jotunheim/react-markdown-editor@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-markdown-editor

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.6&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.7&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [1.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.5&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.6&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [1.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.4&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.5&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.3&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.4&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.2&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.3&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@1.0.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.2&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-markdown-editor

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.53&sourceBranch=refs/tags/@confirmit/react-markdown-editor@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Remove support for className prop and default theme. ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.3.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.52&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.53&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.51&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.52&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.50&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.51&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.49&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.50&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.48&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.49&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.47&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.48&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.44&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.45&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.43&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.44&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.42&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.43&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.41&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.42&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.40&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.41&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.39&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.40&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.38&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.39&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.37&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.38&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.3.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.36&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.37&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.34&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.35&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.33&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.34&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.32&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.33&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.31&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.32&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.30&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.31&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.29&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.30&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.28&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.29&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.27&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.28&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.26&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.27&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.25&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.26&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.24&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.25&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.23&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.24&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.22&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.23&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.21&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.22&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.20&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.21&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.19&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.20&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.18&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.19&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.17&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.18&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.16&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.17&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.15&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.16&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.14&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.15&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.13&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.14&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.12&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.13&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.11&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.12&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.9&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.10&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.8&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.9&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.7&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.8&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.6&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.7&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.5&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.6&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.4&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.3&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.4&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.2&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.3&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.2&targetRepoId=1246) (2021-03-10)

### Bug Fixes

- placement of buttons in popovers for markdown-editor was incorrectly on left side, and confirm button was on left side. They are now right-aligned and confirm button is on right side to be aligned with Design System spec. ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([2d3cd00](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2d3cd00838bdf025919b91dfa6cc10dd545c45cc))

## [0.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.3.0&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.1&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-markdown-editor

# [0.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.25&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.3.0&targetRepoId=1246) (2021-02-24)

### Features

- Add ref prop to MarkdownEditor ([NPM-732](https://jiraosl.firmglobal.com/browse/NPM-732)) ([4259c17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4259c17c819a358c6d8909235ff1f3db5976ef09))

## [0.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.24&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.23&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.21&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.20&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.19&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.18&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.17&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.18&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.16&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.15&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.14&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.13&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.12&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.11&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.10&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.9&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.6&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.3&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.2&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.2.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-markdown-editor

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.1.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.1.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.2.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.1.1&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.1.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-markdown-editor

# [0.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.18&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.1.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [0.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.17&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.18&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.16&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.17&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.15&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.16&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.14&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.15&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.11&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.12&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.10&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.11&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.8&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.9&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.5&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.6&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.4&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.5&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-markdown-editor

## [0.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-editor@0.0.2&sourceBranch=refs/tags/@confirmit/react-markdown-editor@0.0.3&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-markdown-editor

### v0.0.2

- Fix: Popover trigger was not encompassing entire toolbar button, instead only the svg.
- Fix: Toolbar didn't wrap on smaller screens

### v0.0.1

- Initial version
