import React from 'react';
import {screen, render} from '@testing-library/react';
import PortalRootContext from '../src/PortalRootContext';

const ctxRender = (ui, providerProps) => {
  return render(
    <PortalRootContext.Provider value={providerProps}>
      {ui}
    </PortalRootContext.Provider>
  );
};

describe('LayoutContext', () => {
  const providerProps = {
    current: document.body,
  };

  it('should have passed values', () => {
    ctxRender(
      <PortalRootContext.Consumer>
        {({current}) => (
          <ul id="wrapper">
            <li>Root: {JSON.stringify(current)}</li>
          </ul>
        )}
      </PortalRootContext.Consumer>,
      providerProps
    );

    expect(screen.getByText(/^Root:/).textContent).not.toBeFalsy();
  });
});
