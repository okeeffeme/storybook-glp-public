import React from 'react';
import {screen, render} from '@testing-library/react';
import ZIndexStackContext from '../src/ZIndexStackContext';

const ctxRender = (ui, providerProps) => {
  return render(
    <ZIndexStackContext.Provider value={providerProps}>
      {ui}
    </ZIndexStackContext.Provider>
  );
};

describe('ZIndexStackContext', () => {
  const providerProps = {
    initial: 0,
    step: 10,
    obtain: jest.fn(),
    release: jest.fn(),
    obtainFixed: jest.fn(),
  };

  it('should have passed values', () => {
    ctxRender(
      <ZIndexStackContext.Consumer>
        {({initial, step}) => (
          <ul>
            <li>Initial: {initial}</li>
            <li>Step: {step}</li>
          </ul>
        )}
      </ZIndexStackContext.Consumer>,
      providerProps
    );

    expect(screen.getByText(/^Initial:/).textContent).toBe(
      `Initial: ${providerProps.initial}`
    );
    expect(screen.getByText(/^Step:/).textContent).toBe(
      `Step: ${providerProps.step}`
    );
  });
});
