import React from 'react';
import {screen, render} from '@testing-library/react';
import LayoutContext from '../src/LayoutContext';

const ctxRender = (ui, providerProps) => {
  return render(
    <LayoutContext.Provider value={providerProps}>{ui}</LayoutContext.Provider>
  );
};

describe('LayoutContext', () => {
  const providerProps = {
    inputWidth: 720,
  };

  it('should have passed values', () => {
    ctxRender(
      <LayoutContext.Consumer>
        {({inputWidth}) => (
          <ul>
            <li>Width: {inputWidth}</li>
          </ul>
        )}
      </LayoutContext.Consumer>,
      providerProps
    );

    expect(screen.getByText(/^Width:/).textContent).toBe(
      `Width: ${providerProps.inputWidth}`
    );
  });
});
