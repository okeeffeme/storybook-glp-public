# Jotunheim React Contexts

[Changelog](./CHANGELOG.md)

Contains different react contexts that are used across shared components.
Some contexts are provided with hooks and HOCs to have easy access to a context.

## LayoutContext

#### Use Provider to pass settings for all inputs within (TextField, Select, TextArea, SearchField)

```javascript
import React from 'react';
import {LayoutContext} from '@jotunheim/react-contexts';

const ParentComponent = ({children}) => {
  return (
      ..... // pass the ref to the root container into Provider
      <LayoutContext.Provider value={{
        inputWidth: InputWidth.Max600Min80
      }}>
        {children}
      </LayoutContext.Provider>
  );
};
```

## PortalRootContext

#### Use Provider to pass root container

```javascript
import React from 'react';
import {PortalRootContext} from '@jotunheim/react-contexts';

const MyComponent = ({children}) => {
  const ref = React.createRef();
  return (
    <>
      <div ref={ref} />
      ..... // pass the ref to the root container into Provider
      <PortalRootContext.Provider value={ref}>
        {children}
      </PortalRootContext.Provider>
    </>
  );
};
```

#### Get method for rendering portals using React hooks

```javascript
import React, {useEffect} from 'react';
import {usePortal, withPortal} from '@jotunheim/react-transition-portal';

// use 'usePortal' for functional components
const DivFunctional = ({showElement, children}) => {
  /**
   * pass 'className' prop to set custom className for portal element
   *
   * call renderInPortal to pass a child into portal element
   * call mountPortalElement to render element into DOM
   * call unmountPortalElement to remove element from DOM
   * prop isPortalElementInDom contains the state of portal element mount (true/false)
   * prop portalElement refers to the portal DOM element
   */
  const {
    renderInPortal,
    mountPortalElement,
    unmountPortalElement,
    isPortalElementInDom,
    portalElement,
  } = usePortal({className});

  useEffect(() => {
    if (showElement) {
      mountPortalElement();
    }
    return () => unmountPortalElement();
  }, [showElement]);

  return renderInPortal(children);
};

// 'withPortal' wrapper adds methods for managing portals to the class component props
class DivClass extends React.Component {
  onOpen() {
    this.props.mountPortalElement();
  }

  onClose() {
    this.props.unmountPortalElement();
  }

  render() {
    const {renderInPortal, children} = this.props;
    return renderInPortal(children);
  }
}

const DivClassWithPortal = withPortal({className})(DivClass);
```

## ZIndexStackContext

#### Use Provider to pass zIndexStack instance

```javascript
import React from 'react';
import zIndexStack, {ZIndexStackContext} from '@jotunheim/react-contexts';

const MyComponent = ({children}) => (
  // pass to Provider your instance of zIndexStack
  <ZIndexStackContext.Provider value={zIndexStack}>
    {children}
  </ZIndexStackContext.Provider>
);
```

#### Get zIndexStack instance using React hooks and HOC

```javascript
import React from 'react';
import {useZIndexStack, withZIndexStack} from '@jotunheim/react-contexts';

// use 'useZIndexStack' for functional components
const DivFunctional = ({children}) => {
  const zIndexStack = useZIndexStack();
  return <div style={{zIndex: zIndexStack.obtain()}}>{children}</div>;
};

// 'withZIndexStack' wrapper adds 'zIndexStack' to the class component props
class DivClass extends React.Component {
  render() {
    const {zIndexStack, children} = this.props;
    return <div style={{zIndex: zIndexStack.obtain()}}>{children}</div>;
  }
}

const DivClassWithZIndex = withZIndexStack(DivClass);
```
