import React from 'react';
import hoistNonReactStatics from 'hoist-non-react-statics';
import {getDisplayName} from '@jotunheim/react-utils';

import usePortal from './usePortal';

type WithPortalProps = {
  className?: string;
};

const withPortal =
  ({className}: WithPortalProps = {}) =>
  (Component) => {
    const ComponentWithPortal = (props) => {
      const portalProps = usePortal({className});

      return <Component {...props} {...portalProps} />;
    };

    ComponentWithPortal.displayName = getDisplayName(Component);
    hoistNonReactStatics(ComponentWithPortal, Component);

    return ComponentWithPortal;
  };

export default withPortal;
