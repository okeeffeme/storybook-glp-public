import React from 'react';
import hoistNonReactStatics from 'hoist-non-react-statics';
import {getDisplayName} from '@jotunheim/react-utils';

import useZIndexStack from './useZIndexStack';

const withZIndexStack = (Component) => {
  const ZIndexStackComponent = (props) => {
    const zIndexStack = useZIndexStack();
    return <Component {...props} zindexStack={zIndexStack} />;
  };

  ZIndexStackComponent.displayName = getDisplayName(Component);
  hoistNonReactStatics(ZIndexStackComponent, Component);

  return ZIndexStackComponent;
};

export default withZIndexStack;
