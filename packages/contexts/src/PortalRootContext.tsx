import React, {MutableRefObject} from 'react';

const defaultValue: MutableRefObject<HTMLElement | null | undefined> = {
  current: document.body,
};
const PortalRootContext = React.createContext(defaultValue);

export default PortalRootContext;
