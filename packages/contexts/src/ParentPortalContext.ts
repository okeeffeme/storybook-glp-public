import {createContext} from 'react';

type ParentPortalContext = {
  addPortalElementToParent: (portalElement: HTMLElement) => void;
  removePortalElementFromParent: (portalElement: HTMLElement) => void;
};

const context = createContext<ParentPortalContext>({
  addPortalElementToParent: () => {},
  removePortalElementFromParent: () => {},
});

export default context;
