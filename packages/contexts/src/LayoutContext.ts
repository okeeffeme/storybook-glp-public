import {createContext} from 'react';

export enum InputWidth {
  Stretch = 'stretch',
  Max600Min80 = 'max600min80',
}

type LayoutContextValue = {
  inputWidth: InputWidth;
};

export default createContext<LayoutContextValue>({
  inputWidth: InputWidth.Stretch,
});
