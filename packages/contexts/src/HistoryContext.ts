import {createContext} from 'react';

export type Location = {
  pathname?: string;
  hash?: string;
  search?: string;
};
type HistoryContextValue = {
  push: (location: Location) => void;
  createHref?: (location: Location) => string;
};

export default createContext<HistoryContextValue | undefined>(undefined);
