import {useContext} from 'react';
import type {ZIndexStack} from '@jotunheim/react-z-index';

import ZIndexStackContext from './ZIndexStackContext';

export default function useZIndexStack(): ZIndexStack {
  return useContext(ZIndexStackContext);
}
