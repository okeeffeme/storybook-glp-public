import React, {useContext, useEffect, useMemo, useRef} from 'react';
import PortalTriggerContext from './ParentPortalContext';

export default function useParentPortal({
  portalHTMLElement,
}: {
  portalHTMLElement: HTMLElement | null;
}) {
  const descendantPortalHTMLElements = useRef<Set<HTMLElement>>(new Set());
  const portalTriggerContext = useContext(PortalTriggerContext);

  useEffect(() => {
    if (portalHTMLElement) {
      portalTriggerContext.addPortalElementToParent(portalHTMLElement);

      return () => {
        portalTriggerContext.removePortalElementFromParent(portalHTMLElement);
      };
    }
  }, [portalHTMLElement, portalTriggerContext]);

  const parentPortalTriggerContext = useMemo(() => {
    return {
      addPortalElementToParent: (childPortalElement: HTMLElement) => {
        descendantPortalHTMLElements.current.add(childPortalElement);
        portalTriggerContext.addPortalElementToParent(childPortalElement);
      },
      removePortalElementFromParent: (childPortalElement: HTMLElement) => {
        descendantPortalHTMLElements.current.delete(childPortalElement);
        portalTriggerContext.removePortalElementFromParent(childPortalElement);
      },
    };
  }, [portalTriggerContext]);

  const ParentPortalTriggerProvider = useMemo(
    () =>
      function ParentPortalTriggerProvider({children}) {
        return (
          <PortalTriggerContext.Provider value={parentPortalTriggerContext}>
            {children}
          </PortalTriggerContext.Provider>
        );
      },
    [parentPortalTriggerContext]
  );

  return {
    descendantPortalHTMLElements: descendantPortalHTMLElements.current,
    ParentPortalTriggerProvider,
  };
}
