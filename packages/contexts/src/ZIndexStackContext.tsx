import React from 'react';
import {zIndexStack} from '@jotunheim/react-z-index';
import type {ZIndexStack} from '@jotunheim/react-z-index';

const ZIndexStackContext = React.createContext<ZIndexStack>(zIndexStack);

export default ZIndexStackContext;
