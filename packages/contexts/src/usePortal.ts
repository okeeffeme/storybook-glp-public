import {
  useCallback,
  useContext,
  useEffect,
  useState,
  useRef,
  ReactNode,
} from 'react';
import ReactDOM from 'react-dom';
import PortalRootContext from './PortalRootContext';
import {
  addClassNameToDOMNode,
  removeClassNameFromDOMNode,
} from '@jotunheim/react-utils';

export type UsePortalProps = {
  className?: string;
};

function usePortal({className}: UsePortalProps = {}) {
  const rootElement = useContext(PortalRootContext).current;
  const portalElementRef = useRef<HTMLElement | null>(null);

  if (!portalElementRef.current) {
    portalElementRef.current = document.createElement('div');
    portalElementRef.current.classList.add('co-portal');
  }

  const portalElement = portalElementRef.current;

  useEffect(() => {
    if (className && portalElement) {
      addClassNameToDOMNode(portalElement, className);
      return () => removeClassNameFromDOMNode(portalElement, className);
    }
  }, [className, portalElement]);

  const [isPortalElementInDom, setIsPortalElementInDom] = useState(false);

  const mountPortalElement = useCallback(() => {
    if (rootElement && !isPortalElementInDom) {
      rootElement.appendChild(portalElement);
      setIsPortalElementInDom(true);
    }
  }, [rootElement, isPortalElementInDom, portalElement]);

  const unmountPortalElement = useCallback(() => {
    if (
      rootElement &&
      isPortalElementInDom &&
      rootElement.contains(portalElement)
    ) {
      rootElement.removeChild(portalElement);
      setIsPortalElementInDom(false);
    }
  }, [rootElement, isPortalElementInDom, portalElement]);

  const renderInPortal = useCallback(
    (children: ReactNode) => ReactDOM.createPortal(children, portalElement),
    [portalElement]
  );

  return {
    isPortalElementInDom,
    mountPortalElement,
    unmountPortalElement,
    renderInPortal,
    portalElement,
  };
}

export default usePortal;
