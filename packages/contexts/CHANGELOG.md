# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-contexts@2.0.3&sourceBranch=refs/tags/@jotunheim/react-contexts@2.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-contexts

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-contexts@2.0.2&sourceBranch=refs/tags/@jotunheim/react-contexts@2.0.3&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-contexts

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-contexts@2.0.1&sourceBranch=refs/tags/@jotunheim/react-contexts@2.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-contexts

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-contexts@2.0.0&sourceBranch=refs/tags/@jotunheim/react-contexts@2.0.1&targetRepoId=1246) (2022-06-30)

### Bug Fixes

- update z-index peerDep of react-contexts ([NPM-1035](https://jiraosl.firmglobal.com/browse/NPM-1035)) ([085f863](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/085f8638c4adad858a8e4c990de875c9ffd33726))

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@1.0.1&sourceBranch=refs/tags/@confirmit/react-contexts@1.0.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@1.0.0&sourceBranch=refs/tags/@confirmit/react-contexts@1.0.1&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-contexts

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@0.3.0&sourceBranch=refs/tags/@confirmit/react-contexts@1.0.0&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- make z-index package peer dependency ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([2fd04ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2fd04fffc2296f0c2c323fa12891afae7ca30bdb))

### BREAKING CHANGES

- make z-index package peer dependency (AM-7117)

# [0.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@0.2.1&sourceBranch=refs/tags/@confirmit/react-contexts@0.3.0&targetRepoId=1246) (2021-09-16)

### Features

- new LayoutProvider for setting width of inputs ([NPM-571](https://jiraosl.firmglobal.com/browse/NPM-571)) ([db60442](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db60442287854b5eba8de4a66403f20614b8b93c))

## [0.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@0.2.0&sourceBranch=refs/tags/@confirmit/react-contexts@0.2.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-contexts@0.1.0&sourceBranch=refs/tags/@confirmit/react-contexts@0.2.0&targetRepoId=1246) (2020-12-11)

### Features

- Support basename for simple link ([NPM-652](https://jiraosl.firmglobal.com/browse/NPM-652)) ([fb7fcda](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb7fcda552fa78db12efc836450f68990a41775c))

# 0.1.0 (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- add @confirmit/react-contexts package ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([1fc3a61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1fc3a617543507e8ad6232cdc3979698b043ca87))
- add ParentPortalContext ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([6862a60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6862a603ddad7a97de83dcc2b0e0afae0778ae24))
- add PortalRootContext ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([5c77fa1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5c77fa14059d7368db8ea28d1e836449c522c874))
- add ZIndexStackContext ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([2c39a72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2c39a729fb8aa02f2dacaf42b150d2eda34fb456))

### BREAKING CHANGES

- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.

## CHANGELOG

### v0.0.1

- Initial version
