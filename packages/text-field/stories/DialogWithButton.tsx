import React, {ReactNode} from 'react';
import Button from '../../button/src';
import {Dialog, DialogSizes} from '../../dialog/src';

type DialogWithButtonProps = {
  open?: boolean;
  isModal?: boolean;
  keyboard?: boolean;
  className?: string;
  buttonText?: ReactNode;
  children?: ReactNode;
  onToggle?: (open: boolean) => void;
  size?: DialogSizes;
  portalZIndex?: number;
};

const DialogWithButton = ({
  buttonText,
  children,
  isModal,
  keyboard,
  size,
  className,
  portalZIndex,
  ...rest
}: DialogWithButtonProps) => {
  const [open, onToggle] = React.useState(false);

  return (
    <div style={{display: 'inline-block'}} className={className} {...rest}>
      <Button onClick={() => onToggle(true)}>{buttonText}</Button>
      <Dialog
        open={open}
        onHide={() => onToggle(false)}
        isModal={isModal}
        keyboard={keyboard}
        size={size}
        portalZIndex={portalZIndex}>
        {children}
      </Dialog>
    </div>
  );
};

DialogWithButton.Header = Dialog.Header;
DialogWithButton.Body = Dialog.Body;
DialogWithButton.Footer = Dialog.Footer;

export default DialogWithButton;
