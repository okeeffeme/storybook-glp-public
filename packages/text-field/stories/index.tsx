import Icon, {alert, magnify} from '../../icons/src/index';
import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {boolean, select, text, number} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {TextField} from '../src/index';
import DialogWithButton from './DialogWithButton';
import Button from '../../button/src';
import {Dialog} from '../../dialog/src';
import {LayoutContext, InputWidth} from '../../contexts/src';

const UncontrolledTextField = (props) => {
  const [value, onChange] = React.useState('');

  return <TextField value={value} onChange={onChange} {...props} />;
};

const TextFieldWrapper = (props) => {
  const inputRef = React.useRef<HTMLInputElement | null>(null);

  return (
    <div>
      <Button
        onClick={() => {
          inputRef.current && inputRef.current.focus();
        }}>
        Click to focus
      </Button>
      <TextField ref={inputRef} {...props} />
    </div>
  );
};

storiesOf('Components/text-field', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default with knobs', () => {
    const [value, setValue] = React.useState('');

    return (
      <div
        style={{
          padding: '50px 15px',
          height: '400px',
          backgroundColor: '#f1f1f1',
        }}>
        <TextField
          id={text('id', 'some-id')}
          align={select('align', ['left', 'right'], 'left')}
          autoComplete={boolean('autoComplete', false)}
          autoFocus={boolean('autoFocus', true)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          label={text('label', 'Label')}
          maxLength={number('maxLength', 10)}
          maxLengthErrorText={text('maxLengthErrorText', 'Max length exceeded')}
          showMaxLength={boolean('showMaxLength', false)}
          name={text('name', 'name')}
          // Disabled due to performance reasons in storybook
          // https://github.com/storybookjs/storybook/issues/6471
          // onBlur={action('onBlur')}
          // onFocus={action('onFocus')}
          onChange={(newValue) => {
            setValue(newValue);
            action('onChange')(newValue);
          }}
          placeholder={text('placeholder', 'Enter Text Field')}
          required={boolean('required', false)}
          showClear={boolean('showClear', true)}
          type={text('type', 'text') as 'text'}
          value={value}
        />
      </div>
    );
  })
  .add('TextField with prefix and suffix icon and knobs', () => {
    const [value, setValue] = React.useState('');

    return (
      <div style={{margin: '50px 15px'}}>
        <TextField
          id={text('id', 'some-id')}
          autoComplete={boolean('autoComplete', false)}
          autoFocus={boolean('autoFocus', true)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          label={text('label', 'Label')}
          maxLength={number('maxLength', 10)}
          name={text('name', 'name')}
          prefix={<Icon path={magnify} />}
          suffix={<Icon path={alert} />}
          // Disabled due to performance reasons in storybook
          // https://github.com/storybookjs/storybook/issues/6471
          // onBlur={action('onBlur')}
          // onFocus={action('onFocus')}
          onChange={(newValue) => {
            setValue(newValue);
            action('onChange')(newValue);
          }}
          placeholder={text('placeholder', 'Enter Text Field')}
          required={boolean('required', false)}
          showClear={boolean('showClear', true)}
          type={text('type', 'text') as 'text'}
          value={value}
        />
      </div>
    );
  })
  .add('Uncontrolled Wrapper', () => {
    return (
      <div style={{width: 400, margin: 50}}>
        <UncontrolledTextField
          label={'Uncontrolled Text Field'}
          placeholder={'Placeholder'}
          type={'text'}
        />
      </div>
    );
  })
  .add('With ref', () => {
    const [value, setValue] = React.useState('');

    return (
      <div style={{width: 400, margin: 50}}>
        <TextFieldWrapper
          id={'some-id'}
          label={'Without auto-focus'}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          placeholder={'Placeholder'}
          type={'text'}
          value={value}
        />
      </div>
    );
  })
  .add(
    'In Modal: Using ModalButton - Example form with clear all button and autoFocused input',
    () => {
      const [value, setValue] = React.useState(['', '', '']);
      return (
        <DialogWithButton buttonText="basic modal">
          <DialogWithButton.Header title="Title" />
          <DialogWithButton.Body>
            <React.Fragment>
              <TextField
                autoFocus={true}
                id={'some-id'}
                onChange={(val) => {
                  const newValue = [...value];
                  newValue[0] = val;
                  setValue(newValue);
                }}
                placeholder={'Name'}
                type={'text'}
                value={value[0]}
              />
              <TextField
                id={'some-id'}
                onChange={(val) => {
                  const newValue = [...value];
                  newValue[1] = val;
                  setValue(newValue);
                }}
                placeholder={'Description'}
                type={'text'}
                value={value[1]}
              />
              <TextField
                id={'some-id'}
                onChange={(val) => {
                  const newValue = [...value];
                  newValue[2] = val;
                  setValue(newValue);
                }}
                placeholder={'Country'}
                type={'text'}
                value={value[2]}
              />
              <Button onClick={() => setValue(['', '', ''])}>
                CLEAR ALL FIELDS
              </Button>
            </React.Fragment>
          </DialogWithButton.Body>
        </DialogWithButton>
      );
    }
  )
  .add('In Modal: Using Controlled Modal - with autoFocused input', () => {
    const [state, setValue] = React.useState({value: '', isOpen: false});

    return (
      <React.Fragment>
        <Button onClick={() => setValue({...state, isOpen: true})}>
          Open Controlled Modal
        </Button>
        <Dialog
          open={state.isOpen}
          onHide={() => setValue({...state, isOpen: false})}>
          <Dialog.Header title="Title" />
          <Dialog.Body>
            <React.Fragment>
              <TextField
                autoFocus={true}
                id={'some-id'}
                onChange={(value) => setValue({...state, value})}
                helperText={!state.value ? 'error' : undefined}
                error={!state.value}
                placeholder={'Name'}
                type={'text'}
                value={state.value}
              />
            </React.Fragment>
          </Dialog.Body>
        </Dialog>
      </React.Fragment>
    );
  })
  .add('Input type search with native clear button', () => {
    const [value, setValue] = React.useState('');
    return (
      <div style={{width: 400, margin: 50}}>
        <TextField
          label={'Search Field with clear'}
          placeholder={'Placeholder'}
          onChange={setValue}
          showClear={false}
          type={'search'}
          value={value}
        />
      </div>
    );
  })
  .add('type "a" to trigger error', () => {
    const [error, setError] = useState('');
    const [value, setValue] = useState('');
    return (
      <div
        style={{
          padding: '50px 15px',
          height: '400px',
          backgroundColor: '#f1f1f1',
        }}>
        <TextField
          key="text"
          id={text('id', 'some-id')}
          align={select('align', ['left', 'right'], 'left')}
          autoComplete={boolean('autoComplete', false)}
          autoFocus={boolean('autoFocus', false)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          error={!!error.length}
          label={`Type "a" to set error`}
          helperText={error}
          maxLength={number('maxLength', 10)}
          name={text('name', 'name')}
          // Disabled due to performance reasons in storybook
          // onBlur={action('onBlur')}
          // onFocus={action('onFocus')}
          onChange={(value) => {
            setValue(value);
            if (value === 'a') {
              setError('test error1');
            } else {
              setError('');
            }
            action('onChange')(value);
          }}
          required={boolean('required', false)}
          showClear={boolean('showClear', true)}
          type={text('type', 'text') as 'text'}
          value={value}
        />
      </div>
    );
  })
  .add('With Layout Wrapper (max600min80), showing max width', () => {
    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 800, padding: 40, background: '#f1f1f1'}}>
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
        </div>
      </LayoutContext.Provider>
    );
  })
  .add('With Layout Wrapper (max600min80), showing min width', () => {
    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 60, padding: 40, background: '#f1f1f1'}}>
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
          <UncontrolledTextField
            label={'With fixed width'}
            placeholder={'Placeholder'}
            type={'text'}
          />
        </div>
      </LayoutContext.Provider>
    );
  });
