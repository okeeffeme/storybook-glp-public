# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.33&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.34&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.33&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.33&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.31&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.32&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.30&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.31&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.29&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.30&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.28&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.29&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.27&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.28&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.26&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.27&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.25&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.26&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.24&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.25&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.23&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.24&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.22&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.23&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.21&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.22&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.20&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.21&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.19&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.20&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.18&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.19&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.17&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.18&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.15&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.15&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.14&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.15&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.13&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.14&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.12&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.11&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.10&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.9&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.8&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.7&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.3&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.2&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-text-field

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@9.0.0&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-text-field

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.13&sourceBranch=refs/tags/@jotunheim/react-text-field@9.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove inputRef property from TextField ([NPM-1091](https://jiraosl.firmglobal.com/browse/NPM-1091)) ([9878f02](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9878f02e9d630f30674106a29b5d3be9ce46f1d3))

### Features

- remove className prop of TextField ([NPM-957](https://jiraosl.firmglobal.com/browse/NPM-957)) ([eb5dbb0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eb5dbb0ae2f9efb20ade4e280ddb210dd435d13d))

### BREAKING CHANGES

- inputRef property is removed from TextField, though it should not affect anything, as it didn't work properly. Please use ref property instead.
- As part of NPM-925 we remove className props from components.

feat: remove className prop of TextField (NPM-957)

- As part of NPM-925 we remove className props from components.

## [8.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.12&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.11&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.10&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.11&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.9&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.10&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.8&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.7&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.8&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.6&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.7&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.5&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.6&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.3&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.4&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.1.0&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.1&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-text-field

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.0.3&sourceBranch=refs/tags/@jotunheim/react-text-field@8.1.0&targetRepoId=1246) (2022-08-09)

### Features

- pass id of TextField to InputWrapper as htmlFor to be used in label ([NPM-1048](https://jiraosl.firmglobal.com/browse/NPM-1048)) ([3f1ddf5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3f1ddf514f5fd237979b8817fef894476504496b))

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.0.2&sourceBranch=refs/tags/@jotunheim/react-text-field@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.0.1&sourceBranch=refs/tags/@jotunheim/react-text-field@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-text-field

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-text-field@8.0.0&sourceBranch=refs/tags/@jotunheim/react-text-field@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-text-field

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.65&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.66&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.64&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.65&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.63&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.64&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.62&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.63&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.61&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.62&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.60&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.61&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.58&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.59&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.57&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.58&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.56&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.57&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.55&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.56&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.54&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.55&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.53&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.54&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.50&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.51&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.49&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.50&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.48&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.49&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.47&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.48&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.46&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.47&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.45&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.46&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.44&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.45&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.43&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.44&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.42&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.43&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.40&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.41&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.39&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.40&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.38&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.39&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.37&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.38&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.36&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.37&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.35&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.36&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.34&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.35&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.33&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.34&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.32&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.33&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.31&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.32&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.30&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.31&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.29&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.30&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.28&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.29&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.27&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.28&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.26&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.27&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.25&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.26&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.24&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.25&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.23&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.24&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.22&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.23&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.21&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.22&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.20&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.21&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.19&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.20&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.18&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.17&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.18&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.15&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.16&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.14&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.15&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.13&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.14&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.12&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.13&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.11&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.12&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.10&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.11&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.9&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.10&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.8&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.9&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.7&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.8&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.6&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.7&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.5&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.3&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.4&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.2&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.3&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.1&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.2&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-text-field

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@7.0.0&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.1&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-text-field

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.17&sourceBranch=refs/tags/@confirmit/react-text-field@7.0.0&targetRepoId=1246) (2021-01-18)

### Features

- Show error message when exceeding max length ([NPM-573](https://jiraosl.firmglobal.com/browse/NPM-573)) ([a4eb0b5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a4eb0b5d2a36e2abe1d5ed774e7996051446cf94))

### BREAKING CHANGES

- If both maxLength and showMaxLength are specified, input will allow the value to exceed the maxLength, and show an error message instead.

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.16&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.15&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.14&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.13&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.12&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.11&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.10&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.9&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.6&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.3&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.2&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-text-field

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@6.0.1&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-text-field

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@5.0.1&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### Features

- add showMaxLength property ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([0713776](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0713776b978f3d297403602beca61bd42120ef08))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@5.0.1&sourceBranch=refs/tags/@confirmit/react-text-field@6.0.0&targetRepoId=1246) (2020-11-13)

### Features

- add showMaxLength property ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([0713776](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0713776b978f3d297403602beca61bd42120ef08))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.24&sourceBranch=refs/tags/@confirmit/react-text-field@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- add @confirmit/react-themes as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([bac8574](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bac85740887525b06c80a7cca1acdd8586e850d6))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [4.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.23&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.24&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([f2f735c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f2f735c81fd590c7feeae1796051ba5cafe945fb))

## [4.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.22&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.23&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.21&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.22&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.20&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.21&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.17&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.18&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.16&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.17&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.14&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.15&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.11&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.12&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.10&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.11&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.8&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.9&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-text-field@4.2.6&sourceBranch=refs/tags/@confirmit/react-text-field@4.2.7&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-field@4.2.4...@confirmit/react-text-field@4.2.5) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-text-field

## [4.2.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-field@4.2.2...@confirmit/react-text-field@4.2.3) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-text-field

# [4.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-field@4.1.1...@confirmit/react-text-field@4.2.0) (2020-08-12)

### Features

- Added `readOnly` prop

## [4.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-field@4.1.0...@confirmit/react-text-field@4.1.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-text-field

# [4.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-text-field@4.0.16...@confirmit/react-text-field@4.1.0) (2020-08-12)

### Features

- add `password` type ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([e553b93](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/e553b93a8a28e7c799ecdc0ce624dcd94120ceb4))

## CHANGELOG

### v4.0.8

- Fix: Inputs should have white background

### v4.0.0

- Refactor: restructure folder names in repository.

### v3.2.0

- Feat: Helper text is moved to inside a tooltip

### v3.1.0

- Removing package version in class names.

### v3.0.1

- Fix: Missed passing in `step`, `min`, `max` props down to SimpleTextField

### v3.0.0

- **BREAKING**:

  - Use new SimpleTextField and new InputWrapper component with updated DS styles
  - Removed props: `formControlClassName`, `borderClassName`, `icon`
  - Added props: `align` (this is for text alignment in the input), `prefix`, `suffix`

### v2.2.0

- Feat: Added min, max, step props

### v2.1.0

- Feat: Typescript-ify + Using new simple text field base component

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.1.0

- Feat: Now using transform transition for label animation

### v1.0.2

- Fix: Default theme position of icon
- Refactor: Change to static position for helptext
- Refactor: Remove unused styles for border

### v1.0.0

- **BREAKING**
  - Removing reserved space for errormessage, helptext, and label if none exists
  - Removing extra divs:
    - `.co*-text-input__form-control-value` with `data-text-field-component-form-control`
    - `.co*-text-input__form-control-value`
  - Removed props:
    - inActiveIcon
    - formControlClassName
  - Feat: Now using @confirmit/react-icons

### v0.4.4

- Fix: Added disabled style to label

### v0.4.3

- Feat: Added borderClassName prop

### v0.4.0

- **BREAKING**
  - Removed props: closeIcon, ErrorRenderer

### v0.3.0

- **BREAKING**
  - Removed props: baseClassName, classNames
  - Bugfix: label is now displayed when value is set to the number 0

### v0.2.1

- Adding onPaste

### v0.2.0

- Add new prop: icon. This renders an icon before the input

### v0.0.9

- Add asterisk to label if required property is set to true

### v0.0.1

- Initial version
