import React, {MutableRefObject} from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import user from '@testing-library/user-event';

import {TextField} from '../../src';
import Icon, {calendar} from '../../../icons/src/index';

describe('TextField', () => {
  it('should assign reference using RefObject', () => {
    const ref: MutableRefObject<HTMLInputElement | null> = {
      current: null,
    };

    render(<TextField ref={ref} onChange={jest.fn()} />);

    expect(ref.current?.tagName).toBe('INPUT');
  });

  it('should assign reference using CallbackRef', () => {
    let ref: HTMLInputElement | null = null;

    render(
      <TextField
        ref={(inputRef) => {
          ref = inputRef;
        }}
        onChange={jest.fn()}
      />
    );

    expect((ref as HTMLInputElement | null)?.tagName).toBe('INPUT');
  });

  it('clearing input should give it focus', () => {
    const onChange = jest.fn();
    const {getByPlaceholderText, getByTestId} = render(
      <TextField
        placeholder="Enter Text Field"
        onChange={onChange}
        showClear={true}
        value="test"
      />
    );

    const input = getByPlaceholderText('Enter Text Field');
    const clearButton = getByTestId('input-wrapper-clear');

    user.click(clearButton);

    expect(onChange).toHaveBeenCalledWith('');
    expect(input).toHaveFocus();
  });

  it('should add error class to elements', () => {
    const {container} = render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        error={true}
        label={'label'}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        required={true}
        onChange={jest.fn()}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--error').length
    ).toBe(1);
  });

  it('should add disabled class to container', () => {
    const {container} = render(
      <TextField
        label={'Label'}
        id={'story-text-field'}
        name={'story-text-field'}
        disabled={true}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        required={true}
        onChange={jest.fn()}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--disabled').length
    ).toBe(1);
    expect(
      container.querySelectorAll('.comd-simple-text-field--disabled').length
    ).toBe(1);
  });

  it('should add readOnly class to container', () => {
    const {container} = render(
      <TextField
        label={'Label'}
        id={'story-text-field'}
        name={'story-text-field'}
        readOnly={true}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        required={true}
        onChange={jest.fn()}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--readOnly').length
    ).toBe(1);
    expect(
      container.querySelectorAll('.comd-simple-text-field--readOnly').length
    ).toBe(1);
  });

  it('should only add readOnly class to container when readOnly and disabled are true', () => {
    const {container} = render(
      <TextField
        label={'Label'}
        id={'story-text-field'}
        name={'story-text-field'}
        disabled={true}
        readOnly={true}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        required={true}
        onChange={jest.fn()}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--readOnly').length
    ).toBe(1);
    expect(
      container.querySelectorAll('.comd-simple-text-field--readOnly').length
    ).toBe(1);
    expect(
      container.querySelectorAll('.comd-input-wrapper__label--disabled').length
    ).toBe(0);
    expect(
      container.querySelectorAll('.comd-simple-text-field--disabled').length
    ).toBe(0);
  });

  it('form label should display label when there is a value', () => {
    render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        label={'Label'}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
      />
    );

    expect(screen.getAllByText(/label/i).length).toBe(2);
  });

  it('when text is entered it should update component value', () => {
    let value = '';

    const onChange = jest.fn((val) => (value = val));

    render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        value={value}
        placeholder={'Enter Text Field'}
        onChange={onChange}
      />
    );

    const input = screen.getByTestId('simple-text-field');

    fireEvent.click(input);
    fireEvent.change(input, {
      target: {
        value: 'some text',
      },
    });

    expect(value).toBe('some text');
  });

  it('when a label is given it should always display if not explicitly hidden', () => {
    render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        showClear
        label={'Static Label'}
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
      />
    );

    expect(screen.getAllByText(/static label/i).length).toBe(2);

    fireEvent.click(screen.getByTestId('simple-text-field'));

    expect(screen.getAllByText(/static label/i).length).toBe(2);
  });

  it('should not display explicitly hidden label', () => {
    render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        showClear
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
      />
    );

    const input = screen.getByTestId('simple-text-field');

    fireEvent.click(input);

    expect(input.querySelectorAll('[data-input-wrapper-label]')).toHaveLength(
      0
    );
  });

  it('should display icon before input ', () => {
    const {container} = render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        prefix={<Icon path={calendar} />}
        showClear
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(
      container.querySelectorAll(
        '.comd-input-wrapper__prefix-decorator + [data-text-field-component-input]'
      ).length
    ).toBe(1);
    expect(icon.querySelector('path')).toHaveAttribute('d', calendar);
  });

  it('should display icon after input ', () => {
    const {container} = render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        suffix={<Icon path={calendar} />}
        showClear
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(
      container.querySelectorAll(
        '[data-text-field-component-input] + .comd-input-wrapper__suffix-decorator'
      ).length
    ).toBe(1);
    expect(icon.querySelector('path')).toHaveAttribute('d', calendar);
  });

  it('should shrink label when value is number 0', () => {
    const {container} = render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
        value={0}
        label={'This is the label'}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--shrink').length
    ).toBe(1);
  });

  it('should not shrink label when value not set', () => {
    const {container} = render(
      <TextField
        id={'story-text-field'}
        name={'story-text-field'}
        placeholder={'Enter Text Field'}
        onChange={jest.fn()}
        label={'This is the label'}
      />
    );

    expect(
      container.querySelectorAll('.comd-input-wrapper__label--shrink').length
    ).toBe(0);
  });

  describe('Max length :: ', () => {
    it('should not add maxLength by default', () => {
      render(
        <TextField
          label={'Label'}
          id={'story-text-field'}
          name={'story-text-field'}
          onChange={jest.fn()}
        />
      );

      const input = screen.getByTestId('simple-text-field');

      expect(input).not.toHaveProperty('maxlength');
    });

    it('should add maxLength prop to input when specified', () => {
      render(
        <TextField
          label={'Label'}
          id={'story-text-field'}
          name={'story-text-field'}
          maxLength={10}
          onChange={jest.fn()}
        />
      );

      const input = screen.getByTestId('simple-text-field');

      expect(input).toHaveAttribute('maxlength', '10');
    });

    it('should not add maxLength prop to input when specified, when showMaxLength is also enabled', () => {
      render(
        <TextField
          label={'Label'}
          id={'story-text-field'}
          name={'story-text-field'}
          maxLength={10}
          showMaxLength={true}
          onChange={jest.fn()}
        />
      );

      const input = screen.getByTestId('simple-text-field');

      expect(input).not.toHaveProperty('maxlength');
    });
  });
});
