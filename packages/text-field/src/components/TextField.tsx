import React, {forwardRef, useState, useRef, ForwardedRef} from 'react';

import SimpleTextField from '@jotunheim/react-simple-text-field';
import InputWrapper from '@jotunheim/react-input-wrapper';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

type TextFieldProps = {
  onChange: (val) => void;
  align?: 'left' | 'right';
  autoComplete?: boolean;
  autoFocus?: boolean;
  closeIcon?: React.ReactNode;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  helperText?: React.ReactNode;
  prefix?: React.ReactNode;
  id?: string;
  label?: string;
  max?: number;
  maxLength?: number;
  maxLengthErrorText?: string;
  showMaxLength?: boolean;
  min?: number;
  name?: string;
  onBlur?: React.FocusEventHandler;
  onFocus?: React.FocusEventHandler;
  onKeyDown?: React.KeyboardEventHandler;
  onKeyPress?: React.KeyboardEventHandler;
  onKeyUp?: React.KeyboardEventHandler;
  onPaste?: React.ClipboardEventHandler;
  placeholder?: string;
  required?: boolean;
  showClear?: boolean;
  step?: number;
  suffix?: React.ReactNode;
  type?: 'number' | 'text' | 'password' | 'search';
  value?: string | number;
};

const noop = () => {};

export const TextField = forwardRef(function TextField(
  {
    align,
    autoComplete,
    autoFocus,
    disabled,
    readOnly,
    error,
    helperText,
    id,
    label,
    max,
    maxLength,
    maxLengthErrorText,
    showMaxLength,
    min,
    name,
    onBlur = noop,
    onFocus = noop,
    onChange,
    onKeyDown,
    onKeyPress,
    onKeyUp,
    onPaste,
    placeholder,
    prefix = null,
    suffix = null,
    required,
    showClear = false,
    step,
    type,
    value = '',
    ...rest
  }: TextFieldProps,
  ref: ForwardedRef<HTMLInputElement>
) {
  const hasValue = value != null && String(value).length > 0;
  const [active, setActive] = useState(false);
  const inputRef = useRef<HTMLInputElement | null>(null);

  return (
    <InputWrapper
      active={active}
      disabled={disabled}
      readOnly={readOnly}
      error={error}
      helperText={helperText}
      label={label}
      maxLength={maxLength}
      maxLengthErrorText={maxLengthErrorText}
      showMaxLength={showMaxLength}
      prefix={prefix}
      required={required}
      onClear={() => {
        onChange('');
        inputRef?.current?.focus();
      }}
      showClear={showClear}
      suffix={suffix}
      value={value}
      htmlFor={id}
      {...extractDataAndAriaProps(rest)}>
      <SimpleTextField
        data-testid="simple-text-field"
        id={id}
        align={align}
        autoFocus={autoFocus}
        autoComplete={autoComplete}
        disabled={disabled}
        readOnly={readOnly}
        maxLength={showMaxLength ? undefined : maxLength}
        name={name}
        onChange={onChange}
        onBlur={(e) => {
          setActive(false);
          onBlur(e);
        }}
        onFocus={(e) => {
          setActive(true);
          onFocus(e);
        }}
        onKeyDown={onKeyDown}
        onKeyPress={onKeyPress}
        onKeyUp={onKeyUp}
        onPaste={onPaste}
        // If label is not active but has icon means label does not shrink
        // Make sure to show the placeholder in this scenario
        placeholder={!active && !!label && !prefix ? '' : placeholder}
        hasPrefix={!!prefix}
        hasSuffix={showClear && hasValue}
        min={min}
        max={max}
        ref={(localRef) => {
          inputRef.current = localRef;

          if (typeof ref === 'function') {
            ref(localRef);
          } else if (ref) {
            ref.current = localRef;
          }
        }}
        required={required}
        step={step}
        type={type}
        value={value}
        data-text-field-component-input
      />
    </InputWrapper>
  );
});

TextField.displayName = 'TextField';
