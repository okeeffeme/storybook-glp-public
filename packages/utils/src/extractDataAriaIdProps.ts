import extractProps from './extract-props';
import {AriaAttributes} from 'react';

export type DataAriaIdAttributes = AriaAttributes & {id?: string};

export default function extractDataAriaIdProps(props): DataAriaIdAttributes {
  return extractProps(props, /^(data|aria)-|^id$/);
}
