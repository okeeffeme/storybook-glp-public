import React from 'react';

import AuthorizationContext from './authorization-context';

export default (Component, authorization) => {
  const ComponentWithAuthorization = props => {
    return (
      <AuthorizationContext.Provider value={authorization}>
        <Component {...props} />
      </AuthorizationContext.Provider>
    );
  };

  return ComponentWithAuthorization;
};
