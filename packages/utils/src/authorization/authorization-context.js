import {createContext} from 'react';

export default createContext({
  accessToken: null,
});
