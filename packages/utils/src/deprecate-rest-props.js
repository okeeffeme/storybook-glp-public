import deprecationNotice from './deprecation-notice';

export default (componentName, invalidProps = [], allowedProps = []) => {
  /* eslint-disable no-undef */
  if (process.env.NODE_ENV !== 'production') {
    if (invalidProps && invalidProps.length > 0) {
      deprecationNotice(`
Passing props of '${componentName}' component to inner elements will be limited in one of the next major releases.
Consider removing properties: ${invalidProps.join(
        ', '
      )} from element or add properties support.
Allowed props: aria-, data-, ${allowedProps.join(', ')}.
`);
    }
  }
};
