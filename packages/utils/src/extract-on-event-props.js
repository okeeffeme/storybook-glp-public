import extractProps from './extract-props';

export default props => {
  return extractProps(props, /^on[A-Z].*/);
};
