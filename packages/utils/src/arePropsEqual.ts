/*
  Can be used as second parameter of React.memo
 */
export function arePropsEqual<P extends Record<string, unknown>>(
  warnIfChangedProps: (keyof P)[]
) {
  return (prevProps: P, nextProps: P) => {
    return Object.keys(nextProps).every((key) => {
      const propName = key as keyof P;

      if (nextProps[propName] !== prevProps[propName]) {
        if (warnIfChangedProps.includes(propName)) {
          console.warn(
            `Property "${propName}" should be referentially the same across renders`
          );
        }
        return false;
      }
      return true;
    });
  };
}
