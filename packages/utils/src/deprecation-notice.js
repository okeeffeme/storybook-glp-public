const shownMessages = [];

export default notice => {
  /* eslint-disable no-undef */
  /* dont output these warnings in production
     and keep track of already shown messages to reduce noise */
  if (
    !shownMessages.includes(notice) &&
    process.env.NODE_ENV !== 'production'
  ) {
    shownMessages.push(notice);
    /* eslint-disable no-console*/
    console.warn(`
DEPRECATION NOTICE:
${notice}`);
  }
};
