export default function isWithinNode(
  parentNode?: HTMLElement | null,
  targetNode?: HTMLElement | null
) {
  if (!parentNode) return null;

  let node: (Node & ParentNode) | null | undefined = targetNode;
  while (node) {
    if (node === parentNode) {
      return true;
    }
    node = node.parentNode;
  }
  return false;
}
