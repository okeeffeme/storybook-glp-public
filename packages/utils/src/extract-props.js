export default (props, regex) => {
  return Object.keys(props).reduce((nextProps, prop) => {
    if (prop.match(regex)) nextProps[prop] = props[prop];

    return nextProps;
  }, {});
};
