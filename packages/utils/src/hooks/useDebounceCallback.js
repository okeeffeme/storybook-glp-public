import {useEffect, useCallback, useRef} from 'react';

export default function useDebounceCallback(callback, wait, options = {}) {
  const timeout = useRef(null);
  const lastDebounce = useRef(null);
  const {leading = false, trailing = true} = options;

  // cleans up pending timeouts when the function changes
  useEffect(
    () => () => {
      if (timeout.current !== null) {
        clearTimeout(timeout.current);
      }
    },
    [callback, wait]
  );

  return useCallback(
    function (...args) {
      if (leading && timeout.current === null) {
        callback.apply(null, args);
      } else {
        clearTimeout(timeout.current);
        // Assign args to lastDebounce so we know the function is debounced at least once.
        lastDebounce.current = args;
      }

      timeout.current = setTimeout(() => {
        // only invoke trailing call if lastArgs has been debounced at least once
        if (trailing && lastDebounce.current !== null) {
          callback.apply(null, args);
          lastDebounce.current = null;
        }

        timeout.current = null;
      }, wait);
    },
    [callback, leading, trailing, wait]
  );
}
