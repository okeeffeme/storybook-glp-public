import {useEffect, useRef} from 'react';

/*
 * Hook calls callback in a loop of requestAnimationFrame calls until timout has reached.
 * If timeout has reached it lets the latest requestAnimationFrame to be finished.
 * Useful if you need to adjust node styles during animation. See InlineEdit.
 * Make sure callback is memoized with useCallback.
 * If callback reference is changed, then hooks starts animation frame loop.
 * As the dependency of the useCallback you may use some changing state of component like "isEditing", "isOpen"
 * which triggers the animation.
 * */
export default function useRequestAnimationFrames(
  callback: () => void,
  timeout: number
) {
  const animationFrameId = useRef<number | undefined>(undefined);
  const timeoutId = useRef<number | undefined>(undefined);

  useEffect(() => {
    let shouldCancel = false;
    const animationLoop = () => {
      animationFrameId.current = window.requestAnimationFrame(() => {
        callback();

        if (!shouldCancel) {
          animationLoop();
        }
      });
    };

    animationLoop();

    timeoutId.current = window.setTimeout(() => {
      /* Instead of using cancelAnimationFrame it lets the current animation frame to be finished. */
      shouldCancel = true;
    }, timeout);

    return () => {
      if (animationFrameId.current) {
        cancelAnimationFrame(animationFrameId.current);
      }
      if (timeoutId.current) {
        clearTimeout(timeoutId.current);
      }
    };
  }, [callback, timeout]);
}
