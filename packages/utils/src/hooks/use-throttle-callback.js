import {useCallback, useEffect, useRef} from 'react';

export default function useThrottleCallback(callback, wait = 0, options = {}) {
  const {leading = true} = options;
  const timeout = useRef(null);
  const lastTimeout = useRef(null);
  const isLeadingCalled = useRef(false);

  // effect to clean up timeouts when callback or wait changes
  useEffect(() => {
    if (timeout.current !== null) {
      clearTimeout(timeout.current);
      timeout.current = null;
    }

    if (lastTimeout.current !== null) {
      clearTimeout(lastTimeout.current);
      lastTimeout.current = null;
    }

    isLeadingCalled.current = false;
  }, [callback, wait]);

  return useCallback(
    (...args) => {
      // first interval
      if (timeout.current === null) {
        const invokeCb = () => {
          timeout.current = null;
          if (lastTimeout.current === null) {
            isLeadingCalled.current = false;
          }
          callback.apply(this, args);
        };

        // invoke only if leading is true and has yet to be called
        if (leading && !isLeadingCalled.current) {
          invokeCb();
          isLeadingCalled.current = true;
        } else {
          timeout.current = setTimeout(invokeCb, wait);
        }
      } else {
        if (lastTimeout.current !== null) {
          clearTimeout(lastTimeout.current);
        }
        lastTimeout.current = setTimeout(() => {
          isLeadingCalled.current = false;
          lastTimeout.current = null;
          if (timeout.current === null) {
            callback.apply(this, args);
          }
        }, wait);
      }
    },
    [callback, leading, wait]
  );
}
