import React from 'react';

/**
 * @param type A case-sensitive string representing the event type to listen for.
 * @param listener The object that receives a notification (an object that implements the Event interface) when an event of the specified type occurs. This must be an object implementing the EventListener interface, or a JavaScript function. See The event listener callback for details on the callback itself.
 */
export const useWindowEvent = (type: string, listener: (e: Event) => void) => {
  React.useEffect(() => {
    window.addEventListener(type, listener);

    return () => window.removeEventListener(type, listener);
  }, [type, listener]);
};
