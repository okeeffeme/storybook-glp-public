import React from 'react';

export default (props, changedCallback, everyCallback = () => {}) => {
  const prevProps = React.useRef(props);

  React.useLayoutEffect(() => {
    if (process.env.NODE_ENV !== 'production') {
      const keys = Object.keys(props);

      keys.forEach((key) => {
        if (props[key] !== prevProps.current[key]) {
          changedCallback(key, props[key], prevProps.current[key]);
        }

        everyCallback(key, props[key], prevProps.current[key]);
      });
    }

    prevProps.current = props;
  }, [props, changedCallback, everyCallback]);
};
