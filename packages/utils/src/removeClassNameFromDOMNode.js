import {splitClassName} from './utils/splitClassName';

export function removeClassNameFromDOMNode(node, className) {
  if (!className) return;

  splitClassName(className).forEach(className =>
    node.classList.remove(className)
  );
}
