export default props => {
  return Object.keys(props)
    .filter(prop => {
      return !prop.match(/^(data|aria)-/);
    })
    .reduce(
      (nextProps, prop) => ({
        ...nextProps,
        [prop]: props[prop],
      }),
      {}
    );
};
