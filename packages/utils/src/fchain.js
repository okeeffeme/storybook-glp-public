export default (...funcs) => {
  return funcs
    .filter(f => !!f)
    .reduce((acc, f) => {
      if (!acc) return f;

      return (...args) => {
        acc.apply(this, args);
        f.apply(this, args);
      };
    }, null);
};
