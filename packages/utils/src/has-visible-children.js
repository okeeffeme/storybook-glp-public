import {Children} from 'react';

export default function hasVisibleChildren(children) {
  return Children.toArray(children).some(kid => {
    if (kid.props && kid.props.children) {
      return hasVisibleChildren(kid.props.children);
    }

    return !!kid;
  });
}
