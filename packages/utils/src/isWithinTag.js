/* istanbul ignore next */
export default function isWithinTag(tagName, targetNode) {
  let node = targetNode;
  while (node) {
    if (node.tagName && node.tagName.toUpperCase() === tagName.toUpperCase()) {
      return true;
    }
    node = node.parentNode;
  }
  return false;
}
