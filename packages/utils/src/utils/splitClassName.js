export function splitClassName(className) {
  return className.split(' ').filter(Boolean);
}
