/**
 * Remove all the keys that A and B have in common,
 * and set all the remaining keys of A to optional undefined.
 */
type RemoveSharedKeysAndNegateNonSharedKeys<
  A extends Record<string, unknown>,
  B extends Record<string, unknown>
> = {[k in Exclude<keyof A, keyof B>]?: never};

/**
 * Keep all the keys that A and B have in common.
 * For the keys A and B don't have in common,
 * prevent mixing keys of A and keys of B.
 */
export type Either<
  A extends Record<string, unknown>,
  B extends Record<string, unknown>
> =
  | (RemoveSharedKeysAndNegateNonSharedKeys<A, B> & B)
  | (RemoveSharedKeysAndNegateNonSharedKeys<B, A> & A);
