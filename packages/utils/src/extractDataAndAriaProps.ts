import extractProps from './extract-props';
import {AriaAttributes} from 'react';

export default function extractDataAndAriaProps(props): AriaAttributes {
  return extractProps(props, /^(data|aria)-/);
}
