import {splitClassName} from './utils/splitClassName';

export function addClassNameToDOMNode(node, className) {
  if (!className) return;

  splitClassName(className).forEach(className => node.classList.add(className));
}
