export default Component =>
  Component.displayName ||
  Component.name ||
  (Component.constructor &&
    (Component.constructor.displayName || Component.constructor.name)) ||
  '<component>';
