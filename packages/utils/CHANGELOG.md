# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [4.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-utils@4.1.1&sourceBranch=refs/tags/@jotunheim/react-utils@4.2.0&targetRepoId=1246) (2023-03-30)

### Features

- add new utils file either.ts that export Either utility type ([NPM-1289](https://jiraosl.firmglobal.com/browse/NPM-1289)) ([5169551](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/516955137c84404e238f5b8c15ea69e2fd081183))

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-utils@4.1.0&sourceBranch=refs/tags/@jotunheim/react-utils@4.1.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- add types to extractDataAndAriaProps and extractDataAriaIdProps ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([c1f8bce](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1f8bce56b36a13fbebb4529a4948cc810da45e8))

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-utils@4.0.0&sourceBranch=refs/tags/@jotunheim/react-utils@4.1.0&targetRepoId=1246) (2022-10-11)

### Features

- add arePropsEqual ([NPM-1093](https://jiraosl.firmglobal.com/browse/NPM-1093)) ([25965f8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/25965f83abbeb56b37c103bbe7a24bcb202f0b50))

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-utils@3.2.0&sourceBranch=refs/tags/@confirmit/react-utils@3.2.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [3.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-utils@3.1.1&sourceBranch=refs/tags/@confirmit/react-utils@3.2.0&targetRepoId=1246) (2022-04-06)

### Features

- Add useUuid hook ([NPM-994](https://jiraosl.firmglobal.com/browse/NPM-994)) ([15631ba](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/15631ba60903ab9fed21418ec74d4a3520fc5968))

## [3.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-utils@3.1.0&sourceBranch=refs/tags/@confirmit/react-utils@3.1.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-utils@3.0.0&sourceBranch=refs/tags/@confirmit/react-utils@3.1.0&targetRepoId=1246) (2020-09-28)

### Features

- useWindowEvent hook, to easily attach event listeners to window ([NPM-523](https://jiraosl.firmglobal.com/browse/NPM-523)) ([4fba773](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4fba77396451c2ee02c45f091cdfb0b980cb9040)

# [3.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-utils@2.0.0...@confirmit/react-utils@3.0.0) (2020-08-12)

### Features

- add useRequestAnimationFrame hook ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([61e7574](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/61e7574a5960e0bc2cbc0ecba08183f833b6ae04))

### BREAKING CHANGES

- - remove mergePopperModfiers

* isWithinNode does not look for parentPortalNode property of DOM node

## CHANGELOG

### v2.0.0

- Refactor: new name: `@confirmit/react-utils`

### v1.7.0

- Feat: Add 'useThrottleCallback' React hook

### v1.6.2

- Fix: Bug in splitClassName caused addClassNameToDOMNode to not work at all

### v1.5.0

- Feat: Add 'useDebounceCallback' and 'useForceUpdate' React hooks

### v1.4.2

- Fix: `excludeDataAndAriaProps` did not return an object with non-aria and data props

### v1.4.0

- Feat: Add `extractOnEventProps` which extracts on\* props which can be used to apply those events easily to components when passing in `...rest` props f.ex
- Fix: `extractDataAriaIdProps` did not extract id prop

### v1.3.0

- Feat: Add `useRenderTracking` hook, to easily log reasons for why a component re-renders

### v1.2.0

- Feat: Add `getDisplayName` to get the display name of the React component

### v1.1.0

- Feat: Added "hasVisibleChildren" function, for determining whether any of the children are current visible.

### v1.0.0

- **BREAKING**
  - Babel 7 used for transpilation
- Feat: Keep track of messages that are already shown to reduce noise. For identical messages used with different versions/instances of this util, you might still get duplicate messages.

### v0.2.2

- Feat: Add `developerNotice` which displays a message in the console for code not compiled for production mode. Messages will be prefixed with _DEVELOPER NOTICE_, and can for instance be used to indicate that a package is still under development and that the API may change.

### v0.2.0

- Feat: Add `extractDataAriaIdProps` which extracts data-_, aria-_ and id=\* props which can be used to apply those attributes easily to components when passing in `...rest` props f.ex

### v0.1.0

- Initial release
