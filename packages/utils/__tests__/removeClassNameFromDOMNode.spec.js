import {removeClassNameFromDOMNode} from '../src/removeClassNameFromDOMNode';

let fakeDomNode;
describe('addClassNameToDOMNode', () => {
  beforeEach(() => {
    fakeDomNode = {
      classList: {
        remove: jest.fn(() => {}),
      },
    };
  });

  it('should remove a single className to DOM node', () => {
    removeClassNameFromDOMNode(fakeDomNode, 'my-class');

    expect(fakeDomNode.classList.remove.mock.calls.length).toBe(1);
    expect(fakeDomNode.classList.remove.mock.calls[0][0]).toBe('my-class');
  });

  it('should remove two classNames to DOM node when split by space', () => {
    removeClassNameFromDOMNode(fakeDomNode, 'my-first-class my-second-class');

    expect(fakeDomNode.classList.remove.mock.calls.length).toBe(2);
    expect(fakeDomNode.classList.remove.mock.calls[0][0]).toBe(
      'my-first-class'
    );
    expect(fakeDomNode.classList.remove.mock.calls[1][0]).toBe(
      'my-second-class'
    );
  });

  it('should remove zero classNames to DOM node when called with empty string', () => {
    removeClassNameFromDOMNode(fakeDomNode, '');

    expect(fakeDomNode.classList.remove.mock.calls.length).toBe(0);
  });

  it('should remove zero classNames to DOM node when called with space', () => {
    removeClassNameFromDOMNode(fakeDomNode, ' ');

    expect(fakeDomNode.classList.remove.mock.calls.length).toBe(0);
  });
});
