import {arePropsEqual} from '../src/arePropsEqual';

describe('arePropsEqual', () => {
  const warnIfChangedProps = ['onItemChanged', 'items'];

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should be positive if prevProps and nextProps are the same object', () => {
    const prevProps = {
      propA: {},
      onItemChanged: jest.fn(),
      items: [],
    };

    expect(arePropsEqual(warnIfChangedProps)(prevProps, prevProps)).toBe(true);
  });

  it('should be positive if prevProps and nextProps have all the same props as references', () => {
    const prevProps = {
      propA: {},
      onItemChanged: jest.fn(),
      items: [],
    };
    const nextProps = {
      ...prevProps,
    };

    expect(arePropsEqual(warnIfChangedProps)(prevProps, nextProps)).toBe(true);
  });

  it('should be negative if prevProps and nextProps are different but no warnIfChangedProps changed', () => {
    const logSpy = jest.spyOn(console, 'warn');
    const prevProps = {
      propA: {},
      onItemChanged: jest.fn(),
      items: [],
    };
    const nextProps = {
      ...prevProps,
      propA: {},
    };

    expect(arePropsEqual(warnIfChangedProps)(prevProps, nextProps)).toBe(false);
    expect(logSpy).not.toBeCalled();
  });

  it('should be negative if prevProps and nextProps are different, should put warning for first warnIfChangedProps (onItemChanged)', () => {
    const logSpy = jest.spyOn(console, 'warn').mockImplementation();
    const prevProps = {
      propA: {},
      onItemChanged: jest.fn(),
      items: [],
    };
    const nextProps = {
      ...prevProps,
      onItemChanged: jest.fn(),
      items: [],
    };

    expect(arePropsEqual(warnIfChangedProps)(prevProps, nextProps)).toBe(false);
    expect(logSpy).toBeCalledTimes(1);
    expect(logSpy).toBeCalledWith(
      'Property "onItemChanged" should be referentially the same across renders'
    );
  });

  it('should be negative if prevProps and nextProps are different, should put warning for first warnIfChangedProps (items)', () => {
    const logSpy = jest.spyOn(console, 'warn').mockImplementation();
    const prevProps = {
      propA: {},
      items: [],
      onItemChanged: jest.fn(),
    };
    const nextProps = {
      ...prevProps,
      items: [],
      onItemChanged: jest.fn(),
    };

    expect(arePropsEqual(warnIfChangedProps)(prevProps, nextProps)).toBe(false);
    expect(logSpy).toBeCalledTimes(1);
    expect(logSpy).toBeCalledWith(
      'Property "items" should be referentially the same across renders'
    );
  });
});
