import fchain from '../src/fchain';

describe('fchain', () => {
  it('should call every function in the chain', () => {
    const f1 = jest.fn();
    const f2 = jest.fn();
    const f3 = jest.fn();

    const acc = fchain(f1, f2, f3);
    const args = {};

    acc(args);

    expect(f1).toBeCalledWith(args);
    expect(f2).toBeCalledWith(args);
    expect(f3).toBeCalledWith(args);
  });
});
