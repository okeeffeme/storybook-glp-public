import React from 'react';
import {shallow} from 'enzyme';
import {useDebounceCallback} from '../src';

jest.useFakeTimers();

const DEBOUNCE_TIMER = 1000;
const CALLS_TO_RUN = 10;

const onChange = jest.fn();

function DebounceCallbackTestComponent(props) {
  // eslint-disable-next-line react/prop-types
  const {leading, trailing, delay = DEBOUNCE_TIMER} = props;
  const onChangeHandler = useDebounceCallback(onChange, delay, {
    leading,
    trailing,
  });

  return <input onChange={onChangeHandler} />;
}

function fireCallbacksByIntervals(
  callback,
  intervalGroupSize,
  intervalSize,
  delayBetweenCallback,
  delayBetweenIntervals
) {
  for (
    let intervalGroup = 0;
    intervalGroup < intervalGroupSize;
    intervalGroup++
  ) {
    for (let interval = 0; interval < intervalSize; interval++) {
      callback();
      jest.advanceTimersByTime(delayBetweenCallback);
    }
    jest.advanceTimersByTime(delayBetweenIntervals);
  }
}

describe('test useDebounceCallback hook', () => {
  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('should call once at leading with leading = true and trailing = true and called once', async () => {
    const props = {
      leading: true,
      trailing: true,
    };

    let wrapper;
    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    onChangeHandler();

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should debounce when leading = true and trailing = true with multiple calls', async () => {
    const props = {
      leading: true,
      trailing: true,
    };

    let wrapper;
    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER - 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(2);

    jest.clearAllMocks();
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER);
    }

    expect(onChange).toHaveBeenCalledTimes(10);

    jest.clearAllMocks();

    fireCallbacksByIntervals(
      onChangeHandler,
      4,
      4,
      DEBOUNCE_TIMER - 1,
      DEBOUNCE_TIMER + 1
    );
    expect(onChange).toHaveBeenCalledTimes(8);
  });

  it('should debounce to a single call with leading = true and trailing = false and called once', async () => {
    const props = {
      leading: true,
      trailing: false,
    };
    let wrapper;

    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    onChangeHandler();
    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should debounce when leading = true and trailing = false with multiple function calls', async () => {
    const props = {
      leading: true,
      trailing: false,
    };

    let wrapper;
    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER - 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);

    jest.clearAllMocks();
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER);
    }

    expect(onChange).toHaveBeenCalledTimes(10);

    jest.clearAllMocks();

    fireCallbacksByIntervals(
      onChangeHandler,
      4,
      4,
      DEBOUNCE_TIMER - 1,
      DEBOUNCE_TIMER + 1
    );
    expect(onChange).toHaveBeenCalledTimes(4);
  });

  it('should debounce to a single call with leading = false and trailing = true and called once', async () => {
    const props = {
      leading: false,
      trailing: true,
    };
    let wrapper;

    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    onChangeHandler();

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should debounce with leading = false and trailing = true with multiple function calls', async () => {
    const props = {
      leading: false,
      trailing: true,
    };

    let wrapper;
    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER - 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);

    jest.clearAllMocks();

    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(DEBOUNCE_TIMER);
    }

    expect(onChange).toHaveBeenCalledTimes(10);

    jest.clearAllMocks();

    fireCallbacksByIntervals(
      onChangeHandler,
      4,
      4,
      DEBOUNCE_TIMER - 1,
      DEBOUNCE_TIMER + 1
    );
    expect(onChange).toHaveBeenCalledTimes(4);
  });

  it('should debounce to next tick with delay = 0', async () => {
    const props = {
      leading: false,
      trailing: true,
      delay: 0,
    };

    let wrapper;

    wrapper = shallow(<DebounceCallbackTestComponent {...props} />);
    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(10);

    jest.clearAllMocks();

    fireCallbacksByIntervals(
      onChangeHandler,
      4,
      4,
      DEBOUNCE_TIMER - 1,
      DEBOUNCE_TIMER + 1
    );
    expect(onChange).toHaveBeenCalledTimes(16);
  });
});
