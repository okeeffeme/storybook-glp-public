import excludeDataAndAriaProps from '../src/exclude-data-and-aria-props';

describe('excludeDataAndAriaProps', () => {
  it('should return an object containing all non-data and aria props', () => {
    const props = excludeDataAndAriaProps({
      test: 1,
      foo: 'bar',
      'data-test': 1,
      'aria-foo': 'bar',
    });

    expect(props).toEqual({
      test: 1,
      foo: 'bar',
    });
  });
});
