import React from 'react';
import {shallow} from 'enzyme';
import {useThrottleCallback} from '../src';

jest.useFakeTimers();

const THROTTLE_TIMER = 1000;
const CALLS_TO_RUN = 10;

const onChange = jest.fn();

function ThrottleCallbackTestComponent(props) {
  // eslint-disable-next-line react/prop-types
  const {leading, delay = THROTTLE_TIMER} = props;
  const onChangeHandler = useThrottleCallback(onChange, delay, {
    leading,
  });

  return <input onChange={onChangeHandler} />;
}

describe('test useThrottleCallback hook', () => {
  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('should call once with leading = true', async () => {
    const props = {
      leading: false,
    };

    let wrapper;
    wrapper = shallow(<ThrottleCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    onChangeHandler();

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should throttle when leading = false', async () => {
    const props = {
      leading: false,
    };

    let wrapper;
    wrapper = shallow(<ThrottleCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(THROTTLE_TIMER - 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(6);

    jest.clearAllMocks();
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(THROTTLE_TIMER + 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(10);
  });

  it('should throttle when leading = true', async () => {
    const props = {
      leading: true,
    };

    let wrapper;
    wrapper = shallow(<ThrottleCallbackTestComponent {...props} />);

    let onChangeHandler = wrapper.find('input').props().onChange;
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(THROTTLE_TIMER - 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(8);

    jest.clearAllMocks();
    for (let i = 0; i < CALLS_TO_RUN; i++) {
      onChangeHandler();
      jest.advanceTimersByTime(THROTTLE_TIMER + 1);
    }

    jest.runAllTimers();
    expect(onChange).toHaveBeenCalledTimes(10);
  });
});
