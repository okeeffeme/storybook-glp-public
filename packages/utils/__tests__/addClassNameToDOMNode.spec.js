import {addClassNameToDOMNode} from '../src/addClassNameToDOMNode';

let fakeDomNode;
describe('addClassNameToDOMNode', () => {
  beforeEach(() => {
    fakeDomNode = {
      classList: {
        add: jest.fn(() => {}),
      },
    };
  });

  it('should add a single className to DOM node', () => {
    addClassNameToDOMNode(fakeDomNode, 'my-class');

    expect(fakeDomNode.classList.add.mock.calls.length).toBe(1);
    expect(fakeDomNode.classList.add.mock.calls[0][0]).toBe('my-class');
  });

  it('should add two classNames to DOM node when split by space', () => {
    addClassNameToDOMNode(fakeDomNode, 'my-first-class my-second-class');

    expect(fakeDomNode.classList.add.mock.calls.length).toBe(2);
    expect(fakeDomNode.classList.add.mock.calls[0][0]).toBe('my-first-class');
    expect(fakeDomNode.classList.add.mock.calls[1][0]).toBe('my-second-class');
  });

  it('should add zero classNames to DOM node when called with empty string', () => {
    addClassNameToDOMNode(fakeDomNode, '');

    expect(fakeDomNode.classList.add.mock.calls.length).toBe(0);
  });

  it('should add zero classNames to DOM node when called with space', () => {
    addClassNameToDOMNode(fakeDomNode, ' ');

    expect(fakeDomNode.classList.add.mock.calls.length).toBe(0);
  });
});
