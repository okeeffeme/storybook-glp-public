import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {Link} from '../../src/components/Link';

describe('Jotunheim React Link :: ', () => {
  it('renders link with content and default style', () => {
    render(<Link href="https://link.com">link</Link>);

    const link = screen.getByText(/link/i);
    const a = screen.getByRole('link');

    expect(a?.classList.contains('comd-link--default')).toBe(true);
    expect(a?.classList.contains('comd-link--monochrome')).toBe(false);
    expect(a?.classList.contains('comd-link--monochrome-table')).toBe(false);
    expect(link.classList.contains('comd-link__content')).toBe(true);
    expect(link.classList.contains('comd-link__decorator')).toBe(false);
  });

  it('renders new tab icon in suffix when target=blank', () => {
    render(
      <Link href="https://link.com" target="_blank">
        link
      </Link>
    );

    expect(screen.getAllByRole('link')).toHaveLength(1);
  });

  it('should call onClick if clicked on SimpleLink', () => {
    const onClick = jest.fn();

    render(
      <Link data-testid="link" href="https://link.com" onClick={onClick}>
        link
      </Link>
    );

    userEvent.click(screen.getByTestId('link'));

    expect(onClick).toBeCalled();
  });

  it('should call onMouseEnter if mouse entered on SimpleLink', () => {
    const onMouseEnter = jest.fn();
    render(
      <Link
        data-testid="link"
        href="https://link.com"
        onMouseEnter={onMouseEnter}>
        link
      </Link>
    );
    userEvent.hover(screen.getByTestId('link'));

    expect(onMouseEnter).toBeCalled();
  });

  it('should call onMouseLeave if mouse left SimpleLink', () => {
    const onMouseLeave = jest.fn();

    render(
      <Link
        data-testid="link"
        href="https://link.com"
        onMouseLeave={onMouseLeave}>
        link
      </Link>
    );
    userEvent.unhover(screen.getByTestId('link'));
    expect(onMouseLeave).toBeCalled();
  });
});
