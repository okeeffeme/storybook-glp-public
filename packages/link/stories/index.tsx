import React, {useEffect} from 'react';
import {storiesOf} from '@storybook/react';
import {number, optionsKnob} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Link, LinkAppearances} from '../src';
import {HistoryContext} from '../../contexts/src';
import {createBrowserHistory} from 'history';
import Tooltip from '../../tooltip/src';
import Truncate from '../../truncate/src';

const browserHistory = createBrowserHistory();

const someColors = {
  black: '#000000',
  blue: '#cceeff',
  red: '#eb0052',
  yellow: '#ffd600',
};

storiesOf('Components/link', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Links appearances', () => {
    const textWithLink = (link) => (
      <div
        style={{
          fontSize: number('font size', 14, {min: 10, max: 30, step: 2}),
          color: optionsKnob('text color', someColors, someColors.black, {
            display: 'select',
          }),
        }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit {link}, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </div>
    );
    return (
      <div style={{padding: '20px'}}>
        {Object.keys(LinkAppearances).map((appearance) => {
          const sameTabLink = (
            <Link href="" appearance={LinkAppearances[appearance]}>
              Here comes the {appearance} link
            </Link>
          );
          const newTabLink = (
            <Link
              target="_blank"
              href="https://confirmit.com"
              appearance={LinkAppearances[appearance]}>
              And here comes the {appearance} link with `target="_blank"`
            </Link>
          );
          return (
            <div key={appearance} style={{marginBottom: '24px'}}>
              <h1>Appearance: {appearance}</h1>
              <div>
                {textWithLink(sameTabLink)}
                {textWithLink(newTabLink)}
              </div>
            </div>
          );
        })}
      </div>
    );
  })
  .add('With History', () => {
    useEffect(() => {
      const dispose = browserHistory.listen((e) => {
        console.log(e);
      });

      return () => {
        dispose();
      };
    });

    return (
      <HistoryContext.Provider value={browserHistory}>
        <Link href="/not-found" appearance={LinkAppearances.default}>
          Internal Link
        </Link>
        <Link href="http://google.com" appearance={LinkAppearances.monochrome}>
          External Link
        </Link>
      </HistoryContext.Provider>
    );
  })
  .add('With Tooltip', () => {
    return (
      <Tooltip content="Some tooltip">
        <Link href="http://google.com" appearance={LinkAppearances.default}>
          Google it!
        </Link>
      </Tooltip>
    );
  })
  .add('Truncated link', () => {
    return (
      <>
        <div style={{marginBottom: '20px'}}>
          <h1>100px width with tooltip</h1>
          <div style={{width: '100px'}}>
            <Tooltip content="Some tooltip">
              <Truncate lines={1}>
                <Link href="#" appearance={LinkAppearances.default}>
                  Some really long text for the link that will get truncated
                  because the container is small, and it is wrapped in Truncate
                </Link>
              </Truncate>
            </Tooltip>
          </div>
        </div>

        <div style={{marginBottom: '20px'}}>
          <h1>100px width with tooltip and 3 lines</h1>
          <div style={{width: '100px'}}>
            <Tooltip content="Some tooltip">
              <Truncate lines={3}>
                <Link href="#" appearance={LinkAppearances.default}>
                  Some really long text for the link that will get truncated
                  because the container is small, and it is wrapped in Truncate
                </Link>
              </Truncate>
            </Tooltip>
          </div>
        </div>
      </>
    );
  });
