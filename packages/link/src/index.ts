import Link from './components/Link';
import {LinkAppearances} from './components/LinkAppearances';

export {Link, LinkAppearances};
export default Link;
