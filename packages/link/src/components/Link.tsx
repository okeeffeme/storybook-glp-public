import React, {
  AriaAttributes,
  forwardRef,
  MouseEventHandler,
  ReactNode,
  Ref,
} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Icon, {openInNew} from '@jotunheim/react-icons';
import SimpleLink from '@jotunheim/react-simple-link';

import {LinkAppearances} from './LinkAppearances';
import classNames from './Link.module.css';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-link',
  classNames,
});

type LinkProps = {
  appearance?: LinkAppearances;
  children: ReactNode | ReactNode[];
  href: string;
  target?: string;
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  onMouseLeave?: MouseEventHandler<HTMLAnchorElement>;
  onMouseEnter?: MouseEventHandler<HTMLAnchorElement>;
} & AriaAttributes;

export const Link = forwardRef(
  (
    {
      appearance = LinkAppearances.default,
      children,
      href,
      target,
      onClick,
      onMouseLeave,
      onMouseEnter,
      ...rest
    }: LinkProps,
    ref: Ref<HTMLAnchorElement>
  ) => {
    const classes = cn(block(), modifier(appearance));

    const linkProps = {
      className: classes,
      href,
      target,
      onClick,
      onMouseEnter,
      onMouseLeave,
      rel: 'noopener noreferrer',
      ...extractDataAriaIdProps(rest),
    };

    return (
      <SimpleLink data-testid="link" ref={ref} {...linkProps}>
        <span className={element('content')}>
          {children}
          {target === '_blank' && (
            <span className={cn(element('decorator'))}>
              &#8203;
              <Icon path={openInNew} />
            </span>
          )}
        </span>
      </SimpleLink>
    );
  }
);

Link.displayName = 'Link';

export default Link;
