# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.37&sourceBranch=refs/tags/@jotunheim/react-link@3.0.37&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.35&sourceBranch=refs/tags/@jotunheim/react-link@3.0.36&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.34&sourceBranch=refs/tags/@jotunheim/react-link@3.0.35&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.33&sourceBranch=refs/tags/@jotunheim/react-link@3.0.34&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.32&sourceBranch=refs/tags/@jotunheim/react-link@3.0.33&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.31&sourceBranch=refs/tags/@jotunheim/react-link@3.0.32&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.30&sourceBranch=refs/tags/@jotunheim/react-link@3.0.31&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [3.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.29&sourceBranch=refs/tags/@jotunheim/react-link@3.0.30&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.28&sourceBranch=refs/tags/@jotunheim/react-link@3.0.29&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.27&sourceBranch=refs/tags/@jotunheim/react-link@3.0.28&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.26&sourceBranch=refs/tags/@jotunheim/react-link@3.0.27&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.25&sourceBranch=refs/tags/@jotunheim/react-link@3.0.26&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.23&sourceBranch=refs/tags/@jotunheim/react-link@3.0.25&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.23&sourceBranch=refs/tags/@jotunheim/react-link@3.0.24&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.22&sourceBranch=refs/tags/@jotunheim/react-link@3.0.23&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.21&sourceBranch=refs/tags/@jotunheim/react-link@3.0.22&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.19&sourceBranch=refs/tags/@jotunheim/react-link@3.0.21&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.19&sourceBranch=refs/tags/@jotunheim/react-link@3.0.20&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.16&sourceBranch=refs/tags/@jotunheim/react-link@3.0.19&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.16&sourceBranch=refs/tags/@jotunheim/react-link@3.0.18&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.16&sourceBranch=refs/tags/@jotunheim/react-link@3.0.17&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.15&sourceBranch=refs/tags/@jotunheim/react-link@3.0.16&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.14&sourceBranch=refs/tags/@jotunheim/react-link@3.0.15&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.12&sourceBranch=refs/tags/@jotunheim/react-link@3.0.13&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.11&sourceBranch=refs/tags/@jotunheim/react-link@3.0.12&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.10&sourceBranch=refs/tags/@jotunheim/react-link@3.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.9&sourceBranch=refs/tags/@jotunheim/react-link@3.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.8&sourceBranch=refs/tags/@jotunheim/react-link@3.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.6&sourceBranch=refs/tags/@jotunheim/react-link@3.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.3&sourceBranch=refs/tags/@jotunheim/react-link@3.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.2&sourceBranch=refs/tags/@jotunheim/react-link@3.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.1&sourceBranch=refs/tags/@jotunheim/react-link@3.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-link

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-link@3.0.0&sourceBranch=refs/tags/@jotunheim/react-link@3.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-link

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.1.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.61&sourceBranch=refs/tags/@confirmit/react-link@2.1.62&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.60&sourceBranch=refs/tags/@confirmit/react-link@2.1.61&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.59&sourceBranch=refs/tags/@confirmit/react-link@2.1.60&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.1.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.58&sourceBranch=refs/tags/@confirmit/react-link@2.1.59&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.57&sourceBranch=refs/tags/@confirmit/react-link@2.1.58&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.55&sourceBranch=refs/tags/@confirmit/react-link@2.1.56&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.54&sourceBranch=refs/tags/@confirmit/react-link@2.1.55&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.53&sourceBranch=refs/tags/@confirmit/react-link@2.1.54&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.52&sourceBranch=refs/tags/@confirmit/react-link@2.1.53&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.51&sourceBranch=refs/tags/@confirmit/react-link@2.1.52&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.49&sourceBranch=refs/tags/@confirmit/react-link@2.1.50&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.48&sourceBranch=refs/tags/@confirmit/react-link@2.1.49&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.47&sourceBranch=refs/tags/@confirmit/react-link@2.1.48&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.46&sourceBranch=refs/tags/@confirmit/react-link@2.1.47&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.45&sourceBranch=refs/tags/@confirmit/react-link@2.1.46&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.44&sourceBranch=refs/tags/@confirmit/react-link@2.1.45&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.43&sourceBranch=refs/tags/@confirmit/react-link@2.1.44&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [2.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.42&sourceBranch=refs/tags/@confirmit/react-link@2.1.43&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.40&sourceBranch=refs/tags/@confirmit/react-link@2.1.41&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.39&sourceBranch=refs/tags/@confirmit/react-link@2.1.40&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.38&sourceBranch=refs/tags/@confirmit/react-link@2.1.39&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.37&sourceBranch=refs/tags/@confirmit/react-link@2.1.38&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.36&sourceBranch=refs/tags/@confirmit/react-link@2.1.37&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.35&sourceBranch=refs/tags/@confirmit/react-link@2.1.36&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.34&sourceBranch=refs/tags/@confirmit/react-link@2.1.35&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.33&sourceBranch=refs/tags/@confirmit/react-link@2.1.34&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.32&sourceBranch=refs/tags/@confirmit/react-link@2.1.33&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.31&sourceBranch=refs/tags/@confirmit/react-link@2.1.32&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.30&sourceBranch=refs/tags/@confirmit/react-link@2.1.31&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.29&sourceBranch=refs/tags/@confirmit/react-link@2.1.30&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.28&sourceBranch=refs/tags/@confirmit/react-link@2.1.29&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.27&sourceBranch=refs/tags/@confirmit/react-link@2.1.28&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.26&sourceBranch=refs/tags/@confirmit/react-link@2.1.27&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.25&sourceBranch=refs/tags/@confirmit/react-link@2.1.26&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.24&sourceBranch=refs/tags/@confirmit/react-link@2.1.25&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.22&sourceBranch=refs/tags/@confirmit/react-link@2.1.23&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.21&sourceBranch=refs/tags/@confirmit/react-link@2.1.22&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.20&sourceBranch=refs/tags/@confirmit/react-link@2.1.21&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.19&sourceBranch=refs/tags/@confirmit/react-link@2.1.20&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.18&sourceBranch=refs/tags/@confirmit/react-link@2.1.19&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.17&sourceBranch=refs/tags/@confirmit/react-link@2.1.18&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.16&sourceBranch=refs/tags/@confirmit/react-link@2.1.17&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.15&sourceBranch=refs/tags/@confirmit/react-link@2.1.16&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.14&sourceBranch=refs/tags/@confirmit/react-link@2.1.15&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.13&sourceBranch=refs/tags/@confirmit/react-link@2.1.14&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.12&sourceBranch=refs/tags/@confirmit/react-link@2.1.13&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.11&sourceBranch=refs/tags/@confirmit/react-link@2.1.12&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.10&sourceBranch=refs/tags/@confirmit/react-link@2.1.11&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.9&sourceBranch=refs/tags/@confirmit/react-link@2.1.10&targetRepoId=1246) (2020-12-18)

### Bug Fixes

- allow Link to be truncated ([NPM-666](https://jiraosl.firmglobal.com/browse/NPM-666)) ([2cebd50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2cebd508ec59b1c5d3a63ca6f168241af444c148))

## [2.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.8&sourceBranch=refs/tags/@confirmit/react-link@2.1.9&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.7&sourceBranch=refs/tags/@confirmit/react-link@2.1.8&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.6&sourceBranch=refs/tags/@confirmit/react-link@2.1.7&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.5&sourceBranch=refs/tags/@confirmit/react-link@2.1.6&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.4&sourceBranch=refs/tags/@confirmit/react-link@2.1.5&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.3&sourceBranch=refs/tags/@confirmit/react-link@2.1.4&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-link

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.1.1&sourceBranch=refs/tags/@confirmit/react-link@2.1.2&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-link

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.0.5&sourceBranch=refs/tags/@confirmit/react-link@2.1.0&targetRepoId=1246) (2020-11-24)

### Features

- add PortalTrigger support and pass onClick down to link element ([NPM-614](https://jiraosl.firmglobal.com/browse/NPM-614)) ([b90528c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b90528c795d012ee64aa2d5e53df5df44cedfcd8))

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.0.3&sourceBranch=refs/tags/@confirmit/react-link@2.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-link

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.0.2&sourceBranch=refs/tags/@confirmit/react-link@2.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-link

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@2.0.1&sourceBranch=refs/tags/@confirmit/react-link@2.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-link

# [2.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.32&sourceBranch=refs/tags/@confirmit/react-link@2.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink instead of anchor to be able to use routing with history ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([6dc875c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6dc875cece788c09776687d6b1f3dfc19d802bf4))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink has peer dependency @confirmit/react-contexts. No api or layout breaking changes.

## [1.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.31&sourceBranch=refs/tags/@confirmit/react-link@1.0.32&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.28&sourceBranch=refs/tags/@confirmit/react-link@1.0.29&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.27&sourceBranch=refs/tags/@confirmit/react-link@1.0.28&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.26&sourceBranch=refs/tags/@confirmit/react-link@1.0.27&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.23&sourceBranch=refs/tags/@confirmit/react-link@1.0.24&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-link@1.0.21&sourceBranch=refs/tags/@confirmit/react-link@1.0.22&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-link@1.0.17...@confirmit/react-link@1.0.18) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.13](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-link@1.0.12...@confirmit/react-link@1.0.13) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-link

## [1.0.12](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-link@1.0.11...@confirmit/react-link@1.0.12) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-link

## CHANGELOG

### v1.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v0.0.1

- Initial version
