# Jotunheim React Dropzone

[Changelog](./CHANGELOG.md)

A Drop zone for file upload, which also has a Browse button

## Usage

```jsx
const FileUploader = () => {
  const [uploadState, setUploadState] = React.useState(UploadStates.Idle);
  const [files, setFiles] = React.useState([]);
  const [selectedFileName, setSelectedFileName] = React.useState('');

  return (
    <div>
      <Dropzone
        uploadState={uploadState}
        selectedLabel={selectedFileName}
        onFileSelected={(selectedFiles) => {
          setFiles(selectedFiles);
          setSelectedFileName(selectedFiles[0].name);
        }}
        onClearFile={() => {
          setFiles([]);
          setSelectedFileName('');
        }}
        accept={['.doc', '.docx', 'image/*']}
      />
      <Button
        onClick={() => {
          setUploadState(UploadStates.Uploading);
        }}>
        Start upload!
      </Button>
    </div>
  );
};
```
