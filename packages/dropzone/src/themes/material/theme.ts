import classNames from './css/Dropzone.module.css';

export default {
  dropzone: {
    baseClassName: 'comd-dropzone',
    classNames,
  },
};
