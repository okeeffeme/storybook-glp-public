import React, {useRef} from 'react';
import cn from 'classnames';

import Tooltip from '@jotunheim/react-tooltip';
import Button, {IconButton} from '@jotunheim/react-button';
import Icon, {closeCircle, upload} from '@jotunheim/react-icons';
import {extractDataAriaIdProps, useWindowEvent} from '@jotunheim/react-utils';
import {useTheme, bemFactory} from '@jotunheim/react-themes';

import componentThemes from '../themes';

type DropzoneTexts = {
  dropFileToUpload?: string;
  dropMultipleFilesToUpload?: string;
  browse?: string;
};

type DropzoneTypes = {
  onFileSelected: (files: File[]) => void;
  onClearFile: () => void;
  uploadState: UploadStates;
  selectedLabel?: string;
  errorText?: string;
  texts?: DropzoneTexts;
  accept?: string[];
  multiple?: boolean;
};

export enum UploadStates {
  Idle = 'Idle',
  Uploading = 'Uploading',
}

const preventDefault = (e: Event) => {
  e.preventDefault();
};

const defaultAccept = ['*.*'];

const defaultTexts = {
  dropFileToUpload: 'Drop file here, or',
  dropMultipleFilesToUpload: 'Drop files here, or',
  browse: 'Browse',
};

/**
 * @param onFileSelected callback when files are dropped/selected. This always returns an array, to have a simple API to switch between multiple files and single file upload.
 * @param onClearFile callback when clear file is clicked
 * @param selectedLabel the text to display when a file is selected, usually this should be the filename. Setting this will also trigger the "Selected" state.
 * @param uploadState an enum to describe the state the input is in. If an upload is in progress, this should be set to UploadStates.Uploading.
 * @param errorText if set, the input will show an error message
 * @param texts translation strings for the different texts.
 * @param accept an array of file extensions starting with a period ("."), for example .png and/or MIME types to accept in the browse dialog. Keep in mind that users can override this by selecting All files (*.*) in the browse dialog, and dropping files will accept anything, so any errors related to invalid file types needs to be handled by the consumer.
 * @param multiple boolean to express if multiple files are allowed for upload.
 */
export const Dropzone = ({
  onFileSelected,
  selectedLabel,
  errorText,
  texts,
  onClearFile,
  accept = defaultAccept,
  multiple = false,
  uploadState = UploadStates.Idle,
  ...rest
}: DropzoneTypes) => {
  const {baseClassName, classNames} = useTheme('dropzone', componentThemes);
  const {block, element, modifier} = bemFactory({baseClassName, classNames});
  const browseInput = useRef<HTMLInputElement>(null);

  useWindowEvent('dragover', preventDefault);
  useWindowEvent('drop', preventDefault);

  const [isHoveringDroptarget, setIsHoveringDroptarget] = React.useState(false);

  const mergedTexts = {
    ...defaultTexts,
    ...texts,
  };

  const handleDragOver = (event) => {
    event.stopPropagation();
    preventDefault(event);
    setIsHoveringDroptarget(true);
    event.dataTransfer.dropEffect = 'copy';
  };

  const handleDragLeave = (event) => {
    event.stopPropagation();
    preventDefault(event);
    setIsHoveringDroptarget(false);
  };

  const handleDrop = (event) => {
    event.stopPropagation();
    preventDefault(event);
    setIsHoveringDroptarget(false);
    const files = Array.from(event.dataTransfer.files as File[]);

    onFileSelected(multiple ? files : [files[0]]);
  };

  const handleFileChange = (event) => {
    const files = Array.from(event.target.files as File[]);
    onFileSelected(files);
  };

  const handleBrowseClick = () => {
    browseInput.current?.click();
  };

  return (
    <Tooltip
      content={errorText}
      type={Tooltip.Types.error}
      open={Boolean(errorText)}>
      <div
        className={cn(block(), {
          [modifier('file-selected')]: Boolean(selectedLabel),
          [modifier('error')]: Boolean(errorText),
          [modifier('drag-active')]: isHoveringDroptarget,
        })}
        data-testid="dropzone"
        onDragOver={handleDragOver}
        onDragLeave={handleDragLeave}
        onDrop={handleDrop}
        {...extractDataAriaIdProps(rest)}>
        <div className={element('content')}>
          {selectedLabel ? (
            <div className={element('selected-file')}>
              {uploadState === UploadStates.Idle && (
                <IconButton
                  data-testid="dropzone-clear-file"
                  onClick={onClearFile}>
                  <Icon path={closeCircle} />
                </IconButton>
              )}
              <div
                className={element('filename')}
                data-testid="dropzone-selected-label">
                {selectedLabel}
              </div>
            </div>
          ) : (
            <div className={element('file-select')}>
              <div className={element('file-select-icon')}>
                <Icon path={upload} />
              </div>
              <div
                className={element('text')}
                data-testid="dropzone-upload-file-text">
                {multiple
                  ? mergedTexts.dropMultipleFilesToUpload
                  : mergedTexts.dropFileToUpload}
                <div className={element('file-input-label-wrapper')}>
                  <Button
                    onClick={handleBrowseClick}
                    data-testid="dropzone-browse-label">
                    {mergedTexts.browse}
                  </Button>
                </div>
              </div>
              <input
                type="file"
                ref={browseInput}
                id="dropzone-upload-file"
                data-testid="dropzone-file-input"
                className={element('file-input')}
                onChange={handleFileChange}
                accept={accept.join(',')}
                multiple={multiple}
              />
            </div>
          )}
        </div>
      </div>
    </Tooltip>
  );
};

export default Dropzone;
