import Dropzone, {UploadStates} from './components/Dropzone';

export {UploadStates};

export default Dropzone;
