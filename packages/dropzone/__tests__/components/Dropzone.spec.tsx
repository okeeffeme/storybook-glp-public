import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {Dropzone, UploadStates} from '../../src/components/Dropzone';

describe('Jotunheim React Dropzone :: ', () => {
  it('should show file input when selectedLabel is not set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    expect(screen.getByTestId('dropzone-file-input')).toBeInTheDocument();
    expect(
      screen.queryByTestId('dropzone-selected-label')
    ).not.toBeInTheDocument();
  });

  it('should show selected label when selectedLabel is set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
        selectedLabel="my file.png"
      />
    );

    expect(screen.queryByTestId('dropzone-file-input')).not.toBeInTheDocument();
    expect(screen.queryByTestId('dropzone-selected-label')).toBeInTheDocument();
    expect(screen.getByText(/my file.png/i)).toBeInTheDocument();
  });

  it('should not set modifier class when selectedLabel is not set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    expect(
      screen
        .getByTestId('dropzone')
        .classList.contains('comd-dropzone--file-selected')
    ).toBe(false);
  });

  it('should set modifier class when selectedLabel is set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
        selectedLabel="my file.png"
      />
    );

    expect(
      screen
        .getByTestId('dropzone')
        .classList.contains('comd-dropzone--file-selected')
    ).toBe(true);
  });

  it('should not set modifier class when errorText is not set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    expect(
      screen.getByTestId('dropzone').classList.contains('comd-dropzone--error')
    ).toBe(false);
  });

  it('should set modifier class when errorText is set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
        selectedLabel="my file.png"
        errorText="error message"
      />
    );

    expect(
      screen.getByTestId('dropzone').classList.contains('comd-dropzone--error')
    ).toBe(true);
  });

  it('should not set modifier class when not dragging and hovering over component', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    const dropzone = screen.getByTestId('dropzone');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      dataTransfer: {},
    };

    expect(dropzone.classList.contains('comd-dropzone--drag-active')).toBe(
      false
    );

    fireEvent.dragOver(dropzone, event);

    expect(dropzone.classList.contains('comd-dropzone--drag-active')).toBe(
      true
    );

    fireEvent.dragLeave(dropzone, event);

    expect(dropzone.classList.contains('comd-dropzone--drag-active')).toBe(
      false
    );
  });

  it('should set modifier class when dragging and hovering over component', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    const target = screen.getByTestId('dropzone');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      dataTransfer: {},
    };

    fireEvent.dragOver(target, event);

    expect(target.classList.contains('comd-dropzone--drag-active')).toBe(true);
  });

  it('should show clear button when uploadState is idle', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
        selectedLabel="my file.png"
      />
    );

    expect(screen.getByTestId('dropzone-clear-file')).toBeInTheDocument();
  });

  it('should not show clear button when uploadState is uploading', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Uploading}
        selectedLabel="my file.png"
      />
    );

    expect(screen.queryByTestId('dropzone-clear-file')).not.toBeInTheDocument();
  });

  it('should show correct upload text when multiple files are disabled', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Uploading}
      />
    );

    expect(screen.getByTestId('dropzone-upload-file-text').textContent).toEqual(
      'Drop file here, orBrowse'
    );
  });

  it('should show correct upload text when multiple files are enabled', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Uploading}
        multiple={true}
      />
    );

    expect(screen.getByTestId('dropzone-upload-file-text').textContent).toEqual(
      'Drop files here, orBrowse'
    );
  });

  it('should show correct text when translation text is set', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Uploading}
        texts={{
          dropFileToUpload: 'Slipp filen her, eller',
          browse: 'Bla gjennom',
        }}
      />
    );

    expect(screen.getByTestId('dropzone-upload-file-text').textContent).toEqual(
      'Slipp filen her, ellerBla gjennom'
    );
  });

  it('should set correct accept parameters when accept props is passed', () => {
    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={() => {}}
        uploadState={UploadStates.Uploading}
        accept={['.doc', 'image/*']}
      />
    );

    expect(screen.getByTestId('dropzone-file-input')).toHaveAttribute(
      'accept',
      '.doc,image/*'
    );
  });

  it('should call onClearFile callback when clear button is clicked', () => {
    const handleClear = jest.fn();

    render(
      <Dropzone
        onFileSelected={() => {}}
        onClearFile={handleClear}
        uploadState={UploadStates.Idle}
        selectedLabel="my file.png"
      />
    );

    const clearFileBtn = screen.getByTestId('dropzone-clear-file');

    userEvent.click(clearFileBtn);

    expect(handleClear).toHaveBeenCalled();
  });

  it('should call onFileSelected callback when file is dropped', () => {
    const handleFileSelected = jest.fn();

    render(
      <Dropzone
        onFileSelected={handleFileSelected}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    const target = screen.getByTestId('dropzone');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      dataTransfer: {
        files: [{name: 'my file.png'}],
      },
    };

    fireEvent.drop(target, event);

    expect(handleFileSelected).toHaveBeenCalledWith([{name: 'my file.png'}]);
  });

  it('should call onFileSelected callback with only first item when multiple files are dropped and multiple prop is false', () => {
    const handleFileSelected = jest.fn();

    render(
      <Dropzone
        onFileSelected={handleFileSelected}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    const target = screen.getByTestId('dropzone');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      dataTransfer: {
        files: [{name: 'my file.png'}, {name: 'my second file.png'}],
      },
    };

    fireEvent.drop(target, event);

    expect(handleFileSelected).toHaveBeenCalledWith([{name: 'my file.png'}]);
  });

  it('should call onFileSelected callback with all items when multiple files are dropped and multiple prop is true', () => {
    const handleFileSelected = jest.fn();

    render(
      <Dropzone
        onFileSelected={handleFileSelected}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
        multiple={true}
      />
    );

    const target = screen.getByTestId('dropzone');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      dataTransfer: {
        files: [{name: 'my file.png'}, {name: 'my second file.png'}],
      },
    };

    fireEvent.drop(target, event);

    expect(handleFileSelected).toHaveBeenCalledWith([
      {name: 'my file.png'},
      {name: 'my second file.png'},
    ]);
  });

  it('should call onFileSelected callback when file input changes', () => {
    const handleFileSelected = jest.fn();

    render(
      <Dropzone
        onFileSelected={handleFileSelected}
        onClearFile={() => {}}
        uploadState={UploadStates.Idle}
      />
    );

    const target = screen.getByTestId('dropzone-file-input');

    const event = {
      stopPropagation: () => {},
      preventDefault: () => {},
      target: {
        files: [{name: 'my file.png'}],
      },
    };

    fireEvent.change(target, event);

    expect(handleFileSelected).toHaveBeenCalledWith([{name: 'my file.png'}]);
  });
});
