# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.45&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.46&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.45&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.45&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.43&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.44&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.42&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.43&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.41&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.42&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.40&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.41&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.39&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.40&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.38&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.39&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [1.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.37&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.38&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.36&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.37&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.35&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.36&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.34&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.35&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.33&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.34&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.32&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.33&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.31&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.32&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.30&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.31&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.28&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.30&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.28&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.27&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.28&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.26&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.25&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.24&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.23&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.21&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.23&targetRepoId=1246) (2022-12-21)

### Bug Fixes

- add test ids for Dropzone component ([NPM-1171](https://jiraosl.firmglobal.com/browse/NPM-1171)) ([95da4e3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/95da4e386c095360f620eefd2cfad838b4eaa4f2))

## [1.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.21&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.18&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.18&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.18&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.17&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.16&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.14&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.13&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.14&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of Button from Dropzone ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([906c746](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/906c7469cd39beea8488dc1dbd2ad0c5d00e20dd))

## [1.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.12&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.11&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.10&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.9&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.8&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.6&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.3&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.2&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.1&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-dropzone

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropzone@1.0.0&sourceBranch=refs/tags/@jotunheim/react-dropzone@1.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-dropzone

# 1.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [0.2.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.80&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.81&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.79&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.80&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.78&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.79&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [0.2.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.77&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.78&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.76&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.77&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.74&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.75&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.73&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.74&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.72&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.73&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.71&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.72&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.70&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.71&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.69&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.70&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.66&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.67&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.65&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.66&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.64&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.65&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.63&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.64&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.62&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.63&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.61&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.62&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.60&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.61&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.2.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.59&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.60&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.57&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.58&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.56&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.57&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.55&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.56&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.54&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.55&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.53&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.54&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.52&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.53&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.51&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.52&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.50&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.51&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.49&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.50&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.48&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.49&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.47&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.48&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.46&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.47&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.45&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.46&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.44&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.45&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.43&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.44&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.42&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.43&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.41&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.42&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.40&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.41&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.39&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.40&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.38&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.39&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.37&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.38&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.36&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.37&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.34&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.35&targetRepoId=1246) (2021-05-12)

### Bug Fixes

- remove incorrect dependency to themes package, this should only be a peerDependency ([NPM-767](https://jiraosl.firmglobal.com/browse/NPM-767)) ([cf221e0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cf221e02be40bb1a85b53d96ac4b464ae6a97297))

## [0.2.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.33&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.34&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.32&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.33&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.31&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.32&targetRepoId=1246) (2021-04-12)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.30&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.31&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.29&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.30&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.28&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.29&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.27&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.28&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.26&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.27&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.25&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.26&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.24&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.23&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.21&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.20&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.19&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.18&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.17&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.18&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.16&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.17&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.15&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.16&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.14&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.13&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.12&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.13&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.11&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.12&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.8&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.9&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.6&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.7&targetRepoId=1246) (2020-11-24)

### Bug Fixes

- prevent IE11 to fire onFileSelected twice ([NPM-613](https://jiraosl.firmglobal.com/browse/NPM-613)) ([bb4443c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb4443c9e89db34ecdf75629030ee146e2fc4742))

## [0.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.4&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.3&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.2.2&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-dropzone

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.11&sourceBranch=refs/tags/@confirmit/react-dropzone@0.2.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.10&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.11&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.9&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.10&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.8&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.9&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.7&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.8&targetRepoId=1246) (2020-10-21)

### Bug Fixes

- Browse button did not work in IE11 ([NPM-570](https://jiraosl.firmglobal.com/browse/NPM-570)) ([5dbd3cb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5dbd3cb11f75d7662bf93faa92d72d2184a5dbb5))

## [0.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.4&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.5&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.3&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.4&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-dropzone

## [0.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropzone@0.1.2&sourceBranch=refs/tags/@confirmit/react-dropzone@0.1.3&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-dropzone

# 0.1.0 (2020-09-28)

### Features

- initial version of Dropzone component ([NPM-523](https://jiraosl.firmglobal.com/browse/NPM-523)) ([0a8ca28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0a8ca28c2b4df8ca7f876ce29885e704929483ad)

## CHANGELOG

### v0.0.1

- Initial version
