import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Dropzone, {UploadStates} from '../src';
import {boolean, text, array, select, object} from '@storybook/addon-knobs';

storiesOf('Components/dropzone', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [files, setFiles] = React.useState([] as File[]);
    const [selectedText, setSelectedText] = React.useState('');

    React.useEffect(() => {
      setSelectedText(files.map((x: File) => x.name).join(','));
    }, [files]);

    return (
      <div style={{padding: '40px'}}>
        <Dropzone
          multiple={boolean('multiple', false)}
          onFileSelected={(selectedFiles) => {
            setFiles(selectedFiles);
          }}
          selectedLabel={selectedText}
          onClearFile={() => {
            setFiles([]);
            setSelectedText('');
          }}
          errorText={text('errorText', '')}
          texts={object('texts', {
            dropFileToUpload: 'Drop file here, or',
            dropMultipleFilesToUpload: 'Drop files here, or',
            browse: 'Browse',
          })}
          accept={array('accept', ['.doc', '.docx', 'image/*'])}
          uploadState={select('uploadState', UploadStates, UploadStates.Idle)}
        />
      </div>
    );
  });
