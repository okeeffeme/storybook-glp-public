import React, {useContext} from 'react';
import {render, screen} from '@testing-library/react';

import {HorizontalStep} from '../../src/components/HorizontalStep';

jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useContext: jest.fn(),
}));

const useContextMock = useContext as jest.Mock;

describe('Jotunheim React Horizontal Step :: ', () => {
  it('should show helperText if helperText is provided', () => {
    useContextMock.mockReturnValue({layout: 'horizontal'});

    render(
      <HorizontalStep stepNumber={1} title="Hello" helperText="Help me" />
    );
    expect(screen.getByTestId('stepper-step-help-text')).toBeInTheDocument();
  });

  it('should not show helperText if helperText is not provided', () => {
    render(<HorizontalStep stepNumber={1} title="Hello" />);

    expect(
      screen.queryByTestId('stepper-step-help-text')
    ).not.toBeInTheDocument();
  });

  it('should change appearance when the current step is active', () => {
    useContextMock.mockReturnValue({layout: 'horizontal', activeStepNumber: 1});

    render(<HorizontalStep stepNumber={1} title="Hello" />);
    const counter = screen.getByTestId('data-counter');

    expect(counter.classList.contains('comd-counter--default')).toBe(false);
    expect(counter.classList.contains('comd-counter--step')).toBe(true);
  });

  it('should not change appearance when the current step is inactive', () => {
    useContextMock.mockReturnValue({layout: 'horizontal', activeStepNumber: 2});

    render(<HorizontalStep stepNumber={1} title="Hello" />);

    const counter = screen.getByTestId('data-counter');

    expect(counter.classList.contains('comd-counter--default')).toBe(true);
    expect(counter.classList.contains('comd-counter--step')).toBe(false);
  });
});
