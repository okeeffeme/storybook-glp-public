import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';

import {Stepper} from '../../src/components/Stepper';
import {StepperLayout} from '../../src/types';

const render = (layout?: StepperLayout) =>
  renderRTL(
    <Stepper layout={layout}>
      <Stepper.Step stepNumber={1} title="title">
        content
      </Stepper.Step>
    </Stepper>
  );

describe('Jotunheim React Step :: ', () => {
  it('renders vertical stepper by default', () => {
    render();

    const stepper = screen.getByTestId('stepper');

    expect(stepper.classList.contains('comd-stepper--vertical')).toBe(true);
    expect(stepper.classList.contains('comd-stepper--horizontal')).toBe(false);
  });

  it('renders vertical stepper when users define in layout prop', () => {
    render(StepperLayout.vertical);

    const stepper = screen.getByTestId('stepper');

    expect(stepper.classList.contains('comd-stepper--vertical')).toBe(true);
    expect(stepper.classList.contains('comd-stepper--horizontal')).toBe(false);
  });

  it('renders horizontal stepper when users define in layout prop', () => {
    render(StepperLayout.horizontal);

    const stepper = screen.getByTestId('stepper');

    expect(stepper.classList.contains('comd-stepper--vertical')).toBe(false);
    expect(stepper.classList.contains('comd-stepper--horizontal')).toBe(true);
  });
});
