import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';

import {VerticalStep} from '../../src/components/VerticalStep';

const props = {
  stepNumber: 1,
  title: 'Hello',
};

const render = (props, children = 'Content') =>
  renderRTL(<VerticalStep {...props}>{children}</VerticalStep>);

describe('Jotunheim React Vertical Step :: ', () => {
  it('should show helperText if helperText is provided', () => {
    render({...props, helperText: 'Help me'});

    expect(screen.getByTestId('stepper-step-help-text')).toBeInTheDocument();
  });

  it('should not show helperText if helperText is not provided', () => {
    render(props);

    expect(
      screen.queryByTestId('stepper-step-help-text')
    ).not.toBeInTheDocument();
  });

  it('should render children if it is provided', () => {
    render(props, 'Children');

    expect(
      screen
        .getByText(/children/i)
        .classList.contains('comd-stepper-item__body')
    ).toBe(true);
  });

  it('should not render children if it is not provided', () => {
    renderRTL(<VerticalStep {...props} />);

    expect(screen.getByTestId('vertical-step').textContent).toBe('');
  });
});
