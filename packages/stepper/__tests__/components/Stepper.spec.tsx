import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';

import {Stepper} from '../../src/components/Stepper';
import {StepperLayout} from '../../src/types';

const render = (props) =>
  renderRTL(
    <Stepper {...props}>
      <Stepper.Step stepNumber={1} title="title 1">
        content
      </Stepper.Step>
      <Stepper.Step stepNumber={2} title="title 2">
        content
      </Stepper.Step>
    </Stepper>
  );

describe('Jotunheim React Stepper :: ', () => {
  describe('Vertical layout :: ', () => {
    it('renders stepper with vertical layout by default', () => {
      render({});

      const verticalSteps = screen.getAllByTestId('vertical-step');

      expect(verticalSteps.length).toBe(2);
      expect(
        verticalSteps[0].classList.contains('comd-stepper-item__body--vertical')
      ).toBe(true);
      expect(
        verticalSteps[1].classList.contains('comd-stepper-item__body--vertical')
      ).toBe(true);
    });

    it('renders stepper with vertical layout when vertical layout is specified', () => {
      render({layout: StepperLayout.vertical});

      const verticalSteps = screen.getAllByTestId('vertical-step');

      expect(verticalSteps.length).toBe(2);
      expect(
        verticalSteps[0].classList.contains('comd-stepper-item__body--vertical')
      ).toBe(true);
      expect(
        verticalSteps[1].classList.contains('comd-stepper-item__body--vertical')
      ).toBe(true);
    });
  });

  describe('Horizontal layout :: ', () => {
    it('renders stepper with horizontal layout when horizontal layout is specified', () => {
      render({layout: StepperLayout.horizontal});

      const horizontalSteps = screen.getAllByTestId('horizontal-step');

      expect(horizontalSteps.length).toBe(2);
      expect(
        horizontalSteps[0].classList.contains('comd-stepper-item--horizontal')
      ).toBe(true);
      expect(
        horizontalSteps[1].classList.contains('comd-stepper-item--horizontal')
      ).toBe(true);
    });

    it('should highlight the active step when stepper indicates an active number', () => {
      render({
        layout: StepperLayout.horizontal,
        activeStepNumber: 1,
      });

      const counter = screen.getAllByTestId('data-counter');
      const horizontalStep = screen.getAllByTestId('horizontal-step');

      expect(counter.length).toBe(2);
      expect(horizontalStep.length).toBe(2);
      expect(counter[0].classList.contains('comd-counter--step')).toBe(true);
      expect(counter[1].classList.contains('comd-counter--default')).toBe(true);
      expect(horizontalStep[0]).toHaveAttribute('data-step-active');
      expect(horizontalStep[1]).not.toHaveAttribute('data-step-active');
    });

    it('should not highlight any step when stepper does not indicate an active number', () => {
      render({layout: StepperLayout.horizontal});

      const counter = screen.getAllByTestId('data-counter');
      const horizontalStep = screen.getAllByTestId('horizontal-step');

      expect(counter.length).toBe(2);
      expect(horizontalStep.length).toBe(2);
      expect(counter[0].classList.contains('comd-counter--step')).toBe(false);
      expect(counter[1].classList.contains('comd-counter--default')).toBe(true);
      expect(horizontalStep[0]).not.toHaveAttribute('data-step-active');
      expect(horizontalStep[1]).not.toHaveAttribute('data-step-active');
    });
  });
});
