import React, {useContext} from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Step.module.css';
import cn from 'classnames';
import Counter from '@jotunheim/react-counter';
import {BaseStep} from './BaseStep';
import {StepProps} from './Step';
import StepperContext from '../stepper-context';
import {StepperLayout} from '../types';

const {element} = bemFactory({
  baseClassName: 'comd-stepper-item',
  classNames,
});

export const HorizontalStep = ({stepNumber, ...rest}: StepProps) => {
  const {activeStepNumber} = useContext(StepperContext);
  const isActiveStep = stepNumber === activeStepNumber;
  const baseProps = {
    appearance: isActiveStep
      ? Counter.Appearances.step
      : Counter.Appearances.default,
    layout: StepperLayout.horizontal,
    stepNumber,
    ...rest,
  };

  return (
    <BaseStep
      data-testid="horizontal-step"
      active={isActiveStep}
      {...baseProps}>
      <div className={cn(element('body'), element('body', 'horizontal'))} />
    </BaseStep>
  );
};
