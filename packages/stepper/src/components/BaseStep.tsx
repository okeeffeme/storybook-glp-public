import React from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {InformationIcon} from '@jotunheim/react-icons';
import Tooltip from '@jotunheim/react-tooltip';
import Counter, {CounterAppearances} from '@jotunheim/react-counter';

import {StepperLayout} from '../types';

import classNames from './Step.module.css';

type BaseStepProps = {
  active?: boolean;
  appearance?: CounterAppearances;
  children?: React.ReactNode;
  helperText?: React.ReactNode;
  layout: StepperLayout;
  stepNumber: number;
  title: React.ReactNode;
};

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-stepper-item',
  classNames,
});

export const BaseStep = ({
  active = false,
  appearance = CounterAppearances.default,
  children,
  helperText,
  layout,
  stepNumber,
  title,
  ...rest
}: BaseStepProps) => {
  return (
    <section
      className={cn(block(), modifier(layout))}
      {...(active && {['data-step-active']: active})}
      {...extractDataAriaIdProps(rest)}>
      <header className={element('header')}>
        <Counter value={stepNumber} appearance={appearance} />
        <div className={element('title-wrapper')}>
          <div className={element('title')}>{title}</div>
          {helperText && (
            <div
              className={element('helper-text')}
              data-testid="stepper-step-help-text">
              <Tooltip content={helperText}>
                <InformationIcon />
              </Tooltip>
            </div>
          )}
        </div>
      </header>
      {children}
    </section>
  );
};
