import React, {useContext} from 'react';
import {HorizontalStep} from './HorizontalStep';
import StepperContext from '../stepper-context';
import {StepperLayout} from '../types';
import {VerticalStep} from './VerticalStep';

export type StepProps = {
  children?: React.ReactNode;
  helperText?: React.ReactNode;
  stepNumber: number;
  title: React.ReactNode;
};

export const Step = (props: StepProps) => {
  const {layout} = useContext(StepperContext);

  if (layout === StepperLayout.horizontal) {
    return <HorizontalStep {...props} />;
  }
  return <VerticalStep {...props} />;
};

export default Step;
