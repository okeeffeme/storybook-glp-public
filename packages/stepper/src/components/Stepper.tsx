import React from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import Step from './Step';
import classNames from './Stepper.module.css';
import StepperContext from '../stepper-context';
import {StepperLayout} from '../types';
import cn from 'classnames';

type StepperProps = {
  layout?: StepperLayout;
  children?: React.ReactNode;
  activeStepNumber?: number;
};

const {block, modifier} = bemFactory({
  baseClassName: 'comd-stepper',
  classNames,
});

export const Stepper = ({
  layout = StepperLayout.vertical,
  children,
  activeStepNumber,
  ...rest
}: StepperProps) => {
  const contextValue = React.useMemo(
    () => ({layout, activeStepNumber}),
    [layout, activeStepNumber]
  );

  return (
    <StepperContext.Provider value={contextValue}>
      <div
        data-testid="stepper"
        className={cn(block(), modifier(layout))}
        {...extractDataAriaIdProps(rest)}>
        {children}
      </div>
    </StepperContext.Provider>
  );
};

Stepper.Step = Step;

export default Stepper;
