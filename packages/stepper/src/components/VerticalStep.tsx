import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Step.module.css';
import cn from 'classnames';
import Counter from '@jotunheim/react-counter';
import {BaseStep} from './BaseStep';
import {StepperLayout} from '../types';
import {StepProps} from './Step';

const {element} = bemFactory({
  baseClassName: 'comd-stepper-item',
  classNames,
});

export const VerticalStep = ({children, ...rest}: StepProps) => {
  const baseProps = {
    appearance: Counter.Appearances.step,
    layout: StepperLayout.vertical,
    ...rest,
  };

  return (
    <BaseStep {...baseProps}>
      <div
        data-testid="vertical-step"
        className={cn(element('body'), element('body', 'vertical'))}>
        {children}
      </div>
    </BaseStep>
  );
};
