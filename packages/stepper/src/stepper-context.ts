import React from 'react';
import {StepperLayout} from './types';

type Props = {
  layout: StepperLayout;
  activeStepNumber?: number;
};

export default React.createContext<Props>({
  layout: StepperLayout.vertical,
  activeStepNumber: undefined,
});
