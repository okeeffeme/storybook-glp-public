import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Stepper from '../src/components/Stepper';
import {StepperLayout} from '../src/types';

const Pad = ({children}) => {
  return <div style={{padding: '20px'}}>{children}</div>;
};

storiesOf('Components/stepper', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Vertical stepper (default)', () => (
    <>
      <Pad>
        <Stepper>
          <Stepper.Step stepNumber={1} title="Do this first">
            This is the content of step 1
          </Stepper.Step>
          <Stepper.Step stepNumber={2} title="Then do this">
            This is the content of step 2
          </Stepper.Step>
          <Stepper.Step stepNumber={3} title="And finally do this">
            This is the content of step 3
          </Stepper.Step>
        </Stepper>
      </Pad>
    </>
  ))
  .add('Horizontal stepper', () => (
    <>
      <Pad>
        <Stepper activeStepNumber={2} layout={StepperLayout.horizontal}>
          <Stepper.Step stepNumber={1} title="Do this first" />
          <Stepper.Step stepNumber={2} title="Then do this" />
          <Stepper.Step stepNumber={3} title="And finally do this" />
        </Stepper>
      </Pad>
    </>
  ));
