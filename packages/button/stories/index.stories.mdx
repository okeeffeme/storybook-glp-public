import {Meta, Story, Canvas, ArgsTable} from '@storybook/addon-docs/blocks';
import {boolean, text, select} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Button, ButtonRow, IconButton, JumboButton} from '../src';

import Icon, {helpCircle, pencil, bellCircle} from '../../icons/src';
import Tooltip from '../../tooltip/src';
import {Fieldset} from '../../fieldset/src';

import colors from '../../global-styles/src/exports/colors.scss';

import illustration from './illustration-example.png';

<Meta
  title="Components/Button"
  component={Button}
  parameters={{
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  }}
/>

# Buttons

Buttons can be used to provide users access to high priority page level actions or functions. The label displayed on the button should clearly indicate the resulting page or action that will be completed. Buttons can have a single function or can be split to provide access to a menu of options. Each button will have an idle, hover, and click state. Buttons may include icons to enhance recognizability or to convey directionality.

- [Go to Icon Button](?path=/docs/components-button--large-icon#icon-button)
- [Go to Jumbo Button](?path=/docs/components-button--large-icon#jumbo-button)

## Properties

<ArgsTable of={Button} />

## Primary Button

Primary buttons are used to indicate the most important action on a page or in a view, have capital letters, and are filled using the application’s or report’s accent color (see Colors and Contrast). Primary buttons can have processing states if an operation takes longer than 1 second. In this state, the button is disabled.
Example uses:

- In overlays, dialogs and forms when there is a clear primary action.
- Inline actions where the button needs to stand out more in the UI.

<Canvas>
  <Story name="Primary buttons">
    <ButtonRow>
      {Object.values(Button.appearances)
        .filter((x) => x.startsWith('primary'))
        .map((appearance) => {
          return (
            <Button
              key={appearance}
              fullWidth={false}
              disabled={false}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              href={text('href', '')}
              appearance={appearance}>
              {appearance}
            </Button>
          );
        })}
    </ButtonRow>
  </Story>
</Canvas>

## Secondary Button

Secondary buttons are utilized to provide access to subordinate actions or functions and can be displayed in either blue or green. When used in conjunction with primary buttons, secondary buttons should be displayed in blue text. When there are only secondaries, green text can be used on more important actions.

<Canvas>
  <Story name="Secondary buttons">
    <ButtonRow>
      {Object.values(Button.appearances)
        .filter((x) => x.startsWith('secondary'))
        .map((appearance) => {
          return (
            <Button
              key={appearance}
              fullWidth={false}
              disabled={false}
              href={text('href', '')}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              appearance={appearance}>
              {appearance}
            </Button>
          );
        })}
    </ButtonRow>
  </Story>
</Canvas>

There is also a variant with white text color, intended to be used on colored backgrounds, f.ex in Banners

<Canvas>
  <Story name="Secondary banner-buttons">
    <div style={{background: colors.PrimaryAccent, padding: '20px'}}>
      <Button appearance={Button.appearances.secondaryBanner}>
        {Button.appearances.secondaryBanner}
      </Button>
    </div>
    <div style={{background: colors.Danger, padding: '20px'}}>
      <Button appearance={Button.appearances.secondaryBanner}>
        {Button.appearances.secondaryBanner}
      </Button>
    </div>
    <div style={{background: colors.Safe, padding: '20px'}}>
      <Button appearance={Button.appearances.secondaryBanner}>
        {Button.appearances.secondaryBanner}
      </Button>
    </div>
  </Story>
</Canvas>

## Tertiary Button

Tertiary buttons are utilized to provide local access to miscellanious actions or functions and can only be displayed in blue

<Canvas>
  <Story name="Tertiary buttons">
    <ButtonRow>
      {Object.values(Button.appearances)
        .filter((x) => x.startsWith('tertiary'))
        .map((appearance) => {
          return (
            <Button
              key={appearance}
              appearance={appearance}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              href={text('href', '')}>
              {appearance}
            </Button>
          );
        })}
    </ButtonRow>
  </Story>
</Canvas>

### Extra props

#### Prefix and suffix

Buttons can have a `prefix` or `suffix`, usually in the form of an icon

<Canvas>
  <Story name="Prefix/Suffix buttons">
    <ButtonRow>
      {Object.values(Button.appearances).map((appearance) => {
        return (
          <Button
            key={appearance}
            fullWidth={boolean('fullWidth', false)}
            disabled={boolean('disabled', false)}
            href={text('href', '')}
            prefix={<Icon path={helpCircle} />}
            appearance={appearance}>
            {appearance}
          </Button>
        );
      })}
    </ButtonRow>
    <div style={{marginTop: '16px'}}>
      <ButtonRow>
        {Object.values(Button.appearances).map((appearance) => {
          return (
            <Button
              key={appearance}
              fullWidth={boolean('fullWidth', false)}
              disabled={boolean('disabled', false)}
              href={text('href', '')}
              suffix={<Icon path={helpCircle} />}
              appearance={appearance}>
              {appearance}
            </Button>
          );
        })}
      </ButtonRow>
    </div>
  </Story>
</Canvas>

#### Disabled

Buttons will look greyed out when disabled

<Canvas>
  <Story name="Disabled buttons">
    <div>
      {Object.values(Button.appearances).map((appearance) => {
        return (
          <Button
            key={appearance}
            fullWidth={boolean('fullWidth', false)}
            disabled={true}
            appearance={appearance}>
            {appearance}
          </Button>
        );
      })}
    </div>
    <div>
      <IconButton disabled={true}>
        <Icon path={bellCircle} />
      </IconButton>
    </div>
    <div>
      <JumboButton
        iconPath={pencil}
        onClick={(e) => {
          console.log('click');
        }}
        title="Disabled action"
        text="Some text explaining what the Action does"
        disabled={true}
      />
    </div>
  </Story>
</Canvas>

#### Disabled with Tooltips

By default, a disabled `button` does not trigger user interactions so a Tooltip will not activate on normal events like hover. To accommodate disabled elements, add a simple wrapper element, such as a span.

<Canvas>
  <Story name="Disabled buttons with Tooltip">
    <div>
      <Tooltip content={'Disabled button tooltip'}>
        <span>
          <Button disabled={true}>Disabled Button With Tooltip</Button>
        </span>
      </Tooltip>
    </div>
    <div>
      <Tooltip content={'Disabled icon-button tooltip'}>
        <span>
          <IconButton disabled={true}>
            <Icon path={bellCircle} />
          </IconButton>
        </span>
      </Tooltip>
    </div>
    <div>
      <Tooltip content={'Disabled jumbo-button tooltip'}>
        <JumboButton
          iconPath={pencil}
          onClick={(e) => {
            console.log('click');
          }}
          title="Disabled action"
          text="Some text explaining what the Action does"
          disabled={true}
        />
      </Tooltip>
    </div>
  </Story>
</Canvas>

#### Full-width

Buttons can take 100% width of the parent container

<Canvas>
  <Story name="Full-width buttons">
    {Object.values(Button.appearances).map((appearance) => {
      return (
        <Button key={appearance} fullWidth={true} appearance={appearance}>
          {appearance}
        </Button>
      );
    })}
  </Story>
</Canvas>

# Button Row

Buttons may be displayed in groups, with a clear hierarchy created by a use of a mix of primary, secondary button styles. Note that the primary button is always displayed on the right, and there should only be one primary button in the group. Spacing between the buttons is always 16px, even if there is a combination of primary and secondary buttons.
To achieve spacing between buttons, the buttons can be wrapped in a `<ButtonRow>` container.

# Icon Button

Icon buttons allow users to take actions, and make choices with a single tap. Depending on the use case, icon buttons may be displayed on their own or in groups or may be part of a double icon set. Icon buttons support two sizes: 24*24 with a 40*40 background circle displayed on hover, or 20\*20 with a hover effect that does not include the appearance of a background circle.

## Properties

<ArgsTable of={IconButton} />

## Large Icon Button

The larger of the two icon button sizes are used when space permits the display of the circle background on hover. The smaller icon button is employed when space is limited. For example, tin the tree when density is medium or high, and in table column headers.

Multiple large icon buttons should not be wrapped in a `<ButtonRow>` component, but instead, there should be no space between them, meaning the hover circles should be right next to each other.

<Canvas>
  <Story name="Large icon-buttons">
    <ButtonRow>
      <IconButton>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton>
        <Icon path={pencil} />
      </IconButton>
    </ButtonRow>
  </Story>
</Canvas>

## Small Icon Button

<Canvas>
  <Story name="Small icon-buttons">
    <ButtonRow>
      <IconButton size={IconButton.sizes.small}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton size={IconButton.sizes.small}>
        <Icon path={pencil} />
      </IconButton>
    </ButtonRow>
  </Story>
</Canvas>

### Extra props

#### Disabled

Icon button also supports a disabled state

<Canvas>
  <Story name="Disabled icon-buttons">
    <ButtonRow>
      <IconButton
        disabled={true}
        onClick={() => {
          console.log('click');
        }}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton
        disabled={true}
        onClick={() => {
          console.log('click');
        }}>
        <Icon path={pencil} />
      </IconButton>
      <IconButton
        size={IconButton.sizes.small}
        disabled={true}
        onClick={() => {
          console.log('click');
        }}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton
        size={IconButton.sizes.small}
        disabled={true}
        onClick={() => {
          console.log('click');
        }}>
        <Icon path={pencil} />
      </IconButton>
    </ButtonRow>
  </Story>
</Canvas>

#### Appearance

Icon button also supports a "banner" appearance to be used on colored backgrounds, like f.ex in Banners

<Canvas>
  <Story name="Banner icon-buttons">
    <div style={{background: colors.PrimaryAccent, padding: '20px'}}>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={pencil} />
      </IconButton>
    </div>
    <div style={{background: colors.Danger, padding: '20px'}}>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={pencil} />
      </IconButton>
    </div>
    <div style={{background: colors.Safe, padding: '20px'}}>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={helpCircle} />
      </IconButton>
      <IconButton appearance={IconButton.appearances.banner}>
        <Icon path={pencil} />
      </IconButton>
    </div>
  </Story>
</Canvas>

# Jumbo Button

There is another variation of this button for on/off states, which is the [Jumbo Toggle Button](?path=/docs/components-toggle-button--jumbotogglebutton#jumbotogglebutton)

## Properties

<ArgsTable of={JumboButton} />

<Canvas>
  <Story name="Jumbo button">
    <Fieldset>
      <JumboButton
        iconPath={pencil}
        onClick={(e) => {
          console.log('click');
        }}
        title="Do some Action"
        text="Some text explaining what the Action does"
        disabled={boolean('disabled jumbobutton', false)}
      />
      <JumboButton
        imageSrc={illustration}
        onChange={(e) => {
          console.log('click');
          e.preventDefault();
        }}
        href="#"
        title="Do some other Action"
        text="The imageSrc can be SVG (preferred) or any other valid img src value"
        disabled={boolean('disabled jumbobutton', false)}
      />
    </Fieldset>
  </Story>
</Canvas>

### **Resources**

- [Abstract Page](https://share.goabstract.com/1382e7b7-1eb3-4ef3-b372-aa96cda46ca3?collectionLayerId=63f3effa-856a-49cd-8042-6c0cf1282c54&mode=design)
