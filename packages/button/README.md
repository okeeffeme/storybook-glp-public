# Jotunheim React Button

## ButtonRow component

Useful for getting correct spacing between multiple Buttons in a row. This can also be used with WaitButton.

Example

```jsx
<ButtonRow>
  <Button>Cancel</Button>
  <WaitButton>Some async action</WaitButton>
</ButtonRow>
```
