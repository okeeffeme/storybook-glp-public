# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.9&sourceBranch=refs/tags/@jotunheim/react-button@8.1.9&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.7&sourceBranch=refs/tags/@jotunheim/react-button@8.1.8&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.6&sourceBranch=refs/tags/@jotunheim/react-button@8.1.7&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.5&sourceBranch=refs/tags/@jotunheim/react-button@8.1.6&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.4&sourceBranch=refs/tags/@jotunheim/react-button@8.1.5&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.3&sourceBranch=refs/tags/@jotunheim/react-button@8.1.4&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.2&sourceBranch=refs/tags/@jotunheim/react-button@8.1.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.1&sourceBranch=refs/tags/@jotunheim/react-button@8.1.2&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-button

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.1.0&sourceBranch=refs/tags/@jotunheim/react-button@8.1.1&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-button

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.16&sourceBranch=refs/tags/@jotunheim/react-button@8.1.0&targetRepoId=1246) (2023-02-06)

### Features

- add missing className prop to IconButton component ([NPM-1243](https://jiraosl.firmglobal.com/browse/NPM-1243)) ([ea9e19e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ea9e19e2b00b560a032d85ed3318c86063f9bc2b))

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.15&sourceBranch=refs/tags/@jotunheim/react-button@8.0.16&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.14&sourceBranch=refs/tags/@jotunheim/react-button@8.0.15&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.13&sourceBranch=refs/tags/@jotunheim/react-button@8.0.14&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.11&sourceBranch=refs/tags/@jotunheim/react-button@8.0.13&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.11&sourceBranch=refs/tags/@jotunheim/react-button@8.0.12&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.10&sourceBranch=refs/tags/@jotunheim/react-button@8.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.9&sourceBranch=refs/tags/@jotunheim/react-button@8.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.7&sourceBranch=refs/tags/@jotunheim/react-button@8.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.7&sourceBranch=refs/tags/@jotunheim/react-button@8.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.4&sourceBranch=refs/tags/@jotunheim/react-button@8.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.4&sourceBranch=refs/tags/@jotunheim/react-button@8.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.4&sourceBranch=refs/tags/@jotunheim/react-button@8.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.3&sourceBranch=refs/tags/@jotunheim/react-button@8.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.2&sourceBranch=refs/tags/@jotunheim/react-button@8.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-button

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@8.0.0&sourceBranch=refs/tags/@jotunheim/react-button@8.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-button

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.11&sourceBranch=refs/tags/@jotunheim/react-button@8.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Button ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([0930cfb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0930cfbc444bf9ab92ade82dab626988733c9190))
- remove className prop of IconButton ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([1073efa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1073efa43084904dc91045aade4d5957b93e2c55))
- remove default theme from Button ([NPM-1064](https://jiraosl.firmglobal.com/browse/NPM-1064)) ([b0aeb35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b0aeb35f50a6eeee4302ef9ba1efbb54ffd22d77))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.
- As part of NPM-925 we remove className props from components.

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.10&sourceBranch=refs/tags/@jotunheim/react-button@7.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.9&sourceBranch=refs/tags/@jotunheim/react-button@7.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.8&sourceBranch=refs/tags/@jotunheim/react-button@7.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.6&sourceBranch=refs/tags/@jotunheim/react-button@7.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.3&sourceBranch=refs/tags/@jotunheim/react-button@7.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.2&sourceBranch=refs/tags/@jotunheim/react-button@7.0.3&targetRepoId=1246) (2022-07-15)

### Bug Fixes

- clean up ie11 only styles ([NPM-822](https://jiraosl.firmglobal.com/browse/NPM-822)) ([75e363d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/75e363db8cd8a9767da50a62656bff3cf3b1fae6))

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.1&sourceBranch=refs/tags/@jotunheim/react-button@7.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-button

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-button@7.0.0&sourceBranch=refs/tags/@jotunheim/react-button@7.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-button

# 7.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [6.4.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.34&sourceBranch=refs/tags/@confirmit/react-button@6.4.35&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.33&sourceBranch=refs/tags/@confirmit/react-button@6.4.34&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.32&sourceBranch=refs/tags/@confirmit/react-button@6.4.33&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [6.4.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.31&sourceBranch=refs/tags/@confirmit/react-button@6.4.32&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.30&sourceBranch=refs/tags/@confirmit/react-button@6.4.31&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.28&sourceBranch=refs/tags/@confirmit/react-button@6.4.29&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.27&sourceBranch=refs/tags/@confirmit/react-button@6.4.28&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.26&sourceBranch=refs/tags/@confirmit/react-button@6.4.27&targetRepoId=1246) (2022-03-29)

### Bug Fixes

- Removing autoprefixer autoplace ([NPM-993](https://jiraosl.firmglobal.com/browse/NPM-993)) ([3677ce6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3677ce6d0caf2fd3533d7bd7d26f8ecad10148db))

## [6.4.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.25&sourceBranch=refs/tags/@confirmit/react-button@6.4.26&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.24&sourceBranch=refs/tags/@confirmit/react-button@6.4.25&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.23&sourceBranch=refs/tags/@confirmit/react-button@6.4.24&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.22&sourceBranch=refs/tags/@confirmit/react-button@6.4.23&targetRepoId=1246) (2022-02-22)

### Bug Fixes

- Allow mouse-events on disabled IconButton ([NPM-890](https://jiraosl.firmglobal.com/browse/NPM-890)) ([70ef22c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70ef22c66dba909322bdb630936559ec0e3b8f67))

## [6.4.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.20&sourceBranch=refs/tags/@confirmit/react-button@6.4.21&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.19&sourceBranch=refs/tags/@confirmit/react-button@6.4.20&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.18&sourceBranch=refs/tags/@confirmit/react-button@6.4.19&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.17&sourceBranch=refs/tags/@confirmit/react-button@6.4.18&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.16&sourceBranch=refs/tags/@confirmit/react-button@6.4.17&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.15&sourceBranch=refs/tags/@confirmit/react-button@6.4.16&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.14&sourceBranch=refs/tags/@confirmit/react-button@6.4.15&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.13&sourceBranch=refs/tags/@confirmit/react-button@6.4.14&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.11&sourceBranch=refs/tags/@confirmit/react-button@6.4.12&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.10&sourceBranch=refs/tags/@confirmit/react-button@6.4.11&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.9&sourceBranch=refs/tags/@confirmit/react-button@6.4.10&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.8&sourceBranch=refs/tags/@confirmit/react-button@6.4.9&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.7&sourceBranch=refs/tags/@confirmit/react-button@6.4.8&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.6&sourceBranch=refs/tags/@confirmit/react-button@6.4.7&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.5&sourceBranch=refs/tags/@confirmit/react-button@6.4.6&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.4&sourceBranch=refs/tags/@confirmit/react-button@6.4.5&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.3&sourceBranch=refs/tags/@confirmit/react-button@6.4.4&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.2&sourceBranch=refs/tags/@confirmit/react-button@6.4.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.1&sourceBranch=refs/tags/@confirmit/react-button@6.4.2&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-button

## [6.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.4.0&sourceBranch=refs/tags/@confirmit/react-button@6.4.1&targetRepoId=1246) (2021-06-15)

### Bug Fixes

- wrap JumboButton in forwardRef, prevent navigation and onClick from firing on when JumboButton is disabled ([NPM-807](https://jiraosl.firmglobal.com/browse/NPM-807)) ([58ddd06](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/58ddd06dd5a67609f5fef857339e96cb470e12f8))

# [6.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.3.2&sourceBranch=refs/tags/@confirmit/react-button@6.4.0&targetRepoId=1246) (2021-06-14)

### Features

- add JumboButton component ([NPM-802](https://jiraosl.firmglobal.com/browse/NPM-802)) ([5b7df29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5b7df291ed832d0c72166618f8fadde791171dcb))

## [6.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.3.1&sourceBranch=refs/tags/@confirmit/react-button@6.3.2&targetRepoId=1246) (2021-05-25)

### Bug Fixes

- add data, id and aria attributes to ButtonRow when they are passed ([NPM-789](https://jiraosl.firmglobal.com/browse/NPM-789)) ([c5f4344](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c5f434404d5e02f4c75c4601cff1cbcc9e50a3f0))

## [6.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.3.0&sourceBranch=refs/tags/@confirmit/react-button@6.3.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [6.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.2.0&sourceBranch=refs/tags/@confirmit/react-button@6.3.0&targetRepoId=1246) (2021-03-17)

### Bug Fixes

- remove border radius on IconButton small variant since it causes icons to be cropped ([NPM-730](https://jiraosl.firmglobal.com/browse/NPM-730)) ([3d7492e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3d7492ee72bc972313c3bd60945618b27444605f))

### Features

- add ButtonRow component to easily get correct space between umultiple buttons on a row ([NPM-730](https://jiraosl.firmglobal.com/browse/NPM-730)) ([45d0cb7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/45d0cb79ab8a7daf2c08e6d476c06311653bf4a5))

# [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.1.2&sourceBranch=refs/tags/@confirmit/react-button@6.2.0&targetRepoId=1246) (2021-02-15)

### Bug Fixes

- banner appearance on IconButton had incorrect non-hover color ([NPM-705](https://jiraosl.firmglobal.com/browse/NPM-705)) ([c9ea9a1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c9ea9a1fac658554004a3361a48f71cb12a3cefc))

### Features

- expose IconButtonAppearance enum ([NPM-705](https://jiraosl.firmglobal.com/browse/NPM-705)) ([6669077](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/666907759ec5eb5e873377b632ea690e1565b22c))

## [6.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.1.1&sourceBranch=refs/tags/@confirmit/react-button@6.1.2&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-button

## [6.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.1.0&sourceBranch=refs/tags/@confirmit/react-button@6.1.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-button

# [6.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@6.0.1&sourceBranch=refs/tags/@confirmit/react-button@6.1.0&targetRepoId=1246) (2020-12-09)

### Features

- add small size for IconButton. ([NPM-443](https://jiraosl.firmglobal.com/browse/NPM-443)) ([d88c66b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d88c66b4d4f965eaaf3e633d2d1d9d9fee720d30))

### v6.0.1

- Fix: `tertiary` appearance should only have 4px margin for prefix and suffix.

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@5.0.0&sourceBranch=refs/tags/@confirmit/react-button@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([70c6fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70c6fc66e591517275e65be695c787ec215a9841))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@4.2.3&sourceBranch=refs/tags/@confirmit/react-button@5.0.0&targetRepoId=1246) (2020-10-21)

### Features

- expose type prop on IconButton, and follow same behavior as Button ([NPM-549](https://jiraosl.firmglobal.com/browse/NPM-549)) ([d3b8b8b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d3b8b8be5dfdb6cd15293785738c6eaf9021dc5a))
- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. ([NPM-549](https://jiraosl.firmglobal.com/browse/NPM-549)) ([ec92807](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec92807ea763c63d503963c3a3a42cedd6d863a6))

### BREAKING CHANGES

- meaning of the `type` prop has changed
- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. (NPM-549)

## [4.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-button@4.2.2&sourceBranch=refs/tags/@confirmit/react-button@4.2.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-button

## [4.2.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-button@4.2.1...@confirmit/react-button@4.2.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-button

## CHANGELOG

### v4.2.0

- Feat: Typescript support.

### v4.1.0

- Feat: `tertiary` appearance added.

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.1.0

- Removing package version in class names.

### v3.0.1

- support new prop "target"
- set "rel" attribute if it renders as an anchor

### v3.0.0

- **BREAKING**:
  - Layout changes:
    - Height of buttons is now 40px, compared to previous 35px. (Material Theme only)
    - IconButton changed from `display: flex` to `display: inline-flex`.
  - Deprecate `type` prop. This is also a html atrribute, and has caused confusion in usage and it has been used as the html attribute instead of changing appearance of the button. The old `type` usage will be mapped to the new `appearance` prop. Some old button appearances are no longer available, and will be mapped to the closest matching appearance.
    - `primary` -> `primarySuccess`
    - `primaryInvert` -> `secondarySuccess`
    - `danger` -> `primaryDanger`
    - `success` -> `secondarySuccess`
    - `secondary` -> `secondaryNeutral`
    - `link` -> `secondaryNeutral`
  - Default button changed: `type = types.secondary` -> `appearance = appearances.secondaryNeutral`
  - New props:
    - `appearance` - replaces `type`. This also supports new button variants:
      - `primaryNeutral`
      - `primarySuccess`
      - `primaryDanger`
      - `secondaryNeutral`
      - `secondarySuccess` - no support in Default theme
      - `secondaryDanger` - no support in Default theme
      - `secondaryBanner` - no support in Default theme
    - `fullWidth`: will stretch the button to 100% of its container, and center content
    - `prefix`: add prefix to content, usually used for icons
    - `suffix`: add suffix to content, usually used for icons
  - Removed props:
    - `inline`
  - Fix:
    - Styling for disabled anchors
    - Improved ripple effect on click
  - IconButton new prop `appearance`, accepts `default` and `banner`. `banner` will turn icon white.

### v2.0.6

- Refactor: update color variables

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

## v1.1.0

- Feat: Converted to forwardRef components, in order to allow refs on the rendered element

## v1.0.3

- Fix: add `flex: none` to `IconButton` to prevent it from shrinking when inside a flex container

## v1.0.1

- Fix: adding extractOnEventProps to button

## v1.0.0

- BREAKING: Removing props:
  - baseClassName
  - classNames
  - raised
  - size
  - componentClass
    - if an `href` is present, element should be `a`
    - If `href` is not present, element should be `button`
- Feat: now using extractDataAriaIdProps
- Feat: now using useTheme hook
- Feat: adding `disabled` styling to IconButton for material- and default-theme

### v0.1.0

- Fix: Add color style for tab buttons for pseudo selectors link, visited, hover, active to avoid issues with color changing based on styles set globally on `a` elements
- **BREAKING**
  - Added `display: inline-flex` and `align-items: center` to `comd-button__content` to align items properly

### v0.0.9

- Fix: Add simple styling for default theme IconButton component

### v0.0.4

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
