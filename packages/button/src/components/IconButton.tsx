import React, {MouseEventHandler, ReactNode} from 'react';
import cn from 'classnames';

import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import SimpleLink from '@jotunheim/react-simple-link';
import {bemFactory} from '@jotunheim/react-themes';

import {ButtonTypes, ButtonType} from '../utils/appearance';

import classNames from './IconButton.module.css';

const {block, modifier} = bemFactory('comd-icon-button', classNames);

export enum IconButtonAppearance {
  banner = 'banner',
  default = 'default',
}

type Appearance = IconButtonAppearance | 'banner' | 'default';

enum Sizes {
  small = 'small',
  standard = 'standard',
}

type Size = Sizes | 'small' | 'standard';

const preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

export const IconButton: ForwardRefIconButton = React.forwardRef(
  (
    {
      type = ButtonTypes.button,
      disabled,
      href,
      onClick,
      children,
      appearance = IconButtonAppearance.default,
      size = Sizes.standard,
      ...rest
    },
    ref
  ) => {
    const isSmall = size === Sizes.small;

    const classes = cn(block(), modifier(appearance), {
      [modifier('disabled')]: disabled,
      [modifier('small')]: isSmall,
    });
    const isLink = !!href;

    const LinkComponent = isLink ? SimpleLink : 'button';

    return (
      <LinkComponent
        data-testid="icon-button"
        ref={ref}
        className={classes}
        disabled={disabled}
        type={!isLink ? type : undefined}
        href={href}
        onClick={disabled ? preventDefault : onClick}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}>
        {children}
      </LinkComponent>
    );
  }
) as ForwardRefIconButton;

export type IconButtonProps = {
  id?: string;
  type?: ButtonType;
  disabled?: boolean;
  href?: string;
  className?: string;
  onClick?: MouseEventHandler;
  children: ReactNode;
  appearance?: Appearance;
  size?: Size;
};

type ForwardRefIconButton = React.ForwardRefExoticComponent<
  IconButtonProps & React.RefAttributes<HTMLInputElement>
> & {
  appearances: typeof IconButtonAppearance;
  types: typeof ButtonTypes;
  sizes: typeof Sizes;
};

IconButton.types = ButtonTypes;
IconButton.appearances = IconButtonAppearance;
IconButton.displayName = 'IconButton';
IconButton.sizes = Sizes;

export default IconButton;
