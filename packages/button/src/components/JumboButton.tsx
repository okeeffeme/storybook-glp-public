import React, {
  Ref,
  AriaAttributes,
  MouseEventHandler,
  ReactNode,
  forwardRef,
} from 'react';
import cn from 'classnames';

import Icon from '@jotunheim/react-icons';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import SimpleLink from '@jotunheim/react-simple-link';

import classNames from './JumboButton.module.css';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-jumbo-button',
  classNames,
});

type JumboButtonProps = {
  title: ReactNode;
  text?: ReactNode;
  iconPath?: string;
  imageSrc?: string;
  onClick?: MouseEventHandler;
  disabled?: boolean;
  href?: string;
} & AriaAttributes;

type ForwardRefJumboButton = React.ForwardRefExoticComponent<
  JumboButtonProps & React.RefAttributes<HTMLElement>
>;

const preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

export const JumboButton: ForwardRefJumboButton = forwardRef(
  (
    {
      title,
      text,
      iconPath,
      imageSrc,
      href,
      onClick,
      disabled,
      ...rest
    }: JumboButtonProps,
    ref: Ref<HTMLElement>
  ) => {
    const classes = cn(block(), {
      [modifier('disabled')]: disabled,
    });

    const isLink = Boolean(href);
    const LinkComponent = isLink ? SimpleLink : 'button';

    return (
      <LinkComponent
        data-testid="jumbo-button"
        ref={ref}
        href={href}
        rel={isLink ? 'noopener noreferrer' : undefined}
        className={classes}
        onClick={disabled ? preventDefault : onClick}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}>
        <div
          className={cn(element('graphic'), {
            [element('graphic', 'disabled')]: disabled,
          })}>
          {iconPath ? (
            <Icon path={iconPath} size={60} />
          ) : (
            imageSrc && <img className={element('img')} src={imageSrc} />
          )}
        </div>
        <div
          className={cn(element('text-content'), {
            [element('text-content', 'disabled')]: disabled,
          })}>
          <div className={cn(element('text'), element('text', 'title'))}>
            {title}
          </div>
          {text && (
            <div data-test-jumbo-button="text" className={element('text')}>
              {text}
            </div>
          )}
        </div>
      </LinkComponent>
    );
  }
) as ForwardRefJumboButton;

export default JumboButton;
