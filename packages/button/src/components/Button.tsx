import React, {MouseEventHandler, ReactNode, Ref} from 'react';
import cn from 'classnames';

import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import SimpleLink from '@jotunheim/react-simple-link';

import classNames from './Button.module.css';

const {block, element, modifier} = bemFactory('comd-button', classNames);

import {
  Appearances,
  ButtonTypes,
  ButtonType,
  Appearance,
} from '../utils/appearance';

const preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

export const Button: ForwardRefButton = React.forwardRef(
  (
    {
      type = ButtonTypes.button,
      disabled,
      href,
      target,
      onClick,
      children,
      prefix,
      suffix,
      appearance = Appearances.secondaryNeutral,
      fullWidth = false,
      ...rest
    }: ButtonProps,
    ref: Ref<HTMLElement>
  ) => {
    const classes = cn(block(), modifier(appearance), {
      [modifier('disabled')]: disabled,
      [modifier('full-width')]: fullWidth,
    });

    const isLink = !!href;
    const LinkComponent = isLink ? SimpleLink : 'button';

    return (
      <LinkComponent
        data-testid="button"
        ref={ref}
        className={classes}
        disabled={disabled}
        href={href}
        target={target}
        rel={isLink ? 'noopener noreferrer' : undefined}
        type={!isLink ? type : undefined}
        onClick={disabled ? preventDefault : onClick}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}>
        <span className={element('content')}>
          {prefix && (
            <span
              className={cn(
                element('decorator'),
                element('decorator', 'prefix')
              )}>
              {prefix}
            </span>
          )}
          {children}
          {suffix && (
            <span
              className={cn(
                element('decorator'),
                element('decorator', 'suffix')
              )}>
              {suffix}
            </span>
          )}
        </span>
      </LinkComponent>
    );
  }
) as ForwardRefButton;

export type ButtonProps = {
  id?: string;
  type?: ButtonType;
  disabled?: boolean;
  href?: string;
  target?: string;
  onClick?: MouseEventHandler;
  children: ReactNode;
  prefix?: ReactNode;
  suffix?: ReactNode;
  appearance?: Appearance;
  fullWidth?: boolean;
};

type ForwardRefButton = React.ForwardRefExoticComponent<
  ButtonProps & React.RefAttributes<HTMLElement>
> & {
  appearances: typeof Appearances;
  types: typeof ButtonTypes;
};

Button.types = ButtonTypes;
Button.appearances = Appearances;
Button.displayName = 'Button';

export default Button;
