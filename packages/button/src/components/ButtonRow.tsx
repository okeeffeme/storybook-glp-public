import React, {ReactNode} from 'react';

import {bemFactory} from '@jotunheim/react-themes';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import classNames from './ButtonRow.module.css';

const {block} = bemFactory('comd-button-row', classNames);

type ButtonRowProps = {
  children?: ReactNode;
};

/* istanbul ignore next */
export const ButtonRow = ({children, ...rest}: ButtonRowProps) => {
  return (
    <div
      className={block()}
      data-testid="button-row"
      {...extractDataAriaIdProps(rest)}>
      {children}
    </div>
  );
};
