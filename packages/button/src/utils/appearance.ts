export type ButtonType = ButtonTypes | 'button' | 'submit' | 'reset';

export enum ButtonTypes {
  button = 'button',
  reset = 'reset',
  submit = 'submit',
}

export enum Appearances {
  primarySuccess = 'primary-success',
  primaryNeutral = 'primary-neutral',
  primaryDanger = 'primary-danger',
  secondarySuccess = 'secondary-success',
  secondaryNeutral = 'secondary-neutral',
  secondaryDanger = 'secondary-danger',
  secondaryBanner = 'secondary-banner',
  tertiary = 'tertiary',
}

export type Appearance =
  | Appearances
  | 'primary-success'
  | 'primary-neutral'
  | 'primary-danger'
  | 'secondary-success'
  | 'secondary-neutral'
  | 'secondary-danger'
  | 'secondary-banner'
  | 'tertiary';
