import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {JumboButton} from '../../src';
import {pencil} from '../../../icons/src';

describe('JumboButton :: ', () => {
  it('should add data- attributes to dom element when passed', () => {
    render(<JumboButton title="Jumbo button" data-test="attribute" />);

    expect(screen.getByRole('button')).toContainHTML('data-test="attribute"');
  });

  it('should render with an anchor if provided a href', () => {
    render(<JumboButton title="Jumbo button" href="/link" />);
    expect(screen.getByRole('link')).toHaveAttribute('href', '/link');
    expect(screen.getByRole('link')).toHaveProperty(
      'rel',
      'noopener noreferrer'
    );
  });

  it('should NOT render with an anchor if NOT provided a link', () => {
    render(<JumboButton title="Jumbo button" />);

    expect(screen.queryByRole('link')).not.toBeInTheDocument();
    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(screen).toHaveProperty('rel', undefined);
  });

  it('should not render img when iconPath is passed', () => {
    render(<JumboButton title="Jumbo button" iconPath={pencil} />);
    expect(screen.queryByRole('img')).not.toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('should not render img when iconPath is passed', () => {
    render(<JumboButton title="Jumbo button" imageSrc="image.svg" />);

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
    expect(screen.getByRole('img')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', 'image.svg');
  });

  it('should render text content when passed', () => {
    render(<JumboButton title="Jumbo button" text="some text" />);
    expect(screen.getByText('some text')).toBeInTheDocument();
  });

  it('should not render text content when not passed', () => {
    render(<JumboButton title="Jumbo button" />);

    expect(screen.queryByText('some text')).not.toBeInTheDocument();
  });

  it('should add disabled modifiers when disabled is true', () => {
    render(<JumboButton title="Jumbo button" disabled={true} />);
    expect(screen.getByRole('button')).toHaveClass(
      'comd-jumbo-button--disabled'
    );
    expect(
      screen
        .getByRole('button')
        .getElementsByClassName('comd-jumbo-button__graphic--disabled')
    ).toBeTruthy();
    expect(
      screen
        .getByRole('button')
        .getElementsByClassName('comd-jumbo-button__text-content--disabled')
    ).toBeTruthy();
  });

  it('should not add disabled modifiers when disabled is false', () => {
    render(<JumboButton title="Jumbo button" disabled={false} />);

    expect(screen.queryByRole('button')).not.toHaveClass(
      'comd-jumbo-button--disabled'
    );
    expect(screen.queryByRole('button')).not.toHaveClass(
      'comd-jumbo-button__graphic--disabled'
    );
    expect(screen.queryByRole('button')).not.toHaveClass(
      'comd-jumbo-button__text-content--disabled'
    );
  });

  it('should not fire onClick callback when disabled', () => {
    const click = jest.fn();

    render(
      <JumboButton title="Jumbo button" disabled={true} onClick={click} />
    );
    userEvent.click(screen.getByRole('button'));
    expect(click).toHaveBeenCalledTimes(0);
  });

  it('should fire onClick callback when not disabled', () => {
    const click = jest.fn();

    render(
      <JumboButton title="Jumbo button" disabled={false} onClick={click} />
    );

    userEvent.click(screen.getByRole('button'));
    expect(click).toHaveBeenCalledTimes(1);
  });
});
