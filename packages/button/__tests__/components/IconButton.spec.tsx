import React from 'react';
import {render, screen} from '@testing-library/react';
import {IconButton} from '../../src/components/IconButton';
import userEvent from '@testing-library/user-event';

describe('Button', () => {
  describe(':: Types', () => {
    it('should include attribute type="button" when rendering as a <button> by default', () => {
      render(<IconButton>I</IconButton>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'button');
    });

    it('should set type to button when type=button is set', () => {
      render(<IconButton type={IconButton.types.button}>I</IconButton>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'button');
    });

    it('should set type to submit when type=submit is set', () => {
      render(<IconButton type={IconButton.types.submit}>I</IconButton>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'submit');
    });

    it('should set type to reset when type=reset is set', () => {
      render(<IconButton type={IconButton.types.reset}>I</IconButton>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'reset');
    });

    it('should not include attribute type="button" when rendering as a <a>', () => {
      render(<IconButton href="#">I</IconButton>);
      expect(screen.getByRole('link')).not.toHaveProperty('type', 'button');
    });

    it('should not include attribute type="submit" when rendering as a <a> even if type attribute is explicitly set', () => {
      render(
        <IconButton href="#" type={IconButton.types.submit}>
          I
        </IconButton>
      );
      expect(screen.getByRole('link')).not.toHaveProperty('type', 'submit');
    });
  });

  it('should render with an anchor if provided a link', () => {
    render(<IconButton href="/link">I</IconButton>);
    expect(screen.getByRole('link')).toHaveAttribute('href', '/link');
  });

  it('should NOT render with an anchor if NOT provided a link', () => {
    render(<IconButton>I</IconButton>);
    expect(screen.queryByRole('link')).not.toBeInTheDocument();
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should call onClick if clicked', () => {
    const click = jest.fn();
    render(<IconButton onClick={click}>I</IconButton>);
    userEvent.click(screen.getByRole('button'));
    expect(click).toBeCalled();
  });

  it('should NOT call onClick if clicked for disabled button', () => {
    const click = jest.fn();
    render(
      <IconButton disabled onClick={click}>
        I
      </IconButton>
    );
    userEvent.click(screen.getByRole('button'));
    expect(click).toHaveBeenCalledTimes(0);
  });

  it('should render with "--small" modifier when size="small" is set', () => {
    render(<IconButton size={IconButton.sizes.small}>I</IconButton>);
    expect(screen.getByRole('button')).toHaveClass('comd-icon-button--small');
  });

  it('should not render with "--small" modifier when size="small" is not set', () => {
    render(<IconButton>I</IconButton>);

    expect(screen.getByRole('button')).not.toHaveClass(
      'comd-icon-button--small'
    );
  });
});
