import React from 'react';
import {render, screen} from '@testing-library/react';
import {ButtonRow} from '../../src';

describe('ButtonRow :: ', () => {
  it('should add data- attributes to dom element when passed', () => {
    render(<ButtonRow data-testid="attribute" />);
    expect(screen.getByTestId('attribute')).toBeInTheDocument();
  });
});
