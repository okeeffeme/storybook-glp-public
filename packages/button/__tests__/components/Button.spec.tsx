import React from 'react';
import {render, screen} from '@testing-library/react';
import {Button} from '../../src/components/Button';
import userEvent from '@testing-library/user-event';

describe('Button', () => {
  describe(':: Types', () => {
    it('should include attribute type="button" when rendering as a <button> by default', () => {
      render(<Button>link</Button>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'button');
    });

    it('should set type to button when type=button is set', () => {
      render(<Button type={Button.types.button}>link</Button>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'button');
    });

    it('should set type to submit when type=submit is set', () => {
      render(<Button type={Button.types.submit}>link</Button>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'submit');
    });

    it('should set type to reset when type=reset is set', () => {
      render(<Button type={Button.types.reset}>link</Button>);
      expect(screen.getByRole('button')).toHaveProperty('type', 'reset');
    });

    it('should not include attribute type="button" when rendering as a <a>', () => {
      render(<Button href="#">link</Button>);
      expect(screen.getByRole('link')).not.toHaveProperty('type', 'button');
    });

    it('should not include attribute type="submit" when rendering as a <a> even if type attribute is explicitly set', () => {
      render(
        <Button href="#" type={Button.types.submit}>
          link
        </Button>
      );
      expect(screen.getByRole('link')).not.toHaveProperty('type', 'submit');
    });
  });

  it('should render with an anchor if provided a link', () => {
    render(<Button href="/link">link</Button>);
    expect(screen.getByRole('link')).toHaveProperty(
      'rel',
      'noopener noreferrer'
    );
    expect(screen.getByRole('link')).toHaveAttribute('href', '/link');
  });

  it('should NOT render with an anchor if NOT provided a link', () => {
    render(<Button>button</Button>);
    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(screen).toHaveProperty('rel', undefined);
    expect(screen.queryByRole('link')).not.toBeInTheDocument();
  });

  it('should call onClick if clicked', () => {
    const click = jest.fn();
    render(<Button onClick={click}>clickable</Button>);
    userEvent.click(screen.getByRole('button'));
    expect(click).toBeCalled();
  });

  it('should NOT call onClick if clicked for disabled button', () => {
    const click = jest.fn();
    render(
      <Button disabled onClick={click}>
        disabled
      </Button>
    );
    userEvent.click(screen.getByRole('button'));
    expect(click).toHaveBeenCalledTimes(0);
  });

  it('should not add disabled modifier class when disabled not is set', () => {
    render(<Button>button</Button>);
    expect(screen.getByRole('button')).not.toHaveAttribute('disabled');
  });

  it('should add disabled modifier class when disabled is set', () => {
    render(<Button disabled={true}>button</Button>);
    expect(screen.getByRole('button')).toHaveAttribute('disabled');
  });

  it('should not add full-width modifier class when fullWidth not is set', () => {
    render(<Button>button</Button>);
    expect(screen.getByRole('button')).not.toHaveClass(
      'comd-button--full-width'
    );
  });

  it('should add full-width modifier class when fullWidth is set', () => {
    render(<Button fullWidth={true}>button</Button>);
    expect(screen.getByRole('button')).toHaveClass('comd-button--full-width');
  });

  it('should not render prefix when prefix is not set', () => {
    render(<Button>button</Button>);
    expect(screen.getByRole('button')).not.toHaveClass(
      'comd-button__decorator--prefix'
    );
  });

  it('should not render suffix when suffix is not set', () => {
    render(<Button>button</Button>);
    expect(screen.getByRole('button')).not.toHaveClass(
      'comd-button__decorator--suffix'
    );
  });

  it('should render prefix when prefix is set', () => {
    render(<Button prefix="P">button</Button>);

    expect(screen.getByText('P')).toHaveClass('comd-button__decorator--prefix');
  });

  it('should render suffix when suffix is set', () => {
    render(<Button suffix="P">button</Button>);
    expect(screen.getByText('P')).toHaveClass('comd-button__decorator--suffix');
  });
});
