import React from 'react';
import {render, screen} from '@testing-library/react';
import SimpleTextField from '../../src/components/SimpleTextField';
import {ThemeContext, theme} from '../../../themes/src';
import userEvent from '@testing-library/user-event';

describe('Jotunheim React Simple Text Field :: ', () => {
  it('should attach classNames based on theme - material', () => {
    const onChange = jest.fn();

    render(
      <SimpleTextField
        onChange={onChange}
        value={''}
        className={'comd-simple-text-field'}
      />,
      {
        wrapper: ({children}: {children: React.ReactNode}) => (
          <ThemeContext.Provider value={theme.themeNames.material}>
            {children}
          </ThemeContext.Provider>
        ),
      }
    );

    expect(
      screen
        .getByTestId('simple-text-field')
        .classList.contains('comd-simple-text-field')
    ).toBe(true);
  });

  it('should attach classNames based on theme - default', () => {
    const onChange = jest.fn();

    render(
      <SimpleTextField
        onChange={onChange}
        value={''}
        className={'co-simple-text-field'}
      />,
      {
        wrapper: ({children}: {children: React.ReactNode}) => (
          <ThemeContext.Provider value={theme.themeNames.default}>
            {children}
          </ThemeContext.Provider>
        ),
      }
    );

    expect(
      screen
        .getByTestId('simple-text-field')
        .classList.contains('co-simple-text-field')
    ).toBe(true);
  });

  it('should handle onChange', () => {
    const onChange = jest.fn();

    render(<SimpleTextField onChange={onChange} value={''} />);

    userEvent.type(screen.getByTestId('simple-text-field'), 'hello');

    expect(onChange).toHaveBeenCalledTimes(5);
  });
});
