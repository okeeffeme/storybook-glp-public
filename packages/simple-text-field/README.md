# Jotunheim React Simple Text Field

### **This component is for internal use (inside this repo) ONLY**

## DESCRIPTION

React Simple Text Field aims to be the building block of present and future input components;
providing both efficiency and a basis for consistent styling (eventually).
