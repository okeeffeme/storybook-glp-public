# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.1.0&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.1.1&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-simple-text-field

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.4&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.1.0&targetRepoId=1246) (2023-01-11)

### Features

- add test id for SimpleTextField component ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([68aefb8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/68aefb86b7ebaa6af981627f7a9c9a67ee3de0de))

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.3&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.4&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-simple-text-field

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.2&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.3&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-simple-text-field

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.1&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-simple-text-field

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.0&sourceBranch=refs/tags/@jotunheim/react-simple-text-field@5.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-simple-text-field

# 5.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [4.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.5&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [4.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.4&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-simple-text-field

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.3&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.4&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.2&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-simple-text-field

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.1&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@4.0.0&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.1&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-simple-text-field

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@3.0.0&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.0&targetRepoId=1246) (2020-11-13)

### Bug Fixes

- remove default value for maxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([f3580c0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f3580c03ad291eccbd0c959197f18930008c9ec7))

### BREAKING CHANGES

- remove default value for maxLength (NPM-592)

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@3.0.0&sourceBranch=refs/tags/@confirmit/react-simple-text-field@4.0.0&targetRepoId=1246) (2020-11-13)

### Bug Fixes

- remove default value for maxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([f3580c0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f3580c03ad291eccbd0c959197f18930008c9ec7))

### BREAKING CHANGES

- remove default value for maxLength (NPM-592)

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@2.2.2&sourceBranch=refs/tags/@confirmit/react-simple-text-field@3.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

# 2.2.2

### Fix

- Stop event propagation onClick

## [2.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-text-field@2.2.0&sourceBranch=refs/tags/@confirmit/react-simple-text-field@2.2.1&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-simple-text-field

# [2.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-text-field@2.1.0...@confirmit/react-simple-text-field@2.2.0) (2020-08-12)

### Features

- Added `readOnly` prop

# [2.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-simple-text-field@2.0.1...@confirmit/react-simple-text-field@2.1.0) (2020-08-12)

### Features

- add `password` type ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([f5af878](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/f5af8781b8d28bc452dd9e19a571a3b9f1306dd0))

## CHANGELOG

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v1.1.3

- Removed default IE clear button

### v1.1.2

- Fix: Use aria-required attribute on DOM input node instead of required, to avoid built-in browser message

### v1.1.0

- Removing package version in class names.

### v1.0.1

- Fix: Update text color

### v1.0.0

- **BREAKING**:

  - Added styles to this component that match our DS specs for input fields.

### v0.1.0

- Feat: Added min, max, step props

### v0.0.1

- Initial version
