import classNames from './css/SimpleTextField.module.css';

export default {
  simpleTextField: {
    baseClassName: 'comd-simple-text-field',
    classNames,
  },
};
