import React from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import cn from 'classnames';
import componentThemes from '../themes';

type SimpleTextFieldProps = {
  onChange: (value: string | number) => void;
  value: string | number;
  align?: 'left' | 'right';
  autoFocus?: boolean;
  autoComplete?: boolean;
  className?: string;
  disabled?: boolean;
  readOnly?: boolean;
  hasPrefix?: boolean;
  hasSuffix?: boolean;
  id?: string;
  max?: number;
  maxLength?: number;
  min?: number;
  name?: string;
  onBlur?: React.FocusEventHandler;
  onFocus?: React.FocusEventHandler;
  onKeyDown?: React.KeyboardEventHandler;
  onKeyPress?: React.KeyboardEventHandler;
  onKeyUp?: React.KeyboardEventHandler;
  onPaste?: React.ClipboardEventHandler;
  placeholder?: string;
  required?: boolean;
  step?: number;
  type?: 'text' | 'number' | 'password' | 'search';
};

const noop = () => {};

const stopPropagation = (e) => e.stopPropagation();

const SimpleTextField = React.forwardRef(
  (
    {
      align = 'left',
      autoFocus = false,
      autoComplete = false,
      className = '',
      disabled = false,
      readOnly = false,
      hasPrefix,
      hasSuffix,
      id,
      max,
      maxLength,
      min,
      name = '',
      onChange,
      onBlur = noop,
      onFocus = noop,
      onKeyDown,
      onKeyPress,
      onKeyUp,
      onPaste,
      placeholder = '',
      required = false,
      step,
      type = 'text',
      value = '',
      ...rest
    }: SimpleTextFieldProps,
    ref: React.Ref<HTMLInputElement>
  ) => {
    const {baseClassName, classNames} = useTheme(
      'simpleTextField',
      componentThemes
    );
    const {block, modifier} = bemFactory({baseClassName, classNames});

    return (
      <input
        id={id}
        data-testid="simple-text-field"
        autoFocus={autoFocus}
        autoComplete={autoComplete ? 'on' : 'off'}
        className={cn(className, block(), modifier(`align-${align}`), {
          [modifier('disabled')]: disabled && !readOnly,
          [modifier('readOnly')]: readOnly,
          [modifier('prefix')]: hasPrefix,
          [modifier('suffix')]: hasSuffix,
        })}
        disabled={disabled || readOnly}
        maxLength={maxLength}
        name={name}
        onChange={(e) => onChange(e.target.value)}
        onClick={stopPropagation}
        onBlur={onBlur}
        onFocus={onFocus}
        onKeyDown={onKeyDown}
        onKeyPress={onKeyPress}
        onKeyUp={onKeyUp}
        onPaste={onPaste}
        placeholder={placeholder}
        ref={ref}
        aria-required={required}
        value={value}
        step={step}
        min={min}
        max={max}
        type={type}
        data-simple-text-field
        {...extractDataAndAriaProps(rest)}
      />
    );
  }
);

SimpleTextField.displayName = 'SimpleTextField';

export default SimpleTextField;
