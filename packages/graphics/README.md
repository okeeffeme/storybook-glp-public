# Jotunheim React Graphics

[Changelog](./CHANGELOG.md)

A package containing the Confirmit Logo, Application icons, and other common graphic elements

## Confirmit Favicon

The favicon was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16) and slightly modified (removal of webapp manifest, ms-tile, and addition of svg favicon)

## Module Federation

If module federation is enabled in your app, you can also use this via Module Federation as well.

```javascript
const FavIcon = lazy(() => import('ds/FavIcon'));

<Suspense fallback={<></>}>
  <FavIcon />
</Suspense>;
```

The empty fragment is because we don't want a spinner or anything in this case.
You might also consider wrapping this with an error boundary, to be on the safe side.

### Install instructions

Copy the entire "favicon" folder to `public-html-root/favicon/` folder. To verify that it worked, you should be able to visit f.ex `https://author.euro.confirmit.com/[YOUR-APP]/favicon/favicon.ico` and the favicon should appear.

To copy the files, you can use [CopyWebpackPlugin](https://webpack.js.org/plugins/copy-webpack-plugin/). Example implementation:

```javascript
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  // ... more webpack configuration ...
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: 'node_modules/@confirmit/react-graphics/src/favicon',
          to: 'public/favicon',
        },
      ],
    }),
  ],
  // ... more webpack configuration ...
};
```

Insert the following code in the `head` section of your pages (replace `[PUBLIC_PATH]` with the needs for your app. Keep in mind this could be different on development vs production, depending on your setup.):

```html
<link
  rel="apple-touch-icon"
  sizes="180x180"
  href="[PUBLIC_PATH]/favicon/apple-touch-icon.png?v=2"
/>
<link
  rel="icon"
  type="image/png"
  sizes="32x32"
  href="[PUBLIC_PATH]/favicon/favicon-32x32.png?v=2"
/>
<link
  rel="icon"
  type="image/png"
  sizes="16x16"
  href="[PUBLIC_PATH]/favicon/favicon-16x16.png?v=2"
/>
<link rel="icon" href="[PUBLIC_PATH]/favicon/favicon.ico?v=2" />
<link
  rel="icon"
  href="[PUBLIC_PATH]/favicon/favicon.svg?v=2"
  type="image/svg+xml"
/>
```

PS: You might not need all of these. If you browser target is the "modern" evergreen browsers, then this should be enough:

```html
<link
  rel="apple-touch-icon"
  sizes="180x180"
  href="[PUBLIC_PATH]/favicon/apple-touch-icon.png?v=2"
/>
<link
  rel="icon"
  href="[PUBLIC_PATH]/favicon/favicon.svg?v=2"
  type="image/svg+xml"
/>
```
