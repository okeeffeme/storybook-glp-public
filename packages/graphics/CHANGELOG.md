# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.3.1&sourceBranch=refs/tags/@jotunheim/react-graphics@1.3.2&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-graphics

## [1.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.3.0&sourceBranch=refs/tags/@jotunheim/react-graphics@1.3.1&targetRepoId=1246) (2022-11-09)

### Bug Fixes

- prevent aria props usage with svg files and replace with component prop value ([NPM-1114](https://jiraosl.firmglobal.com/browse/NPM-1114)) ([68e25f6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/68e25f614ab42f48d6b11a65de0c10001d057ddb))

# [1.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.2.0&sourceBranch=refs/tags/@jotunheim/react-graphics@1.3.0&targetRepoId=1246) (2022-11-04)

### Features

- shift react-graphics to rtl ([NPM-1109](https://jiraosl.firmglobal.com/browse/NPM-1109)) ([90acc78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/90acc7802397e64791d10bdd4395ac92d45ac16c))

# [1.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.1.2&sourceBranch=refs/tags/@jotunheim/react-graphics@1.2.0&targetRepoId=1246) (2022-10-21)

### Features

- add Workflows icon ([HUB-9776](https://jiraosl.firmglobal.com/browse/HUB-9776)) ([f18f12c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f18f12c2517ff625e3fe613b2b1c6dd0f61af58e))

## [1.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.1.1&sourceBranch=refs/tags/@jotunheim/react-graphics@1.1.2&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-graphics

## [1.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.1.0&sourceBranch=refs/tags/@jotunheim/react-graphics@1.1.1&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-graphics

# [1.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-graphics@1.0.0&sourceBranch=refs/tags/@jotunheim/react-graphics@1.1.0&targetRepoId=1246) (2022-09-09)

### Features

- Add icon for PG Fusion ([HUB-9388](https://jiraosl.firmglobal.com/browse/HUB-9388)) ([acf2f37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/acf2f3797f283357ff8c4d78852afd62a3a126be))

# 1.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [0.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.5.1&sourceBranch=refs/tags/@confirmit/react-graphics@0.5.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [0.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.5.0&sourceBranch=refs/tags/@confirmit/react-graphics@0.5.1&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-graphics

# [0.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.3.0&sourceBranch=refs/tags/@confirmit/react-graphics@0.4.0&targetRepoId=1246) (2021-11-29)

### Features

- SurveyLayouts app icon added ([SE-3497](https://jiraosl.firmglobal.com/browse/SE-3497)) ([69439fe](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/69439fe3208de80b2923dea4a23f1225d4f01551))

# [0.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.2.1&sourceBranch=refs/tags/@confirmit/react-graphics@0.3.0&targetRepoId=1246) (2021-11-01)

### Features

- add MinimalWhite version of logo ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([218a164](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/218a16437cd7cb3591a356f8a6f503ce99f61619))
- introduce new logo for Forsta ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([8015260](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/80152601ef816b6fa8d17179150984994a04e366))
- update to forsta favicon ([NPM-862](https://jiraosl.firmglobal.com/browse/NPM-862)) ([8622072](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8622072e12b47a5de899cf2976b05f013b8bf1a7))

## [0.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.2.0&sourceBranch=refs/tags/@confirmit/react-graphics@0.2.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-graphics@0.1.0&sourceBranch=refs/tags/@confirmit/react-graphics@0.2.0&targetRepoId=1246) (2021-01-24)

### Features

- adding Salesforce and Dynamics 365 logos ([NPM-690](https://jiraosl.firmglobal.com/browse/NPM-690)) ([d84f351](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d84f35190cc64191dbe20f917d8b57370ab6b992))

# 0.1.0 (2021-01-04)

### Bug Fixes

- add default size for logos instead of 100% ([NPM-640](https://jiraosl.firmglobal.com/browse/NPM-640)) ([e4cb1f3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e4cb1f30ed378c9f6cad3897c212e99df167dfdb))

### Features

- add new graphics package ([NPM-640](https://jiraosl.firmglobal.com/browse/NPM-640)) ([eced6f4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eced6f4024904885c4fdd11b376211ad42dcf6d7))

## CHANGELOG

### v0.0.1

- Initial version
