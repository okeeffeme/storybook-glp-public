import {ForstaLogo} from './components/ForstaLogo';
import {ConfirmitLogo} from './components/ConfirmitLogo';
import {ApplicationIcon} from './components/ApplicationIcon';
import {ExternalOrganizationLogo} from './components/ExternalOrganizationLogo';

export {ForstaLogo, ConfirmitLogo, ApplicationIcon, ExternalOrganizationLogo};
