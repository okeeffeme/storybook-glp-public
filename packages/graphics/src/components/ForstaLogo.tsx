import React, {SVGAttributes} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import ForstaLogoWhite from '../ForstaLogo/White.svg';
import ForstaPlusLogoWhite from '../ForstaLogo/WhitePlus.svg';
import ForstaLogoNavy from '../ForstaLogo/Navy.svg';
import ForstaMinimalWhite from '../ForstaLogo/MinimalWhite.svg';
import ForstaMinimalNavy from '../ForstaLogo/MinimalNavy.svg';
import ForstaPlusLogoNavy from '../ForstaLogo/NavyPlus.svg';

enum Appearance {
  White = 'White',
  WhitePlus = 'WhitePlus',
  Navy = 'Navy',
  MinimalWhite = 'MinimalWhite',
  MinimalNavy = 'MinimalNavy',
  NavyPlus = 'NavyPlus',
}

export type ForstaLogoType = {
  appearance?: Appearance;
} & SVGAttributes<SVGElement>;

export const ForstaLogo = ({
  appearance,
  width,
  height,
  ...rest
}: ForstaLogoType) => {
  switch (appearance) {
    case Appearance.Navy:
      return (
        <ForstaLogoNavy
          width={width}
          height={height}
          aria-label="ForstaLogoNavyIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.MinimalWhite:
      return (
        <ForstaMinimalWhite
          width={width}
          height={height}
          aria-label="ForstaLogoMinimalWhiteIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.MinimalNavy:
      return (
        <ForstaMinimalNavy
          width={width}
          height={height}
          aria-label="ForstaLogoMinimalNavyIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.WhitePlus:
      return (
        <ForstaPlusLogoWhite
          width={width}
          height={height}
          aria-label="ForstaLogoWhitePlusIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.NavyPlus:
      return (
        <ForstaPlusLogoNavy
          width={width}
          height={height}
          aria-label="ForstaLogoNavyPlusIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.White:
    default:
      return (
        <ForstaLogoWhite
          width={width}
          height={height}
          aria-label="ForstaLogoWhiteIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
  }
};

ForstaLogo.Appearance = Appearance;
