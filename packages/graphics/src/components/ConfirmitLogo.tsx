import React, {SVGAttributes} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import ConfirmitBugOrange from '../ConfirmitLogo/ConfirmitBugOrange.svg';
import ConfirmitBugSlate from '../ConfirmitLogo/ConfirmitBugSlate.svg';
import ConfirmitHorizonsSlate from '../ConfirmitLogo/ConfirmitHorizonsSlate.svg';
import ConfirmitHorizontalSlate from '../ConfirmitLogo/ConfirmitHorizontalSlate.svg';
import ConfirmitStackedSlate from '../ConfirmitLogo/ConfirmitStackedSlate.svg';
import ConfirmitHorizontalOrangeWhite from '../ConfirmitLogo/ConfirmitLogoHorizontalOrangeWhite.svg';

enum Appearance {
  BugOrange = 'BugOrange',
  BugSlate = 'BugSlate',
  HorizonsSlate = 'HorizonsSlate',
  HorizontalSlate = 'HorizontalSlate',
  StackedSlate = 'StackedSlate',
  HorizontalOrangeWhite = 'HorizontalOrangeWhite',
}

export type ConfirmitLogoType = {
  appearance?: Appearance;
} & SVGAttributes<SVGElement>;

export const ConfirmitLogo = ({
  appearance,
  width,
  height,
  ...rest
}: ConfirmitLogoType) => {
  switch (appearance) {
    case Appearance.BugOrange:
      return (
        <ConfirmitBugOrange
          width={width}
          height={height}
          aria-label="ConfirmitBugOrangeIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case Appearance.BugSlate:
      return (
        <ConfirmitBugSlate
          width={width}
          height={height}
          aria-label="ConfirmitBugSlateIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case Appearance.HorizonsSlate:
      return (
        <ConfirmitHorizonsSlate
          width={width}
          height={height}
          aria-label="ConfirmitHorizonsSlateIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case Appearance.HorizontalSlate:
      return (
        <ConfirmitHorizontalSlate
          width={width}
          height={height}
          aria-label="ConfirmitHorizontalSlateIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case Appearance.StackedSlate:
      return (
        <ConfirmitStackedSlate
          width={width}
          height={height}
          aria-label="ConfirmitStackedSlateIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case Appearance.HorizontalOrangeWhite:
    default:
      return (
        <ConfirmitHorizontalOrangeWhite
          width={width}
          height={height}
          aria-label="ConfirmitLogoHorizontalOrangeWhiteIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
  }
};

ConfirmitLogo.Appearance = Appearance;
