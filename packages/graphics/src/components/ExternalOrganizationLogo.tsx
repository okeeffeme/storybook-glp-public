import React, {SVGAttributes} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import Salesforce from '../ExternalOrganizations/Salesforce.svg';
import DynamicsWithCaption from '../ExternalOrganizations/Dynamics365WithCaption.svg';
import Dynamics from '../ExternalOrganizations/Dynamics365.svg';

enum Appearance {
  Salesforce = 'Salesforce',
  DynamicsWithCaption = 'DynamicsWithCaption',
  Dynamics = 'Dynamics',
}

export type ExternalLogoProps = {
  appearance?: Appearance;
} & SVGAttributes<SVGElement>;

export const ExternalOrganizationLogo = ({
  appearance,
  width,
  height,
  ...rest
}: ExternalLogoProps) => {
  switch (appearance) {
    case Appearance.Dynamics:
      return (
        <Dynamics
          width={width}
          height={height}
          aria-label="DynamicsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.DynamicsWithCaption:
      return (
        <DynamicsWithCaption
          width={width}
          height={height}
          aria-label="DynamicsWithCaptionIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    case Appearance.Salesforce:
      return (
        <Salesforce
          width={width}
          height={height}
          aria-label="SalesforceIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
    default:
      return null;
  }
};

ExternalOrganizationLogo.Appearance = Appearance;
