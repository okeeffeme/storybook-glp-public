import React, {SVGAttributes} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import AccountOverviewIcon from '../ApplicationIcon/AccountOverview.svg';
import ActionsIcon from '../ApplicationIcon/Actions.svg';
import ActiveDashboardsIcon from '../ApplicationIcon/ActiveDashboards.svg';
import CatiIcon from '../ApplicationIcon/Cati.svg';
import CrmConnectorForSalesForceIcon from '../ApplicationIcon/CrmConnectorForSalesforce.svg';
import DiscoveryAnalyticsIcon from '../ApplicationIcon/DiscoveryAnalytics.svg';
import EndUserManagementIcon from '../ApplicationIcon/EndUserManagement.svg';
import GeniusSocialIcon from '../ApplicationIcon/GeniusSocial.svg';
import HierarchyManagementIcon from '../ApplicationIcon/HierarchyManagement.svg';
import HomeIcon from '../ApplicationIcon/Home.svg';
import InstantAnalyticsIcon from '../ApplicationIcon/InstantAnalytics.svg';
import ModelBuilderIcon from '../ApplicationIcon/ModelBuilder.svg';
import ProfessionalAuthoringIcon from '../ApplicationIcon/ProfessionalAuthoring.svg';
import ReportalIcon from '../ApplicationIcon/Reportal.svg';
import SmartHubIcon from '../ApplicationIcon/SmartHub.svg';
import StudioIcon from '../ApplicationIcon/Studio.svg';
import SurveyDesignerIcon from '../ApplicationIcon/SurveyDesigner.svg';
import CustomQuestionsIcon from '../ApplicationIcon/CustomQuestions.svg';
import DigitalFeedbackIcon from '../ApplicationIcon/DigitalFeedback.svg';
import SurveyLayoutsIcon from '../ApplicationIcon/SurveyLayouts.svg';
import PGFusionIcon from '../ApplicationIcon/PGFusion.svg';
import WorkflowsIcon from '../ApplicationIcon/Workflows.svg';

import UnknownIcon from '../ApplicationIcon/Unknown.svg';

enum ApplicationId {
  accountoverview = 'accountoverview',
  actionmanagement = 'actionmanagement',
  actions = 'actions',
  activedashboards = 'activedashboards',
  cati = 'cati',
  crmconnectorforsalesforce = 'crmconnectorforsalesforce',
  discoveryanalytics = 'discoveryanalytics',
  endusermanagement = 'endusermanagement',
  geniussocial = 'geniussocial',
  hierarchymanagement = 'hierarchymanagement',
  home = 'home',
  instantanalytics = 'instantanalytics',
  modelbuilder = 'modelbuilder',
  professionalauthoring = 'professionalauthoring',
  reportal = 'reportal',
  smarthub = 'smarthub',
  studio = 'studio',
  surveydesigner = 'surveydesigner',
  customquestions = 'customquestions',
  digitalfeedback = 'digitalfeedback',
  surveylayouts = 'surveylayouts',
  pgfusion = 'pgfusion',
  workflows = 'workflows',
}

type ApplicationIconType = {
  applicationId: ApplicationId;
} & SVGAttributes<SVGElement>;

export const ApplicationIcon = ({
  applicationId,
  width,
  height,
  ...rest
}: ApplicationIconType) => {
  switch (applicationId) {
    case ApplicationId.accountoverview:
      return (
        <AccountOverviewIcon
          width={width}
          height={height}
          aria-label="AccountOverviewIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.actionmanagement:
    case ApplicationId.actions:
      return (
        <ActionsIcon
          width={width}
          height={height}
          aria-label="ActionsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.activedashboards:
      return (
        <ActiveDashboardsIcon
          width={width}
          height={height}
          aria-label="ActiveDashboardsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.cati:
      return (
        <CatiIcon
          width={width}
          height={height}
          aria-label="CatiIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.crmconnectorforsalesforce:
      return (
        <CrmConnectorForSalesForceIcon
          width={width}
          height={height}
          aria-label="CrmConnectorForSalesForceIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.discoveryanalytics:
      return (
        <DiscoveryAnalyticsIcon
          width={width}
          height={height}
          aria-label="DiscoveryAnalyticsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.endusermanagement:
      return (
        <EndUserManagementIcon
          width={width}
          height={height}
          aria-label="EndUserManagementIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.geniussocial:
      return (
        <GeniusSocialIcon
          width={width}
          height={height}
          aria-label="GeniusSocialIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.hierarchymanagement:
      return (
        <HierarchyManagementIcon
          width={width}
          height={height}
          aria-label="HierarchyManagementIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.home:
      return (
        <HomeIcon
          width={width}
          height={height}
          aria-label="HomeIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.instantanalytics:
      return (
        <InstantAnalyticsIcon
          width={width}
          height={height}
          aria-label="InstantAnalyticsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.modelbuilder:
      return (
        <ModelBuilderIcon
          width={width}
          height={height}
          aria-label="ModelBuilderIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.professionalauthoring:
      return (
        <ProfessionalAuthoringIcon
          width={width}
          height={height}
          aria-label="ProfessionalAuthoringIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.reportal:
      return (
        <ReportalIcon
          width={width}
          height={height}
          aria-label="ReportalIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.smarthub:
      return (
        <SmartHubIcon
          width={width}
          height={height}
          aria-label="SmartHubIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.studio:
      return (
        <StudioIcon
          width={width}
          height={height}
          aria-label="StudioIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.surveydesigner:
      return (
        <SurveyDesignerIcon
          width={width}
          height={height}
          aria-label="SurveyDesignerIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.customquestions:
      return (
        <CustomQuestionsIcon
          width={width}
          height={height}
          aria-label="CustomQuestionsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.digitalfeedback:
      return (
        <DigitalFeedbackIcon
          width={width}
          height={height}
          aria-label="DigitalFeedbackIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.surveylayouts:
      return (
        <SurveyLayoutsIcon
          width={width}
          height={height}
          aria-label="SurveyLayoutsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.pgfusion:
      return (
        <PGFusionIcon
          width={width}
          height={height}
          aria-label="PGFusionIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    case ApplicationId.workflows:
      return (
        <WorkflowsIcon
          width={width}
          height={height}
          aria-label="WorkflowsIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );

    default:
      return (
        <UnknownIcon
          width={width}
          height={height}
          aria-label="UnknownIcon"
          {...extractDataAriaIdProps(rest)}
        />
      );
  }
};

ApplicationIcon.ApplicationId = ApplicationId;
