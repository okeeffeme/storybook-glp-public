import React from 'react';
import {screen, render} from '@testing-library/react';

import {ForstaLogo} from '../../src/';

describe('Jotunheim React Graphics :: ', () => {
  describe('ForstaLogo :: ', () => {
    it('should render ForstaLogoWhite logo when no appearance is added', () => {
      render(<ForstaLogo />);

      expect(
        screen.getByRole('img', {name: 'ForstaLogoWhiteIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoNavyIcon version of logo when appearance is Navy', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.Navy} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoNavyIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoWhiteIcon version of logo when appearance is White', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.White} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoWhiteIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoWhitePlusIcon version of logo when appearance is WhitePlus', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.WhitePlus} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoWhitePlusIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoNavyPlusIcon version of logo when appearance is NavyPlus', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.NavyPlus} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoNavyPlusIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoMinimalWhiteIcon version of logo when appearance is MinimalWhite', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.MinimalWhite} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoMinimalWhiteIcon'})
      ).toBeInTheDocument();
    });

    it('should render ForstaLogoMinimalNavyIcon version of logo when appearance is MinimalNavy', () => {
      render(<ForstaLogo appearance={ForstaLogo.Appearance.MinimalNavy} />);

      expect(
        screen.queryByRole('img', {name: 'ForstaLogoMinimalNavyIcon'})
      ).toBeInTheDocument();
    });

    it('should apply correct width and height values to item', () => {
      render(<ForstaLogo width={150} height={190} />);

      const iconEl = screen.getByRole('img', {name: 'ForstaLogoWhiteIcon'});
      expect(iconEl).toHaveAttribute('width', '150');
      expect(iconEl).toHaveAttribute('height', '190');
    });
  });
});
