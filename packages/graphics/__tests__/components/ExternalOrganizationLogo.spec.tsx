import React from 'react';
import {screen, render} from '@testing-library/react';

import {ExternalOrganizationLogo} from '../../src';

describe('Jotunheim React Graphics :: ', () => {
  describe('ExternalOrganizationLogo :: ', () => {
    it('missed or unknown appearance -> render nothing', () => {
      render(<ExternalOrganizationLogo />);

      expect(screen.queryByRole('img')).not.toBeInTheDocument();
    });

    it('appearance Dynamics -> render Dynamics logo without caption', () => {
      render(
        <ExternalOrganizationLogo
          appearance={ExternalOrganizationLogo.Appearance.Dynamics}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'DynamicsIcon'})
      ).toBeInTheDocument();
    });

    it('appearance DynamicsWithCaption -> render Dynamics logo with caption', () => {
      render(
        <ExternalOrganizationLogo
          appearance={ExternalOrganizationLogo.Appearance.DynamicsWithCaption}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'DynamicsWithCaptionIcon'})
      ).toBeInTheDocument();
    });

    it('appearance Salesforce -> render Salesforce logo', () => {
      render(
        <ExternalOrganizationLogo
          appearance={ExternalOrganizationLogo.Appearance.Salesforce}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'SalesforceIcon'})
      ).toBeInTheDocument();
    });

    it('should apply correct width and height values to item', () => {
      render(
        <ExternalOrganizationLogo
          width={150}
          height={190}
          appearance={ExternalOrganizationLogo.Appearance.Salesforce}
        />
      );

      const iconEl = screen.getByRole('img', {name: 'SalesforceIcon'});
      expect(iconEl).toHaveAttribute('width', '150');
      expect(iconEl).toHaveAttribute('height', '190');
    });
  });
});
