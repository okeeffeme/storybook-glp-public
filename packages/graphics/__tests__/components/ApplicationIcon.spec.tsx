import React from 'react';
import {screen, render} from '@testing-library/react';
import {ApplicationIcon} from '../../src';

describe('Jotunheim React Graphics :: ', () => {
  describe('ApplicationIcon :: ', () => {
    it('should render unknown app icon when id is not recognized', () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore  - want to explicitly break the contract of id prop
      render(<ApplicationIcon applicationId="banana" />);

      expect(
        screen.queryByRole('img', {name: 'UnknownIcon'})
      ).toBeInTheDocument();
    });

    it('should render AccountOverview app icon when id is accountoverview', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.accountoverview}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'AccountOverviewIcon'})
      ).toBeInTheDocument();
    });

    it('should render Actions app icon when id is actions', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.actions}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ActionsIcon'})
      ).toBeInTheDocument();
    });

    it('should render Actions app icon when id is actionmanagement', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.actionmanagement}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ActionsIcon'})
      ).toBeInTheDocument();
    });

    it('should render ActiveDashboards app icon when id is activedashboards', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.activedashboards}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ActiveDashboardsIcon'})
      ).toBeInTheDocument();
    });

    it('should render Cati app icon when id is cati', () => {
      render(
        <ApplicationIcon applicationId={ApplicationIcon.ApplicationId.cati} />
      );

      expect(screen.queryByRole('img', {name: 'CatiIcon'})).toBeInTheDocument();
    });

    it('should render CrmConnectorForSalesForce app icon when id is crmconnectorforsalesforce', () => {
      render(
        <ApplicationIcon
          applicationId={
            ApplicationIcon.ApplicationId.crmconnectorforsalesforce
          }
        />
      );

      expect(
        screen.queryByRole('img', {name: 'CrmConnectorForSalesForceIcon'})
      ).toBeInTheDocument();
    });

    it('should render DiscoveryAnalytics app icon when id is discoveryanalytics', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.discoveryanalytics}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'DiscoveryAnalyticsIcon'})
      ).toBeInTheDocument();
    });

    it('should render EndUserManagement app icon when id is endusermanagement', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.endusermanagement}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'EndUserManagementIcon'})
      ).toBeInTheDocument();
    });

    it('should render GeniusSocial app icon when id is geniussocial', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.geniussocial}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'GeniusSocialIcon'})
      ).toBeInTheDocument();
    });

    it('should render HierarchyManagement app icon when id is hierarchymanagement', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.hierarchymanagement}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'HierarchyManagementIcon'})
      ).toBeInTheDocument();
    });

    it('should render Home app icon when id is home', () => {
      render(
        <ApplicationIcon applicationId={ApplicationIcon.ApplicationId.home} />
      );

      expect(screen.queryByRole('img', {name: 'HomeIcon'})).toBeInTheDocument();
    });

    it('should render InstantAnalytics app icon when id is instantanalytics', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.instantanalytics}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'InstantAnalyticsIcon'})
      ).toBeInTheDocument();
    });

    it('should render ModelBuilder app icon when id is modelbuilder', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.modelbuilder}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ModelBuilderIcon'})
      ).toBeInTheDocument();
    });

    it('should render ProfessionalAuthoring app icon when id is professionalauthoring', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.professionalauthoring}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ProfessionalAuthoringIcon'})
      ).toBeInTheDocument();
    });

    it('should render Reportal app icon when id is reportal', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.reportal}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'ReportalIcon'})
      ).toBeInTheDocument();
    });

    it('should render SmartHub app icon when id is smarthub', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.smarthub}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'SmartHubIcon'})
      ).toBeInTheDocument();
    });

    it('should render Studio app icon when id is studio', () => {
      render(
        <ApplicationIcon applicationId={ApplicationIcon.ApplicationId.studio} />
      );

      expect(
        screen.queryByRole('img', {name: 'StudioIcon'})
      ).toBeInTheDocument();
    });

    it('should render SurveyDesigner app icon when id is surveydesigner', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.surveydesigner}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'SurveyDesignerIcon'})
      ).toBeInTheDocument();
    });

    it('should render CustomQuestions app icon when id is customquestions', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.customquestions}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'CustomQuestionsIcon'})
      ).toBeInTheDocument();
    });

    it('should render DigitalFeedback app icon when id is digitalfeedback', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.digitalfeedback}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'DigitalFeedbackIcon'})
      ).toBeInTheDocument();
    });

    it('should render PGFusionIcon app icon when id is pgfusion', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.pgfusion}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'PGFusionIcon'})
      ).toBeInTheDocument();
    });

    it('should render WorkflowsIcon app icon when id is workflows', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.workflows}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'WorkflowsIcon'})
      ).toBeInTheDocument();
    });

    it('should render SurveyLayoutsIcon app icon when id is surveylayouts', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.surveylayouts}
        />
      );

      expect(
        screen.queryByRole('img', {name: 'SurveyLayoutsIcon'})
      ).toBeInTheDocument();
    });

    it('should apply correct width and height values to item', () => {
      render(
        <ApplicationIcon
          applicationId={ApplicationIcon.ApplicationId.digitalfeedback}
          width={150}
          height={190}
        />
      );

      const iconEl = screen.getByRole('img', {name: 'DigitalFeedbackIcon'});
      expect(iconEl).toHaveAttribute('width', '150');
      expect(iconEl).toHaveAttribute('height', '190');
    });
  });
});
