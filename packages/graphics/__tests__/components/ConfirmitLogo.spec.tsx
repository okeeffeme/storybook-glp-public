import React from 'react';
import {screen, render} from '@testing-library/react';
import {ConfirmitLogo} from '../../src';

describe('Jotunheim React Graphics :: ', () => {
  describe('ConfirmitLogo :: ', () => {
    it('should render default logo when no appearance is added', () => {
      render(<ConfirmitLogo />);

      expect(
        screen.queryByRole('img', {
          name: 'ConfirmitLogoHorizontalOrangeWhiteIcon',
        })
      ).toBeInTheDocument();
    });

    it('should render HorizontalOrangeWhite version of logo when appearance is HorizontalOrangeWhite', () => {
      render(
        <ConfirmitLogo
          appearance={ConfirmitLogo.Appearance.HorizontalOrangeWhite}
        />
      );

      expect(
        screen.queryByRole('img', {
          name: 'ConfirmitLogoHorizontalOrangeWhiteIcon',
        })
      ).toBeInTheDocument();
    });

    it('should render BugOrange version of logo when appearance is BugOrange', () => {
      render(<ConfirmitLogo appearance={ConfirmitLogo.Appearance.BugOrange} />);

      expect(
        screen.queryByRole('img', {name: 'ConfirmitBugOrangeIcon'})
      ).toBeInTheDocument();
    });

    it('should render BugSlate version of logo when appearance is BugSlate', () => {
      render(<ConfirmitLogo appearance={ConfirmitLogo.Appearance.BugSlate} />);

      expect(
        screen.queryByRole('img', {name: 'ConfirmitBugSlateIcon'})
      ).toBeInTheDocument();
    });

    it('should render HorizonsSlate version of logo when appearance is HorizonsSlate', () => {
      render(
        <ConfirmitLogo appearance={ConfirmitLogo.Appearance.HorizonsSlate} />
      );

      expect(
        screen.queryByRole('img', {name: 'ConfirmitHorizonsSlateIcon'})
      ).toBeInTheDocument();
    });

    it('should render HorizontalSlate version of logo when appearance is HorizontalSlate', () => {
      render(
        <ConfirmitLogo appearance={ConfirmitLogo.Appearance.HorizontalSlate} />
      );

      expect(
        screen.queryByRole('img', {name: 'ConfirmitHorizontalSlateIcon'})
      ).toBeInTheDocument();
    });

    it('should render StackedSlate version of logo when appearance is StackedSlate', () => {
      render(
        <ConfirmitLogo appearance={ConfirmitLogo.Appearance.StackedSlate} />
      );

      expect(
        screen.queryByRole('img', {name: 'ConfirmitStackedSlateIcon'})
      ).toBeInTheDocument();
    });

    it('should apply correct width and height values to item', () => {
      render(<ConfirmitLogo width={150} height={190} />);

      const iconEl = screen.getByRole('img', {
        name: 'ConfirmitLogoHorizontalOrangeWhiteIcon',
      });
      expect(iconEl).toHaveAttribute('width', '150');
      expect(iconEl).toHaveAttribute('height', '190');
    });
  });
});
