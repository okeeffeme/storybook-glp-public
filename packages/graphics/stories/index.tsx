import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {
  ForstaLogo,
  ConfirmitLogo,
  ApplicationIcon,
  ExternalOrganizationLogo,
} from '../src';

storiesOf('Components/graphics', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Forsta Logo', () => {
    return (
      <div
        style={{
          padding: '24px',
        }}>
        <div
          style={{
            display: 'grid',
            gridAutoRows: '1fr',
            gap: '16px',
          }}>
          <div>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.NavyPlus}
              width="300"
              height="58.055"
            />
            <div>Forsta Logo Appearance: Navy</div>
          </div>
          <div
            style={{
              background: '#000',
            }}>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.WhitePlus}
              width="300"
              height="58.055"
            />
            <div style={{color: 'white'}}>Forsta Logo Appearance: White</div>
          </div>
          <div
            style={{
              background: '#000',
            }}>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.White}
              width="300"
              height="58.055"
            />
            <div style={{color: 'white'}}>Forsta Logo Appearance: White</div>
          </div>
          <div>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.Navy}
              width="300"
              height="58.055"
            />
            <div>Forsta Logo Appearance: Navy</div>
          </div>
          <div
            style={{
              background: '#000',
            }}>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.MinimalWhite}
              width="100"
              height="58.055"
            />
            <div style={{color: 'white'}}>
              Forsta Logo Appearance: MinimalWhite
            </div>
          </div>
          <div>
            <ForstaLogo
              appearance={ForstaLogo.Appearance.MinimalNavy}
              width="100"
              height="58.055"
            />
            <div>Forsta Logo Appearance: MinimalNavy</div>
          </div>
        </div>
      </div>
    );
  })
  .add('Confirmit Logo', () => {
    return (
      <div
        style={{
          padding: '24px',
        }}>
        <div
          style={{
            display: 'grid',
            gridAutoRows: '1fr',
            gap: '16px',
          }}>
          <div>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.BugOrange}
              width="202"
              height="160"
            />
            <div>Confirmit Logo Appearance: BugOrange</div>
          </div>

          <div>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.BugSlate}
              width="202"
              height="160"
            />
            <div>Confirmit Logo Appearance: BugSlate</div>
          </div>

          <div>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.HorizonsSlate}
              width="361"
              height="98"
            />
            <div>Confirmit Logo Appearance: HorizonsSlate</div>
          </div>

          <div>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.HorizontalSlate}
              width="431"
              height="80"
            />
            <div>Confirmit Logo Appearance: HorizontalSlate</div>
          </div>

          <div>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.StackedSlate}
              width="306"
              height="169"
            />
            <div>Confirmit Logo Appearance: StackedSlate</div>
          </div>

          <div style={{backgroundColor: '#222222'}}>
            <ConfirmitLogo
              appearance={ConfirmitLogo.Appearance.HorizontalOrangeWhite}
              width="431"
              height="80"
            />
            <div style={{color: '#fff'}}>
              Confirmit Logo Appearance: HorizontalOrangeWhite
            </div>
          </div>
        </div>
      </div>
    );
  })
  .add('Application Icon', () => {
    return (
      <div
        style={{
          padding: '24px',
          display: 'grid',
          gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
          gap: '16px',
        }}>
        {Object.keys(ApplicationIcon.ApplicationId).map((app) => {
          return (
            <div key={app} style={{marginBottom: '20px'}}>
              <ApplicationIcon
                applicationId={ApplicationIcon.ApplicationId[app]}
              />
              <div>Application ID: {app}</div>
            </div>
          );
        })}
        <div style={{marginBottom: '20px'}}>
          {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
          {/* @ts-ignore - want to explicitly break the contract of id prop */}
          <ApplicationIcon applicationId="banana" />
          <div>Application ID: Unknown</div>
        </div>
      </div>
    );
  })
  .add('Favicon', () => {
    return (
      <h1
        style={{
          padding: '24px',
        }}>
        See README.md for details on how to add the favicon to your app
      </h1>
    );
  })
  .add('External Organizations', () => {
    return (
      <div
        style={{
          display: 'grid',
          gridAutoRows: '1fr',
          gap: '16px',
        }}>
        <div>
          <ExternalOrganizationLogo
            appearance={ExternalOrganizationLogo.Appearance.Salesforce}
            width={210}
            height={135}
          />
          <div>Appearance: Salesforce</div>
        </div>
        <div>
          <ExternalOrganizationLogo
            appearance={ExternalOrganizationLogo.Appearance.DynamicsWithCaption}
            width={210}
            height={135}
          />
          <div>Appearance: DynamicsWithCaption</div>
        </div>
        <div>
          <ExternalOrganizationLogo
            appearance={ExternalOrganizationLogo.Appearance.Dynamics}
            width={135}
            height={135}
          />
          <div>Appearance: Dynamics</div>
        </div>
      </div>
    );
  });
