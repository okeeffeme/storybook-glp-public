import React from 'react';
import {storiesOf} from '@storybook/react';
import {text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Pager, {PagerTypes} from '../src';

storiesOf('Components/pager', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [currentPage, setCurrentPage] = React.useState(1);

    return (
      <Pager
        page={currentPage}
        pageSize={10}
        totalCount={1000}
        onPageChanged={(nextPage) => setCurrentPage(nextPage)}
        firstButtonTooltip={text('firstButtonTooltip', 'First')}
        previousButtonTooltip={text('previousButtonTooltip', 'Previous')}
        nextButtonTooltip={text('nextButtonTooltip', 'Next')}
        lastButtonTooltip={text('lastButtonTooltip', 'Last')}
        rangeSeparator={text('rangeSeparator', 'of')}
        rangeDropdownHeader={text('rangeDropdownHeader', 'View range')}
        type={PagerTypes.page}
      />
    );
  })
  .add('Range', () => {
    const [currentPage, setCurrentPage] = React.useState(1);

    return (
      <Pager
        page={currentPage}
        pageSize={10}
        totalCount={1000}
        onPageChanged={(nextPage) => setCurrentPage(nextPage)}
        firstButtonTooltip={text('firstButtonTooltip', 'First')}
        previousButtonTooltip={text('previousButtonTooltip', 'Previous')}
        nextButtonTooltip={text('nextButtonTooltip', 'Next')}
        lastButtonTooltip={text('lastButtonTooltip', 'Last')}
        rangeSeparator={text('rangeSeparator', 'of')}
        rangeDropdownHeader={text('rangeDropdownHeader', 'View range')}
        type={PagerTypes.range}
      />
    );
  });
