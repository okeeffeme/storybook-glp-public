import constrainInput from '../../src/utils/constrain-input';

describe('Jotunheim React Pager :: ', () => {
  describe('Constrain input', () => {
    it('should return 1 when input is less than 0', () => {
      expect(constrainInput({input: -1, maxValue: 10})).toEqual(1);
    });

    it('should return 1 when input is 0', () => {
      expect(constrainInput({input: 0, maxValue: 10})).toEqual(1);
    });

    it('should return input when input is between 1 and maxValue', () => {
      expect(constrainInput({input: 6, maxValue: 10})).toEqual(6);
    });

    it('should return maxValue when input is above maxValue', () => {
      expect(constrainInput({input: 11, maxValue: 10})).toEqual(10);
    });

    it('should return maxValue when input is equal to maxValue', () => {
      expect(constrainInput({input: 10, maxValue: 10})).toEqual(10);
    });
  });
});
