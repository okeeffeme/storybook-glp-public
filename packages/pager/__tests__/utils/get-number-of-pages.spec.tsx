import getNoOfPages from '../../src/utils/get-number-of-pages';

describe('Jotunheim React Pager :: ', () => {
  describe('Utility: Get number of pages', () => {
    it('should return 1 when total < pageSize', () => {
      expect(getNoOfPages(4, 10)).toEqual(1);
    });

    it('should return 2 when pageSize < total < pageSize*2', () => {
      expect(getNoOfPages(13, 10)).toEqual(2);
    });

    it('should return X when pageSize*(X-1) < total < pageSize*X', () => {
      expect(getNoOfPages(34, 10)).toEqual(4);
      expect(getNoOfPages(67, 10)).toEqual(7);
      expect(getNoOfPages(102, 10)).toEqual(11);
    });
  });
});
