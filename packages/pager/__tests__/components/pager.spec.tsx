import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PagerTypes from '../../src/constants/pager-types';

import {Pager} from '../../src/components/pager';

describe('Jotunheim React Pager :: ', () => {
  describe('Enabling and disabling pager controls', () => {
    it('should disable first and previous buttons when on first page, and next and last buttons should be enabled', () => {
      const onPageChanged = jest.fn();

      render(
        <Pager
          totalCount={100}
          page={1}
          pageSize={10}
          onPageChanged={onPageChanged}
        />
      );

      expect(screen.getByTestId('pager-first')).toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-previous')).toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-next')).not.toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-last')).not.toHaveAttribute('disabled');
    });

    it('should disable next and last buttons when on last page, and first and previous buttons should be enabled', () => {
      const onPageChanged = jest.fn();

      render(
        <Pager
          totalCount={100}
          page={10}
          pageSize={10}
          onPageChanged={onPageChanged}
        />
      );

      expect(screen.getByTestId('pager-first')).not.toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-previous')).not.toHaveAttribute(
        'disabled'
      );
      expect(screen.getByTestId('pager-next')).toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-last')).toHaveAttribute('disabled');
    });

    it('should enable all pager controls when at a page between first and last', () => {
      const onPageChanged = jest.fn();

      render(
        <Pager
          totalCount={100}
          page={2}
          pageSize={10}
          onPageChanged={onPageChanged}
        />
      );

      expect(screen.getByTestId('pager-first')).not.toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-previous')).not.toHaveAttribute(
        'disabled'
      );
      expect(screen.getByTestId('pager-next')).not.toHaveAttribute('disabled');
      expect(screen.getByTestId('pager-last')).not.toHaveAttribute('disabled');
    });
  });

  it('should disable all pager controls when there is only 1 page', () => {
    const onPageChanged = jest.fn();

    render(
      <Pager
        totalCount={1}
        page={1}
        pageSize={1}
        onPageChanged={onPageChanged}
      />
    );

    expect(screen.getByTestId('pager-first')).toHaveAttribute('disabled');
    expect(screen.getByTestId('pager-previous')).toHaveAttribute('disabled');
    expect(screen.getByTestId('pager-next')).toHaveAttribute('disabled');
    expect(screen.getByTestId('pager-last')).toHaveAttribute('disabled');
  });

  it('should return current page + 1 when clicking next button', () => {
    const handlePageChanged = jest.fn();

    render(
      <Pager
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={handlePageChanged}
      />
    );

    userEvent.click(screen.getByTestId('pager-next'));

    expect(handlePageChanged).toHaveBeenCalledTimes(1);
    expect(handlePageChanged).toHaveBeenCalledWith(6);
  });

  it('should return current page - 1 when clicking previous button', () => {
    const handlePageChanged = jest.fn();

    render(
      <Pager
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={handlePageChanged}
      />
    );

    userEvent.click(screen.getByTestId('pager-previous'));

    expect(handlePageChanged).toHaveBeenCalledTimes(1);
    expect(handlePageChanged).toHaveBeenCalledWith(4);
  });

  it('should return 1 when clicking first button', () => {
    const handlePageChanged = jest.fn();

    render(
      <Pager
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={handlePageChanged}
      />
    );

    userEvent.click(screen.getByTestId('pager-first'));

    expect(handlePageChanged).toHaveBeenCalledTimes(1);
    expect(handlePageChanged).toHaveBeenCalledWith(1);
  });

  it('should return last page when clicking last button', () => {
    const handlePageChanged = jest.fn();

    render(
      <Pager
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={handlePageChanged}
      />
    );

    userEvent.click(screen.getByTestId('pager-last'));

    expect(handlePageChanged).toHaveBeenCalledTimes(1);
    expect(handlePageChanged).toHaveBeenCalledWith(10);
  });
});

describe('Pager types', () => {
  it('should only show pager input when no type is set', () => {
    const onPageChanged = jest.fn();

    render(
      <Pager
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={onPageChanged}
      />
    );
    expect(screen.getAllByTestId('current-page-input').length).toBe(1);
    expect(screen.queryAllByTestId('range-input').length).toBe(0);
  });

  it('should only show pager input when page type is set', () => {
    const onPageChanged = jest.fn();

    render(
      <Pager
        type={PagerTypes.page}
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={onPageChanged}
      />
    );
    expect(screen.getAllByTestId('current-page-input').length).toBe(1);
    expect(screen.queryAllByTestId('range-input').length).toBe(0);
  });

  it('should only show range input when range type is set', () => {
    const onPageChanged = jest.fn();

    render(
      <Pager
        type={PagerTypes.range}
        totalCount={100}
        page={5}
        pageSize={10}
        onPageChanged={onPageChanged}
      />
    );

    expect(screen.queryAllByTestId('current-page-input').length).toBe(0);
    expect(screen.queryAllByTestId('range-input').length).toBe(1);
  });
});
