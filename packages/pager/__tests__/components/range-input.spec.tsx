import React from 'react';
import {render, screen} from '@testing-library/react';

import RangeInput from '../../src/components/range-input';

describe('Jotunheim React Pager :: Range input :: ', () => {
  const onPageChangedHandler = () => jest.fn();
  const element = jest.fn(() => 'class-name');

  describe('Label', () => {
    it('should display correct label when current page = 1, pageSize = 10 and totalCount = 100', () => {
      render(
        <RangeInput
          noOfPages={10}
          onPageChanged={onPageChangedHandler}
          pageSize={10}
          page={1}
          totalCount={100}
          element={element}
        />
      );

      expect(screen.getByText(/1-10/i)).toBeInTheDocument();
      expect(screen.getByText(/of 100/i)).toBeInTheDocument();
    });

    it('should display correct label when current page = 6, pageSize = 10 and totalCount = 100', () => {
      render(
        <RangeInput
          noOfPages={10}
          onPageChanged={onPageChangedHandler}
          pageSize={10}
          page={6}
          totalCount={100}
          element={element}
        />
      );

      expect(screen.getByText(/51-60/i)).toBeInTheDocument();
      expect(screen.getByText(/of 100/i)).toBeInTheDocument();
    });

    it('should display correct label when not using an even pageSize number', () => {
      render(
        <RangeInput
          noOfPages={10}
          onPageChanged={onPageChangedHandler}
          pageSize={9}
          page={5}
          totalCount={100}
          element={element}
        />
      );

      expect(screen.getByText(/37-45/i)).toBeInTheDocument();
      expect(screen.getByText(/of 100/i)).toBeInTheDocument();
    });

    it('should display correct label when on the last page', () => {
      render(
        <RangeInput
          noOfPages={10}
          onPageChanged={onPageChangedHandler}
          pageSize={10}
          page={10}
          totalCount={95}
          element={element}
        />
      );

      expect(screen.getByText(/91-95/i)).toBeInTheDocument();
      expect(screen.getByText(/of 95/i)).toBeInTheDocument();
    });
  });
});
