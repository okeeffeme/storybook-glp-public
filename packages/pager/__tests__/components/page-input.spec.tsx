import React from 'react';
import {render, screen} from '@testing-library/react';

import PageInput from '../../src/components/page-input';

describe('Jotunheim React Pager :: Page input :: ', () => {
  const onPageChanged = jest.fn();
  const element = jest.fn();

  describe('Label', () => {
    it('should display correct label when current page is below valid threshold, noOfPages = 100', () => {
      render(
        <PageInput
          page={0}
          noOfPages={100}
          onPageChanged={onPageChanged}
          element={element}
        />
      );

      expect(screen.getByText('1')).toBeInTheDocument();
      expect(screen.getByText('/ 100')).toBeInTheDocument();
    });

    it('should display correct label when current page is negative number, noOfPages = 100', () => {
      render(
        <PageInput
          page={-1}
          noOfPages={100}
          onPageChanged={onPageChanged}
          element={element}
        />
      );

      expect(screen.getByText('1')).toBeInTheDocument();
      expect(screen.getByText('/ 100')).toBeInTheDocument();
    });

    it('should display correct label when current page = 1, noOfPages = 100', () => {
      render(
        <PageInput
          page={6}
          noOfPages={100}
          onPageChanged={onPageChanged}
          element={element}
        />
      );

      expect(screen.getByText('6')).toBeInTheDocument();
      expect(screen.getByText('/ 100')).toBeInTheDocument();
    });

    it('should display correct label when on the last page', () => {
      render(
        <PageInput
          page={95}
          noOfPages={95}
          onPageChanged={onPageChanged}
          element={element}
        />
      );

      expect(screen.getByText('95')).toBeInTheDocument();
      expect(screen.getByText('/ 95')).toBeInTheDocument();
    });

    it('should display correct label when on the current page is higher than the number of pages', () => {
      render(
        <PageInput
          page={96}
          noOfPages={95}
          onPageChanged={onPageChanged}
          element={element}
        />
      );

      expect(screen.getByText('95')).toBeInTheDocument();
      expect(screen.getByText('/ 95')).toBeInTheDocument();
    });
  });
});
