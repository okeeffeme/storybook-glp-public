export type PagerType = {
  page: 'page';
  range: 'range';
};

const pagerTypes: PagerType = {
  page: 'page',
  range: 'range',
};

export default pagerTypes;
