import Pager from './components/pager';
import PagerTypes from './constants/pager-types';
import type {PagerType} from './constants/pager-types';

export {Pager, PagerTypes, PagerType};

export default Pager;
