interface Inputs {
  input: number;
  maxValue: number;
}

export default ({input, maxValue}: Inputs) => {
  if (Number.isNaN(input) || input <= 0) {
    return 1;
  }

  if (input > maxValue) {
    return maxValue;
  }

  return input;
};
