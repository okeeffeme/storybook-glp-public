export default (total: number, pageSize: number) => Math.ceil(total / pageSize);
