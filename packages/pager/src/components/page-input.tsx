import React, {FC} from 'react';

import {Chip} from '@jotunheim/react-chip';

import constrainInputToValidRange from '../utils/constrain-input';

interface PageInputProps {
  element: (elementName: string, modifierClassName?: string) => string;
  page: number;
  noOfPages: number;
  onPageChanged: (newPage: number) => void;
}

export const PageInput: FC<PageInputProps> = ({
  page,
  noOfPages,
  onPageChanged,
  element,
}) => {
  const inputRef = React.useRef<HTMLInputElement | null>(null);
  const [inputValue, setInputValue] = React.useState<number | string>('');

  const handleChange = ({target}) => {
    setInputValue(target.value);
  };

  const handleBlur = ({target}) => {
    setNewPage(target.value);
    setInputValue('');
  };

  const resetInputToCurrentPage = () => {
    setInputValue(page);
  };

  const setNewPage = (input) => {
    const newPage = constrainInputToValidRange({
      input,
      maxValue: noOfPages,
    });
    if (newPage !== page) {
      onPageChanged(newPage);
    }
  };

  const handleKeyDown = ({key}) => {
    if (key === 'Enter') {
      inputRef.current?.blur();
    } else if (key === 'Escape') {
      resetInputToCurrentPage();

      // Need to wait a bit before blurring, else the value of the input
      // will be set which we don't want to do when using escape
      setTimeout(() => {
        inputRef.current?.blur();
      }, 1);
    }
  };

  return (
    <Chip
      data-testid="page-input"
      className={element('chip')}
      onClick={() => {
        inputRef.current?.select();
      }}>
      <div className={element('current-page-wrapper')}>
        <input
          className={element('current-page-input')}
          ref={inputRef}
          data-testid="current-page-input"
          type="number"
          min={1}
          max={noOfPages}
          value={inputValue || page}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          onBlur={handleBlur}
        />
        <span
          data-locator="pager-page-label"
          className={element('current-page-label-wrapper')}>
          <span>
            {constrainInputToValidRange({input: page, maxValue: noOfPages})}
          </span>
          <span
            className={element(
              'current-page-rest-label'
            )}>{` / ${noOfPages}`}</span>
        </span>
      </div>
    </Chip>
  );
};

export default PageInput;
