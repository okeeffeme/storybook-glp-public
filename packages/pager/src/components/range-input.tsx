import React, {FC} from 'react';

import {Chip} from '@jotunheim/react-chip';
import Dropdown from '@jotunheim/react-dropdown';

import constrainInputToValidRange from '../utils/constrain-input';

interface RangeLabel {
  page: number;
  pageSize: number;
  totalCount: number;
}

interface RangeInputProps {
  element: (elementName: string, modifierClassName?: string) => string;
  page: number;
  pageSize: number;
  noOfPages: number;
  onPageChanged: (page: number) => void;
  totalCount: number;
  rangeSeparator?: string;
  rangeDropdownHeader?: string;
}

const rangeLabel = ({page, pageSize, totalCount}: RangeLabel) =>
  `${constrainInputToValidRange({
    input: page * pageSize - pageSize + 1,
    maxValue: totalCount,
  })}-${constrainInputToValidRange({
    input: page * pageSize,
    maxValue: totalCount,
  })}`;

export const RangeInput: FC<RangeInputProps> = ({
  page,
  noOfPages,
  onPageChanged,
  element,
  pageSize,
  totalCount,
  rangeSeparator = 'of',
  rangeDropdownHeader = 'View range',
}) => {
  const options = Array.from({length: noOfPages}, (value, index) => (
    <Dropdown.MenuItem
      selectable={true}
      selected={index === page}
      key={index}
      onClick={(e) => {
        e.preventDefault();
        onPageChanged(index + 1);
      }}>
      {rangeLabel({page: index, pageSize, totalCount})}
    </Dropdown.MenuItem>
  ));

  const currentPageLabel = rangeLabel({page, pageSize, totalCount});

  return (
    <Dropdown
      placement="bottom"
      menu={
        <Dropdown.Menu>
          <Dropdown.MenuHeader>{rangeDropdownHeader}</Dropdown.MenuHeader>
          {options}
        </Dropdown.Menu>
      }>
      <div data-testid="range-input">
        <Chip className={element('chip')}>
          <div className={element('current-page-wrapper')}>
            <span
              data-locator="pager-range-label"
              data-testid="pager-range-id"
              className={element('current-page-label-wrapper')}>
              <span>{currentPageLabel}</span>
              <span
                className={element(
                  'current-page-rest-label'
                )}>{` ${rangeSeparator} ${totalCount}`}</span>
            </span>
          </div>
        </Chip>
      </div>
    </Dropdown>
  );
};

export default RangeInput;
