import React, {FC} from 'react';

import Icon, {play, fastForward} from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import PageInput from './page-input';
import RangeInput from './range-input';
import NavigationButton from './navigation-button';

import getNoOfPages from '../utils/get-number-of-pages';
import pagerTypes from '../constants/pager-types';

import classNames from './pager.module.css';

const {block, element} = bemFactory('comd-pager', classNames);

interface PagerProps {
  totalCount: number;
  page: number;
  pageSize: number;
  onPageChanged: (previousPage: number) => void;
  firstButtonTooltip?: string;
  previousButtonTooltip?: string;
  nextButtonTooltip?: string;
  lastButtonTooltip?: string;
  rangeSeparator?: string;
  rangeDropdownHeader?: string;
  type?: keyof typeof pagerTypes;
}

export const Pager: FC<PagerProps> = ({
  page,
  pageSize,
  totalCount,
  onPageChanged,
  firstButtonTooltip = 'First',
  previousButtonTooltip = 'Previous',
  nextButtonTooltip = 'Next',
  lastButtonTooltip = 'Last',
  rangeSeparator,
  rangeDropdownHeader,
  type = pagerTypes.page,
  ...rest
}) => {
  const lastPage = getNoOfPages(totalCount, pageSize);

  const previousPage = Math.max(1, page - 1);
  const nextPage = Math.min(lastPage, page + 1);

  const navigationButtonClass = element('navigation-button-wrapper');

  return (
    <div
      className={block()}
      data-testid="pager"
      {...extractDataAriaIdProps(rest)}>
      <NavigationButton
        data-testid="pager-first"
        className={navigationButtonClass}
        onClick={() => onPageChanged(1)}
        disabled={page <= 1}
        data-locator="pager-first"
        tooltip={firstButtonTooltip}>
        <Icon path={fastForward} className={element('icon-flip')} />
      </NavigationButton>

      <NavigationButton
        data-testid="pager-previous"
        className={navigationButtonClass}
        onClick={() => onPageChanged(previousPage)}
        disabled={page <= 1}
        data-locator="pager-previous"
        tooltip={previousButtonTooltip}>
        <Icon path={play} className={element('icon-flip')} />
      </NavigationButton>

      <div className={element('type-wrapper')}>
        {type === pagerTypes.page && (
          <PageInput
            page={page}
            noOfPages={lastPage}
            onPageChanged={onPageChanged}
            element={element}
          />
        )}
        {type === pagerTypes.range && (
          <RangeInput
            page={page}
            pageSize={pageSize}
            noOfPages={lastPage}
            onPageChanged={onPageChanged}
            element={element}
            rangeSeparator={rangeSeparator}
            rangeDropdownHeader={rangeDropdownHeader}
            totalCount={totalCount}
          />
        )}
      </div>

      <NavigationButton
        data-testid="pager-next"
        className={navigationButtonClass}
        onClick={() => onPageChanged(nextPage)}
        disabled={page >= lastPage}
        data-locator="pager-next"
        tooltip={nextButtonTooltip}>
        <Icon path={play} />
      </NavigationButton>

      <NavigationButton
        data-testid="pager-last"
        className={navigationButtonClass}
        onClick={() => onPageChanged(lastPage)}
        disabled={page >= lastPage}
        data-locator="pager-last"
        tooltip={lastButtonTooltip}>
        <Icon path={fastForward} />
      </NavigationButton>
    </div>
  );
};

export default React.memo(Pager);
