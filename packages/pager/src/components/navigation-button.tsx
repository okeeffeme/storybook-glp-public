import React, {FC, PropsWithChildren} from 'react';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import {IconButton} from '@jotunheim/react-button';
import Tooltip from '@jotunheim/react-tooltip';

interface NavigationButtonProps {
  tooltip?: string;
  disabled?: boolean;
  onClick?: () => void;
  className?: string;
}

const NavigationButton: FC<PropsWithChildren<NavigationButtonProps>> = ({
  className,
  tooltip,
  disabled,
  onClick,
  children,
  ...rest
}) => (
  <Tooltip content={!disabled && tooltip}>
    <div className={className}>
      <IconButton
        disabled={disabled}
        onClick={onClick}
        {...extractDataAriaIdProps(rest)}>
        {children}
      </IconButton>
    </div>
  </Tooltip>
);

export default NavigationButton;
