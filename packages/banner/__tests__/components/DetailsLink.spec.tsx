import React from 'react';
import {render, screen} from '@testing-library/react';
import DetailsLink, {DetailsLinkProps} from '../../src/components/DetailsLink';

const defaultProps: DetailsLinkProps = {
  text: '',
  href: '',
  onClick() {},
};

const wrapper = (props: Partial<DetailsLinkProps> = {}) => {
  return render(<DetailsLink {...defaultProps} {...props} />);
};

describe('Banner Details Link :: ', () => {
  it('renders', () => {
    wrapper();

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should not render suffix if href is relative', () => {
    wrapper({
      href: '/more-details/',
    });

    expect(screen.getByRole('link')).toHaveProperty('suffix', undefined);
  });

  it('should render suffix if href is absolute', () => {
    wrapper({
      href: 'http://more-details/',
    });

    expect(screen.getByRole('link')).not.toHaveAttribute('suffix', undefined);
  });
});
