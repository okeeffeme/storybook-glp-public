import React from 'react';
import {render, screen} from '@testing-library/react';
import Message, {MessageProps} from '../../src/components/Message';

const defaultProps: MessageProps = {
  message: {
    id: 10,
    text: '',
    aboutToBeRemoved: false,
    type: 'success',
    closeButton: false,
    tapToDismiss: false,
    showMoreLabel: undefined,
    showLessLabel: undefined,
    showMoreContent: '',
  },
  onMouseOver() {},
  onMouseOut() {},
  onClose() {},
};

const wrapper = (props: Partial<MessageProps> = {}) => {
  return render(<Message {...defaultProps} {...props} />);
};

describe('Banner Message :: ', () => {
  it('renders', () => {
    wrapper();
    expect(screen.findByText('comd-banner__message')).toBeTruthy();
  });

  it('should not render "show more" toggle', () => {
    wrapper();

    expect(screen.queryByText('show-more-toggle')).not.toBeInTheDocument();
  });

  it('should render "show more" toggle, if specified', () => {
    wrapper({
      message: {
        ...defaultProps.message,
        showMoreContent: <span>more details here...</span>,
      },
    });

    expect(screen.getByText('more details here...')).toBeInTheDocument();
  });

  it('should not render close button', () => {
    wrapper();

    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should render close button, if specified', () => {
    wrapper({
      message: {
        ...defaultProps.message,
        closeButton: true,
      },
    });

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should not render details link', () => {
    wrapper();

    expect(screen.queryByText('See details')).not.toBeInTheDocument();
  });

  it('should render details link, if specified', () => {
    wrapper({
      message: {
        ...defaultProps.message,
        link: {
          href: '/more-details',
          text: 'click for more details',
        },
      },
    });

    expect(screen.getByRole('link')).toBeInTheDocument();
  });
});
