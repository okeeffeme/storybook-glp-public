import React from 'react';

import store from '../src/utils/store';
import {MessageOptionsWithId} from '../src/types';

const CustomMessage = ({message}: {message: MessageOptionsWithId}) => {
  const handleClose = (e) => {
    e.preventDefault();
    store.remove(message);
  };

  return (
    <div className="success-with-link">
      <span>Custom message</span>
      <a href="#" onClick={handleClose}>
        Close it
      </a>
    </div>
  );
};

export default CustomMessage;
