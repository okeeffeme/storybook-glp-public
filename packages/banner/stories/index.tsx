import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Banner from '../src/components/Banner';

import store from '../src/utils/store';

import Button from '../../button/src/index';
import CustomMessage from './CustomMessage';

import './demo.scss';

let counter = 0;
let customCloseRef: ReturnType<typeof store['error']> | null = null;
const addSuccess = () => {
  store.success(`success ${counter++}`);
};

const addInfo = () => {
  store.info(`info ${counter++}`);
};

const addWarning = () => {
  store.warning(`warning ${counter++}`);
};

const addError = () => {
  store.error(`error ${counter++}`);
};

const customSuccess = () => {
  store.success({
    text: <span>Custom message</span>,
    link: {
      text: 'Extra link in header',
      href: 'http://www.confirmit.com/',
    },
    closeTimeout: 2000,
  });
};

const customSuccessWithComponent = () => {
  store.success({
    component: CustomMessage,
    closeButton: false,
    closeTimeout: 2000,
  });
};

const manualClosing = () => {
  store.error({
    text: `this should be closed manually`,
    closeTimeout: 0,
  });
};

const programmaticClosing = () => {
  customCloseRef = store.error({
    text: `this should be closed from code`,
    closeTimeout: 0,
    closeButton: false,
    tapToDismiss: false,
  });
};

const handleCustomClose = () => {
  customCloseRef?.remove();
};

const showMore = () => {
  store.error({
    text: `this should be closed manually`,
    showMoreContent: <div>JSX content</div>,
    closeTimeout: 0,
  });
};

const showMoreWithCustomLabels = () => {
  store.error({
    text: `this should be closed manually`,
    showMoreContent: 'Content',
    showMoreLabel: 'Custom open label',
    showLessLabel: 'Custom close label',
    closeTimeout: 0,
  });
};

storiesOf('Components/Banner', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Success', () => (
    <div className="confirmit-banner">
      <Button
        appearance={Button.appearances.primarySuccess}
        onClick={addSuccess}>
        Add success
      </Button>
      <Banner />
    </div>
  ))
  .add('Info', () => (
    <div className="confirmit-banner">
      <Button onClick={addInfo} appearance={Button.appearances.primaryNeutral}>
        Add info
      </Button>
      <Banner />
    </div>
  ))
  .add('Warning', () => (
    <div className="confirmit-banner">
      <Button
        onClick={addWarning}
        appearance={Button.appearances.primaryNeutral}>
        Add warning
      </Button>
      <Banner />
    </div>
  ))
  .add('Error', () => (
    <div className="confirmit-banner">
      <Button appearance={Button.appearances.primaryDanger} onClick={addError}>
        Add error
      </Button>
      <Banner />
    </div>
  ))
  .add('Custom success with link', () => (
    <div className="confirmit-banner">
      <Button
        appearance={Button.appearances.primarySuccess}
        onClick={customSuccess}>
        Custom message with link
      </Button>
      <Banner />
    </div>
  ))
  .add('Manual closing', () => (
    <div className="confirmit-banner">
      <Button onClick={manualClosing}>Manual closing</Button>
      <Banner />
    </div>
  ))
  .add('Programmatic Closing', () => (
    <div className="confirmit-banner">
      <Button onClick={programmaticClosing}>Programmatic closing</Button>
      <Button onClick={handleCustomClose}>Close now</Button>
      <Banner />
    </div>
  ))
  .add('Custom success with component', () => (
    <div className="confirmit-banner">
      <Button
        appearance={Button.appearances.primarySuccess}
        onClick={customSuccessWithComponent}>
        Custom success with component
      </Button>
      <Banner />
    </div>
  ))
  .add('Show more', () => (
    <div className="confirmit-banner">
      <Button onClick={showMore}>Show more</Button>
      <Banner />
    </div>
  ))
  .add('Show more with custom labels', () => (
    <div className="confirmit-banner">
      <Button onClick={showMoreWithCustomLabels}>
        Show more with custom labels
      </Button>
      <Banner />
    </div>
  ));
