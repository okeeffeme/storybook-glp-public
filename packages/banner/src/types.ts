import {
  ClassicComponent,
  ClassicComponentClass,
  ClassType,
  ComponentState,
  FunctionComponent,
  MouseEventHandler,
  ReactNode,
} from 'react';

export enum MessageType {
  'success' = 'success',
  'info' = 'info',
  'error' = 'error',
  'warning' = 'warning',
}

export type MessageTypeLiteral = 'success' | 'info' | 'error' | 'warning';

export type MessageOptions<
  P extends {message: MessageOptionsWithId<P>} = {message: MessageOptionsWithId}
> = {
  text?: ReactNode;
  aboutToBeRemoved?: boolean;
  type?: MessageType | MessageTypeLiteral;
  closeButton?: boolean;
  tapToDismiss?: boolean;
  showMoreLabel?: string;
  showLessLabel?: string;
  showMoreContent?: ReactNode;
  closeTimeout?: number;
  component?:
    | FunctionComponent<P>
    | ClassType<
        P,
        ClassicComponent<P, ComponentState>,
        ClassicComponentClass<P>
      >;
  link?: {
    text?: ReactNode;
    href?: string;
    onClick?: MouseEventHandler;
  };
};

export type MessageOptionsWithId<
  P extends {message: MessageOptionsWithId<P>} = {message: MessageOptionsWithId}
> = MessageOptions<P> & {id: number};
