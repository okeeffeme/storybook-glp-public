import {transitions} from '@jotunheim/global-styles';

const transitionInDuration = transitions.EnterDuration;
const transitionOutDuration = transitions.ExitDuration;

export {transitionInDuration, transitionOutDuration};
