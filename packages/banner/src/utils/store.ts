import {transitionOutDuration} from './constants';
import {MessageOptions, MessageOptionsWithId} from '../types';

const storeCreator = () => {
  const store: MessageOptionsWithId[] = [];
  const listeners: (() => void)[] = [];
  let counter = 0;

  const notify = () => {
    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i];
      listener();
    }
  };

  const toOptions = (options: MessageOptions | string): MessageOptions => {
    if (typeof options === 'string') {
      return {
        text: options,
      };
    }

    return options;
  };

  const defaultOptions: MessageOptions = {
    type: 'success',
    closeButton: true,
    closeTimeout: 5000,
    tapToDismiss: true,
    showMoreLabel: 'Show more',
    showLessLabel: 'Show less',
    showMoreContent: undefined,
  };

  const timeouts = new Map();

  return {
    subscribe(listener) {
      listeners.push(listener);

      const subscribed = true;

      return function unsubscribe() {
        if (!subscribed) return;

        const index = listeners.indexOf(listener);
        listeners.splice(index, 1);
      };
    },
    get messages() {
      return store;
    },
    pause(message: MessageOptions) {
      const timeout = timeouts.get(message);
      if (timeout) {
        clearTimeout(timeout);
        timeouts.delete(message);
      }
    },
    unpause(message: MessageOptionsWithId) {
      if (message.closeTimeout) {
        const timeout = setTimeout(() => {
          timeouts.delete(message);
          this.remove(message);
        }, message.closeTimeout);

        timeouts.set(message, timeout);
      }
    },
    remove(message: MessageOptionsWithId) {
      const index = store.indexOf(message);

      if (index < 0) return;

      message.aboutToBeRemoved = true;

      notify();

      setTimeout(() => {
        const index = store.indexOf(message);

        if (index < 0) return;

        store.splice(index, 1);
        notify();
      }, transitionOutDuration);
    },

    success(options: MessageOptions | string) {
      return this.show({
        ...toOptions(options),
        type: 'success',
      });
    },

    info(options: MessageOptions | string) {
      return this.show({
        ...toOptions(options),
        type: 'info',
      });
    },

    warning(options: MessageOptions | string) {
      return this.show({
        ...toOptions(options),
        type: 'warning',
      });
    },

    error(options: MessageOptions | string) {
      return this.show({
        ...toOptions(options),
        type: 'error',
      });
    },

    show(options: MessageOptions | string) {
      const message: MessageOptionsWithId = {
        ...defaultOptions,
        id: counter++,
        ...toOptions(options),
      };

      store.push(message);
      notify();
      this.unpause(message);

      return {remove: this.remove.bind(this, message)};
    },
  };
};

export default storeCreator();
