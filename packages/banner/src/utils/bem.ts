import {bemFactory} from '@jotunheim/react-themes';

import classNames from '../components/Banner.module.css';

const {block, element, modifier} = bemFactory('comd-banner', classNames);

export {block, element, modifier};
