import React from 'react';
import cn from 'classnames';
import {CSSTransition} from 'react-transition-group';

import {IconButton} from '@jotunheim/react-button';
import Icon, {
  close,
  alert,
  criticalAlert,
  checkCircle,
  informationOutline,
} from '@jotunheim/react-icons';

import type {MessageOptionsWithId} from '../types';

import {transitionInDuration, transitionOutDuration} from '../utils/constants';

import ShowMoreToggle from './ShowMoreToggle';
import DetailsLink from './DetailsLink';
import {element} from '../utils/bem';

const getIcon = (type) => {
  switch (type) {
    case 'success':
      return checkCircle;

    case 'warning':
      return alert;

    case 'error':
      return criticalAlert;

    default:
      return informationOutline;
  }
};

export const Message = ({
  message,
  onClose,
  onMouseOver,
  onMouseOut,
}: MessageProps) => {
  const [isShowingMore, setIsShowingMore] = React.useState(false);

  const {
    text,
    component,
    aboutToBeRemoved,
    type,
    closeButton,
    tapToDismiss,
    showMoreLabel,
    showLessLabel,
    showMoreContent,
    link,
  } = message;

  const classes = cn(element('message'), element('message', type));

  const handleClick = (e) => {
    e.stopPropagation();
    e.preventDefault();
    onClose(message);
  };

  const handleMessageClick = (e) => {
    if (tapToDismiss) {
      handleClick(e);
    }
  };

  const handleToggle = (e) => {
    e.stopPropagation();
    e.preventDefault();

    setIsShowingMore((val) => !val);
  };

  const handleMouseOver = () => {
    onMouseOver(message);
  };

  const handleMouseOut = () => {
    onMouseOut(message);
  };

  const nodeRef = React.useRef(null);

  return (
    <CSSTransition
      nodeRef={nodeRef}
      className={element('transition')}
      in={!aboutToBeRemoved}
      appear={!aboutToBeRemoved}
      classNames={{
        appear: element('transition', 'appear'),
        appearActive: element('transition', 'appear-active'),
        exit: element('transition', 'leave'),
        exitActive: element('transition', 'leave-active'),
      }}
      timeout={{
        enter: transitionInDuration,
        exit: transitionOutDuration,
      }}>
      <div
        ref={nodeRef}
        data-banner-message-container={''}
        data-banner-message-type={type}>
        <div
          className={classes}
          onClick={handleMessageClick}
          onMouseOver={handleMouseOver}
          onMouseOut={handleMouseOut}>
          <div
            className={element('icon-wrapper')}
            data-banner-message-icon-wrapper={''}>
            <Icon path={getIcon(type)} className={element('icon')} />
          </div>

          <div className={element('content')} data-banner-message-content={''}>
            <div className={element('text')} data-banner-message-text={''}>
              <span>
                {text ||
                  (component &&
                    React.createElement(component, {
                      message,
                    }))}
              </span>
              {showMoreContent && (
                <ShowMoreToggle
                  onClick={handleToggle}
                  isShowingMore={isShowingMore}
                  showLessLabel={showLessLabel}
                  showMoreLabel={showMoreLabel}
                />
              )}
              {link && <DetailsLink {...link} />}
            </div>
            {closeButton && (
              <div className={element('close-button')}>
                <IconButton
                  data-banner-close-button=""
                  onClick={handleClick}
                  appearance={IconButton.appearances.banner}>
                  <Icon path={close} />
                </IconButton>
              </div>
            )}
          </div>
        </div>
        {showMoreContent && (
          <div
            className={cn(element('show-more'), element('show-more', type), {
              [element('show-more', 'open')]: isShowingMore,
            })}
            data-banner-show-more-content={''}>
            {showMoreContent}
          </div>
        )}
      </div>
    </CSSTransition>
  );
};

export type MessageProps = {
  message: MessageOptionsWithId;
  onClose: (message: MessageOptionsWithId) => void;
  onMouseOver: (message: MessageOptionsWithId) => void;
  onMouseOut: (message: MessageOptionsWithId) => void;
};

export default Message;
