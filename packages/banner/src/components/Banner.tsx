import React from 'react';
import {TransitionPortal} from '@jotunheim/react-transition-portal';
import {useForceUpdate} from '@jotunheim/react-utils';

import Message from './Message';
import store from '../utils/store';
import {block, element} from '../utils/bem';

const Banner = () => {
  const forceUpdate = useForceUpdate();

  React.useEffect(() => {
    // force update whenever the store notifies,
    // and return the unsubscribe listener.
    return store.subscribe(forceUpdate);
  }, [forceUpdate]);

  return (
    <TransitionPortal open transitionClassNames={block()}>
      <div className={block()} data-banner-container={''}>
        <div className={element('wrapper')} data-banner-wrapper={''}>
          {store.messages.map((message) => (
            <Message
              key={message.id}
              onClose={(message) => {
                store.remove(message);
              }}
              onMouseOver={(message) => {
                store.pause(message);
              }}
              onMouseOut={(message) => {
                store.unpause(message);
              }}
              message={message}
            />
          ))}
        </div>
      </div>
    </TransitionPortal>
  );
};

export default Banner;
