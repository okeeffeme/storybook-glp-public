import React, {MouseEventHandler} from 'react';

import {Button} from '@jotunheim/react-button';

import Icon, {chevronUp, chevronDown} from '@jotunheim/react-icons';

import {element} from '../utils/bem';

const ShowMoreToggle = ({
  onClick,
  isShowingMore,
  showLessLabel,
  showMoreLabel,
}: ShowMoreToggleProps) => {
  return (
    <div className={element('show-more-toggle')}>
      <Button
        appearance={Button.appearances.secondaryBanner}
        onClick={onClick}
        suffix={<Icon path={isShowingMore ? chevronUp : chevronDown} />}
        data-banner-show-more-button={''}>
        {isShowingMore ? showLessLabel : showMoreLabel}
      </Button>
    </div>
  );
};

type ShowMoreToggleProps = {
  onClick: MouseEventHandler;
  isShowingMore: boolean;
  showLessLabel?: string;
  showMoreLabel?: string;
};

export default ShowMoreToggle;
