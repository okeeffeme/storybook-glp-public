import React, {MouseEventHandler, ReactNode} from 'react';

import {Button} from '@jotunheim/react-button';
import Icon, {openInNew} from '@jotunheim/react-icons';
import {element} from '../utils/bem';

const DetailsLink = ({
  text = 'See details',
  href = '',
  onClick = () => {},
}: DetailsLinkProps) => {
  const isAbsoluteUri =
    href.startsWith('http://') || href.startsWith('https://');

  return (
    <div className={element('details-link')}>
      <Button
        data-banner-details-link={''}
        href={href}
        target={isAbsoluteUri ? '_blank' : undefined}
        onClick={(e) => {
          e.stopPropagation();
          onClick(e);
        }}
        suffix={isAbsoluteUri ? <Icon path={openInNew} /> : undefined}
        appearance={Button.appearances.secondaryBanner}>
        {text}
      </Button>
    </div>
  );
};

export type DetailsLinkProps = {
  text?: ReactNode;
  href?: string;
  onClick?: MouseEventHandler;
};

export default DetailsLink;
