# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.30&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.30&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.28&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.29&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.27&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.28&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.26&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.27&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.25&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.26&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.24&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.25&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.23&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.24&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.22&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.23&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.21&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.22&targetRepoId=1246) (2023-02-21)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.20&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.21&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.19&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.20&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.18&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.19&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.17&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.18&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.16&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.17&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.15&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.16&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.14&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.15&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.12&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.14&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.12&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.13&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.11&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.12&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.10&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.9&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.7&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.7&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.4&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.4&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.4&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.3&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.2&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.1.0&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-banner

# [12.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.11&sourceBranch=refs/tags/@jotunheim/react-banner@12.1.0&targetRepoId=1246) (2022-10-11)

### Features

- convert Banner to TypeScript ([NPM-1092](https://jiraosl.firmglobal.com/browse/NPM-1092)) ([7ab23aa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7ab23aa0dc6567fddf50579f34e2648b80dd0703))

## [12.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.10&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.9&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.8&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.6&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.3&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.2&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.1&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-banner

## [12.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-banner@12.0.0&sourceBranch=refs/tags/@jotunheim/react-banner@12.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-banner

# 12.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [11.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.75&sourceBranch=refs/tags/@confirmit/react-banner@11.0.76&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.74&sourceBranch=refs/tags/@confirmit/react-banner@11.0.75&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.73&sourceBranch=refs/tags/@confirmit/react-banner@11.0.74&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [11.0.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.72&sourceBranch=refs/tags/@confirmit/react-banner@11.0.73&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.71&sourceBranch=refs/tags/@confirmit/react-banner@11.0.72&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.69&sourceBranch=refs/tags/@confirmit/react-banner@11.0.70&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.68&sourceBranch=refs/tags/@confirmit/react-banner@11.0.69&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.67&sourceBranch=refs/tags/@confirmit/react-banner@11.0.68&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.66&sourceBranch=refs/tags/@confirmit/react-banner@11.0.67&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.65&sourceBranch=refs/tags/@confirmit/react-banner@11.0.66&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.64&sourceBranch=refs/tags/@confirmit/react-banner@11.0.65&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.61&sourceBranch=refs/tags/@confirmit/react-banner@11.0.62&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.60&sourceBranch=refs/tags/@confirmit/react-banner@11.0.61&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.59&sourceBranch=refs/tags/@confirmit/react-banner@11.0.60&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.58&sourceBranch=refs/tags/@confirmit/react-banner@11.0.59&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.57&sourceBranch=refs/tags/@confirmit/react-banner@11.0.58&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.56&sourceBranch=refs/tags/@confirmit/react-banner@11.0.57&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.55&sourceBranch=refs/tags/@confirmit/react-banner@11.0.56&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [11.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.54&sourceBranch=refs/tags/@confirmit/react-banner@11.0.55&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.52&sourceBranch=refs/tags/@confirmit/react-banner@11.0.53&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.51&sourceBranch=refs/tags/@confirmit/react-banner@11.0.52&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.50&sourceBranch=refs/tags/@confirmit/react-banner@11.0.51&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.49&sourceBranch=refs/tags/@confirmit/react-banner@11.0.50&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.48&sourceBranch=refs/tags/@confirmit/react-banner@11.0.49&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.47&sourceBranch=refs/tags/@confirmit/react-banner@11.0.48&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.46&sourceBranch=refs/tags/@confirmit/react-banner@11.0.47&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.45&sourceBranch=refs/tags/@confirmit/react-banner@11.0.46&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.44&sourceBranch=refs/tags/@confirmit/react-banner@11.0.45&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.43&sourceBranch=refs/tags/@confirmit/react-banner@11.0.44&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.42&sourceBranch=refs/tags/@confirmit/react-banner@11.0.43&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.41&sourceBranch=refs/tags/@confirmit/react-banner@11.0.42&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.40&sourceBranch=refs/tags/@confirmit/react-banner@11.0.41&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.39&sourceBranch=refs/tags/@confirmit/react-banner@11.0.40&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.38&sourceBranch=refs/tags/@confirmit/react-banner@11.0.39&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.37&sourceBranch=refs/tags/@confirmit/react-banner@11.0.38&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.36&sourceBranch=refs/tags/@confirmit/react-banner@11.0.37&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.35&sourceBranch=refs/tags/@confirmit/react-banner@11.0.36&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.34&sourceBranch=refs/tags/@confirmit/react-banner@11.0.35&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.33&sourceBranch=refs/tags/@confirmit/react-banner@11.0.34&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.31&sourceBranch=refs/tags/@confirmit/react-banner@11.0.32&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.30&sourceBranch=refs/tags/@confirmit/react-banner@11.0.31&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.29&sourceBranch=refs/tags/@confirmit/react-banner@11.0.30&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [11.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.28&sourceBranch=refs/tags/@confirmit/react-banner@11.0.29&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.27&sourceBranch=refs/tags/@confirmit/react-banner@11.0.28&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.26&sourceBranch=refs/tags/@confirmit/react-banner@11.0.27&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.25&sourceBranch=refs/tags/@confirmit/react-banner@11.0.26&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.24&sourceBranch=refs/tags/@confirmit/react-banner@11.0.25&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.23&sourceBranch=refs/tags/@confirmit/react-banner@11.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.22&sourceBranch=refs/tags/@confirmit/react-banner@11.0.23&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.20&sourceBranch=refs/tags/@confirmit/react-banner@11.0.21&targetRepoId=1246) (2021-02-11)

### Features

- Upgrade to supporting react-transition-group 4, to support React strict mode

## [11.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.20&sourceBranch=refs/tags/@confirmit/react-banner@11.0.21&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.19&sourceBranch=refs/tags/@confirmit/react-banner@11.0.20&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.18&sourceBranch=refs/tags/@confirmit/react-banner@11.0.19&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.17&sourceBranch=refs/tags/@confirmit/react-banner@11.0.18&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.16&sourceBranch=refs/tags/@confirmit/react-banner@11.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.15&sourceBranch=refs/tags/@confirmit/react-banner@11.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.14&sourceBranch=refs/tags/@confirmit/react-banner@11.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.13&sourceBranch=refs/tags/@confirmit/react-banner@11.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.12&sourceBranch=refs/tags/@confirmit/react-banner@11.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.11&sourceBranch=refs/tags/@confirmit/react-banner@11.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.10&sourceBranch=refs/tags/@confirmit/react-banner@11.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.8&sourceBranch=refs/tags/@confirmit/react-banner@11.0.9&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.7&sourceBranch=refs/tags/@confirmit/react-banner@11.0.8&targetRepoId=1246) (2020-12-01)

**Note:** Add data tags to help with testing

## [11.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.4&sourceBranch=refs/tags/@confirmit/react-banner@11.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.3&sourceBranch=refs/tags/@confirmit/react-banner@11.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-banner

## [11.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@11.0.2&sourceBranch=refs/tags/@confirmit/react-banner@11.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-banner

# [11.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.21&sourceBranch=refs/tags/@confirmit/react-banner@11.0.0&targetRepoId=1246) (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.20&sourceBranch=refs/tags/@confirmit/react-banner@10.0.21&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.19&sourceBranch=refs/tags/@confirmit/react-banner@10.0.20&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.16&sourceBranch=refs/tags/@confirmit/react-banner@10.0.17&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.15&sourceBranch=refs/tags/@confirmit/react-banner@10.0.16&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.14&sourceBranch=refs/tags/@confirmit/react-banner@10.0.15&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.11&sourceBranch=refs/tags/@confirmit/react-banner@10.0.12&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-banner@10.0.9&sourceBranch=refs/tags/@confirmit/react-banner@10.0.10&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-banner@10.0.5...@confirmit/react-banner@10.0.6) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-banner

## [10.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-banner@10.0.0...@confirmit/react-banner@10.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-banner

# [10.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-banner@9.0.14...@confirmit/react-banner@10.0.0) (2020-08-12)

### Documentation

- updated changelog ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([d21e25f](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/d21e25ff3fdac41f0764ca1e83ad1b115a112ee4))

### BREAKING CHANGES

- peerDependency `@confirmit/react-transition-portal": "^1.3.0"` is required

## CHANGELOG

### v9.0.12

- Internal update: use `useForceUpdate` hook from `@confirmit/react-utils`

### v9.0.11

- Fix: compile issue in 9.0.10

### v9.0.10

- Fix: Design Spec updates:
  - Remove max-width on any direct child `span` inside `comd-banner__text`.
  - Max-width of banner is now 800px.
  - Remove underline on links that are `Button`'s.
  - Avoid details link wrapping onto muitiple lines.
  - Height of each banner is now 50px.
  - Only last item has rounded borders.
  - Font-weight is semibold
  - Increase default timeout to 5 seconds.
- Fix: Show More drawer shows correct background color based on the banner type. Previously all were in `$Danger` color.
- Fix: Appear transition was not working.

### v9.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v8.1.0

- Removing package version in class names.

### v8.0.0

- **BREAKING**

  - Renamed to Banner
  - Renamed package to "@confirmit/react-banner"
  - Removed props "baseClassName", "classNames", "store"
  - No longer supports "default" theme

- Feat: Added ability to specify a link, to see more details about a banner message

- Fix: Correct icon for error messages

### v7.0.0

- **BREAKING**
  - Removed props:
    - `closeIcon`
    - `infoIcon`
    - `successIcon`
    - `errorIcon`
    - `warningIcon`
    - `showMoreIcon`
    - `showLessIcon`
  - Use same icons in default and material theme
  - Update colors to DS spec
  - Same icons for Warning and Error

### v6.0.0

- **BREAKING**:
  - Require a new peerDependency `@confirmit/react-transition-portal`
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v5.1.0

- Feat: Add responsive support. Will go to a max of 100% width on devices and not overflow off screen.

### v5.0.0

- **BREAKING**
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v4.0.7

- chore: make peerDependencies less strict

### v4.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v3.4.0

- Added ability to close toast programmatically.

### v3.3.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

### v3.2.0

- Added CSS modules for scoping the CSS to the packages. Read more about the changes and how to adopt them in your project here: [How to modify the webpack config file](../../docs/CssModules.md)

### v3.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only
- **BREAKING** react-transition-group peer dependency is now v2.x.x
- **BREAKING** Theming support is added. Consumer application has to import at least one theme

```js
// entry.js or application.js
import 'confirmit-toastr/themes/default';
```

- **BREAKING** Toastr css classes are changed. Very strict selectors in tests may be broken.
  - toast is now element of toastr block
  - .co-toast--success => .co-toastr\_\_toast--success

### v2.0.0

- **BREAKING** tapToDismiss option (default true), to allow closing by clicking the entire toastr. Set tapToDismiss to false to turn it off.

### v1.0.0

- precompiled css is used in components (might be a breaking change for webpack scss and css loaders)
