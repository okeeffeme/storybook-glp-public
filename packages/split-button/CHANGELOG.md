# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.35&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.36&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.35&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.35&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.33&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.34&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.32&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.33&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.31&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.32&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.30&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.31&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.29&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.30&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.28&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.29&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.27&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.28&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [10.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.26&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.27&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.23&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.26&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- SplitButton no longer shows divider ([NPM-1254](https://jiraosl.firmglobal.com/browse/NPM-1254)) ([eacdd93](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eacdd938a37a4b0ef7cb5290443d1620da189eec))
- use css proper module to make it work with css loaders ([NPM-1254](https://jiraosl.firmglobal.com/browse/NPM-1254)) ([7da7fa3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7da7fa36a7cedec2cee070ed18029970eb3b62c2))

## [10.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.24&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.25&targetRepoId=1246) (2023-02-16)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.23&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.24&targetRepoId=1246) (2023-02-16)

### Bug Fixes

- SplitButton no longer shows divider ([NPM-1254](https://jiraosl.firmglobal.com/browse/NPM-1254)) ([5c7c885](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5c7c8857ef4f5bb14497c9c42fcd0d57a33bd6c3))
- use css proper module to make it work with css loaders ([NPM-1254](https://jiraosl.firmglobal.com/browse/NPM-1254)) ([6dbca7c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6dbca7ce816de1d29929478c5e52192d076150a8))

## [10.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.22&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.23&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.21&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.22&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.20&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.21&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.19&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.20&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.18&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.19&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.17&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.18&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.16&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.17&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.15&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.16&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.13&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.13&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.14&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.12&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.11&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.10&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.8&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.8&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.5&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.5&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.5&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.4&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.3&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.2&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-split-button

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@10.0.0&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-split-button

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.13&sourceBranch=refs/tags/@jotunheim/react-split-button@10.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of Button from SplitButton ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([758daa9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/758daa9e4473e434ebe49fafa7264e690558aea9))

### Features

- remove className prop of SplitButton ([NPM-953](https://jiraosl.firmglobal.com/browse/NPM-953)) ([770162c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/770162c34fca9b717611729e4c69c89dae87e638))
- remove default theme from SplitButton ([NPM-1078](https://jiraosl.firmglobal.com/browse/NPM-1078)) ([534c04d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/534c04d171cb8bf8a77846a438468553c7fcdc5e))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.12&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.11&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.10&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.9&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.8&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.6&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.3&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.2&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.1&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-split-button

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-split-button@9.0.0&sourceBranch=refs/tags/@jotunheim/react-split-button@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-split-button

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@8.0.1&sourceBranch=refs/tags/@confirmit/react-split-button@8.0.2&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-split-button

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@8.0.0&sourceBranch=refs/tags/@confirmit/react-split-button@8.0.1&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-split-button

## [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.82&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.83&targetRepoId=1246) (2022-06-21)

### BREAKING CHANGES

- update SplitButton.MenuLinkItem and SplitButton.MenuTextItem are replaced by SplitButton.MenuItem. ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([6ce3219](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ce3219a699823f7081cc01e1ad966abef71ba3b))
- see v9 of Dropdown for more details.

## [7.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.81&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.82&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.0.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.80&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.81&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.79&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.80&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.77&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.78&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.76&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.77&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.75&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.76&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.74&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.75&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.73&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.74&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.72&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.73&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.68&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.69&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.67&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.68&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.66&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.67&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.65&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.66&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.64&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.65&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.63&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.64&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.62&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.63&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.61&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.62&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.60&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.61&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.58&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.59&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.57&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.58&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.56&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.57&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.55&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.56&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.54&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.55&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.53&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.54&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.52&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.53&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.51&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.52&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.50&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.51&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.49&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.50&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.48&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.49&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.47&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.48&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.46&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.47&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.45&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.46&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.44&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.45&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.43&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.44&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.42&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.43&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.41&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.42&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.40&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.41&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.39&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.40&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.38&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.39&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.37&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.38&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.36&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.37&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.35&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.36&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.34&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.35&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.32&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.33&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.31&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.32&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.30&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.31&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.29&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.30&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.28&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.29&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.27&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.28&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.26&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.27&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.25&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.26&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.24&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.23&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.24&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.21&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.22&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.20&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.21&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.19&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.20&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.18&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.19&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.17&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.18&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.16&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.15&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.14&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.13&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.12&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.11&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.10&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.7&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.4&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.3&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-split-button

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@7.0.2&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-split-button

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@6.0.2&sourceBranch=refs/tags/@confirmit/react-split-button@7.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@6.0.1&sourceBranch=refs/tags/@confirmit/react-split-button@6.0.2&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-split-button

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@6.0.0&sourceBranch=refs/tags/@confirmit/react-split-button@6.0.1&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-split-button

## [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.20&sourceBranch=refs/tags/@confirmit/react-split-button@6.0.0&targetRepoId=1246) (2020-10-21)

### Features

- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. ([NPM-549](https://jiraosl.firmglobal.com/browse/NPM-549)) ([ec92807](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec92807ea763c63d503963c3a3a42cedd6d863a6))

### BREAKING CHANGES

- meaning of the `type` prop has changed
- the deprecated `type` prop on Button has been changed to reflect the native <button> `type` attribute, and defaults to `button`. (NPM-549)

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.17&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.18&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.16&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.17&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.15&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.16&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.12&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.13&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.10&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.11&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-split-button@5.0.8&sourceBranch=refs/tags/@confirmit/react-split-button@5.0.9&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-split-button@5.0.5...@confirmit/react-split-button@5.0.6) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-split-button

## [5.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-split-button@5.0.0...@confirmit/react-split-button@5.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-split-button

# [5.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-split-button@4.0.14...@confirmit/react-split-button@5.0.0) (2020-08-12)

### Features

- typescript support ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([977fc08](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/977fc080ca8a88ea490b4dff3812350f7eae3761))

### BREAKING CHANGES

- only className, data- and aria- attributes are passed down to the top element

## CHANGELOG

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.2.0

- Removing package version in class names.

### v3.1.0

- Feat: Support Button's `appearance` prop

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.0.0

- BREAKING: removing button.props.size - may cause issues on default-theme

### v1.0.0

- **BREAKING**
  - Removed `icon` prop, this was only intended to be internal
  - Removed `baseClassName` prop, this was only intended to be internal
  - Removed `dropdownButtonType` prop, dropdownButton type will be same as button type
  - Throw a validation warning when `button` is not a `Button` or `WaitButton`. This is done because the implementation is expecting to find a Button (or WaitButton) there, as it gets props from that component. This should probably be changed, but needs more thinking.
- Refactor: Change to useTheme, use new icon package
- Fix: Size of dropdown is smaller
- Fix: Support all button types

### v0.2.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.

### v0.0.1

- Initial version
