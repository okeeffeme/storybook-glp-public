import React from 'react';
import {render} from '@testing-library/react';
import user from '@testing-library/user-event';

import {SplitButton} from '../../src';
import {Button} from '../../../button/src';

describe('Split Button', () => {
  it('should render split button', () => {
    const {queryByText, getByTestId} = render(
      <SplitButton button={<Button>Default action</Button>}>
        <SplitButton.MenuItem data-testid="static">Static</SplitButton.MenuItem>
        <SplitButton.MenuItem data-testid="link" href="/">
          GO there
        </SplitButton.MenuItem>
      </SplitButton>
    );

    expect(queryByText('Default action')).toBeInTheDocument();
    expect(queryByText('Static')).not.toBeInTheDocument();
    expect(queryByText('GO there')).not.toBeInTheDocument();

    user.click(getByTestId('split-button-toggle'));
    expect(queryByText('Static')).toBeInTheDocument();
    expect(queryByText('GO there')).toBeInTheDocument();
  });
});
