import React, {ReactElement, ReactNode} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {Button, ButtonProps} from '@jotunheim/react-button';
import {Dropdown, Placement} from '@jotunheim/react-dropdown';
import Icon, {menuDown} from '@jotunheim/react-icons';

import classNames from './SplitButton.module.css';

type SplitButtonProps = {
  button: ReactElement<
    ButtonProps & {
      'data-split-button-button'?: string;
    }
  >;
  children: ReactNode;
  portalZIndex?: number;
  placement?: Placement;
};

const {block, element} = bemFactory({
  baseClassName: 'comd-split-button',
  classNames,
});

export const SplitButton = ({
  button,
  children,
  portalZIndex,
  placement,
  ...rest
}: SplitButtonProps) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const classes = cn(block());
  const buttonType = button.props.type;
  const buttonAppearance = button.props.appearance;

  const handleToggle = () => {
    if (button.props.disabled) {
      return;
    }
    setIsOpen((val) => !val);
  };

  return (
    <div
      className={classes}
      data-testid="split-button"
      {...extractDataAndAriaProps(rest)}>
      {React.cloneElement(button, {
        ['data-split-button-button']: '',
      })}
      <Dropdown
        open={isOpen}
        onToggle={handleToggle}
        placement={placement}
        portalZIndex={portalZIndex}
        menu={<Dropdown.Menu>{children}</Dropdown.Menu>}>
        <Button
          type={buttonType}
          appearance={buttonAppearance}
          disabled={button.props.disabled}
          data-split-button-toggle=""
          data-testid="split-button-toggle">
          <Icon path={menuDown} className={element('icon')} />
        </Button>
      </Dropdown>
    </div>
  );
};

SplitButton.MenuDivider = Dropdown.MenuDivider;
SplitButton.MenuHeader = Dropdown.MenuHeader;
SplitButton.MenuItem = Dropdown.MenuItem;

export default SplitButton;
