import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {radios, boolean} from '@storybook/addon-knobs';

import {SplitButton} from '../src';
import Button from '../../button/src';
import BusyDots from '../../busy-dots/src/index';
import WaitButton from '../../wait-button/src';

storiesOf('Components/split-button', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('With knobs', () => (
    <SplitButton
      button={
        <Button
          disabled={boolean('disabled', false)}
          appearance={radios(
            'appearance',
            Button.appearances,
            Button.appearances.primaryNeutral
          )}>
          Default action
        </Button>
      }>
      <SplitButton.MenuItem>Static</SplitButton.MenuItem>
      <SplitButton.MenuItem>GO there</SplitButton.MenuItem>
    </SplitButton>
  ))
  .add('with wait button', () => (
    <SplitButton button={<WaitButton busy>Wait action</WaitButton>}>
      <SplitButton.MenuItem>
        Waiting <BusyDots />
      </SplitButton.MenuItem>
      <SplitButton.MenuItem>GO there</SplitButton.MenuItem>
    </SplitButton>
  ))
  .add('with bottom-end placement', () => (
    <SplitButton button={<Button>bottom end</Button>} placement="bottom-end">
      <SplitButton.MenuItem>Static</SplitButton.MenuItem>
      <SplitButton.MenuItem>GO there</SplitButton.MenuItem>
    </SplitButton>
  ));
