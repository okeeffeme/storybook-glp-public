import moment from 'moment';

import units from '../../src/utils/units';
import utils, {createUtils} from '../../src/utils/utils';
import LocaleMoment from '../../src/components/locale-moment';

describe('Utils', () => {
  it('createUtils: should return proper moment date', () => {
    const date = moment().year(2016).month(8).date(1).startOf(units.day);

    const dayDate = createUtils(moment).startDay(date);

    expect(dayDate.isSame(date)).toBe(true);
  });

  it('default moment: should return proper first day of week (0)', () => {
    const date = moment().year(2016).month(8).date(1).startOf(units.day);
    const result = date.localeData().firstDayOfWeek();
    expect(result).toEqual(0);
  });

  it('startDay: should return proper moment date', () => {
    const date = moment().year(2016).month(8).date(1).startOf(units.day);

    const dayDate = utils.startDay(date);

    expect(dayDate.isSame(date)).toBe(true);
  });

  it('getWeekNumbers: should return proper week numbers', () => {
    const date = moment().year(2016).month(8).date(1).startOf(units.day);

    const weekNumbers = utils.getWeekNumbers(date).map((x) => x.week());

    expect(weekNumbers.length).toBe(5);
    expect(weekNumbers).toEqual([36, 37, 38, 39, 40]);
  });

  it('getDaysForMonth: should return proper days in month', () => {
    const date = moment().year(2016).month(8).date(1).startOf(units.day);

    const result = utils.getDaysForMonth(date);

    expect(result.length).toBe(35);
    expect(
      result.filter((x, i) => i === 34 && x.isInNextMonth === true).length
    ).toBe(1);
    expect(
      result.filter((x, i) => i < 4 && x.isInPreviousMonth === true).length
    ).toBe(4);
    result
      .filter((x, i) => i >= 4 && i < 34)
      .forEach((x, i) => {
        expect(
          x.day.isSame(
            moment(date)
              .date(i + 1)
              .startOf(units.day)
          )
        ).toBe(true);
      });
  });

  describe('getDaysOfWeek :: ', () => {
    it('should return proper days of week accoring to locale set using createUtils', () => {
      moment.defineLocale('test-locale', {week: {dow: 1}});
      moment.locale('en');
      const localeMoment = LocaleMoment({localeName: 'test-locale'});

      const date = localeMoment()
        .year(2016)
        .month(8)
        .date(1)
        .startOf(units.day);
      const result = createUtils(localeMoment).getDaysOfWeek(date);
      expect(moment.localeData().weekdaysShort()[result[0].day()]).toEqual(
        'Mon'
      );
    });

    it('should return proper days of week', () => {
      const date = moment().year(2016).month(8).date(1).startOf(units.day);
      const daysOfWeek = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
      const formatDaysOfWeek = daysOfWeek.map((day) => ({day}));
      const getDaysForMonth = spyOn(utils, 'getDaysForMonth').and.returnValue(
        formatDaysOfWeek
      );

      spyOn(utils, 'startDay').and.callFake((x) => x);

      const result = utils.getDaysOfWeek(date);

      expect(getDaysForMonth).toHaveBeenCalledWith(date);
      expect(result.length).toBe(7);
      expect(result).toEqual(daysOfWeek);
    });
  });

  describe('getDaysForMonthOfWeeks :: ', () => {
    it('should return proper days with week flag if selectedDate and DisplayDate are in different months', () => {
      const selectedDate = moment()
        .year(2016)
        .month(8)
        .date(1)
        .startOf(units.day);
      const displayDate = moment()
        .year(2016)
        .month(6)
        .date(1)
        .startOf(units.day);

      const result = utils.getDaysForMonthOfWeeks({selectedDate, displayDate});
      expect(result.length).toBe(42);
      expect(result.filter((x) => x.isInSelectedWeek === true).length).toBe(0);
    });

    it('should return proper days with week flag if selectedDate and DisplayDate are in the same month', () => {
      const selectedDate = moment()
        .year(2016)
        .month(7)
        .date(9)
        .startOf(units.day);
      const displayDate = moment()
        .year(2016)
        .month(7)
        .date(1)
        .startOf(units.day);

      const result = utils.getDaysForMonthOfWeeks({selectedDate, displayDate});
      const daysInWeek = result.filter(
        (x, i) => i >= 7 && i <= 13 && x.isInSelectedWeek === true
      );

      expect(result.length).toBe(35);
      expect(daysInWeek.length).toBe(7);
      daysInWeek.forEach((x, i) => {
        expect(
          x.day.isSame(
            moment(selectedDate)
              .date(i + 7)
              .startOf(units.day)
          )
        ).toBe(true);
      });
    });

    it('should return proper days with disabled flag set if all days are to be disabled', () => {
      const selectedDate = moment()
        .year(2016)
        .month(7)
        .date(9)
        .startOf(units.day);
      const displayDate = moment()
        .year(2016)
        .month(7)
        .date(1)
        .startOf(units.day);

      const result = utils.getDaysForMonthOfWeeks({
        selectedDate,
        displayDate,
        disabled: true,
      });

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.disabled).length).toBe(35);
    });
  });

  describe('getDaysForMonthInRange :: ', () => {
    it('should return proper days in range if selectedRange is before current month', () => {
      const start = moment().year(2016).month(7).date(12).startOf(units.day);
      const end = moment().year(2016).month(7).date(24).startOf(units.day);
      const displayDate = moment().year(2016).month(6);

      const selectedRange = {start, end};
      const result = utils.getDaysForMonthInRange({selectedRange, displayDate});

      expect(result.length).toBe(42);
      expect(result.filter((x) => x.isSelected === true).length).toBe(0);
      expect(result.filter((x) => x.isInSelectedRange === true).length).toBe(0);
      expect(result.filter((x) => x.disabled === true).length).toBe(0);
      expect(result.filter((x) => x.isStartInRange === true).length).toBe(0);
      expect(result.filter((x) => x.isEndInRange === true).length).toBe(0);
    });

    it('should return proper days in range if selectedRange is after current month', () => {
      const start = moment().year(2016).month(7).date(12).startOf(units.day);
      const end = moment().year(2016).month(7).date(24).startOf(units.day);
      const displayDate = moment().year(2016).month(8);

      const selectedRange = {start, end};
      const result = utils.getDaysForMonthInRange({selectedRange, displayDate});

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.isSelected === true).length).toBe(0);
      expect(result.filter((x) => x.isInSelectedRange === true).length).toBe(0);
      expect(result.filter((x) => x.disabled === true).length).toBe(0);
      expect(result.filter((x) => x.isStartInRange === true).length).toBe(0);
      expect(result.filter((x) => x.isEndInRange === true).length).toBe(0);
    });

    it('should return proper days in range if max and min are specified', () => {
      const max = moment().year(2016).month(7).date(24).startOf(units.day);
      const min = moment().year(2016).month(7).date(12).startOf(units.day);
      const displayDate = moment().year(2016).month(7);

      const result = utils.getDaysForMonthInRange({max, min, displayDate});

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.disabled === true).length).toBe(22);
      expect(result.filter((x) => x.isInSelectedRange === true).length).toBe(0);
      expect(result.filter((x) => x.isStartInRange === true).length).toBe(0);
      expect(result.filter((x) => x.isEndInRange === true).length).toBe(0);
    });

    it('should return proper days in range if selectedDate is within current month', () => {
      const selectedDate = moment()
        .year(2016)
        .month(7)
        .date(24)
        .startOf(units.day);
      const displayDate = moment().year(2016).month(7);

      const result = utils.getDaysForMonthInRange({selectedDate, displayDate});

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.disabled === true).length).toBe(0);
      expect(result.filter((x) => x.isInSelectedRange === true).length).toBe(0);
      expect(result.filter((x) => x.isStartInRange === true).length).toBe(0);
      expect(result.filter((x) => x.isEndInRange === true).length).toBe(0);
      expect(result.filter((x) => x.isSelected === true).length).toBe(1);
    });

    it('should return proper days in range if selectedRange is within current month', () => {
      const start = moment().year(2016).month(7).date(12).startOf(units.day);
      const end = moment().year(2016).month(7).date(24).startOf(units.day);
      const displayDate = moment().year(2016).month(7);

      const result = utils.getDaysForMonthInRange({
        selectedRange: {start, end},
        displayDate,
      });

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.disabled === true).length).toBe(0);
      expect(result.filter((x) => x.isInSelectedRange === true).length).toBe(
        13
      );
      expect(result.filter((x) => x.isStartInRange === true).length).toBe(1);
      expect(result.filter((x) => x.isEndInRange === true).length).toBe(1);
    });

    it('should return proper days with disabled flag set if all days are to be disabled', () => {
      const start = moment().year(2016).month(7).date(12).startOf(units.day);
      const end = moment().year(2016).month(7).date(24).startOf(units.day);
      const displayDate = moment().year(2016).month(7);

      const result = utils.getDaysForMonthInRange({
        selectedRange: {start, end},
        displayDate,
        disabled: true,
      });

      expect(result.length).toBe(35);
      expect(result.filter((x) => x.disabled === true).length).toBe(35);
    });

    it('should set isToday flag for todays date only', () => {
      const currentDate = new Date();
      const displayDate = moment()
        .year(currentDate.getFullYear())
        .month(currentDate.getMonth());

      const result = utils.getDaysForMonthInRange({
        displayDate,
      });

      const todayResults = result.filter((x) => x.isToday === true);

      expect(todayResults.length).toBe(1);
      expect(todayResults[0].day.isSame(currentDate, 'day')).toBe(true);
    });
  });
});
