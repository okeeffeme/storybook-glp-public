import React from 'react';
import {render, screen} from '@testing-library/react';
import moment from 'moment';
import userEvent from '@testing-library/user-event';
import units from '../../src/utils/units';
import CalendarControls from '../../src/components/_calendar-controls';
describe('Calendar Controls', () => {
  let displayDate = moment().date(12).month(12).year(2012);

  it('should render proper month', () => {
    const props = {
      displayDate,
      limitRange: {
        maxDate: moment().add(1, units.year),
        minDate: moment().add(-1, units.year),
      },
    };

    render(<CalendarControls {...props} />);
    expect(
      screen.getByTestId('calendar-contols-current-month')
    ).toHaveTextContent(props.displayDate.format('MMMM YYYY'));
  });

  it('should disable controls', () => {
    const props = {
      displayDate,
      limitRange: {
        maxDate: moment().add(1, units.year),
        minDate: moment().add(-1, units.year),
      },
      disabled: true,
    };

    render(<CalendarControls {...props} />);
    expect(screen.getByTestId('calendar-contols-prev-btn').toBeDisabled);
    expect(screen.getByTestId('calendar-contols-next-btn').toBeDisabled);
    expect(screen.getByTestId('calendar-contols-current-month')).toHaveClass(
      'comd-calendar__current-month--disabled'
    );
  });

  it('should call onNextMonth when clicking on "<"', () => {
    const props = {
      displayDate,
      onPrevMonth: jest.fn(),
      limitRange: {
        maxDate: moment().add(1, units.year),
        minDate: moment().add(-1, units.year),
      },
    };

    render(<CalendarControls {...props} />);
    userEvent.click(screen.getByTestId('calendar-contols-prev-btn'));

    expect(props.onPrevMonth).toBeCalled();
  });

  it('should call onNextMonth when clicking on ">"', () => {
    const props = {
      displayDate,
      onNextMonth: jest.fn(),
      limitRange: {
        maxDate: moment().add(1, units.year),
        minDate: moment().add(-1, units.year),
      },
    };

    render(<CalendarControls {...props} />);
    userEvent.click(screen.getByTestId('calendar-contols-next-btn'));

    expect(props.onNextMonth).toBeCalled();
  });
});
