import React from 'react';
import {shallow} from 'enzyme';
import moment from 'moment';

import withMoment from '../../src/components/with-moment-hoc';
import {Calendar} from '../../src/components/calendar';
import CalendarControls from '../../src/components/_calendar-controls.jsx';
import CalendarGrid from '../../src/components/_calendar-grid.jsx';
import units from '../../src/utils/units';

const areDatesEqual = (date1, date2) => {
  return moment(date1).isSame(date2, units.day);
};

describe('Calendar', () => {
  const dayDate = moment().year(2012).month(11).date(12).toDate();

  it('component LocaleCalendar should have proper moment and utils initialized', () => {
    const LocaleCalendar = withMoment(Calendar);
    const wrapper = shallow(<LocaleCalendar />);

    expect(wrapper.prop('moment')).not.toBe(undefined);
    expect(wrapper.prop('utils')).not.toBe(undefined);
  });

  it('component LocaleCalendar should have proper moment and utils when locale is passed as prop', () => {
    const firstDay = 4;
    const LocaleCalendar = withMoment({week: {dow: firstDay}})(Calendar);

    const wrapper = shallow(<LocaleCalendar />);

    expect(wrapper.prop('moment')().localeData().firstDayOfWeek()).toEqual(
      firstDay
    );
    expect(
      moment(
        wrapper.prop('utils').getDaysOfWeek('2016-03-15')[7 - firstDay]
      ).day()
    ).toEqual(0);
  });

  xit('component should have proper className', () => {
    const wrapper = shallow(<Calendar disabled={true} />);

    expect(wrapper.prop('className')).toBe(
      `comd-calendar comd-calendar--disabled`
    );

    wrapper.setProps({disabled: false});

    expect(wrapper.prop('className')).toBe(`comd-calendar`);
  });

  it('should pass proper props to CalendarControls', () => {
    const props = {
      includeWeeks: true,
      date: dayDate,
    };

    const wrapper = shallow(<Calendar {...props} />).find(CalendarControls);
    expect(
      wrapper.prop('displayDate').isSame(moment(dayDate).startOf(units.month))
    );
  });

  it('should pass proper props to CalendarGrid', () => {
    const currentDate = moment(dayDate).add(1, units.day).toDate();
    const date = moment(dayDate).add(2, units.day).toDate();
    const selectedRange = {
      start: currentDate,
      end: date,
    };
    const [minDate, maxDate] = [currentDate, date];

    const props = {
      includeWeeks: true,
      date,
      selectedRange,
      minDate,
      maxDate,
    };

    const wrapper = shallow(<Calendar {...props} />).find(CalendarGrid);

    const {start, end} = wrapper.prop('selectedRange');

    expect(wrapper.prop('includeWeeks')).toBe(props.includeWeeks);
    expect(areDatesEqual(wrapper.prop('selectedDate'), props.date)).toBe(true);
    expect(areDatesEqual(start, selectedRange.start)).toBe(true);
    expect(areDatesEqual(end, selectedRange.end)).toBe(true);
    expect(
      areDatesEqual(wrapper.prop('limitRange').minDate, props.minDate)
    ).toBe(true);
    expect(
      areDatesEqual(wrapper.prop('limitRange').maxDate, props.maxDate)
    ).toBe(true);
  });

  it('should call onDisplayDateChange on calling onNextMonth', () => {
    const props = {
      maxDate: moment(dayDate).add(1, units.year).toDate(),
      minDate: moment(dayDate).add(-1, units.year).toDate(),
      date: dayDate,
      onDisplayDateChange: jest.fn(),
    };
    const wrapper = shallow(<Calendar {...props} />).find(CalendarControls);
    wrapper.simulate('nextMonth');

    expect(props.onDisplayDateChange).toBeCalledWith({month: 0, year: 2013});
  });

  it('should call onDisplayMonthChange on calling onPrevMonth', () => {
    const props = {
      maxDate: moment(dayDate).add(1, units.year).toDate(),
      minDate: moment(dayDate).add(-1, units.year).toDate(),
      date: dayDate,
      onDisplayDateChange: jest.fn(),
    };
    const wrapper = shallow(<Calendar {...props} />).find(CalendarControls);
    wrapper.simulate('prevMonth');

    expect(props.onDisplayDateChange).toBeCalledWith({month: 10, year: 2012});
  });

  it('should call onDisplayYearChange and onDisplayMonthChange when calling onSelect', () => {
    const props = {
      date: dayDate,
      maxDate: moment(dayDate).endOf(units.year).toDate(),
      minDate: moment(dayDate).startOf(units.year).toDate(),
      onDisplayDateChange: jest.fn(),
    };

    const expectedDate = moment(dayDate).subtract(1, units.month);

    const wrapper = shallow(<Calendar {...props} />);
    wrapper.find(CalendarControls).simulate('select', expectedDate);

    expect(props.onDisplayDateChange).toBeCalled();
  });
});
