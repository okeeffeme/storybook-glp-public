import React from 'react';
import {render, screen} from '@testing-library/react';
import moment from 'moment';

import DayOfWeek from '../../src/components/_day-of-week.jsx';

describe('DayOfWeek', () => {
  const [day, month, year] = [12, 12, 2012];
  const dayDate = moment().date(day).month(month).year(year);

  it('should pass proper props to component', () => {
    const props = {
      dayOfWeek: dayDate,
      dayFormat: 'dd',
    };

    render(<DayOfWeek {...props} />);

    expect(screen.getByTestId('day-of-week')).toHaveTextContent(
      props.dayOfWeek.format('dd')
    );
  });
});
