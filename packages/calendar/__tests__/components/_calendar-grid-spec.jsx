import React from 'react';
import moment from 'moment';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CalendarGrid from '../../src/components/_calendar-grid.jsx';

import utils from '../../src/utils/utils';

describe('CalendarGrid', () => {
  const date = moment().year(2012).month(12).date(12);

  it('should have proper className if includeWeeks is passed', () => {
    // the class names used here have been changed
    const props = {
      includeWeeks: true,
      limitRange: {},
      moment,
      utils,
    };

    render(<CalendarGrid {...props} />);
    expect(screen.getByTestId('calendar-grid-days')).toHaveClass(
      'comd-calendar__days comd-calendar__days-in-week'
    );
  });
  it('should render collection of DayOfWeek', () => {
    const daysOfWeek = [moment(), moment().add(1, 'd'), moment(2, 'd')];
    jest.spyOn(utils, 'getDaysOfWeek').mockImplementation(() => daysOfWeek);
    const props = {
      limitRange: {},
      moment,
      utils,
    };
    render(<CalendarGrid {...props} />);
    daysOfWeek.forEach((x, i) => {
      expect(screen.getAllByTestId('day-of-week')[i]).toHaveTextContent(
        x.format('dd')
      );
    });
  });

  it('should render WeekNumbers if includeWeeks is true', () => {
    const props = {
      includeWeeks: true,
      limitRange: {},
      moment,
      utils,
    };
    render(<CalendarGrid {...props} />);
    expect(screen.getAllByTestId('week-numbers')).not.toHaveLength(0);
  });

  it('should not render WeekNumbers if includeWeeks is false', () => {
    const props = {
      includeWeeks: false,
      limitRange: {},
      moment,
      utils,
    };
    render(<CalendarGrid {...props} />);

    expect(screen.queryAllByTestId('week-numbers')).toHaveLength(0);
  });

  it('should render collection of Day if includeWeeks is false', () => {
    const partialProps = {
      selectedDate: {someValue: 'value1'},
      displayDate: {someValue: 'value2'},
      selectedRange: {someValue: 'value5'},
    };

    const limitRange = {
      maxDate: {someValue: 'value3'},
      minDate: {someValue: 'value4'},
    };

    const expectedProps = {
      day: moment(date),
    };

    const getDays = jest
      .spyOn(utils, 'getDaysForMonthInRange')
      .mockImplementation(() => [expectedProps]);

    render(
      <CalendarGrid
        limitRange={limitRange}
        {...partialProps}
        includeWeeks={false}
        {...{moment, utils}}
      />
    );
    expect(screen.getAllByTestId('day')[0]).toHaveTextContent(
      expectedProps.day.format('D')
    );
    expect(getDays).toBeCalledWith({
      ...partialProps,
      max: limitRange.maxDate,
      min: limitRange.minDate,
    });
  });

  it('should render collection of Day if includeWeeks is true', () => {
    const partialProps = {
      selectedDate: {someValue: 'value1'},
      displayDate: {someValue: 'value2'},
    };

    const limitRange = {
      maxDate: {someValue: 'value3'},
      minDate: {someValue: 'value4'},
    };

    const expectedProps = {
      day: moment(date),
    };

    const getDays = jest
      .spyOn(utils, 'getDaysForMonthOfWeeks')
      .mockImplementation(() => [expectedProps]);

    render(
      <CalendarGrid
        {...partialProps}
        limitRange={limitRange}
        includeWeeks={true}
        {...{moment, utils}}
      />
    );
    expect(screen.getAllByTestId('day')[0]).toHaveTextContent(
      expectedProps.day.format('D')
    );
    expect(getDays).toBeCalledWith({
      ...partialProps,
      max: limitRange.maxDate,
      min: limitRange.minDate,
      hoveredWeekNumber: null,
    });
  });

  it('should not call onSelect when clicking on Day if disabled is true', () => {
    const select = jest.fn();
    const expectedProps = {
      day: moment(date),
      disabled: true,
    };
    const props = {
      onSelect: select,
      includeWeeks: true,
      limitRange: {},
      moment,
      utils,
    };
    jest
      .spyOn(utils, 'getDaysForMonthOfWeeks')
      .mockImplementation(() => [expectedProps]);

    render(<CalendarGrid {...props} />);
    userEvent.click(screen.getAllByTestId('day')[0], expectedProps);
    expect(select).not.toBeCalled();
  });

  it('should call onSelect when clicking on Day if disabled is false', () => {
    const select = jest.fn();
    const expectedProps = {
      day: moment(date),
      disabled: false,
    };
    const props = {
      onSelect: select,
      displayDate: moment(date),
      includeWeeks: true,
      limitRange: {},
      moment,
      utils,
    };
    jest
      .spyOn(utils, 'getDaysForMonthOfWeeks')
      .mockImplementation(() => [expectedProps]);
    render(<CalendarGrid {...props} />);
    userEvent.click(screen.getAllByTestId('day')[0]);
    expect(select).toBeCalled();
    expect(select).toBeCalledWith(expectedProps.day);
  });
});

describe('CalendarGrid.IsWeekBetween', () => {
  it('should return true when date range is undefined', () => {
    const props = {
      includeWeeks: true,
      limitRange: {},
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date())).toBe(true);
  });
  it('should return false when the date is before date range', () => {
    const props = {
      includeWeeks: true,
      limitRange: {
        minDate: new Date('2021-05-04'),
        maxDate: new Date('2021-05-25'),
      },
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date('2021-04-01'))).toBe(false);
  });
  it('should return false when the date is later then date range', () => {
    const props = {
      includeWeeks: true,
      limitRange: {
        minDate: new Date('2021-05-04'),
        maxDate: new Date('2021-05-25'),
      },
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date('2021-06-01'))).toBe(false);
  });
  it('should return true when the date is in date range', () => {
    const props = {
      includeWeeks: true,
      limitRange: {
        minDate: new Date('2021-05-04'),
        maxDate: new Date('2021-05-25'),
      },
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date('2021-05-15'))).toBe(true);
  });
  it('should return true when the date is in the same week as minDate even if it is  before', () => {
    const props = {
      includeWeeks: true,
      limitRange: {
        minDate: new Date('2021-05-04'),
        maxDate: new Date('2021-05-25'),
      },
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date('2021-05-03'))).toBe(true);
  });
  it('should return true when the date is in the same week as maxDate even if it is  before', () => {
    const props = {
      includeWeeks: true,
      limitRange: {
        minDate: new Date('2021-05-04'),
        maxDate: new Date('2021-05-25'),
      },
      moment,
    };
    const grid = new CalendarGrid(props);
    expect(grid.isWeekBetween(new Date('2021-05-26'))).toBe(true);
  });
});
