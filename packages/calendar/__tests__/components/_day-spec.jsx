import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';

import Day from '../../src/components/_day.jsx';

describe('Day', () => {
  const dayDate = moment().date(12).month(12).year(2012);

  it('should pass proper props to Day', () => {
    const props = {
      day: dayDate,
    };

    render(<Day {...props} />);
    expect(screen.getByTestId('day')).toHaveTextContent(props.day.format('D'));
  });

  it('should get modifier classes when attributes are set', () => {
    const props = {
      day: dayDate,
      isInPreviousMonth: true,
      isInNextMonth: true,
      disabled: true,
      isInSelectedRange: true,
      isSelected: true,
      isToday: true,
      isStartInRange: true,
      isEndInRange: true,
      isInSelectedWeek: true,
    };

    render(<Day {...props} />);
    expect(screen.getByTestId('day')).toHaveClass(
      'comd-calendar__day',
      'comd-calendar__day-prev-month',
      'comd-calendar__day-next-month',
      'comd-calendar__day--disabled',
      'comd-calendar__day--selected-range',
      'comd-calendar__day--selected',
      'comd-calendar__day--today',
      'comd-calendar__day--start-range',
      'comd-calendar__day--end-range',
      'comd-calendar__day--selected-week'
    );
  });

  it('should not get modifier classes when attributes are not set', () => {
    const props = {
      day: dayDate,
    };
    const expectedClasses = 'comd-calendar__day';
    render(<Day {...props} />);
    expect(screen.getByTestId('day')).toHaveClass(expectedClasses);
    expect(screen.getByTestId('day')).not.toHaveClass(
      'comd-calendar__day-prev-month',
      'comd-calendar__day-next-month',
      'comd-calendar__day--disabled',
      'comd-calendar__day--selected-range',
      'comd-calendar__day--selected',
      'comd-calendar__day--today',
      'comd-calendar__day--start-range',
      'comd-calendar__day--end-range',
      'comd-calendar__day--selected-week'
    );
  });

  it('should call onClick when clicking on component', () => {
    const props = {
      day: dayDate,
      onClick: jest.fn(),
      disabled: false,
    };

    render(<Day {...props} />);
    userEvent.click(screen.getByTestId('day'));

    expect(props.onClick).toHaveBeenCalledWith({
      day: props.day,
      disabled: props.disabled,
    });
  });

  it('should render proper day content with custom renderDayContent prop', () => {
    /* eslint-disable react/prop-types */
    const renderDayContent = ({day}) => {
      return <content>{day.date()}</content>;
    };
    /* eslint-enable react/prop-types */
    const props = {
      day: dayDate,
      renderDayContent,
    };

    render(<Day {...props} />);
    expect(screen.getByTestId('day')).toHaveTextContent(props.day.format('DD'));
  });

  it('should render empty span with custom renderDayContent prop if the day is in the previous month and includeWeeks is false', () => {
    /* eslint-disable react/prop-types */
    const renderDayContent = ({day}) => {
      return <content>{day.date()}</content>;
    };
    /* eslint-enable react/prop-types */
    const props = {
      day: dayDate,
      renderDayContent,
      includeWeeks: false,
      isInPreviousMonth: true,
    };

    render(<Day {...props} />);

    expect(screen.getByTestId('day')).toHaveTextContent('');
  });

  it('should render empty span with custom renderDayContent prop if the day is in the next month and includeWeeks is false', () => {
    /* eslint-disable react/prop-types */
    const renderDayContent = ({day}) => {
      return <content>{day.date()}</content>;
    };
    /* eslint-enable react/prop-types */
    const props = {
      day: dayDate,
      renderDayContent,
      includeWeeks: false,
      isInNextMonth: true,
    };

    render(<Day {...props} />);

    expect(screen.getByTestId('day')).toHaveTextContent('');
  });

  it('should render proper day content with custom renderDayContent prop if the day is in the next month and includeWeeks is true', () => {
    /* eslint-disable react/prop-types */
    const renderDayContent = ({day}) => {
      return <content>{day.date()}</content>;
    };
    /* eslint-enable react/prop-types */
    const props = {
      day: dayDate,
      renderDayContent,
      includeWeeks: true,
      isInNextMonth: true,
    };

    render(<Day {...props} />);

    expect(screen.getByTestId('day')).toHaveTextContent(props.day.format('DD'));
  });
});
