import React from 'react';
import moment from 'moment';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import utils from './../../src/utils/utils';
import units from './../../src/utils/units';
import WeekNumbers from '../../src/components/_week-numbers.jsx';

describe('WeekNumbers', () => {
  const dayDate = moment().year(2012).month(11).date(12);

  it('should render collection of .week-number', () => {
    const props = {
      displayDate: dayDate,
      moment,
      utils,
    };
    render(<WeekNumbers {...props} />);
    utils.getWeekNumbers(props.displayDate).forEach((x, i) => {
      expect(screen.getAllByTestId('week-numbers')[i]).toHaveTextContent(
        x.week().toString()
      );
      expect(screen.getAllByTestId('week-numbers')[i]).toHaveAttribute(
        'data-week',
        x.week().toString()
      );
    });
  });

  it('should call onWeekNumberHover with proper argument when calling mouseOver', () => {
    const onMouseEnter = jest.fn();
    const props = {
      displayDate: dayDate,
      onWeekNumberHover: onMouseEnter,
      moment,
      utils,
    };

    const dataset = {
      week: props.displayDate.week(),
      year: props.displayDate.year(),
      month: props.displayDate.month(),
    };
    render(<WeekNumbers {...props} />);
    userEvent.hover(screen.getByText(`${props.displayDate.week()}`), {
      currentTarget: {dataset},
    });

    expect(props.onWeekNumberHover).toHaveBeenCalledWith(dataset);
  });

  it('should call onWeekNumberOut with proper argument when calling mouseOut', () => {
    const props = {
      displayDate: dayDate,
      onWeekNumberOut: jest.fn(),
      moment,
      utils,
    };

    render(<WeekNumbers {...props} />);
    userEvent.unhover(screen.getByText(`${props.displayDate.week()}`));

    expect(props.onWeekNumberOut).toHaveBeenCalled();
  });

  it('should call onWeekNumberClick with proper argument when calling click', () => {
    const props = {
      displayDate: dayDate,
      onWeekNumberHover: jest.fn(),
      onWeekNumberClick: jest.fn(),
      maxDate: moment(dayDate).add(10, units.week),
      minDate: moment(dayDate).subtract(10, units.week),
      moment,
      utils,
    };
    const dataset = {
      week: props.displayDate.week(),
      year: props.displayDate.year(),
      month: props.displayDate.month(),
    };

    render(<WeekNumbers {...props} />);
    userEvent.click(screen.getByText(`${props.displayDate.week()}`), {
      currentTarget: {dataset},
    });

    expect(props.onWeekNumberClick).toHaveBeenCalledWith(dataset);
  });
});
