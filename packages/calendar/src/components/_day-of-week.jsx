import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {element} from '../utils/bem';

export default class DayOfWeek extends Component {
  static propTypes = {
    dayOfWeek: PropTypes.object,
    dayFormat: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  };

  static defaultProps = {
    dayFormat: 'dd',
  };

  render() {
    const {dayOfWeek, dayFormat} = this.props;

    const formattedDayOfWeek =
      typeof dayFormat === 'function'
        ? dayFormat(dayOfWeek)
        : dayOfWeek.format(dayFormat);

    return (
      <div className={element('day-of-week')}>
        <span data-day-of-week="" data-testid="day-of-week">
          {formattedDayOfWeek}
        </span>
      </div>
    );
  }
}
