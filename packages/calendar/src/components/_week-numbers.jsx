import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {isBetween} from '../utils/date-utils';

import {element} from '../utils/bem';

export default class WeekNumbers extends Component {
  static propTypes = {
    displayDate: PropTypes.object,
    dayOfWeek: PropTypes.string,
    baseClassName: PropTypes.string,
    classNames: PropTypes.object,
    maxDate: PropTypes.object,
    minDate: PropTypes.object,
    onWeekNumberHover: PropTypes.func,
    onWeekNumberClick: PropTypes.func,
    onWeekNumberOut: PropTypes.func,
    moment: PropTypes.func,
    utils: PropTypes.object,
    selectedWeek: PropTypes.number,
    hoveredWeek: PropTypes.number,
  };

  parseArgument = (e) => {
    const {week, year, month} = e.currentTarget.dataset;

    return {
      week: +week,
      year: +year,
      month: +month,
    };
  };

  handleMouseOver = (e) => {
    this.props.onWeekNumberHover(this.parseArgument(e));
  };

  handleMouseOut = () => {
    this.props.onWeekNumberOut();
  };

  handleClick = (e) => {
    this.props.onWeekNumberClick(this.parseArgument(e));
  };

  isWeekNumberSelected = (date) => {
    const {moment, selectedWeek} = this.props;
    return (
      (selectedWeek !== null && selectedWeek === date.week()) ||
      (selectedWeek === null && moment().week() === date.week())
    );
  };

  isWeekNumberHovered = (date) => {
    const {hoveredWeek} = this.props;
    return hoveredWeek !== null && hoveredWeek === date.week();
  };

  render() {
    const {displayDate, maxDate, minDate, moment, utils} = this.props;

    const weekNumbers = utils.getWeekNumbers(displayDate).map((date, i) => {
      const className = cn(element('week-number'), {
        [element('week-number', 'disabled')]: !isBetween(
          {date, minDate, maxDate},
          moment
        ),
        [element('week-number', 'selected')]: this.isWeekNumberSelected(date),
        [element('week-number', 'hovered')]: this.isWeekNumberHovered(date),
      });

      return (
        <div
          className={className}
          data-week-number=""
          data-testid="week-numbers"
          key={i}
          data-week={date.week()}
          data-year={date.year()}
          data-month={date.month()}
          onMouseOver={this.handleMouseOver}
          onMouseOut={this.handleMouseOut}
          onClick={this.handleClick}>
          <div className={element('week-number-background')}>
            <span>{date.week()}</span>
          </div>
        </div>
      );
    });

    return <div className={element('week-numbers')}>{weekNumbers}</div>;
  }
}
