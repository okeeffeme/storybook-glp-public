import React, {Component} from 'react';
import PropTypes from 'prop-types';
import defaultMoment from 'moment';
import cn from 'classnames';
import {uncontrollable} from 'uncontrollable';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import CalendarControls from './_calendar-controls';
import CalendarGrid from './_calendar-grid';
import units from '../utils/units';
import defaultUtils from '../utils/utils';

import {block, modifier} from '../utils/bem';

const dateValuePropTypes = PropTypes.oneOfType([
  PropTypes.instanceOf(Date),
  PropTypes.instanceOf(defaultMoment),
]);

export class Calendar extends Component {
  static propTypes = {
    date: dateValuePropTypes,
    displayDate: PropTypes.shape({
      month: PropTypes.number,
      year: PropTypes.number,
    }),

    minDate: dateValuePropTypes,
    maxDate: dateValuePropTypes,
    selectedRange: PropTypes.shape({
      start: dateValuePropTypes,
      end: dateValuePropTypes,
    }),
    yearRange: PropTypes.number,
    includeWeeks: PropTypes.bool,
    selectExactDate: PropTypes.bool,
    disabled: PropTypes.bool,
    monthFormat: PropTypes.string,
    dayFormat: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    moment: PropTypes.func,
    utils: PropTypes.object,

    onSelect: PropTypes.func,
    onDisplayDateChange: PropTypes.func,
    renderDayContent: PropTypes.func,
  };

  static defaultProps = {
    moment: defaultMoment,
    utils: defaultUtils,
    yearRange: 20,
    date: null,
    selectedRange: {
      start: null,
      end: null,
    },
    includeWeeks: false,
    selectExactDate: true,
    monthFormat: 'MMMM YYYY',
    dayFormat: 'dd',
    disabled: false,
    onSelect: () => void 0,
    onDisplayDateChange: () => void 0,
  };

  handleClickNextMonth = () => this.changeMonth(1);

  handleClickPrevMonth = () => this.changeMonth(-1);

  changeMonth = (number) => {
    const displayDate = this.getDisplayDate({...this.props});
    const nextDate = this.props.moment(displayDate).add(number, units.month);

    this.handleSelectDisplayDate(nextDate);
  };

  handleSelectDisplayDate = (date) => {
    const {maxDate, minDate} = this.getLimitRange();
    const {moment, displayDate, date: currentDate} = this.props;

    let nextDisplayDate = date;

    if (minDate && date.isBefore(minDate)) {
      nextDisplayDate = moment(minDate);
    }
    if (maxDate && date.isAfter(maxDate)) {
      nextDisplayDate = moment(maxDate);
    }

    if (
      !nextDisplayDate.isSame(
        this.getDisplayDate({displayDate, moment, date: currentDate}),
        units.day
      )
    ) {
      this.props.onDisplayDateChange({
        year: nextDisplayDate.year(),
        month: nextDisplayDate.month(),
      });
    }
  };

  handleSelect = (date) => {
    this.props.onSelect(date.toDate());
  };

  getLimitRange = () => {
    const {maxDate, minDate, moment} = this.props;

    return {
      maxDate: maxDate ? moment(maxDate) : null,
      minDate: minDate ? moment(minDate) : null,
    };
  };

  getDisplayDate = ({date, displayDate, moment}) => {
    const currentDate = date ? moment(date) : moment();
    const year =
      !displayDate || isNaN(parseInt(displayDate.year))
        ? currentDate.year()
        : displayDate.year;
    const month =
      !displayDate || isNaN(parseInt(displayDate.month))
        ? currentDate.month()
        : displayDate.month;

    return moment().year(year).month(month).date(1);
  };

  render() {
    const {
      date,
      selectedRange: {start, end},
      monthFormat,
      dayFormat,
    } = this.props;
    const {
      includeWeeks,
      disabled,
      selectExactDate,
      yearRange,
      moment,
      utils,
      renderDayContent,
    } = this.props;

    const displayDate = this.getDisplayDate({...this.props});
    const limitRange = this.getLimitRange();
    const actualYearRange = utils.getYearRange({
      limitRange,
      displayDate,
      range: yearRange,
    });

    const props = {
      dayFormat,
      includeWeeks,
      displayDate,
      selectedDate: date ? moment(date) : null,
      selectedRange: {
        start: start,
        end: end,
      },
      limitRange,
      moment,
      utils,
      renderDayContent,
      disabled,
    };

    const className = cn(block(), {
      [modifier('include-weeks')]: includeWeeks,
      [modifier('disabled')]: disabled,
    });

    return (
      <div
        className={className}
        data-testid="calendar"
        {...extractDataAriaIdProps(this.props)}>
        <CalendarControls
          {...{displayDate, selectExactDate, limitRange, moment}}
          yearRange={actualYearRange}
          monthFormat={monthFormat}
          onSelect={this.handleSelectDisplayDate}
          onNextMonth={this.handleClickNextMonth}
          onPrevMonth={this.handleClickPrevMonth}
          disabled={disabled}
        />
        <CalendarGrid {...props} onSelect={this.handleSelect} />
      </div>
    );
  }
}

const UncontrollableCalendar = uncontrollable(Calendar, {
  date: 'onSelect',
  displayDate: 'onDisplayDateChange',
});

export default UncontrollableCalendar;
