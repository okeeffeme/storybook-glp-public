import React, {useContext} from 'react';
import PropTypes from 'prop-types';

import {createUtils} from '../utils/utils';
import LocaleMoment from './locale-moment';
import MomentContext from '../moment-context';

const getRandomString = () =>
  'xxxxxxxx'.replace(/x/g, () => ((Math.random() * 16) | 0).toString(16));

const withMomentUtilsHOC = moment => Component => {
  const withMomentHOC = props => (
    <Component utils={createUtils(moment)} moment={moment} {...props} />
  );

  return withMomentHOC;
};

const withMoment = locale => {
  if (typeof locale === 'undefined') {
    return withMoment;
  }

  if (locale.hasOwnProperty('locale') && typeof locale['locale'] === 'string') {
    return withMomentUtilsHOC(LocaleMoment({localeName: locale['locale']}));
  }

  if (typeof locale === 'string') {
    return withMomentUtilsHOC(LocaleMoment({localeName: locale}));
  }

  if (locale.hasOwnProperty('defineLocale')) {
    return withMomentUtilsHOC(locale);
  }

  if (
    React.Component.isPrototypeOf(locale) ||
    locale.hasOwnProperty('propTypes') ||
    locale.hasOwnProperty('isReactComponent') ||
    typeof locale === 'function'
  ) {
    return (Component => {
      const WithMoment = ({moment, ...rest}) => {
        const momentFromContext = useContext(MomentContext);

        moment = moment || momentFromContext;

        return (
          <Component utils={createUtils(moment)} moment={moment} {...rest} />
        );
      };

      WithMoment.propTypes = {
        moment: PropTypes.func,
      };

      return WithMoment;
    })(locale);
  }

  return Component => {
    const localeName = getRandomString();
    const localeMoment = LocaleMoment({localeName, locale});

    const WithMoment = props => {
      return (
        <Component
          utils={createUtils(localeMoment)}
          moment={localeMoment}
          {...props}
        />
      );
    };

    return WithMoment;
  };
};

export default withMoment;
