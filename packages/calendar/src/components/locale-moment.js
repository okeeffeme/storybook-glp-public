import defaultMoment from 'moment';

const LocaleMoment = ({localeName, locale}) => {
  if (locale) {
    const globalLocale = defaultMoment.locale();
    defaultMoment.defineLocale(localeName, JSON.parse(JSON.stringify(locale)));
    defaultMoment.locale(globalLocale);
  }

  const moment = (...rest) => {
    const momentInstance = defaultMoment(...rest);
    momentInstance.locale(localeName);
    return momentInstance;
  };

  Object.keys(defaultMoment)
    .filter(k => typeof defaultMoment[k] === 'function')
    .forEach(k => {
      moment[k] = defaultMoment[k];
    });

  moment.getLocaleName = moment.getLocaleName || (() => localeName);

  return moment;
};

export default LocaleMoment;
