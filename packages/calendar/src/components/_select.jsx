import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {element} from '../utils/bem';

export default class Select extends Component {
  static propTypes = {
    collection: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      })
    ),
    value: PropTypes.number,
    className: PropTypes.string,
    onSelect: PropTypes.func,
  };

  static defaultProps = {
    collection: [],
  };

  handleSelect = (e) => {
    const {collection, onSelect} = this.props;
    const id = +e.target.value;
    const name = collection.find((x) => x.id === id).name;
    onSelect({id, name});
  };

  render() {
    const {collection, className, value} = this.props;

    const classes = cn(element('df-select'), className);

    return (
      <span className={element('df-select-wrapper')}>
        <select className={classes} value={value} onChange={this.handleSelect}>
          {collection.map((item) => {
            return (
              <option key={item.id} value={item.id}>
                {item.name}
              </option>
            );
          })}
        </select>
      </span>
    );
  }
}
