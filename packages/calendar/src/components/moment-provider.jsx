import React from 'react';
import PropTypes from 'prop-types';

import MomentContext from '../moment-context';

const MomentProvider = ({children, moment, ...rest}) => (
  <MomentContext.Provider value={moment}>
    <div {...rest}>{children}</div>
  </MomentContext.Provider>
);

MomentProvider.propTypes = {
  moment: PropTypes.func,
  children: PropTypes.any,
};

export default MomentProvider;
