import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Icon, {chevronLeft, chevronRight} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';

import Select from './_select';

import {getMonthNames, getYears} from '../utils/date-utils';
import units from '../utils/units';

import {element} from '../utils/bem';

export default class CalendarControls extends Component {
  static propTypes = {
    displayDate: PropTypes.object,
    limitRange: PropTypes.shape({
      maxDate: PropTypes.object,
      minDate: PropTypes.object,
    }),
    yearRange: PropTypes.shape({
      top: PropTypes.number,
      bottom: PropTypes.number,
    }),
    selectExactDate: PropTypes.bool,
    monthFormat: PropTypes.string,
    onSelect: PropTypes.func,
    onNextMonth: PropTypes.func,
    onPrevMonth: PropTypes.func,
    moment: PropTypes.func,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    monthFormat: 'MMMM YYYY',
  };

  constructor(props) {
    super(props);

    this.state = {
      showMonthYear: false,
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = (e) => {
    if (this.wrapperRef && !this.wrapperRef.contains(e.target)) {
      this.setState({showMonthYear: false});
    }
  };

  handleNext = () => {
    this.props.onNextMonth();
  };

  handlePrevious = () => {
    this.props.onPrevMonth();
  };

  handleSelectMonth = ({id}) => {
    const {displayDate, onSelect, moment} = this.props;
    const newDate = moment(displayDate).month(id);

    if (displayDate.isSame(newDate, units.month)) return;

    onSelect(newDate);
  };

  handleSelectYear = ({id}) => {
    const {displayDate, onSelect, moment} = this.props;
    const newDate = moment(displayDate).year(id);

    if (displayDate.isSame(newDate, units.year)) return;

    onSelect(newDate);
  };

  handleMonthClick = () => {
    const {selectExactDate} = this.props;

    selectExactDate &&
      !this.state.showMonthYear &&
      this.setState({
        showMonthYear: !this.state.showMonthYear,
      });
  };

  render() {
    const {
      displayDate,
      limitRange: {maxDate, minDate},
      yearRange,
      monthFormat,
      moment,
      disabled,
    } = this.props;
    const {showMonthYear} = this.state;

    const limit = {
      max:
        maxDate && displayDate.year() === maxDate.year()
          ? maxDate.month()
          : undefined,
      min:
        minDate && displayDate.year() === minDate.year()
          ? minDate.month()
          : undefined,
    };

    const months = () => getMonthNames(limit, moment);
    const years = () => getYears(yearRange, displayDate.year(), moment);

    const prevButtonClasses = cn(
      element('controls-nav-btn'),
      element('prevmonth')
    );
    const nextButtonClasses = cn(
      element('controls-nav-btn'),
      element('nextmonth')
    );

    const isPreviousButtonDisabled = () =>
      disabled || !!displayDate.isSame(minDate, units.month);
    const isNextButtonDisabled = () =>
      disabled || !!displayDate.isSame(maxDate, units.month);

    return (
      <div
        ref={(x) => (this.wrapperRef = x)}
        className={element('controls-container')}>
        <div className={element('controls')}>
          <div className={prevButtonClasses}>
            <IconButton
              disabled={isPreviousButtonDisabled()}
              onClick={this.handlePrevious}
              data-nav-btn="data-nav-prev-btn"
              data-testid="calendar-contols-prev-btn">
              <Icon path={chevronLeft} />
            </IconButton>
          </div>
          <div
            onClick={this.handleMonthClick}
            className={cn({
              [element('current-month')]: !showMonthYear,
              [element('current-month', 'disabled')]:
                !showMonthYear && disabled,
              [element('select-controls')]: showMonthYear,
            })}
            data-current-month=""
            data-testid="calendar-contols-current-month">
            {!showMonthYear && displayDate.format(monthFormat)}
            {showMonthYear && (
              <Select
                value={displayDate.month()}
                collection={months()}
                onSelect={this.handleSelectMonth}
              />
            )}
            {showMonthYear && (
              <Select
                value={displayDate.year()}
                collection={years()}
                onSelect={this.handleSelectYear}
              />
            )}
          </div>
          <div className={nextButtonClasses}>
            <IconButton
              disabled={isNextButtonDisabled()}
              onClick={this.handleNext}
              data-nav-btn="data-nav-next-btn"
              data-testid="calendar-contols-next-btn">
              <Icon path={chevronRight} />
            </IconButton>
          </div>
        </div>
      </div>
    );
  }
}
