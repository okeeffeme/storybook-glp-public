import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Day from './_day';
import DayOfWeek from './_day-of-week';
import WeekNumbers from './_week-numbers';

import units from '../utils/units';

import {element} from '../utils/bem';

export default class CalendarGrid extends Component {
  static propTypes = {
    displayDate: PropTypes.object,
    selectedDate: PropTypes.object,
    selectedRange: PropTypes.shape({
      start: PropTypes.object,
      end: PropTypes.object,
    }),
    dayFormat: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    limitRange: PropTypes.shape({
      minDate: PropTypes.object,
      maxDate: PropTypes.object,
    }),
    includeWeeks: PropTypes.bool,
    hoveredWeekNumber: PropTypes.object,
    onSelect: PropTypes.func,
    moment: PropTypes.func,
    utils: PropTypes.object,
    renderDayContent: PropTypes.func,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    dayFormat: 'dd',
  };

  constructor(props) {
    super(props);

    const {includeWeeks, selectedDate, moment} = this.props;

    this.state = {
      hoveredWeekNumber: null,
      selectedWeek:
        includeWeeks && selectedDate ? moment(selectedDate).week() : null,
      hoveredWeek: null,
    };
  }

  parseDateFromWeek = ({week, month, year}) => {
    const weekNumber = +week === 1 && +month !== 0 ? 53 : +week;
    return this.props.moment().year(year).week(weekNumber);
  };

  handleDaySelect = ({day, disabled}) => this.handleDateSelect(day, disabled);

  handleDateSelect = (date, disabled) => {
    const {includeWeeks, displayDate, onSelect, moment} = this.props;

    if (disabled) return;

    if (includeWeeks) {
      const startOfMonth = moment(displayDate).startOf(units.month);
      const endOfMonth = moment(displayDate).endOf(units.month);

      this.setState({selectedWeek: date.week()});

      if (date.isBefore(startOfMonth)) {
        return onSelect(startOfMonth);
      }
      if (date.week() === 1) {
        return onSelect(moment(date).endOf(units.week));
      }
      if (date.isAfter(endOfMonth)) {
        return onSelect(endOfMonth);
      }
    }

    return onSelect(date);
  };

  isWeekBetween = (date) => {
    const {maxDate, minDate} = this.props.limitRange;
    const {moment} = this.props;
    let result = !!date;
    if (result && minDate)
      result = moment(minDate).week() <= moment(date).week();
    if (result && maxDate)
      result = moment(maxDate).week() >= moment(date).week();
    return result;
  };

  handleWeekClick = (payload) => {
    const date = this.parseDateFromWeek(payload);
    const disabled = !this.isWeekBetween(date);
    this.handleDateSelect(date, disabled);
  };

  handleWeekNumberHover = (payload) => {
    this.setState({
      hoveredWeekNumber: this.parseDateFromWeek(payload),
    });
  };

  handleWeekNumberOut = () => {
    this.setState({
      hoveredWeekNumber: null,
    });
  };

  handleWeekHover = ({day, disabled}) => {
    if (disabled) return;
    this.setState({hoveredWeek: day.week()});
  };

  handleWeekOut = () => {
    this.setState({
      hoveredWeek: null,
    });
  };

  renderDays = () => {
    const {
      selectedDate,
      selectedRange,
      includeWeeks,
      displayDate,
      utils,
      renderDayContent,
      disabled,
    } = this.props;
    const {minDate: min, maxDate: max} = this.props.limitRange;
    const {hoveredWeekNumber} = this.state;
    const dayObjects = includeWeeks
      ? utils.getDaysForMonthOfWeeks({
          selectedDate,
          displayDate,
          hoveredWeekNumber,
          min,
          max,
          disabled,
        })
      : utils.getDaysForMonthInRange({
          selectedRange,
          selectedDate,
          displayDate,
          min,
          max,
          disabled,
        });

    const weeks = dayObjects.reduce((weeks, day, i) => {
      if (i % 7 === 0) {
        weeks.push([]);
      }
      const index = Math.floor(i / 7);
      weeks[index].push(day);
      return weeks;
    }, []);

    return weeks.map((days, i) => {
      const isHoveredWeekNumber = days[0].day.isSame(
        hoveredWeekNumber,
        units.week
      );

      const className = cn(element('week'), {
        [element('week', 'hovered')]: isHoveredWeekNumber,
        [element('week', 'hover-whole')]: includeWeeks,
      });

      return (
        <div className={className} key={i}>
          {days.map((props, i) => (
            <Day
              key={i}
              renderDayContent={renderDayContent}
              {...props}
              onClick={this.handleDaySelect}
              onMouseOver={this.handleWeekHover}
              onMouseOut={this.handleWeekOut}
              includeWeeks={includeWeeks}
            />
          ))}
        </div>
      );
    });
  };

  renderDaysOfWeek = () => {
    const {dayFormat, utils} = this.props;
    return utils.getDaysOfWeek().map((dayOfWeek, i) => {
      return <DayOfWeek key={i} dayOfWeek={dayOfWeek} dayFormat={dayFormat} />;
    });
  };

  render() {
    const {includeWeeks, displayDate, limitRange, utils, moment} = this.props;

    const {selectedWeek, hoveredWeek} = this.state;

    const weekNumberProps = {
      ...limitRange,
      displayDate,
      utils,
      moment,
    };
    const daysClassName = cn(element('days'), {
      [element('days-in-week')]: includeWeeks,
    });

    return (
      <div className={element('grid-container')}>
        {includeWeeks && (
          <WeekNumbers
            onWeekNumberClick={this.handleWeekClick}
            onWeekNumberHover={this.handleWeekNumberHover}
            onWeekNumberOut={this.handleWeekNumberOut}
            selectedWeek={selectedWeek}
            hoveredWeek={hoveredWeek}
            {...weekNumberProps}
          />
        )}
        <div className={element('grid')}>
          <div className={element('days-of-week')} data-days-of-week="">
            {this.renderDaysOfWeek()}
          </div>
          <div className={daysClassName} data-testid="calendar-grid-days">
            {this.renderDays()}
          </div>
        </div>
      </div>
    );
  }
}
