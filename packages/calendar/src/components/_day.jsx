import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {element} from '../utils/bem';

export default class Day extends Component {
  static propTypes = {
    day: PropTypes.shape({
      format: PropTypes.func,
      toDate: PropTypes.func,
    }),
    isInPreviousMonth: PropTypes.bool,
    isInNextMonth: PropTypes.bool,
    disabled: PropTypes.bool,
    isInSelectedRange: PropTypes.bool,
    isSelected: PropTypes.bool,
    isToday: PropTypes.bool,
    isStartInRange: PropTypes.bool,
    isEndInRange: PropTypes.bool,
    isInSelectedWeek: PropTypes.bool,
    includeWeeks: PropTypes.bool,
    onClick: PropTypes.func,
    onMouseOver: PropTypes.func,
    onMouseOut: PropTypes.func,
    renderDayContent: PropTypes.func,
  };

  static defaultProps = {
    renderDayContent: ({day}) => {
      return (
        <div>
          <span>{day.date()}</span>
        </div>
      );
    },
  };

  handleClick = () => {
    const {onClick, day, disabled} = this.props;
    onClick && onClick({day, disabled});
  };

  handleMouseOver = () => {
    const {onMouseOver, day, disabled} = this.props;
    onMouseOver && onMouseOver({day, disabled});
  };

  handleMouseOut = () => {
    const {onMouseOut} = this.props;
    onMouseOut && onMouseOut();
  };

  renderDay = () => {
    const {
      day,
      renderDayContent,
      includeWeeks,
      isInPreviousMonth,
      isInNextMonth,
    } = this.props;

    const isVisibleDay = includeWeeks || !(isInNextMonth || isInPreviousMonth);
    const dayContent = isVisibleDay ? renderDayContent({day}) : <span />;

    return <div className={element('day-background')}>{dayContent}</div>;
  };

  render() {
    const {
      isInPreviousMonth,
      isInNextMonth,
      disabled,
      isInSelectedRange,
      isSelected,
      isToday,
      isStartInRange,
      isEndInRange,
      isInSelectedWeek,
      includeWeeks,
    } = this.props;

    const resultClassName = cn(element('day'), {
      [element('day-prev-month', 'visible')]: isInPreviousMonth && includeWeeks,
      [element('day-next-month', 'visible')]: isInNextMonth && includeWeeks,
      [element('day-prev-month')]: isInPreviousMonth,
      [element('day-next-month')]: isInNextMonth,
      [element('day', 'disabled')]: disabled,
      [element('day', 'selected-range')]: isInSelectedRange,
      [element('day', 'selected')]: isSelected,
      [element('day', 'today')]: isToday,
      [element('day', 'start-range')]: isStartInRange,
      [element('day', 'end-range')]: isEndInRange,
      [element('day', 'selected-week')]: isInSelectedWeek,
    });

    return (
      <div
        className={resultClassName}
        onClick={this.handleClick}
        onMouseOver={this.handleMouseOver}
        onMouseOut={this.handleMouseOut}
        data-testid="day">
        {this.renderDay()}
      </div>
    );
  }
}
