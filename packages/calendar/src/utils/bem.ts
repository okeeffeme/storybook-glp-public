import {bemFactory} from '@jotunheim/react-themes';

import classNames from '../components/calendar.module.css';

const {block, element, modifier} = bemFactory('comd-calendar', classNames);

export {block, element, modifier};
