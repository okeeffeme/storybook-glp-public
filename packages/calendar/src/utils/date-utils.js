import defaultMoment from 'moment';
import units from '../utils/units';

const createArray = length => callback => {
  const array = [];
  for (let i = 0; i < length; i++) {
    array.push(callback(i));
  }

  return array;
};

const getMonthNames = ({min = 0, max = 11}, moment = defaultMoment) => {
  return moment.months().reduce((prev, current, i) => {
    if (i <= max && i >= min) prev.push({id: i, name: current});

    return prev;
  }, []);
};

const getYears = (
  {top, bottom} = {top: 0, bottom: 15},
  current,
  moment = defaultMoment
) => {
  current = current || moment().year();
  const count = Math.abs(top) + Math.abs(bottom) + 1;
  return createArray(count)(i => ({
    id: current + top - i,
    name: current + top - i,
  }));
};

const isBetween = ({date, maxDate, minDate}, moment = defaultMoment) => {
  const isBefore =
    !minDate ||
    moment(minDate)
      .startOf(units.week)
      .isBefore(date);
  const isAfter =
    !maxDate ||
    moment(maxDate)
      .endOf(units.week)
      .isAfter(date);

  return isBefore && isAfter;
};

export {getYears, getMonthNames, isBetween};
