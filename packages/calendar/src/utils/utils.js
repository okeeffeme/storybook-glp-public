import defaultMoment from 'moment';

import units from './units';

class Utils {
  constructor(moment) {
    this.moment = moment || defaultMoment;
  }

  startDay = (x) => this.moment(x).startOf(units.day);

  getDaysForMonth = (monthDate) => {
    const days = [];

    const currentMonth = this.startDay(monthDate).startOf(units.month);

    const weekday = currentMonth.weekday();

    for (let i = 0; i < weekday; i++) {
      const result = this.startDay(currentMonth).date(i - weekday + 1);
      days.push({day: result, isInPreviousMonth: true});
    }

    const dayCount = currentMonth.daysInMonth();
    for (let i = 1; i <= dayCount; i++) {
      const result = this.startDay(currentMonth).date(i);
      days.push({day: result});
    }

    for (let i = 1; days.length % 7 !== 0; i++) {
      const result = this.startDay(currentMonth).date(dayCount + i);
      days.push({day: result, isInNextMonth: true});
    }

    return days;
  };

  getDaysForMonthOfWeeks = ({
    selectedDate: selected = null,
    displayDate: current = null,
    min = null,
    max = null,
    disabled = false,
  }) => {
    const selectedDate = this.startDay(selected);
    const startOfWeek = selectedDate.isValid()
      ? this.startDay(this.startDay(selectedDate).startOf(units.week))
      : null;
    const endOfWeek = selectedDate.isValid()
      ? this.startDay(this.startDay(selectedDate).endOf(units.week))
      : null;
    const minDate = min && this.startDay(min);
    const maxDate = max && this.startDay(max);
    const displayDate = this.startDay(current);

    return this.getDaysForMonth(displayDate).map((dayObject) => {
      const result = {...dayObject};
      const {day: dayMoment} = result;

      result.disabled =
        disabled ||
        (minDate && minDate.isAfter(dayMoment)) ||
        (maxDate && maxDate.isBefore(dayMoment));

      if (
        startOfWeek &&
        endOfWeek &&
        startOfWeek.diff(dayMoment, units.day) <= 0 &&
        endOfWeek.diff(dayMoment, units.day) >= 0
      ) {
        result.isInSelectedWeek = true;
      }

      return result;
    });
  };

  getDaysForMonthInRange = ({
    selectedDate: selected = null,
    min = null,
    max = null,
    displayDate,
    selectedRange = {},
    disabled = false,
  }) => {
    const minDate = min && this.startDay(min);
    const maxDate = max && this.startDay(max);
    const selectedDate = selected && this.startDay(selected);
    const [start, end] = [
      this.startDay(selectedRange.start),
      this.startDay(selectedRange.end),
    ];

    return this.getDaysForMonth(displayDate).map((dayObject) => {
      const result = {...dayObject};
      const {day: dayMoment} = result;

      result.disabled =
        disabled ||
        (minDate && minDate.isAfter(dayMoment)) ||
        (maxDate && maxDate.isBefore(dayMoment));

      if (
        start &&
        end &&
        start.diff(dayMoment, units.day) <= 0 &&
        end.diff(dayMoment, units.day) >= 0
      ) {
        result.isInSelectedRange = true;
      }

      if (dayMoment.isSame(new Date(), 'day', units.day)) {
        result.isToday = true;
      }

      if (dayMoment.isSame(selectedDate, units.day)) {
        result.isSelected = true;
      }

      if (dayMoment.isSame(start, units.day)) {
        result.isStartInRange = true;
      }

      if (dayMoment.isSame(end, units.day)) {
        result.isEndInRange = true;
      }
      return result;
    });
  };

  getDaysOfWeek = (currentMonth) => {
    const daysOfWeek = [];
    const days = this.getDaysForMonth(currentMonth);

    for (let i = 0; i < 7; i++) {
      daysOfWeek.push(this.startDay(days[i].day));
    }
    return daysOfWeek;
  };

  getWeekNumbers = (currentMonth) => {
    const firstDay = this.startDay(currentMonth).startOf(units.month);
    const lastDay = this.startDay(currentMonth).endOf(units.month);

    let last = firstDay;
    const result = [last];
    while (last.week() !== lastDay.week()) {
      last = this.moment(last).add(1, units.week);
      result.push(last);
    }

    return result;
  };

  getYearRange = ({limitRange: {maxDate, minDate}, displayDate, range}) => {
    const currentYear = displayDate.year();
    const maxYear = maxDate ? maxDate.year() : null;
    const minYear = minDate ? minDate.year() : null;

    const yearRange =
      maxYear && minYear && range > maxYear - minYear
        ? maxYear - minYear
        : range;

    const top = parseInt(yearRange / 2);
    let topResult =
      maxYear && currentYear + top > maxYear ? maxYear - currentYear : top;
    const bottom = yearRange - topResult;
    const bottomResult =
      minYear && currentYear - bottom < minYear
        ? currentYear - minYear
        : bottom;
    topResult += yearRange - topResult - bottomResult;

    return {
      top: topResult,
      bottom: bottomResult,
    };
  };
}

function createUtils(moment) {
  return new Utils(moment);
}

const utils = new Utils();
export {utils as default, createUtils};
