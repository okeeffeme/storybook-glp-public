# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.27&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.27&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.25&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.26&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.24&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.25&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.23&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.24&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.22&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.23&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.21&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.22&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.20&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.21&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.19&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.20&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.18&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.19&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.17&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.18&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add test ids within calendar package ([NPM-1177](https://jiraosl.firmglobal.com/browse/NPM-1177)) ([7f82359](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7f823596925a2a9d0ff65a0b1cb95461d7da26a4))

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.16&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.17&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.15&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.16&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.14&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.15&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.13&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.14&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.11&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.13&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.11&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.12&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.10&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.9&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.7&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.7&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.4&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.4&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.4&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.3&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.2&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-calendar

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@10.0.0&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-calendar

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.12&sourceBranch=refs/tags/@jotunheim/react-calendar@10.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove default theme from Calendar ([NPM-1065](https://jiraosl.firmglobal.com/browse/NPM-1065)) ([a82704a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a82704a036643f61732f85b6c06ada16bd5b8131))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.11&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.12&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.10&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.11&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.9&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.10&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.8&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.9&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.6&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.3&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.2&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.1&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-calendar

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-calendar@9.0.0&sourceBranch=refs/tags/@jotunheim/react-calendar@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-calendar

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.28&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.29&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.27&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.28&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.26&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.27&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.25&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.26&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.24&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.25&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.22&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.23&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.21&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.22&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.20&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.21&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.19&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.20&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.18&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.19&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.17&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.18&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.14&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.15&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.13&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.14&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.12&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.13&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.11&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.12&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.10&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.11&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.9&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.10&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.8&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.9&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [8.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.7&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.8&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.5&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.6&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.4&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.5&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.3&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.4&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.2&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.3&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.1&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.2&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.1.0&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.1&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-calendar

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.46&sourceBranch=refs/tags/@confirmit/react-calendar@8.1.0&targetRepoId=1246) (2021-07-27)

### Features

- update calendar to use correct colors according to the Design System spec, and show a circle around todays date ([NPM-497](https://jiraosl.firmglobal.com/browse/NPM-497)) ([3d5c3c1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3d5c3c1121654ad4a41f55e8bd6d287516403bed))

## [8.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.45&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.46&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.44&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.45&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.43&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.44&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.42&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.43&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.41&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.42&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.40&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.41&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.39&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.40&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.38&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.39&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.37&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.38&targetRepoId=1246) (2021-06-02)

### Bug Fixes

- Init selectedWeek value according to initial selectedDate ([SREP-4376](https://jiraosl.firmglobal.com/browse/SREP-4376)) ([88693b46c43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/88693b46c4359e6185023a89ed6a35d632e4f691))

## [8.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.36&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.37&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.35&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.36&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.34&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.35&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.33&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.34&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.32&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.33&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.31&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.32&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.29&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.30&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.28&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.29&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.27&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.28&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [8.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.26&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.27&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.25&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.26&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.24&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.25&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.23&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.24&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.22&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.23&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.21&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.22&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.20&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.21&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.19&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.20&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.18&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.19&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.17&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.18&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.16&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.17&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.15&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.16&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.14&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.13&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.12&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.11&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.10&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.9&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.7&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.4&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.3&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-calendar

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@8.0.2&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-calendar

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.35&sourceBranch=refs/tags/@confirmit/react-calendar@8.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.34&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.35&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.33&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.34&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.30&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.31&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.29&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.30&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.28&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.29&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.25&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.26&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-calendar@7.0.23&sourceBranch=refs/tags/@confirmit/react-calendar@7.0.24&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.20](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-calendar@7.0.19...@confirmit/react-calendar@7.0.20) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.15](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-calendar@7.0.14...@confirmit/react-calendar@7.0.15) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-calendar

## [7.0.14](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-calendar@7.0.13...@confirmit/react-calendar@7.0.14) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-calendar

## CHANGELOG

### v7.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- **BREAKING**: Remove internal exposed prop `navigationIcon`, and use same icons in material and default theme.
- Refactor: New package name: `@confirmit/react-calendar`.

### v6.1.0

- Removing package version in class names.

### v6.0.8

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v6.0.6

- Fix: update to correct colors

### v6.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v5.0.0

- **BREAKING**
  - Removed MomentProvider export, only supported via withMoment HOC. Could not find any real usage of it. If needed we should also export the new MomentContext object, otherwise it cannot be consumed.
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v4.0.8

- fix: (default theme) incorrect display of prev/next buttons

### v4.0.2

- fix: render day content only for visible days (revert prev behaviour)

### v4.0.1

- fix: start and end of range in calendar are now have rounded borders (revert prev styles)

### v4.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v3.0.0

- **BREAKING** Removed unused isActive prop, added disabled prop for disabling the calendar.
- **BREAKING** Classnames have been renamed. If you have used calendar specific classnames to override styling or in tests, this might break.
- Styling updates on the material theme.
- Week day is marked as chosen/hovered when the week row is marked as chosen/hovered.
- Support for css modules.

### v2.3.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

### v.2.2.0

- Feat: Pass down data- and aria- attributes

### v.2.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only
- **BREAKING** Theming support is added. Consumer application has to import at least one theme

```js
// entry.js or application.js
import 'confirmit-calendar/themes/default';
```
