import React from 'react';
import {storiesOf} from '@storybook/react';

import moment from 'moment';
import withMoment from '../src/components/with-moment-hoc';

import Section from './section';
import SectionWithControlled from './section-controlled-calendar';
import SectionTwoCalendars from './section-two-calendars';

import './calendar-demo.scss';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
const SectionWithMoment = withMoment({week: {dow: 3}})(Section);

/* eslint-disable react/prop-types */
const renderDayContent = ({day}) => {
  return (
    <span className="custom-class" style={{color: 'orange'}}>
      {day.date()}
    </span>
  );
};
/* eslint-enable react/prop-types */

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{paddingLeft: '100px'}}>{children}</div>
);

storiesOf('Components/calendar', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('basic', () => (
    <Container>
      <Section title="Basic" />
      <Section
        title="With custom day content render"
        month={11}
        renderDayContent={renderDayContent}
      />
      <Section title="Min date" date={new Date()} minDate={new Date()} />
      <Section title="Max date" date={new Date()} maxDate={new Date()} />
      <Section title="Weeks" date={new Date()} includeWeeks={true} />
      <Section
        title="selectExactDate: false"
        date={new Date()}
        selectExactDate={false}
      />
      <Section
        title="With range"
        date={moment(new Date())}
        selectedRange={{
          start: moment().subtract(7, 'd'),
          end: moment().add(7, 'd'),
        }}
      />
      <Section title="Disabled" disabled={true} />
    </Container>
  ))
  .add('double', () => (
    <Container>
      <SectionTwoCalendars title="Two calendars" />
    </Container>
  ))
  .add('with custom moment', () => (
    <Container>
      <SectionWithMoment
        title="First day is wednesday"
        date={new Date()}
        minDate={moment()}
        selectExactDate={true}
        selectedRange={{
          start: moment().subtract(7, 'd').toDate(),
          end: moment().add(7, 'd').toDate(),
        }}
      />
    </Container>
  ))
  .add('controlled month and year change', () => (
    <Container>
      <SectionWithControlled
        title="Manual month props change"
        displayDate={{month: 8}}
        date={moment().month(9).toDate()}
      />
      <SectionWithControlled title="With props on render" />
    </Container>
  ));
