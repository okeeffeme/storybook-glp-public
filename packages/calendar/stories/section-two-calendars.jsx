import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Calendar from '../src/index';

export default class SectionTwoCalendars extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: props.date,
    };
  }

  handleSelect = nextDate => {
    this.setState({
      date: nextDate,
    });
  };

  render() {
    const {title, ...rest} = this.props;
    const {date} = this.state;

    return (
      <section>
        <h3>{title}</h3>
        <div className="calendars-container">
          <Calendar {...rest} date={date} onSelect={this.handleSelect} />
          <Calendar {...rest} date={date} onSelect={this.handleSelect} />
        </div>
      </section>
    );
  }
}

SectionTwoCalendars.propTypes = {
  date: PropTypes.object,
  title: PropTypes.string,
};
