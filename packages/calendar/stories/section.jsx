import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CalendarUncontrolled from '../src/index';

export default class Section extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: props.date,
    };
  }

  handleSelect = nextDate => {
    this.setState({
      date: nextDate,
    });
  };

  render() {
    const {title, ...rest} = this.props;
    const {date} = this.state;

    return (
      <section>
        <h3>{title}</h3>
        <div className="container">
          <CalendarUncontrolled
            {...rest}
            date={date}
            onSelect={this.handleSelect}
          />
        </div>
      </section>
    );
  }
}

Section.propTypes = {
  date: PropTypes.object,
  title: PropTypes.string,
};
