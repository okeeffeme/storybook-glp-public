import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Calendar from '../src/index';

import moment from 'moment';

export default class Section extends Component {
  constructor(props) {
    super(props);

    const date = props.date ? moment(props.date) : moment();
    const displayDate = props.displayDate || {
      year: date.year(),
      month: date.month(),
    };

    this.state = {
      date: date.toDate(),
      displayDate,
    };
  }

  handleNextMonthClick = () => {
    this.setState(prevState => {
      return {
        displayDate: {
          month: (prevState.displayDate.month + 1) % 12,
        },
      };
    });
  };

  handleNextDayClick = () => {
    this.setState(prevState => {
      return {
        date: moment(prevState.date)
          .add(1, 'day')
          .toDate(),
      };
    });
  };

  handleSelect = nextDate => {
    this.setState({
      date: nextDate,
    });
  };

  handleDisplayDateChange = value => {
    this.setState({
      displayDate: value,
    });
  };

  render() {
    const {title, ...rest} = this.props;
    const {displayDate, date} = this.state;

    return (
      <section>
        <h3>{title}</h3>
        <div className="container">
          <Calendar
            {...rest}
            date={date}
            displayDate={displayDate}
            onSelect={this.handleSelect}
            onDisplayDateChange={this.handleDisplayDateChange}
          />
          <div>
            <button onClick={this.handleNextMonthClick}>
              Next month ({displayDate.month + 1})
            </button>
            <button onClick={this.handleNextDayClick}>
              Next day ({date.getDate()})
            </button>
          </div>
        </div>
      </section>
    );
  }
}

Section.propTypes = {
  title: PropTypes.string,

  date: PropTypes.object,
  displayDate: PropTypes.shape({
    year: PropTypes.number,
    month: PropTypes.number,
  }),
};
