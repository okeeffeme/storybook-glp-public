import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text, select, number} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Tabs from '../src';
import Icon, {viewSequential, numeric, pencil, trashBin} from '../../icons/src';

storiesOf('Components/tabs', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [selectedTabId, setSelectedTabId] = React.useState('tab1');
    return (
      <div>
        <Tabs
          selectedTabId={selectedTabId}
          onTabClicked={(id) => setSelectedTabId(id)}>
          <Tabs.Tab
            title="First Tab Title"
            id="tab1"
            hasPadding={boolean('hasPadding', true)}>
            First tab content
          </Tabs.Tab>
          <Tabs.Tab title="Second Tab Title" id="tab2">
            Selecting this tab, tab 4 will become selectable
          </Tabs.Tab>
          <Tabs.Tab title="Disabled Tab" id="tab3" disabled={true}>
            This tab is disabled, you should not be able to reach the content
          </Tabs.Tab>
          <Tabs.Tab
            title="Disabled Tab"
            id="tab4"
            disabled={selectedTabId !== 'tab2'}>
            This tab is only enabled when you have tab 2 selected.
          </Tabs.Tab>
        </Tabs>
      </div>
    );
  })
  .add('With Icon', () => {
    const [selectedTabId, setSelectedTabId] = React.useState('tab1');
    return (
      <div>
        <Tabs
          selectedTabId={selectedTabId}
          onTabClicked={(id) => setSelectedTabId(id)}>
          <Tabs.Tab
            title="First Tab Title"
            id="tab1"
            tabIcon={<Icon path={viewSequential} />}>
            First tab content
          </Tabs.Tab>
          <Tabs.Tab
            title="Second Tab Title"
            id="tab2"
            tabIcon={<Icon path={numeric} />}>
            Tab 2
          </Tabs.Tab>
          <Tabs.Tab title="Tab with no icon" id="tab3">
            Tab 3
          </Tabs.Tab>
        </Tabs>
      </div>
    );
  })
  .add('With tabChildren and headerChildren', () => {
    const [selectedTabId, setSelectedTabId] = React.useState('tab1');
    return (
      <div>
        <Tabs
          headerChildren={() => (
            <div style={{background: 'lightblue'}}>This is headerChildren</div>
          )}
          selectedTabId={selectedTabId}
          onTabClicked={(id) => setSelectedTabId(id)}>
          <Tabs.Tab
            title="first tab title"
            id="tab1"
            tabChildren={
              <div style={{background: 'wheat'}}>This is tabChildren</div>
            }>
            First tab content
          </Tabs.Tab>
          <Tabs.Tab title="second tab title" id="tab2">
            Selecting this tab, tab 4 will become selectable
          </Tabs.Tab>
          <Tabs.Tab title="disabled tab" id="tab3" disabled={true}>
            This tab is disabled, you should not be able to reach the content
          </Tabs.Tab>
          <Tabs.Tab
            title="disabled tab"
            id="tab4"
            disabled={selectedTabId !== 'tab2'}>
            This tab is only enabled when you have tab 2 selected.
          </Tabs.Tab>
        </Tabs>
      </div>
    );
  })
  .add('With no children', () => {
    const [selectedTabId, setSelectedTabId] = React.useState('tab1');
    return (
      <div>
        <Tabs
          selectedTabId={selectedTabId}
          onTabClicked={(id) => setSelectedTabId(id)}>
          <Tabs.Tab title="first tab title" id="tab1" />
          <Tabs.Tab title="second tab title" id="tab2" />
        </Tabs>
      </div>
    );
  })
  .add('With tabs as links', () => {
    const [selectedTabId, setSelectedTabId] = React.useState('tab1');
    return (
      <div>
        <Tabs
          selectedTabId={selectedTabId}
          onTabClicked={(id) => setSelectedTabId(id)}>
          <Tabs.Tab href="/tab1" title="first tab title" id="tab1" />
          <Tabs.Tab href="/tab2" title="second tab title" id="tab2" />
        </Tabs>
      </div>
    );
  })
  .add('With knobs', () => {
    const tabs = Array.from(
      {length: number('tabCount', 5)},
      (_value, index) => `tabid${index + 1}`
    );

    const [selectedTabId, setSelectedTabId] = React.useState(tabs[0]);

    return (
      <div>
        <Tabs
          selectedTabId={select('selectedTabId', tabs, selectedTabId, 'Tabs')}
          onTabClicked={(id) => setSelectedTabId(id)}>
          {tabs.map((tab, index) => (
            <Tabs.Tab
              key={tab}
              hasPadding={boolean(`hasPadding (${tab})`, true, `tab ${index}`)}
              disabled={boolean(`disabled (${tab})`, false, `tab ${index}`)}
              title={text(`title (${tab})`, `${tab} tab title`, `tab ${index}`)}
              id={text(`id (${tab})`, `${tab}`, `tab ${index}`)}>
              Content of tab {tab}
            </Tabs.Tab>
          ))}
        </Tabs>
      </div>
    );
  })
  .add('Many tabs', () => {
    const tabs = Array.from(
      {length: number('tabCount', 30)},
      (_value, index) => `tabid${index + 1}`
    );

    const [selectedTabId, setSelectedTabId] = React.useState('tabid10');

    return (
      <Tabs
        selectedTabId={selectedTabId}
        onTabClicked={(id) => setSelectedTabId(id)}>
        {tabs.map((option) => (
          <Tabs.Tab key={option} id={option} title={`title of ${option}`}>
            <div>
              On load, tabs should select and scroll automatically to tab 10
              (unless tab 10 is already visible).
            </div>
            <div>Content of {option}.</div>
            <div>
              Scroll programatically to:
              {tabs.map((t, index) => {
                return (
                  <button key={index} onClick={() => setSelectedTabId(t)}>
                    {t}
                  </button>
                );
              })}
            </div>
          </Tabs.Tab>
        ))}
      </Tabs>
    );
  })
  .add('With options dropdown', () => {
    const tabs = Array.from(
      {length: 5},
      (_value, index) => `tabid${index + 1}`
    );

    const optionsMenuItems = [
      {
        command: 'edit',
        name: 'Edit',
        iconPath: pencil,
      },
      {
        command: 'delete',
        name: 'Delete',
        iconPath: trashBin,
      },
    ];
    const [selectedTabId, setSelectedTabId] = React.useState('tabid1');
    const [selectedCommand, setSelectedCommand] = React.useState<
      string | undefined
    >(undefined);

    return (
      <Tabs
        selectedTabId={selectedTabId}
        onTabClicked={(id) => setSelectedTabId(id)}>
        {tabs.map((option) => (
          <Tabs.Tab
            key={option}
            id={option}
            title={`title of ${option}`}
            optionsMenuItems={optionsMenuItems}
            onOptionSelect={(command) => setSelectedCommand(command)}>
            {selectedCommand
              ? `${selectedCommand} selected`
              : 'Select from dropdown'}
          </Tabs.Tab>
        ))}
      </Tabs>
    );
  });
