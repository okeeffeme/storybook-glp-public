# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.12&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.13&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.12&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.12&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.10&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.11&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.9&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.10&targetRepoId=1246) (2023-03-30)

### Bug Fixes

- fix TabButton ref assigning ([NPM-1286](https://jiraosl.firmglobal.com/browse/NPM-1286)) ([dcc67aa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dcc67aa5f594536efc0eb013711cd147ef2280aa))

## [12.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.8&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.9&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.7&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.8&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.6&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.7&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.5&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.6&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.4&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.5&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.3&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.4&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [12.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.2&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.3&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.1&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.2&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.2.0&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.1&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-tabs

# [12.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.20&sourceBranch=refs/tags/@jotunheim/react-tabs@12.2.0&targetRepoId=1246) (2023-02-06)

### Bug Fixes

- fixing import errors ([NPM-1181](https://jiraosl.firmglobal.com/browse/NPM-1181)) ([4526eca](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4526eca2c80c5d297f39369acb71c0f39f42d5a8))

### Features

- Convert tabs package to TypeScript ([NPM-1243](https://jiraosl.firmglobal.com/browse/NPM-1243)) ([9b308b6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9b308b69b1767b3f700d3009c4a29bd9ec914548))

# [12.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.20&sourceBranch=refs/tags/@jotunheim/react-tabs@12.1.0&targetRepoId=1246) (2023-02-06)

### Features

- Convert tabs package to TypeScript ([NPM-1243](https://jiraosl.firmglobal.com/browse/NPM-1243)) ([9b308b6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9b308b69b1767b3f700d3009c4a29bd9ec914548))

## [12.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.19&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.20&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.18&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.19&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.17&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.18&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.16&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.17&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.15&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.16&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.13&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.13&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.14&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.12&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.11&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.10&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.8&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.8&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.5&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.5&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.5&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.4&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.3&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.2&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-tabs

## [12.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@12.0.0&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-tabs

# [12.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.13&sourceBranch=refs/tags/@jotunheim/react-tabs@12.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Tabs ([NPM-956](https://jiraosl.firmglobal.com/browse/NPM-956)) ([844e29a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/844e29af42f68bbb715dfd7aa35ca0d5fe8be33e))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [11.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.12&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.11&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.10&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.9&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.8&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.6&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.3&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.2&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.1&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-tabs

## [11.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tabs@11.0.0&sourceBranch=refs/tags/@jotunheim/react-tabs@11.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-tabs

# 11.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.15&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.16&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.14&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.15&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.13&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.14&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- update Dropdown.MenuItem usage ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([6ce3219](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ce3219a699823f7081cc01e1ad966abef71ba3b))

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.12&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.13&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.11&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.12&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.10&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.11&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.8&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.9&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.7&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.8&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.6&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.7&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.5&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.6&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.4&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.5&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-tabs

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@10.0.3&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.4&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-tabs

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.13&sourceBranch=refs/tags/@confirmit/react-tabs@10.0.0&targetRepoId=1246) (2022-02-11)

### BREAKING CHANGES

- remove "icon" prop from optionsMenuItems (NPM-858)

## [9.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.12&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.13&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.11&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.12&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.10&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.11&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.9&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.10&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.8&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.9&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.7&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.8&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.6&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.7&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.5&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.6&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [9.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.4&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.5&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.2&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.1&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.2&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.2.0&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-tabs

# [9.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.29&sourceBranch=refs/tags/@confirmit/react-tabs@9.2.0&targetRepoId=1246) (2021-09-13)

### Features

- support iconPath for tab button dropdown options ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([9066748](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9066748dd5ea3bf578aac0dd023f5df71babd33a))

## [9.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.28&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.29&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.27&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.28&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.26&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.27&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.25&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.26&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.24&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.25&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.23&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.24&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.22&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.23&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.21&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.22&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.20&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.21&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.19&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.20&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.18&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.19&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.17&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.18&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.16&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.17&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.15&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.16&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.14&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.15&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.13&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.14&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.12&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.13&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.11&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.12&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.10&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.11&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.9&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.10&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.8&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.9&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.6&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.7&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.5&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.6&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.4&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.5&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [9.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.3&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.4&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.2&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.3&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.1&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.2&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.1.0&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.1&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-tabs

# [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.27&sourceBranch=refs/tags/@confirmit/react-tabs@9.1.0&targetRepoId=1246) (2021-03-10)

### Features

- add option to turn off padding on Tab ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([e6f6e74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e6f6e74508e55f83a390f30f253f93b306df2273))

## [9.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.26&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.27&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.25&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.24&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.22&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.23&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.21&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.20&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.19&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.18&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.17&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.18&targetRepoId=1246) (2020-12-16)

### Bug Fixes

- tab-button should support routing ([NPM-659](https://jiraosl.firmglobal.com/browse/NPM-659)) ([db136af](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db136af5aaba7b9845b214432a42fb59c05a190b))

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.16&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.15&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.14&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.13&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.12&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.11&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.10&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.7&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.4&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.3&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-tabs

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@9.0.2&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-tabs

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.31&sourceBranch=refs/tags/@confirmit/react-tabs@9.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [8.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.30&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.31&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.29&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.30&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.28&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.29&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.25&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.26&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.24&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.25&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.23&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.24&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.20&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.21&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.18&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.19&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tabs@8.1.16&sourceBranch=refs/tags/@confirmit/react-tabs@8.1.17&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.14](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tabs@8.1.13...@confirmit/react-tabs@8.1.14) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.9](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tabs@8.1.8...@confirmit/react-tabs@8.1.9) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-tabs

## [8.1.8](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tabs@8.1.7...@confirmit/react-tabs@8.1.8) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-tabs

## CHANGELOG

## v8.1.0

- Feat: Add support for showing icon in Tab with `tabIcon` prop
- Fix: Update styles according to latest Design spec:
  - Top padding for tabs-container is gone, that reduces tab-container size from 56px to 48px
  - Add 8px margin on right of Options dropdown
  - Center align text in tab (previously it was centered including the extra 8px top padding for the entire container which looked weird)
- Fix: Issue where tabs would automatically scroll to already selected tab one extra time when scrolled out of view

## v8.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-tabs`

### v7.1.0

- Removing package version in class names.

### v7.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Remove "baseClassName" and "classNames" from Tabs and Tab components
  - Package version as css class name is added to blocks and elements at compile time.

### v6.3.2

- Fix: Add color style for tab buttons for pseudo selectors link, visited, hover, active to avoid issues with color changing based on styles set globally on `a` elements

### v6.3.0

- Feat: Add `href` prop on Tab component. If this is set, the Tab will be rendered as an anchor instead of a button.
- Fix: Add negative margin-top on tabChildren and tabHeaderChildren so they align properly with the Tab title
- Fix: Reorder `tabChildren` and `optionsMenu` on Tab so that `optionsMenu` is always last
- Fix: `optionsMenu` toggle incorrectly rendered both an anchor and a button
- Fix: Update size of scrollbuttons to be same size as other IconButtons
- Refactor: Use new icons package

### v6.2.0

- Added props: tabClass, activeTabClass, disabledTabClass

### v6.1.2

- Refactor: update internal components using poppers to use `placement` instead of `defaultPlacement` prop

### v6.1.1

- Fix: scroll to selected tab id when its outside the visible area.
  - This used to only work the first time the tabs were rendered. Now it happens on each render, if the tabid is outside the visible area.

### v6.1.0

- Add optional options dropdown menu to Tab component (suppressed by default) with new optionsMenuItems,
  and onOptionSelect props. optionsMenuItems takes an array of: command, name, icon (optional)

### v6.0.19

- fix: Remove redundant classname in tab that were not used which resulted in undefined classname

### v6.0.0

- feat: Update tabs component styles to match new mock ups

### v5.0.0

**BREAKING**

- If the selected tab has no visible children, don't render the tab-container div that would
  go around the children

### v4.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.
- Fix: Minor style adjustments on hovered/selected tabs

### v3.1.0

- feat: make `selectedTabId` not required, and default to empty string.
- fix: update styling of material design to UX guidelines

### v3.0.0

- **BREAKING**: Update `react` and `react-dom` requirement to ^16.8.0
- **BREAKING**: Content of TabItem will now be rendered as the content of the tab.
- **BREAKING**: `Tabs.Item` renamed to `Tabs.Tab`.
- feat: scroll buttons will appear if there are more tabs than there is room for in the visible area
- feat: support extra content in TabHeader area with `headerChildren` (see demo pages)
- feat: support extra content in Tab with `tabChildren` (see demo pages)
- fix: correct styling for Default theme

### v2.1.0

- Update styling for tabs

### v2.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v1.0.0

- Initial version
