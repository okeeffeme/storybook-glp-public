import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Tabs from '../../src/components/tabs';
import Icon, {numeric} from '../../../icons/src';

describe('Tabs', () => {
  it('should render anchor instead of button if href is present on Tab', () => {
    render(
      <Tabs
        data-testid="tabs"
        onTabClicked={jest.fn()}
        selectedTabId="tab1"
        showBorder={false}>
        <Tabs.Tab href="some-link" id="tab1">
          tab1
        </Tabs.Tab>
      </Tabs>
    );
    expect(screen.getByRole('tab')).toHaveAttribute('href');
    expect(screen.queryByRole('button')).not.toBeInTheDocument();
  });

  it('should render button instead of anchor if href is not present on Tab', () => {
    render(
      <Tabs
        data-testid="tabs"
        onTabClicked={jest.fn()}
        selectedTabId="tab1"
        showBorder={false}>
        <Tabs.Tab id="tab1">tab1</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tab')).not.toHaveAttribute('href');
    expect(screen.getByRole('tab')).toHaveClass('comd-tabs__link-button');
  });

  it('should not show border if border is false', () => {
    render(
      <Tabs
        data-testid="tabs"
        onTabClicked={() => {}}
        selectedTabId="tab1"
        showBorder={false}>
        <Tabs.Tab id="tab1">tab1</Tabs.Tab>
      </Tabs>
    );

    expect(
      screen.getByTestId('tabs').getElementsByClassName('comd-tabs__hr')
    ).toHaveLength(0);
  });

  it('should show border if border is true ', () => {
    render(
      <Tabs
        data-testid="tabs"
        onTabClicked={() => {}}
        selectedTabId="tab1"
        showBorder={true}>
        <Tabs.Tab id="tab1">tab1</Tabs.Tab>
      </Tabs>
    );

    expect(
      screen.getByTestId('tabs').getElementsByClassName('comd-tabs__hr')
    ).toHaveLength(1);
  });

  it('should only render content of selected tab', () => {
    render(
      <Tabs data-testid="tabs" onTabClicked={() => {}} selectedTabId="tab1">
        <Tabs.Tab id="tab1">tab1</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2</Tabs.Tab>
      </Tabs>
    );
    expect(screen.getByTestId('tabs')).toHaveTextContent('tab1');
    expect(screen.getByTestId('tabs')).not.toHaveTextContent('tab1 content');
  });

  it('should render selected tab', () => {
    let selectedTabId = 'tab1';

    const onTabClicked = jest.fn((val) => (selectedTabId = val));

    render(
      <Tabs
        data-testid="tabs"
        onTabClicked={onTabClicked}
        selectedTabId={selectedTabId}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    const tab2 = screen.getByRole('tab', {selected: false});

    expect(screen.getByTestId('tabs')).toHaveTextContent('tab1 content');

    userEvent.click(tab2);

    expect(selectedTabId).toBe('tab2');
  });

  it('should add padding class modifier by default', () => {
    render(
      <Tabs data-testid="tabs" onTabClicked={jest.fn()}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
      </Tabs>
    );

    const tab = screen.getByText(/tab1 content/i);

    expect(
      tab.classList.contains('comd-tabs__tab-content--padding')
    ).toBeTruthy();
  });

  it('should not add padding class modifier when hasPadding is false', () => {
    render(
      <Tabs data-testid="tabs" onTabClicked={jest.fn()}>
        <Tabs.Tab hasPadding={false} id="tab1">
          tab1 content
        </Tabs.Tab>
      </Tabs>
    );

    const tab = screen.getByRole('tabpanel');

    expect(
      tab.classList.contains('comd-tabs__tab-content--padding')
    ).toBeFalsy();
  });

  it('should not render tab wrapper if there are no content', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId="tab2">
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab data-testid="halo" id="tab2" />
      </Tabs>
    );

    expect(screen.queryByRole('tabpanel')).not.toBeInTheDocument();
  });

  it('should render the first as selected if selectedTabId is not set', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tabpanel').textContent).toBe('tab1 content');
  });

  it('should render the first as selected if selectedTabId is set to null', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId={undefined}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tabpanel').textContent).toBe('tab1 content');
  });

  it('should render the first as selected if selectedTabId is set to undefined', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId={undefined}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tabpanel').textContent).toBe('tab1 content');
  });

  it('should render the first as selected if the set selectedTabId is empty string', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId="">
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tabpanel').textContent).toBe('tab1 content');
  });

  it('should render the first as selected if the set selectedTabId is an id that does not exist', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId="unknownTabId">
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
        <Tabs.Tab id="tab2">tab2 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByRole('tabpanel').textContent).toBe('tab1 content');
  });

  it('should not render header children when not defined', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs data-testid="tabs" onTabClicked={onTabClicked} selectedTabId="tab1">
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByTestId('tabs')).not.toHaveClass(
      '.comd-tabs__header-children'
    );
  });

  it('should render header children when defined', () => {
    render(
      <Tabs
        onTabClicked={() => {}}
        selectedTabId="tab1"
        headerChildren={() => <div>child</div>}>
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByText(/child/i)).toBeTruthy();
  });

  it('should render tab icon when defined', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId="tab1">
        <Tabs.Tab id="tab1" tabIcon={<Icon path={numeric} />}>
          tab1 content
        </Tabs.Tab>
      </Tabs>
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });
  it('should not render tab icon when not defined', () => {
    const onTabClicked = jest.fn();

    render(
      <Tabs onTabClicked={onTabClicked} selectedTabId="tab1">
        <Tabs.Tab id="tab1">tab1 content</Tabs.Tab>
      </Tabs>
    );

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
  });
});
