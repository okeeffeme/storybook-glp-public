import React, {
  forwardRef,
  FC,
  MouseEventHandler,
  MutableRefObject,
  ReactNode,
  useState,
} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {IconButton} from '@jotunheim/react-button';
import {Dropdown} from '@jotunheim/react-dropdown';
import Icon, {dotsVertical} from '@jotunheim/react-icons';
import SimpleLink from '@jotunheim/react-simple-link';

import {TabId} from './types';

import classNames from './tabs.module.css';

interface Option {
  command: 'edit' | 'delete';
  iconPath?: string;
  name: string;
}

interface TabButtonProps {
  tabId?: TabId;
  selectedTabId?: TabId;
  selectedTab: MutableRefObject<HTMLLIElement | null>;
  onTabClicked?: (tabId: TabId) => void;
  href?: string;
  tabIcon?: ReactNode;
  tabChildren?: ReactNode;
  className?: string;
  tabClass?: string;
  activeTabClass?: string;
  disabledTabClass?: string;
  disabled?: boolean;
  title?: ReactNode;
  optionsMenuItems?: Option;
  onOptionSelect?: (command: 'edit' | 'delete', tabId: TabId) => void;
}

interface OptionsMenuButtonProps {
  className?: string;
  onMouseEnter?: MouseEventHandler;
  onMouseLeave?: MouseEventHandler;
}

const {element} = bemFactory({baseClassName: 'comd-tabs', classNames});

const OptionsMenuButton = forwardRef<HTMLDivElement, OptionsMenuButtonProps>(
  function OptionsMenuButton(props, ref) {
    return (
      <div ref={ref} className={element('options-menu-button')}>
        <IconButton>
          <Icon path={dotsVertical} />
        </IconButton>
      </div>
    );
  }
);

export const TabButton: FC<TabButtonProps> = ({
  tabId,
  disabled = false,
  selectedTabId,
  selectedTab,
  onTabClicked,
  title = null,
  tabChildren = null,
  optionsMenuItems = [],
  onOptionSelect,
  tabClass,
  tabIcon,
  activeTabClass,
  disabledTabClass,
  href,
}) => {
  const [isHovered, setIsHovered] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const isActive = selectedTabId === tabId;
  const isLink = !!href;
  const ElementType = isLink ? SimpleLink : 'button';

  const renderDropdown = () => {
    if (!Array.isArray(optionsMenuItems) || !optionsMenuItems.length) {
      return null;
    }

    if (tabId === selectedTabId || isHovered)
      return (
        <Dropdown
          placement="bottom-start"
          open={isMenuOpen}
          menu={
            <Dropdown.Menu>
              {optionsMenuItems.map((item: Option, i) => {
                return (
                  <Dropdown.MenuItem
                    onClick={() =>
                      onOptionSelect &&
                      tabId &&
                      onOptionSelect(item.command, tabId)
                    }
                    iconPath={item.iconPath}
                    key={i}
                    data-menu-item={item.command}>
                    <div className={cn(element('menu-item'))}>
                      <div className={element('menu-item-text')}>
                        {item.name}
                      </div>
                    </div>
                  </Dropdown.MenuItem>
                );
              })}
            </Dropdown.Menu>
          }
          onToggle={(isOpen) => {
            setIsMenuOpen(isOpen);

            if (!isOpen) {
              setIsHovered(false); // necessary when menu is closed on a hovered tab
            }
          }}>
          <OptionsMenuButton />
        </Dropdown>
      );
    return <div className={element('dropdown-placeholder')} />;
  };

  return (
    <li
      role="presentation"
      ref={(x) => {
        if (isActive) {
          selectedTab.current = x;
        }
      }}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => {
        if (!isMenuOpen) {
          setIsHovered(false); // necessary when menu is opened while hovering over tab
        }
      }}
      className={cn(element('header-tab'), tabClass, {
        [element('header-tab', 'active')]: isActive,
        [element('header-tab', 'disabled')]: disabled,
        [`${activeTabClass}`]: isActive,
        [`${disabledTabClass}`]: disabled,
      })}>
      <ElementType
        role="tab"
        rel={isLink ? 'noopener noreferrer' : undefined}
        href={href}
        disabled={disabled}
        aria-selected={selectedTabId === tabId}
        id={`${tabId}_tab`}
        aria-controls={tabId?.toString()}
        data-tab-id={tabId}
        onClick={() => onTabClicked && tabId && onTabClicked(tabId)}
        className={element('link-button')}>
        {tabIcon && (
          <div className={element('tab-icon-wrapper')}>{tabIcon}</div>
        )}
        {title}
      </ElementType>
      {tabChildren && (
        <div className={element('header-tab-children')}>{tabChildren}</div>
      )}
      {renderDropdown()}
    </li>
  );
};

export default TabButton;
