import React, {FC, PropsWithChildren, ReactNode} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {TabId} from './types';

import classNames from './tabs.module.css';

const {element} = bemFactory({baseClassName: 'comd-tabs', classNames});

interface Option {
  command: string;
  iconPath?: string;
  name: string;
}

export interface TabProps {
  id: TabId;
  href?: string;
  title?: string;
  tabIcon?: ReactNode;
  tabChildren?: ReactNode;
  className?: string;
  disabled?: boolean;
  hasPadding?: boolean;
  onOptionSelect?: (command: string, tabId: TabId) => void;
  optionsMenuItems?: Option[];
}

/* istanbul ignore next */
const Tab: FC<PropsWithChildren<TabProps>> = ({
  children,
  className,
  id,
  hasPadding = true,
  ...rest
}) => {
  return (
    <div
      role="tabpanel"
      aria-labelledby={`${id}_tab`}
      className={cn(element('tab-content'), className, {
        [element('tab-content', 'padding')]: hasPadding,
      })}
      id={id.toString()}
      {...extractDataAndAriaProps(rest)}>
      {children}
    </div>
  );
};

export default Tab;
