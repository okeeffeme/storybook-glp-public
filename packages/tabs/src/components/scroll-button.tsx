import React, {FC, MouseEventHandler} from 'react';
import cn from 'classnames';

import {IconButton} from '@jotunheim/react-button';
import Icon, {chevronRight} from '@jotunheim/react-icons';

interface ScrollButtonProps {
  element: (el: string, direction?: string) => string;
  onClick: MouseEventHandler;
  direction: string;
}

export const ScrollButton: FC<ScrollButtonProps> = ({
  element,
  direction,
  onClick,
}) => (
  <div
    data-scroll-button-container
    className={cn(
      element('scroll-button-container'),
      element('scroll-button-container', direction)
    )}>
    <IconButton onClick={onClick}>
      <Icon path={chevronRight} />
    </IconButton>
  </div>
);

export default ScrollButton;
