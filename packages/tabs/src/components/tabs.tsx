import React, {
  useEffect,
  useRef,
  useState,
  Children,
  FC,
  PropsWithChildren,
  ReactElement,
  ReactNode,
  MouseEventHandler,
} from 'react';
import cn from 'classnames';

import {
  extractDataAriaIdProps,
  hasVisibleChildren,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import Tab, {TabProps} from './tab';
import TabButton from './tab-button';
import ScrollButton from './scroll-button';

import {TabId} from './types';

import classNames from './tabs.module.css';

const {block, element} = bemFactory({baseClassName: 'comd-tabs', classNames});

interface TabsProps {
  onTabClicked: (id: string) => void;
  onChange?: (id: string) => void;

  selectedTabId?: TabId;
  tabContentClass?: string;
  tabNavContainerClass?: string;
  tabNavClass?: string;
  tabClass?: string;
  activeTabClass?: string;
  disabledTabClass?: string;
  headerChildren?: (selectedTabId?: TabId) => ReactNode;
  showBorder?: boolean;
}

export const Tabs: FC<PropsWithChildren<TabsProps>> & {Tab: FC<TabProps>} = ({
  children,
  tabNavContainerClass,
  tabNavClass,
  tabContentClass,
  headerChildren = undefined,
  onTabClicked,
  showBorder = true,
  tabClass,
  selectedTabId = '',
  activeTabClass,
  disabledTabClass,
  ...rest
}) => {
  const [showScrollLeft, setShowScrollLeft] = useState(false);
  const [showScrollRight, setShowScrollRight] = useState(false);

  const firstTabRef = useRef<HTMLDivElement | null>(null);
  const lastTabRef = useRef<HTMLDivElement | null>(null);
  const selectedTabRef = useRef<HTMLLIElement | null>(null);
  const tabsContainerRef = useRef<HTMLDivElement | null>(null);
  const tabScrollContainerRef = useRef<HTMLDivElement | null>(null);
  const prevSelectedTabIdRef = useRef<TabId | undefined>(undefined);

  const _children = Children.toArray(children) as ReactElement[];

  if (!_children.find((child) => child.props.id === selectedTabId)) {
    selectedTabId = _children[0] && _children[0].props.id;
  }

  const getScrollDistance = () => {
    // To avoid scrolling past any items, only scroll 1/3 of the container width
    return tabScrollContainerRef.current?.clientWidth ?? 0 / 3;
  };

  const handleScrollLeftClick: MouseEventHandler = (e) => {
    e.preventDefault();

    if (!tabScrollContainerRef.current) {
      return;
    }

    tabScrollContainerRef.current.scrollLeft -= getScrollDistance();
  };

  const handleScrollRightClick: MouseEventHandler = (e) => {
    e.preventDefault();

    if (!tabScrollContainerRef.current) {
      return;
    }

    tabScrollContainerRef.current.scrollLeft += getScrollDistance();
  };

  const scrollToCurrentTab = () => {
    if (
      !tabsContainerRef.current ||
      !tabScrollContainerRef.current ||
      !selectedTabRef.current
    ) {
      return;
    }

    const scrollButton = tabsContainerRef.current.querySelector(
      '[data-scroll-button-container]'
    );

    if (!scrollButton) {
      return;
    }

    // subtract width of scroll button to avoid start of tab being hidden below scroll button
    tabScrollContainerRef.current.scrollLeft =
      selectedTabRef.current.offsetLeft - scrollButton.clientWidth;
  };

  useEffect(() => {
    if (window.IntersectionObserver) {
      const onIntersection = (entries) => {
        entries.forEach((entry) => {
          switch (entry.target) {
            case firstTabRef.current:
              setShowScrollLeft(!entry.isIntersecting);
              break;

            case lastTabRef.current:
              setShowScrollRight(!entry.isIntersecting);
              break;

            case selectedTabRef.current:
              if (
                !entry.isIntersecting &&
                prevSelectedTabIdRef.current !== selectedTabId
              ) {
                // only auto-scroll if selectedTabId changed, otherwise you cannot use the left/right
                // buttons to scroll manually
                scrollToCurrentTab();
              }
              break;
          }
        });
        prevSelectedTabIdRef.current = selectedTabId;
      };

      const intersectionObserver = new IntersectionObserver(onIntersection, {
        root: tabScrollContainerRef.current,
      });

      if (
        !firstTabRef.current ||
        !lastTabRef.current ||
        !selectedTabRef.current
      ) {
        return;
      }

      intersectionObserver.observe(firstTabRef.current);
      intersectionObserver.observe(lastTabRef.current);
      intersectionObserver.observe(selectedTabRef.current);

      return () => intersectionObserver.disconnect();
    }
  }, [selectedTabId]);

  const selectedTabContent = _children.find(
    (child) => selectedTabId === child.props.id
  );

  const selectedTabHasChildren = hasVisibleChildren(
    selectedTabContent?.props.children
  );

  return (
    <div
      className={block()}
      data-testid="tabs"
      {...extractDataAriaIdProps(rest)}>
      <div className={cn(element('header'), tabNavContainerClass)}>
        <div className={element('tabs-container')} ref={tabsContainerRef}>
          {showScrollLeft && (
            <ScrollButton
              element={element}
              direction="left"
              onClick={handleScrollLeftClick}
            />
          )}
          <div
            className={cn(element('header-scroll-container'), {
              [element('header-scroll-container--no-intersectionobserver')]:
                !window.IntersectionObserver,
            })}
            ref={tabScrollContainerRef}>
            <div
              className={cn(element('io'), element('io', 'first'))}
              ref={firstTabRef}
            />
            <ul className={cn(element('links'), tabNavClass)} role="tablist">
              {_children.map((child, i) => {
                const {id, ...childRestProps} = child.props;
                return (
                  <TabButton
                    key={i}
                    tabId={id}
                    onTabClicked={onTabClicked}
                    selectedTabId={selectedTabId}
                    selectedTab={selectedTabRef}
                    tabClass={tabClass}
                    activeTabClass={activeTabClass}
                    disabledTabClass={disabledTabClass}
                    {...childRestProps}
                  />
                );
              })}
            </ul>
            <div
              className={cn(element('io'), element('io', 'last'))}
              ref={lastTabRef}
            />
          </div>
          {showScrollRight && (
            <ScrollButton
              element={element}
              direction="right"
              onClick={handleScrollRightClick}
            />
          )}
        </div>
        {headerChildren && (
          <div className={element('header-children')}>
            {headerChildren(selectedTabId)}
          </div>
        )}
      </div>
      {showBorder && <div className={element('hr')} />}
      {selectedTabHasChildren && (
        <div className={cn(element('tab-container'), tabContentClass)}>
          {selectedTabContent}
        </div>
      )}
    </div>
  );
};

Tabs.Tab = Tab;

export default Tabs;
