import Tabs from './components/tabs';
import Tab from './components/tab';

export type {TabId} from './components/types';

export {Tab};
export default Tabs;
