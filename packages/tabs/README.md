# Jotunheim React Tabs

Jotunheim React tab control

You should load a polyfill for IntersectionObserver (f.ex the [w3c polyfill](https://github.com/w3c/IntersectionObserver/tree/master/polyfill)) for this to work best. If IntersectionObserver is not supported (or polyfilled), a horizontal scrollbar will be shown if there is not enough space to show all the tabs.
This is however _not_ listed as a peerDependency, since it is stricly not required (you might want to use a different polyfill/install it in other ways.)

## Example

### v6.1 ->

```jsx
<Tabs
  selectedTabId={selectedTab}
  onTabClicked={clickedTabId => handleTabClicked({value: clickedTabId})}}>
  <Tabs.Tab
    title="First tab title"
    id="tab1"

    // below are optional
    optionsMenuItems=[{command: 'command1', name: 'Command 1', icon: <SomeIcon />}]
    onOptionSelect={(command, tabId) => handleSelect(command, tabId)}>
    Content of tab 1
  </Tabs.Tab>
</Tabs>
```

### v3 ->

```jsx
<Tabs
  selectedTabId={selectedTab}
  onTabClicked={clickedTabId => handleTabClicked({value: clickedTabId})}}>
  <Tabs.Tab title="First tab title" id="tab1">
    Content of tab 1
  </Tabs.Tab>
  <Tabs.Tab title="Second tab title" id="tab2">
    Content of tab 2
  </Tabs.Tab>
  <Tabs.Tab title="Third tab title" id="tab3">
    Content of tab 3
  </Tabs.Tab>
</Tabs>
```

### pre-v3

```jsx
<div>
  <Tabs
    activeTab={selectedTab}
    onChange={({name}) => handleTabClicked({value: name})}>
    <Tabs.Item name="tab1">First tab title</Tabs.Item>
    <Tabs.Item name="tab2">Second tab title</Tabs.Item>
    <Tabs.Item name="tab3">Third tab title</Tabs.Item>
  </Tabs>
  {selectedTab === 'tab1' && <div>Content of tab 1</div>}
  {selectedTab === 'tab2' && <div>Content of tab 2</div>}
  {selectedTab === 'tab3' && <div>Content of tab 3</div>}
</div>
```
