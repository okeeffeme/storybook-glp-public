# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.26&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.26&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.24&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.25&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.23&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.24&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.22&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.23&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.21&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.22&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.20&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.21&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.19&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.20&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([41a6033](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/41a60333121d7d46e86098c72582ab5112e6c07b))

## [4.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.18&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.19&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.17&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.18&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.16&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.17&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.15&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.16&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.14&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.15&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.12&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.14&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.12&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.13&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.11&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.12&targetRepoId=1246) (2023-01-11)

### Bug Fixes

- adding data-testid Carousel ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([88d35ea](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/88d35ead37c4c0c81249dccb00211bc440f0c4fc))

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.10&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.11&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.9&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.10&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.7&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.9&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.7&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.4&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.4&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.4&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.3&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.2&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-carousel

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@4.0.0&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-carousel

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.11&sourceBranch=refs/tags/@jotunheim/react-carousel@4.0.0&targetRepoId=1246) (2022-10-11)

### Features

- convert Carousel to TypeScript ([NPM-1093](https://jiraosl.firmglobal.com/browse/NPM-1093)) ([d8a8022](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d8a802299bf1ba5b01ee97bc26b8cfd4934c5e44))
- remove className prop of Carousel ([NPM-930](https://jiraosl.firmglobal.com/browse/NPM-930)) ([5e5efc7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5e5efc7d832bc0d751494823f32a438072956507))
- remove default theme from Carousel ([NPM-1066](https://jiraosl.firmglobal.com/browse/NPM-1066)) ([38be9dd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/38be9ddc46df07ecb6c8f91646d257374f1a5a56))

### BREAKING CHANGES

- - onActiveSlideShown, defaultActiveIndex properties are removed

* Carousel is controllable component now, use activeIndex and onActiveIndexChanged props to control the component

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [3.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.10&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-carousel

## [3.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.9&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-carousel

## [3.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.8&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.9&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [3.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.7&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.8&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [3.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.5&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.6&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-carousel

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.2&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.3&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-carousel

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.1&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.2&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-carousel

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-carousel@3.0.0&sourceBranch=refs/tags/@jotunheim/react-carousel@3.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-carousel

## 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.0.92](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.91&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.92&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.91](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.90&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.91&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.90](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.89&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.90&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.0.89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.88&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.89&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.87&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.88&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.86&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.87&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.86](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.85&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.86&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.84&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.85&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.83&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.84&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.81&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.82&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.80&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.81&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.79&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.80&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.78&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.79&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.77&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.78&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.76&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.77&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.75&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.76&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.73&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.74&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.72&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.73&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.71&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.72&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.70&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.71&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.69&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.70&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.68&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.69&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.67&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.68&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.66&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.67&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.65&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.66&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.64&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.65&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.63&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.64&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.62&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.63&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.61&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.62&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.60&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.61&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.59&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.60&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.58&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.59&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.56&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.57&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.55&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.56&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.54&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.55&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.53&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.54&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.52&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.53&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.51&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.52&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.50&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.51&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.49&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.50&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.48&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.49&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.47&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.48&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.46&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.47&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.45&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.46&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.44&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.45&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.43&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.44&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.42&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.43&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.41&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.42&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.40&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.41&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.38&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.39&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.35&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.36&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.34&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.35&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.33&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.34&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.31&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.32&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.28&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.29&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.27&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.28&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.26&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.27&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.23&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.24&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-carousel@2.0.21&sourceBranch=refs/tags/@confirmit/react-carousel@2.0.22&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-carousel@2.0.17...@confirmit/react-carousel@2.0.18) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.13](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-carousel@2.0.12...@confirmit/react-carousel@2.0.13) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-carousel

## [2.0.12](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-carousel@2.0.11...@confirmit/react-carousel@2.0.12) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-carousel

## CHANGELOG

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- **BREAKING**: Removed props `arrowIcon`
- Refactor: restructure folder names in repository.
- Fix: use color variable for pager dots
- Refactor: use new icons for left/right buttons

### v1.1.0

- Removing package version in class names.

### v1.0.7

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.1

- Initial version
