import React, {AriaAttributes, ReactNode, useCallback, useEffect} from 'react';
import cn from 'classnames';

import Icon, {chevronRight, chevronLeft} from '@jotunheim/react-icons';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {getNextIndex, getPreviousIndex} from '../utils/utils';

import classNames from './Carousel.module.css';

const {block, element} = bemFactory('comd-carousel', classNames);

const LEFT_ARROW = 37;
const RIGHT_ARROW = 39;

export function Carousel({
  showNavigation = true,
  showPagination = true,
  activeIndex = 0,
  onActiveIndexChanged,
  children,
  keyboard = true,
  ...rest
}: CarouselProps) {
  const childrenArray = React.Children.toArray(children);
  const childrenCount = childrenArray.length;

  const next = useCallback(() => {
    const nextActiveIndex = getNextIndex(activeIndex, childrenCount);
    onActiveIndexChanged?.(nextActiveIndex);
  }, [activeIndex, childrenCount, onActiveIndexChanged]);

  const prev = useCallback(() => {
    const nextActiveIndex = getPreviousIndex(activeIndex, childrenCount);
    onActiveIndexChanged?.(nextActiveIndex);
  }, [activeIndex, childrenCount, onActiveIndexChanged]);

  const goTo = (activeIndex) => {
    if (activeIndex >= 0 && activeIndex < childrenCount) {
      onActiveIndexChanged?.(activeIndex);
    }
  };

  /* istanbul ignore next */
  const handleKeyUp = useCallback(
    (e) => {
      if (keyboard) {
        e.keyCode === LEFT_ARROW && prev();
        e.keyCode === RIGHT_ARROW && next();
      }
    },
    [keyboard, prev, next]
  );

  useEffect(() => {
    /* istanbul ignore else */
    if (keyboard) {
      document.addEventListener('keyup', handleKeyUp);

      return () => {
        document.removeEventListener('keyup', handleKeyUp);
      };
    }
  }, [keyboard, handleKeyUp]);

  const slidesCount = childrenCount > 0 ? childrenCount : 1;

  const classesNavigationPrev = cn(
    element('nav-arrow'),
    element('nav-arrow', 'prev')
  );
  const classesNavigationNext = cn(
    element('nav-arrow'),
    element('nav-arrow', 'next')
  );

  const pages = Object.keys(childrenArray).map(Number);

  return (
    <div
      className={cn(block())}
      data-testid="carousel"
      {...extractDataAndAriaProps(rest)}>
      <div
        className={element('slides')}
        data-testid="carousel-slides"
        style={{
          marginLeft: `${activeIndex * -100}%`,
          width: `${slidesCount * 100}%`,
        }}>
        {childrenArray.map((child, index) => (
          <div
            key={index}
            className={element('slide')}
            style={{
              position: 'relative',
              width: `${100 / childrenArray.length}%`,
            }}>
            <div className={element('slide-content')}>{child}</div>
          </div>
        ))}
      </div>

      {showNavigation && slidesCount > 1 && (
        <div>
          <a
            className={classesNavigationPrev}
            onClick={prev}
            data-testid="iconnav">
            <Icon path={chevronLeft} />
          </a>
          <a
            className={classesNavigationNext}
            onClick={next}
            data-testid="iconnav">
            <Icon path={chevronRight} />
          </a>
        </div>
      )}

      {showPagination && slidesCount > 1 && (
        <div
          className={element('pagination')}
          data-testid="carousel-pagination">
          {pages.map((page) => (
            <div
              key={page}
              data-testid="carousel-page"
              className={cn(element('page'), {
                [element('page', 'active')]: page === activeIndex,
              })}
              onClick={() => {
                goTo(page);
              }}
            />
          ))}
        </div>
      )}
    </div>
  );
}

type CarouselProps = {
  children?: ReactNode;
  showNavigation?: boolean;
  showPagination?: boolean;
  keyboard?: boolean;
  activeIndex?: number;
  onActiveIndexChanged?: (activeIndex: number) => void;
} & AriaAttributes;
