export const getNextIndex = (activeIndex: number, numberOfSlides: number) => {
  return activeIndex < numberOfSlides - 1 ? activeIndex + 1 : 0;
};

export const getPreviousIndex = (
  activeIndex: number,
  numberOfSlides: number
) => {
  return activeIndex > 0 ? activeIndex - 1 : numberOfSlides - 1;
};
