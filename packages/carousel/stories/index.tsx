import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Carousel} from '../src/index';

storiesOf('Components/Carousel', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('carousel 3 slides', () => {
    const [activeIndex, setActiveIndex] = useState(0);

    return (
      <Carousel activeIndex={activeIndex} onActiveIndexChanged={setActiveIndex}>
        <div>
          <u>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </u>{' '}
          It has survived not only five centuries, but also the leap into
          electronic typesetting, remaining essentially unchanged. It was
          popularised in the 1960s with the release of Letraset sheets
          containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem
          Ipsum.
        </div>
        <div>
          <b>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock
          </b>
          , a Latin professor at Hampden-Sydney College in Virginia, looked up
          one of the more obscure Latin words, consectetur, from a Lorem Ipsum
          passage, and going through the cites of the word in classical
          literature, discovered the undoubtable source. Lorem Ipsum comes from
          sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
          Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a
          treatise on the theory of ethics, very popular during the Renaissance.
          The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes
          from a line in section 1.10.32.
        </div>
        <div>
          There are many variations of passages of Lorem Ipsum available, but
          the majority have suffered alteration in some form, by injected
          humour, or randomised words which don't look even slightly believable.
          If you are going to use a passage of Lorem Ipsum, you need to be sure
          there isn't anything embarrassing hidden in the middle of text. All
          the Lorem Ipsum generators on the Internet tend to repeat predefined
          chunks as necessary, making this the first true generator on the
          Internet. It uses a dictionary of over 200 Latin words, combined with
          a handful of model sentence structures, to generate Lorem Ipsum which
          looks reasonable. The generated Lorem Ipsum is therefore always free
          from repetition, injected humour, or non-characteristic words etc.
        </div>
      </Carousel>
    );
  })
  .add('carousel 3 slides, second slide active', () => {
    const [activeIndex, setActiveIndex] = useState(1);

    return (
      <Carousel activeIndex={activeIndex} onActiveIndexChanged={setActiveIndex}>
        <div>
          <u>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </u>{' '}
          It has survived not only five centuries, but also the leap into
          electronic typesetting, remaining essentially unchanged. It was
          popularised in the 1960s with the release of Letraset sheets
          containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem
          Ipsum.
        </div>
        <div>
          <b>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock
          </b>
          , a Latin professor at Hampden-Sydney College in Virginia, looked up
          one of the more obscure Latin words, consectetur, from a Lorem Ipsum
          passage, and going through the cites of the word in classical
          literature, discovered the undoubtable source. Lorem Ipsum comes from
          sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
          Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a
          treatise on the theory of ethics, very popular during the Renaissance.
          The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes
          from a line in section 1.10.32.
        </div>
        <div>
          There are many variations of passages of Lorem Ipsum available, but
          the majority have suffered alteration in some form, by injected
          humour, or randomised words which don't look even slightly believable.
          If you are going to use a passage of Lorem Ipsum, you need to be sure
          there isn't anything embarrassing hidden in the middle of text. All
          the Lorem Ipsum generators on the Internet tend to repeat predefined
          chunks as necessary, making this the first true generator on the
          Internet. It uses a dictionary of over 200 Latin words, combined with
          a handful of model sentence structures, to generate Lorem Ipsum which
          looks reasonable. The generated Lorem Ipsum is therefore always free
          from repetition, injected humour, or non-characteristic words etc.
        </div>
      </Carousel>
    );
  })
  .add('carousel 3 slides, with much content', () => {
    const [activeIndex, setActiveIndex] = useState(0);

    return (
      <Carousel activeIndex={activeIndex} onActiveIndexChanged={setActiveIndex}>
        <div>
          <u>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </u>{' '}
          It has survived not only five centuries, but also the leap into
          electronic typesetting, remaining essentially unchanged. It was
          popularised in the 1960s with the release of Letraset sheets
          containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem
          Ipsum.
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
        </div>
        <div>
          <b>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock
          </b>
          , a Latin professor at Hampden-Sydney College in Virginia, looked up
          one of the more obscure Latin words, consectetur, from a Lorem Ipsum
          passage, and going through the cites of the word in classical
          literature, discovered the undoubtable source. Lorem Ipsum comes from
          sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
          Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a
          treatise on the theory of ethics, very popular during the Renaissance.
          The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes
          from a line in section 1.10.32.
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Consequuntur laboriosam voluptate quaerat culpa natus, nam earum cum
            voluptatum, ipsum rerum labore. Aliquid eaque voluptatem quaerat
            aperiam doloremque corrupti modi a!
          </p>
          <img alt="" src="https://placekitten.com/2560/1440/" />
        </div>
        <div>
          There are many variations of passages of Lorem Ipsum available, but
          the majority have suffered alteration in some form, by injected
          humour, or randomised words which don't look even slightly believable.
          If you are going to use a passage of Lorem Ipsum, you need to be sure
          there isn't anything embarrassing hidden in the middle of text. All
          the Lorem Ipsum generators on the Internet tend to repeat predefined
          chunks as necessary, making this the first true generator on the
          Internet. It uses a dictionary of over 200 Latin words, combined with
          a handful of model sentence structures, to generate Lorem Ipsum which
          looks reasonable. The generated Lorem Ipsum is therefore always free
          from repetition, injected humour, or non-characteristic words etc.
        </div>
      </Carousel>
    );
  })
  .add('carousel 1 slide', () => (
    <Carousel>
      <div>
        <p>
          <b>
            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax
            quiz prog.
          </b>{' '}
          Junk MTV quiz graced by fox whelps.
        </p>
        <p>
          Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs
          vex! <u>Fox nymphs grab quick-jived waltz.</u>
        </p>
        <p>
          Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl
          quack. Quick wafting zephyrs vex bold Jim.
        </p>
      </div>
    </Carousel>
  ));
