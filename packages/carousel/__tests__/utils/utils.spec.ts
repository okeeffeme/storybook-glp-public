import {getNextIndex, getPreviousIndex} from '../../src/utils/utils';

describe('Carousel :: utils', () => {
  it('should get correct values from getNextIndex', () => {
    const numberOfSlides = 3;
    let activeIndex = 0;
    expect(getNextIndex(activeIndex, numberOfSlides)).toEqual(1);

    activeIndex = 1;
    expect(getNextIndex(activeIndex, numberOfSlides)).toEqual(2);

    activeIndex = 2;
    expect(getNextIndex(activeIndex, numberOfSlides)).toEqual(0);
  });

  it('should get correct values from getPreviousIndex', () => {
    const numberOfSlides = 3;
    let activeIndex = 0;
    expect(getPreviousIndex(activeIndex, numberOfSlides)).toEqual(2);

    activeIndex = 1;
    expect(getPreviousIndex(activeIndex, numberOfSlides)).toEqual(0);

    activeIndex = 2;
    expect(getPreviousIndex(activeIndex, numberOfSlides)).toEqual(1);
  });
});
