import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {Carousel} from '../../src/index';

describe('Carousel', () => {
  it('should render with two slides', () => {
    render(
      <Carousel>
        <div>
          slide <b>1</b>
        </div>
        <div>slide 2</div>
      </Carousel>
    );

    expect(screen.getAllByText(/slide/i).length).toBe(2);
  });

  it('should render with one slide', () => {
    render(
      <Carousel>
        <div>
          slide <b>1</b>
        </div>
      </Carousel>
    );

    expect(screen.getAllByText(/slide/i).length).toBe(1);
  });

  it('should call onActiveSlideShown with 1 index when next is clicked', (done) => {
    const itemChanged = jest.fn((index) => {
      if (index == 1) {
        done();
      }
    });

    render(
      <Carousel data-testid="carousel" onActiveIndexChanged={itemChanged}>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );
    userEvent.click(screen.getAllByTestId('icon')[1]);

    expect(itemChanged).toHaveBeenCalledWith(1);
  });

  it('should call onActiveSlideShown with 2 index when next is clicked', (done) => {
    const itemChanged = jest.fn((index) => {
      if (index == 2) {
        done();
      }
    });

    render(
      <Carousel activeIndex={1} onActiveIndexChanged={itemChanged}>
        <div>slide 1</div>
        <div>slide 2</div>
        <div>slide 3</div>
      </Carousel>
    );

    userEvent.click(screen.getAllByTestId('icon')[1]);

    expect(itemChanged).toHaveBeenCalledWith(2);
  });

  it('should call onActiveSlideShown with 1 index when second page is clicked ', (done) => {
    const itemChanged = jest.fn((index) => {
      if (index == 1) {
        done();
      }
    });

    render(
      <Carousel onActiveIndexChanged={itemChanged}>
        <div>slide 1</div>
        <div>slide 2</div>
        <div>slide 3</div>
      </Carousel>
    );
    userEvent.click(
      screen.getAllByTestId('carousel-page')[1] as HTMLDivElement
    );

    expect(itemChanged).toHaveBeenCalledWith(1);
  });

  it('should not render arrow navigation when only 1 slide', () => {
    render(
      <Carousel>
        <div>slide 1</div>
      </Carousel>
    );
    expect(screen.getByTestId('carousel')).not.toHaveClass(
      'comd-carousel__nav-arrow'
    );
  });

  it('should not render arrow navigation when showNavigation prop is false', () => {
    render(
      <Carousel showNavigation={false}>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );

    expect(screen.getByTestId('carousel')).not.toHaveClass(
      'comd-carousel__nav-arrow'
    );
  });

  it('should render arrow navigation when more than 1 slide', () => {
    render(
      <Carousel>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );
    expect(screen.queryAllByTestId('iconnav')).toHaveLength(2);
  });

  it('should not render pagination when pagination prop is false', () => {
    const {container} = render(
      <Carousel showPagination={false}>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );

    expect(
      container.querySelectorAll('.comd-carousel__pagination').length
    ).toBe(0);
  });

  it('should not render pagination when only 1 slide', () => {
    render(
      <Carousel>
        <div>slide 1</div>
      </Carousel>
    );
    expect(screen.queryByTestId('carousel-pagination')).not.toBeInTheDocument();
  });

  it('should render pagination when more than 1 slide', () => {
    render(
      <Carousel>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );
    expect(screen.getAllByTestId('carousel-pagination')).toHaveLength(1);
  });

  it('should show slide 1 when activeIndex is set to 1', () => {
    const onActiveIndexChanged = jest.fn();

    render(
      <Carousel onActiveIndexChanged={onActiveIndexChanged} activeIndex={1}>
        <div>slide 0</div>
        <div>slide 1</div>
        <div>slide 2</div>
      </Carousel>
    );
    expect(screen.getByTestId('carousel-slides')).toHaveStyle(
      'marginLeft: -100%; width: 300%'
    );
  });
});
