import React from 'react';
import MarkdownView from '../../src/components/markdown-view';
import {render, screen} from '@testing-library/react';
import toJson from 'enzyme-to-json';

const currentURL = 'https://www.confirmit.com/';

describe('<MarkdownView />', () => {
  describe('rendering tests.', () => {
    it('The component should render correctly #1', () => {
      render(
        <MarkdownView markdown="## Hello world" data-testid="markdown-view" />
      );
      expect(toJson(screen.getByTestId('markdown-view'))).toMatchSnapshot();
    });

    it('The component should render correctly #2', () => {
      render(
        <MarkdownView
          markdown={'<a class="dangerousHTML" href="#">Hello world</a>'}
          data-testid="markdown-view"
        />
      );
      expect(toJson(screen.getByTestId('markdown-view'))).toMatchSnapshot();
    });

    it('The component should render correctly #3', () => {
      render(
        <MarkdownView
          markdown={'<a class="dangerousHTML" href="#">Hello world</a>'}
          options={{sanitize: false}}
          data-testid="markdown-view"
        />
      );
      expect(toJson(screen.getByTestId('markdown-view'))).toMatchSnapshot();
    });

    it('The component should not render MarkdownView if no markdown property is passed', () => {
      render(<MarkdownView data-testid="markdown-view" />);
      expect(screen.queryByTestId('markdown-view')).not.toBeInTheDocument();
    });

    it('The component should render markdown passed as markdown property', () => {
      render(
        <MarkdownView data-testid="markdown-view" markdown="## Hello world" />
      );
      expect(screen.getByTestId('markdown-view')).toBeInTheDocument();
    });
    it('The component should render markdown passed as children as well', () => {
      render(<MarkdownView>## Hello world</MarkdownView>);
      expect(screen.getByText('Hello world')).toBeInTheDocument();
    });
  });

  describe('widget structure tests.', () => {
    it('The component should have `.comd-markdown-view` className of a wrapping div component', () => {
      render(
        <MarkdownView data-testid="markdown-view" markdown="## Hello world" />
      );
      expect(screen.getByTestId('markdown-view')).toHaveClass(
        'comd-markdown-view'
      );
    });

    it('The component should change wrapping component to another HTMLElement and have `.comd-markdown-view` className', () => {
      render(
        <MarkdownView
          data-testid="markdown-view"
          component="span"
          markdown="## Hello world"
        />
      );
      expect(screen.getByTestId('markdown-view')).toHaveClass(
        'comd-markdown-view'
      );
    });

    it('The component should create H1-6 tags which have an anchor with lower-case dash delimited link', () => {
      render(<MarkdownView markdown="## Hello world" />);
      expect(
        screen.getByRole('heading').querySelector('a')
      ).toBeInTheDocument();
      expect(screen.getByRole('link')).toBeInTheDocument();
      expect(
        screen.getByRole('heading').querySelector('a').querySelector('span')
      ).toBeInTheDocument();
      expect(screen.getByRole('heading')).toHaveTextContent('Hello world');
    });

    it('The component should create a link tag without target attribute receiving absolute path', () => {
      render(
        <MarkdownView
          markdown={'[Hello world](' + currentURL + '/blob/master/LICENSE)'}
          currentURL={currentURL}
          externalLinksTarget={'_blank'}
        />
      );
      expect(screen.getByRole('link')).toBeInTheDocument();
      expect(screen.getByRole('link')).not.toHaveAttribute('target');
    });

    it('The component should create a link tag without target attribute receiving relative path', () => {
      render(
        <MarkdownView
          markdown="[Hello world](Products)"
          currentURL={currentURL}
          externalLinksTarget={'_blank'}
        />
      );
      expect(screen.getByRole('link')).toBeInTheDocument();
      expect(screen.getByRole('link')).not.toHaveAttribute('target');
    });

    it('The component should create a link tag with target attribute receiving link to an external document', () => {
      render(
        <MarkdownView
          markdown="[Hello world](https://www.google.com)"
          currentURL={currentURL}
          externalLinksTarget={'_blank'}
        />
      );
      expect(screen.getByRole('link')).toBeInTheDocument();
      expect(screen.getByRole('link')).toHaveAttribute('target', '_blank');
    });
  });

  describe('sanitizing tests.', () => {
    it('The component should disable sanitizing via options', () => {
      render(
        <MarkdownView
          data-testid="markdown-view"
          markdown={'<a class="dangerousHTML" href="#">Hello world</a>'}
          options={{sanitize: false}}
        />
      );
      expect(screen.getByTestId('markdown-view')).toHaveClass(
        'comd-markdown-view'
      );
      expect(screen.getByRole('link')).toHaveClass('dangerousHTML');
      expect(screen.getByRole('link')).toHaveTextContent('Hello world');
    });

    it('The component should enable sanitizing via options', () => {
      render(
        <MarkdownView
          data-testid="markdown-view"
          markdown={'<a class="dangerousHTML" href="#">Hello world</a>'}
          options={{sanitize: true}}
        />
      );
      expect(screen.queryByRole('link')).not.toBeInTheDocument();
    });
  });
});
