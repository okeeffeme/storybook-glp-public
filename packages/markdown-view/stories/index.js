import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import MarkdownView from '../src/components/markdown-view';

storiesOf('Components/markdown-view', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Basic usage', () => <MarkdownView>{markdownExample}</MarkdownView>)
  .add('Rendering images', () => (
    <div>
      <MarkdownView
        onRender={() =>
          `<p>PNG _ with transparent background:</p>
          <img src="https://www.freeiconspng.com/uploads/red-spot-light-png-6.png" alt="transparent background image"/>
          <p>JPEG _ with solid background:</p>
          <img src="https://as2.ftcdn.net/v2/jpg/03/32/37/35/1000_F_332373584_9PFoBUo7T7n5tvFylxHPlmjo864X7536.jpg" alt="solid background image"/>`
        }></MarkdownView>
    </div>
  ))
  .add('Custom options sent to marked (e.g. disable tables)', () => (
    <MarkdownView options={{tables: false}}>{markdownExample}</MarkdownView>
  ))
  .add('onRender executed when markdown is parsed', () => (
    <MarkdownView
      onRender={(err, result) =>
        `<div style="border: 2px solid red"><h1>Add a red border and this text</h1>${result}</div>`
      }>
      {markdownExample}
    </MarkdownView>
  ));

const markdownExample = `Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

There must be at least 3 dashes separating each header cell.
The outer pipes (|) are optional, and you don't need to make the
raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | \\\`renders\\\` | **nicely**
1 | 2 | 3
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself].

URLs and URLs in angle brackets will automatically get turned into links.
http://www.example.com or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com`;
