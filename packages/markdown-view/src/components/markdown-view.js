import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import getMarked from './getMarked';

import {developerNotice, extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './markdown-view.module.css';

const {block} = bemFactory('comd-markdown-view', classNames);

const MarkdownView = ({
  children,
  markdown,
  onRender,
  component: Component,
  onClick,
  style,
  options,
  currentURL = window.location.origin,
  externalLinksTarget = '_blank',
  ...rest
}) => {
  developerNotice(
    'The MarkdownView component is under development, expect both API and visual changes when upgrading.'
  );

  const marked = useRef(getMarked({options, currentURL, externalLinksTarget}));
  const markdownString = children || markdown || '';
  const parsedMarkdown = marked.current(markdownString, onRender);

  return parsedMarkdown ? (
    <Component
      data-testid="markdown-view"
      onClick={onClick}
      style={style}
      className={cn(block())}
      dangerouslySetInnerHTML={{__html: parsedMarkdown}}
      {...extractDataAriaIdProps(rest)}
    />
  ) : null;
};

MarkdownView.propTypes = {
  /**
   * configuration for marked.js
   * */
  options: PropTypes.object,
  /**
   * function to be executed when markdown is parsed
   * */
  onRender: PropTypes.func,
  /**
   * markdown string to be parsed
   * */
  children: PropTypes.string,
  /**
   * markdown string to be parsed, can be used instead of `children`
   * */
  markdown: PropTypes.string,
  component: PropTypes.string,
  currentURL: PropTypes.string,
  externalLinksTarget: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
};

MarkdownView.defaultProps = {
  component: 'div',
  options: {},
};

export default MarkdownView;
