import {marked} from 'marked';

function enhanceHeadings(renderer) {
  renderer.heading = (text, level) => {
    const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
    return `<h${level}><a name="${escapedText}" data-test-anchor href="#${escapedText}"><span data-test-header-link /></a>${text}</h${level}>`;
  };

  return renderer;
}

function enhanceLinks(renderer, currentURL, externalLinksTarget) {
  renderer.link = function (href, title, text) {
    const link = marked.Renderer.prototype.link.call(this, href, title, text);
    if (link === text) return link;

    try {
      const dest = new URL(href, currentURL);
      const source = new URL(currentURL);

      if (dest.host !== source.host) {
        return link.replace(
          '<a',
          `<a rel="noreferrer noopener" target=${externalLinksTarget}`
        );
      }
    } catch {
      return link;
    }

    return link;
  };

  return renderer;
}

export default function getMarked({
  options = {},
  currentURL,
  externalLinksTarget,
}) {
  const defaultOptions = {
    renderer: enhanceHeadings(
      enhanceLinks(
        options.renderer || new marked.Renderer(),
        currentURL,
        externalLinksTarget
      )
    ),
    gfm: true,
    tables: true,
    breaks: true,
    pedantic: false,
    sanitize: true,
    smartLists: true,
    smartypants: false,
  };

  typeof options === 'object' &&
    marked.setOptions({...defaultOptions, ...options});

  return marked;
}
