# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@5.0.4&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.5&targetRepoId=1246) (2023-03-30)

### Bug Fixes

- Remove unwanted styles from markdown images ([74037e1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/74037e156d0c40bda57544c9bb664c5a2b5ef196))

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@5.0.3&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-markdown-view

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@5.0.2&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@5.0.1&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.2&targetRepoId=1246) (2023-02-13)

### Bug Fixes

- Use base CSS variables ([SREP-6182](https://jiraosl.firmglobal.com/browse/SREP-6182)) ([cfdfda8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cfdfda8569b61f2c51c5e9f6d07b72bff2737649))
- Use PrimaryText variable on Markdown Editor ([SREP-6182](https://jiraosl.firmglobal.com/browse/SREP-6182)) ([87a5ff8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/87a5ff851048e78d89fc68aee7c86b4895c0cc45))
- Use variables for markdown editor text color ([SREP-6182](https://jiraosl.firmglobal.com/browse/SREP-6182)) ([8aaaa51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8aaaa51ff67e12635419d6ccf54ec4e3ac2aaec9))

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@5.0.0&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-markdown-view

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@4.0.1&sourceBranch=refs/tags/@jotunheim/react-markdown-view@5.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of MarkdownView ([NPM-944](https://jiraosl.firmglobal.com/browse/NPM-944)) ([706375b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/706375b386a096b46cc30aa453ac44904d7caad5))
- remove default theme from MarkdownView ([NPM-1073](https://jiraosl.firmglobal.com/browse/NPM-1073)) ([956fd45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/956fd454930709edcc5726c4cadbc847a18cce49))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-markdown-view@4.0.0&sourceBranch=refs/tags/@jotunheim/react-markdown-view@4.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-markdown-view

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-view@3.1.0&sourceBranch=refs/tags/@confirmit/react-markdown-view@3.1.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-view@3.0.5&sourceBranch=refs/tags/@confirmit/react-markdown-view@3.1.0&targetRepoId=1246) (2022-04-08)

### Features

- Updating markdown-view to use latest version of marked, adding caret ([NPM-999](https://jiraosl.firmglobal.com/browse/NPM-999)) ([fc5bf7c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fc5bf7c93c82fd2446e8ba09275ddb95934af1b0))

## [3.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-view@3.0.4&sourceBranch=refs/tags/@confirmit/react-markdown-view@3.0.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-markdown-view

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-view@3.0.3&sourceBranch=refs/tags/@confirmit/react-markdown-view@3.0.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-markdown-view@3.0.2&sourceBranch=refs/tags/@confirmit/react-markdown-view@3.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-markdown-view

## [3.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-markdown-view@3.0.1...@confirmit/react-markdown-view@3.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-markdown-view

## CHANGELOG

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-markdown-view`

### v2.1.0

- Removing package version in class names.

### v2.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v1.0.0

- **BREAKING** - Removed props: baseClassName, classNames
- `MarkdownView` re-written to a functional component
- Feat: Add `currentURL` and `externalLinksTarget` props, with default values:
  - `currentURL = window.location.origin`
  - `externalLinksTarget = '_blank'`

### v0.3.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v0.2.0

- **BREAKING** (but will most likely not cause any issues): updated peer dependency `marked` to version 0.6.2

### v0.1.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v0.0.1

- Initial version
