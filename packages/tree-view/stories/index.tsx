import React, {useCallback} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {boolean, select} from '@storybook/addon-knobs';
import Icon, {plus, surveyIcon, trashBin} from '../../icons/src';

import TreeView, {
  TreeNode,
  NodeId,
  NodePosition,
  NodeSelectedState,
  NodeSelectionMode,
  NodeSelectorVisibility,
  SelectionTree,
  TreeDensity,
} from '../src';
import {DropInfo} from '../src/types';
import {Truncate} from '../../truncate/src';

type MyNode = TreeNode<{
  parentId?: NodeId;
  metadata?: {
    type: string;
  };
  hasMoreChildren?: boolean;
}>;

type State = {
  selectedNodeIds: NodeId[];
  nodesByParentId: Record<NodeId, MyNode[] | undefined>;
};

const duplicate = (nodes) => JSON.parse(JSON.stringify(nodes));

const renderNodeIcon = (node: MyNode) => {
  if (Array.isArray(node.children)) {
    return <Icon path={surveyIcon} />;
  }
  return null;
};

const renderNodeContents = (node: MyNode) => {
  return (
    <Truncate>
      <span>{node.label}</span>
      {node.metadata?.type && <span>&nbsp;{node.metadata?.type}</span>}
    </Truncate>
  );
};

const flatten = (nodes: MyNode[], flattenedNodes: MyNode[] = []) =>
  nodes.reduce((result, node) => {
    result.push(node);

    if (node.children) {
      flatten(node.children, result);
    }

    delete node.children;

    return result;
  }, flattenedNodes);

const initialNodes = [
  {
    id: '1',
    label: 'Node 1',
    metadata: {
      type: '(single)',
    },
  },
  {
    id: '2',
    label: 'Node 2',
    metadata: {
      type: '(grid)',
    },
  },
  {
    id: '3',
    label: 'Node 3',
    metadata: {
      type: '(multi)',
    },
  },
  {
    id: '4',
    label: 'Node 4',
    isExpanded: true,
    children: [
      {
        id: '4.1',
        label: 'Node 5',
      },
      {
        id: '4.2',
        label: 'Node 6',
        children: [
          {
            id: '4.2.1',
            label: 'Node 7',
          },
          {
            id: '4.2.2',
            label: 'Node 8',
          },
        ],
      },
    ],
  },
];

const toggleNodeSelection = (
  arr: NodeId[],
  node: MyNode,
  toggleOn: boolean
) => {
  const index = arr.indexOf(node.id);

  if (toggleOn && index === -1) {
    return [...arr, node.id];
  }

  if (!toggleOn && index > -1) {
    return [...arr.slice(0, index), ...arr.slice(index + 1)];
  }

  return arr;
};

type Action = {
  type: string;
  node: MyNode;
  toggleOn?: boolean;
};

function reducer(state: State, action: Action): State {
  const nodesByParentId = state.nodesByParentId;
  const {type, node} = action;

  switch (type) {
    case 'select': {
      const index = state.selectedNodeIds.indexOf(node.id);

      return {
        ...state,
        selectedNodeIds: toggleNodeSelection(
          state.selectedNodeIds,
          node,
          index === -1
        ),
      };
    }

    case 'select-with-children': {
      const {toggleOn = false} = action;
      let selection = toggleNodeSelection(
        state.selectedNodeIds,
        node,
        toggleOn
      );

      const toggleChildren = (children: MyNode[] = []) => {
        children.forEach((c) => {
          selection = toggleNodeSelection(selection, c, toggleOn);
          toggleChildren(state.nodesByParentId[c.id]);
        });
      };

      toggleChildren(state.nodesByParentId[node.id]);

      return {
        ...state,
        selectedNodeIds: selection,
      };
    }

    case 'toggle': {
      if (node.parentId) {
        const siblings = nodesByParentId[node.parentId] ?? [];
        const children = nodesByParentId[node.id];

        return {
          ...state,
          nodesByParentId: {
            ...nodesByParentId,
            [node.parentId]: siblings.map((n) => {
              if (n.id === node.id) {
                return {
                  ...n,
                  isExpanded: !n.isExpanded,
                };
              }

              return n;
            }),
            [node.id]: children ?? [
              {
                id: `${node.id}_1`,
                label: 'Child 1',
                parentId: node.id,
                metadata: {
                  type: '(multi)',
                },
              },
              {
                id: `${node.id}_2`,
                label: 'Child 2',
                parentId: node.id,
                hasMoreChildren: true,
                metadata: {
                  type: '(folder)',
                },
              },
              {
                id: `${node.id}_3`,
                label:
                  'Wordy Child - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ',
                parentId: node.id,
                metadata: {
                  type: '(multi)',
                },
              },
            ],
          },
        };
      }
    }
  }

  return state;
}

const isNodeExpandable = (node: MyNode) => !!node.children?.length;

const isNodeExpandableLazy = (node: MyNode) => node.hasMoreChildren ?? false;

storiesOf('Components/tree-view', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Toggle nodes', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(duplicate(initialNodes));

    const onToggleNode = React.useCallback(
      // Here we are asserting that this callback will receive a node of type MyNode.
      // The TreeView component therefore knows that the value of its nodes prop must be
      // of type MyNode. If those 2 types don't match, we'll get a type error.
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));

        // TypeScript knows that node.metadata?.type, if defined, is a string
        console.log(node.metadata?.type.toUpperCase());
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        isNodeExpandable={isNodeExpandable}
        onToggleNode={onToggleNode}
        renderNodeContents={renderNodeContents}
      />
    );
  })
  .add('Multi-select nodes', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(duplicate(initialNodes));
    const getNodeSelectionMode = (/*node*/) => NodeSelectionMode.multi;
    const [selectedNodeIds, setSelectedNodeIds] = React.useState<NodeId[]>([]);

    const onNodeSelectionToggled = React.useCallback(
      (node: MyNode) => {
        const index = selectedNodeIds.indexOf(node.id);

        if (index === -1) {
          setSelectedNodeIds(selectedNodeIds.concat([node.id]));
        } else {
          setSelectedNodeIds([
            ...selectedNodeIds.slice(0, index),
            ...selectedNodeIds.slice(index + 1),
          ]);
        }
      },
      [selectedNodeIds, setSelectedNodeIds]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        getNodeSelectionMode={getNodeSelectionMode}
        selectedNodeIds={selectedNodeIds}
        isNodeExpandable={isNodeExpandable}
        onNodeSelectionToggled={onNodeSelectionToggled}
        onToggleNode={onToggleNode}
      />
    );
  })
  .add('Single-select node', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(duplicate(initialNodes));

    const getNodeSelectionMode = (/*node*/) => NodeSelectionMode.single;
    const [selectedNodeIds, setSelectedNodeIds] = React.useState<NodeId[]>([]);

    const onNodeSelectionToggled = React.useCallback(
      (node: MyNode) => {
        const index = selectedNodeIds.indexOf(node.id);

        if (index === -1) {
          setSelectedNodeIds([node.id]);
        } else {
          setSelectedNodeIds([]);
        }
      },
      [selectedNodeIds, setSelectedNodeIds]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={initialNodes}
        getNodeSelectionMode={getNodeSelectionMode}
        selectedNodeIds={selectedNodeIds}
        isNodeExpandable={isNodeExpandable}
        onNodeSelectionToggled={onNodeSelectionToggled}
        onToggleNode={onToggleNode}
      />
    );
  })
  .add('Selection mode always, with some disabled nodes', () => {
    const getNodeSelectionMode = (/*node*/) => NodeSelectionMode.multi;

    const [nodes, setNodes] = React.useState<MyNode[]>(initialNodes);
    const [selectedNodeIds, setSelectedNodeIds] = React.useState<NodeId[]>([
      1, 4,
    ]);

    const onNodeSelectionToggled = React.useCallback(
      (node: MyNode) => {
        const index = selectedNodeIds.indexOf(node.id);

        if (index === -1) {
          setSelectedNodeIds(selectedNodeIds.concat([node.id]));
        } else {
          setSelectedNodeIds([
            ...selectedNodeIds.slice(0, index),
            ...selectedNodeIds.slice(index + 1),
          ]);
        }
      },
      [selectedNodeIds, setSelectedNodeIds]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        nodeSelectorVisibility={NodeSelectorVisibility.always}
        getNodeSelectionMode={getNodeSelectionMode}
        selectedNodeIds={selectedNodeIds}
        isNodeExpandable={isNodeExpandable}
        onNodeSelectionToggled={onNodeSelectionToggled}
        isSelectorDisabled={(node) => node.id >= 4}
        onToggleNode={onToggleNode}
      />
    );
  })
  .add('Click to change "current" node', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(initialNodes);

    const [currentNodeIds, setCurrentNodeIds] = React.useState<NodeId[]>([]);
    const onNodeClicked = React.useCallback(
      (node: MyNode) => {
        setCurrentNodeIds([node.id]);
      },
      [setCurrentNodeIds]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        currentNodeIds={currentNodeIds}
        onNodeClicked={onNodeClicked}
        isNodeExpandable={isNodeExpandable}
        onToggleNode={onToggleNode}
      />
    );
  })
  .add('Add and delete via menu, highlight new', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(
      duplicate(initialNodes.slice(0, 2))
    );

    const onGetNodeActions = React.useCallback(
      (/*node*/) => {
        return [
          {
            text: 'Add node below',
            action: 'add-node-below',
            iconPath: plus,
          },
          {
            text: 'Delete node',
            action: 'delete-node',
            iconPath: trashBin,
          },
        ];
      },
      []
    );

    const onContextMenuItemClick = React.useCallback(
      (action, node) => {
        let newId,
          newNodes = nodes;
        const index = nodes.indexOf(node);

        switch (action.action) {
          case 'add-node-below':
            newId = new Date().valueOf();

            newNodes = [...nodes];

            newNodes.splice(index + 1, 0, {
              label: `Node ${nodes.length + 1}`,
              id: newId,
            });

            setNewNodeIds([newId]);
            break;

          case 'delete-node':
            newNodes = [...nodes.slice(0, index), ...nodes.slice(index + 1)];
            break;
        }

        setNodes(newNodes);
      },
      [nodes, setNodes]
    );

    const [newNodeIds, setNewNodeIds] = React.useState<NodeId[]>([]);

    return (
      <TreeView
        nodes={nodes}
        showNodeMenu={true}
        highlightNodeIds={newNodeIds}
        onGetNodeActions={onGetNodeActions}
        isNodeExpandable={isNodeExpandable}
        onContextMenuItemClick={onContextMenuItemClick}
      />
    );
  })
  .add('Drag and drop', () => {
    let initial = true;
    const [nodes, setNodes] = React.useState(duplicate(initialNodes));
    if (initial) {
      nodes[3].isExpanded = false;
      initial = false;
    }

    const logString = (val: string): void => console.log(val);

    type MyCustomProps = {myProp: string};

    const onNodesDropped = React.useCallback(
      // Here we assert that the drop info will include our custom prop myProp (: DropInfo<MyCustomProps>).
      // If we hadn't provided a value for the prepareDragInfo prop of the TreeView component, or if the object returned from
      // prepareDragInfo didn't have the prop myProp, we would get a type error when trying to use myProp in this function
      ({nodeIds, position, targetNode, myProp}: DropInfo<MyCustomProps>) => {
        let targetNodeIndex = nodes.indexOf(targetNode);
        const fromIndex = nodes.findIndex((n) => n.id === nodeIds[0]);

        if (position === NodePosition.after) {
          targetNodeIndex++;
        }

        const newNodes = [...nodes];
        newNodes.splice(targetNodeIndex, 0, newNodes.splice(fromIndex, 1)[0]);

        setNodes(newNodes);

        logString(myProp); // TypeScript understands that myProp is a string
      },
      [nodes, setNodes]
    );

    // The same observations as for the onNodesDropped function are valid here
    const onValidateDrop = useCallback(({myProp}: DropInfo<MyCustomProps>) => {
      logString(myProp);

      return true;
    }, []);

    const onToggleNode = React.useCallback(
      (node) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        isDragEnabled={true}
        onToggleNode={onToggleNode}
        onNodesDropped={onNodesDropped}
        // Here we take a copy of the default drag info and we add our own custom prop myProp.
        // The component now konws that the callbacks receiving drop info as an arg can expect it to contain the custom prop myProp
        prepareDragInfo={(info) => ({...info, myProp: 'alessio', myProp2: 5})}
        onValidateDrop={onValidateDrop}
        allowDropInside={(node) => {
          return Boolean(node.children);
        }}
        isNodeExpandable={isNodeExpandable}
      />
    );
  })
  .add('Drag and drop between trees', () => {
    const sourceNodes = flatten(duplicate(initialNodes));
    const [targetNodes, setTargetNodes] = React.useState(
      duplicate(initialNodes)
    );

    const onNodesDropped = React.useCallback(
      ({nodeIds, position, targetNode /*sourceTreeId,*/}) => {
        let targetNodeIndex = targetNodes.indexOf(targetNode);
        const droppedNodes = nodeIds
          .map((id) => sourceNodes.find((n) => n.id === id))
          .map((n) => ({
            ...n,
            label: `Node ${targetNodes.length + 1}`,
            id: new Date().valueOf(),
          }));

        if (position === NodePosition.after) {
          targetNodeIndex++;
        }

        const newNodes = [...targetNodes];
        newNodes.splice(targetNodeIndex, 0, ...droppedNodes);
        setTargetNodes(newNodes);
      },
      [targetNodes, setTargetNodes, sourceNodes]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setTargetNodes(duplicate(targetNodes));
      },
      [targetNodes, setTargetNodes]
    );

    return (
      <div
        style={{
          paddingTop: '50px',
          display: 'grid',
          gridTemplateColumns: '1fr 1fr',
          gridGap: '100px',
        }}>
        <div>
          <h3>Drag from this tree...</h3>
          <TreeView
            nodes={sourceNodes}
            treeId="source"
            isDragEnabled={true}
            isNodeExpandable={isNodeExpandable}
            onToggleNode={onToggleNode}
            allowDropInside={(node) => {
              return Boolean(node.children);
            }}
          />
        </div>
        <div>
          <h3>...to this tree</h3>
          <TreeView
            nodes={targetNodes}
            treeId="target"
            onValidateDrop={(/*dropInfo*/) => true}
            onNodesDropped={onNodesDropped}
            isNodeExpandable={isNodeExpandable}
            onToggleNode={onToggleNode}
            allowDropInside={(node) => {
              return Boolean(node.children);
            }}
          />
        </div>
      </div>
    );
  })
  .add('Lazy-load using a nodes-dictionary', () => {
    /*
    Using a reducer here to ensure that the onToggleNode callback doesn't change too often,
    which would cause each node in the tree to re-render each time,
    which could potentially be a bit slow if the tree is big.
     */

    const [state, dispatch] = React.useReducer(reducer, {
      selectedNodeIds: [],
      nodesByParentId: {
        root: [
          {
            id: 1,
            label: 'Node 1',
            parentId: 'root',
            metadata: {
              type: '(single)',
            },
          },
          {
            id: 2,
            label: 'Node 2',
            parentId: 'root',
            metadata: {
              type: '(folder)',
            },
            hasMoreChildren: true,
          },
        ],
      },
    });

    const onToggleNode = React.useCallback(
      (node) => {
        dispatch({
          type: 'toggle',
          node,
        });
      },
      [dispatch]
    );

    return (
      <TreeView
        parentId={'root'}
        onToggleNode={onToggleNode}
        nodesByParentId={state.nodesByParentId}
        isNodeExpandable={isNodeExpandableLazy}
        renderNodeContents={renderNodeContents}
      />
    );
  })
  .add(
    'Current, expand/collapse, context menu, multi-select, keyboard navigation',
    () => {
      const getNodeSelectionMode = (/*node*/) => NodeSelectionMode.multi;
      const [nodes, setNodes] = React.useState(duplicate(initialNodes));

      const [currentNodeIds, setCurrentNodeIds] = React.useState<NodeId[]>([]);
      const [selectedNodeIds, setSelectedNodeIds] = React.useState<NodeId[]>(
        []
      );

      const onNodeSelectionToggled = React.useCallback(
        (node) => {
          const index = selectedNodeIds.indexOf(node.id);

          if (index === -1) {
            setSelectedNodeIds(selectedNodeIds.concat([node.id]));
          } else {
            setSelectedNodeIds([
              ...selectedNodeIds.slice(0, index),
              ...selectedNodeIds.slice(index + 1),
            ]);
          }
        },
        [selectedNodeIds, setSelectedNodeIds]
      );
      const onNodeClicked = React.useCallback(
        (node) => {
          setCurrentNodeIds([node.id]);
        },
        [setCurrentNodeIds]
      );
      const onToggleNode = React.useCallback(
        (node) => {
          node.isExpanded = !node.isExpanded;
          setNodes(duplicate(nodes));
        },
        [nodes, setNodes]
      );
      const onGetNodeActions = React.useCallback(
        (/*node*/) => {
          return [
            {
              text: 'Add node below',
              action: 'add-node-below',
              iconPath: plus,
              tooltip: 'Add node below this node',
            },
            {
              text: 'Delete node',
              action: 'delete-node',
              iconPath: trashBin,
              tooltip: 'Delete this node immediately',
            },
          ];
        },
        []
      );

      const onContextMenuItemClick = React.useCallback(
        (action, node) => {
          let newId,
            newNodes = nodes;
          const index = nodes.indexOf(node);

          switch (action.action) {
            case 'add-node-below':
              newId = new Date().valueOf();

              newNodes = [...nodes];

              newNodes.splice(index + 1, 0, {
                label: `Node ${nodes.length + 1}`,
                id: newId,
              });

              setNewNodeIds([newId]);
              break;

            case 'delete-node':
              newNodes = [...nodes.slice(0, index), ...nodes.slice(index + 1)];
              break;
          }

          setNodes(newNodes);
        },
        [nodes, setNodes]
      );

      const [newNodeIds, setNewNodeIds] = React.useState<NodeId[]>([]);

      return (
        <div style={{width: '500px', overflow: 'auto', height: '200px'}}>
          <TreeView
            isKeyboardNavigationEnabled={boolean(
              'isKeyboardNavigationEnabled',
              true
            )}
            isDragEnabled={true}
            nodes={nodes}
            currentNodeIds={currentNodeIds}
            onNodeClicked={onNodeClicked}
            isNodeExpandable={isNodeExpandable}
            onToggleNode={onToggleNode}
            showNodeMenu={boolean('showNodeMenu', true)}
            onValidateDrop={(/*dropInfo*/) => true}
            highlightNodeIds={newNodeIds}
            onGetNodeActions={onGetNodeActions}
            onContextMenuItemClick={onContextMenuItemClick}
            getNodeSelectionMode={getNodeSelectionMode}
            selectedNodeIds={selectedNodeIds}
            onNodeSelectionToggled={onNodeSelectionToggled}
            renderNodeIcon={renderNodeIcon}
            density={select('density', TreeDensity, TreeDensity.standard)}
            getNodeSelectedState={(node) => {
              if (selectedNodeIds.includes(node.id)) {
                return NodeSelectedState.on;
              }

              if (
                selectedNodeIds.some((id) => {
                  return String(id).startsWith(`${node.id}.`);
                })
              ) {
                return NodeSelectedState.partial;
              }

              return NodeSelectedState.off;
            }}
          />
        </div>
      );
    }
  )
  .add('Nodes without dividers', () => {
    const [nodes, setNodes] = React.useState<MyNode[]>(duplicate(initialNodes));
    const [currentNodeIds, setCurrentNodeIds] = React.useState<NodeId[]>([]);

    const onNodeClicked = React.useCallback(
      (node) => {
        setCurrentNodeIds([node.id]);
      },
      [setCurrentNodeIds]
    );

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        node.isExpanded = !node.isExpanded;
        setNodes(duplicate(nodes));
      },
      [nodes, setNodes]
    );

    return (
      <TreeView
        nodes={nodes}
        currentNodeIds={currentNodeIds}
        onNodeClicked={onNodeClicked}
        isNodeExpandable={isNodeExpandable}
        showDividers={false}
        onToggleNode={onToggleNode}
      />
    );
  })
  .add('Selection tree', () => {
    const [state, dispatch] = React.useReducer(reducer, {
      selectedNodeIds: [],
      nodesByParentId: {
        root: [
          {
            id: 1,
            label: 'Node 1',
            parentId: 'root',
            metadata: {
              type: '(multi)',
            },
          },
          {
            id: 2,
            label: 'Node 2',
            parentId: 'root',
            metadata: {
              type: '(folder)',
            },
            hasMoreChildren: true,
          },
          {
            id: 3,
            label: 'Node 3',
            parentId: 'root',
            metadata: {
              type: '(single)',
            },
          },
          {
            id: 4,
            label: 'Node 4',
            parentId: 'root',
            metadata: {
              type: '(folder)',
            },
            hasMoreChildren: true,
          },
        ],
      },
    });

    const onToggleNode = React.useCallback(
      (node: MyNode) => {
        dispatch({
          type: 'toggle',
          node,
        });
      },
      [dispatch]
    );

    const onNodeSelectionToggled = React.useCallback(
      (node: MyNode) => {
        dispatch({
          type: 'select',
          node,
        });
      },
      [dispatch]
    );

    const onChildSelectionToggled = React.useCallback(
      (node: MyNode, toggleOn: boolean) => {
        dispatch({
          type: 'select-with-children',
          node,
          toggleOn,
        });
      },
      [dispatch]
    );

    return (
      <div style={{width: 300, border: '1px solid #ccc'}}>
        <SelectionTree
          parentId={'root'}
          onToggleNode={onToggleNode}
          nodesByParentId={state.nodesByParentId}
          selectedNodeIds={state.selectedNodeIds}
          isNodeExpandable={isNodeExpandableLazy}
          showChildSelection={true}
          onNodeSelectionToggled={onNodeSelectionToggled}
          onChildSelectionToggled={onChildSelectionToggled}
          renderNodeContents={renderNodeContents}
        />
      </div>
    );
  });
