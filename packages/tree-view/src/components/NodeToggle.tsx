import React, {MouseEventHandler} from 'react';
import cn from 'classnames';
import {IconButton} from '@jotunheim/react-button';
import Icon, {menuDown} from '@jotunheim/react-icons';
import {useTheme, bemFactory} from '@jotunheim/react-themes';

import componentThemes from '../themes';

type Props = {
  isExpanded: boolean;
  isExpandable: boolean;
  onToggleExpanded: MouseEventHandler;
};

const NodeToggle = ({isExpandable, onToggleExpanded, isExpanded}: Props) => {
  // The toggle wrapper should always be rendered to reserve space for the toggle
  // to keep all nodes aligned properly in the tree

  const {toggleBaseClassName, classNames} = useTheme(
    'treeView',
    componentThemes
  );
  const {block, element} = bemFactory({
    baseClassName: toggleBaseClassName,
    classNames,
  });

  return (
    <div
      className={block()}
      data-testid="tree-node-toggle"
      data-react-tree-expand-toggle>
      {isExpandable && (
        <div
          className={cn(element('button'), {
            [element('button', 'collapsed')]: !isExpanded,
          })}>
          <IconButton
            data-testid="expand-toggle-button"
            size={IconButton.sizes.small}
            onClick={onToggleExpanded}
            data-react-tree-expand-toggle-button
            data-react-tree-expand-toggle-button--is-expanded={`${isExpanded}`}>
            <Icon path={menuDown} />
          </IconButton>
        </div>
      )}
    </div>
  );
};

export default NodeToggle;
