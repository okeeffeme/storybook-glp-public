import React from 'react';

import TreeView from './TreeView';

import {TreeMode} from '../constants';

import {NavigationTreeProps} from '../types';

const NavigationTree = <
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>(
  props: NavigationTreeProps<T, U>
) => {
  return (
    <TreeView data-testid="tree-view" {...props} mode={TreeMode.navigation} />
  );
};

export default NavigationTree;
