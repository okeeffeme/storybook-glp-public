import React, {MouseEventHandler, ReactNode} from 'react';
import SimpleLink from '@jotunheim/react-simple-link';

type Props = {
  href?: string;
  children?: ReactNode;
  className?: string;
  onClick: MouseEventHandler;
};

const NodeLink = ({href, onClick, children, className}: Props) => {
  const commonProps = {
    href,
    onClick,
    className,
    tabIndex: -1,
    draggable: false,
    'data-tree-node-link': 'true',
  };

  if (href) {
    return <SimpleLink {...commonProps}>{children}</SimpleLink>;
  }

  // This should ideally be a button, but FF, IE and Edge has issues when dragging a button
  // even though we are not actually dragging the button (we are dragging a parent node),
  // it still causes issues with drag, maybe because it is the highest z-index'ed item?
  // Having a div with a role of button solves our use cases here.
  // Keyboard events are handled separately.
  return (
    <div data-testid="node-link" {...commonProps} role="button">
      {children}
    </div>
  );
};

export default NodeLink;
