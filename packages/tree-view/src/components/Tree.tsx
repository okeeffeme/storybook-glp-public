import React, {Context, useContext} from 'react';
import cn from 'classnames';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
// import {useRenderTracking} from '@jotunheim/react-utils';
import {NodeSelectionMode} from '../constants';
import {TreeNode, NodeId} from '../types';
import {defaultNodes, isContained} from '../utils';

import useNodeFocus from '../hooks/useNodeFocus';
import useHighlightNodeIds from '../hooks/useHighlighNodeIds';

import Node from './Node';
import {TreeSettings, TreeState} from '../context';

import componentThemes from '../themes';

type ContainerProps<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  isRoot?: boolean;
  stateContext: Context<TreeState<T>>;
  settingsContext: Context<TreeSettings<T, U>>;
};

type ListProps<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  nodes: TreeNode<T>[];
  parentId: NodeId;
  isParentDeleted: boolean;
  isMultiSelecting: boolean;
  currentNodeIds?: NodeId[];
  selectedNodeIds?: NodeId[];
  highlightNodeIds?: NodeId[];
} & ContainerProps<T, U>;

function List<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>(props: ListProps<T, U>) {
  const {
    nodes,
    isRoot,
    parentId,
    currentNodeIds = defaultNodes,
    selectedNodeIds = defaultNodes,
    highlightNodeIds = defaultNodes,
    isParentDeleted,
    isMultiSelecting,
    stateContext,
    settingsContext,
  } = props;

  const settings = useContext(settingsContext);
  const {
    autoFocus,
    onToggleNode,
    onNodeClicked,
    onDeleteNodes,
    isDragEnabled,
    isNodeDeletable,
    isNodeExpandable,
    getNodeSelectedState,
    getNodeSelectionMode,
    onNodeSelectionToggled,
  } = settings;

  const {listBaseClassName, classNames} = useTheme('treeView', componentThemes);
  const {keyHandlingRef, onNodeKeyDown} = useNodeFocus({
    parentId,
    autoFocus,
    onToggleNode,
    onNodeClicked,
    onDeleteNodes,
    currentNodeIds,
    selectedNodeIds,
    isNodeDeletable,
    isNodeExpandable,
    onNodeSelectionToggled,
  });
  const nodeIdsToHighlight = useHighlightNodeIds(highlightNodeIds);

  // Uncomment this to track potentially unwanted re-renders
  // useRenderTracking({
  //   ...props,
  //   ...settings,
  // }, key => console.log('TREE', key, 'has changed in level:', props.parentId));

  const {block, element, modifier} = bemFactory({
    baseClassName: listBaseClassName,
    classNames,
  });

  let hasSelector = false;
  const output = nodes.map((node, nodeIndex) => {
    const selectionMode = getNodeSelectionMode(node);
    const isExpandable = isNodeExpandable(node);

    if (selectionMode !== NodeSelectionMode.none) {
      hasSelector = true;
    }

    return (
      <Node
        key={node.id}
        node={node}
        parentId={parentId}
        nodeIndex={nodeIndex}
        className={element('li')}
        isCurrent={currentNodeIds.includes(node.id)}
        isExpandable={isExpandable}
        isParentDeleted={isParentDeleted}
        isMultiSelecting={isMultiSelecting}
        isHighlighted={nodeIdsToHighlight.includes(node.id)}
        selectedState={getNodeSelectedState(node, selectedNodeIds)}
        selectionMode={selectionMode}
        selectedNodeIds={selectedNodeIds}
        onNodeKeyDown={onNodeKeyDown}
        stateContext={stateContext}
        settingsContext={settingsContext}
      />
    );
  });

  return (
    <ol
      data-testid="tree"
      className={cn(block(), {
        [modifier('is-root')]: isRoot,
        [modifier('has-selector')]: hasSelector,
        [modifier('is-drag-enabled')]: isRoot && isDragEnabled,
      })}
      data-react-tree="tree-list"
      data-react-tree-is-root={`${isRoot}`}
      ref={keyHandlingRef}>
      {output}
    </ol>
  );
}

const ListMemo = React.memo(List) as typeof List;

type TreeProps<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  nodes?: TreeNode<T>[];
  parentId?: NodeId;
  isParentDeleted?: boolean;
} & ContainerProps<T, U>;

const Tree = <
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>({
  nodes,
  isRoot = false,
  parentId = '',
  isParentDeleted = false,
  stateContext,
  settingsContext,
}: TreeProps<T, U>) => {
  const {currentNodeIds, selectedNodeIds, nodesByParentId, highlightNodeIds} =
    React.useContext(stateContext);

  // Make sure we have a list of nodes.
  // Prefer the nodes from the store if they exist (could have been lazily loaded or reloaded),
  // otherwise use the nodes that are passed down.
  nodes = nodesByParentId[parentId] || nodes || defaultNodes;

  // Don't pass the ids to each level, only the affected ones, to avoid too many re-renders
  const childIds = nodes.map((n) => n.id);
  const hasCurrentChild = isContained(currentNodeIds, childIds);
  const hasSelectedChild = isContained(selectedNodeIds, childIds);
  const hasHighlightedChild = isContained(highlightNodeIds, childIds);

  return (
    <ListMemo
      nodes={nodes}
      isRoot={isRoot}
      parentId={parentId}
      isParentDeleted={isParentDeleted}
      isMultiSelecting={selectedNodeIds.length > 0}
      currentNodeIds={hasCurrentChild ? currentNodeIds : undefined}
      selectedNodeIds={hasSelectedChild ? selectedNodeIds : undefined}
      highlightNodeIds={hasHighlightedChild ? highlightNodeIds : undefined}
      stateContext={stateContext}
      settingsContext={settingsContext}
    />
  );
};

export default Tree;
