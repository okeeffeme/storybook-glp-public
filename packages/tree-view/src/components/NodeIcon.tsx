import React, {ReactNode} from 'react';

import {TreeNode} from '../types';

type Props<T extends Record<string, unknown>> = {
  node: TreeNode<T>;
  className?: string;
  renderNodeIcon?: (node: TreeNode<T>) => ReactNode;
};

const NodeIcon = <T extends Record<string, unknown>>({
  className,
  renderNodeIcon,
  node,
}: Props<T>) => (
  <div className={className}>{renderNodeIcon?.(node) ?? null}</div>
);

export default NodeIcon;
