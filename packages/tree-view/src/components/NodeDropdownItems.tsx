import React from 'react';
import cn from 'classnames';

import Tooltip from '@jotunheim/react-tooltip';
import Dropdown from '@jotunheim/react-dropdown';
import {InformationIcon} from '@jotunheim/react-icons';
import {useTheme, bemFactory} from '@jotunheim/react-themes';

import {TreeNode, NodeAction} from '../types';

import componentThemes from '../themes';

type Props<T extends Record<string, unknown>> = {
  node: TreeNode<T>;
  onClose: () => void;
  onGetNodeActions?: (node: TreeNode<T>) => NodeAction[];
  onContextMenuItemClick?: (nodeAction: NodeAction, node: TreeNode<T>) => void;
};

const NodeDropdownItems = <T extends Record<string, unknown>>({
  node,
  onClose,
  onGetNodeActions,
  onContextMenuItemClick,
}: Props<T>) => {
  const {actionOverflowMenuBaseClassName, classNames} = useTheme(
    'treeView',
    componentThemes
  );
  const dropdownMenu = bemFactory({
    baseClassName: actionOverflowMenuBaseClassName,
    classNames,
  });

  const actions = onGetNodeActions?.(node) ?? [];

  return (
    <>
      {actions.map((menuAction, i) => {
        const {
          iconPath,
          text,
          href = '#',
          action = '',
          header,
          tooltip,
          isDivider = false,
          isDisabled = false,
        } = menuAction;

        if (header) {
          return <Dropdown.MenuHeader key={i}>{header}</Dropdown.MenuHeader>;
        }

        if (isDivider) {
          return <Dropdown.MenuDivider key={i} />;
        }

        return (
          <Dropdown.MenuItem
            key={i}
            href={href}
            disabled={isDisabled}
            iconPath={iconPath}
            onClick={(e) => {
              e.preventDefault();
              onClose();
              onContextMenuItemClick?.(menuAction, node);
            }}
            data-dropdown-action={action}>
            <div
              className={cn(
                dropdownMenu.element('item'),
                dropdownMenu.element('item', action.toLowerCase())
              )}>
              <div className={dropdownMenu.element('text')}>{text}</div>
              {tooltip && (
                <Tooltip content={tooltip} placement={'top'}>
                  <InformationIcon />
                </Tooltip>
              )}
            </div>
          </Dropdown.MenuItem>
        );
      })}
    </>
  );
};

export default NodeDropdownItems;
