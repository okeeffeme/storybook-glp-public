import React from 'react';

import TreeView from './TreeView';

import {TreeMode, TreeDensity} from '../constants';

import {SelectionTreeProps} from '../types';

const SelectionTree = <T extends Record<string, unknown>>(
  props: SelectionTreeProps<T>
) => {
  return (
    <TreeView
      {...props}
      mode={TreeMode.selection}
      density={TreeDensity.comfortable}
      showDividers={false}
    />
  );
};

export default SelectionTree;
