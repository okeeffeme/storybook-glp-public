import React from 'react';
import cn from 'classnames';
import {StrictModifiers} from '@popperjs/core';

import {IconButton} from '@jotunheim/react-button';
import Icon, {dotsVertical} from '@jotunheim/react-icons';
import {useTheme, bemFactory} from '@jotunheim/react-themes';
import Dropdown, {PlacementTypes} from '@jotunheim/react-dropdown';

import NodeDropdownItems from './NodeDropdownItems';

import {TreeNode, NodeAction} from '../types';

import componentThemes from '../themes';

const getDataAttrs = (node) => {
  const {id, name} = node;
  return Object.keys({id, name}).reduce((result, current) => {
    result[`data-dropdown-for-node-${current}`] = node[current];
    return result;
  }, {});
};

// Allow popper to be rendered outside tree scrollcontainer,
// this overrides the default set in usePopper
const modifiers: StrictModifiers[] = [
  {
    name: 'flip',
    options: {
      altBoundary: false,
    },
  },
];

type Props<T extends Record<string, unknown>> = {
  node: TreeNode<T>;
  isCurrentNode?: boolean;
  onGetNodeActions?: (node: TreeNode<T>) => NodeAction[];
  onContextMenuItemClick?: (nodeAction: NodeAction, node: TreeNode<T>) => void;
};

const NodeDropdown = <T extends Record<string, unknown>>({
  node,
  isCurrentNode = false,
  onGetNodeActions,
  onContextMenuItemClick,
}: Props<T>) => {
  const {
    classNames,
    actionOverflowToggleBaseClassName,
    actionOverflowMenuBaseClassName,
  } = useTheme('treeView', componentThemes);
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const {block, modifier} = bemFactory({
    baseClassName: actionOverflowToggleBaseClassName,
    classNames,
  });

  const dropdownMenu = bemFactory({
    baseClassName: actionOverflowMenuBaseClassName,
    classNames,
  });

  return (
    <div
      data-testid="node-dropdown"
      className={cn(block(), {
        [modifier('is-current')]: isCurrentNode,
        [modifier('is-open')]: isMenuOpen,
      })}>
      <Dropdown
        data-testid="tree-action-overflow"
        data-react-tree-action-overflow
        open={isMenuOpen}
        modifiers={modifiers}
        placement={PlacementTypes.bottomStart}
        menu={
          <div className={dropdownMenu.block()}>
            <Dropdown.Menu
              {...getDataAttrs(node)}
              data-react-tree-action-overflow-menu>
              {isMenuOpen && (
                <NodeDropdownItems
                  node={node}
                  onClose={() => {
                    setIsMenuOpen(false);
                  }}
                  onGetNodeActions={onGetNodeActions}
                  onContextMenuItemClick={onContextMenuItemClick}
                />
              )}
            </Dropdown.Menu>
          </div>
        }
        onToggle={setIsMenuOpen}>
        <IconButton
          size={IconButton.sizes.small}
          data-react-tree-action-overflow-button>
          <Icon path={dotsVertical} />
        </IconButton>
      </Dropdown>
    </div>
  );
};

export default React.memo(NodeDropdown) as typeof NodeDropdown;
