import React from 'react';
import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton} from '@jotunheim/react-button';
import Icon, {
  checkboxMultipleMarked,
  uncheckMultipleMarked,
} from '@jotunheim/react-icons';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import {TreeNode, TreeViewTexts} from '../types';

import componentThemes from '../themes';

type Props<T extends Record<string, unknown>> = {
  node: TreeNode<T>;
  texts: TreeViewTexts;
  onChildSelectionToggled: (node: TreeNode<T>, toggleOn: boolean) => void;
};

const ChildSelection = <T extends Record<string, unknown>>({
  node,
  texts,
  onChildSelectionToggled,
}: Props<T>) => {
  const {classNames, nodeBaseClassName} = useTheme('treeView', componentThemes);

  const {element} = bemFactory({
    baseClassName: nodeBaseClassName,
    classNames,
  });

  return (
    <div data-testid="child-selection" className={element('child-selection')}>
      <Tooltip content={texts.selectWithChildren}>
        <IconButton
          data-select-children-button
          size={IconButton.sizes.small}
          onClick={() => {
            onChildSelectionToggled(node, true);
          }}>
          <Icon path={checkboxMultipleMarked} />
        </IconButton>
      </Tooltip>

      <Tooltip content={texts.unselectWithChildren}>
        <IconButton
          data-unselect-children-button
          size={IconButton.sizes.small}
          onClick={() => {
            onChildSelectionToggled(node, false);
          }}>
          <Icon path={uncheckMultipleMarked} />
        </IconButton>
      </Tooltip>
    </div>
  );
};

export default ChildSelection;
