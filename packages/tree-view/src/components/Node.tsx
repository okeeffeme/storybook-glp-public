import React, {Context, useContext} from 'react';
import cn from 'classnames';

import {bemFactory, useTheme} from '@jotunheim/react-themes';
// import {useRenderTracking} from '@jotunheim/react-utils';

import Tree from './Tree';
import NodeLink from './NodeLink';
import NodeIcon from './NodeIcon';
import NodeToggle from './NodeToggle';
import NodeSelector from './NodeSelector';
import NodeDropdown from './NodeDropdown';
import ChildSelection from './ChildSelection';

import useDragDrop from '../hooks/useDragDrop';

import {TreeSettings, TreeState} from '../context';

import {TreeMode, NodeSelectedState, NodeSelectionMode} from '../constants';

import {NodeId, TreeNode} from '../types';

import componentThemes from '../themes';

export type NodeProps<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  node: TreeNode<T>;
  parentId: NodeId;
  className: string;
  nodeIndex: number;
  isCurrent: boolean;
  isExpandable: boolean;
  isHighlighted: boolean;
  selectedState: NodeSelectedState;
  selectionMode: NodeSelectionMode;
  onNodeKeyDown: (e: MouseEvent, node: TreeNode<T>, nodeIndex: number) => void;
  isParentDeleted: boolean;
  selectedNodeIds: NodeId[];
  isMultiSelecting: boolean;
  stateContext: Context<TreeState<T>>;
  settingsContext: Context<TreeSettings<T, U>>;
};

export const Node = <
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>(
  props: NodeProps<T, U>
) => {
  const {
    node,
    parentId,
    nodeIndex,
    className,
    isCurrent = false,
    isExpandable,
    onNodeKeyDown,
    selectedState,
    selectionMode,
    isHighlighted = false,
    selectedNodeIds,
    isParentDeleted,
    isMultiSelecting,
    stateContext,
    settingsContext,
  } = props;

  const settings = useContext(settingsContext);
  const {
    mode,
    texts,
    treeId,
    showNodeMenu,
    showDividers,
    onToggleNode,
    onNodeClicked,
    isDragEnabled,
    renderNodeIcon,
    createNodeHref,
    onNodesDropped,
    onValidateDrop,
    prepareDragInfo,
    allowDropInside,
    onGetNodeActions,
    renderNodeContents,
    showChildSelection,
    onValidateDragStart,
    onContextMenuItemClick,
    onNodeSelectionToggled,
    isKeyboardNavigationEnabled,
    isSelectorDisabled,
    onChildSelectionToggled,
  } = settings;

  // Uncomment this to track potentially unwanted re-renders
  // useRenderTracking({
  //   ...props,
  //   ...settings,
  // }, key => console.log('NODE', key, 'has changed in node:', props.node.id));

  const {nodeRef, itemRef, onDragStart, onDragOver, onDragEnd, onDrop} =
    useDragDrop({
      node,
      treeId,
      parentId,
      onToggleNode,
      isExpandable,
      onNodesDropped,
      onValidateDrop,
      allowDropInside,
      selectedNodeIds,
      prepareDragInfo,
      onValidateDragStart,
    });

  const {classNames, nodeBaseClassName} = useTheme('treeView', componentThemes);
  const [isDragging, setIsDragging] = React.useState(false);

  const onToggleExpanded = (e) => {
    e.stopPropagation();

    onToggleNode(node);
  };

  const showAsDeleted = () => node.isDeleted || isParentDeleted;

  const {block, element, modifier} = bemFactory({
    baseClassName: nodeBaseClassName,
    classNames,
  });

  const createNodeClassName = () => {
    const {type} = node;

    return cn(block(), {
      [modifier(type ?? '')]: !!type,
      [modifier('is-new')]: isHighlighted,
      [modifier('is-deleted')]: showAsDeleted(),
      [modifier('is-current')]: isCurrent,
      [modifier('is-dragging')]: isDragging && onValidateDragStart(node),
      [modifier('is-selected')]: selectedState === NodeSelectedState.on,
      [modifier('has-dividers')]: showDividers,
    });
  };

  const onToggleNodeSelection = (shiftKey) =>
    onNodeSelectionToggled(node, parentId, shiftKey);

  const onKeyDown = (e) => {
    if (isKeyboardNavigationEnabled) {
      onNodeKeyDown(e, node, nodeIndex);
    }
  };

  const onLinkClicked = (e) => {
    if (e.shiftKey || isMultiSelecting) {
      e.preventDefault();
      e.stopPropagation();

      onToggleNodeSelection(e.shiftKey);
    } else if (e.ctrlKey === false && e.metaKey === false) {
      e.preventDefault();
      e.stopPropagation();

      onNodeClicked(node);
    }
  };

  const {name, id, isExpanded = false, children} = node;
  const hasChildren = Array.isArray(children) && children.length > 0;

  const handleDragStart = (e) => {
    onDragStart(e);
    setIsDragging(true);
  };

  const handleDragEnd = () => {
    onDragEnd();
    setIsDragging(false);
  };

  const disabledSelector = isSelectorDisabled(node);

  return (
    <li
      data-testid="node-item"
      className={className}
      ref={itemRef}
      data-react-tree-li
      onDrop={onDrop}
      onDragEnter={onDragOver}
      onDragOver={onDragOver}>
      <div
        data-testid="tree-node"
        ref={nodeRef}
        tabIndex={0}
        draggable={isDragEnabled}
        onKeyDown={onKeyDown}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        className={createNodeClassName()}
        data-tree-has-dividers={showDividers}
        data-tree-node-is-highlighted={`${isHighlighted}`}
        data-tree-node-name={name}
        data-tree-node-is-current={`${isCurrent}`}
        data-tree-node-is-deleted={`${showAsDeleted()}`}
        data-tree-node-type={node.type ?? ''}
        data-tree-node-id={id}>
        {mode === TreeMode.navigation &&
          selectionMode !== NodeSelectionMode.none && (
            <NodeSelector
              node={node}
              treeId={treeId}
              onToggle={onToggleNodeSelection}
              selectedState={selectedState}
              selectionMode={selectionMode}
              className={element('multiselection-selector')}
              labelClassName={element('multiselection-label')}
              disabled={disabledSelector}
            />
          )}

        <div className={element('position-helper')}>
          <NodeToggle
            isExpanded={isExpanded}
            isExpandable={isExpandable}
            onToggleExpanded={onToggleExpanded}
          />

          {mode === TreeMode.selection ? (
            <div className={element('link')}>
              <NodeSelector
                node={node}
                label={renderNodeContents(node)}
                treeId={treeId}
                onToggle={onToggleNodeSelection}
                selectedState={selectedState}
                selectionMode={selectionMode}
                disabled={disabledSelector}
                className={element('selector-wrapper')}
              />
            </div>
          ) : (
            <NodeLink
              href={createNodeHref(node)}
              className={element('link')}
              onClick={onLinkClicked}>
              <>
                <NodeIcon
                  node={node}
                  className={element('icon')}
                  renderNodeIcon={renderNodeIcon}
                />
                <div className={element('content')}>
                  {renderNodeContents(node)}
                </div>
              </>
            </NodeLink>
          )}

          {showNodeMenu(node) && (
            <NodeDropdown
              node={node}
              isCurrentNode={isCurrent}
              onGetNodeActions={onGetNodeActions}
              onContextMenuItemClick={onContextMenuItemClick}
            />
          )}

          {isExpandable && showChildSelection(node) && (
            <ChildSelection
              node={node}
              texts={texts}
              onChildSelectionToggled={onChildSelectionToggled}
            />
          )}
        </div>
      </div>
      {isExpanded && (hasChildren || isExpandable) && (
        <div>
          <Tree
            nodes={children}
            parentId={id}
            isParentDeleted={showAsDeleted()}
            stateContext={stateContext}
            settingsContext={settingsContext}
          />
        </div>
      )}
    </li>
  );
};

export default React.memo(Node) as typeof Node;
