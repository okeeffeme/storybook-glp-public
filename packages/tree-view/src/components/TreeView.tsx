import React, {useEffect, useMemo, useRef} from 'react';
import cn from 'classnames';

import {useTheme, bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import Tree from './Tree';
import {getSettingsContext, getStateContext} from '../context';
import {clearDragInfo, clearPreviousTarget} from '../hooks/useDragDrop';

import componentThemes from '../themes';
import {
  defaultNodes,
  defaultObject,
  defaultTexts,
  defaultTrueCallback,
  defaultFalseCallback,
  defaultUndefinedCallback,
  defaultRenderNodeContents,
  defaultGetNodeSelectionMode,
  defaultGetNodeSelectedState,
  defaultEmptyArrayCallback,
  defaultNullCallback,
  defaultNodeIsExpandable,
} from '../utils';

import {TreeMode, TreeDensity, NodeSelectorVisibility} from '../constants';

import {
  DragInfo,
  SelectionTreeProps,
  NodeCallbackBoolean,
  NavigationTreeProps,
} from '../types';

type Props<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  mode: TreeMode;
} & NavigationTreeProps<T, U> &
  SelectionTreeProps<T>;

export const TreeView = <
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>({
  mode = TreeMode.navigation,
  density = TreeDensity.standard,
  texts,
  nodes,
  parentId,
  className,
  treeId = '',
  autoFocus = false,
  showNodeMenu = false,
  showDividers = true,
  isDragEnabled = false,
  showChildSelection = false,
  isKeyboardNavigationEnabled = false,
  onChildSelectionToggled = defaultUndefinedCallback,
  prepareDragInfo = (defaultInfo) => defaultInfo as DragInfo<U>,
  createNodeHref = defaultUndefinedCallback,
  currentNodeIds = defaultNodes,
  nodesByParentId = defaultObject,
  selectedNodeIds = defaultNodes,
  highlightNodeIds = defaultNodes,
  nodeSelectorVisibility = NodeSelectorVisibility.hover,
  onNodeSelectionToggled = defaultUndefinedCallback,
  onToggleNode = defaultUndefinedCallback,
  renderNodeIcon = defaultNullCallback,
  renderNodeContents = defaultRenderNodeContents,
  onNodeClicked = defaultUndefinedCallback,
  onDeleteNodes = defaultUndefinedCallback,
  onNodesDropped = defaultUndefinedCallback,
  onValidateDrop = defaultFalseCallback,
  onGetNodeActions = defaultEmptyArrayCallback,
  onValidateDragStart = defaultTrueCallback,
  onContextMenuItemClick = defaultUndefinedCallback,
  allowDropInside = defaultFalseCallback,
  isNodeDeletable = defaultFalseCallback,
  isNodeExpandable = defaultNodeIsExpandable,
  isSelectorDisabled = defaultFalseCallback,
  getNodeSelectedState = defaultGetNodeSelectedState,
  getNodeSelectionMode = defaultGetNodeSelectionMode,
  ...rest
}: Props<T, U>) => {
  const {baseClassName, classNames} = useTheme('treeView', componentThemes);
  const {block, modifier} = bemFactory({
    baseClassName,
    classNames,
  });

  const isSelectorVisible =
    nodeSelectorVisibility === NodeSelectorVisibility.always ||
    selectedNodeIds.length > 0;

  const stateContextValue = useMemo(
    () => ({
      currentNodeIds,
      selectedNodeIds,
      highlightNodeIds,
      nodesByParentId,
    }),
    [highlightNodeIds, selectedNodeIds, currentNodeIds, nodesByParentId]
  );

  const settingsContextValue = useMemo(() => {
    let newShowNodeMenu: NodeCallbackBoolean<T>;
    if (typeof showNodeMenu === 'boolean') {
      newShowNodeMenu = () => showNodeMenu;
    } else {
      newShowNodeMenu = showNodeMenu;
    }

    let newShowChildSelection: NodeCallbackBoolean<T>;
    if (typeof showChildSelection === 'boolean') {
      newShowChildSelection = () => showChildSelection;
    } else {
      newShowChildSelection = showChildSelection;
    }

    return {
      mode,
      texts: {
        ...defaultTexts,
        ...texts,
      },
      treeId,
      autoFocus,
      showDividers,
      showNodeMenu: newShowNodeMenu,
      onToggleNode,
      onNodeClicked,
      onDeleteNodes,
      onNodesDropped,
      isDragEnabled,
      createNodeHref,
      renderNodeIcon,
      onValidateDrop,
      isNodeDeletable,
      allowDropInside,
      prepareDragInfo,
      isNodeExpandable,
      onGetNodeActions,
      showChildSelection: newShowChildSelection,
      isSelectorDisabled,
      renderNodeContents,
      onValidateDragStart,
      getNodeSelectedState,
      getNodeSelectionMode,
      onContextMenuItemClick,
      onNodeSelectionToggled,
      onChildSelectionToggled,
      isKeyboardNavigationEnabled,
    };
  }, [
    mode,
    texts,
    treeId,
    autoFocus,
    showDividers,
    showNodeMenu,
    onToggleNode,
    onNodeClicked,
    onDeleteNodes,
    onNodesDropped,
    isDragEnabled,
    createNodeHref,
    renderNodeIcon,
    onValidateDrop,
    isNodeDeletable,
    allowDropInside,
    prepareDragInfo,
    isNodeExpandable,
    onGetNodeActions,
    showChildSelection,
    isSelectorDisabled,
    renderNodeContents,
    onValidateDragStart,
    getNodeSelectedState,
    getNodeSelectionMode,
    onContextMenuItemClick,
    onNodeSelectionToggled,
    onChildSelectionToggled,
    isKeyboardNavigationEnabled,
  ]);

  useEffect(() => {
    return () => {
      clearPreviousTarget();

      clearDragInfo();
    };
  }, []);

  const StateContext = useRef(getStateContext<T>());
  const SettingsContext = useRef(getSettingsContext<T, U>());

  return (
    <SettingsContext.current.Provider value={settingsContextValue}>
      <StateContext.current.Provider value={stateContextValue}>
        <div
          data-testid="tree-view"
          className={cn(block(), className, {
            [modifier('comfortable')]: density === TreeDensity.comfortable,
            [modifier('compact')]: density === TreeDensity.compact,
            [modifier('mode-navigation')]: mode === TreeMode.navigation,
            [modifier('mode-selection')]: mode === TreeMode.selection,
          })}
          data-react-tree-mode={mode}
          data-react-tree-density={density}
          data-react-tree-is-selector-visible={isSelectorVisible}
          {...extractDataAriaIdProps(rest)}>
          <Tree
            nodes={nodes}
            parentId={parentId}
            isRoot={true}
            stateContext={StateContext.current}
            settingsContext={SettingsContext.current}
          />
        </div>
      </StateContext.current.Provider>
    </SettingsContext.current.Provider>
  );
};

export default TreeView;
