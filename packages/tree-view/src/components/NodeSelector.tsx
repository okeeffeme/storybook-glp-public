import {
  CheckBox,
  Radio,
  ToggleIconAlignment,
  ToggleWidth,
} from '@jotunheim/react-toggle';
import React, {MouseEvent, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import {NodeSelectedState, NodeSelectionMode} from '../constants';

import {TreeNode} from '../types';

import {toToggleState} from '../utils';

import componentThemes from '../themes';

type Props<T extends Record<string, unknown>> = {
  node: TreeNode<T>;
  treeId: string;
  label?: ReactNode;
  onToggle: (shiftKey: boolean) => void;
  className?: string;
  labelClassName?: string;
  selectedState: NodeSelectedState;
  selectionMode: NodeSelectionMode;
  disabled: boolean;
};

const getAriaCheckedValue = (selectedState: NodeSelectedState) => {
  if (selectedState === NodeSelectedState.partial) return 'mixed';
  return selectedState === NodeSelectedState.on;
};

const NodeSelector = <T extends Record<string, unknown>>({
  node,
  label,
  treeId,
  onToggle,
  selectedState,
  selectionMode,
  className,
  labelClassName,
  disabled,
}: Props<T>) => {
  const isSingleSelect = selectionMode === NodeSelectionMode.single;
  const Selector = isSingleSelect ? Radio : CheckBox;
  const nodeName = `${treeId}_${node.id}_node_selector`;

  const {classNames, nodeBaseClassName} = useTheme('treeView', componentThemes);
  const {element} = bemFactory({
    baseClassName: nodeBaseClassName,
    classNames,
  });

  return (
    <div data-testid="node-selector" className={className}>
      <div
        role={isSingleSelect ? 'radio' : 'checkbox'}
        aria-disabled={disabled}
        aria-checked={getAriaCheckedValue(selectedState)}
        className={cn(element('checkbox-wrapper'), labelClassName)}>
        <Selector
          data-tree-node-selector
          id={nodeName}
          disabled={disabled}
          checked={toToggleState(selectedState)}
          iconAlignment={ToggleIconAlignment.center}
          width={ToggleWidth.full}
          onChange={(checked, e) => {
            e?.stopPropagation();

            if (!disabled) {
              // We have to cast to a mouse event, as react provides change event as type which doesn't have shiftKey
              onToggle(
                (e as unknown as MouseEvent)?.nativeEvent.shiftKey ?? false
              );
            }
          }}>
          {label}
        </Selector>
      </div>
    </div>
  );
};

export default NodeSelector;
