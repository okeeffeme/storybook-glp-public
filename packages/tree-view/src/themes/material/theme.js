import classNames from './css/tree-view.module.css';

export default {
  treeView: {
    baseClassName: 'comd-tree-view',
    listBaseClassName: 'comd-tree-list',
    nodeBaseClassName: 'comd-tree-node',
    toggleBaseClassName: 'comd-tree-node-toggle',
    actionOverflowToggleBaseClassName: 'comd-tree-node-dropdown',
    actionOverflowMenuBaseClassName: 'comd-tree-node-dropdown-menu',
    classNames,
  },
};
