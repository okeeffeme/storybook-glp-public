import {ToggleState} from '@jotunheim/react-toggle';

import {NodeSelectedState, NodeSelectionMode} from './constants';

import {NodeId, TreeNode, TreeViewTexts} from './types';

const assertUnreachable = (val: never) => {
  throw new Error(
    `Did not expect to get here: Did you forget to handle the value ${val}?`
  );
};

export const defaultNodes = [];

export const defaultObject = {};

export const defaultTexts: TreeViewTexts = {
  selectWithChildren: 'Select with all children',
  unselectWithChildren: 'Unselect with all children',
};

export const isContained = (arr1: NodeId[] = [], arr2: NodeId[] = []) =>
  arr2.some((val) => arr1.includes(val));

export const toToggleState = (
  nodeSelectedState: NodeSelectedState
): ToggleState => {
  switch (nodeSelectedState) {
    case NodeSelectedState.off:
      return ToggleState.Off;
    case NodeSelectedState.on:
      return ToggleState.On;
    case NodeSelectedState.partial:
      return ToggleState.Indeterminate;
    default:
      return assertUnreachable(nodeSelectedState); // here we return a function that always throws which means all codepaths return something so TS is happy, plus all codePaths return something compatible with a value of type `ToggleState`.
  }
};

export const defaultGetNodeSelectedState = <T extends Record<string, unknown>>(
  node: TreeNode<T>,
  ids: NodeId[]
) => {
  return ids.includes(node.id) ? NodeSelectedState.on : NodeSelectedState.off;
};

export const defaultTrueCallback = () => true;

export const defaultFalseCallback = () => false;

export const defaultRenderNodeContents = <T extends Record<string, unknown>>(
  node: TreeNode<T>
) => node.label;

export const defaultGetNodeSelectionMode = () => NodeSelectionMode.none;

export const defaultUndefinedCallback = () => undefined;

export const defaultNullCallback = () => null;

export const defaultEmptyArrayCallback = () => [];

export const defaultNodeIsExpandable = <T extends Record<string, unknown>>(
  node: TreeNode<T>
) => !!node.children?.length;
