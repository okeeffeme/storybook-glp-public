export enum TreeMode {
  navigation,
  selection,
}

export enum TreeDensity {
  standard = 'standard',
  comfortable = 'comfortable',
  compact = 'compact',
}

export enum NodePosition {
  before = 'Before',
  after = 'After',
  inside = 'Inside',
}

export enum NodeSelectedState {
  on = 'on',
  off = 'off',
  partial = 'partial',
}

export enum NodeSelectionMode {
  none = 'none',
  single = 'single',
  multi = 'multi',
}

export enum NodeSelectorVisibility {
  hover = 'hover',
  always = 'always',
}
