import NavigationTree from './components/NavigationTree';
import SelectionTree from './components/SelectionTree';

// Types should be exported this way so that they are properly erased by webpack,
// otherwise we will get js errors.
export type {TreeNode, NodeId, NodeAction, DragInfo, DropInfo} from './types';

export {
  TreeDensity,
  NodePosition,
  NodeSelectedState,
  NodeSelectionMode,
  NodeSelectorVisibility,
} from './constants';

export {SelectionTree, NavigationTree};

export default NavigationTree;
