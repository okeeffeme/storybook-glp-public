import {createContext, ReactNode} from 'react';

import {TreeMode, NodeSelectedState, NodeSelectionMode} from './constants';

import {
  NodeId,
  DragInfo,
  DropInfo,
  TreeNode,
  NodeAction,
  TreeViewTexts,
  NodeCallbackBoolean,
} from './types';

import {
  defaultTexts,
  defaultEmptyArrayCallback,
  defaultFalseCallback,
  defaultGetNodeSelectedState,
  defaultGetNodeSelectionMode,
  defaultNullCallback,
  defaultTrueCallback,
  defaultUndefinedCallback,
} from './utils';

export type TreeState<T extends Record<string, unknown>> = {
  currentNodeIds: NodeId[];
  nodesByParentId: Record<NodeId, TreeNode<T>[] | undefined>;
  selectedNodeIds: NodeId[];
  highlightNodeIds: NodeId[];
};

export function getStateContext<T extends Record<string, unknown>>() {
  return createContext<TreeState<T>>({
    currentNodeIds: [],
    nodesByParentId: {},
    selectedNodeIds: [],
    highlightNodeIds: [],
  });
}

export type TreeSettings<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  mode: TreeMode;
  texts: TreeViewTexts;
  treeId: string;
  autoFocus: boolean;
  showDividers: boolean;
  isDragEnabled: boolean;
  isKeyboardNavigationEnabled: boolean;
  renderNodeIcon: (node: TreeNode<T>) => ReactNode;
  createNodeHref: (node: TreeNode<T>) => string | undefined;
  showChildSelection: NodeCallbackBoolean<T>;
  onChildSelectionToggled: (node: TreeNode<T>, toggleOn: boolean) => void;
  prepareDragInfo: (info: DragInfo, node: TreeNode<T>) => DragInfo<U>;
  onToggleNode: (node: TreeNode<T>) => void;
  getNodeSelectedState: (
    node: TreeNode<T>,
    selectedNodeIds: NodeId[]
  ) => NodeSelectedState;
  getNodeSelectionMode: (node: TreeNode<T>) => NodeSelectionMode;
  isNodeExpandable: NodeCallbackBoolean<T>;
  isNodeDeletable: NodeCallbackBoolean<T>;
  onNodeClicked: (node: TreeNode<T>) => void;
  onDeleteNodes: (nodeIds: NodeId[]) => void;
  onNodeSelectionToggled: (
    node: TreeNode<T>,
    parentId: NodeId,
    shiftKey: boolean
  ) => void;
  showNodeMenu: NodeCallbackBoolean<T>;
  isSelectorDisabled: NodeCallbackBoolean<T>;
  onNodesDropped: (info: DropInfo<U>) => void;
  onValidateDrop: (info: DropInfo<U>) => boolean;
  allowDropInside: NodeCallbackBoolean<T>;
  onGetNodeActions: (node: TreeNode<T>) => NodeAction[];
  renderNodeContents: (node: TreeNode<T>) => ReactNode;
  onValidateDragStart: NodeCallbackBoolean<T>;
  onContextMenuItemClick: (nodeAction: NodeAction, node: TreeNode<T>) => void;
};

export const defaultSettings = {
  mode: TreeMode.navigation,
  treeId: '',
  texts: defaultTexts,
  autoFocus: false,
  showDividers: true,
  isDragEnabled: false,
  isKeyboardNavigationEnabled: false,
  onToggleNode: defaultUndefinedCallback,
  showNodeMenu: defaultFalseCallback,
  createNodeHref: defaultUndefinedCallback,
  renderNodeIcon: defaultNullCallback,
  renderNodeContents: defaultNullCallback,
  prepareDragInfo: (defaultInfo) => defaultInfo,
  onNodeClicked: defaultUndefinedCallback,
  onDeleteNodes: defaultUndefinedCallback,
  onNodesDropped: defaultUndefinedCallback,
  onValidateDrop: defaultFalseCallback,
  onGetNodeActions: defaultEmptyArrayCallback,
  onValidateDragStart: defaultTrueCallback,
  onContextMenuItemClick: defaultUndefinedCallback,
  onNodeSelectionToggled: defaultUndefinedCallback,
  onChildSelectionToggled: defaultUndefinedCallback,
  allowDropInside: defaultFalseCallback,
  isNodeDeletable: defaultFalseCallback,
  isNodeExpandable: defaultFalseCallback,
  showChildSelection: defaultFalseCallback,
  isSelectorDisabled: defaultFalseCallback,
  getNodeSelectedState: defaultGetNodeSelectedState,
  getNodeSelectionMode: defaultGetNodeSelectionMode,
};

export function getSettingsContext<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>() {
  return createContext<TreeSettings<T, U>>(defaultSettings);
}
