import {ReactNode} from 'react';

import {
  TreeDensity,
  NodePosition,
  NodeSelectedState,
  NodeSelectionMode,
  NodeSelectorVisibility,
} from './constants';

export type NodeId = string | number;

export type TreeNode<
  AdditionalProps extends Record<string, unknown> = Record<string, unknown>
> = {
  id: NodeId;
  name?: string;
  type?: string;
  label?: string;
  isDeleted?: boolean;
  isExpanded?: boolean;
  children?: Array<TreeNode<AdditionalProps>>;
} & AdditionalProps;

export type NodeAction = {
  text: string;
  action: string;
  href?: string;
  iconPath?: string;
  header?: string;
  tooltip?: string;
  isDivider?: boolean;
  isDisabled?: boolean;
};

export type NodeCallbackBoolean<T extends Record<string, unknown>> = (
  node: TreeNode<T>
) => boolean;

export type TreeViewTexts = {
  selectWithChildren?: string;
  unselectWithChildren?: string;
};

export type CommonTreeProps<T extends Record<string, unknown>> = {
  texts?: TreeViewTexts;
  parentId?: NodeId;
  onToggleNode?: (node: TreeNode<T>) => void;
  selectedNodeIds?: NodeId[];
  isNodeExpandable?: NodeCallbackBoolean<T>;
  isSelectorDisabled?: NodeCallbackBoolean<T>;
  renderNodeContents?: (node: TreeNode<T>) => ReactNode;
  getNodeSelectedState?: (
    node: TreeNode<T>,
    selectedNodeIds: NodeId[]
  ) => NodeSelectedState;
  onNodeSelectionToggled?: (
    node: TreeNode<T>,
    parentId: NodeId,
    shiftKey: boolean
  ) => void;
} & (
  | {
      nodes: TreeNode<T>[];
      nodesByParentId?: undefined;
    }
  | {
      nodes?: undefined;
      nodesByParentId: Record<NodeId, TreeNode<T>[] | undefined>;
    }
);

export type DragInfo<
  AdditionalProps extends Record<string, unknown> = Record<string, unknown>
> = {
  nodeIds: NodeId[];
  sourceTreeId: string;
  dropData?: string;
} & AdditionalProps;

export type DropInfo<
  AdditionalProps extends Record<string, unknown> = Record<string, unknown>
> = DragInfo<AdditionalProps> & {
  targetNode: TreeNode;
  position: NodePosition;
};

export type NavigationTreeProps<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = {
  treeId?: string;
  density?: TreeDensity;
  autoFocus?: boolean;
  className?: string;
  showNodeMenu?: boolean | NodeCallbackBoolean<T>;
  isKeyboardNavigationEnabled?: boolean;
  onValidateDragStart?: NodeCallbackBoolean<T>;
  showDividers?: boolean;
  isDragEnabled?: boolean;
  currentNodeIds?: NodeId[];
  highlightNodeIds?: NodeId[];
  nodeSelectorVisibility?: NodeSelectorVisibility;
  getNodeSelectionMode?: (node: TreeNode<T>) => NodeSelectionMode;
  createNodeHref?: (node: TreeNode<T>) => string | undefined;
  renderNodeIcon?: (node: TreeNode<T>) => ReactNode;
  onGetNodeActions?: (node: TreeNode<T>) => NodeAction[];
  onNodeClicked?: (node: TreeNode<T>) => void;
  onDeleteNodes?: (nodeIds: NodeId[]) => void;
  onContextMenuItemClick?: (nodeAction: NodeAction, node: TreeNode<T>) => void;
  allowDropInside?: NodeCallbackBoolean<T>;
  isNodeDeletable?: NodeCallbackBoolean<T>;
} & (
  | {
      onValidateDrop?: (info: DropInfo<U>) => boolean;
      onNodesDropped?: (info: DropInfo<U>) => void;
      prepareDragInfo: (info: DragInfo, node: TreeNode<T>) => DragInfo<U>;
    }
  | {
      onValidateDrop?: (info: DropInfo) => boolean;
      onNodesDropped?: (info: DropInfo) => void;
      prepareDragInfo?: undefined;
    }
) &
  CommonTreeProps<T>;

export type SelectionTreeProps<T extends Record<string, unknown>> = {
  showChildSelection?: boolean | NodeCallbackBoolean<T>;
  onChildSelectionToggled?: (node: TreeNode<T>, toggleOn: boolean) => void;
} & CommonTreeProps<T>;
