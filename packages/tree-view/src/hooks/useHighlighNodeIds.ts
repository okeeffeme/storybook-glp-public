import React from 'react';
import {NodeId} from '..';

const STOP_HIGHLIGHT_DELAY = 3000;

export default (nodeIds: NodeId[]) => {
  const [nodeIdsToHighlight, setNodeIdsToHighlight] = React.useState(nodeIds);
  const hideNewTimer = React.useRef<ReturnType<typeof setTimeout>>();

  React.useEffect(() => {
    setNodeIdsToHighlight(nodeIds);

    hideNewTimer.current = setTimeout(() => {
      setNodeIdsToHighlight([]);
    }, STOP_HIGHLIGHT_DELAY);
    if (!hideNewTimer.current) return;

    const timeoutId = hideNewTimer.current;
    return () => clearTimeout(timeoutId);
  }, [nodeIds]);

  return nodeIdsToHighlight;
};
