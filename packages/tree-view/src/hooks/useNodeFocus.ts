import React from 'react';

import {TreeNode, NodeCallbackBoolean, NodeId} from '../types';
// import {KEY_CODES} from 'confirmit-utils/constants';

// TODO: move to confirmit-utils and replace in SD
const KEY_CODES = {
  ArrowLeft: 37,
  ArrowRight: 39,
  ArrowDown: 40,
  ArrowUp: 38,
  Delete: 46,
  Enter: 13,
  Space: 32,
  Escape: 27,
  Backspace: 8,
  Tab: 9,
  X: 88,
};

const focusPreviousNode = (node, nodeIndex, container) => {
  if (nodeIndex > -1) {
    // find previous sibling node
    const previousSiblingLiEl = container.childNodes[nodeIndex - 1];

    if (previousSiblingLiEl) {
      let lastChildLiEl = getLastLiElement(previousSiblingLiEl);
      let tempLiEl = lastChildLiEl;

      // some nodes could be expanded, recursively find last children
      while (tempLiEl) {
        tempLiEl = getLastLiElement(tempLiEl);
        if (tempLiEl) {
          lastChildLiEl = tempLiEl;
        }
      }

      if (lastChildLiEl) {
        return focusAndReturnNode(lastChildLiEl);
      } else {
        return focusAndReturnNode(previousSiblingLiEl);
      }
    } else {
      // when no previous sibling select parent node
      return focusParentNode(container);
    }
  }
};

const focusParentNode = (container) => {
  const parentLiEl = container.parentNode.parentNode;

  if (parentLiEl.tagName === 'LI') {
    return focusAndReturnNode(parentLiEl);
  }
};

const getLastLiElement = (currentLiElement) => {
  const olElement = currentLiElement.lastChild.firstChild;

  return olElement.tagName === 'OL' ? olElement.lastChild : null;
};

const focusAndReturnNode = (parent) => {
  const nextNode = parent.querySelector('[data-tree-node-id]');

  nextNode.focus();

  return nextNode.getAttribute('data-tree-node-id');
};

const focusNextNode = (node, nodeIndex, container, hasChildren) => {
  if (nodeIndex > -1) {
    // focus first children when node is expanded
    // when "Expand all" is used expanded prop seems to be true for all nodes, check that it has children
    if (node.isExpanded && hasChildren) {
      // focus first node in expanded container
      const currentLi = container.childNodes[nodeIndex];
      const olEl = currentLi.querySelector('ol[data-react-tree="tree-list"]');

      return focusAndReturnNode(olEl);
    }

    const nextSibling = container.childNodes[nodeIndex + 1];

    if (nextSibling) {
      return focusAndReturnNode(nextSibling);
    } else {
      return focusParentNextSibling(container);
    }
  }
};

const focusParentNextSibling = (container) => {
  let parentLi = container.parentNode.parentNode;

  while (parentLi && !parentLi.nextSibling) {
    parentLi = parentLi.parentNode.parentNode.parentNode; // "one" level up
    parentLi = parentLi.tagName === 'LI' ? parentLi : null;
  }

  if (parentLi) {
    const siblings = parentLi.parentNode.childNodes;
    const parentLiIndex = Array.prototype.indexOf.call(siblings, parentLi);

    if (parentLiIndex < siblings.length) {
      const parentNextSibling = siblings[parentLiIndex + 1];

      return focusAndReturnNode(parentNextSibling);
    }
  }
};

const treeNodeIsFocused = () =>
  document.activeElement &&
  document.activeElement.getAttribute('data-tree-node-id');

const focusNode = (node) => node && node.focus();

type Props<T extends Record<string, unknown>> = {
  parentId: NodeId;
  autoFocus?: boolean;
  currentNodeIds: NodeId[];
  selectedNodeIds: NodeId[];
  isNodeDeletable: NodeCallbackBoolean<T>;
  isNodeExpandable: NodeCallbackBoolean<T>;
  onToggleNode: (node: TreeNode<T>) => void;
  onNodeClicked: (node: TreeNode<T>) => void;
  onDeleteNodes: (nodeIds: NodeId[]) => void;
  onNodeSelectionToggled: (
    node: TreeNode<T>,
    parentId: NodeId,
    shiftKey: boolean
  ) => void;
};

const useNodeFocus = <T extends Record<string, unknown>>({
  parentId,
  autoFocus,
  currentNodeIds,
  selectedNodeIds,
  isNodeDeletable,
  isNodeExpandable,
  onToggleNode,
  onNodeClicked,
  onDeleteNodes,
  onNodeSelectionToggled,
}: Props<T>) => {
  const keyHandlingRef = React.useRef<HTMLOListElement>(null);
  const currentNodeId = React.useRef<NodeId>();

  const getNodeDom = (nodeId) =>
    keyHandlingRef.current?.querySelector(`[data-tree-node-id="${nodeId}"]`);

  React.useEffect(() => {
    if (currentNodeIds[0] !== currentNodeId.current) {
      currentNodeId.current = currentNodeIds[0];

      if (autoFocus && !treeNodeIsFocused()) {
        focusNode(getNodeDom(currentNodeId.current));
      }
    }
  }, [autoFocus, currentNodeIds]);

  const onNodeKeyDown = React.useCallback(
    (e, node, nodeIndex) => {
      const handleArrowDown = () =>
        focusNextNode(
          node,
          nodeIndex,
          keyHandlingRef.current,
          isNodeExpandable(node)
        );

      const handleArrowUp = () =>
        focusPreviousNode(node, nodeIndex, keyHandlingRef.current);

      const handleArrowRight = () => {
        if (!isNodeExpandable(node)) {
          return;
        }

        if (node.isExpanded) {
          // Pressing right on an expanded container should move focus to the first node inside the container
          handleArrowDown();
        } else {
          onToggleNode(node);
        }
      };

      const handleArrowLeft = () => {
        if (isNodeExpandable(node) && node.isExpanded) {
          onToggleNode(node);
        } else {
          // Pressing left on a node that is inside an expanded container should move focus to the container, without collapsing it.
          focusParentNode(keyHandlingRef.current);
        }
      };

      const handleEnterKey = () => {
        if (selectedNodeIds.length > 0) {
          onNodeSelectionToggled(node, parentId, false);
        } else {
          onNodeClicked(node);
        }
      };

      const handleDeleteKey = () => {
        if (!isNodeDeletable(node)) {
          return;
        }

        if (selectedNodeIds.length > 0) {
          onDeleteNodes(selectedNodeIds);
        } else {
          onDeleteNodes([node.id]);
        }
      };

      switch (e.which) {
        case KEY_CODES.ArrowDown:
          e.preventDefault();
          handleArrowDown();
          break;

        case KEY_CODES.Tab:
          {
            const handler = e.shiftKey ? handleArrowUp : handleArrowDown;
            if (handler()) {
              e.preventDefault(); // prevent only when nextNodeId is found otherwise let the browser focus element outside the tree
            }
          }
          break;

        case KEY_CODES.ArrowUp:
          e.preventDefault();
          handleArrowUp();
          break;

        case KEY_CODES.ArrowRight:
          e.preventDefault();
          handleArrowRight();
          break;

        case KEY_CODES.ArrowLeft:
          e.preventDefault();
          handleArrowLeft();
          break;

        case KEY_CODES.Delete:
          handleDeleteKey();
          break;

        case KEY_CODES.Space:
          e.preventDefault();
          onNodeSelectionToggled(node, parentId, false);
          break;

        case KEY_CODES.Enter:
          e.preventDefault();
          e.stopPropagation();

          handleEnterKey();
          break;
      }
    },
    [
      parentId,
      onToggleNode,
      onNodeClicked,
      onDeleteNodes,
      selectedNodeIds,
      isNodeDeletable,
      isNodeExpandable,
      onNodeSelectionToggled,
    ]
  );

  return {keyHandlingRef, onNodeKeyDown};
};

export default useNodeFocus;
