import React, {DragEvent} from 'react';

import {TreeSettings} from '../context';
import {NodePosition} from '../constants';
import {DragInfo, DropInfo, TreeNode} from '../types';

import {NodeProps} from '../components/Node';

const DROP_TIMEOUT = 10;
const AUTO_EXPAND_DELAY = 1000;
const LAST_NODE_SELECTOR =
  '[data-react-tree-is-root="true"] > [data-react-tree-li]:last-child';
const DROP_TARGET_ATTRIBUTE = 'data-react-tree-droptarget-position';

function getDefaultDragInfo(sourceTreeId = ''): DragInfo {
  return {
    nodeIds: [],
    sourceTreeId,
  };
}

let DRAG_INFO: DragInfo = getDefaultDragInfo();

let _expandTimer, _previousValidTarget, _previousHoverTarget;

export const clearDragInfo = () => (DRAG_INFO.nodeIds = []);

export const clearPreviousTarget = () => {
  clearTimeout(_expandTimer);

  if (_previousValidTarget) {
    _previousValidTarget.removeAttribute(DROP_TARGET_ATTRIBUTE);
  }

  _expandTimer = null;
  _previousHoverTarget = null;
  _previousValidTarget = null;
};

type Props<
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
> = Pick<
  NodeProps<T, U>,
  'isExpandable' | 'node' | 'parentId' | 'selectedNodeIds'
> &
  Pick<
    TreeSettings<T, U>,
    | 'allowDropInside'
    | 'onNodesDropped'
    | 'onToggleNode'
    | 'onValidateDragStart'
    | 'onValidateDrop'
    | 'prepareDragInfo'
    | 'treeId'
  >;

const useDragAndDrop = <
  T extends Record<string, unknown>,
  U extends Record<string, unknown>
>({
  node,
  treeId,
  parentId,
  onToggleNode,
  onNodesDropped,
  onValidateDrop,
  allowDropInside,
  selectedNodeIds,
  prepareDragInfo,
  isExpandable,
  onValidateDragStart,
}: Props<T, U>) => {
  const nodeRef = React.useRef<HTMLDivElement>(null);
  const itemRef = React.useRef<HTMLLIElement>(null);
  const dragImageContainer = React.useRef<HTMLDivElement>();

  const onDragStart = (e: DragEvent) => {
    e.stopPropagation();

    if (!onValidateDragStart(node)) {
      e.preventDefault();
      return;
    }

    DRAG_INFO = getDefaultDragInfo(treeId);

    if (selectedNodeIds.length > 0) {
      DRAG_INFO.nodeIds.push(...selectedNodeIds);
    } else {
      DRAG_INFO.nodeIds.push(node.id);
    }

    DRAG_INFO = prepareDragInfo(DRAG_INFO, node);

    e.dataTransfer.setData('Text', DRAG_INFO.dropData || '');
    e.dataTransfer.effectAllowed = 'copyMove';

    showDragImage(e);
  };

  const showDragImage = (e: DragEvent) => {
    if (!e.dataTransfer.setDragImage || !nodeRef.current) {
      return;
    }

    dragImageContainer.current = document.createElement('div');
    dragImageContainer.current.setAttribute(
      'data-react-tree-drag-clone',
      'true'
    );

    const count = selectedNodeIds.length;
    if (count > 0) {
      dragImageContainer.current.setAttribute(
        'data-react-tree-drag-count',
        String(count)
      );
    }

    const nodeToClone = nodeRef.current;
    const clonedNode = nodeToClone.cloneNode(true);
    dragImageContainer.current.appendChild(clonedNode);

    nodeToClone.parentNode?.insertBefore(
      dragImageContainer.current,
      nodeToClone
    );

    e.dataTransfer.setDragImage(dragImageContainer.current, 0, 0);
  };

  const onDragOver = (e: DragEvent) => {
    const dropPosition = getDropPosition(e);
    const isValidDrop = validateDrop(getDropInfo(dropPosition));

    e.preventDefault();
    e.stopPropagation();

    // the try/catch below is only necessary in the un-minified version of React
    // https://github.com/facebook/react/issues/5700
    try {
      e.dataTransfer.dropEffect = isValidDrop ? 'move' : 'none';
    } catch (ex) {
      // fails in Edge for some reason
    }

    if (getDragDropId(node) !== _previousHoverTarget) {
      clearPreviousTarget();

      _previousHoverTarget = getDragDropId(node);
    }

    if (isValidDrop) {
      setValidTarget();
      autoExpandIfInside(dropPosition);
      setDropPositionCssClass(dropPosition);
    } else {
      clearPreviousTarget();
    }
  };

  const getDragDropId = (node: TreeNode<T>) => `${node.id}_${treeId}`;

  const autoExpandIfInside = (dropPosition: NodePosition) => {
    if (dropPosition === NodePosition.inside) {
      if (!_expandTimer && isExpandable && !node.isExpanded) {
        _expandTimer = setTimeout(() => {
          onToggleNode(node);
        }, AUTO_EXPAND_DELAY);
      }
    } else {
      clearTimeout(_expandTimer);
      _expandTimer = null;
    }
  };

  const validateDrop = (dropInfo: DropInfo<U>) => {
    return DRAG_INFO.nodeIds.length > 0 && onValidateDrop(dropInfo);
  };

  const getDropPosition = (e: DragEvent) => {
    if (!nodeRef.current || !itemRef.current) {
      return NodePosition.after;
    }

    if (node.id === parentId) {
      return NodePosition.inside;
    }

    const height =
      (nodeRef.current &&
        parseFloat(
          getComputedStyle(nodeRef.current, null).height.replace('px', '')
        )) ||
      0;

    const posTop =
      nodeRef.current.getBoundingClientRect().top + document.body.scrollTop;

    const relativeClientY = e.clientY - posTop;

    const paddingBottom = parseInt(
      getComputedStyle(itemRef.current).paddingBottom || String(0),
      10
    );

    const isHoveringAtBottom = relativeClientY > height / 2 - 5;

    const isHoveringBeyondBottomOfLast = () => {
      if (!itemRef.current) return;

      const rect = itemRef.current.getBoundingClientRect();
      const top = rect.top + document.body.scrollTop;

      return (
        itemRef.current.matches(LAST_NODE_SELECTOR) &&
        itemRef.current.offsetHeight - (e.clientY - top) < paddingBottom
      );
    };

    if (isHoveringAtBottom) {
      if (allowDropInside(node) && !isHoveringBeyondBottomOfLast()) {
        return NodePosition.inside;
      }

      return NodePosition.after;
    }

    return NodePosition.before;
  };

  const setDropPositionCssClass = (dropPosition: NodePosition) => {
    const liElement = itemRef.current;
    if (!liElement) return;

    liElement.setAttribute(DROP_TARGET_ATTRIBUTE, dropPosition.toLowerCase());
  };

  const setValidTarget = () => (_previousValidTarget = itemRef.current);

  const onDragEnd = () => {
    if (dragImageContainer.current) {
      dragImageContainer.current.remove();
      dragImageContainer.current = undefined;
    }

    clearPreviousTarget();

    setTimeout(() => {
      clearDragInfo();
    }, DROP_TIMEOUT + DROP_TIMEOUT);
  };

  const onDrop = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();

    const dropPosition = getDropPosition(e);
    const dropInfo = getDropInfo(dropPosition);

    if (validateDrop(dropInfo)) {
      setTimeout(() => {
        onNodesDropped(dropInfo);
      }, DROP_TIMEOUT);
    }
  };

  const getDropInfo = (position: NodePosition): DropInfo<U> => ({
    ...(DRAG_INFO as DragInfo<U>),
    position,
    targetNode: node,
  });

  return {nodeRef, itemRef, onDragStart, onDragOver, onDragEnd, onDrop};
};

export default useDragAndDrop;
