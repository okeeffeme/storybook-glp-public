import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {TreeMode} from '../../src/constants';
import {TreeNode} from '../../src/types';
import Component from '../../src/components/Node';
import {theme, ThemeContext} from '../../../themes/src/index';
import {
  defaultSettings,
  getSettingsContext,
  getStateContext,
} from '../../src/context';
import {NodeSelectedState, NodeSelectionMode} from '../../src';

type MyNode = {
  numberOfChildren?: number;
} & TreeNode;

const StateContext = getStateContext<MyNode>();
const SettingsContext = getSettingsContext<MyNode, Record<string, unknown>>();

const render = (props = {}, settings = {}) =>
  renderRTL(
    <SettingsContext.Provider
      value={{
        ...defaultSettings,
        ...settings,
      }}>
      <Component
        {...defaultProps}
        {...props}
        stateContext={StateContext}
        settingsContext={SettingsContext}
      />
    </SettingsContext.Provider>,
    {
      wrapper: themeContextWrapper,
    }
  );

describe('Jotunheim React Tree View :: Node :: ', () => {
  it('should render arrow when node has children', function () {
    render();

    expect(screen.getByTestId('expand-toggle-button')).toBeInTheDocument();
  });

  it('should not render arrow when not expandable', function () {
    render({
      isExpandable: false,
    });

    expect(
      screen.queryByTestId('expand-toggle-button')
    ).not.toBeInTheDocument();
  });

  it('should render node menu when showNodeMenu is set to true', function () {
    render(undefined, {
      showNodeMenu: () => true,
    });

    expect(screen.getByTestId('node-dropdown')).toBeInTheDocument();
  });

  it('should not render node menu when showNodeMenu is set to false', function () {
    render(undefined, {
      showNodeMenu: () => false,
    });

    expect(screen.queryByTestId('node-dropdown')).not.toBeInTheDocument();
  });

  it('should be collapsed when isExpanded is false', function () {
    render({
      node: {
        ...defaultProps.node,
        isExpanded: false,
      },
    });

    expect(screen.getAllByTestId('tree-node-toggle').length).toBe(1);
  });

  it('should be expanded when isExpanded is true', function () {
    render({
      node: {
        ...defaultProps.node,
        isExpanded: true,
        children: [{id: 2, label: 'child 1'}],
      },
    });

    const treeNodeToggle = screen.getAllByTestId('tree-node-toggle');

    expect(treeNodeToggle.length).toBe(2);

    expect(treeNodeToggle[0]).not.toHaveAttribute(
      'data-react-tree-expand-toggle-button--is-expanded'
    );
    expect(treeNodeToggle[1]).not.toHaveAttribute(
      'data-react-tree-expand-toggle-button--is-expanded'
    );
  });

  it('should render checkbox when selectionMode is multiple', () => {
    render({
      node: {
        ...defaultProps.node,
        isExpanded: true,
        children: [{id: 2, label: 'child 1'}],
      },
      selectionMode: NodeSelectionMode.multi,
    });

    expect(screen.getAllByRole('checkbox').length).toBe(2);
  });

  it('should render radio when selectionMode is single', () => {
    render({
      node: {
        ...defaultProps.node,
        isExpanded: true,
        children: [{id: 2, label: 'child 1'}],
      },
      selectionMode: NodeSelectionMode.single,
    });

    expect(screen.getAllByRole('radio').length).toBe(2);
  });

  it('should not render checkbox/radio when selectionMode is none', () => {
    render({
      node: {
        ...defaultProps.node,
        isExpanded: true,
        children: [{id: 2, label: 'child 1'}],
      },
      selectionMode: NodeSelectionMode.none,
    });

    expect(screen.queryByRole('radio')).not.toBeInTheDocument();
    expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
  });

  it('should render selector as enabled when specified', () => {
    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        isSelectorDisabled() {
          return false;
        },
      }
    );

    expect(screen.getAllByRole('checkbox')[0]).toHaveAttribute(
      'aria-disabled',
      'false'
    );
  });

  it('should render selector as disabled when specified', () => {
    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        isSelectorDisabled() {
          return true;
        },
      }
    );

    expect(screen.getAllByRole('checkbox')[0]).toHaveAttribute(
      'aria-disabled',
      'true'
    );
  });

  it('should trigger onNodeSelectionToggled when checkbox is clicked', () => {
    const onNodeSelectionToggled = jest.fn();

    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        onNodeSelectionToggled,
        isSelectorDisabled() {
          return false;
        },
      }
    );

    userEvent.click(screen.getAllByRole('checkbox')[1]);

    expect(onNodeSelectionToggled).toHaveBeenCalled();
  });

  it('should not trigger onNodeSelectionToggled when clicking checkbox while disabled', () => {
    const onNodeSelectionToggled = jest.fn();

    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        onNodeSelectionToggled,
        isSelectorDisabled() {
          return true;
        },
      }
    );

    userEvent.click(screen.getAllByRole('checkbox')[1]);

    expect(onNodeSelectionToggled).not.toHaveBeenCalled();
  });

  it('should render dividers if specified', () => {
    render(undefined, {showDividers: true});

    expect(screen.getByTestId('tree-node')).toHaveAttribute(
      'data-tree-has-dividers',
      'true'
    );
  });

  it('should not render dividers if not specified', () => {
    render(undefined, {showDividers: false});

    expect(screen.getByTestId('tree-node')).toHaveAttribute(
      'data-tree-has-dividers',
      'false'
    );
  });

  it('should not render child selection by default', () => {
    render();

    expect(screen.queryByTestId('child-selection')).not.toBeInTheDocument();
  });

  it('should render child selection when specified', () => {
    render(undefined, {
      showChildSelection: () => true,
    });

    expect(screen.getByTestId('child-selection')).toBeInTheDocument();
  });

  it('should not render child selection when specified if node is not expandable', () => {
    render(
      {
        isExpandable: false,
      },
      {
        showChildSelection: () => true,
      }
    );

    expect(screen.queryByTestId('child-selection')).not.toBeInTheDocument();
  });

  it('should render multi selection selector for Navigation tree', () => {
    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        mode: TreeMode.navigation,
      }
    );

    expect(
      screen
        .getByTestId('node-selector')
        .classList.contains('comd-tree-node__multiselection-selector')
    ).toBe(true);
  });

  it('should render multi selection selector for Selection tree', () => {
    render(
      {
        selectionMode: NodeSelectionMode.multi,
      },
      {
        mode: TreeMode.selection,
      }
    );

    expect(
      screen
        .getByTestId('node-selector')
        .classList.contains('comd-tree-node__multiselection-selector')
    ).toBe(false);
  });

  it('should render NodeLink component for Navigation tree', () => {
    render(undefined, {
      mode: TreeMode.navigation,
    });

    expect(screen.getByTestId('node-link')).toBeInTheDocument();
  });

  it('should not render NodeLink component for Navigation tree', () => {
    render(undefined, {
      mode: TreeMode.selection,
    });

    expect(screen.queryByTestId('node-link')).not.toBeInTheDocument();
  });
});

const themeContextWrapper = ({children}) => (
  <ThemeContext.Provider value={theme.themeNames.material}>
    {children}
  </ThemeContext.Provider>
);

const defaultProps = {
  node: {
    id: 1,
    type: 'container',
    label: 'container',
    numberOfChildren: 1,
    isExpanded: false,
  },
  nodeIndex: 0,
  parentId: 'root',
  className: 'tree-node',
  isCurrent: false,
  isHighlighted: false,
  selectedState: NodeSelectedState.off,
  selectedNodeIds: [],
  isParentDeleted: false,
  isMultiSelecting: false,
  onNodeKeyDown: () => {},
  selectionMode: NodeSelectionMode.multi,
  isExpandable: true,
};
