import React from 'react';
import PropTypes from 'prop-types';
import {mount} from 'enzyme';
import {act} from 'react-dom/test-utils';

import {theme, ThemeContext} from '../../../themes/src';
import TreeView, {NodeSelectorVisibility} from '../../src';
import Node from '../../src/components/Node';

const EXPANDABLE_NODE_ID = '4';
const NON_EXPANDABLE_NODE_ID = '6';

describe('Jotunheim React Tree :: Tree View :: ', () => {
  it('should add "comd-tree-list--is-drag-enabled" class to root tree if isDragEnabled is true', () => {
    const tree = render({
      isDragEnabled: true,
    });

    const rootTree = tree.find('[data-react-tree-is-root="true"]');

    expect(rootTree.hasClass('comd-tree-list--is-drag-enabled')).toBe(true);
  });

  it('should not add "comd-tree-list--is-drag-enabled" class to root tree if isDragEnabled is false', () => {
    const tree = render({
      isDragEnabled: false,
    });

    const rootTree = tree.find('[data-react-tree-is-root="true"]');

    expect(rootTree.hasClass('comd-tree-list--is-drag-enabled')).toBe(false);
  });

  it('should not add "comd-tree-list--is-drag-enabled" to non-root tree, even when isDragEnabled is true', () => {
    const tree = render({
      isDragEnabled: true,
    });

    const nonRootTree = tree.find('[data-react-tree-is-root="false"]');

    expect(nonRootTree.hasClass('comd-tree-list--is-drag-enabled')).toBe(false);
  });

  it('should pass isDragEnabled=true to Node', () => {
    const tree = render({
      isDragEnabled: true,
    });

    const node = tree.find(Node);

    expect(
      node.at(0).find('.comd-tree-node').hostNodes().prop('draggable')
    ).toBe(true);
  });

  it('should pass isDragEnabled=false to Node', () => {
    const tree = render({
      isDragEnabled: false,
    });

    const node = tree.find(Node);

    expect(
      node.at(0).find('.comd-tree-node').hostNodes().prop('draggable')
    ).toBe(false);
  });

  it('should show selectors when some nodes are selected', function () {
    const tree = render({
      selectedNodeIds: ['1'],
    });

    expect(
      tree.find('[data-react-tree-is-selector-visible=true]').exists()
    ).toBe(true);
  });

  it('should not show selectors when no nodes are selected', function () {
    const tree = render({
      selectedNodeIds: [],
    });

    expect(
      tree.find('[data-react-tree-is-selector-visible=true]').exists()
    ).toBe(false);
  });

  it('should show selectors when no nodes are selected but set to always show', function () {
    const tree = render({
      selectedNodeIds: [],
      nodeSelectorVisibility: NodeSelectorVisibility.always,
    });

    expect(
      tree.find('[data-react-tree-is-selector-visible=true]').exists()
    ).toBe(true);
  });

  it('should not render a current node by default', function () {
    const tree = render({});

    expect(tree.find('.comd-tree-node--is-current').length).toBe(0);
  });

  it('should render a current node when currentNodeIDs is set', function () {
    const tree = render({
      currentNodeIds: ['1'],
    });

    expect(tree.find('.comd-tree-node--is-current').length).toBe(1);
  });

  it('should set correct data in dataTransfer when drag of node is started', function () {
    act(() => {
      const tree = render({
        nodes: [{id: '1', name: 'q1'}],
        onValidateDragStart() {
          return true;
        },
        prepareDragInfo(dragInfo, node) {
          dragInfo.dropData = `^f('${node.name}')^`;

          return dragInfo;
        },
      });

      const event = {
        dataTransfer: {
          setData: jest.fn(),
        },
      };

      tree.find('[data-tree-node-id="1"]').simulate('dragStart', event);

      expect(event.dataTransfer.setData).toHaveBeenCalledWith(
        'Text',
        `^f('q1')^`
      );

      tree.unmount();
    });
  });

  it('should not set any data in dataTransfer when drag is not allowed', function () {
    act(() => {
      const props = {
        nodes: [{id: '1', name: 'q1'}],
        prepareDragInfo: jest.fn(),
        onValidateDragStart() {
          return false;
        },
      };
      const tree = render(props);

      const event = {
        dataTransfer: {
          setData: jest.fn(),
        },
      };

      tree.find('[data-tree-node-id="1"]').simulate('dragStart', event);

      expect(event.dataTransfer.setData).not.toHaveBeenCalled();

      tree.unmount();
    });
  });

  it('should set dragging effect when valid drag is started', function () {
    act(() => {
      const tree = render({
        nodes: [{id: '1', name: 'q1'}],
        onValidateDragStart() {
          return true;
        },
      });

      const event = {
        dataTransfer: {
          setData: jest.fn(),
          effectAllowed: '',
        },
      };

      const node = tree.find('[data-tree-node-id="1"]');

      node.simulate('dragStart', event);

      expect(event.dataTransfer.effectAllowed).toBe('copyMove');

      tree.unmount();
    });
  });

  it('should not set dragging effect when drag is not allowed', function () {
    act(() => {
      const tree = render({
        nodes: [{id: '1', name: 'q1'}],
        onValidateDragStart() {
          return false;
        },
      });

      const event = {
        dataTransfer: {
          setData: jest.fn(),
          effectAllowed: undefined,
        },
      };

      const node = tree.find('[data-tree-node-id="1"]');

      node.simulate('dragStart', event);

      expect(event.dataTransfer.effectAllowed).toBe(undefined);

      tree.unmount();
    });
  });

  it('should set dropEffect=move when hovering valid target', function () {
    act(() => {
      const tree = render({
        onValidateDragStart() {
          return true;
        },
        onValidateDrop() {
          return true;
        },
      });

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find('[data-tree-node-id="2"]');

      const event = {
        dataTransfer: {
          setData() {},
          dropEffect: '',
        },
      };

      node.simulate('dragStart', event);
      target.simulate('dragEnter', event);
      target.simulate('dragOver', event);

      expect(event.dataTransfer.dropEffect).toBe('move');
      expect(
        target
          .parents()
          .first()
          .getDOMNode()
          .getAttribute('data-react-tree-droptarget-position')
      ).toBe('before');

      tree.unmount();
    });
  });

  it('should set dropEffect=none when hovering invalid target', function () {
    act(() => {
      const tree = render({
        onValidateDragStart() {
          return true;
        },
        onValidateDrop() {
          return false;
        },
      });

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find('[data-tree-node-id="2"]');

      const event = {
        dataTransfer: {
          setData() {},
          dropEffect: '',
        },
      };

      node.simulate('dragStart', event);
      target.simulate('dragEnter', event);
      target.simulate('dragOver', event);

      expect(event.dataTransfer.dropEffect).toBe('none');

      expect(
        target
          .parents()
          .first()
          .getDOMNode()
          .getAttribute('data-react-tree-droptarget-position')
      ).toBe(null);

      tree.unmount();
    });
  });

  it('should automatically expand when hovering expandable node', function () {
    act(() => {
      const props = {
        parentId: EXPANDABLE_NODE_ID, // using the parentId to trigger the dropPosition being "inside"
        onValidateDrop() {
          return true;
        },
        onValidateDragStart() {
          return true;
        },
        allowDropInside() {
          return true;
        },
        onToggleNode: jest.fn(),
      };

      const tree = render(props);

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find(`[data-tree-node-id="${EXPANDABLE_NODE_ID}"]`);

      const event = {
        dataTransfer: {
          setData() {},
        },
      };

      jest.useFakeTimers();

      expect(props.onToggleNode).not.toHaveBeenCalled();

      node.simulate('dragStart', event);
      target.simulate('dragEnter', event);

      jest.runAllTimers();

      expect(props.onToggleNode).toHaveBeenCalled();

      tree.unmount();
    });
  });

  it('should not automatically expand when hovering non-expandable node', function () {
    act(() => {
      const props = {
        onValidateDragStart() {
          return true;
        },
        onValidateDrop() {
          return true;
        },
        onToggleNode: jest.fn(),
      };

      const tree = render(props);

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find(
        `[data-tree-node-id="${NON_EXPANDABLE_NODE_ID}"]`
      );

      const event = {
        dataTransfer: {
          setData() {},
        },
      };

      jest.useFakeTimers();

      node.simulate('dragStart', event);
      target.simulate('dragEnter', event);

      jest.runAllTimers();

      expect(props.onToggleNode).not.toHaveBeenCalled();

      tree.unmount();
    });
  });

  it('should not automatically expand when hovering expandable node, then hovering out (mouseLeave)', function () {
    act(() => {
      const props = {
        onValidateDragStart() {
          return true;
        },
        onValidateDrop() {
          return true;
        },
        onToggleNode: jest.fn(),
      };

      const tree = render(props);

      const node = tree.find('[data-tree-node-id="1"]');
      const target1 = tree.find(`[data-tree-node-id="${EXPANDABLE_NODE_ID}"]`);
      const target2 = tree.find(
        `[data-tree-node-id="${NON_EXPANDABLE_NODE_ID}"]`
      );

      const event = {
        dataTransfer: {
          setData() {},
        },
      };

      jest.useFakeTimers();

      node.simulate('dragStart', event);

      target1.simulate('dragEnter', event);
      target1.simulate('dragOver', event);

      target2.simulate('dragEnter', event);
      target2.simulate('dragOver', event);

      jest.runAllTimers();

      expect(props.onToggleNode).not.toHaveBeenCalled();

      tree.unmount();
    });
  });

  it('should trigger drop when valid drop has occurred', function () {
    act(() => {
      const props = {
        onValidateDragStart() {
          return true;
        },
        onValidateDrop: jest.fn(() => {
          return true;
        }),
        onNodesDropped: jest.fn((dropInfo) => {
          expect(dropInfo.nodeIds).toEqual(['1']);
          expect(dropInfo.targetNode.id).toBe('2');
        }),
      };

      const tree = render(props);

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find('[data-tree-node-id="2"]');

      const event = {
        dataTransfer: {
          setData() {},
        },
      };

      jest.useFakeTimers();

      node.simulate('dragStart', event);

      target.simulate('dragEnter', event);
      target.simulate('dragOver', event);
      target.simulate('drop', event);

      jest.runAllTimers();

      expect(props.onNodesDropped).toHaveBeenCalled();
      expect(props.onValidateDrop).toHaveBeenCalled();

      tree.unmount();
    });
  });

  it('should not trigger drop when invalid drop has occurred', function () {
    act(() => {
      const props = {
        onValidateDragStart() {
          return true;
        },
        onValidateDrop: jest.fn(() => {
          return false;
        }),
        onNodesDropped: jest.fn(),
      };

      const tree = render(props);

      const node = tree.find('[data-tree-node-id="1"]');
      const target = tree.find('[data-tree-node-id="2"]');

      const event = {
        dataTransfer: {
          setData() {},
        },
      };

      jest.useFakeTimers();

      node.simulate('dragStart', event);

      target.simulate('dragEnter', event);
      target.simulate('dragOver', event);
      target.simulate('drop', event);

      jest.runAllTimers();

      expect(props.onNodesDropped).not.toHaveBeenCalled();
      expect(props.onValidateDrop).toHaveBeenCalled();

      tree.unmount();
    });
  });

  describe('showNodeMenu :: ', () => {
    it('should show node menu for all visible (expanded) nodes when showNodeMenu is true', () => {
      const tree = render({
        showNodeMenu: true,
      });

      const visibleNodesCount = 6;

      expect(
        tree.find('[data-react-tree-action-overflow]').hostNodes().length
      ).toBe(visibleNodesCount);
    });

    it('should not show node menu when showNodeMenu is false', () => {
      const tree = render({
        showNodeMenu: false,
      });

      expect(tree.find('[data-react-tree-action-overflow]').exists()).toBe(
        false
      );
    });

    it('should not show node menu when showNodeMenu is undefined', () => {
      const tree = render({
        showNodeMenu: undefined,
      });

      expect(tree.find('[data-react-tree-action-overflow]').exists()).toBe(
        false
      );
    });

    it('should show node menu when showNodeMenu is a function that returns true for all nodes', () => {
      const tree = render({
        showNodeMenu: () => true,
      });

      const visibleNodesCount = 6;

      expect(
        tree.find('[data-react-tree-action-overflow]').hostNodes().length
      ).toBe(visibleNodesCount);
    });

    it('should show node menu when showNodeMenu is a function that returns true for node id 1 and 2', () => {
      const tree = render({
        showNodeMenu: (node) => {
          if (node.id === '1' || node.id === '2') {
            return true;
          }
          return false;
        },
      });

      const menuCount = 2;

      expect(
        tree.find('[data-react-tree-action-overflow]').hostNodes().length
      ).toBe(menuCount);
    });

    it('should not show node menu when showNodeMenu is a function that returns false for all nodes', () => {
      const tree = render({
        showNodeMenu: () => false,
      });

      expect(tree.find('[data-react-tree-action-overflow]').exists()).toBe(
        false
      );
    });
  });
});

const defaultProps = {
  isNodeExpandable: (node) => node.numberOfChildren > 0,
  showNodeMenu: false,
  nodes: [
    {
      id: '1',
      type: 'directive',
      label: 'multiple questions per page',
      isDeleted: true,
    },
    {
      id: '2',
      type: 'pagebreak',
      label: 'one question per page',
      selected: false,
    },
    {
      id: '3',
      type: 'container',
      label: 'container',
      numberOfChildren: 1,
      isExpanded: false,
      children: [
        {
          id: '99',
          type: 'single',
          label: 'single',
          numberOfChildren: 0,
        },
      ],
    },
    {
      id: '4',
      type: 'container',
      label: 'container',
      numberOfChildren: 4,
      isExpanded: false,
    },
    {
      id: '5',
      type: 'folder',
      label: 'folder',
      numberOfChildren: 1,
      isExpanded: true,
      children: [
        {
          id: '6',
          type: 'single',
          label: 'single',
          numberOfChildren: 0,
        },
      ],
    },
  ],
};

const render = (props) => {
  return mount(<TreeView {...defaultProps} {...props} />, {
    wrappingComponent: themeContextWrapper,
  });
};

const themeContextWrapper = ({children}) => (
  <ThemeContext.Provider value={theme.themeNames.material}>
    {children}
  </ThemeContext.Provider>
);

themeContextWrapper.propTypes = {
  children: PropTypes.any,
};
