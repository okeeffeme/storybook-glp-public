# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.36&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.37&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.36&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.36&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.34&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.35&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.33&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.34&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.32&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.33&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.31&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.32&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.30&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.31&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.29&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.30&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.28&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.29&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.27&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.28&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.26&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.27&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.25&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.26&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.24&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.25&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.23&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.24&targetRepoId=1246) (2023-02-07)

### Bug Fixes

- add test ids to Select component ([NPM-1209](https://jiraosl.firmglobal.com/browse/NPM-1209)) ([95c84ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/95c84ffde7f2dfa167a161913f5694a757e49c43))

## [10.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.22&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.23&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.21&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.22&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.20&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.21&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.19&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.20&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.18&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.19&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.17&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.18&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.13&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.17&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- add expendable handler for all components within tree-view storybook ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([ca8c222](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ca8c222b107b4496173faea865053321f3d8bd06))
- add test ids for tree-view's components ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([73d441e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/73d441e696948820c90ef33010a44ed260d133c2))

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.13&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.16&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- add expendable handler for all components within tree-view storybook ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([ca8c222](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ca8c222b107b4496173faea865053321f3d8bd06))
- add test ids for tree-view's components ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([73d441e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/73d441e696948820c90ef33010a44ed260d133c2))

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.13&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.15&targetRepoId=1246) (2023-01-18)

### Bug Fixes

- add expendable handler for all components within tree-view storybook ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([ca8c222](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ca8c222b107b4496173faea865053321f3d8bd06))
- add test ids for tree-view's components ([NPM-1220](https://jiraosl.firmglobal.com/browse/NPM-1220)) ([73d441e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/73d441e696948820c90ef33010a44ed260d133c2))

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.13&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.12&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.11&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.10&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.8&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.8&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.5&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.5&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.5&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.4&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.5&targetRepoId=1246) (2022-11-23)

### Bug Fixes

- cast ChangeEvent to a MouseEvent in Toggle.onChange ([NPM-1102](https://jiraosl.firmglobal.com/browse/NPM-1102)) ([0852162](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/08521628439619dc8ba190b11946cdb1d2ba4ee5))

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.3&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.2&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@10.0.0&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-tree-view

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.13&sourceBranch=refs/tags/@jotunheim/react-tree-view@10.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Tree ([NPM-962](https://jiraosl.firmglobal.com/browse/NPM-962)) ([be7f242](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/be7f242de512357485705937d821091cd821652a))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.12&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.11&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.10&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.9&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.8&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.6&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.3&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.2&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.1&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-tree-view

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tree-view@9.0.0&sourceBranch=refs/tags/@jotunheim/react-tree-view@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-tree-view

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.15&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.16&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.14&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.15&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.13&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.14&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- update Dropdown usage to remove classNames ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([5d61629](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5d61629ca8cbf2cd4416f4a5449cde17211edf85))
- update usages of Dropdown ([NPM-972](https://jiraosl.firmglobal.com/browse/NPM-972)) ([d4788da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d4788da8098f822eef8fc5c6c77489af8836cc00))

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.12&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.13&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.11&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.12&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.10&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.11&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.8&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.9&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.7&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.8&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.6&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.7&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.5&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.6&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.4&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.5&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-tree-view

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@8.0.3&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.4&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-tree-view

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.8&sourceBranch=refs/tags/@confirmit/react-tree-view@8.0.0&targetRepoId=1246) (2022-02-11)

### BREAKING CHANGES

- remove deprecated "icon" prop from node menu actions (NPM-858)

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.7&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.8&targetRepoId=1246) (2022-02-08)

### Bug Fixes

- Correcting alignment of checkboxes in selection-tree ([NPM-965](https://jiraosl.firmglobal.com/browse/NPM-965)) ([6ef89d1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ef89d10503310787b17892037413f37a7f4f15d))

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.6&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.7&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-tree-view

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.5&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.6&targetRepoId=1246) (2022-01-28)

### Bug Fixes

- Add data attributes for child selection ([NPM-916](https://jiraosl.firmglobal.com/browse/NPM-916)) ([70139ed](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70139ed7783b6b9acc955b4048a327a0357439fb))

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.4&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.5&targetRepoId=1246) (2022-01-25)

### Bug Fixes

- Add 'mixed' value for aria-checked in NodeSelector ([NPM-916](https://jiraosl.firmglobal.com/browse/NPM-916)) ([c50b7ae](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c50b7aedaf949611fc5553d9b3e525d6a5dd0775))

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.3&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.4&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-tree-view

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.2&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.3&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-tree-view

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.1&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.2&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-tree-view

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.1.0&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.1&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-tree-view

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.0.2&sourceBranch=refs/tags/@confirmit/react-tree-view@7.1.0&targetRepoId=1246) (2021-12-07)

### Features

- add truncation of long texts in TreeView ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([bc5d6bb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bc5d6bb247292e7c80b8b3a9f2957c75011e6da8))

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.0.1&sourceBranch=refs/tags/@confirmit/react-tree-view@7.0.2&targetRepoId=1246) (2021-11-30)

### Bug Fixes

- Expose types DragInfo and DropInfo ([NPM-895](https://jiraosl.firmglobal.com/browse/NPM-895)) ([fb83310](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb83310bf6a669b7f57110bc3fdfeed6155bdc2a))

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@7.0.0&sourceBranch=refs/tags/@confirmit/react-tree-view@7.0.1&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-tree-view

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.8&sourceBranch=refs/tags/@confirmit/react-tree-view@7.0.0&targetRepoId=1246) (2021-11-23)

### Bug Fixes

- make sure either "nodes" or "nodesByParentId" is required, and also that you cannot supply both ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([19d51d9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/19d51d90543f9a67c50346448ee9f0717937603e))
- reduce padding when no nodes on root are expandable or selectable ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([11dee61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/11dee61ba452e056c0cc94393e0d25ef261034ae))
- rename APP_CAPS enums to PascalCase ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([6997cbc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6997cbc62a6a2be17683778496bb6cd0e259ecd6))
- show radio button when selection mode is single ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([aa30f89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/aa30f89b01c41a5727d7da76864e4f0d88fab147))

### Features

- add ability to select/unselect all children ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([0d78000](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0d780003ee6a702b637f22979af68e66469f6e64))
- add ability to set the tooltips of the select-all/unselect-all buttons ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([e0b273e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e0b273e7adf7846d2474a3c4268164f33425718e))
- Convert to TypeScript, add support for new SelectionTree component ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([9778b79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9778b7921d20d5746e1a2c711b88e0a9cc0a0db6))
- introduce a "mode" property, to choose between "selection" mode and "navigation" mode (the default, which is the only current behavior) ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([b77b8fa](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b77b8fa99e0d3db59d25a7d2da95939c74df1576))

### Reverts

- Revert "NPM-739 Test type checking without tsc in pre-push hook" ([645fe44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/645fe4430e0cb020035e5e961d978366e3336ed0))

### BREAKING CHANGES

- rename APP_CAPS enums to PascalCase (NPM-739)

## [6.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.7&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.8&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [6.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.6&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.7&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.4&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.5&targetRepoId=1246) (2021-09-27)

### Bug Fixes

- tree not passing state of shiftKey ([NPM-874](https://jiraosl.firmglobal.com/browse/NPM-874)) ([61b2e19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/61b2e19273920571b9f0404fa5e1341b946ad386))

## [6.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.3&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.4&targetRepoId=1246) (2021-09-20)

### Bug Fixes

- use shared Checkbox component in the node-selector. properly support getNodeSelectedState in the whole tree. ([NPM-453](https://jiraosl.firmglobal.com/browse/NPM-453)) ([87fff3e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/87fff3e65f51bb94533085229a14e1ab7b5a0df4))

## [6.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.2&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.1&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.2&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.4.0&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-tree-view

# [6.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.12&sourceBranch=refs/tags/@confirmit/react-tree-view@6.4.0&targetRepoId=1246) (2021-09-13)

### Features

- support iconPath on node actions menu. This deprecates icon property, and it will be removed later. ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([ccb03ad](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ccb03adf422dba8a37b4c2659d3d8118057a2605))

## [6.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.11&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.12&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.10&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.11&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.9&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.10&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.8&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.9&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.7&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.8&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.6&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.7&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.5&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.6&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.4&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.5&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.3&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.4&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.2&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.1&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.2&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.3.0&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.1&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-tree-view

# [6.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.7&sourceBranch=refs/tags/@confirmit/react-tree-view@6.3.0&targetRepoId=1246) (2021-06-15)

### Features

- Add ability to turn off dividers between nodes ([NPM-800](https://jiraosl.firmglobal.com/browse/NPM-800)) ([2f9413e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2f9413ecf412e2a15af7a3b28dff27fcc2bf0adc))

## [6.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.6&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.7&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.5&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.6&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.4&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.5&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.3&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.4&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.2&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.3&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.2.1&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.2&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-tree-view

# [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.10&sourceBranch=refs/tags/@confirmit/react-tree-view@6.2.0&targetRepoId=1246) (2021-05-05)

### Features

- Added information icon showing tooltip for menu options in tree view ([NPM-775](https://jiraosl.firmglobal.com/browse/NPM-775)) ([5cb4fcd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5cb4fcd7de6e83ccf0bf1d73da40ea974190f55d))

## [6.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.9&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.10&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.8&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.9&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.7&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.8&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [6.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.6&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.7&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.5&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.6&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.4&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.3&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.4&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.2&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.3&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.1&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.2&targetRepoId=1246) (2021-02-22)

### Bug Fixes

- ensure is-drag-enabled class modifier is added only to root tree ([NPM-728](https://jiraosl.firmglobal.com/browse/NPM-728)) ([88d2633](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/88d2633cfb712aad0dc2a56967ddfd60a622dd5b))
- pass isDragEnabled to Node ([NPM-728](https://jiraosl.firmglobal.com/browse/NPM-728)) ([ad31b8e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ad31b8edd7169f2752d6f3fdddd8d31ca1e1b446))

## [6.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.1.0&sourceBranch=refs/tags/@confirmit/react-tree-view@6.1.1&targetRepoId=1246) (2021-02-19)

### Bug Fixes

- add default value to showNodeMenu to avoid breaking if this value is not set ([NPM-726](https://jiraosl.firmglobal.com/browse/NPM-726)) ([823a986](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/823a9868379add4df98f4ae73aef17cb893b93be))

### v6.1.0

- **FEATURE**:
  - 'showNodeMenu' prop support type func along with bool

## [6.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.23&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.24&targetRepoId=1246) (2021-02-18)

### Bug Fixes

- only apply bottom padding to the tree when dragging is enabled ([NPM-724](https://jiraosl.firmglobal.com/browse/NPM-724)) ([eaba8cf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eaba8cfeecd786719c77250d52d48e550ddcd319))
- only apply bottom padding to the tree when dragging is enabled ([NPM-724](https://jiraosl.firmglobal.com/browse/NPM-724)) ([fcb2025](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fcb202522b74dfbe9eac352c13bfbea1d2d05f48))

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.21&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.22&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.19&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.20&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.18&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.19&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.17&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.18&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.16&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.17&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.15&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.16&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.14&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.15&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.13&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.12&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.11&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.10&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.9&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.6&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.3&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.2&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-tree-view

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@6.0.1&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-tree-view

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.26&sourceBranch=refs/tags/@confirmit/react-tree-view@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([a0eda33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a0eda334e6769e7ba022b654a8970577171b7bf6))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

## [5.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.25&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.26&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.24&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.25&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.21&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.22&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.20&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.21&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.19&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.20&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.16&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.17&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.14&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.15&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tree-view@5.1.12&sourceBranch=refs/tags/@confirmit/react-tree-view@5.1.13&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.11](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.1.10...@confirmit/react-tree-view@5.1.11) (2020-09-08)

### Bug Fixes

- make sure `img` elements as NodeIcon are sized properly when tree density is changed

## [5.1.10](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.1.9...@confirmit/react-tree-view@5.1.10) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.8](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.1.7...@confirmit/react-tree-view@5.1.8) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.1.5...@confirmit/react-tree-view@5.1.6) (2020-08-27)

### Bug Fixes

- allow options menu in tree-view to render outside scrollparent to not interfere/be in the way of tree nodes ([NPM-508](https://jiraosl.firmglobal.com/browse/NPM-508)) ([52bf28e](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/52bf28e833ec4f6352a5cb769c80ee18be153564))

## [5.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.1.0...@confirmit/react-tree-view@5.1.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-tree-view

## [5.1.0]

- Feat: Ability to disable node-selectors (checkbox/radio) via "isSelectorDisabled" callback
- Feat: Ability to always show the node-selectors (instead of only after selecting first) via a new "nodeSelectorVisibility" prop

## [5.0.13](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tree-view@5.0.12...@confirmit/react-tree-view@5.0.13) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-tree-view

### v5.0.0

- **BREAKING**:
  - Refactored node-selector to just show svg, no underlying label/input as this interfered with click events and react-select

### v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v3.0.0

- **BREAKING**:
  - Renamed isNodeSelectable callback to getNodeSelectionMode, to specify which selection-mode to show (single/multi/none). Defaults to none.
  - Exporting new NODE_SELECTION_MODE enum, to be used with the getNodeSelectionMode callback.

### v2.1.0

- Removing package version in class names.

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.0.0

- **BREAKING** - Removed props: listBaseClassName, classNames

### v0.0.17

- Refactor: update internal components using poppers to use `placement` instead of `defaultPlacement` prop

### v0.0.10

- Bugfix: Drag/drop in Edge and IE11 did not work, because dragging buttons just doesnt work

### v0.0.9

- Bugfix: Toggle button misaligned in Firefox and IE (was fixed on incorrect element in v0.0.7)
- Bugfix: Drag/drop on FF did not work
- Bugfix: Remove redundant useCallback

### v0.0.7

- Bugfix: Toggle button misaligned in Firefox and IE

### v0.0.5

- Bugfix: Fixed overflow menu by enabling preventOverflow option. When this was disabled, it could cause menu to be shown outside viewport (making some elements unreachable), even when there was space available in viewport to render entire menu. This also seems to fix the previous issue where the overflow menu was causing horizontal scrolling when tree was rendered on the right edge of the viewport.
- Bugfix: Add 2px padding on left side of text on tree nodes
- Bugfix: Add correct colors for pulse effect for new nodes

### v0.0.4

- Bugfix: Fixed tabbing between nodes

### v0.0.1

- Initial version
