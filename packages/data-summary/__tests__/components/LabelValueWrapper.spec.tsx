import React from 'react';
import {render, screen} from '@testing-library/react';

import LabelValueWrapper from '../../src/components/LabelValueWrapper';
import {ItemSize} from '../../src';

describe('Jotunheim React Data Summary Item :: LabelValueWrapper ', () => {
  const label = 'Test Label';
  const value = 'Test Value';

  it('should render label and value', () => {
    render(<LabelValueWrapper label={label} value={value} />);

    expect(screen.getByTestId('label').textContent).toEqual(label);
    expect(screen.getByTestId('value').textContent).toEqual(value);
  });

  it('should add size-large class when size=large is set', () => {
    render(
      <LabelValueWrapper
        data-testid="label-value-wrapper"
        label={label}
        value={value}
        size={ItemSize.Large}
      />
    );

    expect(
      screen
        .getByText(label)
        .classList.contains('comd-data-summary-label-value-wrapper--size-large')
    ).toBe(false);
    expect(
      screen
        .getByText(value)
        .classList.contains('comd-data-summary-label-value-wrapper--size-large')
    ).toBe(false);
  });

  it('should not add size-large class when size=large is not set', () => {
    render(<LabelValueWrapper label={label} value={value} />);

    expect(
      screen
        .getByText(value)
        .classList.contains('comd-data-summary-label-value-wrapper--size-large')
    ).toBe(false);
  });

  it('should not render info icon tooltip if helperText not specified', () => {
    render(<LabelValueWrapper label={label} value={value} />);

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
  });

  it('should render busy dots if isValueLoading equals true', () => {
    render(
      <LabelValueWrapper label={label} value={value} isValueLoading={true} />
    );

    expect(screen.getAllByTestId('value').length).toBe(1);
  });

  it('should not render busy dots if isValueLoading equals true', () => {
    render(
      <LabelValueWrapper label={label} value={value} isValueLoading={false} />
    );

    expect(screen.getByTestId('label').textContent).toEqual(label);
    expect(screen.queryAllByTestId('busy-dots').length).toBe(0);
  });
});
