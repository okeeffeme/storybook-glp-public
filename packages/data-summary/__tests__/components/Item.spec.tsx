import React from 'react';
import {render, screen} from '@testing-library/react';
import DataSummary, {ItemSize, IconType} from '../../src';

import {accountPlus} from '../../../icons/src';

describe('Jotunheim React Data Summary Item :: ', () => {
  it('should render icon on normal size items', () => {
    render(
      <DataSummary.Item
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
      />
    );

    const label = screen.getByTestId('label');

    expect(
      label.classList.contains(
        'comd-data-summary-label-value-wrapper__label--size-large'
      )
    ).toBe(false);

    expect(screen.queryByTestId('item-icon')).toBeInTheDocument();
  });

  it('should render default icon color be default', () => {
    render(
      <DataSummary.Item
        data-testid="data-summary"
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
      />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute('fill', 'TextDisabled');
  });

  it('should render danger icon if prop isDangerIcon equals true', () => {
    render(
      <DataSummary.Item
        data-testid="data-summary"
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
        iconType={IconType.Danger}
      />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute('fill', 'Danger');
  });

  it('should not render icon when item size = large', () => {
    render(
      <DataSummary.Item
        data-testid="data-summary"
        label={'label'}
        value={'value'}
        size={ItemSize.Large}
        iconPath={accountPlus}
      />
    );

    const label = screen.getByTestId('label');

    expect(
      label.classList.contains(
        'comd-data-summary-label-value-wrapper__label--size-large'
      )
    ).toBe(true);
    expect(screen.queryByTestId('item-icon')).not.toBeInTheDocument();
  });

  it('should not render icon if iconPath not specified', () => {
    render(<DataSummary.Item label={'label'} value={'value'} />);

    expect(screen.queryByTestId('item-icon')).not.toBeInTheDocument();
  });
});
