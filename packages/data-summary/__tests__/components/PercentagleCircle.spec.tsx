import React from 'react';
import {render, screen} from '@testing-library/react';

import PercentageCircle from '../../src/components/PercentageCircle';

describe('Jotunheim React Data Summary PercentageCircle :: ', () => {
  it('should use default stroke color when no strokeColor is set', () => {
    render(<PercentageCircle value={12} />);

    expect(screen.getByTestId('percentage-circle-background')).toHaveAttribute(
      'fill',
      'Disabled'
    );
    expect(screen.getByTestId('percentage-circle-fill')).toHaveAttribute(
      'stroke',
      'PrimaryAccent'
    );
  });

  it('should use strokeColor when strokeColor is set', () => {
    render(<PercentageCircle value={12} strokeColor="red" />);

    expect(screen.getByTestId('percentage-circle-background')).toHaveAttribute(
      'fill',
      'Disabled'
    );
    expect(screen.getByTestId('percentage-circle-fill')).toHaveAttribute(
      'stroke',
      'red'
    );
  });
});
