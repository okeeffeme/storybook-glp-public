import React from 'react';
import {render, screen} from '@testing-library/react';

import DataSummary from '../../src';

describe('Jotunheim React Data Summary :: ', () => {
  it('should render with default props', () => {
    render(
      <DataSummary>
        <DataSummary.Item value={'qwe'} label={'asd'} />
      </DataSummary>
    );

    expect(screen.getAllByText('asd')).toHaveLength(1);
  });
});
