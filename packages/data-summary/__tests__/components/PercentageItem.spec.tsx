import React from 'react';
import {render, screen} from '@testing-library/react';

import DataSummary from '../../src';

describe('Jotunheim React Data Summary PercentageItem :: ', () => {
  it('should render label and value, and append % symbol to value', () => {
    render(<DataSummary.PercentageItem label="banana" value={12} />);

    expect(screen.getByText(/banana/i)).toBeInTheDocument();
    expect(screen.getByText(/12%/i)).toBeInTheDocument();
  });

  it('should not show decimals, and round down value by default', () => {
    render(<DataSummary.PercentageItem label="completes" value={12.4} />);

    expect(screen.getByText(/12%/i)).toBeInTheDocument();
  });

  it('should not show decimals, and round up value by default', () => {
    render(<DataSummary.PercentageItem label="completes" value={12.5} />);

    expect(screen.getByText(/13%/i)).toBeInTheDocument();
  });

  it('should round value and show decimals when maxDecimals is set', () => {
    render(
      <DataSummary.PercentageItem
        label="completes"
        value={12.3456}
        maxDecimals={3}
      />
    );

    expect(screen.getByText(/12.346%/i)).toBeInTheDocument();
  });
});
