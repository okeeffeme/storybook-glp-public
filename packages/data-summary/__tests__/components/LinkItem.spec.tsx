import React from 'react';
import {render, screen} from '@testing-library/react';
import DataSummary, {IconType} from '../../src';
import {accountPlus} from '../../../icons/src';

describe('Jotunheim React Data Summary Link Item :: ', () => {
  it('should render link', () => {
    render(
      <DataSummary.LinkItem
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
        iconType={IconType.Danger}
        href={'#'}
      />
    );

    expect(screen.getByTestId('label')).toBeInTheDocument();
  });

  it('should render icon if icon path passed', () => {
    render(
      <DataSummary.Item
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
      />
    );

    expect(screen.getByTestId('icon')).toBeInTheDocument();
  });

  it('should render default icon color by default', () => {
    render(
      <DataSummary.LinkItem
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
        href={'#'}
      />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute('fill', 'TextDisabled');
  });

  it('should render danger icon if prop isDangerIcon equals true', () => {
    render(
      <DataSummary.LinkItem
        label={'label'}
        value={'value'}
        iconPath={accountPlus}
        iconType={IconType.Danger}
        href={'#'}
      />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute('fill', 'Danger');
  });

  it('should not render icon if iconPath not specified', () => {
    render(<DataSummary.LinkItem label={'label'} value={'value'} href={'#'} />);

    expect(screen.queryByTestId('icon')).not.toBeInTheDocument();
  });
});
