# Jotunheim React Overview

[Changelog](./CHANGELOG.md)

This component is used to give an overview of primary information on any given page
