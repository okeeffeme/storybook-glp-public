export enum ItemSize {
  Normal = 'normal',
  Large = 'large',
}

export enum IconType {
  Danger = 'danger',
  Default = 'default',
}
