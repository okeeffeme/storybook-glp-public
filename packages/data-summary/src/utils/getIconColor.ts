import {IconType} from '../types';
import {colors} from '@jotunheim/global-styles';

export const getIconColor = (iconType: IconType) => {
  switch (iconType) {
    case IconType.Danger:
      return colors.Danger;
    default:
      return colors.TextDisabled;
  }
};
