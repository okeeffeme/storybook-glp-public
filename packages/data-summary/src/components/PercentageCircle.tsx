import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {colors} from '@jotunheim/global-styles';

import styles from './PercentageCircle.module.css';

const MaxCircleValue = Number(styles.MaxCircleValue);

const {block} = bemFactory({
  baseClassName: 'comd-data-summary-percentage-circle',
  classNames: styles,
});

type PercentageCircleProps = {
  value: number;
  strokeColor?: string;
};

export const PercentageCircle = ({
  strokeColor = colors.PrimaryAccent,
  value,
}: PercentageCircleProps) => {
  const currentCircleValue = (MaxCircleValue * value) / 100;

  return (
    <svg
      width="40"
      height="40"
      viewBox="0 0 40 40"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg">
      <path
        data-testid="percentage-circle-background"
        d="M20,0c11.038,0 20,8.962 20,20c0,11.038 -8.962,20 -20,20c-11.038,0 -20,-8.962 -20,-20c0,-11.038 8.962,-20 20,-20Zm0,4c-8.831,0 -16,7.169 -16,16c0,8.831 7.169,16 16,16c8.831,0 16,-7.169 16,-16c0,-8.831 -7.169,-16 -16,-16Z"
        fill={colors.Disabled}
      />
      <path
        className={block()}
        data-testid="percentage-circle-fill"
        d="M20 2.085a18.005 18.005 0 0 1 0 36.009a18.005 18.005 0 0 1 0 -36.009"
        stroke={strokeColor}
        strokeWidth="4"
        strokeLinecap="butt"
        fill="none"
        strokeDasharray={`${currentCircleValue}, ${MaxCircleValue}`}
      />
    </svg>
  );
};

export default PercentageCircle;
