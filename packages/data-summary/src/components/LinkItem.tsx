import React, {ReactNode} from 'react';
import cn from 'classnames';

import {Truncate} from '@jotunheim/react-truncate';
import {bemFactory} from '@jotunheim/react-themes';
import Icon from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Link, LinkAppearances} from '@jotunheim/react-link';

import {getIconColor} from '../utils/getIconColor';
import {IconType} from '../types';
import LabelValueWrapper from './LabelValueWrapper';
import classNames from './LinkItem.module.css';

const {block, element} = bemFactory({
  baseClassName: 'comd-data-summary-link-item',
  classNames,
});

type LinkProps = {
  iconPath?: string;
  label: string;
  value: ReactNode;
  href: string;
  helperText?: ReactNode;
  iconType?: IconType;
  isValueLoading?: boolean;
};

export const LinkItem: React.FC<LinkProps> = ({
  iconPath,
  label,
  value,
  helperText,
  iconType = IconType.Default,
  isValueLoading = false,
  href,
  ...rest
}) => {
  const iconColor = getIconColor(iconType);
  const iconWrapperClassNames = cn(element('icon'), {
    [element('icon', 'default')]: iconType === IconType.Default,
  });

  return (
    <Link
      href={href}
      appearance={LinkAppearances.table}
      data-summary-link-item={label}>
      <div className={block()} {...extractDataAriaIdProps(rest)}>
        {iconPath && (
          <div data-testid="link-icon">
            <Icon
              path={iconPath}
              size={48}
              fill={iconColor}
              className={iconWrapperClassNames}
            />
          </div>
        )}
        <LabelValueWrapper
          helperText={helperText}
          label={<Truncate>{label}</Truncate>}
          value={<Truncate>{value}</Truncate>}
          isValueLoading={isValueLoading}
        />
      </div>
    </Link>
  );
};

export default LinkItem;
