import React, {ReactNode} from 'react';
import cn from 'classnames';

import Tooltip from '@jotunheim/react-tooltip';
import {InformationIcon} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';
import {BusyDots} from '@jotunheim/react-busy-dots';

import {ItemSize} from '../types';
import classNames from './LabelValueWrapper.module.css';

type LabelValueWrapperProps = {
  size?: string;
  label: ReactNode;
  value: ReactNode;
  helperText?: ReactNode;
  isValueLoading?: boolean;
};

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-data-summary-label-value-wrapper',
  classNames,
});

export const LabelValueWrapper = ({
  size = ItemSize.Normal,
  label,
  value,
  helperText,
  isValueLoading = false,
}: LabelValueWrapperProps) => {
  return (
    <div
      className={cn(block(), {
        [modifier('size-large')]: size === ItemSize.Large,
      })}>
      <div
        className={cn(element('label'), {
          [element('label', 'size-large')]: size === ItemSize.Large,
        })}
        data-testid="label">
        {label}
        {helperText && (
          <Tooltip content={helperText} data-testid="helperTextTooltip">
            <div className={element('label-icon')}>
              <InformationIcon />
            </div>
          </Tooltip>
        )}
      </div>
      <div
        className={cn(element('value'), {
          [element('value', 'size-large')]: size === ItemSize.Large,
        })}
        data-testid="value">
        {isValueLoading ? <BusyDots /> : value}
      </div>
    </div>
  );
};

export default LabelValueWrapper;
