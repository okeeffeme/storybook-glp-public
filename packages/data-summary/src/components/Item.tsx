import React, {ReactNode} from 'react';

import {Truncate} from '@jotunheim/react-truncate';
import {bemFactory} from '@jotunheim/react-themes';
import Icon from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import {getIconColor} from '../utils/getIconColor';
import {ItemSize, IconType} from '../types';
import LabelValueWrapper from './LabelValueWrapper';
import classNames from './Item.module.css';

const {block, element} = bemFactory({
  baseClassName: 'comd-data-summary-item',
  classNames,
});

type ItemProps = {
  size?: string;
  iconPath?: string;
  label: string;
  value: ReactNode;
  helperText?: ReactNode;
  iconType?: IconType;
  isValueLoading?: boolean;
};

export const Item: React.FC<ItemProps> = ({
  size = ItemSize.Normal,
  iconPath,
  label,
  value,
  helperText,
  iconType = IconType.Default,
  isValueLoading = false,
  ...rest
}) => {
  const iconColor = getIconColor(iconType);

  return (
    <div className={block()} {...extractDataAriaIdProps(rest)}>
      {iconPath && size !== ItemSize.Large && (
        <div className={element('icon')} data-testid="item-icon">
          <Icon path={iconPath} size={48} fill={iconColor} />
        </div>
      )}
      <LabelValueWrapper
        size={size}
        helperText={helperText}
        label={<Truncate>{label}</Truncate>}
        value={<Truncate>{value}</Truncate>}
        isValueLoading={isValueLoading}
      />
    </div>
  );
};

export default Item;
