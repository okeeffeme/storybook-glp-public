import React, {ReactNode} from 'react';
import {Truncate} from '@jotunheim/react-truncate';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import LabelValueWrapper from './LabelValueWrapper';
import PercentageCircle from './PercentageCircle';
import classNames from './PercentageItem.module.css';

const {block, element} = bemFactory({
  baseClassName: 'comd-data-summary-percentage-item',
  classNames,
});

type PercentageItemProps = {
  label: string;
  value: number;
  helperText?: ReactNode;
  strokeColor?: string;
  maxDecimals?: number;
};

export const PercentageItem = ({
  label,
  value,
  helperText,
  strokeColor,
  maxDecimals = 0,
  ...rest
}: PercentageItemProps) => {
  const roundedValue = Number(value.toFixed(maxDecimals));

  return (
    <div className={block()} {...extractDataAriaIdProps(rest)}>
      <div className={element('percentage-circle')}>
        <PercentageCircle strokeColor={strokeColor} value={roundedValue} />
      </div>
      <LabelValueWrapper
        helperText={helperText}
        label={<Truncate>{label}</Truncate>}
        value={<Truncate>{roundedValue}%</Truncate>}
      />
    </div>
  );
};

export default PercentageItem;
