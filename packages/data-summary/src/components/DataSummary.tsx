import React, {ReactNode} from 'react';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import Item from './Item';
import PercentageItem from './PercentageItem';
import LinkItem from './LinkItem';
import classNames from './DataSummary.module.css';

const {block} = bemFactory({
  baseClassName: 'comd-data-summary',
  classNames,
});

type DataSummaryProps = {
  children: ReactNode;
};

export const DataSummary = ({children, ...rest}: DataSummaryProps) => {
  return (
    <div
      className={block()}
      data-testid="data-summary"
      {...extractDataAriaIdProps(rest)}>
      {children}
    </div>
  );
};

DataSummary.Item = Item;
DataSummary.PercentageItem = PercentageItem;
DataSummary.LinkItem = LinkItem;

export default DataSummary;
