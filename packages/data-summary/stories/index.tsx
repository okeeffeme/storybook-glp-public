import React from 'react';
import {storiesOf} from '@storybook/react';
import {select, number, text, boolean} from '@storybook/addon-knobs';
import {
  listStatus,
  bookVariant,
  account,
  clipboardTextPlay,
} from '../../icons/src';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {colors} from '../../global-styles/src';
import DataSummary, {ItemSize, IconType} from '../src';

storiesOf('Components/data-summary', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <div
      style={{
        border: '1px solid #000',
        padding: 24,
      }}>
      <DataSummary>
        <DataSummary.Item
          label={text('label', 'With knobs')}
          value={text('value', '3543')}
          helperText={text('helperText', 'This is info')}
          size={select('size', ItemSize, ItemSize.Large)}
          isValueLoading={boolean('isValueLoading', false)}
        />
        <DataSummary.Item iconPath={listStatus} label="Status" value="Failed" />
        <DataSummary.Item iconPath={bookVariant} label="Programs" value="1" />
        <DataSummary.Item
          iconPath={account}
          label="Created by"
          value="Roberty Byrn"
          helperText="This is info"
          iconType={IconType.Danger}
        />
        <DataSummary.PercentageItem label="Completes" value={100} />
        <DataSummary.PercentageItem label="Incompletes" value={1} />
        <DataSummary.LinkItem
          iconPath={clipboardTextPlay}
          label="Dataflow ID"
          value="23155"
          href="#"
        />
        <DataSummary.LinkItem
          iconPath={listStatus}
          label="Last run"
          value="Failed"
          helperText="This is info"
          iconType={IconType.Danger}
          href="#"
        />
      </DataSummary>
    </div>
  ))
  .add('Percentage Item', () => (
    <div
      style={{
        border: '1px solid #000',
        padding: 24,
      }}>
      <DataSummary>
        <DataSummary.PercentageItem
          label={text('label', 'With knobs')}
          value={number('value', 25)}
          maxDecimals={number('maxDecimals', 0)}
          helperText={text('helperText', 'This is info')}
          strokeColor={text('strokeColor', colors.PrimaryAccent)}
        />
        <DataSummary.PercentageItem
          label="Error (custom color)"
          value={13}
          strokeColor={colors.Danger}
        />
        <DataSummary.PercentageItem label="Completes" value={100} />
        <DataSummary.PercentageItem label="Incompletes" value={1} />
        <DataSummary.PercentageItem
          label="Started"
          value={98.123456789}
          maxDecimals={2}
          helperText="This is info"
        />
      </DataSummary>
    </div>
  ))
  .add('Link Item', () => (
    <div
      style={{
        border: '1px solid #000',
        padding: 24,
      }}>
      <DataSummary>
        <DataSummary.LinkItem
          iconPath={account}
          label="Created by"
          value="Roberty Byrn"
          href="#"
        />
        <DataSummary.LinkItem
          iconPath={listStatus}
          label={text('label', 'Status')}
          value={text('value', 'Failed')}
          helperText={text('helperText', 'This is info')}
          iconType={select('iconType', IconType, IconType.Danger)}
          isValueLoading={boolean('isValueLoading', false)}
          href="#"
        />
      </DataSummary>
    </div>
  ));
