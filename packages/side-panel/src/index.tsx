import SidePanel from './components/side-panel';
import Header from './components/header';
import Content from './components/content';

export {Header, Content, SidePanel};
export default SidePanel;
