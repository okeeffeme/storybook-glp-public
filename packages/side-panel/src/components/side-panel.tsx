import React, {
  forwardRef,
  CSSProperties,
  ForwardRefExoticComponent,
  RefAttributes,
  PropsWithChildren,
} from 'react';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import Header from './header';
import Content from './content';

import classNames from './side-panel.module.css';

const {block} = bemFactory('comd-side-panel', classNames);

interface SidePanelProps {
  style?: CSSProperties;
}

type ForwardRefSidePanel = ForwardRefExoticComponent<
  PropsWithChildren<SidePanelProps> & RefAttributes<HTMLElement>
> & {Header: typeof Header; Content: typeof Content};

export const SidePanel: ForwardRefSidePanel = forwardRef(function SidePanel(
  {children, style, ...rest},
  ref
) {
  return (
    <aside
      ref={ref}
      className={block()}
      style={style}
      data-testid="side-panel"
      {...extractDataAriaIdProps(rest)}
      {...extractOnEventProps(rest)}>
      {children}
    </aside>
  );
}) as ForwardRefSidePanel;

SidePanel.displayName = 'SidePanel';

SidePanel.Header = Header;
SidePanel.Content = Content;

export default SidePanel;
