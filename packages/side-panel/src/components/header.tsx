import React, {forwardRef, PropsWithChildren, CSSProperties} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';

import classNames from './side-panel.module.css';

const {element} = bemFactory('comd-side-panel', classNames);

export interface HeaderProps {
  style?: CSSProperties;
  className?: string;
}

const Header = forwardRef<HTMLElement, PropsWithChildren<HeaderProps>>(
  function Header({children, className, style, ...rest}, ref) {
    const classes = cn(element('header'), className);

    return (
      <header
        ref={ref}
        className={classes}
        style={style}
        {...extractDataAriaIdProps(rest)}
        {...extractOnEventProps(rest)}>
        {children}
      </header>
    );
  }
);

export default Header;
