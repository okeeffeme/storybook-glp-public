import React, {forwardRef, FC, PropsWithChildren, CSSProperties} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';

import classNames from './side-panel.module.css';

const {element} = bemFactory('comd-side-panel', classNames);

export interface ContentProps {
  className?: string;
  style?: CSSProperties;
  onClick?: () => void;
}

const Content: FC<PropsWithChildren<ContentProps>> = forwardRef<
  HTMLDivElement,
  ContentProps
>(function Content({children, className, style, ...rest}, ref) {
  const classes = cn(element('content'), {
    [`${className}`]: className,
  });

  return (
    <div
      ref={ref}
      className={classes}
      style={style}
      {...extractDataAriaIdProps(rest)}
      {...extractOnEventProps(rest)}>
      {children}
    </div>
  );
});

export default Content;
