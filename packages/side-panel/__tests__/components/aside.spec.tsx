import React from 'react';
import {render, screen} from '@testing-library/react';
import {theme, ThemeContext} from '../../../themes/src/index';

import SidePanel from '../../src/components/side-panel';

describe('Jotunheim React Side Panel :: ', () => {
  it('renders', () => {
    render(
      <ThemeContext.Provider value={theme.themeNames.material}>
        <SidePanel>
          <SidePanel.Header>Header</SidePanel.Header>
          <SidePanel.Content>Content</SidePanel.Content>
        </SidePanel>
      </ThemeContext.Provider>
    );

    expect(screen.getByTestId('side-panel')).toBeInTheDocument();
  });
});
