import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import SidePanel from '../src';
import {Tooltip} from '../../tooltip/src';

storiesOf('Components/side-panel', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <SidePanel>
      <SidePanel.Header>Header</SidePanel.Header>
      <SidePanel.Content>Contents</SidePanel.Content>
    </SidePanel>
  ))
  .add('With Tooltip around SidePanel', () => (
    <Tooltip content="Tooltip around SidePanel">
      <SidePanel>
        <SidePanel.Header>Header</SidePanel.Header>
        <SidePanel.Content>Contents</SidePanel.Content>
      </SidePanel>
    </Tooltip>
  ))
  .add('With Tooltip around Header and Content', () => (
    <SidePanel>
      <Tooltip content="Tooltip around Header">
        <SidePanel.Header>Header</SidePanel.Header>
      </Tooltip>
      <Tooltip content="Tooltip around Content">
        <SidePanel.Content>Contents</SidePanel.Content>
      </Tooltip>
    </SidePanel>
  ));
