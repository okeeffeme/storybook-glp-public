# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@4.1.0&sourceBranch=refs/tags/@jotunheim/react-side-panel@4.1.1&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-side-panel

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@4.0.2&sourceBranch=refs/tags/@jotunheim/react-side-panel@4.1.0&targetRepoId=1246) (2023-03-29)

### Features

- Convert side-panel package to TypeScript ([NPM-1238](https://jiraosl.firmglobal.com/browse/NPM-1238)) ([795c713](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/795c713c506ee70c0413db906419068377793b51))

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@4.0.1&sourceBranch=refs/tags/@jotunheim/react-side-panel@4.0.2&targetRepoId=1246) (2023-01-27)

### Bug Fixes

- adding data-testid to Side-panel component ([NPM-1210](https://jiraosl.firmglobal.com/browse/NPM-1210)) ([cd452bc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cd452bc26775b068ab8c3692093b0ec422c395c0))

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@4.0.0&sourceBranch=refs/tags/@jotunheim/react-side-panel@4.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-side-panel

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@3.0.1&sourceBranch=refs/tags/@jotunheim/react-side-panel@4.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of SidePanel ([NPM-950](https://jiraosl.firmglobal.com/browse/NPM-950)) ([f2b27c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f2b27c2306003bb1ab42253a0550f57178e406a9))
- remove default theme from SidePanel ([NPM-1074](https://jiraosl.firmglobal.com/browse/NPM-1074)) ([9d92afd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9d92afdc8e744220d7e31f725b1172c9a507c736))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-side-panel@3.0.0&sourceBranch=refs/tags/@jotunheim/react-side-panel@3.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-side-panel

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-side-panel@2.1.3&sourceBranch=refs/tags/@confirmit/react-side-panel@2.1.4&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-side-panel@2.1.2&sourceBranch=refs/tags/@confirmit/react-side-panel@2.1.3&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-side-panel

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-side-panel@2.1.1&sourceBranch=refs/tags/@confirmit/react-side-panel@2.1.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-side-panel@2.1.0&sourceBranch=refs/tags/@confirmit/react-side-panel@2.1.1&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-side-panel

# [2.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-side-panel@2.0.1...@confirmit/react-side-panel@2.1.0) (2020-08-12)

### Features

- wrap SidePanel components with forwardRef. Pass event callbacks down ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([d3dfbbf](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/d3dfbbfc748ce9dd6ea14ba338374448e4762b2e))

## CHANGELOG

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- **BREAKING**: Named export changed from `Aside` to `SidePanel`. Otherwise, the API is not changed. Classnames has also been renamed from `co/comd-aside` to `co/comd-side-panel`.
- Refactor: New name `@confirmit/react-side-panel`.

### v1.1.0

- Removing package version in class names.

### v1.0.0

- **BREAKING**
  - Remove "baseClassName" and "classNames" from Aside, Aside.Header, and Aside.Content
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.1.0

- **BREAKING**
  - Remove flex styling from Material theme

### v0.0.1

- Initial version
