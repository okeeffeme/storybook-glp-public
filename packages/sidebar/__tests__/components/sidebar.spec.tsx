import React from 'react';
import {render as renderRTL, screen} from '@testing-library/react';

import {Sidebar} from '../../src/components/sidebar';
import Item from '../../src/components/item';

const defaultProps = {
  baseClassName: 'comd-sidebar',
};

const render = (props) => {
  return renderRTL(
    <Sidebar {...defaultProps} {...props}>
      <Item id="item1" icon={<span />}>
        item 1
      </Item>
      <Item id="item2" icon={<span />}>
        item 2
      </Item>
      <Item id="item3" icon={<span />}>
        {false && <div>not displayed</div>}
        {null && <div>also not displayed</div>}
      </Item>
    </Sidebar>
  );
};
describe('Jotunheim React Sidebar :: ', () => {
  it('should render correct items in sidebar', () => {
    render({selectedItemId: 'item2'});

    const items = screen.getAllByTestId('item');

    expect(items).toHaveLength(3);
    expect(items[1].classList.contains('comd-sidebar__item--active')).toBe(
      true
    );
  });

  it('should render correct items in sidebar, but no current item, when no selectedItem is set', () => {
    render({});

    const items = screen.getAllByTestId('item');

    expect(items).toHaveLength(3);
    expect(items[0].classList.contains('comd-sidebar__item--active')).toBe(
      false
    );
    expect(items[1].classList.contains('comd-sidebar__item--active')).toBe(
      false
    );
    expect(items[2].classList.contains('comd-sidebar__item--active')).toBe(
      false
    );
  });

  it('should render container for selected item', () => {
    render({selectedItemId: 'item2'});

    expect(screen.getByText(/item 2/i)).toBeInTheDocument();
  });

  it("should not render container for current item if it doesn't have visible children", () => {
    render({selectedItemId: 'item3'});

    const items = screen.getAllByTestId('item');

    expect(items[2].classList.contains('comd-sidebar__item--active')).toBe(
      true
    );
    expect(screen.queryByText(/item 3/i)).not.toBeInTheDocument();
  });

  it('should render disabled item', () => {
    renderRTL(
      <Sidebar>
        <Item id="item1" icon={<span />}>
          Item 1
        </Item>
        <Item id="item2" disabled={true} icon={<span />}>
          item 2
        </Item>
      </Sidebar>
    );

    const iconButton = screen.getAllByTestId('icon-button');

    expect(iconButton).toHaveLength(2);
    expect(iconButton[0]).not.toHaveAttribute('disabled');
    expect(iconButton[1]).toHaveAttribute('disabled');
  });
});
