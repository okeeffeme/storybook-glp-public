import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Sidebar from '../src';
import Icon, {surveyIcon, account, report} from '../../icons/src';

storiesOf('Components/sidebar', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [selectedItemId, setSelectedItemId] = React.useState('studio');
    return (
      <div style={{backgroundColor: '#f1f1f1', height: '300px'}}>
        <Sidebar
          selectedItemId={selectedItemId}
          onChange={(id) => setSelectedItemId(id)}>
          <Sidebar.Item
            id="studio"
            icon={<Icon path={report} />}
            tooltip="Studio">
            <section>Studio</section>
          </Sidebar.Item>

          <Sidebar.Item
            id="sd"
            icon={<Icon path={surveyIcon} />}
            tooltip="Survey Designer"
            disabled={true}>
            <section>Survey Designer</section>
          </Sidebar.Item>

          <Sidebar.Divider />

          <Sidebar.Item id="sh" icon={<Icon path={account} />}>
            {false && <div>Should not be displayed</div>}
            {null && <div>Should also not be displayed</div>}
          </Sidebar.Item>
        </Sidebar>
      </div>
    );
  });
