import React, {ReactNode} from 'react';

interface SidebarState {
  setCurrentChildren?: (children: ReactNode) => void;
  selectedItemId?: string;
  onChange?: (id: string) => void;
}
export default React.createContext<SidebarState>({});
