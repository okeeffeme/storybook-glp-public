import React, {
  useEffect,
  forwardRef,
  useContext,
  CSSProperties,
  PropsWithChildren,
  ReactNode,
} from 'react';
import cn from 'classnames';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Tooltip} from '@jotunheim/react-tooltip';
import {IconButton} from '@jotunheim/react-button';
import {bemFactory} from '@jotunheim/react-themes';
import SidebarContext from '../sidebar-context';

import classNames from './sidebar.module.css';

const {element} = bemFactory('comd-sidebar', classNames);

export interface ItemProps {
  id: string;
  icon?: ReactNode;
  href?: string;
  style?: CSSProperties;
  tooltip?: string;
  disabled?: boolean;
}

const Item = forwardRef<HTMLDivElement, PropsWithChildren<ItemProps>>(
  function Item(
    {id, icon, tooltip, style, href, disabled, children, ...rest},
    ref
  ) {
    const {setCurrentChildren, selectedItemId, onChange} =
      useContext(SidebarContext);

    const isCurrent = id === selectedItemId;

    useEffect(() => {
      if (isCurrent) {
        setCurrentChildren && setCurrentChildren(children);
      }
    }, [isCurrent, setCurrentChildren, children]);

    return (
      <div
        ref={ref}
        style={style}
        data-testid="item"
        className={cn(element('item'), {
          [element('item', 'active')]: isCurrent,
        })}
        {...extractDataAriaIdProps(rest)}>
        <Tooltip content={tooltip} placement="right">
          <IconButton
            data-testid="icon-button"
            href={href}
            onClick={() => onChange && onChange(id)}
            disabled={disabled}>
            {icon}
          </IconButton>
        </Tooltip>
      </div>
    );
  }
);

export default Item;
