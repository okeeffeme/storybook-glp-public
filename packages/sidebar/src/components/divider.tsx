import React, {FC, AriaAttributes} from 'react';
import cn from 'classnames';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './sidebar.module.css';

const {element} = bemFactory('comd-sidebar', classNames);

export interface DividerProps extends AriaAttributes {
  id?: string;
}

const Divider: FC<DividerProps> = (props) => {
  return (
    <div
      className={cn(element('divider'))}
      {...extractDataAriaIdProps(props)}
    />
  );
};

export default Divider;
