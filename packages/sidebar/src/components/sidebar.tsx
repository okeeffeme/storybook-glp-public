import React, {
  useState,
  useMemo,
  CSSProperties,
  FC,
  PropsWithChildren,
  ReactNode,
} from 'react';
import cn from 'classnames';

import {bemFactory} from '@jotunheim/react-themes';
import {
  extractDataAriaIdProps,
  hasVisibleChildren,
} from '@jotunheim/react-utils';

import Item, {ItemProps} from './item';
import Divider, {DividerProps} from './divider';

import SidebarContext from '../sidebar-context';

import classNames from './sidebar.module.css';

interface SidebarProps {
  style?: CSSProperties;
  baseClassName?: string;
  onChange?: (id: string) => void;
  selectedItemId?: string;
  itemContentClass?: string;
}

const {block, element} = bemFactory('comd-sidebar', classNames);

export const Sidebar: FC<PropsWithChildren<SidebarProps>> & {
  Item: FC<ItemProps>;
  Divider: FC<DividerProps>;
} = ({
  onChange,
  selectedItemId,
  itemContentClass,
  style,
  children,
  ...rest
}) => {
  const [currentChildren, setCurrentChildren] = useState<ReactNode | null>(
    null
  );
  const contextValue = useMemo(
    () => ({setCurrentChildren, selectedItemId, onChange}),
    [selectedItemId, onChange]
  );

  return (
    <SidebarContext.Provider value={contextValue}>
      <div
        className={block()}
        style={style}
        data-testid="sidebar"
        {...extractDataAriaIdProps(rest)}>
        <div className={cn(element('items'))}>{children}</div>

        {hasVisibleChildren(currentChildren) && (
          <div
            data-sidebar-item-id={selectedItemId}
            data-testid="sidebar-current-item"
            className={cn(element('current'), itemContentClass)}>
            {currentChildren}
          </div>
        )}
      </div>
    </SidebarContext.Provider>
  );
};

Sidebar.Item = Item;
Sidebar.Divider = Divider;

export default Sidebar;
