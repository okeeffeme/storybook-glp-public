export enum TriggerTypes {
  click = 'click',
  hover = 'hover',
  none = 'none',
}

export type Trigger = TriggerTypes | 'click' | 'hover' | 'none';
