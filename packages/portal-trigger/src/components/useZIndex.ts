import {useEffect, useState} from 'react';
import {useZIndexStack} from '@jotunheim/react-contexts';

type UseZIndexProps = {
  portalHTMLElement: HTMLElement | null;
  portalZIndex?: number;
};

export default function useZIndex({
  portalHTMLElement,
  portalZIndex,
}: UseZIndexProps) {
  const [zIndex, setZIndex] = useState<number>(0);
  const zIndexStack = useZIndexStack();
  /* Obtain z-index once portal is about to appear. */
  const handleTransitionEnter = () => {
    setZIndex(zIndexStack.obtain());
  };

  /* Release the z-index once "exit" animation is done. */
  const handleTransitionLeft = () => {
    zIndexStack.release(zIndex);
  };

  /* Release zIndex if PortalTrigger is unmounted
   * If z-index is already release during "exit" animation of TransitionPortal
   * then no index will be released. */
  useEffect(() => {
    return () => {
      zIndexStack.release(zIndex);
    };
  }, [zIndexStack, portalZIndex, zIndex]);

  useEffect(() => {
    if (portalHTMLElement) {
      portalHTMLElement.style.zIndex = (portalZIndex ?? zIndex) + '';
    }
  }, [zIndex, portalZIndex, portalHTMLElement]);

  return {
    handleTransitionEnter,
    handleTransitionLeft,
  };
}
