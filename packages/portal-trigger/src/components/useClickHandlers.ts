import {useCallback, useEffect} from 'react';
import {isWithinNode} from '@jotunheim/react-utils';
import {Trigger, TriggerTypes} from './TriggerTypes';
import {MouseEventButton} from './MouseEventButton';

type UseClickHandlersProps = {
  triggerHTMLElement: HTMLElement | null;
  portalHTMLElement: HTMLElement | null;
  open: boolean;
  onToggle: (open: boolean) => void;
  descendantPortalHTMLElements: Set<HTMLElement>;
  trigger: Trigger;
  closeOnPortalClick: boolean;
  closeOnOutsideClick: boolean;
  closeOnTriggerClick: boolean;
};

export default function useClickHandlers({
  triggerHTMLElement,
  portalHTMLElement,
  open,
  onToggle,
  descendantPortalHTMLElements,
  trigger,
  closeOnOutsideClick,
  closeOnPortalClick,
  closeOnTriggerClick,
}: UseClickHandlersProps) {
  const handleDocumentClick = useCallback(
    (e) => {
      if (trigger !== TriggerTypes.click) return;

      if (!(open || e.button === MouseEventButton.LeftButton)) return;

      const isWithinTrigger = isWithinNode(triggerHTMLElement, e.target);
      /* Check is element clicked within portal or its descendant portals.
       * Use case: DatePicker that is rendered in Popover.
       *  */
      const isWithinPortal = [
        portalHTMLElement,
        ...Array.from(descendantPortalHTMLElements),
      ].some((portalHTMLElement) => {
        return isWithinNode(portalHTMLElement, e.target);
      });

      if (isWithinTrigger) {
        if (closeOnTriggerClick || !open) {
          onToggle(!open);
        }
      } else if (open) {
        if (isWithinPortal && closeOnPortalClick) {
          onToggle(false);
        } else if (!isWithinPortal && closeOnOutsideClick) {
          onToggle(false);
        }
      }
    },
    [
      open,
      onToggle,
      descendantPortalHTMLElements,
      trigger,
      closeOnOutsideClick,
      closeOnPortalClick,
      closeOnTriggerClick,
      triggerHTMLElement,
      portalHTMLElement,
    ]
  );

  useEffect(() => {
    document.addEventListener('mouseup', handleDocumentClick);

    return () => {
      document.removeEventListener('mouseup', handleDocumentClick);
    };
  }, [handleDocumentClick]);
}
