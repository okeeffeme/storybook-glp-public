export enum MouseEventButton {
  LeftButton = 0,
  MiddleButton = 1,
  RightButton = 2,
  BrowserBackButton = 3,
  BrowserForwardButton = 4,
}
