import React, {
  LegacyRef,
  MouseEventHandler,
  MutableRefObject,
  ReactElement,
} from 'react';
import {Trigger, TriggerTypes} from './TriggerTypes';
import {fchain} from '@jotunheim/react-utils';

export type TriggerElementProps = {
  onMouseEnter?: MouseEventHandler;
  onMouseLeave?: MouseEventHandler;
  ['data-portal-trigger-active']?: string;
};

export type TriggerElement = ReactElement<
  TriggerElementProps & {ref: LegacyRef<HTMLElement>}
>;

export type TriggerRef =
  | ((instance: HTMLElement | null) => void)
  | MutableRefObject<HTMLElement | null>
  | null;

type UseTriggerElementCloneProps = {
  open: boolean;
  triggerElement: TriggerElement;
  onMouseEnter: MouseEventHandler;
  onMouseLeave: MouseEventHandler;
  trigger: Trigger;
  setTriggerHTMLElement: (ref: HTMLElement | null) => void;
  triggerRef?: TriggerRef;
};

export default function useTriggerElementClone({
  open,
  triggerElement,
  trigger,
  onMouseEnter,
  onMouseLeave,
  setTriggerHTMLElement,
  triggerRef,
}: UseTriggerElementCloneProps) {
  const handleMouseEnter =
    trigger === TriggerTypes.hover
      ? fchain(triggerElement.props.onMouseEnter, onMouseEnter)
      : triggerElement.props.onMouseEnter;
  const handleMouseLeave =
    trigger === TriggerTypes.hover
      ? fchain(triggerElement.props.onMouseLeave, onMouseLeave)
      : triggerElement.props.onMouseLeave;

  const setTriggerRef = (triggerHTMLElement) => {
    if (typeof triggerRef === 'function') {
      triggerRef(triggerHTMLElement);
    } else if (triggerRef) {
      triggerRef.current = triggerHTMLElement;
    }
  };

  const triggerElementClone = React.cloneElement(triggerElement, {
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave,
    ref: (triggerHTMLElement) => {
      setTriggerHTMLElement(triggerHTMLElement);
      setTriggerRef(triggerHTMLElement);
    },
    ['data-portal-trigger-active']: open ? '' : undefined,
  });

  return triggerElementClone;
}
