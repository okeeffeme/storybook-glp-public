import {useEffect, useRef} from 'react';

type UseHoverHandlers = {
  open: boolean;
  onToggle: (open: boolean) => void;
  delay: number;
};

const clearTimeoutRef = (timer) => {
  if (timer.current) {
    clearTimeout(timer.current);
    timer.current = null;
  }
};

export default function useHoverHandlers({
  open,
  onToggle,
  delay,
}: UseHoverHandlers) {
  const triggerTimerId = useRef<number | null>();
  const leaveTimerId = useRef<number | null>();

  const handleMouseEnter = () => {
    cancelLeave();

    if (open) {
      return;
    }

    triggerTimerId.current = window.setTimeout(() => {
      onToggle(true);
      triggerTimerId.current = null;
    }, delay);
  };

  const handleMouseLeave = () => {
    cancelEnter();

    if (!open) {
      return;
    }

    leaveTimerId.current = window.setTimeout(() => {
      onToggle(false);
    }, 100);
  };

  const cancelEnter = () => {
    clearTimeoutRef(triggerTimerId);
  };

  const cancelLeave = () => {
    clearTimeoutRef(leaveTimerId);
  };

  /* Remove delay timeout if component is unmounted before animation is started */
  useEffect(() => {
    return () => {
      clearTimeoutRef(triggerTimerId);
      clearTimeoutRef(leaveTimerId);
    };
  }, []);

  return {
    handleMouseEnter,
    handleMouseLeave,
    cancelLeave,
  };
}
