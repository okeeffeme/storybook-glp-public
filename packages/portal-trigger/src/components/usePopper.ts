import {
  useCallback,
  useLayoutEffect,
  useMemo,
  useRef,
  MutableRefObject,
} from 'react';
import {
  createPopper,
  Instance,
  Options,
  StrictModifiers,
  Modifier,
} from '@popperjs/core';
import {Placement} from './PlacementTypes';

export type PopperRefInstance = Instance | null;
export type PopperRef =
  | ((instance: PopperRefInstance) => void)
  | MutableRefObject<PopperRefInstance>
  | null;

type UsePopperProps = {
  triggerHTMLElement: HTMLElement | null;
  portalHTMLElement: HTMLElement | null;
  placement: Placement;
  onPlacementChange?: (placement: Placement) => void;
  modifiers?: StrictModifiers[];
  positionFixed?: boolean;
  popperRef?: PopperRef;
};

export default function usePopper({
  modifiers,
  onPlacementChange,
  placement,
  positionFixed,
  triggerHTMLElement,
  portalHTMLElement,
  popperRef,
}: UsePopperProps) {
  const placementRef = useRef<Placement>(placement);
  const popper = useRef<Instance | null>(null);

  const handlePlacementChange = useCallback(
    (placement: Placement) => {
      if (placementRef.current !== placement) {
        placementRef.current = placement;
        onPlacementChange && onPlacementChange(placementRef.current);
      }
    },
    [onPlacementChange]
  );

  const onUpdateModifier: Modifier<'onUpdate', Options> = useMemo(
    () => ({
      name: 'onUpdate',
      phase: 'afterWrite',
      enabled: true,
      fn({state}) {
        handlePlacementChange(state.placement as Placement);
      },
    }),
    [handlePlacementChange]
  );

  const popperOptions: Partial<Options> = useMemo(() => {
    return {
      placement: placement as Placement,
      strategy: positionFixed ? 'fixed' : 'absolute',
      modifiers: [onUpdateModifier, ...(modifiers ?? [])],
    };
  }, [positionFixed, placement, onUpdateModifier, modifiers]);

  const assignRef = useCallback(() => {
    if (typeof popperRef === 'function') {
      popperRef(popper.current);
    } else if (popperRef) {
      popperRef.current = popper.current;
    }
  }, [popperRef]);

  /* We create/recreate popper each time triggerElement, portalElement  and popperOptions are set.
   * In most cases only portalElement is changed, as it is take from TransitionPortal.
   * portalElement has value when TransitionPortal is open.
   * Once TransitionPortal is closed (exited) portalElement is null as it is removed from the DOM.
   * */
  useLayoutEffect(() => {
    if (triggerHTMLElement && portalHTMLElement && !popper.current) {
      popper.current = createPopper(
        triggerHTMLElement,
        portalHTMLElement,
        popperOptions
      );
      assignRef();
    }

    return () => {
      if (popper.current) {
        popper.current.destroy();
        popper.current = null;
        assignRef();
      }
    };
  }, [assignRef, triggerHTMLElement, portalHTMLElement, popperOptions]);
}
