import React, {
  forwardRef,
  MutableRefObject,
  ReactNode,
  useEffect,
  useState,
} from 'react';
import {
  CSSTransitionClassNames,
  TransitionPortal,
} from '@jotunheim/react-transition-portal';
import {transitions} from '@jotunheim/global-styles';
import {StrictModifiers} from '@popperjs/core';
import {useUncontrolledProp} from 'uncontrollable';
import {useParentPortal} from '@jotunheim/react-contexts';
import useHoverHandlers from './useHoverHandlers';
import {Placement, PlacementTypes} from './PlacementTypes';
import {Trigger, TriggerTypes} from './TriggerTypes';
import useClickHandlers from './useClickHandlers';
import usePopper, {PopperRef} from './usePopper';
import useTriggerElementClone, {
  TriggerElement,
  TriggerRef,
} from './useTriggerElementClone';
import useZIndex from './useZIndex';

import classNames from './PortalTrigger.module.css';

export type PortalTriggerProps = {
  children: TriggerElement;
  overlay?: ReactNode;
  defaultOpen?: boolean;
  open?: boolean;
  placement?: Placement;
  onPlacementChange?: (placement: Placement) => void;
  positionFixed?: boolean;
  animation?: boolean;
  transitionClassNames: string | CSSTransitionClassNames;
  transitionEnterTimeout?: number;
  transitionLeaveTimeout?: number;
  portalZIndex?: number;
  delay?: number;
  closeOnPortalClick?: boolean;
  closeOnOutsideClick?: boolean;
  closeOnTriggerClick?: boolean;
  trigger?: Trigger;
  onToggle?: (open: boolean) => void;
  modifiers?: StrictModifiers[];
  popperRef?: PopperRef;
  portalRef?:
    | ((instance: HTMLElement | null) => void)
    | MutableRefObject<HTMLElement | null>
    | null;
};

type ForwardRefPortalTrigger = React.ForwardRefExoticComponent<
  PortalTriggerProps & React.RefAttributes<HTMLElement | null>
> & {
  triggerTypes: typeof TriggerTypes;
  placementTypes: typeof PlacementTypes;
};

export const PortalTrigger: ForwardRefPortalTrigger = forwardRef(
  (props: PortalTriggerProps, ref: TriggerRef) => {
    const {
      transitionClassNames,
      children,
      overlay,
      onPlacementChange,
      placement = PlacementTypes.bottom,
      positionFixed = false,
      defaultOpen = false,
      animation = true,
      transitionEnterTimeout = transitions.EnterDuration,
      transitionLeaveTimeout = transitions.ExitDuration,
      trigger = TriggerTypes.hover,
      closeOnPortalClick = false,
      closeOnOutsideClick = true,
      closeOnTriggerClick = true,
      delay = 0,
      modifiers,
      popperRef,
      portalZIndex,
      portalRef,
    } = props;

    const [open, onToggle] = useUncontrolledProp(
      props.open,
      defaultOpen,
      props.onToggle
    );

    /* Keep references of portal and trigger HTML elements in state to be able to react on their creation */
    const [portalHTMLElement, setPortalHTMLElement] =
      useState<HTMLElement | null>(null);
    const [triggerHTMLElement, setTriggerHTMLElement] =
      useState<HTMLElement | null>(null);

    useEffect(() => {
      if (typeof portalRef === 'function') {
        portalRef(portalHTMLElement);
      } else if (portalRef) {
        portalRef.current = portalHTMLElement;
      }
    }, [portalHTMLElement, portalRef]);

    const {ParentPortalTriggerProvider, descendantPortalHTMLElements} =
      useParentPortal({portalHTMLElement});

    const {handleMouseEnter, handleMouseLeave, cancelLeave} = useHoverHandlers({
      open,
      onToggle,
      delay,
    });

    useClickHandlers({
      open,
      onToggle,
      closeOnTriggerClick,
      closeOnPortalClick,
      closeOnOutsideClick,
      trigger,
      descendantPortalHTMLElements,
      portalHTMLElement,
      triggerHTMLElement,
    });

    usePopper({
      portalHTMLElement,
      triggerHTMLElement,
      positionFixed,
      placement,
      onPlacementChange,
      modifiers,
      popperRef,
    });

    const triggerElementClone = useTriggerElementClone({
      onMouseLeave: handleMouseLeave,
      onMouseEnter: handleMouseEnter,
      trigger,
      triggerElement: React.Children.only(children),
      open,
      setTriggerHTMLElement,
      triggerRef: ref,
    });

    const {handleTransitionEnter, handleTransitionLeft} = useZIndex({
      portalHTMLElement,
      portalZIndex,
    });

    return (
      <ParentPortalTriggerProvider>
        {!!overlay && (
          <TransitionPortal
            open={open}
            className={classNames['comd-portal-trigger-portal']}
            portalRef={setPortalHTMLElement}
            transitionClassNames={transitionClassNames}
            animation={animation}
            transitionEnterTimeout={
              animation ? transitionEnterTimeout : undefined
            }
            transitionLeaveTimeout={
              animation ? transitionLeaveTimeout : undefined
            }
            onTransitionEnter={handleTransitionEnter}
            onTransitionLeft={handleTransitionLeft}>
            {
              <div
                data-testid="portal-trigger-overlay"
                onMouseEnter={
                  trigger === TriggerTypes.hover ? cancelLeave : undefined
                }
                onMouseLeave={
                  trigger === TriggerTypes.hover ? handleMouseLeave : undefined
                }>
                {overlay}
              </div>
            }
          </TransitionPortal>
        )}
        {triggerElementClone}
      </ParentPortalTriggerProvider>
    );
  }
) as ForwardRefPortalTrigger;

PortalTrigger.triggerTypes = TriggerTypes;
PortalTrigger.placementTypes = PlacementTypes;

export default PortalTrigger;
