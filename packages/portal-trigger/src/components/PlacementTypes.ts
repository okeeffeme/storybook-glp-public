export enum PlacementTypes {
  auto = 'auto',
  autoStart = 'auto-start',
  autoEnd = 'auto-end',
  top = 'top',
  left = 'left',
  bottom = 'bottom',
  right = 'right',
  topStart = 'top-start',
  topEnd = 'top-end',
  bottomStart = 'bottom-start',
  bottomEnd = 'bottom-end',
  rightStart = 'right-start',
  rightEnd = 'right-end',
  leftStart = 'left-start',
  leftEnd = 'left-end',
}

export type Placement =
  | PlacementTypes
  | 'auto'
  | 'auto-start'
  | 'auto-end'
  | 'top'
  | 'left'
  | 'bottom'
  | 'right'
  | 'top-start'
  | 'top-end'
  | 'bottom-start'
  | 'bottom-end'
  | 'right-start'
  | 'right-end'
  | 'left-start'
  | 'left-end';
