import PortalTrigger from './components/PortalTrigger';
import {
  TriggerElement,
  TriggerElementProps,
  TriggerRef,
} from './components/useTriggerElementClone';
import {TriggerTypes, Trigger} from './components/TriggerTypes';
import {PlacementTypes, Placement} from './components/PlacementTypes';
import {StrictModifiers} from '@popperjs/core';
import {PopperRef, PopperRefInstance} from './components/usePopper';
import {MouseEventButton} from './components/MouseEventButton';

export {PortalTrigger, TriggerTypes, PlacementTypes, MouseEventButton};
export type {
  TriggerElement,
  TriggerElementProps,
  Placement,
  Trigger,
  StrictModifiers,
  PopperRef,
  PopperRefInstance,
  TriggerRef,
};
export default PortalTrigger;
