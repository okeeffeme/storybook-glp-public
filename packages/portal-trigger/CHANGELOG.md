# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.13&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.14&targetRepoId=1246) (2023-04-12)

### Bug Fixes

- only open portals with left mouse click ([NPM-1294](https://jiraosl.firmglobal.com/browse/NPM-1294)) ([9107a3b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9107a3b5aa27268db804ea6cb32249e06e0d9187))

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.12&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.13&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.11&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.12&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.10&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.11&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.9&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.10&targetRepoId=1246) (2023-01-20)

### Bug Fixes

- add animation attribute on TransitionPortal component for testing purposes ([NPM-1203](https://jiraosl.firmglobal.com/browse/NPM-1203)) ([1ab2028](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1ab2028dd9dded44f1710b687622147852c5ffe1))

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.8&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.9&targetRepoId=1246) (2023-01-06)

### Bug Fixes

- add test id for PortalTrigger component ([NPM-1204](https://jiraosl.firmglobal.com/browse/NPM-1204)) ([ce69697](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ce69697979d8e82f0d5702b77f6b5b2669aff152))

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.7&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.8&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.6&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.7&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [6.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.5&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.6&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.4&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.5&targetRepoId=1246) (2022-09-07)

### Bug Fixes

- update @popperjs/core to 2.11.6 version ([NPM-987](https://jiraosl.firmglobal.com/browse/NPM-987)) ([29c4e08](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/29c4e0880868a1868b907d8702de8d5ea788aadc))

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.2&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.1&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.0&sourceBranch=refs/tags/@jotunheim/react-portal-trigger@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-portal-trigger

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.5&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.4&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.5&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.3&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.4&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.2&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.3&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.1&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.2&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.2.0&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.1&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-portal-trigger

# [5.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.9&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.2.0&targetRepoId=1246) (2021-07-26)

### Features

- expose PopperRefInstance type ([NPM-831](https://jiraosl.firmglobal.com/browse/NPM-831)) ([d100a7e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d100a7e1b2dc32614a6d6b4e9337c816c83dc04e))

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.8&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.9&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.7&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.8&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.6&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.7&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.5&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.6&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.4&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.2&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.3&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.1&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.2&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-portal-trigger

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.1.0&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.1&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-portal-trigger

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@5.0.0&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.1.0&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- use globals as defaults for transition durations ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([d709b3e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d709b3eebaaf494f170f238a96b66540d7159033))

### Features

- should not hide popover when hovering the contents (only applicable when trigger is hover) ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([7d97510](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7d97510c6c40886205e1980ee2aa34468743864f))

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@4.0.0&sourceBranch=refs/tags/@confirmit/react-portal-trigger@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- add ParentPortalContext ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([6862a60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6862a603ddad7a97de83dcc2b0e0afae0778ae24))
- confirmit/react-z-index peer is no more required ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([1c6b3d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1c6b3d84112c9ecd50d485adb1ba087e8b97001c))
- use useParentPortal to handle clicks in nested portals. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([2d95f2b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2d95f2b252f3de14f5f5bdc9517d82d2f930269f))

### BREAKING CHANGES

- @confirmit/react-contexts added as peer dependency
- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.
- @confirmit/react-contexts is new peer dependency

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@3.0.2&sourceBranch=refs/tags/@confirmit/react-portal-trigger@4.0.0&targetRepoId=1246) (2020-10-26)

### Features

- bind portal position to viewport instead of scroll parent ([NPM-581](https://jiraosl.firmglobal.com/browse/NPM-581)) ([f7e2dd3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f7e2dd37ddd8e9b0f9682b34ed96b0167b0fe19a))

### BREAKING CHANGES

- There are no api changes, but change of how portal trigger tooltip is rendered.
  It now uses viewport instead scroll parent to determine its position, which in most cases a desired behaviour.
  If you need to use the old behaviour you can add:

```
const flipModifier: StrictModifiers = {
  name: 'flip',
  options: {
    altBoundary: true,
  },
};
const modifiers = useMemo(() => { return [flipModifier]}, [flipModifier])
return <PortalTrigger modifiers={modifiers} />
```

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@3.0.1&sourceBranch=refs/tags/@confirmit/react-portal-trigger@3.0.2&targetRepoId=1246) (2020-09-28)

### Bug Fixes

- change fallback positions for helperTexts on Input fields, to avoid issues where right position is blocked, and it will fallback to bottom, potentially covering the Menu options in the Select component. Changed positions for all Inputs for consistency. ([NPM-542](https://jiraosl.firmglobal.com/browse/NPM-542)) ([c173049](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1730499cdae33acfdc4be8c7c813616f08ad29d)

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-portal-trigger@3.0.0&sourceBranch=refs/tags/@confirmit/react-portal-trigger@3.0.1&targetRepoId=1246) (2020-09-10)

### Bug Fixes

- onMouseLeave should be properly called for trigger element with "click" trigger ([NPM-524](https://jiraosl.firmglobal.com/browse/NPM-524)) ([23bdadd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/23bdaddb2ddc6db66cc384db28ef0931c6d14add)

# [3.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-portal-trigger@2.0.3...@confirmit/react-portal-trigger@3.0.0) (2020-08-12)

### Features

- upgrade PortalTrigger to @popperjs/core@2.x ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([9db5291](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/9db5291a4c9a95e7eb709f84ed3cd860857adc1e))

### BREAKING CHANGES

- - `PortalTrigger` is now build on `@popperjs/core@2.x`. There are some changes
    of how position and visibility of portal is calculated.

* peerDependency `@confirmit/react-transition-portal": "^1.3.0"` is required
* PortalTrigger.popperModifiers`replaced with`PortalTrigger.modifiers` property.
  See https://popper.js.org/docs/v2/modifiers/.
* `PortalTrigger` does not support themes anymore. `comd-portal-trigger-portal`
  classname is assigned on portal element.
* `PortalTrigger` assign `data-portal-trigger-active` attribute on trigger element
  instead of any class names.

## CHANGELOG

### v2.0.1

- Fix: return `onPlacementChange` back. Tooltip and Popover render styles based on actual placement of popper.

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v1.1.0

- Removing package version in class names.

### v1.0.6

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v1.0.0

- **BREAKING**:
  - Require a new peerDependency `@confirmit/react-transition-portal`
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.4.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.

### v0.3.3

- onMouseEnter and onMouseLeave props are kept on the original trigger element

### v0.3.0

- onMouseEnter and onMouseLeave is being used under the hood to simplify logic for hiding and showing portal

### v0.2.0

- Feature: new prop "positionFixed" which it false by default
- "Popper.js" is no longer in peerDeps, now it is in dependencies

### v0.1.1

- `withZIndexStack` wrapper added to provide `zindexStack` prop

### v0.1.0

- Added "delay" prop which allows to postpone content opening

### v0.0.1

- Initial version
