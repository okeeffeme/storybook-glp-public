import React, {MutableRefObject} from 'react';
import {render, screen, act} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import type {PopperRefInstance} from '../../src/components/usePopper';
import type {Instance} from '@popperjs/core';

import PortalTrigger, {TriggerTypes, MouseEventButton} from '../../src';

describe('PortalTrigger', () => {
  it('should render TransitionPortal and trigger element', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.getByText(/trigger/i)).toBeInTheDocument();
  });

  it('should attach event mouse-event handlers on overlay when trigger is hover', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition"
        trigger={TriggerTypes.hover}>
        <span>trigger</span>
      </PortalTrigger>
    );

    const overlay = screen.getByTestId('transition-portal');

    userEvent.hover(screen.getByText(/trigger/i));

    expect(overlay.onmouseenter).toBeDefined();
    expect(overlay.onmouseleave).toBeDefined();
  });

  it('should not attach mouse-event handlers on overlay when trigger is click', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition"
        trigger={TriggerTypes.click}>
        <span>trigger</span>
      </PortalTrigger>
    );

    const overlay = screen.getByTestId('transition-portal');

    userEvent.click(screen.getByText(/trigger/i));

    expect(overlay.onmouseenter).toBe(null);
    expect(overlay.onmouseleave).toBe(null);
  });

  it('should render uncontrolled TransitionPortal and trigger element', () => {
    render(
      <PortalTrigger
        open={undefined}
        defaultOpen={true}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.getByText(/trigger/i)).toBeInTheDocument();
  });

  it('should render TransitionPortal with no open', () => {
    render(
      <PortalTrigger
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.queryByText('overlay content!')).not.toBeInTheDocument();
  });

  it('should not render TransitionPortal if overlay is undefined', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={undefined}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('should not render TransitionPortal if overlay is null', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={null}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('should not render TransitionPortal if overlay is empty string', () => {
    render(
      <PortalTrigger
        open={true}
        overlay=""
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('should add data- attribute on trigger if open', () => {
    render(
      <PortalTrigger
        open={true}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.getByText(/trigger/i)).toHaveAttribute(
      'data-portal-trigger-active'
    );
  });

  it('should not add data- attribute on trigger if closed', () => {
    render(
      <PortalTrigger
        open={false}
        overlay={<span>overlay content!</span>}
        transitionClassNames="someTransition">
        <span>trigger</span>
      </PortalTrigger>
    );

    expect(screen.getByText(/trigger/i)).not.toHaveAttribute(
      'data-portal-trigger-active'
    );
  });

  describe('usePopper', () => {
    it('should create a popper instance and assign to RefObject', () => {
      const popperRef = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current).not.toBeNull();
    });

    it('should destroy a popper instance and unassigned it from RefObject', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      const {unmount} = render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      const popperInstance = popperRef.current as Instance;
      jest.spyOn(popperInstance, 'destroy');

      unmount();

      expect(popperRef.current).toBeNull();
      expect(popperInstance.destroy).toBeCalled();
    });

    it('should create a popper instance and assign to RefCallback', () => {
      let popper = null;
      const popperRef = (ref) => (popper = ref);

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popper).not.toBeNull();
    });

    it('should destroy a popper instance and unassigned it from RefObject', () => {
      let popper = null;
      const popperRef = (ref) => (popper = ref);

      const {unmount} = render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      const popperInstance = popper as any; // eslint-disable-line @typescript-eslint/no-explicit-any
      jest.spyOn(popperInstance, 'destroy');

      unmount();

      expect(popper).toBeNull();
      expect(popperInstance.destroy).toBeCalled();
    });

    it('should not create popper instance if not open', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={false}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current).toBeNull();
    });

    it('should destroy a popper instance and unassigned it from RefObject once closed', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      const {unmount} = render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      const popperInstance = popperRef.current as Instance;
      jest.spyOn(popperInstance, 'destroy');

      unmount();

      expect(popperRef.current).toBeNull();
      expect(popperInstance.destroy).toBeCalled();
    });

    it('should use absolute strategy by default', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current?.state.options.strategy).toBe('absolute');
    });

    it('should use fixed strategy', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          positionFixed={true}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current?.state.options.strategy).toBe('fixed');
    });

    it('should use bottom placement by default', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current?.state.options.placement).toBe('bottom');
    });

    it('should use "placement" from props', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(popperRef.current?.state.options.placement).toBe('top');
    });

    it('should call onPlacementChange if popper changes placement', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };
      const onPlacementChange = jest.fn();

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onPlacementChange={onPlacementChange}>
          <span>trigger</span>
        </PortalTrigger>
      );

      const onUpdate = popperRef.current?.state.options.modifiers.find(
        (modifier) => modifier.name === 'onUpdate'
      );
      onUpdate.fn({state: {placement: 'bottom'}});

      expect(onPlacementChange).toBeCalledWith('bottom');
    });

    it('should add flip modifiers', () => {
      const popperRef: MutableRefObject<PopperRefInstance> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          popperRef={popperRef}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          modifiers={[
            {
              name: 'flip',
              options: {
                fallbackPlacements: ['bottom', 'top'],
              },
            },
          ]}>
          <span>trigger</span>
        </PortalTrigger>
      );

      const modifier = popperRef.current?.state.orderedModifiers.find(
        (modifier) => modifier.name === 'flip'
      );

      expect(modifier?.options).toEqual({
        fallbackPlacements: ['bottom', 'top'],
      });
    });
  });

  describe('useZIndex', () => {
    it('should obtain second z-index from stack for the second portal', () => {
      const portalRef1: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          portalRef={portalRef1}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef1.current?.style.zIndex).toBe('10');

      const portalRef2: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          portalRef={portalRef2}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef2.current?.style.zIndex).toBe('20');
    });

    it('should obtain first z-index from stack for the second portal if first portal is unmounted', () => {
      const portalRef1: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      const {unmount} = render(
        <PortalTrigger
          open={true}
          portalRef={portalRef1}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef1.current?.style.zIndex).toBe('10');

      unmount();

      const portalRef2: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          portalRef={portalRef2}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef2.current?.style.zIndex).toBe('10');
    });

    it('should obtain first z-index from stack for the second portal if first portal is closed', () => {
      jest.useFakeTimers();

      const portalRef1: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      const {rerender} = render(
        <PortalTrigger
          open={true}
          portalRef={portalRef1}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef1.current?.style.zIndex).toBe('10');

      rerender(
        <PortalTrigger
          open={false}
          portalRef={portalRef1}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      /* Let closing animation to be finished */
      jest.runAllTimers();

      const portalRef2: MutableRefObject<HTMLElement | null> = {
        current: null,
      };

      render(
        <PortalTrigger
          open={true}
          portalRef={portalRef2}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition">
          <span>trigger</span>
        </PortalTrigger>
      );

      expect(portalRef2.current?.style.zIndex).toBe('10');
    });
  });

  describe('useHoverHandlers', () => {
    it('should call onToggle(true) if mouse enters closed portal trigger', () => {
      jest.useFakeTimers();
      const onToggleHandler = jest.fn();

      render(
        <PortalTrigger
          open={false}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggleHandler}>
          <span>trigger</span>
        </PortalTrigger>
      );

      userEvent.hover(screen.getByText(/trigger/i));

      /* onToggle is run with a some delay */
      act(() => {
        jest.runAllTimers();
      });

      expect(onToggleHandler).toBeCalledWith(true);
    });

    it('should not call onToggle if mouse enters opened portal trigger', () => {
      jest.useFakeTimers();
      const onToggleHandler = jest.fn();

      render(
        <PortalTrigger
          open={true}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggleHandler}>
          <span>trigger</span>
        </PortalTrigger>
      );

      userEvent.hover(screen.getByText(/trigger/i));

      /* onToggle is run with a some delay */
      act(() => {
        jest.runAllTimers();
      });

      expect(onToggleHandler).not.toBeCalled();
    });

    it('should call onToggle(false) if leaves opened portal trigger', () => {
      jest.useFakeTimers();
      const onToggleHandler = jest.fn();

      render(
        <PortalTrigger
          open={true}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggleHandler}>
          <span>trigger</span>
        </PortalTrigger>
      );

      userEvent.unhover(screen.getByText(/trigger/i));

      /* onToggle is run with a some delay */
      act(() => {
        jest.runAllTimers();
      });

      expect(onToggleHandler).toBeCalledWith(false);
    });

    it('should not call onToggle if mouse leaves closed portal trigger', () => {
      jest.useFakeTimers();
      const onToggleHandler = jest.fn();

      render(
        <PortalTrigger
          open={false}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggleHandler}>
          <span>trigger</span>
        </PortalTrigger>
      );

      userEvent.unhover(screen.getByText(/trigger/i));

      /* onToggle is run with a some delay */
      act(() => {
        jest.runAllTimers();
      });

      expect(onToggleHandler).not.toBeCalled();
    });

    it('should not call onToggle if mouse enters closed portal trigger but component unmounted', () => {
      jest.useFakeTimers();
      const onToggleHandler = jest.fn();

      render(
        <PortalTrigger
          open={false}
          overlay={<span>overlay content!</span>}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggleHandler}>
          <span>trigger</span>
        </PortalTrigger>
      );

      userEvent.unhover(screen.getByText(/trigger/i));

      /* onToggle is run with a some delay */
      act(() => {
        jest.runAllTimers();
      });

      expect(onToggleHandler).not.toBeCalled();
    });
  });

  describe('useClickHandlers', () => {
    let mouseUp, onToggle;
    const insideTriggerRef = {
      current: null,
    };
    const insidePortalRef = {
      current: null,
    };
    const trigger = (
      <span data-trigger="">
        trigger
        <span ref={insideTriggerRef} data-inside-trigger="" />
      </span>
    );
    const overlay = <span ref={insidePortalRef} data-inside-portal="" />;
    beforeEach(() => {
      jest.clearAllMocks();
      jest
        .spyOn(document, 'addEventListener')
        .mockImplementation(function (eventName, callback) {
          if (eventName === 'mouseup') {
            mouseUp = callback;
          }
        });
      jest
        .spyOn(document, 'removeEventListener')
        .mockImplementation((eventName) => {
          if (eventName === 'mouseup') {
            mouseUp = null;
          }
        });

      onToggle = jest.fn();
    });

    it('should call onToggle(true) if clicked on the closed portal trigger with left mouse button', () => {
      render(
        <PortalTrigger
          open={false}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({
          target: insideTriggerRef.current,
          button: MouseEventButton.LeftButton,
        });
      });

      expect(onToggle).toBeCalledWith(true);
    });

    it('should not call onToggle(true) if clicked on the closed portal trigger with right mouse button', () => {
      render(
        <PortalTrigger
          open={false}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({
          target: insideTriggerRef.current,
          button: MouseEventButton.RightButton,
        });
      });

      expect(onToggle).toBeCalledTimes(0);
    });

    it('should call onToggle(false) if clicked on the opened portal trigger', () => {
      render(
        <PortalTrigger
          open={true}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({target: insideTriggerRef.current});
      });

      expect(onToggle).toBeCalledWith(false);
    });

    it('should not call onToggle if clicked on the opened portal trigger with "closeOnTriggerClick" = false', () => {
      render(
        <PortalTrigger
          open={true}
          closeOnTriggerClick={false}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({target: insideTriggerRef.current});
      });

      expect(onToggle).not.toBeCalled();
    });

    it('should not call onToggle if clicked on the opened portal', () => {
      render(
        <PortalTrigger
          open={true}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({target: insidePortalRef.current});
      });

      expect(onToggle).not.toBeCalled();
    });

    it('should call onToggle(false) if clicked on the opened portal with "closeOnPortalClick" = true', () => {
      render(
        <PortalTrigger
          open={true}
          closeOnPortalClick={true}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        mouseUp({target: insidePortalRef.current});
      });

      expect(onToggle).toBeCalledWith(false);
    });

    it('should call onToggle(false) if clicked outside of the opened portal', () => {
      render(
        <PortalTrigger
          open={true}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        /* null means the same if the node is not found inside portal or trigger */
        mouseUp({target: null});
      });

      expect(onToggle).toBeCalledWith(false);
    });

    it('should not call onToggle if clicked outside of the opened portal with "closeOnOutsideClick" = false', () => {
      render(
        <PortalTrigger
          open={true}
          closeOnOutsideClick={false}
          trigger="click"
          overlay={overlay}
          placement="top"
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      act(() => {
        /* null means the same if the node is not found inside portal or trigger */
        mouseUp({target: null});
      });

      expect(onToggle).not.toBeCalled();
    });

    it('should remove "mouseup" listener if portal trigger unmounted', () => {
      const {unmount} = render(
        <PortalTrigger
          open={true}
          closeOnOutsideClick={false}
          trigger="click"
          overlay={overlay}
          transitionClassNames="someTransition"
          onToggle={onToggle}>
          {trigger}
        </PortalTrigger>
      );

      unmount();

      expect(mouseUp).toBeNull();
    });
  });
});
