# Jotunheim React Portal Trigger

`PortalTrigger` is low-level component, that helps build elements such as Tooltip, Popover and Dropdown.
It is based on `TransitionPortal` component and `@popperjs/core` library.

### Optimizations

Try to memoize certain properties that are passed to `PortalTrigger` to reduce the number of `popper` instance recreation.

- `positionFixed` and `placement` are uncontrolled properties that should be static most of the time
- `modifiers` are popper modifiers and most of the time are static within a component, so recommendation to define modifiers outside of component.

```jsx
const modifiers = [
  {
    name: 'flip',
    options: {fallbackPlacements: ['right', 'left', 'top', 'bottom']},
  },
];

const ComponentWithModifiers = (props) => {
  return <PortalTrigger {...props} modifiers={modifiers} />;
};
```

### Tips and Tricks

`PortalTrigger` and `popper` does not handle dynamic content of trigger or portal.
In this case you should update popper instance manually using `popperRef` property.

```tsx
const popperRef = React.useRef(null);
/* eslint-disable react-hooks/exhaustive-deps */
const forcePopperUpdate = React.useCallback(() => {
  if (popperRef.current) {
    popperRef.current.forceUpdate();
  }
}, [isEditing]); // isEditing is implicit dependency, if isEditing is changed then component animation has been started.
/* eslint-enable react-hooks/exhaustive-deps */

useRequestAnimationFrames(forcePopperUpdate, animationDuration);

return <PortalTrigger popperRef={popperRef} />;
```

`useRequestAnimationFrames` can help if popper position is changed while some animation,
in other cases it might be enough.

### Changelog

[Changelog](./CHANGELOG.md)
