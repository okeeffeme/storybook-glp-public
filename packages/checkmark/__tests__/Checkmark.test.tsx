import React from 'react';
import {screen, render} from '@testing-library/react';
import {CheckMark} from '../src';

describe('Checkmark', () => {
  it('should render component without children', () => {
    render(<CheckMark />);

    const status = screen.getByRole('status');

    expect(status.querySelectorAll('span').length).toBe(1);
    expect(status.querySelectorAll('svg').length).toBe(1);
  });

  it('should render component with children', () => {
    render(<CheckMark>This is the checkmark!</CheckMark>);

    const status = screen.getByRole('status');

    expect(status.querySelectorAll('svg').length).toBe(1);
    expect(status.querySelectorAll('span').length).toBe(2);
    expect(status.querySelectorAll('span')[1]).toHaveTextContent(
      /this is the checkmark!/i
    );
  });

  it('should render component with children', () => {
    render(
      <CheckMark labelOnLeft={true}>This is the left checkmark!</CheckMark>
    );

    const status = screen.getByRole('status');

    expect(status.querySelectorAll('svg').length).toBe(1);
    expect(status.querySelectorAll('span').length).toBe(2);
    expect(status.querySelectorAll('span')[0]).toHaveTextContent(
      /this is the left checkmark!/i
    );
  });
});
