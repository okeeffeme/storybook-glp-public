# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.17&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.17&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.15&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.16&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.14&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.15&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.13&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.14&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.12&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.13&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.11&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.12&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.10&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.11&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([8440377](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8440377580a0aeeac380794f5dfa142082846d71))

## [5.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.9&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.10&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.8&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.9&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.7&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.8&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.6&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.7&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.5&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.6&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.3&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.5&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.3&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.4&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.2&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.3&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.2.1&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.2&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.1.0&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.1&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-checkmark

# [5.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.4&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.2.0&targetRepoId=1246) (2022-12-16)

### Features

- add role 'status' to Checkmark component ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([5933292](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/59332926d44c6d768285cc0e96d858d199ce4c18))

# [5.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.4&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.1.0&targetRepoId=1246) (2022-12-15)

### Features

- add role 'status' to Checkmark component ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([5933292](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/59332926d44c6d768285cc0e96d858d199ce4c18))

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.4&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.4&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.3&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.2&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@5.0.0&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-checkmark

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.10&sourceBranch=refs/tags/@jotunheim/react-checkmark@5.0.0&targetRepoId=1246) (2022-10-11)

### Features

- convert Checkmark to TypeScript ([NPM-1094](https://jiraosl.firmglobal.com/browse/NPM-1094)) ([4c0d5cb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4c0d5cbbffd4c918716d1bb9764dcd743234a0ab))
- remove className prop of CheckMark ([NPM-931](https://jiraosl.firmglobal.com/browse/NPM-931)) ([8ca4672](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8ca4672be95f6d4f6ff280195f8375b37b1c001c))
- remove default theme from CheckMark ([NPM-1067](https://jiraosl.firmglobal.com/browse/NPM-1067)) ([7a86b70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7a86b70aa78f222e56655bd69ded36af5287fc74))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.9&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.10&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.8&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.9&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.7&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.8&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.5&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.6&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.2&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.3&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.1&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.2&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-checkmark

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-checkmark@4.0.0&sourceBranch=refs/tags/@jotunheim/react-checkmark@4.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-checkmark

## 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.0.92](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.91&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.92&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.91](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.90&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.91&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.90](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.89&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.90&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.0.89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.88&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.89&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.87&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.88&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.86&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.87&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.86](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.85&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.86&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.84&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.85&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.83&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.84&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.81&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.82&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.80&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.81&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.79&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.80&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.78&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.79&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.77&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.78&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.76&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.77&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.75&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.76&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.73&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.74&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.72&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.73&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.71&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.72&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.70&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.71&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.69&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.70&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.68&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.69&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.67&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.68&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.66&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.67&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.65&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.66&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.64&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.65&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.63&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.64&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.62&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.63&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.61&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.62&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.60&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.61&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.59&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.60&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.58&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.59&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.56&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.57&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.55&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.56&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.54&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.55&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [3.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.53&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.54&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.52&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.53&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.51&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.52&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.50&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.51&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.49&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.50&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.48&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.49&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.47&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.48&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.46&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.47&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.45&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.46&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.44&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.45&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.43&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.44&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.42&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.43&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.41&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.42&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.40&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.41&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.38&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.39&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.35&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.36&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.34&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.35&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.33&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.34&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.31&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.32&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.28&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.29&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.27&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.28&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.26&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.27&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.23&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.24&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-checkmark@3.0.21&sourceBranch=refs/tags/@confirmit/react-checkmark@3.0.22&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-checkmark@3.0.17...@confirmit/react-checkmark@3.0.18) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.13](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-checkmark@3.0.12...@confirmit/react-checkmark@3.0.13) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-checkmark

## [3.0.12](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-checkmark@3.0.11...@confirmit/react-checkmark@3.0.12) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-checkmark

## CHANGELOG

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v2.1.0

- Removing package version in class names.

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.0.0

- **BREAKING** - Removed props: baseClassName, classNames, icon

### v0.0.7

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
