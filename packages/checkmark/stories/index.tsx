import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {CheckMark} from '../src/index';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{display: 'flex', flexDirection: 'column'}}>{children}</div>
);

storiesOf('Components/Checkmark', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('check mark', () => (
    <Container>
      <CheckMark>Label on right</CheckMark>
      <CheckMark labelOnLeft>Label on left</CheckMark>
    </Container>
  ));
