import React, {AriaAttributes, ReactNode} from 'react';

import Icon, {check} from '@jotunheim/react-icons';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Checkmark.module.css';

const {block, element} = bemFactory('comd-checkmark', classNames);

export const CheckMark = ({
  children,
  labelOnLeft = false,
  ...rest
}: CheckMarkProps) => {
  return (
    <div
      role="status"
      className={block()}
      data-testid="checkmark"
      {...extractDataAndAriaProps(rest)}>
      {!labelOnLeft && (
        <span className={element('icon')}>
          <Icon path={check} size="19" />
        </span>
      )}
      {children && <span className={element('text')}>{children}</span>}
      {labelOnLeft && (
        <span className={element('icon')}>
          <Icon path={check} size="19" />
        </span>
      )}
    </div>
  );
};

type CheckMarkProps = {
  children?: ReactNode;
  labelOnLeft?: boolean;
} & AriaAttributes;

export default CheckMark;
