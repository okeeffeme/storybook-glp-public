import React from 'react';
import {render, screen} from '@testing-library/react';
import Dot from '../../src/components/Dot';

describe('Jotunheim React Dot :: ', () => {
  it('renders', () => {
    render(<Dot />);
    expect(screen.getByTestId('dot')).toBeInTheDocument();
  });
});
