# Jotunheim React Dot

[Changelog](./CHANGELOG.md)

Dot Badges are displayed near the item to which they refer, do not contain characters, and are not clickable.
