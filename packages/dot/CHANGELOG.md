# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@2.1.2&sourceBranch=refs/tags/@jotunheim/react-dot@2.1.3&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-dot

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@2.1.1&sourceBranch=refs/tags/@jotunheim/react-dot@2.1.2&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-dot

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@2.1.0&sourceBranch=refs/tags/@jotunheim/react-dot@2.1.1&targetRepoId=1246) (2023-01-23)

### Bug Fixes

- adding data-testid Dot ([NPM-1185](https://jiraosl.firmglobal.com/browse/NPM-1185)) ([bf8c953](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bf8c95313f8564a753b83ce385822375d47789d4))

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@2.0.0&sourceBranch=refs/tags/@jotunheim/react-dot@2.1.0&targetRepoId=1246) (2022-10-13)

### Features

- convert Dot to Typescript ([NPM-1097](https://jiraosl.firmglobal.com/browse/NPM-1097)) ([11b6476](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/11b647669b9dc893915e30f9d9c7a3650ce890f8))

# [2.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@1.0.1&sourceBranch=refs/tags/@jotunheim/react-dot@2.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of Dot ([NPM-936](https://jiraosl.firmglobal.com/browse/NPM-936)) ([c5951b6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c5951b6589ac0e14f00f2a8d7c43924b6ed90c7f))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dot@1.0.0&sourceBranch=refs/tags/@jotunheim/react-dot@1.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-dot

## 1.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [0.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dot@0.0.5&sourceBranch=refs/tags/@confirmit/react-dot@0.0.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [0.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dot@0.0.4&sourceBranch=refs/tags/@confirmit/react-dot@0.0.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-dot

## [0.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dot@0.0.3&sourceBranch=refs/tags/@confirmit/react-dot@0.0.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dot@0.0.2&sourceBranch=refs/tags/@confirmit/react-dot@0.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-dot

## [0.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-dot@0.0.1...@confirmit/react-dot@0.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-dot

## CHANGELOG

### v0.0.1

- Initial version
