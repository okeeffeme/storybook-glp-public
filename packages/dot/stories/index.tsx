import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Dot from '../src/components/Dot';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{padding: '24px'}}>{children}</div>
);

storiesOf('Components/dot', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <Container>
      <Dot />
    </Container>
  ));
