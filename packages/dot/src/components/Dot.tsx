import React from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {
  DataAriaIdAttributes,
  extractDataAriaIdProps,
} from '@jotunheim/react-utils';

import classNames from './Dot.module.css';

const {block} = bemFactory('comd-dot', classNames);

const Dot = (props: DataAriaIdAttributes) => {
  return (
    <span
      className={block()}
      data-react-dot={true}
      data-testid="dot"
      {...extractDataAriaIdProps(props)}
    />
  );
};

export default Dot;
