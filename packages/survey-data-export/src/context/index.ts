import {createContext} from 'react';
import type {ActionCreator} from '../state/action-creators';

interface SurveyDataExportContext {
  dispatch: (action: ActionCreator) => void;
  language: string;
}

const SurveyDataExportContext = createContext<SurveyDataExportContext>({
  language: 'en',
  dispatch: () => null,
});

export default SurveyDataExportContext;
