import {isValidEmail, areValidEmails} from 'confirmit-utils';
import {
  displayTruncateOpenEnds,
  displayCustomDelimiter,
  displayDateRangeFilter,
  isEmailOrFtpTransfer,
  isExternalFTPTransfer,
} from '../utils/display-option-rules';

import {
  validCharacters,
  isIntegerInRange,
  isValidDate,
  isValidTime,
  isEndDateAfterStartDate,
} from './validation-utils';
import {translate} from '../utils/translation';
import type {Model} from '../utils/model-provider';

export const checkIsValid = ({validationErrors, isScheduleValid}) => {
  return (
    Object.values(validationErrors).filter(Boolean).length === 0 &&
    isScheduleValid
  );
};

export interface ValidationErrors {
  truncateOpenEnds?: string;
  formattingDelimiter?: string;
  hostAndPortError?: string;
  userNameError?: string;
  passwordError?: string;
  overrideFileName?: string;
  startTimeError?: string;
  endTimeError?: string;
  startDateError?: string;
  endDateError?: string;
  endDateBeforeStartDateError?: string;
  emailRecipient?: string;
  replyTo?: string;
  subject?: string;
  plainTextBody?: string;
}

interface ValidateModel {
  model: Model;
  language: string;
}

export const validateModel = ({model, language}: ValidateModel) => {
  const {
    fileSettings,
    fileSettings: {formatting},
    dateFilterSettings,
    transferSettings: {
      transferType,
      emailRecipient,
      emailOverride,
      // {emailOverrideEnabled, replyTo, subject, plainTextBody},
      hostAndPort,
      userName,
      password,
      overrideFileNameEnabled,
      overrideFileName,
    },
  } = model;

  const {startDate, startTime, endDate, endTime} = dateFilterSettings;

  let errors: ValidationErrors = {};

  if (displayTruncateOpenEnds(fileSettings.fileType)) {
    const truncateError = validateTruncateOpenEnds({
      value: fileSettings.truncateOpenEnds,
      truncationEnabled: fileSettings.truncateOpenEndsEnabled,
      language,
    });
    if (truncateError) {
      errors.truncateOpenEnds = truncateError;
    }
  }

  if (displayCustomDelimiter(formatting.fileFormatType)) {
    const formattingDelimiterError = isEmptyOrInvalidCharacters({
      value: formatting.delimiter,
      language,
    });

    if (formattingDelimiterError) {
      errors.formattingDelimiter = formattingDelimiterError;
    }
  }

  if (displayDateRangeFilter(dateFilterSettings.dateFilterType)) {
    errors = {
      ...errors,
      ...validateDateRange({
        startDate,
        startTime,
        endDate,
        endTime,
        language,
      }),
    };
  }

  if (isExternalFTPTransfer(transferType)) {
    const hostAndPortValidation = isEmpty({value: hostAndPort, language});
    const userNameValidation = isEmpty({value: userName, language});
    const passwordValidation = isEmpty({value: password, language});

    errors = {
      ...errors,
      hostAndPortError: hostAndPortValidation,
      userNameError: userNameValidation,
      passwordError: passwordValidation,
    };
  }

  if (isEmailOrFtpTransfer(transferType)) {
    errors = {
      ...errors,
      ...validateEmails({
        email: emailRecipient,
        language,
      }),
    };
  }

  if (
    isEmailOrFtpTransfer(transferType) &&
    emailOverride?.emailOverrideEnabled
  ) {
    errors = {
      ...errors,
      ...validateSubject({subject: emailOverride?.subject, language}),
      ...validatePlainTextBody({
        plainTextBody: emailOverride?.plainTextBody,
        language,
      }),
      ...validateReplyTo({
        email: emailOverride?.replyTo,
        language,
      }),
    };
  }

  if (isEmailOrFtpTransfer(transferType) && overrideFileNameEnabled) {
    const overrideFileNameError = isEmptyOrInvalidCharacters({
      value: overrideFileName,
      language,
    });

    if (overrideFileNameError) {
      errors.overrideFileName = overrideFileNameError;
    }
  }

  return errors;
};

const validateTruncateOpenEnds = ({value, truncationEnabled, language}) => {
  if (truncationEnabled === false) {
    return;
  }

  if (!isIntegerInRange({value, minValue: 1, maxValue: 4000})) {
    return translate({
      language,
      key: 'errorMessages.truncateOpenEndsRange',
    });
  }
};

const isEmpty = ({value, language}) => {
  if (!value || value.length < 1) {
    return translate({
      language,
      key: 'errorMessages.emptyString',
    });
  }
};

const isEmptyOrInvalidCharacters = ({value, language}) => {
  if (value.length < 1) {
    return translate({
      language,
      key: 'errorMessages.emptyString',
    });
  }
  if (!validCharacters(value)) {
    return translate({
      language,
      key: 'errorMessages.invalidCharacters',
    });
  }
};

const validateDateRange = ({
  startDate,
  startTime,
  endDate,
  endTime,
  language,
}) => {
  const errors: ValidationErrors = {};

  if (!isValidTime(startTime)) {
    errors.startTimeError = translate({
      language,
      key: 'errorMessages.invalidTime',
    });
  }

  if (!isValidTime(endTime)) {
    errors.endTimeError = translate({
      language,
      key: 'errorMessages.invalidTime',
    });
  }

  if (!isValidDate(startDate)) {
    errors.startDateError = translate({
      language,
      key: 'errorMessages.invalidDate',
    });
  }

  if (!isValidDate(endDate)) {
    errors.endDateError = translate({
      language,
      key: 'errorMessages.invalidDate',
    });
  }

  if (!isEndDateAfterStartDate({startDate, startTime, endDate, endTime})) {
    errors.endDateBeforeStartDateError = translate({
      language,
      key: 'errorMessages.endDateBeforeStartDate',
    });
  }

  return errors;
};

const validateEmails = ({email, language}) => {
  const errors: ValidationErrors = {};

  if (email.length === 0 || !areValidEmails(email)) {
    errors.emailRecipient = translate({
      language,
      key: 'errorMessages.invalidEmail',
    });
  }

  return errors;
};

const validateSubject = ({subject, language}) => ({
  subject: isEmpty({value: subject, language}),
});

const validatePlainTextBody = ({plainTextBody, language}) => ({
  plainTextBody: isEmpty({value: plainTextBody, language}),
});

const validateReplyTo = ({email, language}) => {
  const errors: ValidationErrors = {};

  if (email.length === 0) {
    return errors;
  }

  if (isValidEmail(email) === false) {
    errors.replyTo = translate({
      language,
      key: 'errorMessages.invalidEmail',
    });
  }

  return errors;
};
