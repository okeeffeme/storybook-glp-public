import type {Moment} from 'moment';
import {appendTimeToDate} from '../utils/date-time';

interface TimeString {
  val: string;
  maxVal: number;
  expectedLength?: number;
}

export const validCharacters = (value: string) => {
  if (isNullOrEmpty(value)) {
    return true;
  }

  const rx = new RegExp('^[^<>&]+$');
  const matches = rx.exec(value);

  return matches !== null && value === matches[0];
};

const isNullOrEmpty = (s) => typeof s !== 'string' || s.trim().length === 0;

interface Range {
  value: number | string;
  minValue: number;
  maxValue: number;
}

export const isIntegerInRange = ({value, minValue, maxValue}: Range) => {
  if (isNaN(value as number)) {
    return false;
  }

  const numericValue = parseInt(value as string, 10);
  return (
    !isNaN(numericValue) && numericValue >= minValue && numericValue <= maxValue
  );
};

export const isValidDate = (date: Moment) => {
  return date !== null && date.isValid();
};

interface Time {
  hour: string;
  minute: string;
}

export const isValidTime = ({hour, minute}: Time) => {
  if (!hour || !minute) {
    return false;
  }

  return validateHour(hour) && validateMinute(minute);
};

const validateHour = (hour: string) =>
  validateTimeString({val: hour, maxVal: 24});

const validateMinute = (minutes: string) =>
  validateTimeString({val: minutes, expectedLength: 2, maxVal: 60});

const validateTimeString = ({val, expectedLength, maxVal}: TimeString) => {
  if (!val.length) return false;

  if (expectedLength && val.length !== expectedLength) return false;

  const intVal = +val;
  if (!Number.isInteger(intVal)) return false;

  return intVal >= 0 && intVal < maxVal;
};

export const isEndDateAfterStartDate = ({
  startDate,
  startTime,
  endDate,
  endTime,
}) => {
  const startDateTime = appendTimeToDate(startDate, startTime);
  const endDateTime = appendTimeToDate(endDate, endTime);
  return (
    !endDateTime.isBefore(startDateTime) && !endDateTime.isSame(startDateTime)
  );
};
