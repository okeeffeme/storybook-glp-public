import React, {
  useReducer,
  useMemo,
  useEffect,
  useRef,
  ReactNode,
  FC,
} from 'react';
import cn from 'classnames';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {initialSettingsCreated} from '../state/action-creators';
import reducer, {getInitialState} from '../state/reducer';
import Context from '../context';
import {ContractData, getContractData} from '../utils/model-provider';
import {translate} from '../utils/translation';
import {
  displayExportSetupSection,
  displayResponseFilter,
  displayDataFilterSection,
} from '../utils/display-option-rules';
import {
  FILE_TYPE,
  ENGLISH_LANGUAGE_ID,
  DEFAULT_LANGUAGE,
  DEFAULT_RESPONSE_STATUSES,
  TRANSFER_TYPE,
  SCHEDULE_TYPE,
  DATABASE_TYPE,
  INTERVIEW_STATUS,
} from '../utils/constants';
import type {Template, Option} from '../utils';

import Alert from '@jotunheim/react-alert';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import ExportSetup from './export-setup';
import ResponseFilter from './response-filter';
import AdvancedOptions from './advanced-options/advanced-options';
import SurveyTitle from './survey-title';
import DateFilter from './date-filter';
import Scheduling from './scheduling';

import classNames from './survey-data-export.module.css';

interface OnValidationChanged {
  isValid: boolean;
}

interface SurveyDataExportProps {
  surveyId: string;
  surveyName: string;
  onValidationChanged: (obj: OnValidationChanged) => void;
  className?: string;
  defaultFileType?: FILE_TYPE;
  surveyLanguages: Option[];
  localeList: Option[];
  fileTypes?: FILE_TYPE[];
  defaultTransferType?: TRANSFER_TYPE;
  transferTypes?: TRANSFER_TYPE[];
  availableResponseStatuses?: INTERVIEW_STATUS[];
  defaultResponseStatuses?: INTERVIEW_STATUS[];
  defaultExportLanguageId?: number;
  defaultEmailRecipient?: string;
  defaultScheduleType?: SCHEDULE_TYPE;
  defaultSchedule?: Record<string, string>;
  displayScheduler?: boolean;
  disableEmailRecipient?: boolean;
  collapseAdvancedOptions?: boolean;
  extraFilters?: Record<string, string>;
  language?: string;
  databases?: DATABASE_TYPE[];
  templates?: Template[];
  onSettingsChanged: (data: ContractData) => void;
  onInitialSettingsCreated?: (data: ContractData) => void;
  dialogDescription?: string;
  displayDateFilter?: boolean;
  displayAdvancedOptions?: boolean;
  enforceSftp?: boolean;
  externalFtpErrorMessage?: ReactNode;
  displayAllErrors?: boolean;
}

const {block} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

const SurveyDataExport: FC<SurveyDataExportProps> = ({
  defaultFileType = FILE_TYPE.Excel,
  defaultTransferType = TRANSFER_TYPE.Download,
  defaultResponseStatuses = DEFAULT_RESPONSE_STATUSES,
  defaultExportLanguageId = ENGLISH_LANGUAGE_ID,
  defaultEmailRecipient = '',
  defaultScheduleType = SCHEDULE_TYPE.Asap,
  defaultSchedule,
  className,
  surveyId,
  surveyName,
  fileTypes = [
    FILE_TYPE.Excel,
    FILE_TYPE.ExcelWithLabels,
    FILE_TYPE.DelimitedTextFile,
    FILE_TYPE.DelimitedTxtWithLabels,
    FILE_TYPE.TripleS,
    FILE_TYPE.TripleSConfirmit,
    FILE_TYPE.SPSS,
    FILE_TYPE.SPSSSav,
    FILE_TYPE.Quantum,
    FILE_TYPE.SAS,
    FILE_TYPE.FixedWidthFile,
  ],
  surveyLanguages,
  localeList,
  collapseAdvancedOptions = true,
  disableEmailRecipient = false,
  extraFilters,
  transferTypes = [
    TRANSFER_TYPE.Download,
    TRANSFER_TYPE.Email,
    TRANSFER_TYPE.FTP,
  ],
  language = DEFAULT_LANGUAGE,
  templates = [],
  onInitialSettingsCreated = () => {},
  onSettingsChanged = () => {},
  onValidationChanged = () => {},
  availableResponseStatuses,
  dialogDescription,
  displayDateFilter = true,
  displayAdvancedOptions = true,
  displayScheduler = false,
  databases = [DATABASE_TYPE.Production],
  enforceSftp,
  externalFtpErrorMessage,
  displayAllErrors,
  ...rest
}) => {
  const initialState = getInitialState({
    defaultFileType,
    defaultTransferType,
    defaultResponseStatuses,
    defaultExportLanguageId,
    defaultEmailRecipient,
    defaultScheduleType,
    defaultSchedule,
    databases,
  });

  const [state, dispatch] = useReducer(reducer, initialState);

  const mounted = useRef(false);

  useEffect(() => {
    const contractData = getContractData(state.model);

    if (!mounted.current) {
      mounted.current = true;

      onInitialSettingsCreated(contractData);
      dispatch(initialSettingsCreated({language}));
    } else {
      onSettingsChanged(contractData);
    }
  }, [state.model, language, onInitialSettingsCreated, onSettingsChanged]);

  useEffect(() => {
    onValidationChanged({
      isValid: state.isValid,
    });
  }, [state.isValid, onValidationChanged]);

  const {
    model,
    model: {
      fileSettings: {fileType},
      responseStatusesFilter,
      dateFilterSettings,
      transferSettings: {
        transferType,
        emailRecipient,
        hostAndPort,
        useSftp,
        folderName,
        userName,
        password,
      },
      templateSettings,
      comment,
      database,
      onlyDataChangedSinceLastRun,
    },
    validationErrors,
    validationErrors: {
      startTimeError,
      endTimeError,
      startDateError,
      endDateError,
      endDateBeforeStartDateError,
    },
  } = state;

  const contextData = useMemo(
    () => ({
      dispatch,
      language,
    }),
    [dispatch, language]
  );

  return (
    <Context.Provider value={contextData}>
      <div
        data-testid="survey-data-export"
        className={cn(block(), className)}
        {...extractDataAriaIdProps(rest)}>
        <Fieldset.Group layoutStyle={LayoutStyle.Vertical}>
          <SurveyTitle surveyName={surveyName} surveyId={surveyId} />

          {dialogDescription && <Alert>{dialogDescription}</Alert>}

          {displayExportSetupSection(fileTypes, templates, transferTypes) && (
            <ExportSetup
              fileType={fileType}
              allowedFileTypes={fileTypes}
              transferType={transferType}
              allowedTransferTypes={transferTypes}
              emailRecipient={emailRecipient}
              disableEmailRecipient={disableEmailRecipient}
              emailMessage={comment}
              templates={templates}
              templateSettings={templateSettings}
              database={database}
              databases={databases}
              hostAndPort={hostAndPort}
              useSftp={useSftp}
              folderName={folderName}
              userName={userName}
              password={password}
              validationErrors={validationErrors}
              enforceSftp={enforceSftp}
              externalFtpErrorMessage={externalFtpErrorMessage}
              displayAllErrors={displayAllErrors}
            />
          )}

          {displayAdvancedOptions && (
            <AdvancedOptions
              collapsed={collapseAdvancedOptions}
              model={model}
              surveyLanguages={surveyLanguages}
              localeList={localeList}
              validationErrors={validationErrors}
              displayAllErrors={displayAllErrors}
            />
          )}

          {displayDataFilterSection(
            availableResponseStatuses,
            displayDateFilter
          ) && (
            <Fieldset
              layoutStyle={LayoutStyle.Vertical}
              title={translate({language, key: 'responseFilter.title'})}>
              {displayResponseFilter(availableResponseStatuses) && (
                <ResponseFilter
                  selectedStatuses={responseStatusesFilter}
                  availableStatuses={availableResponseStatuses}
                />
              )}
              {displayDateFilter && (
                <DateFilter
                  dateFilterSettings={dateFilterSettings}
                  startTimeError={startTimeError}
                  endTimeError={endTimeError}
                  startDateError={startDateError}
                  endDateError={endDateError}
                  endDateBeforeStartDateError={endDateBeforeStartDateError}
                />
              )}
            </Fieldset>
          )}

          {extraFilters && <>{extraFilters}</>}

          {displayScheduler && (
            <Scheduling
              executionSettings={model.transferSettings.executionSettings}
              transferType={transferType}
              onlyDataChangedSinceLastRun={onlyDataChangedSinceLastRun}
            />
          )}
        </Fieldset.Group>
      </div>
    </Context.Provider>
  );
};

export default SurveyDataExport;
