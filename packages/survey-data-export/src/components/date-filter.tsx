import React, {memo, useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {dateFilterChanged} from '../state/action-creators';
import Context from '../context';
import {translate} from '../utils/translation';
import {buildTimeFromText} from '../utils/date-time';
import {displayDateRangeFilter} from '../utils/display-option-rules';
import {dataFilterList} from '../utils/enums';
import type {DateFilterSettings} from '../utils/model-provider';

import DatePicker from '@jotunheim/react-date-picker';
import {TextField} from '@jotunheim/react-text-field';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import SelectWithOptions from './select-with-options';

import classNames from './survey-data-export.module.css';

interface DateFilterProps {
  dateFilterSettings?: DateFilterSettings;
  startTimeError?: string;
  endTimeError?: string;
  startDateError?: string;
  endDateError?: string;
  endDateBeforeStartDateError?: string;
}

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

export const DateFilter: FC<DateFilterProps> = ({
  dateFilterSettings,
  startTimeError = '',
  endTimeError = '',
  startDateError = '',
  endDateError = '',
  endDateBeforeStartDateError = '',
}) => {
  const {dispatch, language} = useContext(Context);

  return (
    <>
      <div data-testid="date-filter" className={element('two-columns')}>
        <SelectWithOptions
          propertyName={'dateFilterType'}
          value={dateFilterSettings?.dateFilterType}
          onChange={({value, propertyName}) => {
            dispatch(dateFilterChanged({propertyName, value, language}));
          }}
          options={dataFilterList(language)}
          label={translate({
            key: 'responseFilter.dateFilter',
            language,
          })}
        />
      </div>

      <div className={element('two-columns')}>
        {displayDateRangeFilter(dateFilterSettings?.dateFilterType) && (
          <>
            <Fieldset data-col1 layoutStyle={LayoutStyle.Horizontal}>
              <div className={element('date-picker')}>
                <DatePicker
                  name="startDate"
                  label={translate({
                    language,
                    key: 'responseFilter.startDate',
                  })}
                  showLabel={true}
                  format="DD MMM YYYY"
                  date={dateFilterSettings?.startDate}
                  onChange={(value) => {
                    dispatch(
                      dateFilterChanged({
                        propertyName: 'startDate',
                        value,
                        language,
                      })
                    );
                  }}
                  maxDate={dateFilterSettings?.endDate}
                  hasError={!!startDateError}
                  errorText={startDateError}
                />
              </div>

              <div className={element('date-range-time-input-wrapper')}>
                <TextField
                  type={'text'}
                  id={'startTime'}
                  maxLength={5}
                  value={dateFilterSettings?.startTime.text}
                  onChange={(value) => {
                    dispatch(
                      dateFilterChanged({
                        propertyName: 'startTime',
                        value: buildTimeFromText(value),
                        language,
                      })
                    );
                  }}
                  error={!!startTimeError}
                  helperText={startTimeError}
                />
              </div>
            </Fieldset>

            <Fieldset data-col2 layoutStyle={LayoutStyle.Horizontal}>
              <div className={element('date-picker')}>
                <DatePicker
                  name="endDate"
                  label={translate({
                    language,
                    key: 'responseFilter.endDate',
                  })}
                  showLabel={true}
                  format="DD MMM YYYY"
                  date={dateFilterSettings?.endDate}
                  onChange={(value) => {
                    dispatch(
                      dateFilterChanged({
                        propertyName: 'endDate',
                        value,
                        language,
                      })
                    );
                  }}
                  minDate={dateFilterSettings?.startDate}
                  hasError={!!endDateError || !!endDateBeforeStartDateError}
                  errorText={endDateError || endDateBeforeStartDateError}
                />
              </div>

              <div className={element('date-range-time-input-wrapper')}>
                <TextField
                  type={'text'}
                  id={'endTime'}
                  maxLength={5}
                  value={dateFilterSettings?.endTime.text}
                  onChange={(value) => {
                    dispatch(
                      dateFilterChanged({
                        propertyName: 'endTime',
                        value: buildTimeFromText(value),
                        language,
                      })
                    );
                  }}
                  error={!!endTimeError}
                  helperText={endTimeError}
                />
              </div>
            </Fieldset>
          </>
        )}
      </div>
    </>
  );
};

export default memo(DateFilter);
