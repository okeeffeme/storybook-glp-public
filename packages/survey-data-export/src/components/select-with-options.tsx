import React, {FC, ReactNode} from 'react';
import type {SelectOption} from '@jotunheim/react-simple-select';
import {bemFactory} from '@jotunheim/react-themes';

import {Select} from '@jotunheim/react-select';

import classNames from './survey-data-export.module.css';

interface OptionBase {
  value: string | number;
  label: string | number;
}
export interface OptionShape extends OptionBase {
  options?: OptionBase[];
}

export interface OnChange {
  value: SelectOption[];
  propertyName: string;
}

interface SelectWithOptions {
  propertyName: string;
  onChange: (obj: OnChange) => void;
  value?: SelectOption;
  label?: string;
  disabled?: boolean;
  helpText?: ReactNode;
  options?: OptionShape[];
}

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

const SelectWithOptions: FC<SelectWithOptions> = ({
  value,
  propertyName,
  label,
  disabled,
  helpText,
  onChange,
  options,
}) => {
  return (
    <div className={element('input-container')}>
      <Select
        data-testid={propertyName}
        data-test-select={propertyName}
        label={label}
        value={value}
        disabled={disabled}
        // to do: fix this after select is migrated to TS
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        options={options}
        // to do: fix this after select is migrated to TS
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        onChange={(value) => {
          onChange({value, propertyName});
        }}
        helperText={helpText}
      />
    </div>
  );
};

export default SelectWithOptions;
