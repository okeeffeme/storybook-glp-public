import React, {memo, useContext, useMemo, FC, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {
  rootSettingChanged,
  fileTypeChanged,
  templateSelected,
  transferSettingChanged,
} from '../state/action-creators';
import Context from '../context';
import {
  createTemplateValue,
  getIdAndTypeFromTemplateValue,
  getTemplateList,
  Template,
} from '../utils';
import {
  isExternalFTPTransfer,
  isEmailOrFtpTransfer,
  displayFileTypesSelect,
  displayTemplateListSelect,
  displayTransferTypesSelect,
} from '../utils/display-option-rules';
import {translate} from '../utils/translation';
import {fileTypesList, deliveryTypeList} from '../utils/enums';
import {DATABASE_TYPE, FILE_TYPE, TRANSFER_TYPE} from '../utils/constants';
import type {TemplateSettings} from '../utils/model-provider';
import type {ValidationErrors} from '../validation/model-validation';

import {TextField} from '@jotunheim/react-text-field';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import {ToggleButton, ToggleButtonGroup} from '@jotunheim/react-toggle-button';
import SelectWithOptions from './select-with-options';
import Textarea from './textarea';
import ExternalFtpSettings from './ExternalFtpSettings';

import classNames from './survey-data-export.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface ExportSetupProps {
  fileType?: FILE_TYPE;
  allowedFileTypes?: FILE_TYPE[];
  transferType?: TRANSFER_TYPE;
  allowedTransferTypes?: TRANSFER_TYPE[];
  emailRecipient?: string;
  disableEmailRecipient?: boolean;
  emailMessage?: string;
  templates: Template[];
  templateSettings: TemplateSettings;
  database?: DATABASE_TYPE;
  databases?: DATABASE_TYPE[];
  validationErrors?: ValidationErrors;
  externalFtpErrorMessage?: ReactNode;
  hostAndPort?: string;
  useSftp?: boolean;
  enforceSftp?: boolean;
  folderName?: string;
  userName?: string;
  password?: string;
  displayAllErrors?: boolean;
}

export const ExportSetup: FC<ExportSetupProps> = ({
  fileType,
  allowedFileTypes,
  transferType,
  allowedTransferTypes,
  emailRecipient,
  disableEmailRecipient,
  emailMessage,
  templates,
  templateSettings,
  database,
  databases = [],
  hostAndPort,
  useSftp,
  folderName,
  userName,
  password,
  enforceSftp,
  validationErrors,
  displayAllErrors,
  externalFtpErrorMessage,
}) => {
  const {dispatch, language} = useContext(Context);

  const filteredFileTypes = useMemo(
    () =>
      fileTypesList(language).filter((item) =>
        allowedFileTypes?.includes(item.value)
      ),
    [allowedFileTypes, language]
  );

  const filteredTransferTypes = useMemo(
    () =>
      deliveryTypeList(language).filter((item) =>
        allowedTransferTypes?.includes(item.value)
      ),
    [allowedTransferTypes, language]
  );

  const templateList = useMemo(() => {
    return getTemplateList({templates, language});
  }, [templates, language]);

  const handleDatabaseChange = (event) => {
    const {
      target: {value, checked},
    } = event;

    if (!checked) {
      return;
    }

    dispatch(rootSettingChanged({propertyName: 'database', value, language}));
  };

  return (
    <div>
      <Fieldset
        layoutStyle={LayoutStyle.Vertical}
        title={translate({language, key: 'exportSetup.title'})}>
        {databases.length > 1 && (
          <ToggleButtonGroup
            data-testid="survey-data-export-database-selectors"
            label={translate({language, key: 'exportSetup.dataType'})}>
            <ToggleButton
              id="production"
              text={translate({
                language,
                key: 'exportSetup.productionDatabase',
              })}
              value={DATABASE_TYPE.Production}
              checked={database === DATABASE_TYPE.Production}
              disabled={!databases.includes(DATABASE_TYPE.Production)}
              onChange={handleDatabaseChange}
            />
            <ToggleButton
              id="test"
              text={translate({language, key: 'exportSetup.testDatabase'})}
              value={DATABASE_TYPE.Test}
              checked={database === DATABASE_TYPE.Test}
              disabled={!databases.includes(DATABASE_TYPE.Test)}
              onChange={handleDatabaseChange}
            />
          </ToggleButtonGroup>
        )}

        <div className={element('two-columns')}>
          {displayFileTypesSelect(filteredFileTypes) && (
            <SelectWithOptions
              propertyName={'fileType'}
              label={translate({language, key: 'exportSetup.fileType'})}
              value={fileType}
              onChange={({value}) =>
                dispatch(fileTypeChanged({value, language}))
              }
              options={filteredFileTypes}
            />
          )}
          {displayTemplateListSelect(templateList) && (
            <SelectWithOptions
              propertyName={'idWithTemplateType'}
              label={translate({language, key: 'exportSetup.dataTemplate'})}
              value={createTemplateValue({
                id: templateSettings.id,
                templateType: templateSettings.templateType,
              })}
              onChange={({value}) => {
                const {id, templateType} = getIdAndTypeFromTemplateValue(
                  value.toString()
                );

                dispatch(templateSelected({id, templateType, language}));
              }}
              options={templateList}
            />
          )}
          {displayTransferTypesSelect(filteredTransferTypes) && (
            <SelectWithOptions
              propertyName={'transferType'}
              label={translate({language, key: 'exportSetup.deliverBy'})}
              value={transferType}
              onChange={({value}) =>
                dispatch(
                  transferSettingChanged({
                    propertyName: 'transferType',
                    value,
                    language,
                  })
                )
              }
              options={filteredTransferTypes}
            />
          )}
          {isEmailOrFtpTransfer(transferType) && (
            <div className={cn(element('input-container'))}>
              <TextField
                id="emailRecipient"
                value={emailRecipient}
                disabled={disableEmailRecipient}
                label={translate({
                  language,
                  key: 'exportSetup.emailRecipient',
                })}
                required
                onChange={(value) => {
                  dispatch(
                    transferSettingChanged({
                      propertyName: 'emailRecipient',
                      value,
                      language,
                    })
                  );
                }}
                error={!!validationErrors?.emailRecipient}
                helperText={validationErrors?.emailRecipient}
              />
            </div>
          )}
        </div>

        {isExternalFTPTransfer(transferType) && (
          <div data-testid="survey-data-export-external-ftp-settings">
            <ExternalFtpSettings
              hostAndPort={hostAndPort}
              useSftp={useSftp}
              folderName={folderName}
              userName={userName}
              password={password}
              enforceSftp={enforceSftp}
              validationErrors={validationErrors}
              displayAllErrors={displayAllErrors}
              externalFtpErrorMessage={externalFtpErrorMessage}
            />
          </div>
        )}

        {isEmailOrFtpTransfer(transferType) && (
          <Textarea
            propertyName={'comment'}
            value={emailMessage}
            label={translate({
              language,
              key: 'exportSetup.emailMessageComment',
            })}
            onChange={({value, propertyName}) =>
              dispatch(
                rootSettingChanged({
                  propertyName,
                  value,
                  language,
                })
              )
            }
          />
        )}
      </Fieldset>
    </div>
  );
};

export default memo(ExportSetup);
