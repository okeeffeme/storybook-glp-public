import React, {useRef, useContext, useLayoutEffect, FC, ReactNode} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {transferSettingChanged} from '../state/action-creators';
import Context from '../context';
import {translate} from '../utils/translation';
import type {ValidationErrors} from '../validation/model-validation';

import classNames from './survey-data-export.module.css';

import Alert from '@jotunheim/react-alert';
import {TextField} from '@jotunheim/react-text-field';
import ButtonToggle from './button-toggle';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface ExternalFtpSettingsProps {
  useSftp?: boolean;
  folderName?: string;
  hostAndPort?: string;
  userName?: string;
  password?: string;
  enforceSftp?: boolean;
  validationErrors?: ValidationErrors;
  displayAllErrors?: boolean;
  externalFtpErrorMessage?: ReactNode;
}

export const ExternalFtpSettings: FC<ExternalFtpSettingsProps> = ({
  hostAndPort,
  useSftp,
  folderName,
  userName,
  password,
  enforceSftp,
  validationErrors,
  displayAllErrors,
  externalFtpErrorMessage,
}) => {
  const {dispatch, language} = useContext(Context);
  const AlertRef = useRef<HTMLDivElement>(null);

  const shouldDisplayAlert = externalFtpErrorMessage && displayAllErrors;

  useLayoutEffect(() => {
    if (AlertRef.current?.scrollIntoView && shouldDisplayAlert) {
      AlertRef.current.scrollIntoView({behavior: 'smooth', block: 'center'});
    }
  }, [shouldDisplayAlert]);

  return (
    <>
      <ButtonToggle
        label={translate({
          language,
          key: 'exportSetup.externalFtp.useSftp',
        })}
        id="useSftp"
        disabled={enforceSftp}
        checked={enforceSftp || useSftp}
        onChange={(e) => {
          dispatch(
            transferSettingChanged({
              propertyName: 'useSftp',
              value: e.currentTarget.checked,
              language,
            })
          );
        }}
      />

      <div className={element('two-columns')}>
        <div className={cn(element('input-container'))}>
          <TextField
            id="folderName"
            value={folderName}
            label={translate({
              language,
              key: 'exportSetup.externalFtp.folderName',
            })}
            onChange={(value) => {
              dispatch(
                transferSettingChanged({
                  propertyName: 'folderName',
                  value,
                  language,
                })
              );
            }}
          />
        </div>

        <div className={cn(element('input-container'))}>
          <TextField
            id="hostAndPort"
            value={hostAndPort}
            label={translate({
              language,
              key: 'exportSetup.externalFtp.hostAndPort',
            })}
            required={true}
            onChange={(value) => {
              dispatch(
                transferSettingChanged({
                  propertyName: 'hostAndPort',
                  value,
                  language,
                })
              );
            }}
            error={displayAllErrors && !!validationErrors?.hostAndPortError}
            helperText={displayAllErrors && validationErrors?.hostAndPortError}
          />
        </div>

        <div className={cn(element('input-container'))}>
          <TextField
            id="userName"
            value={userName}
            label={translate({
              language,
              key: 'exportSetup.externalFtp.userName',
            })}
            required={true}
            onChange={(value) => {
              dispatch(
                transferSettingChanged({
                  propertyName: 'userName',
                  value,
                  language,
                })
              );
            }}
            error={displayAllErrors && !!validationErrors?.userNameError}
            helperText={displayAllErrors && validationErrors?.userNameError}
          />
        </div>

        <div className={cn(element('input-container'))}>
          <TextField
            id="password"
            type="password"
            value={password}
            label={translate({
              language,
              key: 'exportSetup.externalFtp.password',
            })}
            required={true}
            onChange={(value) => {
              dispatch(
                transferSettingChanged({
                  propertyName: 'password',
                  value,
                  language,
                })
              );
            }}
            error={displayAllErrors && !!validationErrors?.passwordError}
            helperText={displayAllErrors && validationErrors?.passwordError}
          />
        </div>
      </div>

      {shouldDisplayAlert && (
        <div ref={AlertRef}>
          <Alert
            data-testid="survey-data-export-external-ftp-error-message"
            type={Alert.types.danger}>
            {externalFtpErrorMessage}
          </Alert>
        </div>
      )}
    </>
  );
};

export default ExternalFtpSettings;
