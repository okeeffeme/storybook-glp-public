import React, {memo, useContext, FC} from 'react';
import {responseStatusChanged} from '../state/action-creators';
import Context from '../context';
import {translate} from '../utils/translation';
import {interviewStatuses} from '../utils/enums';
import {RESPONSE_STATUS_NO_FILTER} from '../utils/constants';

import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import {Radio, CheckBox} from '@jotunheim/react-toggle';

interface ResponseFilterProps {
  selectedStatuses: string[];
  availableStatuses?: string[];
}

export const ResponseFilter: FC<ResponseFilterProps> = ({
  selectedStatuses,
  availableStatuses,
}) => {
  const {dispatch, language} = useContext(Context);

  const filteredStatuses =
    availableStatuses && Array.isArray(availableStatuses)
      ? interviewStatuses(language).filter((item) =>
          availableStatuses.includes(item.value)
        )
      : interviewStatuses(language);

  return (
    <Fieldset
      layoutStyle={LayoutStyle.Horizontal}
      data-test="survey-data-export-response-filter">
      <Radio
        data-testid="survey-data-export-allStatuses"
        id="allStatuses"
        checked={selectedStatuses.length === 0}
        onChange={() =>
          dispatch(
            responseStatusChanged({
              propertyName: RESPONSE_STATUS_NO_FILTER,
              value: undefined,
            })
          )
        }>
        {translate({language, key: 'responseFilter.allStatuses'})}
      </Radio>

      {filteredStatuses.map(({value, label}) => (
        <CheckBox
          data-testid={`survey-data-export-${label}`}
          key={value}
          id={value}
          checked={selectedStatuses.indexOf(value) > -1}
          onChange={(checked) => {
            dispatch(
              responseStatusChanged({
                propertyName: value,
                value: checked,
              })
            );
          }}>
          {label}
        </CheckBox>
      ))}
    </Fieldset>
  );
};

export default memo(ResponseFilter);
