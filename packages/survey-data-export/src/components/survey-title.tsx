import React, {FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import Icon, {surveyIcon} from '@jotunheim/react-icons';

import classNames from './SurveyTitle.module.css';

interface SurveyTitleProps {
  surveyId: string;
  surveyName: string;
}

const {block, element} = bemFactory({
  baseClassName: 'comd-survey-data-export-title',
  classNames,
});

export const SurveyTitle: FC<SurveyTitleProps> = ({surveyId, surveyName}) => {
  return (
    <div className={block()} data-test="survey-data-export-title">
      <Icon className={element('icon')} path={surveyIcon} />
      <span className={element('survey-name')}>{surveyName}</span>
      <span className={element('survey-id')}>{surveyId}</span>
    </div>
  );
};

export default SurveyTitle;
