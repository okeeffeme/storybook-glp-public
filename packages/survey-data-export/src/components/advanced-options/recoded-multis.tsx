import React, {memo, useContext, FC} from 'react';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';

import ButtonToggle from '../button-toggle';

interface RecordedMultisProps {
  recodeMultis: boolean;
}

const RecodedMultis: FC<RecordedMultisProps> = ({recodeMultis}) => {
  const {dispatch, language} = useContext(Context);

  const handleRecodedMultisChange = ({target}) => {
    dispatch(
      fileSettingChanged({
        propertyName: 'recodeMultis',
        value: target.checked,
        language,
      })
    );
  };

  return (
    <ButtonToggle
      id="recodeMultis"
      label={translate({
        language,
        key: 'advancedOptions.recodeMultis',
      })}
      checked={recodeMultis}
      onChange={handleRecodedMultisChange}
    />
  );
};

export default memo(RecodedMultis);
