import React, {memo, useContext, FC} from 'react';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';
import {fileEncodingList, tripleFilesToIncludeList} from '../../utils/enums';

import SelectWithOptions from '../select-with-options';
import RecodedMultis from './recoded-multis';

interface FixedWidthOptionsProps {
  filesToInclude?: string;
  fileEncodingName?: string;
  schemaEncodingName?: string;
  recodeMultis?: boolean;
}

export const FixedWidthOptions: FC<FixedWidthOptionsProps> = ({
  filesToInclude,
  fileEncodingName,
  schemaEncodingName,
  recodeMultis = false,
}) => {
  const {dispatch, language} = useContext(Context);

  const handleFileSettingChange = ({value, propertyName}) =>
    dispatch(
      fileSettingChanged({
        propertyName,
        value,
        language,
      })
    );

  return (
    <React.Fragment>
      <SelectWithOptions
        propertyName={'filesToInclude'}
        label={translate({
          language,
          key: 'advancedOptions.filesToInclude',
        })}
        value={filesToInclude}
        options={tripleFilesToIncludeList(language)}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'fileEncodingName'}
        label={translate({
          language,
          key: 'advancedOptions.dataFileEncoding',
        })}
        value={fileEncodingName}
        options={fileEncodingList}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'schemaEncodingName'}
        label={translate({
          language,
          key: 'advancedOptions.schemaEncodingName',
        })}
        value={schemaEncodingName}
        options={fileEncodingList}
        onChange={handleFileSettingChange}
      />

      <RecodedMultis recodeMultis={recodeMultis} />
    </React.Fragment>
  );
};

export default memo(FixedWidthOptions);
