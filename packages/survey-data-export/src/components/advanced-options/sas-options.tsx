import React, {FC} from 'react';

import ExcludeHierarchyLabels from './exclude-hierachy-labels';

interface SasOptionsProps {
  excludeHierarchy?: boolean;
}

const SasOptions: FC<SasOptionsProps> = ({excludeHierarchy = false}) => (
  <ExcludeHierarchyLabels
    data-testid="sas-options"
    excludeHierarchy={excludeHierarchy}
  />
);

export default SasOptions;
