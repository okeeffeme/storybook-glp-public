import React, {useContext, useState, FC} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {transferSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {isTemplateSelected, Option} from '../../utils';
import {translate} from '../../utils/translation';

import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import ButtonToggle from '../button-toggle';
import TruncateOpenEnds from './truncate-open-ends';
import LoopHandling from './loop-handling';
import CommonOptions from './common-options';
import DelimitedTextOptions from './delimited-text-options';
import TripleSOptions from './triple-s-options';
import SpssOptions from './spss-options';
import QuantumOptions from './quantum-options';
import FixedWidthOptions from './fixed-width-options';
import EmailOptions from './email-options';
import SasOptions from './sas-options';
import OverrideFileName from './override-file-name';

import {
  isFileDelimitedTextDescendant,
  isFileTripleSDescendant,
  isFileSpssDescendant,
  isQuantumFile,
  isSasFile,
  isFixedWidthFile,
  displayTruncateOpenEnds,
  isEmailTransfer,
  displayLoopHandling,
} from '../../utils/display-option-rules';

import classNames from '../survey-data-export.module.css';
import {Model} from '../../utils/model-provider';
import {ValidationErrors} from '../../validation/model-validation';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface AdvancedOptionsProps {
  collapsed?: boolean;
  validationErrors?: ValidationErrors;
  model?: Model;
  surveyLanguages?: Option[];
  localeList?: Option[];
  displayAllErrors?: boolean;
}

const AdvancedOptions: FC<AdvancedOptionsProps> = ({
  collapsed,
  model,
  //   {
  //   questionLabelText,
  //   answerElementLabels,
  //   exportLanguageId,
  //   fileSettings,
  //   transferSettings,
  //   templateSettings: {id, textInLabels},
  // },
  surveyLanguages,
  localeList,
  validationErrors,
  displayAllErrors,
}) => {
  const {dispatch, language} = useContext(Context);

  const [advancedSettingsToggle, setAdvancedSettingsToggle] = useState(
    !collapsed
  );

  return (
    <Fieldset data-testid="advanced-options" layoutStyle={LayoutStyle.Vertical}>
      <ButtonToggle
        label={translate({language, key: 'advancedOptions.title'})}
        id={'advancedOptionsToggle'}
        checked={advancedSettingsToggle}
        onChange={() => {
          setAdvancedSettingsToggle(!advancedSettingsToggle);
        }}
      />

      {advancedSettingsToggle && (
        <div className={cn(element('two-columns'), element('panel-body'))}>
          {displayLoopHandling(model?.fileSettings.fileType) && (
            <LoopHandling
              loopHandling={model?.loopHandling}
              loopPosition={model?.loopPosition}
            />
          )}

          {displayTruncateOpenEnds(model?.fileSettings.fileType) && (
            <TruncateOpenEnds
              fileType={model?.fileSettings.fileType}
              truncateOpenEnds={model?.fileSettings?.truncateOpenEnds}
              truncateOpenEndsError={validationErrors?.truncateOpenEnds}
              truncateOpenEndsEnabled={
                model?.fileSettings?.truncateOpenEndsEnabled
              }
            />
          )}
          <CommonOptions
            answerElementLabels={model?.answerElementLabels}
            questionLabelText={model?.questionLabelText}
            exportLanguageId={model?.exportLanguageId}
            surveyLanguages={surveyLanguages}
            textInLabels={model?.templateSettings?.textInLabels}
            isTemplateSelected={isTemplateSelected(model?.templateSettings?.id)}
          />

          <div className={element('one-column')}>
            <ButtonToggle
              label={translate({
                language,
                key: 'advancedOptions.deliverFilesUncompressed',
              })}
              id={'uncompressed'}
              checked={model?.transferSettings.uncompressed}
              onChange={({target}) => {
                dispatch(
                  transferSettingChanged({
                    propertyName: 'uncompressed',
                    value: target.checked,
                    language,
                  })
                );
              }}
            />
            <OverrideFileName
              overrideFileName={model?.transferSettings.overrideFileName}
              overrideFileNameError={validationErrors?.overrideFileName}
              overrideFileNameEnabled={
                model?.transferSettings.overrideFileNameEnabled
              }
              transferType={model?.transferSettings.transferType}
            />
          </div>

          {isFileDelimitedTextDescendant(model?.fileSettings.fileType) && (
            <DelimitedTextOptions
              locale={model?.fileSettings.locale}
              mediaFiles={model?.fileSettings.mediaFiles}
              openEndHandling={model?.fileSettings.openEndHandling}
              fileEncodingName={model?.fileSettings.fileEncodingName}
              formatting={model?.fileSettings.formatting}
              localeList={localeList}
              formattingDelimiterError={validationErrors?.formattingDelimiter}
            />
          )}

          {isFileTripleSDescendant(model?.fileSettings.fileType) && (
            <TripleSOptions
              version={model?.fileSettings.version}
              format={model?.fileSettings.format}
              multiQuestionFormat={model?.fileSettings.multiQuestionFormat}
              filesToInclude={model?.fileSettings.filesToInclude}
              fileEncodingName={model?.fileSettings.fileEncodingName}
              schemaEncodingName={model?.fileSettings.schemaEncodingName}
              recodeMultis={model?.fileSettings.recodeMultis}
              excludeHierarchy={model?.fileSettings.excludeHierarchy}
            />
          )}

          {isFileSpssDescendant(model?.fileSettings.fileType) && (
            <SpssOptions
              fileType={model?.fileSettings.fileType}
              excludeHierarchy={model?.fileSettings.excludeHierarchy}
              characterEncodingName={model?.fileSettings.characterEncodingName}
              decimalDelimiter={model?.fileSettings.decimalDelimiter}
              includeAnswerLabelMultiQuestions={
                model?.fileSettings.includeAnswerLabelMultiQuestions
              }
            />
          )}

          {isQuantumFile(model?.fileSettings.fileType) && (
            <QuantumOptions
              excludeHierarchy={model?.fileSettings.excludeHierarchy}
              recodeMultis={model?.fileSettings.recodeMultis}
            />
          )}

          {isSasFile(model?.fileSettings.fileType) && (
            <SasOptions
              excludeHierarchy={model?.fileSettings.excludeHierarchy}
            />
          )}

          {isFixedWidthFile(model?.fileSettings.fileType) && (
            <FixedWidthOptions
              filesToInclude={model?.fileSettings.filesToInclude}
              fileEncodingName={model?.fileSettings.fileEncodingName}
              schemaEncodingName={model?.fileSettings.schemaEncodingName}
              recodeMultis={model?.fileSettings.recodeMultis}
            />
          )}

          {isEmailTransfer(model?.transferSettings.transferType) && (
            <EmailOptions
              emailOverrideEnabled={
                model?.transferSettings.emailOverride?.emailOverrideEnabled
              }
              mailEncodingName={
                model?.transferSettings.emailOverride?.mailEncodingName
              }
              replyTo={model?.transferSettings.emailOverride?.replyTo}
              subject={model?.transferSettings.emailOverride?.subject}
              plainTextBody={
                model?.transferSettings.emailOverride?.plainTextBody
              }
              replyToError={validationErrors?.replyTo}
              subjectError={validationErrors?.subject}
              plainTextBodyError={validationErrors?.plainTextBody}
              displayAllErrors={displayAllErrors}
            />
          )}
        </div>
      )}
    </Fieldset>
  );
};

export default AdvancedOptions;
