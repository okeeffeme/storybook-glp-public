import React, {memo, useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {
  fileSettingChanged,
  formattingSettingChanged,
  mediaSettingChanged,
} from '../../state/action-creators';
import Context from '../../context';
import {
  displayCustomDelimiter,
  displayTextQualifier,
} from '../../utils/display-option-rules';
import {translate} from '../../utils/translation';
import {
  fileEncodingList,
  fileFormatTypeList,
  mediaFileNamingList,
  openEndHandlingList,
} from '../../utils/enums';
import type {MediaFiles, Formatting} from '../../utils/model-provider';
import type {Option} from '../../utils';

import classNames from '../survey-data-export.module.css';

import {TextField} from '@jotunheim/react-text-field';
import SelectWithOptions from '../select-with-options';
import ButtonToggle from '../button-toggle';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface DelimitedTextOptionsProps {
  locale?: number;
  mediaFiles?: MediaFiles;
  openEndHandling?: string;
  fileEncodingName?: string;
  formatting?: Formatting;
  localeList?: Option[];
  formattingDelimiterError?: string;
}

export const DelimitedTextOptions: FC<DelimitedTextOptionsProps> = ({
  locale,
  mediaFiles,
  openEndHandling,
  fileEncodingName,
  formatting,
  localeList = [],
  formattingDelimiterError,
}) => {
  const {dispatch, language} = useContext(Context);

  const handleFileSettingChange = ({value, propertyName}) =>
    dispatch(
      fileSettingChanged({
        propertyName,
        value,
        language,
      })
    );

  return (
    <React.Fragment>
      <SelectWithOptions
        propertyName={'fileFormatType'}
        label={translate({
          language,
          key: 'advancedOptions.fileFormatType',
        })}
        value={formatting?.fileFormatType}
        options={fileFormatTypeList(language)}
        onChange={({value, propertyName}) =>
          dispatch(
            formattingSettingChanged({
              propertyName,
              value,
              language,
            })
          )
        }
      />

      {displayCustomDelimiter(formatting?.fileFormatType) && (
        <div className={element('input-container')}>
          <TextField
            id="delimiter"
            value={formatting?.delimiter}
            label={translate({
              language,
              key: 'advancedOptions.delimiter',
            })}
            onChange={(value) => {
              dispatch(
                formattingSettingChanged({
                  propertyName: 'delimiter',
                  value,
                  language,
                })
              );
            }}
            error={!!formattingDelimiterError}
            helperText={formattingDelimiterError}
          />
        </div>
      )}

      {displayTextQualifier(formatting?.fileFormatType) && (
        <ButtonToggle
          label={translate({
            language,
            key: 'advancedOptions.quotedValues',
          })}
          id={'quotedValues'}
          checked={formatting?.quotedValues}
          onChange={({target}) => {
            dispatch(
              formattingSettingChanged({
                propertyName: 'quotedValues',
                value: target.checked,
                language,
              })
            );
          }}
        />
      )}

      <SelectWithOptions
        propertyName={'locale'}
        label={translate({
          language,
          key: 'advancedOptions.locale',
        })}
        value={locale}
        options={localeList}
        onChange={handleFileSettingChange}
      />

      <ButtonToggle
        label={translate({
          language,
          key: 'advancedOptions.includeMedia',
        })}
        id={'includeMedia'}
        checked={mediaFiles?.includeMedia}
        onChange={({target}) => {
          dispatch(
            mediaSettingChanged({
              propertyName: 'includeMedia',
              value: target.checked,
              language,
            })
          );
        }}
      />

      {mediaFiles?.includeMedia && (
        <SelectWithOptions
          propertyName={'mediaFileNaming'}
          label={translate({
            language,
            key: 'advancedOptions.mediaFileNaming',
          })}
          value={mediaFiles?.mediaFileNaming}
          options={mediaFileNamingList}
          onChange={({value, propertyName}) =>
            dispatch(
              mediaSettingChanged({
                propertyName,
                value,
                language,
              })
            )
          }
        />
      )}

      <SelectWithOptions
        propertyName={'openEndHandling'}
        label={translate({
          language,
          key: 'advancedOptions.openEndHandling',
        })}
        value={openEndHandling}
        options={openEndHandlingList(language)}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'fileEncodingName'}
        label={translate({
          language,
          key: 'advancedOptions.fileEncodingName',
        })}
        value={fileEncodingName}
        options={fileEncodingList}
        onChange={handleFileSettingChange}
      />
    </React.Fragment>
  );
};

export default memo(DelimitedTextOptions);
