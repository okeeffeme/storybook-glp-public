import React, {memo, useContext, FC} from 'react';
import {
  rootSettingChanged,
  templateSettingChanged,
} from '../../state/action-creators';
import Context from '../../context';
import {
  answerElementLabelsList,
  questionLabelTextList,
  textInLabelsList,
} from '../../utils/enums';
import {translate} from '../../utils/translation';
import {TEXT_IN_LABELS} from '../../utils/constants';
import type {Option} from '../../utils';

import SelectWithOptions from '../select-with-options';

interface CommonOptionsProps {
  questionLabelText?: string;
  answerElementLabels?: string;
  exportLanguageId?: number;
  surveyLanguages?: Option[];
  textInLabels?: string;
  isTemplateSelected?: boolean;
}

export const CommonOptions: FC<CommonOptionsProps> = ({
  questionLabelText,
  answerElementLabels,
  exportLanguageId,
  surveyLanguages = [],
  textInLabels,
  isTemplateSelected,
}) => {
  const {dispatch, language} = useContext(Context);

  const overrideWithTemplate =
    isTemplateSelected && textInLabels === TEXT_IN_LABELS.Template;

  const handleRootSettingChange = ({value, propertyName}) =>
    dispatch(
      rootSettingChanged({
        propertyName,
        value,
        language,
      })
    );

  return (
    <React.Fragment>
      {isTemplateSelected && (
        <SelectWithOptions
          propertyName={'textInLabels'}
          label={translate({language, key: 'exportSetup.textInLabels'})}
          value={textInLabels}
          onChange={({value, propertyName}) =>
            dispatch(
              templateSettingChanged({
                propertyName,
                value,
                language,
              })
            )
          }
          options={textInLabelsList(language)}
        />
      )}

      {overrideWithTemplate === false && (
        <React.Fragment>
          <SelectWithOptions
            propertyName={'questionLabelText'}
            label={translate({
              language,
              key: 'advancedOptions.questionLabelText',
            })}
            value={questionLabelText}
            options={questionLabelTextList(language)}
            onChange={handleRootSettingChange}
          />

          <SelectWithOptions
            propertyName={'answerElementLabels'}
            label={translate({
              language,
              key: 'advancedOptions.answerElementLabels',
            })}
            value={answerElementLabels}
            options={answerElementLabelsList(language)}
            onChange={handleRootSettingChange}
          />

          <SelectWithOptions
            propertyName={'exportLanguageId'}
            label={translate({
              language,
              key: 'advancedOptions.exportLanguage',
            })}
            value={exportLanguageId}
            options={surveyLanguages}
            onChange={handleRootSettingChange}
          />
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default memo(CommonOptions);

// uncomment to see what changes between renders
// export default React.memo(CommonOptions, (prevProps, nextProps) => {
//   const prevKeys = Object.keys(prevProps);
//
//   prevKeys.forEach(key => {
//     if (prevProps[key] !== nextProps[key]) {
//       console.log(key, 'has changed');
//     }
//   });
// });
