import React, {memo, useContext, FC} from 'react';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';

import ButtonToggle from '../button-toggle';

interface ExcludeHierarchyLabelsProps {
  excludeHierarchy: boolean;
}

const ExcludeHierarchyLabels: FC<ExcludeHierarchyLabelsProps> = ({
  excludeHierarchy,
}) => {
  const {dispatch, language} = useContext(Context);

  const handleExcludeHierarchyChange = ({target}) => {
    dispatch(
      fileSettingChanged({
        propertyName: 'excludeHierarchy',
        value: target.checked,
        language,
      })
    );
  };

  return (
    <ButtonToggle
      label={translate({
        language,
        key: 'advancedOptions.excludeHierarchy',
      })}
      id={'excludeHierarchy'}
      checked={excludeHierarchy}
      onChange={handleExcludeHierarchyChange}
    />
  );
};

export default memo(ExcludeHierarchyLabels);
