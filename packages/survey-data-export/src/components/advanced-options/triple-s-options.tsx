import React, {memo, useContext, FC} from 'react';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';
import {
  fileEncodingList,
  tripleFilesToIncludeList,
  tripleSFormatList,
  tripleSMultiQuestionFormatList,
  tripleSVersionList,
} from '../../utils/enums';

import SelectWithOptions from '../select-with-options';
import RecodedMultis from './recoded-multis';
import ExcludeHierarchyLabels from './exclude-hierachy-labels';

interface TripleSOptionsProps {
  version?: string;
  format?: string;
  multiQuestionFormat?: string;
  filesToInclude?: string;
  fileEncodingName?: string;
  schemaEncodingName?: string;
  recodeMultis?: boolean;
  excludeHierarchy?: boolean;
}

export const TripleSOptions: FC<TripleSOptionsProps> = ({
  version,
  format,
  multiQuestionFormat,
  filesToInclude,
  fileEncodingName,
  schemaEncodingName,
  recodeMultis = false,
  excludeHierarchy = false,
}) => {
  const {dispatch, language} = useContext(Context);

  const handleFileSettingChange = ({value, propertyName}) =>
    dispatch(
      fileSettingChanged({
        propertyName,
        value,
        language,
      })
    );

  return (
    <React.Fragment>
      <SelectWithOptions
        propertyName={'version'}
        label={translate({
          language,
          key: 'advancedOptions.version',
        })}
        value={version}
        options={tripleSVersionList}
        onChange={handleFileSettingChange}
      />
      <SelectWithOptions
        propertyName={'format'}
        label={translate({
          language,
          key: 'advancedOptions.format',
        })}
        value={format}
        options={tripleSFormatList(language)}
        onChange={handleFileSettingChange}
      />
      <SelectWithOptions
        propertyName={'multiQuestionFormat'}
        label={translate({
          language,
          key: 'advancedOptions.multiQuestionFormat',
        })}
        value={multiQuestionFormat}
        options={tripleSMultiQuestionFormatList}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'filesToInclude'}
        label={translate({
          language,
          key: 'advancedOptions.filesToInclude',
        })}
        value={filesToInclude}
        options={tripleFilesToIncludeList(language)}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'fileEncodingName'}
        label={translate({
          language,
          key: 'advancedOptions.dataFileEncoding',
        })}
        value={fileEncodingName}
        options={fileEncodingList}
        onChange={handleFileSettingChange}
      />

      <SelectWithOptions
        propertyName={'schemaEncodingName'}
        label={translate({
          language,
          key: 'advancedOptions.schemaEncodingName',
        })}
        value={schemaEncodingName}
        options={fileEncodingList}
        onChange={handleFileSettingChange}
      />

      <RecodedMultis recodeMultis={recodeMultis} />
      <ExcludeHierarchyLabels excludeHierarchy={excludeHierarchy} />
    </React.Fragment>
  );
};

export default memo(TripleSOptions);
