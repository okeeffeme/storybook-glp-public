import React, {FC, memo, useContext, useState} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {emailOverrideSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';
import {fileEncodingList} from '../../utils/enums';

import {TextField} from '@jotunheim/react-text-field';
import SelectWithOptions from '../select-with-options';
import ButtonToggle from '../button-toggle';
import Textarea from '../textarea';

import classNames from '../survey-data-export.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface EmailOptionsProps {
  mailEncodingName?: string;
  emailOverrideEnabled?: boolean;
  replyTo?: string;
  replyToError?: string;
  subject?: string;
  subjectError?: string;
  plainTextBody?: string;
  plainTextBodyError?: string;
  displayAllErrors?: boolean;
}

export const EmailOptions: FC<EmailOptionsProps> = ({
  emailOverrideEnabled,
  mailEncodingName,
  replyTo,
  subject,
  plainTextBody,
  replyToError,
  subjectError,
  plainTextBodyError,
  displayAllErrors,
}) => {
  const {dispatch, language} = useContext(Context);

  const [isInteractedSubject, setIsInteractedSubject] = useState(false);
  const [isInteractedPlainTextBody, setIsInteractedPlainTextBody] =
    useState(false);

  const handleEmailOverrideSettingChange = ({value, propertyName}) => {
    if (propertyName === 'plainTextBody') {
      setIsInteractedPlainTextBody(true);
    }
    dispatch(
      emailOverrideSettingChanged({
        propertyName,
        value,
        language,
      })
    );
  };

  return (
    <div data-testid="email-options" className={element('one-column')}>
      <ButtonToggle
        label={translate({
          language,
          key: 'advancedOptions.emailOverrideEnabled',
        })}
        id={'emailOverrideEnabled'}
        checked={emailOverrideEnabled}
        onChange={({target}) => {
          dispatch(
            emailOverrideSettingChanged({
              propertyName: 'emailOverrideEnabled',
              value: target.checked,
              language,
            })
          );
        }}
      />

      {emailOverrideEnabled && (
        <React.Fragment>
          <div className={element('two-columns')}>
            <SelectWithOptions
              propertyName={'mailEncodingName'}
              label={translate({
                language,
                key: 'advancedOptions.mailEncodingName',
              })}
              value={mailEncodingName}
              options={fileEncodingList}
              onChange={handleEmailOverrideSettingChange}
            />
            <div className={cn(element('input-container'))}>
              <TextField
                id="replyTo"
                value={replyTo}
                label={translate({
                  language,
                  key: 'advancedOptions.replyTo',
                })}
                onChange={(value) => {
                  dispatch(
                    emailOverrideSettingChanged({
                      propertyName: 'replyTo',
                      value,
                      language,
                    })
                  );
                }}
                error={!!replyToError}
                helperText={replyToError}
              />
            </div>
          </div>

          <div className={element('one-column')}>
            <div className={cn(element('input-container'))}>
              <TextField
                id="subject"
                value={subject}
                label={translate({
                  language,
                  key: 'advancedOptions.subject',
                })}
                required
                onChange={(value) => {
                  setIsInteractedSubject(true);
                  dispatch(
                    emailOverrideSettingChanged({
                      propertyName: 'subject',
                      value,
                      language,
                    })
                  );
                }}
                error={
                  (isInteractedSubject || displayAllErrors) && !!subjectError
                }
                helperText={
                  (isInteractedSubject || displayAllErrors) && subjectError
                }
              />
            </div>
          </div>
          <div className={element('one-column')}>
            <Textarea
              propertyName={'plainTextBody'}
              value={plainTextBody}
              onChange={handleEmailOverrideSettingChange}
              label={translate({
                language,
                key: 'advancedOptions.plainTextBody',
              })}
              required
              error={
                (isInteractedPlainTextBody || displayAllErrors) &&
                !!plainTextBodyError
              }
              helperText={
                (isInteractedPlainTextBody || displayAllErrors) &&
                plainTextBodyError
              }
            />
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

export default memo(EmailOptions);
