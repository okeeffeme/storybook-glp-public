import React, {memo, useContext, FC} from 'react';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {fileEncodingList, spssDecimalLimiterList} from '../../utils/enums';
import {
  displayCharacterEncodingName,
  displayDecimalDelimiter,
} from '../../utils/display-option-rules';
import {translate} from '../../utils/translation';
import type {FILE_TYPE} from '../../utils/constants';

import ButtonToggle from '../button-toggle';
import SelectWithOptions from '../select-with-options';
import ExcludeHierarchyLabels from './exclude-hierachy-labels';

interface SpssOptionsProps {
  fileType?: FILE_TYPE;
  excludeHierarchy?: boolean;
  characterEncodingName?: string;
  decimalDelimiter?: string;
  includeAnswerLabelMultiQuestions?: boolean;
}

export const SpssOptions: FC<SpssOptionsProps> = ({
  fileType,
  excludeHierarchy = false,
  characterEncodingName,
  decimalDelimiter,
  includeAnswerLabelMultiQuestions,
}) => {
  const {dispatch, language} = useContext(Context);

  const handleFileSettingChange = ({value, propertyName}) =>
    dispatch(
      fileSettingChanged({
        propertyName,
        value,
        language,
      })
    );

  return (
    <React.Fragment>
      <ExcludeHierarchyLabels excludeHierarchy={excludeHierarchy} />

      {displayCharacterEncodingName(fileType) && (
        <SelectWithOptions
          propertyName={'characterEncodingName'}
          label={translate({
            language,
            key: 'advancedOptions.characterEncodingName',
          })}
          value={characterEncodingName}
          options={fileEncodingList}
          onChange={handleFileSettingChange}
        />
      )}

      {displayDecimalDelimiter(fileType) && (
        <SelectWithOptions
          propertyName={'decimalDelimiter'}
          label={translate({
            language,
            key: 'advancedOptions.decimalDelimiter',
          })}
          value={decimalDelimiter}
          options={spssDecimalLimiterList(language)}
          onChange={handleFileSettingChange}
        />
      )}

      <ButtonToggle
        label={translate({
          language,
          key: 'advancedOptions.includeAnswerLabelMultiQuestions',
        })}
        id={'includeAnswerLabelMultiQuestions'}
        checked={includeAnswerLabelMultiQuestions}
        onChange={({target}) => {
          dispatch(
            fileSettingChanged({
              propertyName: 'includeAnswerLabelMultiQuestions',
              value: target.checked,
              language,
            })
          );
        }}
      />
    </React.Fragment>
  );
};

export default memo(SpssOptions);
