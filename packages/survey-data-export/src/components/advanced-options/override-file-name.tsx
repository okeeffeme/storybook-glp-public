import React, {memo, useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {transferSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';
import {TRANSFER_TYPE} from '../../utils/constants';
import {isDownloadTransfer} from '../../utils/display-option-rules';

import {Alert} from '@jotunheim/react-alert';
import {TextField} from '@jotunheim/react-text-field';
import ButtonToggle from '../button-toggle';

import classNames from '../survey-data-export.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface OverrideFileNameProps {
  overrideFileName?: string;
  overrideFileNameError?: string;
  overrideFileNameEnabled?: boolean;
  transferType?: TRANSFER_TYPE;
}

export const OverrideFileName: FC<OverrideFileNameProps> = ({
  overrideFileNameEnabled,
  overrideFileName,
  overrideFileNameError,
  transferType,
}) => {
  const {dispatch, language} = useContext(Context);

  return (
    <React.Fragment>
      <ButtonToggle
        label={translate({
          language,
          key: 'advancedOptions.overrideFileNameEnabled',
        })}
        id={'overrideFileNameEnabled'}
        checked={overrideFileNameEnabled}
        onChange={({target}) => {
          dispatch(
            transferSettingChanged({
              propertyName: 'overrideFileNameEnabled',
              value: target.checked,
              language,
            })
          );
        }}
      />
      {overrideFileNameEnabled && (
        <React.Fragment>
          <div className={element('input-container')}>
            <Alert type={Alert.types.info}>
              {translate({
                language,
                key: 'advancedOptions.overrideFileNameInfo1',
              })}
              <div className={element('override-file-name-keywords')}>
                <div>
                  <div>PROJECTID</div>
                  <div>TASKID</div>
                  <div>USERID</div>
                </div>
                <div>
                  <div>NOW</div>
                  <div>YEAR</div>
                  <div>MONTH</div>
                </div>
                <div>
                  <div>DAY</div>
                  <div>HOUR</div>
                  <div>MINUTE</div>
                </div>
                {!isDownloadTransfer(transferType) ? (
                  <>
                    <div>
                      <div>START</div>
                      <div>STARTYEAR</div>
                      <div>STARTMONTH</div>
                      <div>STARTDAY</div>
                    </div>
                    <div>
                      <div>END</div>
                      <div>ENDYEAR</div>
                      <div>ENDMONTH</div>
                      <div>ENDDAY</div>
                    </div>
                  </>
                ) : (
                  <>
                    <div></div>
                    <div></div>
                  </>
                )}
              </div>
              {translate({
                language,
                key: 'advancedOptions.overrideFileNameInfo2',
              })}
            </Alert>
          </div>
          <div className={element('input-container')}>
            <TextField
              id="overrideFileName"
              value={overrideFileName}
              label={translate({
                language,
                key: 'advancedOptions.overrideFileName',
              })}
              onChange={(value) => {
                dispatch(
                  transferSettingChanged({
                    propertyName: 'overrideFileName',
                    value,
                    language,
                  })
                );
              }}
              error={!!overrideFileNameError}
              helperText={overrideFileNameError}
            />
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default memo(OverrideFileName);
