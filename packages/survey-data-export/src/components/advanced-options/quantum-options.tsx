import React, {FC} from 'react';

import ExcludeHierarchyLabels from './exclude-hierachy-labels';
import RecodedMultis from './recoded-multis';

interface QuantumOptionsProps {
  excludeHierarchy?: boolean;
  recodeMultis?: boolean;
}

const QuantumOptions: FC<QuantumOptionsProps> = ({
  excludeHierarchy = false,
  recodeMultis = false,
}) => (
  <React.Fragment>
    <RecodedMultis data-testid="quantum-options" recodeMultis={recodeMultis} />
    <ExcludeHierarchyLabels excludeHierarchy={excludeHierarchy} />
  </React.Fragment>
);

export default QuantumOptions;
