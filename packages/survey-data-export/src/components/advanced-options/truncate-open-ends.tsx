import React, {memo, useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {fileSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {enforceOpenEndsTruncation} from '../../utils/display-option-rules';
import {translate} from '../../utils/translation';
import type {FILE_TYPE} from '../../utils/constants';

import {TextField} from '@jotunheim/react-text-field';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import {Switch} from '@jotunheim/react-switch';

import classNames from '../survey-data-export.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

interface TruncateOpenEndsProps {
  truncateOpenEndsEnabled?: boolean;
  truncateOpenEnds?: string;
  truncateOpenEndsError?: string;
  fileType?: FILE_TYPE;
}

const TruncateOpenEnds: FC<TruncateOpenEndsProps> = ({
  fileType,
  truncateOpenEndsEnabled,
  truncateOpenEnds,
  truncateOpenEndsError,
}) => {
  const {dispatch, language} = useContext(Context);

  return (
    <div
      data-testid="truncate-open-ends"
      className={element('input-container')}>
      <Fieldset layoutStyle={LayoutStyle.Horizontal}>
        <div>
          <Switch
            label={translate({
              language,
              key: 'advancedOptions.truncateOpenEnds',
            })}
            id={'truncateOpenEndsEnabled'}
            disabled={enforceOpenEndsTruncation(fileType)}
            checked={truncateOpenEndsEnabled}
            onChange={({target}) => {
              dispatch(
                fileSettingChanged({
                  propertyName: 'truncateOpenEndsEnabled',
                  value: target.checked,
                  language,
                })
              );
            }}
          />
        </div>
        {truncateOpenEndsEnabled && (
          <div className={element('open-ends-character-limit')}>
            <TextField
              id="truncateOpenEnds"
              value={truncateOpenEnds}
              label={translate({
                language,
                key: 'advancedOptions.characterLimit',
              })}
              disabled={!truncateOpenEndsEnabled}
              onChange={(value) => {
                dispatch(
                  fileSettingChanged({
                    propertyName: 'truncateOpenEnds',
                    value,
                    language,
                  })
                );
              }}
              error={!!truncateOpenEndsError}
              helperText={truncateOpenEndsError}
            />
          </div>
        )}
      </Fieldset>
    </div>
  );
};

export default memo(TruncateOpenEnds);
