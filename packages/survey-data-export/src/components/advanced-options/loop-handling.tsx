import React, {memo, useContext, FC} from 'react';
import {rootSettingChanged} from '../../state/action-creators';
import Context from '../../context';
import {translate} from '../../utils/translation';
import {loopHandlingList, loopPositionList} from '../../utils/enums';
import {LOOP_HANDLING} from '../../utils/constants';

import SelectWithOptions from '../select-with-options';

interface LoopHandlingProps {
  loopHandling?: string;
  loopPosition?: string;
}

export const LoopHandling: FC<LoopHandlingProps> = ({
  loopHandling,
  loopPosition,
}) => {
  const {dispatch, language} = useContext(Context);

  return (
    <React.Fragment>
      <SelectWithOptions
        propertyName={'loopHandling'}
        label={translate({
          language,
          key: 'advancedOptions.loopHandling',
        })}
        value={loopHandling}
        helpText={translate({
          language,
          key: 'advancedOptions.loopHandlingHelpText',
        })}
        options={loopHandlingList(language)}
        onChange={({value, propertyName}) =>
          dispatch(
            rootSettingChanged({
              propertyName,
              value,
              language,
            })
          )
        }
      />
      <SelectWithOptions
        propertyName={'loopPosition'}
        label={translate({
          language,
          key: 'advancedOptions.loopPosition',
        })}
        value={loopPosition}
        disabled={loopHandling === LOOP_HANDLING.SeparateFiles}
        options={loopPositionList(language)}
        onChange={({value, propertyName}) =>
          dispatch(
            rootSettingChanged({
              propertyName,
              value,
              language,
            })
          )
        }
      />
    </React.Fragment>
  );
};

export default memo(LoopHandling);
