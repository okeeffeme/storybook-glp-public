import React, {FC, ReactNode} from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import {TextArea} from '@jotunheim/react-textarea';

import classNames from './survey-data-export.module.css';

interface OnChange {
  value: string;
  propertyName: string;
}

interface TextareaProps {
  propertyName: string;
  value?: string;
  label?: string;
  onChange: (obj: OnChange) => void;
  required?: boolean;
  error?: boolean;
  helperText?: ReactNode;
}

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

const Textarea: FC<TextareaProps> = ({
  propertyName,
  value,
  label,
  onChange,
  required,
  error,
  helperText,
}) => {
  const handleTextAreaChange = (value) => onChange({value, propertyName});

  return (
    <div className={element('input-container')}>
      <TextArea
        rows={8}
        resizable={true}
        label={label}
        onChange={handleTextAreaChange}
        value={value}
        showMaxLength={true}
        maxLength={255}
        required={required}
        error={error}
        helperText={helperText}
      />
    </div>
  );
};

export default Textarea;
