import React, {memo, useContext, useMemo, FC} from 'react';
import {
  rootSettingChanged,
  schedulerSettingsChanged,
  schedulerValidationChanged,
} from '../state/action-creators';
import Context from '../context';
import {translate} from '../utils/translation';
import {SCHEDULE_TYPE, TRANSFER_TYPE} from '../utils/constants';
import type {ExecutionSettings} from '../utils/model-provider';

import Scheduler from '@jotunheim/react-scheduler';
import {ToggleButtonGroup, ToggleButton} from '@jotunheim/react-toggle-button';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';
import ButtonToggle from './button-toggle';

interface SchedulingProps {
  className?: string;
  transferType?: TRANSFER_TYPE;
  executionSettings?: ExecutionSettings;
  onlyDataChangedSinceLastRun?: boolean;
}

export const Scheduling: FC<SchedulingProps> = ({
  transferType,
  executionSettings,
  onlyDataChangedSinceLastRun,
}) => {
  const {dispatch, language} = useContext(Context);

  const texts = useMemo(
    () => ({
      OneTime: translate({language, key: 'scheduling.scheduler.oneTime'}),
      Hourly: translate({language, key: 'scheduling.scheduler.hourly'}),
      Daily: translate({language, key: 'scheduling.scheduler.daily'}),
      Weekly: translate({language, key: 'scheduling.scheduler.weekly'}),
      Monthly: translate({language, key: 'scheduling.scheduler.monthly'}),
      days: translate({language, key: 'scheduling.scheduler.days'}),
      weeks: translate({language, key: 'scheduling.scheduler.weeks'}),
      months: translate({language, key: 'scheduling.scheduler.months'}),
      day: translate({language, key: 'scheduling.scheduler.day'}),
      every: translate({language, key: 'scheduling.scheduler.every'}),
      ofEvery: translate({language, key: 'scheduling.scheduler.ofEvery'}),
      repeats: translate({language, key: 'scheduling.scheduler.repeats'}),
      repeatsOn: translate({language, key: 'scheduling.scheduler.repeatsOn'}),
      starts: translate({language, key: 'scheduling.scheduler.starts'}),
      ends: translate({language, key: 'scheduling.scheduler.ends'}),
      monday: translate({language, key: 'scheduling.scheduler.monday'}),
      tuesday: translate({language, key: 'scheduling.scheduler.tuesday'}),
      wednesday: translate({language, key: 'scheduling.scheduler.wednesday'}),
      thursday: translate({language, key: 'scheduling.scheduler.thursday'}),
      friday: translate({language, key: 'scheduling.scheduler.friday'}),
      saturday: translate({language, key: 'scheduling.scheduler.saturday'}),
      sunday: translate({language, key: 'scheduling.scheduler.sunday'}),
      mondayLong: translate({language, key: 'scheduling.scheduler.mondayLong'}),
      tuesdayLong: translate({
        language,
        key: 'scheduling.scheduler.tuesdayLong',
      }),
      wednesdayLong: translate({
        language,
        key: 'scheduling.scheduler.wednesdayLong',
      }),
      thursdayLong: translate({
        language,
        key: 'scheduling.scheduler.thursdayLong',
      }),
      fridayLong: translate({language, key: 'scheduling.scheduler.fridayLong'}),
      saturdayLong: translate({
        language,
        key: 'scheduling.scheduler.saturdayLong',
      }),
      sundayLong: translate({language, key: 'scheduling.scheduler.sundayLong'}),
      invalidDateError: translate({
        language,
        key: 'scheduling.scheduler.invalidDateError',
      }),
      invalidStartDateError: translate({
        language,
        key: 'scheduling.scheduler.invalidStartDateError',
      }),
      invalidDateTimeErrorMinDateTime: translate({
        language,
        key: 'scheduling.scheduler.invalidDateTimeErrorMinDateTime',
      }),
      invalidEndDateError: translate({
        language,
        key: 'scheduling.scheduler.invalidEndDateError',
      }),
      invalidTimeError: translate({
        language,
        key: 'scheduling.scheduler.invalidTimeError',
      }),
      invalidIntervalError: translate({
        language,
        key: 'scheduling.scheduler.invalidIntervalError',
      }),
      invalidWeekIntervalError: translate({
        language,
        key: 'scheduling.scheduler.invalidWeekIntervalError',
      }),
      invalidMonthDayError: translate({
        language,
        key: 'scheduling.scheduler.invalidMonthDayError',
      }),
      invalidWeekDaysError: translate({
        language,
        key: 'scheduling.scheduler.invalidWeekDaysError',
      }),
      summary: translate({language, key: 'scheduling.scheduler.summary'}),
      summaryPrefix: translate({
        language,
        key: 'scheduling.scheduler.summaryPrefix',
      }),
      summaryOncePrefix: translate({
        language,
        key: 'scheduling.scheduler.summaryOncePrefix',
      }),
      from: translate({language, key: 'scheduling.scheduler.from'}),
      at: translate({language, key: 'scheduling.scheduler.at'}),
      on: translate({language, key: 'scheduling.scheduler.on'}),
      until: translate({language, key: 'scheduling.scheduler.until'}),
      and: translate({language, key: 'scheduling.scheduler.and'}),
    }),
    [language]
  );

  const handleScheduleTypeChange = (event) => {
    const {
      target: {value, checked},
    } = event;

    if (!checked) {
      return;
    }

    dispatch(schedulerSettingsChanged({settings: {type: value}, language}));
  };

  return (
    <Fieldset
      data-testid="scheduling-section"
      layoutStyle={LayoutStyle.Vertical}
      title={translate({language, key: 'scheduling.title'})}>
      <ToggleButtonGroup>
        <ToggleButton
          id="now"
          text={translate({language, key: 'scheduling.now'})}
          value={SCHEDULE_TYPE.Asap}
          checked={executionSettings?.type === SCHEDULE_TYPE.Asap}
          onChange={handleScheduleTypeChange}
        />
        <ToggleButton
          id="future"
          text={translate({language, key: 'scheduling.future'})}
          value={SCHEDULE_TYPE.Future}
          checked={executionSettings?.type === SCHEDULE_TYPE.Future}
          onChange={handleScheduleTypeChange}
          disabled={transferType === TRANSFER_TYPE.Download}
        />
      </ToggleButtonGroup>

      {executionSettings?.type === SCHEDULE_TYPE.Future && (
        <>
          <Scheduler
            data-test="scheduler"
            recurrenceMode={executionSettings?.recurrenceMode}
            startDateTime={executionSettings?.startDateTime}
            endDateTime={executionSettings?.endDateTime}
            weekDays={executionSettings?.weekDays}
            intervalHour={executionSettings?.intervalHour}
            intervalDay={executionSettings?.intervalDay}
            intervalMonth={executionSettings?.intervalMonth}
            intervalMonthDay={executionSettings?.intervalMonthDay}
            intervalWeek={executionSettings?.intervalWeek}
            texts={texts}
            onScheduleChange={(settings) => {
              dispatch(schedulerSettingsChanged({settings, language}));
            }}
            onValidationChange={({isValid}) => {
              dispatch(schedulerValidationChanged({isValid, language}));
            }}
          />
          {executionSettings?.recurrenceMode !==
            Scheduler.recurrenceModes.OneTime && (
            <ButtonToggle
              label={translate({
                language,
                key: 'scheduling.onlyDataChangedSinceLastRun',
              })}
              id="onlyDataChangedSinceLastRun"
              checked={onlyDataChangedSinceLastRun}
              onChange={({target}) => {
                dispatch(
                  rootSettingChanged({
                    propertyName: 'onlyDataChangedSinceLastRun',
                    value: target.checked,
                    language,
                  })
                );
              }}
            />
          )}
        </>
      )}
    </Fieldset>
  );
};

export default memo(Scheduling);
