import React, {FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';

import {Switch, SwitchProps} from '@jotunheim/react-switch';

import classNames from './survey-data-export.module.css';

interface ButtonToggleProps extends SwitchProps {
  label?: string;
  element?: () => void;
}

const {element} = bemFactory({
  baseClassName: 'comd-survey-data-export',
  classNames,
});

const ButtonToggle: FC<ButtonToggleProps> = ({...rest}) => {
  return (
    <div className={element('input-container')}>
      <Switch {...rest} />
    </div>
  );
};

export default ButtonToggle;
