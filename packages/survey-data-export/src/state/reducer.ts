import {ActionType} from './action-types';
import type {
  SetFileTypeChanged,
  SetDateFilterChanged,
  SetMediaSettingChanged,
  SetInitialSettingsCreated,
  SetResponseStatusChanged,
  SetFileSettingChanged,
  SetEmailOverrideSettingChanged,
  SetFormattingSettingChanged,
  SetRootSettingChanged,
  SetSchedulerSettingsChanged,
  SetSchedulerValidationChanged,
  SetTemplateSelected,
  SetTemplateSettingChanged,
  SetTransferSettingChanged,
  SettingsPayload,
  TemplatePayload,
  ExtendedSettingsPayload,
  SchedulerSettingsPayload,
  ValidationPayload,
  ResponseStatusPayload,
} from './action-creators';
import {update, updateIn} from '../utils';
import {enforceOpenEndsTruncation} from '../utils/display-option-rules';
import {
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
  RESPONSE_STATUS_NO_FILTER,
  FILE_TYPE,
  INTERVIEW_STATUS,
  DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE,
} from '../utils/constants';
import {checkIsValid, validateModel} from '../validation/model-validation';
import {
  DateFilterSettings,
  EmailOverride,
  ExecutionSettings,
  FileSettings,
  Formatting,
  getModelDefaults,
  MediaFiles,
  Model,
  ModelDefaults,
  TemplateSettings,
} from '../utils/model-provider';

interface State {
  isScheduleValid: boolean;
  isValid: boolean;
  model: Model;
}

export const getInitialState = ({
  defaultFileType,
  defaultTransferType,
  defaultResponseStatuses,
  defaultExportLanguageId,
  defaultEmailRecipient,
  defaultScheduleType,
  defaultPlainTextBody,
  defaultSubject,
  startDate,
  defaultSchedule,
  databases,
}: ModelDefaults) => ({
  model: {
    ...getModelDefaults({
      defaultPlainTextBody,
      defaultSubject,
      startDate,
      defaultFileType,
      defaultTransferType,
      defaultResponseStatuses,
      defaultExportLanguageId,
      defaultEmailRecipient,
      defaultScheduleType,
      defaultSchedule,
      databases,
    }),
  },
  isValid: true,
  isScheduleValid: true,
  validationErrors: {},
});

const reducer = (
  state: State,
  action:
    | SetFileTypeChanged
    | SetDateFilterChanged
    | SetMediaSettingChanged
    | SetInitialSettingsCreated
    | SetResponseStatusChanged
    | SetFileSettingChanged
    | SetEmailOverrideSettingChanged
    | SetFormattingSettingChanged
    | SetRootSettingChanged
    | SetSchedulerSettingsChanged
    | SetSchedulerValidationChanged
    | SetTemplateSelected
    | SetTemplateSettingChanged
    | SetTransferSettingChanged
) => {
  switch (action.type) {
    case ActionType.initialSettingsCreated:
      return validateModelAndUpdateState(state, action.payload.language);
    case ActionType.responseStatusChanged:
      return responseStatusChanged(state, action.payload);
    case ActionType.dateFilterChanged:
      return dateFilterChanged(state, action.payload);
    case ActionType.rootSettingChanged:
      return rootSettingChanged(state, action.payload);
    case ActionType.transferSettingChanged:
      return transferSettingChanged(state, action.payload);
    case ActionType.schedulerSettingsChanged:
      return schedulerSettingsChanged(state, action.payload);
    case ActionType.schedulerValidationChanged:
      return schedulerValidationChanged(state, action.payload);
    case ActionType.fileSettingChanged:
      return fileSettingChanged(state, action.payload);
    case ActionType.fileTypeChanged:
      return fileTypeChanged(state, action.payload);
    case ActionType.mediaSettingChanged:
      return mediaSettingChanged(state, action.payload);
    case ActionType.templateSelected:
      return templateSelected(state, action.payload);
    case ActionType.templateSettingChanged:
      return templateSettingChanged(state, action.payload);
    case ActionType.formattingSettingChanged:
      return formattingSettingChanged(state, action.payload);
    case ActionType.emailOverrideSettingChanged:
      return emailOverrideSettingChanged(state, action.payload);
  }
};

const fileTypeChanged = (state: State, payload: SettingsPayload<FILE_TYPE>) => {
  const {value, language} = payload;

  const newState = updateIn<State, FileSettings>(
    state,
    ['model', 'fileSettings'],
    (fileSettings) => {
      return {
        ...fileSettings,
        fileType: value,
        truncateOpenEndsEnabled: enforceOpenEndsTruncation(value)
          ? true
          : fileSettings.truncateOpenEndsEnabled,
      };
    }
  );

  return validateModelAndUpdateState(newState, language);
};

const validateModelAndUpdateState = (state: State, language: string) => {
  const {model, isScheduleValid} = state;

  const validationErrors = validateModel({model, language});
  const isValid = checkIsValid({validationErrors, isScheduleValid});

  return {
    ...state,
    validationErrors,
    isValid,
  };
};

const templateSelected = (state: State, payload: TemplatePayload) => {
  const {id, templateType, language} = payload;

  const newState = updateIn<State, TemplateSettings>(
    state,
    ['model', 'templateSettings'],
    (settings) => ({
      ...settings,
      id,
      templateType,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const templateSettingChanged = (
  state: State,
  payload: ExtendedSettingsPayload
) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, TemplateSettings>(
    state,
    ['model', 'templateSettings'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const formattingSettingChanged = (
  state: State,
  payload: ExtendedSettingsPayload
) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, Formatting>(
    state,
    ['model', 'fileSettings', 'formatting'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const rootSettingChanged = (state: State, payload: ExtendedSettingsPayload) => {
  const {value, propertyName, language} = payload;

  const newState = update<State, Model>(state, 'model', (settings) => ({
    ...settings,
    [propertyName]: value,
  }));

  return validateModelAndUpdateState(newState as State, language);
};

const transferSettingChanged = (
  state: State,
  payload: ExtendedSettingsPayload
) => {
  const {value, propertyName, language} = payload;

  let newState = updateIn<State, State>(
    state,
    ['model', 'transferSettings'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  if (propertyName === 'transferType') {
    if (value === TRANSFER_TYPE.Download) {
      newState = updateIn(
        newState,
        ['model', 'transferSettings', 'executionSettings', 'type'],
        () => SCHEDULE_TYPE.Asap
      );

      if (
        state.model.transferSettings.overrideFileName ===
        DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Other
      ) {
        newState = updateIn(
          newState,
          ['model', 'transferSettings', 'overrideFileName'],
          () => DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Download
        );
      }
    } else if (
      state.model.transferSettings.overrideFileName ===
      DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Download
    ) {
      newState = updateIn(
        newState,
        ['model', 'transferSettings', 'overrideFileName'],
        () => DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Other
      );
    }
  }

  return validateModelAndUpdateState(newState, language);
};

const schedulerSettingsChanged = (
  state: State,
  payload: SchedulerSettingsPayload
) => {
  return updateIn<State, ExecutionSettings>(
    state,
    ['model', 'transferSettings', 'executionSettings'],
    (settings) => ({
      ...settings,
      ...payload.settings,
    })
  );
};

const schedulerValidationChanged = (
  state: State,
  payload: ValidationPayload
) => {
  const {isValid, language} = payload;

  const newState = {
    ...state,
    isScheduleValid: isValid,
  };

  return validateModelAndUpdateState(newState, language);
};

const fileSettingChanged = (state: State, payload: ExtendedSettingsPayload) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, FileSettings>(
    state,
    ['model', 'fileSettings'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const mediaSettingChanged = (
  state: State,
  payload: ExtendedSettingsPayload
) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, MediaFiles>(
    state,
    ['model', 'fileSettings', 'mediaFiles'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const responseStatusChanged = (
  state: State,
  payload: ResponseStatusPayload
) => {
  const {value, propertyName} = payload;

  return updateIn<State, INTERVIEW_STATUS[]>(
    state,
    ['model', 'responseStatusesFilter'],
    (statusesFilter) => {
      let responseStatusesFilter;

      if (propertyName === RESPONSE_STATUS_NO_FILTER) {
        responseStatusesFilter = [];
      } else {
        if (value) {
          responseStatusesFilter = [...statusesFilter, propertyName];
        } else {
          responseStatusesFilter = statusesFilter.filter(
            (status) => status !== propertyName
          );
        }
      }
      return responseStatusesFilter;
    }
  );
};

const dateFilterChanged = (state: State, payload: ExtendedSettingsPayload) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, DateFilterSettings>(
    state,
    ['model', 'dateFilterSettings'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

const emailOverrideSettingChanged = (
  state: State,
  payload: ExtendedSettingsPayload
) => {
  const {value, propertyName, language} = payload;

  const newState = updateIn<State, EmailOverride>(
    state,
    ['model', 'transferSettings', 'emailOverride'],
    (settings) => ({
      ...settings,
      [propertyName]: value,
    })
  );

  return validateModelAndUpdateState(newState, language);
};

export default reducer;
