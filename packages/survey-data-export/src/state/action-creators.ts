import {ActionType} from './action-types';
import {FILE_TYPE, INTERVIEW_STATUS} from '../utils/constants';
import type {TemplateType} from '../utils';
import type {ExecutionSettings} from '../utils/model-provider';

export interface SettingsPayload<T> {
  value: T;
  language: string;
}

export interface ValidationPayload {
  isValid: boolean;
  language: string;
}

export interface ExtendedSettingsPayload extends SettingsPayload<string> {
  propertyName: string;
}

export interface ResponseStatusPayload {
  propertyName: string;
  value: INTERVIEW_STATUS;
}

export interface TemplatePayload {
  id: number;
  templateType: TemplateType;
  language: string;
}

export interface SchedulerSettingsPayload {
  settings: ExecutionSettings;
  language: string;
}

export interface SetInitialSettingsCreated {
  type: ActionType.initialSettingsCreated;
  payload: {language: string};
}

export interface SetFileTypeChanged {
  type: ActionType.fileTypeChanged;
  payload: SettingsPayload<FILE_TYPE>;
}

export interface SetTemplateSelected {
  type: ActionType.templateSelected;
  payload: TemplatePayload;
}

export interface SetTemplateSettingChanged {
  type: ActionType.templateSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetTransferSettingChanged {
  type: ActionType.transferSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetSchedulerSettingsChanged {
  type: ActionType.schedulerSettingsChanged;
  payload: SchedulerSettingsPayload;
}
export interface SetSchedulerValidationChanged {
  type: ActionType.schedulerValidationChanged;
  payload: ValidationPayload;
}

export interface SetFileSettingChanged {
  type: ActionType.fileSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetRootSettingChanged {
  type: ActionType.rootSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetFormattingSettingChanged {
  type: ActionType.formattingSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetMediaSettingChanged {
  type: ActionType.mediaSettingChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetResponseStatusChanged {
  type: ActionType.responseStatusChanged;
  payload: ResponseStatusPayload;
}

export interface SetDateFilterChanged {
  type: ActionType.dateFilterChanged;
  payload: ExtendedSettingsPayload;
}

export interface SetEmailOverrideSettingChanged {
  type: ActionType.emailOverrideSettingChanged;
  payload: ExtendedSettingsPayload;
}

export type ActionCreator =
  | SetTransferSettingChanged
  | SetRootSettingChanged
  | SetFileSettingChanged
  | SetEmailOverrideSettingChanged
  | SetFormattingSettingChanged
  | SetMediaSettingChanged
  | SetTemplateSettingChanged
  | SetDateFilterChanged
  | SetFileTypeChanged
  | SetTemplateSelected
  | SetResponseStatusChanged
  | SetSchedulerSettingsChanged
  | SetSchedulerValidationChanged;

export const initialSettingsCreated = ({
  language,
}): SetInitialSettingsCreated => ({
  type: ActionType.initialSettingsCreated,
  payload: {
    language,
  },
});

export const fileTypeChanged = ({value, language}): SetFileTypeChanged => ({
  type: ActionType.fileTypeChanged,
  payload: {
    value,
    language,
  },
});

export const templateSelected = ({
  id,
  templateType,
  language,
}): SetTemplateSelected => ({
  type: ActionType.templateSelected,
  payload: {
    id,
    templateType,
    language,
  },
});

export const templateSettingChanged = ({
  propertyName,
  value,
  language,
}): SetTemplateSettingChanged => ({
  type: ActionType.templateSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const transferSettingChanged = ({
  propertyName,
  value,
  language,
}): SetTransferSettingChanged => ({
  type: ActionType.transferSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const schedulerSettingsChanged = ({
  settings,
  language,
}): SetSchedulerSettingsChanged => ({
  type: ActionType.schedulerSettingsChanged,
  payload: {
    settings,
    language,
  },
});

export const schedulerValidationChanged = ({
  isValid,
  language,
}): SetSchedulerValidationChanged => ({
  type: ActionType.schedulerValidationChanged,
  payload: {
    isValid,
    language,
  },
});

export const fileSettingChanged = ({
  propertyName,
  value,
  language,
}): SetFileSettingChanged => ({
  type: ActionType.fileSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const rootSettingChanged = ({
  propertyName,
  value,
  language,
}): SetRootSettingChanged => ({
  type: ActionType.rootSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const formattingSettingChanged = ({
  propertyName,
  value,
  language,
}): SetFormattingSettingChanged => ({
  type: ActionType.formattingSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const mediaSettingChanged = ({
  propertyName,
  value,
  language,
}): SetMediaSettingChanged => ({
  type: ActionType.mediaSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const responseStatusChanged = ({
  propertyName,
  value,
}): SetResponseStatusChanged => ({
  type: ActionType.responseStatusChanged,
  payload: {
    propertyName,
    value,
  },
});

export const dateFilterChanged = ({
  propertyName,
  value,
  language,
}): SetDateFilterChanged => ({
  type: ActionType.dateFilterChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});

export const emailOverrideSettingChanged = ({
  propertyName,
  value,
  language,
}): SetEmailOverrideSettingChanged => ({
  type: ActionType.emailOverrideSettingChanged,
  payload: {
    propertyName,
    value,
    language,
  },
});
