import translations from '../translation';
import {DEFAULT_LANGUAGE} from './constants';

interface Translate {
  key: string;
  language: string;
}

const get = (obj: string, path: string) =>
  path.split('.').reduce((acc, val) => acc && acc[val], obj);

export const translate = ({key, language}: Translate) =>
  get(translations[language], key) ||
  get(translations[DEFAULT_LANGUAGE as string], key);

export default translate;
