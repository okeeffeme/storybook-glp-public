import moment, {Moment} from 'moment';
import type {RecurrenceMode, WeekDay} from '@jotunheim/react-scheduler';
import {isTemplateSelected, TemplateType} from './';
import {buildISOStringFromDateAndTime, buildTimeFromText} from './date-time';
import {
  ANSWER_ELEMENT_LABELS,
  DATE_FILTER_TYPE,
  ENGLISH_LANGUAGE_ID,
  DEFAULT_RESPONSE_STATUSES,
  ENGLISH_UNITED_STATES_LOCALE,
  FILE_ENCODING,
  FILE_FORMAT_TYPE,
  MEDIA_FILE_NAMING,
  OPEN_END_HANDLING,
  QUESTION_LABEL_TEXT,
  SPSS_DECIMAL_LIMITER,
  TRANSFER_TYPE,
  TRIPLES_FILE_TO_INCLUDE,
  TRIPLES_FORMAT,
  TRIPLES_MULTI_QUESTION_FORMAT,
  TRIPLES_VERSION,
  TRUNCATE_OPEN_ENDS_LIMIT,
  FILE_TYPE,
  SCHEDULE_TYPE,
  LOOP_HANDLING,
  LOOP_POSITION,
  NO_TEMPLATE_SELECTED_VALUE,
  TEXT_IN_LABELS,
  DATABASE_TYPE,
  INTERVIEW_STATUS,
  DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE,
} from './constants';

export interface EmailOverride {
  emailOverrideEnabled: boolean;
  mailEncodingName: FILE_ENCODING;
  plainTextBody: string;
  replyTo: string;
  subject: string;
}

export interface ExecutionSettings {
  type: SCHEDULE_TYPE;
  recurrenceMode?: RecurrenceMode;
  weekDays?: WeekDay[];
  startDateTime?: string;
  endDateTime?: string;
  intervalHour?: number;
  intervalDay?: number;
  intervalWeek?: number;
  intervalMonth?: number;
  intervalMonthDay?: number;
}

export interface TransferSettings {
  transferType: TRANSFER_TYPE;
  emailRecipient: string;
  emailOverride?: EmailOverride;
  executionSettings: ExecutionSettings;
  overrideFileNameEnabled?: boolean;
  overrideFileName?: string;
  useSftp: boolean;
  folderName: string;
  uncompressed: boolean;
  hostAndPort: string | undefined;
  userName: string | undefined;
  password: string | undefined;
}

export type Time = {
  text: string;
  hour: string;
  minute: string;
};

export interface DateFilterSettings {
  dateFilterType: DATE_FILTER_TYPE;
  startDate: Moment;
  endDate: Moment;
  startTime: Time;
  endTime: Time;
}

export interface TemplateSettings {
  id: string | number;
  templateType: TemplateType;
  textInLabels: string;
}

export interface Model {
  exportLanguageId: number;
  responseStatusesFilter: INTERVIEW_STATUS[];
  onlyDataChangedSinceLastRun: boolean;
  transferSettings: TransferSettings;
  fileSettings: FileSettings;
  dateFilterSettings: DateFilterSettings;
  questionLabelText: string;
  answerElementLabels: string;
  database: DATABASE_TYPE;
  comment: string;
  loopHandling: LOOP_HANDLING;
  loopPosition: LOOP_POSITION;
  templateSettings?: TemplateSettings;
}

export interface ModelDefaults {
  defaultFileType: FILE_TYPE;
  defaultResponseStatuses: INTERVIEW_STATUS[];
  defaultExportLanguageId: number;
  defaultEmailRecipient: string;
  defaultSubject?: string;
  defaultPlainTextBody?: string;
  defaultTransferType: TRANSFER_TYPE;
  startDate?: Moment;
  defaultScheduleType: SCHEDULE_TYPE;
  databases: DATABASE_TYPE[];
  defaultSchedule?: Record<string, string>;
}

export interface Formatting {
  fileFormatType: FILE_FORMAT_TYPE;
  delimiter: string;
  quotedValues: boolean;
}

export interface MediaFiles {
  includeMedia: boolean;
  mediaFileNaming: MEDIA_FILE_NAMING;
}

export interface FileSettings {
  fileType: FILE_TYPE;
  truncateOpenEnds?: string;
  truncateOpenEndsEnabled?: boolean;
  locale: number;
  mediaFiles: MediaFiles;
  openEndHandling: OPEN_END_HANDLING;
  fileEncodingName: FILE_ENCODING;
  version: TRIPLES_VERSION;
  format: TRIPLES_FORMAT;
  multiQuestionFormat: TRIPLES_MULTI_QUESTION_FORMAT;
  filesToInclude: TRIPLES_FILE_TO_INCLUDE;
  schemaEncodingName: FILE_ENCODING;
  recodeMultis: false;
  excludeHierarchy: false;
  characterEncodingName: FILE_ENCODING;
  decimalDelimiter: SPSS_DECIMAL_LIMITER;
  includeAnswerLabelMultiQuestions: boolean;
  formatting: Formatting;
}

export function getModelDefaults({
  defaultFileType = FILE_TYPE.Excel,
  defaultResponseStatuses = DEFAULT_RESPONSE_STATUSES,
  defaultExportLanguageId = ENGLISH_LANGUAGE_ID,
  defaultEmailRecipient = '',
  defaultSubject = '',
  defaultPlainTextBody = '',
  defaultTransferType = TRANSFER_TYPE.Download,
  startDate = moment(),
  defaultScheduleType = SCHEDULE_TYPE.Asap,
  defaultSchedule = {},
  databases = [DATABASE_TYPE.Production],
}: ModelDefaults): Model {
  return {
    exportLanguageId: defaultExportLanguageId,
    responseStatusesFilter: defaultResponseStatuses,
    onlyDataChangedSinceLastRun: false,
    transferSettings: {
      transferType: defaultTransferType,
      emailRecipient: defaultEmailRecipient,
      emailOverride: {
        emailOverrideEnabled: false,
        mailEncodingName: FILE_ENCODING.UTF8,
        plainTextBody: defaultPlainTextBody,
        replyTo: '',
        subject: defaultSubject,
      },
      executionSettings: {
        ...defaultSchedule,
        type: defaultScheduleType,
      },
      overrideFileNameEnabled: false,
      overrideFileName:
        defaultTransferType === TRANSFER_TYPE.Download
          ? DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Download
          : DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE.Other,
      hostAndPort: undefined,
      useSftp: true,
      folderName: '',
      userName: undefined,
      password: undefined,
      uncompressed: false,
    },
    fileSettings: {
      fileType: defaultFileType,
      truncateOpenEnds: TRUNCATE_OPEN_ENDS_LIMIT,
      truncateOpenEndsEnabled: false,
      locale: ENGLISH_UNITED_STATES_LOCALE,
      mediaFiles: {
        includeMedia: false,
        mediaFileNaming: MEDIA_FILE_NAMING.Detailed,
      },
      openEndHandling: OPEN_END_HANDLING.IncludeOpenEnds,
      fileEncodingName: FILE_ENCODING.UTF8,
      version: TRIPLES_VERSION.Xml20,
      format: TRIPLES_FORMAT.Fixed,
      multiQuestionFormat: TRIPLES_MULTI_QUESTION_FORMAT.Bitstring,
      filesToInclude: TRIPLES_FILE_TO_INCLUDE.SchemaAndData,
      schemaEncodingName: FILE_ENCODING.UTF8,
      recodeMultis: false,
      excludeHierarchy: false,
      characterEncodingName: FILE_ENCODING.UTF8,
      decimalDelimiter: SPSS_DECIMAL_LIMITER.Point,
      includeAnswerLabelMultiQuestions: true,
      formatting: {
        fileFormatType: FILE_FORMAT_TYPE.TabSeparated,
        delimiter: '',
        quotedValues: true,
      },
    },
    dateFilterSettings: {
      dateFilterType: DATE_FILTER_TYPE.None,
      // startDate, startTime, endDate and endTime are merged and converted
      // to ISO strings before exposing via onSettingsChanged handler
      // they are not directly exposed
      startDate: startDate,
      endDate: moment(startDate).add(1, 'd'),
      startTime: buildTimeFromText('00:00') as Time,
      endTime: buildTimeFromText('00:00') as Time,
    },
    questionLabelText: QUESTION_LABEL_TEXT.QuestionId,
    answerElementLabels: ANSWER_ELEMENT_LABELS.AnswerQuestionLabel,
    database: databases.includes(DATABASE_TYPE.Production)
      ? DATABASE_TYPE.Production
      : DATABASE_TYPE.Test,
    comment: '',
    loopHandling: LOOP_HANDLING.SeparateFiles,
    loopPosition: LOOP_POSITION.AsQuestionnaire,
    templateSettings: {
      id: NO_TEMPLATE_SELECTED_VALUE,
      templateType: '',
      textInLabels: TEXT_IN_LABELS.Template,
    },
  };
}

export interface ContractData {
  dateFilterSettings: {
    dateFilterType: DATE_FILTER_TYPE;
    endDate: string;
    startDate: string;
  };
  answerElementLabels: string;
  responseStatusesFilter: INTERVIEW_STATUS[];
  transferSettings: TransferSettings;
  exportLanguageId: number;
  onlyDataChangedSinceLastRun: boolean;
  fileSettings: FileSettings;
  database: DATABASE_TYPE;
  loopHandling: LOOP_HANDLING;
  templateSettings?: TemplateSettings;
  comment: string;
  loopPosition: LOOP_POSITION;
  questionLabelText: string;
}

export const getContractData = (model: Model): ContractData => {
  const {
    dateFilterSettings: {
      startDate,
      startTime,
      endDate,
      endTime,
      ...dateFilterSettingsRest
    },
  } = model;

  const contractData = {
    ...model,
    dateFilterSettings: {
      ...dateFilterSettingsRest,
      startDate: buildISOStringFromDateAndTime(startDate, startTime),
      endDate: buildISOStringFromDateAndTime(endDate, endTime),
    },
  };

  if (!isTemplateSelected(contractData.templateSettings?.id)) {
    delete contractData.templateSettings;
  }

  if (!contractData.fileSettings.truncateOpenEndsEnabled) {
    contractData.fileSettings = {...contractData.fileSettings};
    delete contractData.fileSettings.truncateOpenEnds;
    delete contractData.fileSettings.truncateOpenEndsEnabled;
  }

  if (!contractData.transferSettings.emailOverride?.emailOverrideEnabled) {
    contractData.transferSettings = {...contractData.transferSettings};
    delete contractData.transferSettings.emailOverride;
  }

  if (!contractData.transferSettings.overrideFileNameEnabled) {
    contractData.transferSettings = {...contractData.transferSettings};

    delete contractData.transferSettings.overrideFileName;
    delete contractData.transferSettings.overrideFileNameEnabled;
  }

  return contractData;
};
