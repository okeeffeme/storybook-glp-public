import {translate} from './translation';
import {
  NO_TEMPLATE_SELECTED_VALUE,
  TEMPLATE_ID_AND_TYPE_SEPARATOR,
} from './constants';

export interface Option {
  value: number;
  label: string;
}

export type TemplateType = 'ClassicTemplate' | '';

export interface Template {
  id: number;
  type: TemplateType;
  name: string;
}

interface TemplateValue {
  id: string | number;
  templateType: TemplateType;
}

interface TemplateList {
  templates: Template[];
  language: string;
}

export const isTemplateSelected = (id?: string | number) =>
  id != NO_TEMPLATE_SELECTED_VALUE;

// generate value to avoid id collision between classic and data templates
export const createTemplateValue = ({id, templateType}: TemplateValue) =>
  `${id}${TEMPLATE_ID_AND_TYPE_SEPARATOR}${templateType}`;

export const getIdAndTypeFromTemplateValue = (value: string) => {
  const parameters = value.split(TEMPLATE_ID_AND_TYPE_SEPARATOR);
  return {id: parameters[0], templateType: parameters[1]};
};

export const getTemplateList = ({templates, language}: TemplateList) => {
  const firstItem = {
    label: translate({
      language,
      key: 'exportSetup.noTemplateFilter',
    }),
    value: createTemplateValue({
      id: NO_TEMPLATE_SELECTED_VALUE,
      templateType: '',
    }),
  };

  return templates.reduce(
    (accumulator, currentValue) => {
      accumulator.push({
        label: currentValue.name,
        value: createTemplateValue({
          id: currentValue.id,
          templateType: currentValue.type,
        }),
      });

      return accumulator;
    },
    [firstItem]
  );
};

//TODO: move to confirmit-utils
export function update<T, R>(
  obj: T,
  propName: string | number,
  updateFn: (obj: R) => R
) {
  const nextState = updateFn(obj[propName]);

  if (obj[propName] === nextState) {
    return obj;
  }

  if (obj instanceof Array) {
    return [
      ...obj.slice(0, propName as number),
      nextState,
      ...obj.slice((propName as number) + 1),
    ];
  }

  return {
    ...obj,
    [propName]: nextState,
  };
}

//TODO: move to confirmit-utils
export function updateIn<T, R>(
  obj: T,
  path: string[],
  updateFn: (obj: R) => R
) {
  const propName = path[0];
  const nextPath = path.slice(1);

  return nextPath.length
    ? update(obj, propName, () => updateIn(obj[propName], nextPath, updateFn))
    : update(obj, propName, updateFn);
}
