import translate from './translation';
import {
  FILE_TYPE,
  TRANSFER_TYPE,
  INTERVIEW_STATUS,
  DATE_FILTER_TYPE,
  QUESTION_LABEL_TEXT,
  ANSWER_ELEMENT_LABELS,
  FILE_FORMAT_TYPE,
  MEDIA_FILE_NAMING,
  OPEN_END_HANDLING,
  FILE_ENCODING,
  TRIPLES_VERSION,
  TRIPLES_FORMAT,
  TRIPLES_MULTI_QUESTION_FORMAT,
  TRIPLES_FILE_TO_INCLUDE,
  SPSS_DECIMAL_LIMITER,
  LOOP_HANDLING,
  LOOP_POSITION,
  TEXT_IN_LABELS,
} from './constants';

export const fileTypesList = (language: string) => [
  {
    value: FILE_TYPE.Excel,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.excel',
    }),
  },
  {
    value: FILE_TYPE.ExcelWithLabels,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.excelWithLabels',
    }),
  },
  {
    value: FILE_TYPE.DelimitedTextFile,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.delimitedTextFile',
    }),
  },
  {
    value: FILE_TYPE.DelimitedTxtWithLabels,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.delimitedTxtWithLabels',
    }),
  },
  {
    value: FILE_TYPE.TripleS,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.tripleS',
    }),
  },
  {
    value: FILE_TYPE.TripleSConfirmit,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.tripleSConfirmit',
    }),
  },
  {
    value: FILE_TYPE.SPSS,
    label: translate({language, key: 'exportSetup.fileTypeList.spss'}),
  },
  {
    value: FILE_TYPE.SPSSSav,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.spsssav',
    }),
  },
  {
    value: FILE_TYPE.Quantum,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.quantum',
    }),
  },
  {
    value: FILE_TYPE.SAS,
    label: translate({language, key: 'exportSetup.fileTypeList.sas'}),
  },
  {
    value: FILE_TYPE.FixedWidthFile,
    label: translate({
      language,
      key: 'exportSetup.fileTypeList.fixedWidthFile',
    }),
  },
];

export const deliveryTypeList = (language: string) => [
  {
    value: TRANSFER_TYPE.Download,
    label: translate({
      language,
      key: 'exportSetup.deliveryTypeList.download',
    }),
  },
  {
    value: TRANSFER_TYPE.Email,
    label: translate({
      language,
      key: 'exportSetup.deliveryTypeList.email',
    }),
  },
  {
    value: TRANSFER_TYPE.FTP,
    label: translate({
      language,
      key: 'exportSetup.deliveryTypeList.ftp',
    }),
  },
  {
    value: TRANSFER_TYPE.ExternalFTP,
    label: translate({
      language,
      key: 'exportSetup.deliveryTypeList.externalFtp',
    }),
  },
];

export const interviewStatuses = (language: string) => [
  {
    value: INTERVIEW_STATUS.Completed,
    label: translate({
      language,
      key: 'responseFilter.interviewStatuses.completed',
    }),
  },
  {
    value: INTERVIEW_STATUS.Incomplete,
    label: translate({
      language,
      key: 'responseFilter.interviewStatuses.incomplete',
    }),
  },
  {
    value: INTERVIEW_STATUS.Screened,
    label: translate({
      language,
      key: 'responseFilter.interviewStatuses.screened',
    }),
  },
  {
    value: INTERVIEW_STATUS.QuotaFull,
    label: translate({
      language,
      key: 'responseFilter.interviewStatuses.quotaFull',
    }),
  },
  {
    value: INTERVIEW_STATUS.Error,
    label: translate({
      language,
      key: 'responseFilter.interviewStatuses.error',
    }),
  },
];

export const dataFilterList = (language: string) => [
  {
    value: DATE_FILTER_TYPE.None,
    label: translate({
      language,
      key: 'responseFilter.dateFilterList.none',
    }),
  },
  {
    value: DATE_FILTER_TYPE.Range,
    label: translate({
      language,
      key: 'responseFilter.dateFilterList.range',
    }),
  },
];

export const questionLabelTextList = (language: string) => [
  {
    value: QUESTION_LABEL_TEXT.TextOnly,
    label: translate({
      language,
      key: 'advancedOptions.questionLabelTextList.textOnly',
    }),
  },
  {
    value: QUESTION_LABEL_TEXT.TitleOnly,
    label: translate({
      language,
      key: 'advancedOptions.questionLabelTextList.titleOnly',
    }),
  },
  {
    value: QUESTION_LABEL_TEXT.TitleAndText,
    label: translate({
      language,
      key: 'advancedOptions.questionLabelTextList.titleAndText',
    }),
  },
  {
    value: QUESTION_LABEL_TEXT.QuestionId,
    label: translate({
      language,
      key: 'advancedOptions.questionLabelTextList.questionId',
    }),
  },
];

export const answerElementLabelsList = (language: string) => [
  {
    value: ANSWER_ELEMENT_LABELS.AnswerQuestionLabel,
    label: translate({
      language,
      key: 'advancedOptions.answerElementLabelsList.answerQuestionLabel',
    }),
  },
  {
    value: ANSWER_ELEMENT_LABELS.QuestionLabelAnswer,
    label: translate({
      language,
      key: 'advancedOptions.answerElementLabelsList.questionLabelAnswer',
    }),
  },
  {
    value: ANSWER_ELEMENT_LABELS.Answer,
    label: translate({
      language,
      key: 'advancedOptions.answerElementLabelsList.answer',
    }),
  },
];

export const fileFormatTypeList = (language: string) => [
  {
    value: FILE_FORMAT_TYPE.TabSeparated,
    label: translate({
      language,
      key: 'advancedOptions.fileFormatTypeList.tabSeparated',
    }),
  },
  {
    value: FILE_FORMAT_TYPE.CommaSeparated,
    label: translate({
      language,
      key: 'advancedOptions.fileFormatTypeList.commaSeparated',
    }),
  },
  {
    value: FILE_FORMAT_TYPE.Custom,
    label: translate({
      language,
      key: 'advancedOptions.fileFormatTypeList.custom',
    }),
  },
];

export const mediaFileNamingList = [
  {
    value: MEDIA_FILE_NAMING.Detailed,
    label: '<pid>_<respid>_<loopids>_<qid>_<uniqueid>.<ext>',
  },
  {
    value: MEDIA_FILE_NAMING.Unique,
    label: '<uniqueid>.<ext>',
  },
];

export const openEndHandlingList = (language: string) => [
  {
    value: OPEN_END_HANDLING.IncludeOpenEnds,
    label: translate({
      language,
      key: 'advancedOptions.openEndHandlingList.includeOpenEnds',
    }),
  },
  {
    value: OPEN_END_HANDLING.ExcludeOpenEnds,
    label: translate({
      language,
      key: 'advancedOptions.openEndHandlingList.excludeOpenEnds',
    }),
  },
  {
    value: OPEN_END_HANDLING.OpenEndsPerAnswer,
    label: translate({
      language,
      key: 'advancedOptions.openEndHandlingList.openEndsPerAnswer',
    }),
  },
  {
    value: OPEN_END_HANDLING.OpenEndsPerRow,
    label: translate({
      language,
      key: 'advancedOptions.openEndHandlingList.openEndsPerRow',
    }),
  },
];

export const fileEncodingList = [
  {
    value: FILE_ENCODING.ANSI,
    label: 'ANSI',
  },
  {
    value: FILE_ENCODING.Unicode,
    label: 'Unicode',
  },
  {
    value: FILE_ENCODING.UTF8,
    label: 'UTF-8',
  },
];

export const tripleSVersionList = [
  {
    value: TRIPLES_VERSION.Xml11,
    label: 'Triple-S XML 1.1',
  },
  {
    value: TRIPLES_VERSION.Xml12,
    label: 'Triple-S XML 1.2',
  },
  {
    value: TRIPLES_VERSION.Xml20,
    label: 'Triple-S XML 2.0 ',
  },
];

export const tripleSFormatList = (language: string) => [
  {
    value: TRIPLES_FORMAT.CSV,
    label: translate({
      language,
      key: 'advancedOptions.tripleSFormatList.csv',
    }),
  },
  {
    value: TRIPLES_FORMAT.Fixed,
    label: translate({
      language,
      key: 'advancedOptions.tripleSFormatList.fixed',
    }),
  },
];

export const tripleSMultiQuestionFormatList = [
  {
    value: TRIPLES_MULTI_QUESTION_FORMAT.Bitstring,
    label: 'BitString',
  },
  {
    value: TRIPLES_MULTI_QUESTION_FORMAT.Spread,
    label: 'Spread',
  },
];

export const tripleFilesToIncludeList = (language: string) => [
  {
    value: TRIPLES_FILE_TO_INCLUDE.SchemaAndData,
    label: translate({
      language,
      key: 'advancedOptions.tripleFilesToIncludeList.schemaAndData',
    }),
  },
  {
    value: TRIPLES_FILE_TO_INCLUDE.SchemaOnly,
    label: translate({
      language,
      key: 'advancedOptions.tripleFilesToIncludeList.schemaOnly',
    }),
  },
  {
    value: TRIPLES_FILE_TO_INCLUDE.DataOnly,
    label: translate({
      language,
      key: 'advancedOptions.tripleFilesToIncludeList.dataOnly',
    }),
  },
];

export const spssDecimalLimiterList = (language: string) => [
  {
    value: SPSS_DECIMAL_LIMITER.Point,
    label: translate({
      language,
      key: 'advancedOptions.spssDecimalLimiterList.point',
    }),
  },
  {
    value: SPSS_DECIMAL_LIMITER.Comma,
    label: translate({
      language,
      key: 'advancedOptions.spssDecimalLimiterList.comma',
    }),
  },
];

export const loopHandlingList = (language: string) => [
  {
    value: LOOP_HANDLING.SeparateFiles,
    label: translate({
      language,
      key: 'advancedOptions.loopHandlingList.separateFiles',
    }),
  },
  {
    value: LOOP_HANDLING.SingleFile,
    label: translate({
      language,
      key: 'advancedOptions.loopHandlingList.singleFile',
    }),
  },
];

export const loopPositionList = (language: string) => [
  {
    value: LOOP_POSITION.AsQuestionnaire,
    label: translate({
      language,
      key: 'advancedOptions.loopPositionList.asInQuestionnaire',
    }),
  },
  {
    value: LOOP_POSITION.PerLevel,
    label: translate({
      language,
      key: 'advancedOptions.loopPositionList.appendAtTheEnd',
    }),
  },
];

export const textInLabelsList = (language: string) => [
  {
    value: TEXT_IN_LABELS.Template,
    label: translate({
      language,
      key: 'exportSetup.textInLabelsList.fromTemplate',
    }),
  },
  {
    value: TEXT_IN_LABELS.Survey,
    label: translate({
      language,
      key: 'exportSetup.textInLabelsList.fromSurvey',
    }),
  },
];
