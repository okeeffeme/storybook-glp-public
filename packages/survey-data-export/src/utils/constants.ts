export enum TRANSFER_TYPE {
  Download = 'Download',
  Email = 'Email',
  FTP = 'FtpServer',
  ExternalFTP = 'ExternalFtpServer',
}

export enum FILE_TYPE {
  Excel = 'Excel',
  ExcelWithLabels = 'ExcelWithLabels',
  DelimitedTextFile = 'DelimitedTextFile',
  DelimitedTxtWithLabels = 'DelimitedTextFileWithLabels',
  TripleS = 'TripleS',
  TripleSConfirmit = 'TripleSConfirmit',
  SPSS = 'SPSS',
  SPSSSav = 'SPSSSav',
  Quantum = 'Quantum',
  SAS = 'SAS',
  FixedWidthFile = 'FixedWidthFile',
}

export enum INTERVIEW_STATUS {
  Completed = 'Complete',
  Incomplete = 'Incomplete',
  Screened = 'Screened',
  QuotaFull = 'QuotaFull',
  Error = 'Error',
}

export const RESPONSE_STATUS_NO_FILTER = 'RESPONSE_NO_FILTER';

export enum DATE_FILTER_TYPE {
  None = 'None',
  Range = 'Fixed',
}

export const QUESTION_LABEL_TEXT = {
  TextOnly: 'TextOnly',
  QuestionId: 'QuestionId',
  TitleAndText: 'TitleAndText',
  TitleOnly: 'TitleOnly',
};

export const ANSWER_ELEMENT_LABELS = {
  Answer: 'Answer',
  AnswerQuestionLabel: 'AnswerQuestionLabel',
  QuestionLabelAnswer: 'QuestionLabelAnswer',
};

export enum FILE_FORMAT_TYPE {
  TabSeparated = 'TabSeparated',
  CommaSeparated = 'CommaSeparated',
  Custom = 'Custom',
}

export const ENGLISH_UNITED_STATES_LOCALE = 1033;

export enum MEDIA_FILE_NAMING {
  Detailed = 'Detailed',
  Unique = 'Unique',
}

export enum OPEN_END_HANDLING {
  IncludeOpenEnds = 'IncludeOpenEnds',
  ExcludeOpenEnds = 'ExcludeOpenEnds',
  OpenEndsPerAnswer = 'OpenEndsPerAnswer',
  OpenEndsPerRow = 'OpenEndsPerRow',
}

export enum FILE_ENCODING {
  ANSI = 'ANSI',
  Unicode = 'Unicode',
  UTF8 = 'UTF8',
}

export enum TRIPLES_VERSION {
  Xml11 = 'Xml11',
  Xml12 = 'Xml12',
  Xml20 = 'Xml20',
}

export enum TRIPLES_FORMAT {
  Fixed = 'Fixed',
  CSV = 'CSV',
}

export enum TRIPLES_MULTI_QUESTION_FORMAT {
  Bitstring = 'Bitstring',
  Spread = 'Spread',
}

export enum TRIPLES_FILE_TO_INCLUDE {
  SchemaOnly = 'SchemaOnly',
  DataOnly = 'DataOnly',
  SchemaAndData = 'SchemaAndData',
}

export const TRUNCATE_OPEN_ENDS_LIMIT = '200';

export enum SPSS_DECIMAL_LIMITER {
  Point = '.',
  Comma = ',',
}

export const ENGLISH_LANGUAGE_ID = 9;

export const DEFAULT_RESPONSE_STATUSES = [];

export enum SCHEDULE_TYPE {
  Asap = 'asap',
  Future = 'scheduleLater',
}

export enum LOOP_HANDLING {
  SeparateFiles = 'SeparateFiles',
  SingleFile = 'SingleFile',
}

export enum LOOP_POSITION {
  AsQuestionnaire = 'AsQuestionnaire',
  PerLevel = 'PerLevel',
}

export const TEXT_IN_LABELS = {
  Template: 'Template',
  Survey: 'Survey',
};

export const DEFAULT_LANGUAGE = 'en';

export const NO_TEMPLATE_SELECTED_VALUE = '-999999';

export const TEMPLATE_ID_AND_TYPE_SEPARATOR = '___';

export enum DATABASE_TYPE {
  Production = 'production',
  Test = 'test',
}

export const DEFAULT_OVERRIDE_FILE_NAME_BY_TRANSFER_TYPE = {
  Download: '^PROJECTID^_^USERID^_^TASKID^',
  Other: '^PROJECTID^_^USERID^_^START^_^END^_^TASKID^',
} as const;
