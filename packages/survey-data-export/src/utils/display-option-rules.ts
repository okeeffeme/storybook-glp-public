import {
  DATE_FILTER_TYPE,
  FILE_FORMAT_TYPE,
  FILE_TYPE,
  TRANSFER_TYPE,
} from './constants';

const allowedTypesForTruncate = [
  FILE_TYPE.Excel,
  FILE_TYPE.ExcelWithLabels,
  FILE_TYPE.DelimitedTextFile,
  FILE_TYPE.DelimitedTxtWithLabels,
  FILE_TYPE.TripleS,
  FILE_TYPE.TripleSConfirmit,
  FILE_TYPE.SPSS,
  FILE_TYPE.SPSSSav,
  FILE_TYPE.FixedWidthFile,
];

export const displayTruncateOpenEnds = (fileType?: FILE_TYPE) => {
  if (!fileType) {
    return false;
  }

  return allowedTypesForTruncate.includes(fileType);
};

export const isDownloadTransfer = (transferType?: TRANSFER_TYPE) =>
  transferType === TRANSFER_TYPE.Download;

export const isEmailTransfer = (transferType?: TRANSFER_TYPE) =>
  transferType === TRANSFER_TYPE.Email;

const isFTPTransfer = (transferType?: TRANSFER_TYPE) =>
  transferType === TRANSFER_TYPE.FTP;

export const isExternalFTPTransfer = (transferType?: TRANSFER_TYPE) =>
  transferType === TRANSFER_TYPE.ExternalFTP;

export const isEmailOrFtpTransfer = (transferType?: TRANSFER_TYPE) =>
  isEmailTransfer(transferType) ||
  isFTPTransfer(transferType) ||
  isExternalFTPTransfer(transferType);

// some types enforce to specify truncation limit without an option to disable it
export const enforceOpenEndsTruncation = (fileType?: FILE_TYPE) => {
  if (!fileType) {
    return false;
  }

  return [
    FILE_TYPE.TripleS,
    FILE_TYPE.TripleSConfirmit,
    FILE_TYPE.SPSS,
    FILE_TYPE.SPSSSav,
    FILE_TYPE.FixedWidthFile,
  ].includes(fileType);
};

export const isFileDelimitedTextDescendant = (fileType?: FILE_TYPE) =>
  fileType === FILE_TYPE.DelimitedTextFile ||
  fileType === FILE_TYPE.DelimitedTxtWithLabels;

export const isFileTripleSDescendant = (fileType?: FILE_TYPE) =>
  fileType === FILE_TYPE.TripleS || fileType === FILE_TYPE.TripleSConfirmit;

export const isFileSpssDescendant = (fileType?: FILE_TYPE) =>
  isSpssFile(fileType) || fileType === FILE_TYPE.SPSSSav;

const isSpssFile = (fileType?: FILE_TYPE) => fileType === FILE_TYPE.SPSS;

export const isQuantumFile = (fileType?: FILE_TYPE) =>
  fileType === FILE_TYPE.Quantum;

export const isSasFile = (fileType?: FILE_TYPE) => fileType === FILE_TYPE.SAS;

export const isFixedWidthFile = (fileType?: FILE_TYPE) =>
  fileType === FILE_TYPE.FixedWidthFile;

export const displayCustomDelimiter = (fileFormatType?: FILE_FORMAT_TYPE) =>
  fileFormatType === FILE_FORMAT_TYPE.Custom;

export const displayTextQualifier = (fileFormatType?: FILE_FORMAT_TYPE) =>
  fileFormatType === FILE_FORMAT_TYPE.Custom ||
  fileFormatType === FILE_FORMAT_TYPE.CommaSeparated;

export const displayCharacterEncodingName = (fileType: FILE_TYPE | undefined) =>
  isSpssFile(fileType);

export const displayDecimalDelimiter = (fileType: FILE_TYPE | undefined) =>
  isSpssFile(fileType);

export const displayOnlyDataChangedSinceLastRun = (
  transferType: TRANSFER_TYPE
) => transferType === TRANSFER_TYPE.Email;

export const displayDateRangeFilter = (dateFilterType?: DATE_FILTER_TYPE) =>
  dateFilterType === DATE_FILTER_TYPE.Range;

export const displayLoopHandling = (fileType?: FILE_TYPE) =>
  !isQuantumFile(fileType);

export const displayFileTypesSelect = <T>(fileTypes: T[]) =>
  fileTypes && fileTypes.length > 1;

export const displayTemplateListSelect = (templates) =>
  templates && templates.length > 1;

export const displayTransferTypesSelect = <T>(transferTypes: T[]) =>
  transferTypes && transferTypes.length > 1;

export const displayExportSetupSection = (
  fileTypes,
  templates,
  transferTypes
) =>
  displayFileTypesSelect(fileTypes) ||
  displayTemplateListSelect(templates) ||
  displayTransferTypesSelect(transferTypes);

export const displayResponseFilter = (availableResponseStatuses) =>
  availableResponseStatuses ? !!availableResponseStatuses.length : true;

export const displayDataFilterSection = (
  availableResponseStatuses,
  displayDateFilter
) => displayResponseFilter(availableResponseStatuses) || displayDateFilter;
