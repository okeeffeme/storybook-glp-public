import moment, {Moment} from 'moment';

interface Time {
  hour: string;
  minute: string;
}

export const buildISOStringFromDateAndTime = (date: Moment, time: Time) => {
  const dateTime = appendTimeToDate(date, time);
  return dateTime.toISOString(true);
};

export const appendTimeToDate = (date: Moment, {hour, minute}: Time) => {
  return moment(date).startOf('day').add(hour, 'h').add(minute, 'm');
};

export function buildTimeFromText(text: string) {
  const output = {
    text: text,
  };

  if (!text) {
    return output;
  }

  const separatorIndex = getSeparatorIndex(text);

  if (!separatorIndex) {
    return output;
  }

  return {
    ...output,
    hour: text.substring(0, separatorIndex),
    minute: text.substring(separatorIndex + 1, text.length),
  };
}

const getSeparatorIndex = (startTimeText: string) => {
  const COLON_SEPARATOR = ':',
    FULL_STOP_SEPARATOR = '.';

  let separatorIndex = startTimeText.indexOf(COLON_SEPARATOR);

  if (separatorIndex > 0) {
    return separatorIndex;
  }

  separatorIndex = startTimeText.indexOf(FULL_STOP_SEPARATOR);

  return separatorIndex > 0 ? separatorIndex : undefined;
};
