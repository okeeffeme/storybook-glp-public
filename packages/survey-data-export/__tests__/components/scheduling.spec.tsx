import React from 'react';
import {render, screen} from '@testing-library/react';
import {Scheduling} from '../../src/components/scheduling';
import {SCHEDULE_TYPE} from '../../src/utils/constants';

import SurveyDataExportContext from '../../src/context';

const ctxValue = {
  dispatch: () => null,
  language: 'en',
};

const renderComponent = (executionSettings) =>
  render(<Scheduling executionSettings={executionSettings} />, {
    wrapper: ({children}) => (
      <SurveyDataExportContext.Provider value={ctxValue}>
        {children}
      </SurveyDataExportContext.Provider>
    ),
  });

describe('Scheduling', () => {
  it('should hide scheduler when schedule type is asap', () => {
    renderComponent({type: SCHEDULE_TYPE.Asap});

    expect(screen.queryByTestId('scheduler')).not.toBeInTheDocument();
  });

  it('should show scheduler when schedule type is future', () => {
    renderComponent({type: SCHEDULE_TYPE.Future});

    expect(screen.getByTestId('scheduler')).toBeInTheDocument();
  });

  it('should show onlyDataChangedSinceLastRun when schedule is recurring', () => {
    renderComponent({
      type: SCHEDULE_TYPE.Future,
      recurrenceMode: 'Hourly',
    });

    expect(
      screen.getByText('Only include data changed since last run')
    ).toBeInTheDocument();
  });

  it('should hide onlyDataChangedSinceLastRun when schedule is not recurring', () => {
    renderComponent({
      type: SCHEDULE_TYPE.Future,
      recurrenceMode: 'OneTime',
    });

    expect(
      screen.queryByText('Only include data changed since last run')
    ).not.toBeInTheDocument();
  });

  it('should hide onlyDataChangedSinceLastRun when schedule is ASAP', () => {
    renderComponent({
      type: SCHEDULE_TYPE.Asap,
      recurrenceMode: 'Hourly',
    });

    expect(
      screen.queryByText('Only include data changed since last run')
    ).not.toBeInTheDocument();
  });
});
