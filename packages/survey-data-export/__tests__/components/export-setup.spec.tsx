import React from 'react';
import {render, screen} from '@testing-library/react';
import {ExportSetup} from '../../src/components/export-setup';
import {
  TRANSFER_TYPE,
  FILE_TYPE,
  DATABASE_TYPE,
} from '../../src/utils/constants';
describe('Export setup', () => {
  const renderComponent = (settings) =>
    render(
      <ExportSetup
        fileType={FILE_TYPE.DelimitedTextFile}
        allowedFileTypes={[FILE_TYPE.DelimitedTextFile, FILE_TYPE.SPSS]}
        transferType={TRANSFER_TYPE.Download}
        allowedTransferTypes={[
          TRANSFER_TYPE.Download,
          TRANSFER_TYPE.Email,
          TRANSFER_TYPE.FTP,
        ]}
        emailRecipient={'user123@confirmit.com'}
        templates={[
          {id: 1, type: 'ClassicTemplate', name: 'Hello world'},
          {id: 2, type: 'DataTemplate', name: 'My template'},
        ]}
        templateSettings={{
          id: '1',
          type: 'ClassicTemplate',
        }}
        databases={[]}
        {...settings}
      />
    );

  it('should show emailRecipient and comment when email transfer is selected', () => {
    renderComponent({transferType: TRANSFER_TYPE.Email});

    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      'user123@confirmit.com'
    );
    expect(screen.getAllByText('Email Message Comment').length).toBe(2);
  });

  it('should show emailRecipient and comment when FTP transfer is selected', () => {
    renderComponent({transferType: TRANSFER_TYPE.FTP});

    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      'user123@confirmit.com'
    );
    expect(screen.getAllByText('Email Message Comment').length).toBe(2);
  });

  it('should hide emailRecipient and comment when download is selected', () => {
    renderComponent({transferType: TRANSFER_TYPE.Download});

    expect(screen.queryByTestId('simple-text-field')).not.toBeInTheDocument();
    expect(screen.queryAllByText('Email Message Comment').length).toBe(0);
  });

  it('should not render transfer type select when only one option is specified', () => {
    renderComponent({
      transferType: TRANSFER_TYPE.Download,
      allowedTransferTypes: [TRANSFER_TYPE.Download],
    });

    expect(screen.getByTestId('fileType')).toBeInTheDocument();
  });

  it('should render transfer type select when two or more options are specified', () => {
    renderComponent({
      transferType: TRANSFER_TYPE.Download,
      allowedTransferTypes: [TRANSFER_TYPE.Download, TRANSFER_TYPE.Email],
    });

    expect(screen.getByTestId('fileType')).toBeInTheDocument();
  });

  it('should not render template list select when only one option is specified', () => {
    // For templates, we already have one option by default, so pass empty array
    renderComponent({
      templates: [],
      templateSettings: {},
    });

    expect(screen.queryByTestId('idWithTemplateType')).not.toBeInTheDocument();
  });

  it('should render template list select when two or more options are specified', () => {
    renderComponent({
      templates: [
        {id: 1, type: 'ClassicTemplate', name: 'Hello world'},
        {id: 2, type: 'DataTemplate', name: 'My template'},
      ],
      templateSettings: {
        id: '1',
        type: 'ClassicTemplate',
      },
    });

    expect(screen.getByTestId('idWithTemplateType')).toBeInTheDocument();
  });

  it('should not render file type select when only one option is specified', () => {
    // For templates, we already have one option by default, so pass empty array
    renderComponent({
      allowedFileTypes: ['Excel'],
      fileType: 'Excel',
    });

    expect(screen.queryByTestId('fileType')).not.toBeInTheDocument();
  });

  it('should render file type select when two or more options are specified', () => {
    renderComponent({
      allowedFileTypes: ['Excel', 'ExcelWithLabels'],
      fileType: 'Excel',
    });

    expect(screen.getByTestId('fileType')).toBeInTheDocument();
  });

  it('should filter out Ftp transfer when Download and Email transfer allowed', () => {
    renderComponent({
      transferType: TRANSFER_TYPE.Download,
      allowedTransferTypes: [TRANSFER_TYPE.Download, TRANSFER_TYPE.Email],
    });

    expect(screen.getByTestId('transferType')).toBeInTheDocument();
    expect(screen.getByText('Download to Browser')).toBeInTheDocument();
  });

  it('should filter out Excel file when DelimitedTextFile and SPSS allowed', () => {
    renderComponent({
      fileType: FILE_TYPE.DelimitedTextFile,
      allowedFileTypes: [FILE_TYPE.DelimitedTextFile, FILE_TYPE.SPSS],
    });

    expect(screen.getByTestId('transferType')).toBeInTheDocument();
    expect(screen.getByText('Delimited Text File')).toBeInTheDocument();
  });
  it('should disable test database button when database not available', () => {
    renderComponent({
      databases: [DATABASE_TYPE.Production, 'invalid-db'],
    });

    expect(
      screen.getByRole('checkbox', {name: 'Production Database'})
    ).not.toHaveAttribute('disabled');
    expect(
      screen.getByRole('checkbox', {name: 'Test Database'})
    ).toHaveAttribute('disabled');
  });

  it('should disable production database button when database not available', () => {
    renderComponent({
      databases: [DATABASE_TYPE.Test, 'invalid-db'],
    });

    expect(
      screen.getByRole('checkbox', {name: 'Production Database'})
    ).toHaveAttribute('disabled');
    expect(
      screen.getByRole('checkbox', {name: 'Test Database'})
    ).not.toHaveAttribute('disabled');
  });

  it('should not show database selector when only 1 database is available', () => {
    renderComponent({databases: [DATABASE_TYPE.Test]});

    expect(
      screen.queryByTestId('survey-data-export-database-selectors')
    ).not.toBeInTheDocument();
  });

  it('should show database selector when more than 1 database is available', () => {
    renderComponent({
      databases: [DATABASE_TYPE.Test, DATABASE_TYPE.Production],
    });

    expect(
      screen.getByTestId('survey-data-export-database-selectors')
    ).toBeInTheDocument();
  });

  it('should show external FTP settings when transferType is ExternalFTP', () => {
    renderComponent({
      transferType: TRANSFER_TYPE.ExternalFTP,
      allowedTransferTypes: [TRANSFER_TYPE.ExternalFTP],
    });

    expect(
      screen.getByTestId('survey-data-export-external-ftp-settings')
    ).toBeInTheDocument();
  });

  it('should not show external FTP settings when transferType is not ExternalFTP', () => {
    renderComponent({
      transferType: TRANSFER_TYPE.Email,
      allowedTransferTypes: [TRANSFER_TYPE.ExternalFTP, TRANSFER_TYPE.Email],
    });

    expect(
      screen.queryByTestId('survey-data-export-external-ftp-settings')
    ).not.toBeInTheDocument();
  });
});
