import React from 'react';
import {render, screen} from '@testing-library/react';

import {ExternalFtpSettings} from '../../src/components/ExternalFtpSettings';

const renderComponent = (settings) =>
  render(<ExternalFtpSettings {...settings} />);
describe('Export setup', () => {
  it('should show useSFTP switch as on and disabled when enforceSftp is true, even when useSFTP is false', () => {
    renderComponent({
      enforceSftp: true,
      useSftp: false,
    });

    const checkbox = screen.getByRole('checkbox');

    expect(checkbox).toHaveAttribute('id', 'useSftp');
    expect(checkbox).toHaveAttribute('checked');
    expect(checkbox).toHaveAttribute('disabled');
  });

  it('should show useSFTP switch as on when enforceSftp is false, and useSFTP is true', () => {
    renderComponent({
      enforceSftp: false,
      useSftp: true,
    });

    const checkbox = screen.getByRole('checkbox');

    expect(checkbox).toHaveAttribute('id', 'useSftp');
    expect(checkbox).toHaveAttribute('checked');
    expect(checkbox).not.toHaveAttribute('disabled');
  });

  it('should show useSFTP switch as off when enforceSftp is false, and useSFTP is false', () => {
    renderComponent({
      enforceSftp: false,
      useSftp: true,
    });

    const checkbox = screen.getByRole('checkbox');

    expect(checkbox).toHaveAttribute('id', 'useSftp');
    expect(checkbox).toHaveAttribute('checked');
    expect(checkbox).not.toHaveAttribute('disabled');
  });

  it('should show error message when externalFtpErrorMessage is set and displayAllErrors is true', () => {
    renderComponent({
      externalFtpErrorMessage: 'some error message',
      displayAllErrors: true,
    });

    expect(screen.getByText(/some error message/i)).toBeInTheDocument();
  });

  it('should not show error message when externalFtpErrorMessage is set and displayAllErrors is false', () => {
    renderComponent({
      externalFtpErrorMessage: 'some error message',
      displayAllErrors: false,
    });

    expect(
      screen.queryByTestId('survey-data-export-external-ftp-error-message')
    ).not.toBeInTheDocument();
  });

  it('should not show error message when externalFtpErrorMessage is not set', () => {
    renderComponent({});

    expect(
      screen.queryByTestId('survey-data-export-external-ftp-error-message')
    ).not.toBeInTheDocument();
  });
});
