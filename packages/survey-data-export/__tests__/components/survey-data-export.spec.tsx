import React from 'react';
import {render, screen} from '@testing-library/react';

import {
  ENGLISH_LANGUAGE_ID,
  ENGLISH_UNITED_STATES_LOCALE,
} from '../../src/utils/constants';
import SurveyDataExport from '../../src/components/survey-data-export';

const renderComponent = (props) =>
  render(
    <SurveyDataExport
      surveyId="p123"
      surveyName="ACME Survey"
      surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
      localeList={[
        {
          value: ENGLISH_UNITED_STATES_LOCALE,
          label: 'English (United States)',
        },
      ]}
      {...props}
    />
  );

describe('Jotunheim React Survey Data Export :: ', () => {
  it('should render with default props', () => {
    renderComponent({});

    expect(screen.getByTestId('survey-data-export')).toBeInTheDocument();
  });

  it('should render builder via extraFilters prop', () => {
    const Builder = () => <div>The builder</div>;

    renderComponent({
      extraFilters: <Builder />,
    });

    expect(screen.getByText(/the builder/i)).toBeInTheDocument();
  });

  it('should render date filter when displayDateFilter is set to true', () => {
    renderComponent({
      displayDateFilter: true,
    });

    expect(screen.getByTestId('date-filter')).toBeInTheDocument();
  });

  it('should not render date filter when displayDateFilter is set to false', () => {
    renderComponent({
      displayDateFilter: false,
    });

    expect(screen.queryByTestId('date-filter')).not.toBeInTheDocument();
  });

  it('should render advanced options when displayAdvancedOptions is set to true', () => {
    renderComponent({
      displayAdvancedOptions: true,
    });

    expect(screen.getByTestId('advanced-options')).toBeInTheDocument();
  });

  it('should not render advanced options when displayAdvancedOptions is set to false', () => {
    renderComponent({
      displayAdvancedOptions: false,
    });

    expect(screen.queryByTestId('advanced-options')).not.toBeInTheDocument();
  });

  it("should render dialog description when it's specified", () => {
    renderComponent({
      dialogDescription: 'Dialog Description',
    });

    expect(screen.getByTestId('alert')).toBeInTheDocument();
    expect(screen.getByText(/dialog description/i)).toBeInTheDocument();
  });

  it("should not render dialog description when it's not specified", () => {
    renderComponent({
      dialogDescription: '',
    });

    expect(screen.queryByTestId('alert')).not.toBeInTheDocument();
  });
});
