import React from 'react';
import {render, screen} from '@testing-library/react';
import moment from 'moment';
import {DateFilter} from '../../src/components/date-filter';
import {DATE_FILTER_TYPE} from '../../src/utils/constants';
import {buildTimeFromText} from '../../src/utils/date-time';

const renderComponent = (props) =>
  render(
    <DateFilter
      dateFilterSettings={{
        dateFilterType: DATE_FILTER_TYPE.None,
      }}
      {...props}
    />
  );

describe('Date Filter', () => {
  it('should hide time inputs when None filter', () => {
    renderComponent({
      dateFilterSettings: {
        dateFilterType: DATE_FILTER_TYPE.None,
      },
    });

    expect(screen.queryAllByText(/start date/i).length).toBe(0);
    expect(screen.queryAllByText(/end date/i).length).toBe(0);
  });

  it('should render time inputs when Range date filter', () => {
    renderComponent({
      dateFilterSettings: {
        dateFilterType: DATE_FILTER_TYPE.Range,
        startDate: moment('2019-01-28'),
        endDate: moment('2019-01-29'),
        startTime: buildTimeFromText('00:00'),
        endTime: buildTimeFromText('00:00'),
      },
    });

    expect(screen.getAllByText(/start date/i).length).toBe(2);
    expect(screen.getAllByText(/end date/i).length).toBe(2);
  });
});
