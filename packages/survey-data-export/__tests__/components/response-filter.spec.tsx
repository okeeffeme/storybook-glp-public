import React from 'react';
import {render, screen} from '@testing-library/react';

import {ResponseFilter} from '../../src/components/response-filter';
import {INTERVIEW_STATUS} from '../../src/utils/constants';

describe('Response filter', () => {
  it('should render filter for empty status list', () => {
    render(<ResponseFilter selectedStatuses={[]} />);

    expect(screen.getByRole('radio', {name: 'All Statuses'})).toHaveAttribute(
      'checked'
    );
    expect(
      screen.getByRole('checkbox', {name: 'Completed'})
    ).not.toHaveAttribute('checked');
    expect(
      screen.getByRole('checkbox', {name: 'Quota Full'})
    ).not.toHaveAttribute('checked');
    expect(
      screen.getByRole('checkbox', {name: 'Incomplete'})
    ).not.toHaveAttribute('checked');
    expect(
      screen.getByRole('checkbox', {name: 'Screened'})
    ).not.toHaveAttribute('checked');
    expect(screen.getByRole('checkbox', {name: 'Error'})).not.toHaveAttribute(
      'checked'
    );
  });

  it('should render filter for selected response statuses', () => {
    const selectedStatuses = [
      INTERVIEW_STATUS.Completed,
      INTERVIEW_STATUS.QuotaFull,
    ];

    render(<ResponseFilter selectedStatuses={selectedStatuses} />);

    expect(screen.getByRole('checkbox', {name: 'Completed'})).toHaveAttribute(
      'checked'
    );
    expect(screen.getByRole('checkbox', {name: 'Quota Full'})).toHaveAttribute(
      'checked'
    );
    expect(
      screen.getByRole('radio', {name: 'All Statuses'})
    ).not.toHaveAttribute('checked');
    expect(
      screen.getByRole('checkbox', {name: 'Incomplete'})
    ).not.toHaveAttribute('checked');
    expect(
      screen.getByRole('checkbox', {name: 'Screened'})
    ).not.toHaveAttribute('checked');
    expect(screen.getByRole('checkbox', {name: 'Error'})).not.toHaveAttribute(
      'checked'
    );
  });

  it('should render only available statuses', () => {
    const availableStatuses = [
      INTERVIEW_STATUS.Completed,
      INTERVIEW_STATUS.QuotaFull,
    ];

    render(
      <ResponseFilter
        selectedStatuses={[]}
        availableStatuses={availableStatuses}
      />
    );

    expect(
      screen.getByRole('radio', {name: 'All Statuses'})
    ).toBeInTheDocument();
    expect(
      screen.getByRole('checkbox', {name: 'Completed'})
    ).toBeInTheDocument();
    expect(
      screen.getByRole('checkbox', {name: 'Quota Full'})
    ).toBeInTheDocument();
    expect(
      screen.queryByRole('checkbox', {name: 'Incomplete'})
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole('checkbox', {name: 'Screened'})
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole('checkbox', {name: 'Error'})
    ).not.toBeInTheDocument();
  });
});
