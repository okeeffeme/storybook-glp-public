import React from 'react';
import {render, screen} from '@testing-library/react';
import {SurveyTitle} from '../../src/components/survey-title';

describe('Survey Title', () => {
  it('renders', () => {
    const surveyId = 'p123';
    const surveyName = 'ACME Survey';

    render(<SurveyTitle surveyId={surveyId} surveyName={surveyName} />);

    expect(screen.getByText(surveyId)).toBeInTheDocument();
    expect(screen.getByText(surveyName)).toBeInTheDocument();
  });
});
