import React from 'react';
import {render, screen} from '@testing-library/react';

import {SpssOptions} from '../../../src/components/advanced-options/spss-options';
import {getModelDefaults} from '../../../src/utils/model-provider';
import {
  DATABASE_TYPE,
  FILE_TYPE,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../../../src/utils/constants';
import moment from 'moment';

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

const renderComponent = (fileSettings) =>
  render(
    <SpssOptions
      fileType={fileSettings.fileType}
      excludeHierarchy={fileSettings.excludeHierarchy}
      characterEncodingName={fileSettings.characterEncodingName}
      decimalDelimiter={fileSettings.decimalDelimiter}
      includeAnswerLabelMultiQuestions={
        fileSettings.includeAnswerLabelMultiQuestions
      }
    />
  );

describe('Advanced Options :: SPSS Options', () => {
  it('should show Character Encoding when SPSS file', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.fileType = FILE_TYPE.SPSS;

    renderComponent(fileSettings);

    expect(screen.getByTestId('characterEncodingName')).toBeInTheDocument();
  });

  it('should hide Character Encoding when SPSSSav file', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.fileType = FILE_TYPE.SPSSSav;

    renderComponent(fileSettings);

    expect(
      screen.queryByTestId('characterEncodingName')
    ).not.toBeInTheDocument();
  });

  it('should show Decimal Delimiter when SPSS file', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.fileType = FILE_TYPE.SPSS;

    renderComponent(fileSettings);

    expect(screen.getByTestId('decimalDelimiter')).toBeInTheDocument();
  });

  it('should hide Decimal Delimiter when SPSSSav file', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.fileType = FILE_TYPE.SPSSSav;

    renderComponent(fileSettings);

    expect(screen.queryByTestId('decimalDelimiter')).not.toBeInTheDocument();
  });
});
