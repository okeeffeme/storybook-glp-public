import React from 'react';
import {render, screen} from '@testing-library/react';

import {LoopHandling} from '../../../src/components/advanced-options/loop-handling';
import {LOOP_HANDLING, LOOP_POSITION} from '../../../src/utils/constants';

const renderComponent = (props) =>
  render(
    <LoopHandling
      loopHandling={LOOP_HANDLING.SeparateFiles}
      loopPosition={LOOP_POSITION.AsQuestionnaire}
      {...props}
    />
  );

describe('Advanced Options :: Loop Handling', () => {
  it('should enable loopPosition when singleFile is selected for loopHandling', () => {
    renderComponent({loopHandling: LOOP_HANDLING.SingleFile});

    expect(
      screen.getByTestId('loopPosition').querySelector('input')
    ).not.toHaveAttribute('disabled');
  });

  it('should disable loopPosition when separateFiles is selected for loopHandling', () => {
    renderComponent({
      loopHandling: LOOP_HANDLING.SeparateFiles,
    });

    expect(
      screen.getByTestId('loopPosition').querySelector('input')
    ).toHaveAttribute('disabled');
  });
});
