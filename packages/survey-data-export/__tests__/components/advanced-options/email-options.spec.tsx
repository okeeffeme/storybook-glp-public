import React from 'react';
import {render, screen} from '@testing-library/react';

import {EmailOptions} from '../../../src/components/advanced-options/email-options';
import {FILE_ENCODING} from '../../../src/utils/constants';

const renderComponent = (props) =>
  render(
    <EmailOptions
      emailOverrideEnabled={true}
      mailEncodingName={FILE_ENCODING.UTF8}
      replyTo={''}
      replyToError={''}
      subject={''}
      plainTextBody={''}
      {...props}
    />
  );

describe('Advanced Options :: Email Options', () => {
  it('should show email encoding when email overrides are enabled', () => {
    renderComponent({emailOverrideEnabled: true});

    expect(screen.getAllByText('Reply To').length).toBe(2);
    expect(screen.getByTestId('mailEncodingName')).toBeInTheDocument();
  });

  it('should hide email encoding when email overrides are enabled', () => {
    renderComponent({emailOverrideEnabled: false});

    expect(screen.queryAllByText('Reply To').length).toBe(0);
    expect(screen.queryByTestId('mailEncodingName')).not.toBeInTheDocument();
  });
});
