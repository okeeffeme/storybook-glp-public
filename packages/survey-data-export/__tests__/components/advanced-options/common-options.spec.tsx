import React from 'react';
import {render, screen} from '@testing-library/react';

import {
  QUESTION_LABEL_TEXT,
  ANSWER_ELEMENT_LABELS,
  TEXT_IN_LABELS,
} from '../../../src/utils/constants';
import {CommonOptions} from '../../../src/components/advanced-options/common-options';

const renderComponent = (props) =>
  render(
    <CommonOptions
      exportLanguageId={9}
      surveyLanguages={[{value: 9, label: 'English'}]}
      questionLabelText={QUESTION_LABEL_TEXT.QuestionId}
      answerElementLabels={ANSWER_ELEMENT_LABELS.AnswerQuestionLabel}
      {...props}
    />
  );

describe('Advanced Options :: Common Options', () => {
  it('should show textInLabels when template is selected', () => {
    renderComponent({isTemplateSelected: true});

    expect(screen.getByTestId('textInLabels')).toBeInTheDocument();
  });

  it('should hide textInLabels when template is not selected', () => {
    renderComponent({isTemplateSelected: false});

    expect(screen.queryByTestId('textInLabels')).not.toBeInTheDocument();
  });

  it('should show label and language settings when template is not selected', () => {
    renderComponent({isTemplateSelected: false});

    expect(screen.getByTestId('questionLabelText')).toBeInTheDocument();
    expect(screen.getByTestId('answerElementLabels')).toBeInTheDocument();
    expect(screen.getByTestId('exportLanguageId')).toBeInTheDocument();
  });

  it('should show label and language settings when template is selected and textInLabels is from survey', () => {
    renderComponent({
      isTemplateSelected: true,
      textInLabels: TEXT_IN_LABELS.Survey,
    });

    expect(screen.getByTestId('questionLabelText')).toBeInTheDocument();
    expect(screen.getByTestId('answerElementLabels')).toBeInTheDocument();
    expect(screen.getByTestId('exportLanguageId')).toBeInTheDocument();
  });

  it('should hide label and language settings when template is selected and textInLabels is from template', () => {
    renderComponent({
      isTemplateSelected: true,
      textInLabels: TEXT_IN_LABELS.Template,
    });

    expect(screen.queryByTestId('questionLabelText')).not.toBeInTheDocument();
    expect(screen.queryByTestId('answerElementLabels')).not.toBeInTheDocument();
    expect(screen.queryByTestId('exportLanguageId')).not.toBeInTheDocument();
  });
});
