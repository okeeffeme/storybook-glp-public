import React from 'react';
import moment from 'moment';
import {render, screen} from '@testing-library/react';

import {DelimitedTextOptions} from '../../../src/components/advanced-options/delimited-text-options';
import {
  FileSettings,
  getModelDefaults,
} from '../../../src/utils/model-provider';
import {
  DATABASE_TYPE,
  FILE_FORMAT_TYPE,
  FILE_TYPE,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../../../src/utils/constants';

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

const renderComponent = (fileSettings: FileSettings) =>
  render(
    <DelimitedTextOptions
      locale={fileSettings.locale}
      mediaFiles={fileSettings.mediaFiles}
      openEndHandling={fileSettings.openEndHandling}
      fileEncodingName={fileSettings.fileEncodingName}
      formatting={fileSettings.formatting}
      formattingDelimiterError={''}
    />
  );

describe('Advanced Options :: Delimited Text Options', () => {
  it('should show Custom Delimiter when Custom file format', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.Custom;

    renderComponent(fileSettings);

    expect(screen.getByRole('textbox', {name: 'Custom Delimiter'}));
  });

  it('should hide Custom Delimiter when TabSeparated file format', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.TabSeparated;

    renderComponent(fileSettings);

    expect(screen.queryByRole('textbox', {name: 'Custom Delimiter'}));
  });

  it('should show Quoted Values when Custom file format', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.Custom;

    renderComponent(fileSettings);

    expect(
      screen.getByRole('checkbox', {name: 'Text Qualifier (With Quotes)'})
    ).toBeInTheDocument();
  });

  it('should show Quoted Values when CommaSeparated file format', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.CommaSeparated;

    renderComponent(fileSettings);

    expect(
      screen.getByRole('checkbox', {name: 'Text Qualifier (With Quotes)'})
    ).toBeInTheDocument();
  });

  it('should hide Quoted Values when TabSeparated file format', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.TabSeparated;

    renderComponent(fileSettings);

    expect(
      screen.queryByRole('checkbox', {name: 'Text Qualifier (With Quotes)'})
    ).not.toBeInTheDocument();
  });

  it('should show Media File Naming when includeMedia is enabled', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.mediaFiles.includeMedia = true;

    renderComponent(fileSettings);

    expect(screen.getByTestId('mediaFileNaming')).toBeInTheDocument();
  });

  it('should hide Media File Naming when includeMedia is disabled', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);
    fileSettings.mediaFiles.includeMedia = false;

    renderComponent(fileSettings);

    expect(screen.queryByTestId('mediaFileNaming')).not.toBeInTheDocument();
  });
});
