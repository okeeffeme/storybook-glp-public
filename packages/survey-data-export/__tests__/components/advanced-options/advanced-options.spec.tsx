import React from 'react';
import {render, screen} from '@testing-library/react';

import {getModelDefaults} from '../../../src/utils/model-provider';
import {
  FILE_TYPE,
  ENGLISH_LANGUAGE_ID,
  DEFAULT_LANGUAGE,
  TRANSFER_TYPE,
  SCHEDULE_TYPE,
  DATABASE_TYPE,
} from '../../../src/utils/constants';
import AdvancedOptions from '../../../src/components/advanced-options/advanced-options';
import moment from 'moment/moment';

const componentProps = {
  language: DEFAULT_LANGUAGE,
  collapsed: false,
  validationErrors: {},
};

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

const assertOptions = (config) => {
  expect(screen.queryAllByTestId('truncate-open-ends')).toHaveLength(
    config.truncateOpenEnds
  );
  expect(screen.queryAllByTestId('questionLabelText')).toHaveLength(
    config.commonOptions
  );
  expect(screen.queryAllByTestId('fileFormatType')).toHaveLength(
    config.delimitedTextOptions
  );
  expect(screen.queryAllByTestId('version')).toHaveLength(
    config.tripleSOptions
  );
  expect(screen.queryAllByTestId('characterEncodingName')).toHaveLength(
    config.spssOptions
  );
  expect(screen.queryAllByTestId('quantum-options')).toHaveLength(
    config.quantumOptions
  );
  expect(screen.queryAllByTestId('sas-options')).toHaveLength(
    config.sasOptions
  );
  expect(screen.queryAllByTestId('filesToInclude')).toHaveLength(
    config.fixedWidthOptions
  );
  expect(screen.queryAllByTestId('loopHandling')).toHaveLength(
    config.loopHandling
  );
};

// need to use mount here because React.memo wrapped components have generic "Component" name
const renderComponent = (modelProps) =>
  render(
    <AdvancedOptions
      model={getModelDefaults({
        defaultFileType: FILE_TYPE.Excel,
        defaultExportLanguageId: ENGLISH_LANGUAGE_ID,
        defaultResponseStatuses: [],
        ...modelProps,
      })}
      {...componentProps}
    />
  );

describe('Advanced Options', () => {
  it('should render correct options for Excel', () => {
    const component = renderComponent({
      defaultFileType: FILE_TYPE.Excel,
    });

    expect(component.container).toMatchSnapshot();

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for ExcelWithLabels', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.ExcelWithLabels,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for DelimitedTextFile', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.DelimitedTextFile,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 1,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for DelimitedTxtWithLabels', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.DelimitedTxtWithLabels,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 1,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for TripleS', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.TripleS,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 1,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 1,
      loopHandling: 1,
    });
  });

  it('should render correct options for TripleSConfirmit', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.TripleSConfirmit,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 1,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 1,
      loopHandling: 1,
    });
  });

  it('should render correct options for SPSS', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.SPSS,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 1,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for SPSSSav', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.SPSSSav,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for Quantum', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.Quantum,
    });

    assertOptions({
      truncateOpenEnds: 0,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 0,
    });
  });

  it('should render correct options for SAS', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.SAS,
    });

    assertOptions({
      truncateOpenEnds: 0,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 0,
      loopHandling: 1,
    });
  });

  it('should render correct options for FixedWidthFile', () => {
    renderComponent({
      defaultFileType: FILE_TYPE.FixedWidthFile,
    });

    assertOptions({
      truncateOpenEnds: 1,
      commonOptions: 1,
      delimitedTextOptions: 0,
      tripleSOptions: 0,
      spssOptions: 0,
      quantumOptions: 0,
      sasOptions: 0,
      fixedWidthOptions: 1,
      loopHandling: 1,
    });
  });

  describe('Email Overrides', () => {
    it('should render overrides when Email transfer is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Email;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(screen.getAllByTestId('email-options')).toHaveLength(1);
    });

    it('should hide overrides when FTP transfer is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.FTP;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(screen.queryAllByTestId('email-options')).toHaveLength(0);
    });

    it('should hide overrides when Download transfer is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Download;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(screen.queryAllByTestId('email-options')).toHaveLength(0);
    });

    it('should render OverrideFileName when Email transfer is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Email;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(
        screen.getByRole('checkbox', {
          name: 'Override auto-generated export file name',
        })
      ).toBeInTheDocument();
    });

    it('should render OverrideFileName when Download to browser is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Download;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(
        screen.queryByRole('checkbox', {
          name: 'Override auto-generated export file name',
        })
      ).toBeInTheDocument();
    });

    it('should render Uncompressed when Email transfer is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Email;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(
        screen.getByRole('checkbox', {
          name: 'Deliver the files uncompressed',
        })
      ).toBeInTheDocument();
    });

    it('should render Uncompressed when Download to browser is selected', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Download;

      render(<AdvancedOptions model={model} {...componentProps} />);

      expect(
        screen.queryByRole('checkbox', {
          name: 'Deliver the files uncompressed',
        })
      ).toBeInTheDocument();
    });
  });
});
