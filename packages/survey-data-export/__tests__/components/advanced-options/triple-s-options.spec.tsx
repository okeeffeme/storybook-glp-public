import React from 'react';
import moment from 'moment/moment';
import {render} from '@testing-library/react';
import {
  FileSettings,
  getModelDefaults,
} from '../../../src/utils/model-provider';
import {
  DATABASE_TYPE,
  FILE_TYPE,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../../../src/utils/constants';

import {TripleSOptions} from '../../../src/components/advanced-options/triple-s-options';

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

const renderComponent = (fileSettings: FileSettings) =>
  render(
    <TripleSOptions
      version={fileSettings.version}
      format={fileSettings.format}
      multiQuestionFormat={fileSettings.multiQuestionFormat}
      filesToInclude={fileSettings.filesToInclude}
      fileEncodingName={fileSettings.fileEncodingName}
      schemaEncodingName={fileSettings.schemaEncodingName}
      recodeMultis={fileSettings.recodeMultis}
      excludeHierarchy={fileSettings.excludeHierarchy}
    />
  );

describe('Advanced Options :: Triple-S Options', () => {
  it('renders', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);

    const {container} = renderComponent(fileSettings);

    expect(container).toMatchSnapshot();
  });
});
