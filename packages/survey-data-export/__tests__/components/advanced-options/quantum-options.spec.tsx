import React from 'react';
import {render} from '@testing-library/react';

import QuantumOptions from '../../../src/components/advanced-options/quantum-options';

describe('Advanced Options :: Quantum Options', () => {
  it('renders', () => {
    const {container} = render(
      <QuantumOptions excludeHierarchy={false} recodeMultis={true} />
    );

    expect(container).toMatchSnapshot();
  });
});
