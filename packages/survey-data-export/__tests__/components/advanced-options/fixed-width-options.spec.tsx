import React from 'react';
import {render} from '@testing-library/react';

import {FixedWidthOptions} from '../../../src/components/advanced-options/fixed-width-options';
import {getModelDefaults} from '../../../src/utils/model-provider';
import {
  DATABASE_TYPE,
  FILE_TYPE,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../../../src/utils/constants';
import moment from 'moment';

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

describe('Advanced Options :: Fixed Width Options', () => {
  it('renders', () => {
    const {fileSettings} = getModelDefaults(modelDefaults);

    const {container} = render(
      <FixedWidthOptions
        filesToInclude={fileSettings.filesToInclude}
        fileEncodingName={fileSettings.fileEncodingName}
        schemaEncodingName={fileSettings.schemaEncodingName}
        recodeMultis={fileSettings.recodeMultis}
      />
    );

    expect(container).toMatchSnapshot();
  });
});
