import {translate} from '../src/utils/translation';

describe('Translation utils:: ', () => {
  it('should return english text when english is preferred language', () => {
    const text = translate({language: 'en', key: 'exportSetup.title'});
    expect(text).toBe('Export Setup');
  });

  it('should return norwegian text when norwegian is preferred language', () => {
    const text = translate({language: 'nb', key: 'exportSetup.title'});
    expect(text).toBe('Eksportinstillinger');
  });

  it('should default to english text when polish is preferred language but translation does not exist', () => {
    const text = translate({language: 'pl', key: 'exportSetup.title'});
    expect(text).toBe('Export Setup');
  });

  it('should return undefined when english is preferred language but translation does not exist', () => {
    const text = translate({
      language: 'en',
      key: 'exportSetup.title.nonExistingTranslation',
    });
    expect(text).toBe(undefined);
  });
});
