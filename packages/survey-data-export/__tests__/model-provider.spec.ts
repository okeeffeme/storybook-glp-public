import moment from 'moment';
import deepFreeze from '../test-utils/deepFreeze';
import {getContractData, getModelDefaults} from '../src/utils/model-provider';
import {
  DATABASE_TYPE,
  FILE_TYPE,
  INTERVIEW_STATUS,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../src';
import {FILE_ENCODING} from '../src/utils/constants';

const modelDefaults = {
  databases: [DATABASE_TYPE.Production],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.Excel,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 9,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

describe('Model Provider :: ', () => {
  describe('getModelDefaults :: ', () => {
    it('getModelDefaults should return correct model', () => {
      const model = getModelDefaults(modelDefaults);

      // need to exclude startDate and endDate properties (moments) to make comparison easier
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const {startDate, endDate, ...dateFilterSettings} =
        model.dateFilterSettings;

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const {dateFilterSettings: _dateFilterSettings, ...modelRest} = model;

      const expectedModel = {
        answerElementLabels: 'AnswerQuestionLabel',
        comment: '',
        exportLanguageId: 9,
        fileSettings: {
          characterEncodingName: 'UTF8',
          decimalDelimiter: '.',
          excludeHierarchy: false,
          fileEncodingName: 'UTF8',
          fileType: 'Excel',
          filesToInclude: 'SchemaAndData',
          format: 'Fixed',
          formatting: {
            delimiter: '',
            fileFormatType: 'TabSeparated',
            quotedValues: true,
          },
          includeAnswerLabelMultiQuestions: true,
          locale: 1033,
          mediaFiles: {includeMedia: false, mediaFileNaming: 'Detailed'},
          multiQuestionFormat: 'Bitstring',
          openEndHandling: 'IncludeOpenEnds',
          recodeMultis: false,
          schemaEncodingName: 'UTF8',
          truncateOpenEnds: '200',
          truncateOpenEndsEnabled: false,
          version: 'Xml20',
        },
        loopHandling: 'SeparateFiles',
        loopPosition: 'AsQuestionnaire',
        onlyDataChangedSinceLastRun: false,
        questionLabelText: 'QuestionId',
        responseStatusesFilter: [],
        templateSettings: {
          id: '-999999',
          templateType: '',
          textInLabels: 'Template',
        },
        transferSettings: {
          transferType: 'Download',
          emailRecipient: 'user123@confirmit.com',
          emailOverride: {
            emailOverrideEnabled: false,
            mailEncodingName: FILE_ENCODING.UTF8,
            plainTextBody: '',
            replyTo: '',
            subject: '',
          },
          executionSettings: {
            type: 'asap',
          },
          folderName: '',
          hostAndPort: undefined,
          overrideFileName: '^PROJECTID^_^USERID^_^TASKID^',
          overrideFileNameEnabled: false,
          password: undefined,
          useSftp: true,
          userName: undefined,
          uncompressed: false,
        },
        database: 'production',
      };

      expect(modelRest).toEqual(expectedModel);
      expect(dateFilterSettings).toEqual(expectedDateFilter);
    });

    it('getModelDefaults should return correct model when initial values are specified', () => {
      const model = getModelDefaults({
        databases: [],
        defaultScheduleType: SCHEDULE_TYPE.Asap,
        defaultFileType: FILE_TYPE.DelimitedTextFile,
        defaultTransferType: TRANSFER_TYPE.FTP,
        defaultResponseStatuses: [INTERVIEW_STATUS.Completed],
        defaultExportLanguageId: 25,
        defaultEmailRecipient: 'user123@confirmit.com',
        startDate: moment('2019-01-24T00:00:00.000Z'),
      });

      // need to exclude startDate and endDate properties (moments) to make comparison easier
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const {startDate, endDate, ...dateFilterRest} = model.dateFilterSettings;

      expect(model.fileSettings.fileType).toBe(FILE_TYPE.DelimitedTextFile);
      expect(model.transferSettings.transferType).toBe(TRANSFER_TYPE.FTP);
      expect(model.responseStatusesFilter).toEqual(['Complete']);
      expect(model.exportLanguageId).toBe(25);
      expect(model.transferSettings.emailRecipient).toBe(
        'user123@confirmit.com'
      );
      expect(dateFilterRest).toEqual(expectedDateFilter);
    });
  });

  describe('getContractData :: ', () => {
    it('should return correct data for default model', () => {
      const model = getModelDefaults(modelDefaults);

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.exportLanguageId).toBe(9);
      expect(contract.responseStatusesFilter).toEqual([]);
      expect(contract.fileSettings).not.toBeUndefined();
      expect(contract.transferSettings).not.toBeUndefined();
      expect(contract.dateFilterSettings).not.toBeUndefined();
    });

    it('should return startDate and endDate in ISO format', () => {
      const model = getModelDefaults({
        databases: [],
        defaultScheduleType: SCHEDULE_TYPE.Asap,
        defaultFileType: FILE_TYPE.DelimitedTextFile,
        defaultTransferType: TRANSFER_TYPE.Download,
        defaultResponseStatuses: [],
        defaultExportLanguageId: 25,
        defaultEmailRecipient: 'user123@confirmit.com',
        startDate: moment('2019-01-24T00:00:00.000Z'),
      });

      deepFreeze(model);

      const contract = getContractData(model);

      const expectedContract = {
        dateFilterType: 'None',
        startDate: '2019-01-24T00:00:00.000+01:00',
        endDate: '2019-01-25T00:00:00.000+01:00',
      };

      expect(contract.dateFilterSettings).toEqual(expectedContract);
    });

    it('should return templateSettings when template is selected', () => {
      const model = getModelDefaults(modelDefaults);

      if (!model.templateSettings) {
        return;
      }

      model.templateSettings.id = '1';
      model.templateSettings.templateType = 'ClassicTemplate';

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.templateSettings).toEqual({
        id: '1',
        templateType: 'ClassicTemplate',
        textInLabels: 'Template',
      });
    });

    it('should not return templateSettings when template is not selected', () => {
      const model = getModelDefaults(modelDefaults);

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.templateSettings).toBeUndefined();
    });

    it('should return truncateOpenEnds when truncateOpenEndsEnabled is true', () => {
      const model = getModelDefaults(modelDefaults);
      model.fileSettings.truncateOpenEndsEnabled = true;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.fileSettings.truncateOpenEnds).toBe('200');
      expect(contract.fileSettings.truncateOpenEndsEnabled).toBe(true);
    });

    it('should not return truncateOpenEnds when truncateOpenEndsEnabled is false', () => {
      const model = getModelDefaults(modelDefaults);
      model.fileSettings.truncateOpenEndsEnabled = false;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.fileSettings.truncateOpenEnds).toBeUndefined();
      expect(contract.fileSettings.truncateOpenEndsEnabled).toBeUndefined();
    });

    it('should return correct default overrideFileName when transferType is Download', () => {
      const model = getModelDefaults({
        ...modelDefaults,
        defaultTransferType: TRANSFER_TYPE.Download,
      });

      expect(model.transferSettings.overrideFileName).toEqual(
        '^PROJECTID^_^USERID^_^TASKID^'
      );
    });

    it('should return correct default overrideFileName when transferType is not Download', () => {
      const model = getModelDefaults({
        ...modelDefaults,
        defaultTransferType: TRANSFER_TYPE.Email,
      });

      expect(model.transferSettings.overrideFileName).toEqual(
        '^PROJECTID^_^USERID^_^START^_^END^_^TASKID^'
      );
    });

    it('should return overrideFileName when overrideFileNameEnabled is true', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.overrideFileNameEnabled = true;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.transferSettings.overrideFileName).toBe(
        '^PROJECTID^_^USERID^_^TASKID^'
      );
      expect(contract.transferSettings.overrideFileNameEnabled).toBe(true);
    });

    it('should not return overrideFileName when overrideFileNameEnabled is false', () => {
      const model = getModelDefaults(modelDefaults);
      model.transferSettings.overrideFileNameEnabled = false;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.transferSettings.overrideFileName).toBeUndefined();
      expect(contract.transferSettings.overrideFileNameEnabled).toBeUndefined();
    });

    it('should return emailOverride when emailOverrideEnabled is true', () => {
      const model = getModelDefaults(modelDefaults);

      if (!model.transferSettings.emailOverride) {
        return;
      }

      model.transferSettings.emailOverride.emailOverrideEnabled = true;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.transferSettings.emailOverride).toEqual({
        emailOverrideEnabled: true,
        mailEncodingName: 'UTF8',
        plainTextBody: '',
        replyTo: '',
        subject: '',
      });
    });

    it('should not return emailOverride when emailOverrideEnabled is false', () => {
      const model = getModelDefaults(modelDefaults);

      if (!model.transferSettings.emailOverride) {
        return;
      }

      model.transferSettings.emailOverride.emailOverrideEnabled = false;

      deepFreeze(model);
      const contract = getContractData(model);

      expect(contract.transferSettings.emailOverride).toBeUndefined();
    });
  });
});

const expectedDateFilter = {
  dateFilterType: 'None',
  endTime: {hour: '00', minute: '00', text: '00:00'},
  startTime: {hour: '00', minute: '00', text: '00:00'},
};
