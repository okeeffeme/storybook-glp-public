import moment from 'moment';
import {
  validCharacters,
  isIntegerInRange,
  isValidDate,
  isValidTime,
  isEndDateAfterStartDate,
} from '../../src/validation/validation-utils';
import {buildTimeFromText} from '../../src/utils/date-time';

describe('validation-utils :: ', () => {
  describe('validCharacters', () => {
    it('should return true when input is valid', () => {
      const result = validCharacters('should be valid text');
      expect(result).toBe(true);
    });

    it('should return true when input is an empty string', () => {
      const result = validCharacters('');
      expect(result).toBe(true);
    });

    it('should return false when input has invalid characters', () => {
      const result = validCharacters('not < valid >');
      expect(result).toBe(false);
    });
  });

  describe('isIntegerInRange', () => {
    it('should be true when number in range', () => {
      const result = isIntegerInRange({value: '3', minValue: 1, maxValue: 10});
      expect(result).toBe(true);
    });

    it('should be true when number is equal to minValue', () => {
      const result = isIntegerInRange({value: '3', minValue: 1, maxValue: 10});
      expect(result).toBe(true);
    });

    it('should be true when number is equal to maxValue', () => {
      const result = isIntegerInRange({value: '10', minValue: 1, maxValue: 10});
      expect(result).toBe(true);
    });

    it('should be false for invalid string', () => {
      const result = isIntegerInRange({
        value: 'invalid',
        minValue: 1,
        maxValue: 10,
      });
      expect(result).toBe(false);
    });

    it('should be false for string which starts with number', () => {
      const result = isIntegerInRange({
        value: '2abc',
        minValue: 1,
        maxValue: 10,
      });
      expect(result).toBe(false);
    });
  });

  describe('isValidDate', () => {
    it('should be true for valid date', () => {
      const result = isValidDate(moment('2019-02-06'));
      expect(result).toBe(true);
    });

    it('should be false for empty date', () => {
      const emptyInput = '';
      const result = isValidDate(moment(emptyInput));
      expect(result).toBe(false);
    });
  });

  describe('isValidTime', () => {
    it('should be true for valid time', () => {
      const result = isValidTime({hour: '9', minute: '25'});
      expect(result).toBe(true);
    });

    it('should be false for invalid time', () => {
      expect(isValidTime({hour: '', minute: ''})).toBe(false);
      expect(isValidTime({hour: '', minute: '45'})).toBe(false);
      expect(isValidTime({hour: '9', minute: ''})).toBe(false);
    });
  });

  describe('isEndDateAfterStartDate', () => {
    it('should be true when end date is after start date', () => {
      const result = isEndDateAfterStartDate({
        startDate: moment(),
        startTime: buildTimeFromText('00:00'),
        endDate: moment().add(1, 'd'),
        endTime: buildTimeFromText('00:00'),
      });
      expect(result).toBe(true);
    });

    it('should be true when same day but endTime is after startTime', () => {
      const result = isEndDateAfterStartDate({
        startDate: moment(),
        startTime: buildTimeFromText('00:00'),
        endDate: moment(),
        endTime: buildTimeFromText('14:00'),
      });
      expect(result).toBe(true);
    });

    it('should be false when same day but endTime is before startTime', () => {
      const result = isEndDateAfterStartDate({
        startDate: moment(),
        startTime: buildTimeFromText('11:00'),
        endDate: moment(),
        endTime: buildTimeFromText('10:00'),
      });
      expect(result).toBe(false);
    });

    it('should be false when end date is before start date', () => {
      const result = isEndDateAfterStartDate({
        startDate: moment().add(1, 'd'),
        startTime: buildTimeFromText('00:00'),
        endDate: moment(),
        endTime: buildTimeFromText('00:00'),
      });
      expect(result).toBe(false);
    });

    it('should be false when end date and start date are equal', () => {
      const result = isEndDateAfterStartDate({
        startDate: moment(),
        startTime: buildTimeFromText('00:00'),
        endDate: moment(),
        endTime: buildTimeFromText('00:00'),
      });
      expect(result).toBe(false);
    });
  });
});
