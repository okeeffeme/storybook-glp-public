import moment from 'moment';
import {
  validateModel,
  checkIsValid,
} from '../../src/validation/model-validation';
import {getModelDefaults} from '../../src/utils/model-provider';
import {
  DATE_FILTER_TYPE,
  FILE_FORMAT_TYPE,
  FILE_TYPE,
  TRANSFER_TYPE,
  DEFAULT_LANGUAGE,
  SCHEDULE_TYPE,
} from '../../src/utils/constants';
import {buildTimeFromText} from '../../src/utils/date-time';

const modelDefaults = {
  databases: [],
  defaultScheduleType: SCHEDULE_TYPE.Asap,
  defaultFileType: FILE_TYPE.DelimitedTextFile,
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultResponseStatuses: [],
  defaultExportLanguageId: 25,
  defaultEmailRecipient: 'user123@confirmit.com',
  startDate: moment('2019-01-24T00:00:00.000Z'),
};

describe('model-validation :: ', () => {
  it('should be valid model when default model provided', () => {
    const validationErrors = validateModel({
      model: getModelDefaults(modelDefaults),
      language: DEFAULT_LANGUAGE,
    });

    expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(true);
  });

  it('should not be valid model when schedule is invalid', () => {
    const validationErrors = validateModel({
      model: getModelDefaults(modelDefaults),
      language: DEFAULT_LANGUAGE,
    });

    expect(checkIsValid({validationErrors, isScheduleValid: false})).toBe(
      false
    );
  });

  describe('validate truncateOpenEnds', () => {
    const assertTruncateOpenEndsIsInvalid = (validationErrors) => {
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.truncateOpenEnds).toBeTruthy();
    };
    let model;

    beforeEach(() => {
      model = getModelDefaults(modelDefaults);
    });

    it('should be valid model when truncateOpenEnds contains numbers', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '300';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid model when truncateOpenEnds is empty', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });

    it('should be valid model when truncateOpenEnds is empty and truncation is disabled', () => {
      model.fileSettings.truncateOpenEnds = '';
      model.fileSettings.truncateOpenEndsEnabled = false;

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should be valid model when truncateOpenEnds is empty and selected Quantum file hides the field', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '';
      model.fileSettings.fileType = FILE_TYPE.Quantum;

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid model when truncateOpenEnds contains invalid characters', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '2<&';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });

    it('should not be valid model when truncateOpenEnds contains letters', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '2abc';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });

    it('should not be valid model when truncateOpenEnds has more than 4 digits', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '12345';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });

    it('should not be valid model when truncateOpenEnds is 0', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '0';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });

    it('should not be valid model when truncateOpenEnds is greater than 4000', () => {
      model.fileSettings.truncateOpenEndsEnabled = true;
      model.fileSettings.truncateOpenEnds = '4001';
      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      assertTruncateOpenEndsIsInvalid(validationErrors);
    });
  });

  describe('validate custom delimiter', () => {
    let model;

    beforeEach(() => {
      model = getModelDefaults(modelDefaults);
    });

    it('should be valid model when delimiter is valid', () => {
      model.fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.Custom;
      model.fileSettings.formatting.delimiter = '===';

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid model when delimiter is empty', () => {
      model.fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.Custom;
      model.fileSettings.formatting.delimiter = '';

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(validationErrors.formattingDelimiter).toBeTruthy();
    });

    it('should be valid model when delimiter is empty but CommaSeparated is selected', () => {
      model.fileSettings.formatting.fileFormatType =
        FILE_FORMAT_TYPE.CommaSeparated;
      model.fileSettings.formatting.delimiter = '';

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid model when delimiter has invalid characters', () => {
      model.fileSettings.formatting.fileFormatType = FILE_FORMAT_TYPE.Custom;
      model.fileSettings.formatting.delimiter = '<<';

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(validationErrors.formattingDelimiter).toBeTruthy();
    });
  });

  describe('validate date range fields', () => {
    let model;

    beforeEach(() => {
      model = getModelDefaults(modelDefaults);
      model.dateFilterSettings.dateFilterType = DATE_FILTER_TYPE.Range;
    });

    it('should be valid when Range is selected with default values', () => {
      model.dateFilterSettings.dateFilterType = DATE_FILTER_TYPE.Range; // to be explicit

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid when startDate is invalid', () => {
      model.dateFilterSettings.startDate = null;

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.startDateError).toBeTruthy();
    });

    it('should not be valid when endDate is invalid', () => {
      model.dateFilterSettings.endDate = null;

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.endDateError).toBeTruthy();
    });

    it('should be valid when startTime is invalid', () => {
      model.dateFilterSettings.startTime = buildTimeFromText('13:28');

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid when startTime is invalid', () => {
      model.dateFilterSettings.startTime = buildTimeFromText('13:2a');

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.startTimeError).toBeTruthy();
    });

    it('should be valid when endTime is valid', () => {
      model.dateFilterSettings.endTime = buildTimeFromText('13:28');

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        true
      );
    });

    it('should not be valid when endTime is invalid', () => {
      model.dateFilterSettings.endTime = buildTimeFromText('1z:28');

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.endTimeError).toBeTruthy();
    });

    it('should not be valid when start date-time is after end date-time', () => {
      model.dateFilterSettings.startDate = moment('2019-01-28');
      model.dateFilterSettings.endDate = moment('2019-01-28');
      model.dateFilterSettings.startTime = buildTimeFromText('13:30');
      model.dateFilterSettings.endTime = buildTimeFromText('13:00');

      const validationErrors = validateModel({
        model,
        language: DEFAULT_LANGUAGE,
      });
      expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
        false
      );
      expect(validationErrors.endDateBeforeStartDateError).toBeTruthy();
    });
  });

  describe('validate email recipient', () => {
    let model;

    describe('when Email transfer', () => {
      beforeEach(() => {
        model = getModelDefaults(modelDefaults);
        model.transferSettings.transferType = TRANSFER_TYPE.Email;
      });

      it('should be valid model when emailRecipient is valid', () => {
        model.transferSettings.emailRecipient = 'user123@confirmit.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when emailRecipient contains two emails separated by semicolon', () => {
        model.transferSettings.emailRecipient =
          'firstUser@forsta.com;secondUser@forsta.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when emailRecipient contains two emails separated by comma', () => {
        model.transferSettings.emailRecipient =
          'firstUser@forsta.com,secondUser@forsta.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should not be valid model when emailRecipient is empty', () => {
        model.transferSettings.emailRecipient = '';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.emailRecipient).toBeTruthy();
      });

      it('should not be valid model when emailRecipient is invalid', () => {
        model.transferSettings.emailRecipient = 'user123@confirmit';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.emailRecipient).toBeTruthy();
      });

      it('should not be valid model when one of the emails is invalid', () => {
        model.transferSettings.emailRecipient =
          'userA@forsta.com;userB<@forsta.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.emailRecipient).toBeTruthy();
      });

      it('should not be valid model when all emails are invalid', () => {
        model.transferSettings.emailRecipient =
          'userA<@forsta.com;userB<@forsta.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.emailRecipient).toBeTruthy();
      });
    });

    describe('when Download to browser is selected', () => {
      beforeEach(() => {
        model = getModelDefaults(modelDefaults);
        model.transferSettings.transferType = TRANSFER_TYPE.Download;
      });

      it('should be valid model when delimiter is valid', () => {
        model.transferSettings.emailRecipient = 'user123@confirmit.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when emailRecipient is empty', () => {
        model.transferSettings.emailRecipient = '';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when emailRecipient is invalid', () => {
        model.transferSettings.emailRecipient = 'user123@confirmit';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });
    });
  });

  describe('validate replyTo email', () => {
    let model;

    describe('when Email transfer is selected and email overrides are enabled', () => {
      beforeEach(() => {
        model = getModelDefaults({
          ...modelDefaults,
          defaultEmailRecipient: 'user123@confirmit.com',
          defaultSubject: 'subject message',
          defaultPlainTextBody: 'plainTextBody message',
        });
        model.transferSettings.transferType = TRANSFER_TYPE.Email;
        model.transferSettings.emailOverride.emailOverrideEnabled = true;
      });

      it('should be valid model when replyTo is valid', () => {
        model.transferSettings.emailOverride.replyTo = 'user123@confirmit.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when replyTo is empty', () => {
        model.transferSettings.emailOverride.replyTo = '';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should not be valid model when replyTo is invalid', () => {
        model.transferSettings.emailOverride.replyTo = 'user123@confirmit';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.replyTo).toBeTruthy();
      });

      it('should not be valid model when replyTo contains two emails', () => {
        model.transferSettings.emailOverride.replyTo =
          'userA@forsta.com;userB@forsta.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(validationErrors.replyTo).toBeTruthy();
      });
    });

    describe('validate subject and plainTextBody', () => {
      let model;

      describe('when Email transfer is selected and email overrides are enabled', () => {
        beforeEach(() => {
          model = getModelDefaults({
            ...modelDefaults,
            defaultEmailRecipient: 'user123@confirmit.com',
          });
          model.transferSettings.transferType = TRANSFER_TYPE.Email;
          model.transferSettings.emailOverride.emailOverrideEnabled = true;
        });

        it('should be valid model when subject and plainTextBody are not empty', () => {
          model.transferSettings.emailOverride.replyTo =
            'user123@confirmit.com';
          model.transferSettings.emailOverride.subject = 'subject message';
          model.transferSettings.emailOverride.plainTextBody =
            'plainTextBody message';
          const validationErrors = validateModel({
            model,
            language: DEFAULT_LANGUAGE,
          });
          expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
            true
          );
        });

        it('should not be valid model when subject is empty and plainBodyText is not', () => {
          model.transferSettings.emailOverride.replyTo =
            'user123@confirmit.com';
          model.transferSettings.emailOverride.subject = '';
          model.transferSettings.emailOverride.plainTextBody =
            'plainTextBody message';

          const validationErrors = validateModel({
            model,
            language: DEFAULT_LANGUAGE,
          });
          expect(validationErrors.subject).toBeTruthy();
        });

        it('should not be valid model when plainBodyText is empty and subject is not', () => {
          model.transferSettings.emailOverride.replyTo =
            'user123@confirmit.com';
          model.transferSettings.emailOverride.subject = 'subject message';
          model.transferSettings.emailOverride.plainTextBody = '';

          const validationErrors = validateModel({
            model,
            language: DEFAULT_LANGUAGE,
          });
          expect(validationErrors.plainTextBody).toBeTruthy();
        });

        it('should not be valid model when plainTextBody and subject are empty', () => {
          const validationErrors = validateModel({
            model,
            language: DEFAULT_LANGUAGE,
          });
          expect(validationErrors.subject).toBeTruthy();
          expect(validationErrors.plainTextBody).toBeTruthy();
        });
      });
    });

    describe('when Email transfer is selected and email overrides are disabled', () => {
      beforeEach(() => {
        model = getModelDefaults({
          ...modelDefaults,
          defaultEmailRecipient: 'user123@confirmit.com',
        });
        model.transferSettings.transferType = TRANSFER_TYPE.Email;
        model.transferSettings.emailOverride.emailOverrideEnabled = false;
      });

      it('should be valid model when replyTo is valid', () => {
        model.transferSettings.emailOverride.replyTo = 'user123@confirmit.com';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when replyTo is empty', () => {
        model.transferSettings.emailOverride.replyTo = '';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should be valid model when replyTo is invalid', () => {
        model.transferSettings.emailOverride.replyTo = 'user123@confirmit';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });
    });
  });

  describe('validate override file name', () => {
    let model;

    beforeEach(() => {
      model = getModelDefaults(modelDefaults);
      model.transferSettings.transferType = TRANSFER_TYPE.Email;
      model.transferSettings.emailRecipient = 'user@confirmit.com';
    });

    describe('when overrideFileNameEnabled is disabled', () => {
      it('should be valid model when overrideFileName constains invalid characters', () => {
        model.transferSettings.overrideFileNameEnabled = false;
        model.transferSettings.overrideFileName = '<>with_invalid_characters';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });
    });

    describe('when overrideFileNameEnabled is enabled', () => {
      it('should be valid model when overrideFileNameEnabled is enabled and overrideFileName is valid', () => {
        model.transferSettings.overrideFileNameEnabled = true;
        model.transferSettings.overrideFileName = '^PROJECTID^_^USERID^';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });

        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          true
        );
      });

      it('should not be valid model when overrideFileName contains invalid characters', () => {
        model.transferSettings.overrideFileNameEnabled = true;
        model.transferSettings.overrideFileName = '<>with_invalid_characters';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          false
        );
      });

      it('should not be valid model when overrideFileName is empty', () => {
        model.transferSettings.overrideFileNameEnabled = true;
        model.transferSettings.overrideFileName = '';

        const validationErrors = validateModel({
          model,
          language: DEFAULT_LANGUAGE,
        });
        expect(checkIsValid({validationErrors, isScheduleValid: true})).toBe(
          false
        );
      });
    });
  });
});
