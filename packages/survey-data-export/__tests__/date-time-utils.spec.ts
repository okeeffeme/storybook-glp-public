import moment from 'moment';
import {
  buildISOStringFromDateAndTime,
  buildTimeFromText,
} from '../src/utils/date-time';

describe('Date and time utils :: ', () => {
  describe('buildISOStringFromDateAndTime', () => {
    it('should return date and time combined in ISO format', () => {
      const date = moment('2019-01-28');
      const time = {
        hour: '13',
        minute: '11',
      };

      const text = buildISOStringFromDateAndTime(date, time);
      expect(text).toBe('2019-01-28T13:11:00.000+01:00');
    });
  });

  describe('buildTimeFromText', () => {
    it('should parse text which has colon separator', () => {
      const time = buildTimeFromText('9:35');

      expect(time).toEqual({
        hour: '9',
        minute: '35',
        text: '9:35',
      });
    });

    it('should parse text with dot separator', () => {
      const time = buildTimeFromText('8.40');

      expect(time).toEqual({
        hour: '8',
        minute: '40',
        text: '8.40',
      });
    });

    it('should not parse time when empty input', () => {
      const time = buildTimeFromText('');
      expect(time).toEqual({
        text: '',
        minute: undefined,
        hour: undefined,
      });
    });

    it('should not parse time for input without separator', () => {
      const time = buildTimeFromText('15 55');
      expect(time).toEqual({
        text: '15 55',
        minute: undefined,
        hour: undefined,
      });
    });
  });
});
