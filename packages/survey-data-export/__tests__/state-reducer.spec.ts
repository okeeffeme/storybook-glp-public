import deepFreeze from '../test-utils/deepFreeze';
import reducer, {getInitialState} from '../src/state/reducer';
import {
  fileSettingChanged,
  fileTypeChanged,
  transferSettingChanged,
  rootSettingChanged,
  initialSettingsCreated,
  templateSettingChanged,
  dateFilterChanged,
  mediaSettingChanged,
  schedulerValidationChanged,
  formattingSettingChanged,
  responseStatusChanged,
  schedulerSettingsChanged,
  templateSelected,
} from '../src/state/action-creators';

import {
  FILE_TYPE,
  INTERVIEW_STATUS,
  RESPONSE_STATUS_NO_FILTER,
  DATE_FILTER_TYPE,
  FILE_ENCODING,
  ANSWER_ELEMENT_LABELS,
  MEDIA_FILE_NAMING,
  FILE_FORMAT_TYPE,
  TRANSFER_TYPE,
  DEFAULT_RESPONSE_STATUSES,
  ENGLISH_LANGUAGE_ID,
  TEXT_IN_LABELS,
  NO_TEMPLATE_SELECTED_VALUE,
  SCHEDULE_TYPE,
} from '../src/utils/constants';

const language = 'en';

const defaultSettings = {
  databases: [],
  defaultFileType: FILE_TYPE.Excel,
  defaultResponseStatuses: DEFAULT_RESPONSE_STATUSES,
  defaultExportLanguageId: ENGLISH_LANGUAGE_ID,
  defaultEmailRecipient: '',
  defaultTransferType: TRANSFER_TYPE.Download,
  defaultScheduleType: SCHEDULE_TYPE.Asap,
};

describe('State Update Utils :: ', () => {
  it('should validate model when initial settings are created', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.fileSettings.truncateOpenEndsEnabled = true;
    oldState.model.fileSettings.truncateOpenEnds = '2foo00';

    expect(oldState.isValid).toBe(true);

    const action = initialSettingsCreated({
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.isValid).toBe(false);
  });

  it('should update dateFilterSettings when dateFilterType changed', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.dateFilterSettings.dateFilterType = DATE_FILTER_TYPE.None;

    const action = dateFilterChanged({
      propertyName: 'dateFilterType',
      value: DATE_FILTER_TYPE.Range,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.dateFilterSettings.dateFilterType).toBe(
      DATE_FILTER_TYPE.Range
    );
    expect(newState.model.dateFilterSettings.startTime).toBeTruthy();
  });

  it('should validate dateFilterSettings when startTime has invalid time', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.dateFilterSettings.dateFilterType = DATE_FILTER_TYPE.Range;

    expect(oldState.isValid).toBe(true);

    const action = dateFilterChanged({
      propertyName: 'startTime',
      value: 'invalid time',
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.dateFilterSettings.startTime).toBe('invalid time');
    expect(newState.isValid).toBe(false);
  });

  it('should update root settings when answerElementLabels changed', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.answerElementLabels =
      ANSWER_ELEMENT_LABELS.AnswerQuestionLabel;

    const action = rootSettingChanged({
      propertyName: 'answerElementLabels',
      value: ANSWER_ELEMENT_LABELS.Answer,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.answerElementLabels).toBe(
      ANSWER_ELEMENT_LABELS.Answer
    );
    expect(newState.model.exportLanguageId).toBeTruthy();
  });

  it('should update fileSettings when schemaEncodingName changed', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.fileSettings.schemaEncodingName = FILE_ENCODING.UTF8;

    const action = fileSettingChanged({
      propertyName: 'schemaEncodingName',
      value: FILE_ENCODING.ANSI,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.fileSettings.schemaEncodingName).toBe(
      FILE_ENCODING.ANSI
    );
    expect(newState.model.fileSettings.filesToInclude).toBeTruthy();
  });

  it('should update mediaFiles when mediaFileNaming changed', () => {
    const oldState = getInitialState(defaultSettings);
    oldState.model.fileSettings.mediaFiles.mediaFileNaming =
      MEDIA_FILE_NAMING.Detailed;

    const action = mediaSettingChanged({
      propertyName: 'mediaFileNaming',
      value: MEDIA_FILE_NAMING.Unique,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.fileSettings.mediaFiles.mediaFileNaming).toBe(
      MEDIA_FILE_NAMING.Unique
    );
    expect(newState.model.fileSettings.mediaFiles.includeMedia).toBe(false);
  });

  it('should update formatting when fileFormatType changed', () => {
    const oldState = getInitialState(defaultSettings);
    expect(oldState.model.fileSettings.formatting.fileFormatType).toBe(
      FILE_FORMAT_TYPE.TabSeparated
    );

    const action = formattingSettingChanged({
      propertyName: 'fileFormatType',
      value: FILE_FORMAT_TYPE.CommaSeparated,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.model.fileSettings.formatting.fileFormatType).toBe(
      FILE_FORMAT_TYPE.CommaSeparated
    );
    expect(newState.model.fileSettings.formatting.quotedValues).toBe(true);
  });

  describe('templateSettings', () => {
    it('should update templateSettings when textInLabels changed', () => {
      const oldState = getInitialState(defaultSettings);
      expect(oldState.model.templateSettings?.textInLabels).toBe('Template');

      const action = templateSettingChanged({
        propertyName: 'textInLabels',
        value: TEXT_IN_LABELS.Survey,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.templateSettings.textInLabels).toBe(
        TEXT_IN_LABELS.Survey
      );
    });

    it('should update templateSettings when template selected', () => {
      const oldState = getInitialState(defaultSettings);
      expect(oldState.model.templateSettings?.id).toBe(
        NO_TEMPLATE_SELECTED_VALUE
      );

      const action = templateSelected({
        id: '5',
        templateType: 'DataTemplate',
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.templateSettings.id).toBe('5');
      expect(newState.model.templateSettings.templateType).toBe('DataTemplate');
    });
  });

  describe('responseStatusesFilter', () => {
    it('should add Screened status when selected', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.responseStatusesFilter = [INTERVIEW_STATUS.QuotaFull];

      const action = responseStatusChanged({
        propertyName: INTERVIEW_STATUS.Screened,
        value: true,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.responseStatusesFilter).toEqual([
        INTERVIEW_STATUS.QuotaFull,
        INTERVIEW_STATUS.Screened,
      ]);
    });

    it('should removed Screened status when deselected', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.responseStatusesFilter = [
        INTERVIEW_STATUS.QuotaFull,
        INTERVIEW_STATUS.Screened,
      ];

      const action = responseStatusChanged({
        propertyName: INTERVIEW_STATUS.Screened,
        value: false,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.responseStatusesFilter).toEqual([
        INTERVIEW_STATUS.QuotaFull,
      ]);
    });

    it('should remove status filter when RESPONSE_STATUS_NO_FILTER is selected', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.responseStatusesFilter = [
        INTERVIEW_STATUS.QuotaFull,
        INTERVIEW_STATUS.Screened,
      ];

      const action = responseStatusChanged({
        propertyName: RESPONSE_STATUS_NO_FILTER,
        value: [],
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.responseStatusesFilter).toEqual([]);
    });
  });

  describe('fileType', () => {
    it('should set fileType to SAS when selected', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.fileSettings.fileType = FILE_TYPE.Excel;

      const action = fileTypeChanged({value: FILE_TYPE.SAS, language});

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.fileSettings.fileType).toBe(FILE_TYPE.SAS);
    });

    it('should set fileType and enforce truncateOpenEndsEnabled when fileType is TripleS', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.fileSettings.fileType = FILE_TYPE.Excel;
      oldState.model.fileSettings.truncateOpenEndsEnabled = false;

      const action = fileTypeChanged({
        value: FILE_TYPE.TripleS,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.fileSettings.fileType).toBe(FILE_TYPE.TripleS);
      expect(newState.model.fileSettings.truncateOpenEndsEnabled).toBe(true);
    });

    it('should set fileType and not enforce truncateOpenEndsEnabled when fileType is DelimitedTextFile', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.fileSettings.truncateOpenEndsEnabled = false;

      const action = fileTypeChanged({
        value: FILE_TYPE.DelimitedTextFile,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.fileSettings.fileType).toBe(
        FILE_TYPE.DelimitedTextFile
      );
      expect(newState.model.fileSettings.truncateOpenEndsEnabled).toBe(false);
    });
  });

  describe('transferSettings', () => {
    it('should update transferSettings when overrideFileName changed', () => {
      const oldState = getInitialState(defaultSettings);
      expect(oldState.model.transferSettings.overrideFileName).toBe(
        '^PROJECTID^_^USERID^_^TASKID^'
      );

      const action = transferSettingChanged({
        propertyName: 'overrideFileName',
        value: 'data-export-file',
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.overrideFileName).toBe(
        'data-export-file'
      );
    });

    it('should not disable overrideFileNameEnabled when changing transfer type to Download', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.transferType = TRANSFER_TYPE.FTP;
      oldState.model.transferSettings.overrideFileNameEnabled = true;

      const action = transferSettingChanged({
        propertyName: 'transferType',
        value: TRANSFER_TYPE.Download,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.transferType).toBe(
        TRANSFER_TYPE.Download
      );
      expect(newState.model.transferSettings.overrideFileNameEnabled).toBe(
        true
      );
    });

    it('should not disable overrideFileNameEnabled when changing transfer type to Email', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.transferType = TRANSFER_TYPE.FTP;
      oldState.model.transferSettings.overrideFileNameEnabled = true;

      const action = transferSettingChanged({
        propertyName: 'transferType',
        value: TRANSFER_TYPE.Email,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.transferType).toBe(
        TRANSFER_TYPE.Email
      );
      expect(newState.model.transferSettings.overrideFileNameEnabled).toBe(
        true
      );
    });

    it('should change default overrideFileName when changing transfer type if overrideFileName is not edited', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.overrideFileNameEnabled = true;

      expect(oldState.model.transferSettings.overrideFileName).toBe(
        '^PROJECTID^_^USERID^_^TASKID^'
      );

      const action = transferSettingChanged({
        propertyName: 'transferType',
        value: TRANSFER_TYPE.Email,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.overrideFileName).toBe(
        '^PROJECTID^_^USERID^_^START^_^END^_^TASKID^'
      );
    });

    it('should not change default overrideFileName when changing transfer type if overrideFileName is edited', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.overrideFileNameEnabled = true;

      expect(oldState.model.transferSettings.overrideFileName).toBe(
        '^PROJECTID^_^USERID^_^TASKID^'
      );

      const action = transferSettingChanged({
        propertyName: 'overrideFileName',
        value: 'new file name',
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.overrideFileName).toBe(
        'new file name'
      );
    });

    it('should change execution type from Future to asap when changing transfer type to Download', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.transferType = TRANSFER_TYPE.FTP;
      oldState.model.transferSettings.executionSettings.type =
        SCHEDULE_TYPE.Future;

      const action = transferSettingChanged({
        propertyName: 'transferType',
        value: TRANSFER_TYPE.Download,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.transferType).toBe(
        TRANSFER_TYPE.Download
      );
      expect(newState.model.transferSettings.executionSettings.type).toBe(
        SCHEDULE_TYPE.Asap
      );
    });

    it('should not change execution type when changing transfer type to Email', () => {
      const oldState = getInitialState(defaultSettings);
      oldState.model.transferSettings.transferType = TRANSFER_TYPE.FTP;
      oldState.model.transferSettings.executionSettings.type =
        SCHEDULE_TYPE.Future;

      const action = transferSettingChanged({
        propertyName: 'transferType',
        value: TRANSFER_TYPE.Email,
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(newState.model.transferSettings.transferType).toBe(
        TRANSFER_TYPE.Email
      );
      expect(newState.model.transferSettings.executionSettings.type).toBe(
        SCHEDULE_TYPE.Future
      );
    });
  });

  describe('executionSettings', () => {
    it('should update transferSettings when scheduling is changed', () => {
      const oldState = getInitialState(defaultSettings);
      expect(
        oldState.model.transferSettings.executionSettings.recurrenceMode
      ).toBe(undefined);
      expect(
        oldState.model.transferSettings.executionSettings.intervalMonth
      ).toBe(undefined);

      const action = schedulerSettingsChanged({
        settings: {
          recurrenceMode: 'Monthly',
          intervalMonth: 1,
        },
        language,
      });

      deepFreeze(oldState);
      deepFreeze(action);

      const newState = reducer(oldState, action);

      expect(
        newState.model.transferSettings.executionSettings.recurrenceMode
      ).toBe('Monthly');
      expect(
        newState.model.transferSettings.executionSettings.intervalMonth
      ).toBe(1);
    });
  });

  it('should set validation to false when scheduler validation fails', () => {
    const oldState = getInitialState(defaultSettings);

    expect(oldState.isValid).toBe(true);
    expect(oldState.isScheduleValid).toBe(true);

    const action = schedulerValidationChanged({
      isValid: false,
      language,
    });

    deepFreeze(oldState);
    deepFreeze(action);

    const newState = reducer(oldState, action);

    expect(newState.isValid).toBe(false);
    expect(newState.isScheduleValid).toBe(false);
  });
});
