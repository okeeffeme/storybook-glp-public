import {
  isTemplateSelected,
  createTemplateValue,
  getIdAndTypeFromTemplateValue,
  getTemplateList,
} from '../src/utils';
import {NO_TEMPLATE_SELECTED_VALUE} from '../src/utils/constants';

describe('Utils', () => {
  it('isTemplateSelected should return correct value', () => {
    expect(isTemplateSelected(1)).toBe(true);
    expect(isTemplateSelected('1')).toBe(true);
    expect(isTemplateSelected(NO_TEMPLATE_SELECTED_VALUE)).toBe(false);
  });

  it('createTemplateValue should return correct value', () => {
    expect(createTemplateValue({id: 1, templateType: 'ClassicTemplate'})).toBe(
      '1___ClassicTemplate'
    );
  });

  it('getIdAndTypeFromTemplateValue should return id and template type', () => {
    expect(getIdAndTypeFromTemplateValue('1___ClassicTemplate')).toEqual({
      id: '1',
      templateType: 'ClassicTemplate',
    });
  });

  it('getTemplateList should return No data entry when templates is empty', () => {
    expect(
      getTemplateList({
        templates: [],
        language: '9',
      })
    ).toEqual([{label: 'None (all data)', value: '-999999___'}]);
  });

  it('getTemplateList should return list of templates', () => {
    expect(
      getTemplateList({
        templates: [
          {id: 1, type: 'ClassicTemplate', name: 'Hello world'},
          {id: 2, type: 'ClassicTemplate', name: 'My template'},
        ],
        language: '9',
      })
    ).toEqual([
      {label: 'None (all data)', value: '-999999___'},
      {label: 'Hello world', value: '1___ClassicTemplate'},
      {label: 'My template', value: '2___ClassicTemplate'},
    ]);
  });
});
