import React from 'react';
import moment from 'moment';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {text, number, object, select, boolean} from '@storybook/addon-knobs';
import Scheduler from '../../scheduler/src/index';

import {
  DATABASE_TYPE,
  FILE_TYPE,
  INTERVIEW_STATUS,
  TRANSFER_TYPE,
} from '../src/index';

import SurveyDataExport from '../src/components/survey-data-export';
import SurveyDataExportDialog from './survey-data-export-dialog';

import {
  ENGLISH_LANGUAGE_ID,
  ENGLISH_UNITED_STATES_LOCALE,
  SCHEDULE_TYPE,
} from '../src/utils/constants';

import Button from '../../button/src';

/* eslint-disable no-console */
const logToConsole = (settings) => console.log(settings);

/* eslint-disable-next-line */
const Wrapper = ({children}) => <div style={{margin: 20}}>{children}</div>;

storiesOf('Components/survey-data-export', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('With Knobs', () => {
    const fileTypes = [
      FILE_TYPE.Excel,
      FILE_TYPE.ExcelWithLabels,
      FILE_TYPE.DelimitedTextFile,
      FILE_TYPE.DelimitedTxtWithLabels,
      FILE_TYPE.TripleS,
      FILE_TYPE.TripleSConfirmit,
      FILE_TYPE.SPSS,
      FILE_TYPE.SPSSSav,
      FILE_TYPE.Quantum,
      FILE_TYPE.SAS,
      FILE_TYPE.FixedWidthFile,
    ];

    return (
      <Wrapper>
        <SurveyDataExport
          defaultTransferType={TRANSFER_TYPE.FTP}
          collapseAdvancedOptions={false}
          surveyId={text('surveyId', 'p123')}
          surveyName={text('surveyName', 'ACME Survey')}
          onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
          onValidationChanged={(validation) => logToConsole(validation)}
          fileTypes={object('fileTypes', fileTypes)}
          defaultEmailRecipient={'userA@forsta.com;userB@forsta.com'}
          dialogDescription={text(
            'dialogDescription',
            'This is dialog description'
          )}
          defaultFileType={select(
            'defaultFileType',
            fileTypes,
            FILE_TYPE.FixedWidthFile
          )}
          defaultExportLanguageId={number('defaultExportLanguageId', 21)}
          localeList={[
            {
              value: ENGLISH_UNITED_STATES_LOCALE,
              label: 'English (United States)',
            },
          ]}
          surveyLanguages={object('surveyLanguages', [
            {value: 9, label: 'English'},
            {value: 21, label: 'Norwegian'},
          ])}
          defaultResponseStatuses={object('defaultResponseStatuses', [
            INTERVIEW_STATUS.Completed,
            INTERVIEW_STATUS.Error,
          ])}
          disableEmailRecipient={boolean('disableEmailRecipient', false)}
          transferTypes={object('transferTypes', [
            TRANSFER_TYPE.Download,
            TRANSFER_TYPE.FTP,
            TRANSFER_TYPE.Email,
            TRANSFER_TYPE.ExternalFTP,
          ])}
          templates={object('templates', [
            {id: 1, type: 'ClassicTemplate', name: 'Hello world'},
            {id: 2, type: 'ClassicTemplate', name: 'My template'},
          ])}
          enforceSftp={boolean('enforceSftp', true)}
          externalFtpErrorMessage={text(
            'externalFtpErrorMessage',
            'Incorrect host name, username or password'
          )}
          displayAllErrors={boolean('displayAllErrors', false)}
        />
      </Wrapper>
    );
  })
  .add('With defaults', () => (
    <SurveyDataExport
      surveyId="p123"
      surveyName="ACME Survey"
      surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
      localeList={[
        {value: ENGLISH_UNITED_STATES_LOCALE, label: 'English (United States)'},
      ]}
      onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
      onValidationChanged={(validation) => logToConsole(validation)}
    />
  ))
  .add('Expanded, 6 file types, status filter and languages', () => (
    <SurveyDataExport
      collapseAdvancedOptions={false}
      surveyId="p123"
      surveyName="ACME Annual Survey"
      fileTypes={[
        FILE_TYPE.Excel,
        FILE_TYPE.TripleSConfirmit,
        FILE_TYPE.DelimitedTextFile,
        FILE_TYPE.SPSS,
        FILE_TYPE.Quantum,
        FILE_TYPE.FixedWidthFile,
      ]}
      defaultFileType={FILE_TYPE.FixedWidthFile}
      defaultResponseStatuses={[
        INTERVIEW_STATUS.Completed,
        INTERVIEW_STATUS.Error,
      ]}
      localeList={[
        {value: ENGLISH_UNITED_STATES_LOCALE, label: 'English (United States)'},
      ]}
      defaultExportLanguageId={21}
      surveyLanguages={[
        {value: 9, label: 'English'},
        {value: 21, label: 'Norwegian'},
      ]}
      defaultEmailRecipient={'user123@confirmit.com'}
      extraFilters={<b>Inject your condition builder here</b>}
      onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
      onValidationChanged={(validation) => logToConsole(validation)}
    />
  ))
  .add('In dialog', () => {
    const [isOpen, setIsOpen] = React.useState(false);
    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <SurveyDataExportDialog
          isOpen={isOpen}
          surveyId="p123"
          surveyName="ACME Survey"
          surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
          localeList={[
            {
              value: ENGLISH_UNITED_STATES_LOCALE,
              label: 'English (United States)',
            },
          ]}
          defaultEmailRecipient={'user123@confirmit.com'}
          displayScheduler={true}
          onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
          onValidationChanged={(validation) => logToConsole(validation)}
          onConfirm={(settings) => {
            setIsOpen(false);
            logToConsole('export confirmed');
            logToConsole(settings);
          }}
          onCancel={() => {
            setIsOpen(false);
            logToConsole('export canceled');
          }}
        />
      </div>
    );
  })
  .add('Norwegian translation', () => (
    <SurveyDataExport
      surveyId="p123"
      surveyName="ACME Survey"
      collapseAdvancedOptions={false}
      surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
      localeList={[
        {value: ENGLISH_UNITED_STATES_LOCALE, label: 'English (United States)'},
      ]}
      language={select('language', ['nb', 'en'], 'nb')}
      defaultEmailRecipient={'user123@confirmit.com'}
      defaultTransferType={TRANSFER_TYPE.FTP}
      displayScheduler={true}
      defaultScheduleType={SCHEDULE_TYPE.Future}
      onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
      onValidationChanged={(validation) => logToConsole(validation)}
    />
  ))
  .add('Scheduled in the future', () => (
    <SurveyDataExport
      surveyId="p123"
      surveyName="ACME Survey"
      surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
      localeList={[
        {value: ENGLISH_UNITED_STATES_LOCALE, label: 'English (United States)'},
      ]}
      defaultEmailRecipient={'user123@confirmit.com'}
      defaultTransferType={TRANSFER_TYPE.Email}
      defaultScheduleType={SCHEDULE_TYPE.Future}
      defaultSchedule={{
        startDateTime: moment().add(3, 'h').toISOString(true),
        endDateTime: moment().add(6, 'h').toISOString(true),
        recurrenceMode: Scheduler.recurrenceModes.Hourly,
        intervalHour: 5,
      }}
      displayScheduler={true}
      databases={[DATABASE_TYPE.Production, DATABASE_TYPE.Test]}
      onSettingsChanged={(exportSettings) => logToConsole(exportSettings)}
      onValidationChanged={(validation) => logToConsole(validation)}
    />
  ));
