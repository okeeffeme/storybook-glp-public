import React, {useState, FC} from 'react';
import moment from 'moment/moment';
import type {ContractData} from '../src/utils/model-provider';
import {
  ENGLISH_LANGUAGE_ID,
  FILE_TYPE,
  SCHEDULE_TYPE,
  TRANSFER_TYPE,
} from '../src/utils/constants';
import {getContractData, getModelDefaults} from '../src/utils/model-provider';

import {Dialog} from '../../dialog/src';
import SurveyDataExport from '../src/components/survey-data-export';
import Button, {ButtonRow} from '../../button/src';

interface SurveyDataExportDialogProps {
  onConfirm: (state: ContractData) => void;
  onCancel: () => void;
  onSettingsChanged: (state: ContractData) => void;
  onValidationChanged: () => void;
  isOpen: boolean;
}

const SurveyDataExportDialog: FC<SurveyDataExportDialogProps> = ({
  onCancel,
  onSettingsChanged,
  onConfirm,
  isOpen,
  ...rest
}) => {
  const model = getModelDefaults({
    databases: [],
    defaultScheduleType: SCHEDULE_TYPE.Asap,
    defaultFileType: FILE_TYPE.DelimitedTextFile,
    defaultTransferType: TRANSFER_TYPE.Download,
    defaultResponseStatuses: [],
    defaultExportLanguageId: 25,
    defaultEmailRecipient: 'user123@confirmit.com',
    startDate: moment('2019-01-24T00:00:00.000Z'),
  });

  const contactData = getContractData(model);

  const [isValid, setIsValid] = useState(true);
  const [settings] = useState<ContractData>(contactData);

  const handleSettingsChanged = (settings) => onSettingsChanged(settings);

  const handleExportClick = () => {
    onConfirm && onConfirm(settings);
  };

  return (
    <Dialog open={isOpen} size={'large'}>
      <Dialog.Header title="Export Survey Data" onCloseButtonClick={onCancel} />
      <Dialog.Body>
        <SurveyDataExport
          {...rest}
          surveyId="p123"
          surveyName="ACME Survey"
          surveyLanguages={[{value: ENGLISH_LANGUAGE_ID, label: 'English'}]}
          localeList={[{value: 1033, label: 'English (United States)'}]}
          onInitialSettingsCreated={handleSettingsChanged}
          onSettingsChanged={handleSettingsChanged}
          onValidationChanged={({isValid}) => setIsValid(isValid)}
        />
      </Dialog.Body>
      <Dialog.Footer>
        <ButtonRow>
          <Button onClick={onCancel}>Cancel</Button>
          <Button
            appearance={Button.appearances.primarySuccess}
            disabled={!isValid}
            onClick={handleExportClick}>
            Export
          </Button>
        </ButtonRow>
      </Dialog.Footer>
    </Dialog>
  );
};

export default SurveyDataExportDialog;
