# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.1.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.1.1&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

# [9.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.10&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.1.0&targetRepoId=1246) (2023-04-11)

### Features

- made override file name dynamic depending on transfer type ([AUT-5865](https://jiraosl.firmglobal.com/browse/AUT-5865)) ([8147cdf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8147cdf88eedfcefe259716242f634448d40276d))

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.10&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.10&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.8&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.9&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.7&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.8&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.6&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.7&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.5&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.6&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.5&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.3&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.4&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.2&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.3&targetRepoId=1246) (2023-03-10)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.2&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.1&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.9&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@9.0.0&targetRepoId=1246) (2023-02-28)

### chore

- adjusted file-name keywords for download type ([AUT-5865](https://jiraosl.firmglobal.com/browse/AUT-5865)) ([a30c017](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a30c017143b5a21a605325a7c425c452321dcf3a))

### Features

- enabled overrideFileName for all types ([AUT-5865](https://jiraosl.firmglobal.com/browse/AUT-5865)) ([569815a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/569815aaf848e53b80ab731ba8ef0d83ffc00b80))

### BREAKING CHANGES

- "Override auto-generated export file name" is now enabled for every transfer type

## [8.5.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.8&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.9&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.7&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.8&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.6&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.7&targetRepoId=1246) (2023-02-21)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.6&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.5&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.3&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.4&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.3&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.2&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.1&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

# [8.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.14&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.5.0&targetRepoId=1246) (2023-02-09)

### Features

- convert survey-data-export package to TypeScript ([NPM-1237](https://jiraosl.firmglobal.com/browse/NPM-1237)) ([2f8456d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2f8456d563cf9921cfa037f10cc4b96062bd5102))

## [8.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.13&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.14&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.12&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.13&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.11&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.12&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.10&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.11&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.9&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.10&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.8&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.9&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.7&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.8&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.6&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.7&targetRepoId=1246) (2023-01-27)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.5&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.6&targetRepoId=1246) (2023-01-25)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.5&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.3&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.4&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.3&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.2&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.1&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- add test id for survey-data-exports' components ([NPM-1216](https://jiraosl.firmglobal.com/browse/NPM-1216)) ([945112d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/945112d60dce32dc33a2903c5e66e0fdf091c33b))

# [8.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.18&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.4.0&targetRepoId=1246) (2023-01-18)

### Bug Fixes

- remove prop type dependency on WEEK_DAYS ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([c3b69ea](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c3b69ea2960d0398652d9647512ab66b3be21703))

### Features

- expose summary text builder function, expose some types ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([404e333](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/404e33393e9192d18d856716debb8ae631ef592b))

# [8.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.18&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.3.0&targetRepoId=1246) (2023-01-16)

### Bug Fixes

- remove prop type dependency on WEEK_DAYS ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([c3b69ea](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c3b69ea2960d0398652d9647512ab66b3be21703))

### Features

- expose summary text builder function, expose some types ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([404e333](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/404e33393e9192d18d856716debb8ae631ef592b))

## [8.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.16&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.18&targetRepoId=1246) (2023-01-12)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.16&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.17&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.15&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.16&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.14&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.15&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.13&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.14&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.12&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.13&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.11&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.12&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.10&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.11&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.9&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.8&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.7&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.3&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.4&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.2&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.3&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.2&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

# [8.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.1.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.2.0&targetRepoId=1246) (2022-10-17)

### Features

- make subject line and message text required, replace displayOverrideEmailErrors and displayExternalFtpErrors props with displayAllErrors ([NPM-1082](https://jiraosl.firmglobal.com/browse/NPM-1082)) ([ea7ea87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ea7ea876c7eb8b9795986ecb0a37da19cc12a778))

## [8.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.20&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.21&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.19&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.20&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.18&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.19&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.17&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.18&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.16&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.17&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.15&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.16&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.14&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.15&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.13&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.14&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.12&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.11&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.12&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.9&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.10&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.6&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.7&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.5&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.6&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.4&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.5&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.3&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.4&targetRepoId=1246) (2022-07-12)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.2&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.3&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.1&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.2&targetRepoId=1246) (2022-07-04)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.0&sourceBranch=refs/tags/@jotunheim/react-survey-data-export@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-survey-data-export

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.5.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.9&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.10&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.9&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.8&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.6&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.7&targetRepoId=1246) (2022-06-21)

### Bug Fixes

- use areValidEmails from confirmit-utils ([NPM-1015](https://jiraosl.firmglobal.com/browse/NPM-1015)) ([0ac56a4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0ac56a4373ceddc54664499a5e5b104721b49174))

## [7.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.5&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.6&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.4&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.5&targetRepoId=1246) (2022-06-02)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.4&targetRepoId=1246) (2022-06-02)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.3&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.2&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.5.0&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.1&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [7.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.16&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.5.0&targetRepoId=1246) (2022-05-18)

### Features

- allowed multiple emails separated by comma or semicolon ([AUT-6198](https://jiraosl.firmglobal.com/browse/AUT-6198)) ([2fe08f1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2fe08f16cd9dc3632f052246a32a006de45c0c79))

## [7.4.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.15&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.16&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.12&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.13&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.11&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.12&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.10&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.11&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.9&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.10&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.9&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.8&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.3&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.2&targetRepoId=1246) (2022-02-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.4.0&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.1&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [7.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.29&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.4.0&targetRepoId=1246) (2022-02-03)

### Features

- add ability to deliver uncompressed files ([AUT-6069](https://jiraosl.firmglobal.com/browse/AUT-6069)) ([a397723](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a39772363fab67c547b9e2fa0d0185a86b59e284))

## [7.3.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.28&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.29&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.27&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.28&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.26&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.27&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.25&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.26&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.24&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.25&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.23&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.24&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.22&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.23&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.21&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.22&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.20&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.21&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.19&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.20&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.18&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.19&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.17&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.18&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.16&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.17&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.15&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.16&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.3.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.14&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.15&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.12&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.13&targetRepoId=1246) (2021-10-12)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.11&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.12&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.10&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.11&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.9&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.10&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.9&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.8&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.6&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.7&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.5&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.6&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.4&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.5&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.4&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.3&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.2&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.3.0&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.1&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [7.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.26&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.3.0&targetRepoId=1246) (2021-09-06)

### Features

- add "Only include data changed since last run" option for recurring tasks ([AUT-5858](https://jiraosl.firmglobal.com/browse/AUT-5858)) ([96c2be5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/96c2be5acd236382ab4e7955ce7fc145050d0f0c))

## [7.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.25&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.26&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.24&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.25&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.23&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.24&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.22&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.23&targetRepoId=1246) (2021-07-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.21&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.22&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.20&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.21&targetRepoId=1246) (2021-07-23)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.19&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.20&targetRepoId=1246) (2021-07-22)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.18&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.19&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.17&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.18&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.16&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.17&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.15&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.16&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.14&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.15&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.13&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.14&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.12&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.13&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.11&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.12&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.10&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.11&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.9&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.10&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.9&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.8&targetRepoId=1246) (2021-06-02)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.6&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.7&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.5&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.6&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.4&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.5&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.4&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.3&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.2.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.2&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.17&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.2.0&targetRepoId=1246) (2021-05-12)

### Bug Fixes

- incorrect prop type for recurrenceMode ([NPM-767](https://jiraosl.firmglobal.com/browse/NPM-767)) ([e0ffda2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e0ffda28a25dfbfb3b5257513da901268b411aa9))

### Features

- add external ftp settings ([NPM-767](https://jiraosl.firmglobal.com/browse/NPM-767)) ([e5bcc05](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e5bcc057ee859907c92b64dbd16708ca4b52fe5c))

## [7.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.16&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.17&targetRepoId=1246) (2021-05-05)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.15&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.16&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.14&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.15&targetRepoId=1246) (2021-04-29)

### Bug Fixes

- adding some data attributes ([NPM-771](https://jiraosl.firmglobal.com/browse/NPM-771)) ([7479dae](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7479daeeaffad235d93aae03b32a1f088560b6f6))
- snapshots ([NPM-771](https://jiraosl.firmglobal.com/browse/NPM-771)) ([3b9bfe9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3b9bfe90be34ff7c2f4702d7ae17ae79e5f542a1))

## [7.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.13&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.14&targetRepoId=1246) (2021-04-23)

### Bug Fixes

- hide database selector if less than 1 database is available ([NPM-765](https://jiraosl.firmglobal.com/browse/NPM-765)) ([9892c33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9892c332d0e524ab83df138a60e3c483b1894d6f))

## [7.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.12&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.13&targetRepoId=1246) (2021-04-22)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.11&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.12&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.10&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.11&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.9&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.10&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.9&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.8&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.6&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.7&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.5&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.6&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.4&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.5&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.4&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.3&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.2&targetRepoId=1246) (2021-03-22)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.1.0&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.1.1&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## 7.1.0

### Features

- displayScheduler prop - show scheduler for FTP and Email delivery type when enabled (disabled by default)
- databases prop - support for test database (production database is selected when not specified for backward compatibility)

## [7.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.48&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.49&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.47&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.48&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.46&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.47&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.45&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.46&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.44&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.45&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.43&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.44&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.42&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.43&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.41&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.42&targetRepoId=1246) (2021-03-09)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.40&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.41&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.39&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.40&targetRepoId=1246) (2021-03-03)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.38&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.39&targetRepoId=1246) (2021-02-22)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.37&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.38&targetRepoId=1246) (2021-02-19)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.35&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.36&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.34&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.35&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.32&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.33&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.31&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.32&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.30&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.31&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.28&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.29&targetRepoId=1246) (2021-02-10)

### Bug Fixes

- switch order for open ends option ([AUT-5729](https://jiraosl.firmglobal.com/browse/AUT-5729)) ([6194eca](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6194eca831bc3f41793648f1d67fa61f3aa9f280))

## [7.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.27&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.28&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.26&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.27&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.25&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.26&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.24&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.25&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.23&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.24&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.22&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.23&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.20&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.21&targetRepoId=1246) (2020-12-16)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.19&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.20&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.18&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.19&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.17&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.18&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.16&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.17&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.15&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.16&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.14&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.13&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.12&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.13&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.11&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.12&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.8&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.9&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.7&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.8&targetRepoId=1246) (2020-11-25)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.6&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.7&targetRepoId=1246) (2020-11-24)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@7.0.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@6.0.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@6.0.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@7.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@6.0.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@6.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-survey-data-export

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@5.0.3&sourceBranch=refs/tags/@confirmit/react-survey-data-export@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@5.0.2&sourceBranch=refs/tags/@confirmit/react-survey-data-export@5.0.3&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([b12e211](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b12e2116c143df9d123fb8ec4cd1340ed150424d))

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@5.0.1&sourceBranch=refs/tags/@confirmit/react-survey-data-export@5.0.2&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- minor updates after Switch improvements ([NPM-597](https://jiraosl.firmglobal.com/browse/NPM-597)) ([abdd6e5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/abdd6e5887c9bdfa21ba57e55f1f73235630ecf7))

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.63&sourceBranch=refs/tags/@confirmit/react-survey-data-export@5.0.0&targetRepoId=1246) (2020-10-28)

### Features

- Change the default of Survey Data Export to disable truncation of open ends by default. ([NPM-583](https://jiraosl.firmglobal.com/browse/NPM-583)) ([b66fc4c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b66fc4c5eb1998d6f310566189fe5565b6a99d35))

### BREAKING CHANGES

- changes the default of truncation from true to false

## [4.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.62&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.63&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.60&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.61&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.57&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.58&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.56&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.57&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.54&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.55&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.49&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.50&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.48&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.49&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.46&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.47&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-data-export@4.0.44&sourceBranch=refs/tags/@confirmit/react-survey-data-export@4.0.45&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.41](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.40...@confirmit/react-survey-data-export@4.0.41) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.37](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.36...@confirmit/react-survey-data-export@4.0.37) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.35](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.34...@confirmit/react-survey-data-export@4.0.35) (2020-08-27)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.34](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.32...@confirmit/react-survey-data-export@4.0.33) (2020-08-26)

### Bug Fixes

- restrict email fields to 255 characters since there is a limitation on this in backend services ([NPM-506](https://jiraosl.firmglobal.com/browse/NPM-506)) ([52603fd](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/52603fdb1a55d19bed4b9b69599672b93c810142))

## [4.0.31](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.30...@confirmit/react-survey-data-export@4.0.31) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.30](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.29...@confirmit/react-survey-data-export@4.0.30) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.26](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.25...@confirmit/react-survey-data-export@4.0.26) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## [4.0.24](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-survey-data-export@4.0.23...@confirmit/react-survey-data-export@4.0.24) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-survey-data-export

## CHANGELOG

## v4.0.21

- Fix: Email body height was too small
- Fix: Override email options layout was broken
- Fix: Custom delimiter layout was broken

## v4.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-survey-data-export`

### v3.2.0

- Feat: Error text is moved to inside a tooltip

### v3.1.0

- Removing package version in class names.

### v3.0.2

- Fix: minor layout issues after new style for inputfields
- Fix: update import to renamed package `@confirmit/react-switch`

### v3.0.0

- **BREAKING**:
  - Major UI changes to @confirmit/react-text-field

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.0.0

- **BREAKING**
  - default theme is removed, component will always render with material theme
- Refactor: Use @confirmit/react-text-area component instead of custom textarea implementation
- Refactor: Use @confirmit/react-icons package
- Fix: Various layout fixes that have been broken (textfields misaligned, buttontoggles misaligned, etc.)

### v0.7.4

- Fix: Updating textfield for material-theme, using support for label from ButtonToggle

### v0.7.0

- Dialog description is now rendered inside an Alert component

### v0.6.0

- added ability to hide unnecessary filters by passing empty array (fileTypes, templates, transferTypes)
- added props:
  - availableResponseStatuses - list of displayed statuses
  - dialogDescription - description above dialog content
  - displayDateFilter - allows to hide date filter
  - displayAdvancedOptions - allows to hide advanced options

### v0.4.2

- bugfix: fix validation message

### v0.4.1

- bugfix: do not allow values greater than 4000 for "Truncate Open Ends"
- hide "Text in Question Labels", "Text in Answer Element Labels" and "Export Language" when template is seleccted and "Text in Labels" is set to from template

### v0.4.0

- refactoring: use react hooks and performance optimization

### v0.3.1

- use correct label for includeMedia setting

### v0.3.0

- added ability to select templates
- **BREAKING** changed data returned by onSettingsChanged and onInitialSettingsCreated
  - fileSettings.truncateOpenEnds no longer returned when truncateOpenEndsEnabled is disabled
  - transferSettings.emailOverride no longer returned when emailOverrideEnabled is disabled
  - transferSettings.overrideFileName no longer returned when overrideFileNameEnabled is disabled

### v0.2.13

- fix bug related to styling of select component

### v0.2.11

- added ability to override file name for Email and FTP transfer

### v0.2.8

- added defaultTransferType and transferTypes props to control delivery methods (Download, Email, FTP)

### v0.2.7

- bugfix: override email options is only visible for email transfer and not for ftp

### v0.2.2

- added language prop to control preferred language
- added norwegian translation

### v0.2.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v0.1.5

- added extraFilters prop to allow rendering additional filters

### v0.1.4

- added ability to control loop settings: Loop Handling and Position of Loop Variables

### v0.1.2

- added ability to render disabled email recipient field (disableEmailRecipient prop)
- exported TRANSFER_TYPE constant

### v0.1.0

- Added export by email
- Added export by FTP

### v0.0.1

- Initial version
