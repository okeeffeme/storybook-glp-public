# Jotunheim React Survey Data Export

Component to export survey data

## External FTP settings

This feature can be enabled by passing `TRANSFER_TYPE.ExternalFtp` to the `transferTypes`-prop. In addition it has some extra props to control behavior:

`externalFtpErrorMessage` prop is used to display an Alert message with error states from creating the task. This error message does not affect the model validation. If this Alert is not visible in the viewport when it appears, it will be automatically scrolled to. Some examples of error messages:

- folder does not exist
- incorrect username or password
- host not reachable
- etc.

`enforceSftp` prop is used to enforce that `useSftp` is always on and not possible to turn it off. This should be applied from company settings.

`displayAllErrors` prop is used to hide or show validation messages in fields. If field is required, but does not have a default value, it will show validation message immediately. This flag can be used to hide those messages until user has tried to confirm task creation.
