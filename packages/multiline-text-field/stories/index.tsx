import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, number, select, text} from '@storybook/addon-knobs';
import {action} from '@storybook/addon-actions';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import MultilineTextField from '../src/components/multiline-text-field';

storiesOf('Components/multiline-text-field', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [value, setValue] = React.useState('');

    return (
      <div
        style={{padding: '100px', height: '400px', backgroundColor: '#f1f1f1'}}>
        <MultilineTextField
          align={select('align', ['left', 'right'], 'left')}
          autoFocus={boolean('autoFocus', true)}
          disabled={boolean('disabled', false)}
          readOnly={boolean('readOnly', false)}
          error={boolean('error', false)}
          helperText={text('helperText', 'Helptext or error message')}
          label={text('label', 'Label')}
          // Disabled due to performance reasons in storybook
          // https://github.com/storybookjs/storybook/issues/6471
          // onBlur={action('onBlur')}
          // onFocus={action('onFocus')}
          onChange={(newVal) => {
            setValue(newVal);
            action('onChange')(newVal);
          }}
          placeholder={text('placeholder', 'Enter Text Field')}
          required={boolean('required', false)}
          maxLength={number('maxLength', 64)}
          maxLengthErrorText={text('maxLengthErrorText', 'Max length exceeded')}
          showMaxLength={boolean('showMaxLength', false)}
          value={value}
        />
      </div>
    );
  });
