import React, {forwardRef, MutableRefObject, useEffect, useRef} from 'react';
import autosize from 'autosize';

import TextArea, {TextAreaProps} from '@jotunheim/react-textarea';

export const stripNewLinesAndCarriageReturns = (str) =>
  str.replace(/\r?\n/g, '');

const MultilineTextField = forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (
    {
      align,
      autoFocus,
      disabled,
      readOnly,
      error,
      helperText,
      label,
      onBlur,
      onFocus,
      onChange,
      maxLength,
      maxLengthErrorText,
      placeholder,
      required,
      rows = 1,
      showMaxLength,
      value = '',
      ...rest
    },
    ref
  ) => {
    let inputRef = useRef<HTMLTextAreaElement | null>(null);

    if (ref) {
      inputRef = ref as MutableRefObject<HTMLTextAreaElement>;
    }

    useEffect(() => {
      if (inputRef && inputRef.current) {
        autosize(inputRef.current);
      }
    }, []);

    // Handle stripping out new lines and carriage returns in case something gets pasted
    const handleChange = (value) => {
      onChange(stripNewLinesAndCarriageReturns(value));
    };

    // Don't allow user to press enter and create new line
    const handleKeyDown = (event) => {
      if (event.keyCode === 13) {
        event.preventDefault();
      }
    };

    return (
      <TextArea
        align={align}
        autoFocus={autoFocus}
        disabled={disabled}
        readOnly={readOnly}
        data-testid="multiline-text-field"
        error={error}
        data-error={error}
        helperText={helperText}
        label={label}
        maxLength={maxLength}
        maxLengthErrorText={maxLengthErrorText}
        onBlur={onBlur}
        onFocus={onFocus}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        placeholder={placeholder}
        ref={inputRef}
        required={required}
        rows={rows}
        showMaxLength={showMaxLength}
        value={value}
        {...rest}
      />
    );
  }
);

MultilineTextField.displayName = 'MultilineTextField';
MultilineTextField.propTypes = TextArea.propTypes;

export default MultilineTextField;
