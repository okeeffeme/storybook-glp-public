import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import MultilineTextField, {
  stripNewLinesAndCarriageReturns,
} from '../../src/components/multiline-text-field';

describe('Jotunheim React Multiline Text Field :: ', () => {
  it('should add error class', () => {
    // ARRANGE => Render component
    render(
      <MultilineTextField
        error={true}
        label={'label'}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        required={true}
        onChange={jest.fn()}
      />
    );
    expect(screen.getByTestId('multiline-text-field')).toHaveAttribute(
      'data-error',
      'true'
    );
    expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
      'comd-input-wrapper__label--error'
    );
  });

  it('should add disabled class and disabled attribute', () => {
    render(
      <MultilineTextField
        disabled={true}
        label={'label'}
        value={'Some Value'}
        placeholder={'Enter Text Field'}
        isRequired={true}
        onChange={jest.fn()}
      />
    );

    expect(screen.getByText('Some Value')).toBeDisabled();
    expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
      'comd-input-wrapper__label--disabled'
    );
    expect(screen.getByText('Some Value')).toHaveClass(
      'comd-textarea--disabled'
    );
  });

  it('should remove any carriage returns and new lines onChange', () => {
    let value = '';
    const onChange = jest.fn((newValue) => (value += newValue));

    render(
      <MultilineTextField
        id={'story-text-field'}
        name={'story-text-field'}
        label={'Label'}
        value={value}
        placeholder={'Enter Text Field'}
        onChange={(value) => onChange(value)}
      />
    );

    userEvent.click(screen.getByRole('textbox'));
    userEvent.keyboard(`Hello World\r\n, How\n are you?`);

    expect(value).toBe('Hello World, How are you?');
  });

  it('stripNewLinesAndCarriageReturns - should not do anything for strings without carriage returns', () => {
    expect(stripNewLinesAndCarriageReturns('Hello World')).toBe('Hello World');
  });

  it('stripNewLinesAndCarriageReturns - strip out both unix and windows carriage returns', () => {
    expect(stripNewLinesAndCarriageReturns('Hello\r\n World\n')).toBe(
      'Hello World'
    );
  });
});
