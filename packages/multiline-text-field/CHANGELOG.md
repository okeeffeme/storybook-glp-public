# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.16&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.17&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.16&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.16&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.14&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.15&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.13&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.14&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.12&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.13&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.11&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.12&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.10&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.11&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.9&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.10&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.8&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.9&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.6&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.8&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.6&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.7&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.5&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.6&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.4&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.5&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.3&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.4&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- add test id to MultilineLineTextField component ([NPM-1201](https://jiraosl.firmglobal.com/browse/NPM-1201)) ([27b002c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/27b002c5f2f0b34c7342d709860c0bd351aa93e6))

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.2&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.3&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.1&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.2&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.0&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.1&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

# [7.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.20&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.2.0&targetRepoId=1246) (2023-01-31)

### Features

- convert multiline-text-field package to TypeScript ([NPM-1234](https://jiraosl.firmglobal.com/browse/NPM-1234)) ([fc3b082](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fc3b08207e6d0eb0bd372a11d1902459c4c2495d))

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.20&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.1.0&targetRepoId=1246) (2023-01-31)

### Features

- convert multiline-text-field package to TypeScript ([NPM-1234](https://jiraosl.firmglobal.com/browse/NPM-1234)) ([fc3b082](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fc3b08207e6d0eb0bd372a11d1902459c4c2495d))

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.19&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.20&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.18&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.19&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.17&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.18&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.16&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.17&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.14&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.14&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.13&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.12&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.11&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.10&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.9&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.8&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.7&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.4&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.4&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.4&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.3&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.2&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.0&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.17&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@7.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of MultilineTextField ([NPM-945](https://jiraosl.firmglobal.com/browse/NPM-945)) ([65b5cf0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/65b5cf0041fc4ba0a55b657dd4f3bd5cbe01bc00))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.16&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.15&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.14&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.13&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.12&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.11&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.10&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.9&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.7&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.4&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.3&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.2&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.1&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.0&sourceBranch=refs/tags/@jotunheim/react-multiline-text-field@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-multiline-text-field

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.65&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.66&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.64&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.65&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.63&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.64&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.62&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.63&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.61&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.62&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.60&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.61&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.58&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.59&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.57&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.58&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.56&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.57&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.55&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.56&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.54&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.55&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.53&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.54&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.50&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.51&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.49&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.50&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.48&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.49&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.47&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.48&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.46&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.47&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.45&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.46&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.44&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.45&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.43&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.44&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.42&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.43&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.40&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.41&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.39&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.40&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.38&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.39&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.37&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.38&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.36&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.37&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.35&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.36&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.34&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.35&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.33&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.34&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.32&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.33&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.31&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.32&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.30&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.31&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.29&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.30&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.28&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.29&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.27&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.28&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.26&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.27&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.25&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.26&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.24&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.25&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.23&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.24&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.22&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.23&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.21&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.22&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.20&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.21&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.19&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.20&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.18&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.17&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.18&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.15&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.16&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.14&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.15&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.13&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.14&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.12&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.13&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.11&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.12&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.10&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.11&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.9&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.10&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.8&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.9&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.7&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.8&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.6&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.7&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.5&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.3&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.4&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.2&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.3&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.1&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.2&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.0&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.1&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.17&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@5.0.0&targetRepoId=1246) (2021-01-18)

### Features

- Show error message when exceeding max length ([NPM-573](https://jiraosl.firmglobal.com/browse/NPM-573)) ([7a55770](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7a557706063f72f42913288c219b8f3963b513c6))

### BREAKING CHANGES

- If both maxLength and showMaxLength are specified, input will allow the value to exceed the maxLength, and show an error message instead.

## [4.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.16&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.15&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.14&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.13&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.12&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.11&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.10&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.9&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.6&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.3&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.2&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.1&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@3.0.1&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@3.0.1&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@4.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@3.0.1&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@3.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.45&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@3.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- add @confirmit/react-themes as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([bac8574](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bac85740887525b06c80a7cca1acdd8586e850d6))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [2.3.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.44&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.45&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.43&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.44&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.42&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.43&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.41&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.42&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.38&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.39&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.37&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.38&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.36&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.37&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.33&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.34&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.32&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.33&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.30&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.31&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.28&sourceBranch=refs/tags/@confirmit/react-multiline-text-field@2.3.29&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.27](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-multiline-text-field@2.3.26...@confirmit/react-multiline-text-field@2.3.27) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.25](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-multiline-text-field@2.3.24...@confirmit/react-multiline-text-field@2.3.25) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.19](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-multiline-text-field@2.3.18...@confirmit/react-multiline-text-field@2.3.19) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## [2.3.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-multiline-text-field@2.3.17...@confirmit/react-multiline-text-field@2.3.18) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-multiline-text-field

## CHANGELOG

### v2.3.9

- Fix: Inputs should have white background

### v2.3.0

- Refactor: restructure folder names in repository.

### v2.2.0

- Feat: Helper text is moved to inside a tooltip

### v2.1.0

- Removing package version in class names.

### v2.0.0

- **BREAKING**:
  - Use TextArea in combination with `autosize` package to achieve MultiLine functionality instead of a custom implementation.
  - Remove unused `showClear` prop
  - Updated to match DS specs

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.1

- Initial version
