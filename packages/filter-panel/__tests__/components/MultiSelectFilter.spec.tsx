import React from 'react';
import {render, screen} from '@testing-library/react';
import FilterPanel, {FilterType, Language, Query} from '../../src';
import user from '@testing-library/user-event';
const filterId = 'multiSelectFilter';
const filterWithCustomOperatorId = 'filterWithCustomOperatorId';
const TEST_OPTIONS = [
  {label: 'label1', value: 'value1'},
  {label: 'label2', value: 'value2'},
];
const onApply = jest.fn();
const noOp = () => {};
const getCustomOperatorText = () => {
  return 'includes';
};

type RenderProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};
const renderComponent = (props?: RenderProps) => {
  const finalProps = {
    onDeleteAll: noOp,
    onApply,
    onDelete: noOp,
    value: [],
    ...props,
  };
  return render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'filter title',
          type: FilterType.MultiSelect,
          props: {
            options: TEST_OPTIONS,
          },
        },
        {
          id: filterWithCustomOperatorId,
          title: 'filter title',
          getCustomOperatorText,
          type: FilterType.MultiSelect,
          props: {
            options: TEST_OPTIONS,
          },
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('multi-select-filter', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('should render pills when select filter is present', () => {
    renderComponent({
      value: [
        {
          id: filterId,
          label: ['label1', 'custom'],
          operator: 'in',
          value: ['value1', 'value2'],
        },
        {
          id: filterWithCustomOperatorId,
          label: ['label1', 'custom'],
          operator: 'in',
          value: ['value1', 'custom'],
        },
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(Pill[1]).toHaveAttribute('data-pills', filterWithCustomOperatorId);
    expect(Pill[0]).toHaveTextContent('filter title: label1, custom');
    expect(Pill[1]).toHaveTextContent('filter title includes label1, custom');
  });

  it('should open the panel with applied filer when clicked on the pill', () => {
    renderComponent({
      value: [
        {
          id: filterId,
          label: ['label1'],
          operator: 'in',
          value: ['value1'],
        },
      ],
    });

    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(screen.getByTestId('filter-panel')).not.toHaveAttribute(
      'data-active-filter',
      filterId
    );

    user.click(Pill[0]);

    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId
    );
    expect(screen.getAllByText('label1')).not.toHaveLength(0);
  });

  it('should allow the panel to choose multiple options and apply', () => {
    renderComponent({
      value: [
        {
          id: filterId,
          label: ['label1', 'label2', 'custom'],
          operator: 'in',
          value: ['value1', 'value2', 'custom'],
        },
      ],
    });
    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));

    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
      'data-filter-list-id',
      filterId
    );
    user.click(screen.getAllByTestId('filter-list-id')[0]);
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId
    );
    user.click(screen.getByTestId('multi-select-filter'));
    expect(screen.getByTestId('react-select')).toHaveTextContent('label1');
    expect(screen.getByTestId('react-select')).toHaveTextContent('label2');
    expect(screen.getByTestId('react-select')).toHaveTextContent('custom');
    user.click(screen.getAllByTestId('input-chip-delete-button')[0]);
    user.click(screen.getByTestId('filter-footer-apply-button'));
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      label: ['label2', 'custom'],
      operator: 'in',
      value: ['value2', 'custom'],
    });
  });
});
