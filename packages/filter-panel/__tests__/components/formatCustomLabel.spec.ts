import moment from 'moment';
import {formatCustomLabel} from '../../src/date-and-time-filter/formatCustomLabel';
import {Language} from '../../src/';

describe('formatCustomLabel', () => {
  //initial time in UTC, CET +1 hour
  it('should inclides "No Start Date" if no start time', () => {
    expect(
      formatCustomLabel(Language.EN)(
        undefined,
        moment('2020-12-21T13:35:07.99+00:00')
      )
    ).toEqual('In Range No Start Date - 2020-12-21 14:35:07');
  });

  it('should inclides "No End Date" if no end time', () => {
    expect(
      formatCustomLabel(Language.EN)(
        moment('2020-12-21T13:35:07.99+00:00', undefined)
      )
    ).toEqual('In Range 2020-12-21 14:35:07 - No End Date');
  });
});
