import reducer, {initialState} from '../../src/state/reducer';
import * as action from '../../src/state/actions';

describe('Filter panel reducer', () => {
  it('Should handle action setFilterValid', () => {
    const stateValidFilter = reducer(initialState, action.setFilterValid(true));
    expect(stateValidFilter.isFilterValid).toEqual(true);

    const stateInValidFilter = reducer(
      stateValidFilter,
      action.setFilterValid(false)
    );
    expect(stateInValidFilter.isFilterValid).toEqual(false);
  });

  it('Should handle action setFilter', () => {
    const sampleFilterArgs = {header: 'filter 1'};
    const stateWithFilter = reducer(
      initialState,
      action.setFilter(sampleFilterArgs)
    );
    expect(stateWithFilter.header).toEqual(sampleFilterArgs.header);

    const stateWithNoFilter = reducer(stateWithFilter, action.setFilter());
    expect(stateWithNoFilter.header).toEqual(initialState.header);
  });

  it('Should handle action toggleFilterPanel', () => {
    const stateActivePanel = reducer(
      initialState,
      action.toggleFilterPanel(true)
    );
    expect(stateActivePanel.isActive).toEqual(true);

    const stateWithFilter = {
      ...stateActivePanel,
      activeFilterId: 'filter1',
      header: 'filter header',
      query: {id: 'filter1', value: '1'},
    };
    const stateInActivePanel = reducer(
      stateWithFilter,
      action.toggleFilterPanel(false)
    );
    expect(stateInActivePanel.isActive).toEqual(false);
    expect(stateInActivePanel.header).toEqual(initialState.header);
    expect(stateInActivePanel.query).toEqual(initialState.query);
  });

  it('Should handle action updateQuery', () => {
    const sampleQuery = {id: 'filter1', value: 1};
    const stateWithQuery = reducer(
      initialState,
      action.updateQuery(sampleQuery)
    );

    expect(stateWithQuery.query).toEqual(sampleQuery);

    const stateWithQueryUpdated = reducer(
      stateWithQuery,
      action.updateQuery({...sampleQuery, value: 2})
    );
    expect(stateWithQueryUpdated.query).toEqual({...sampleQuery, value: 2});

    const stateWithNoQuery = reducer(stateWithQuery, action.updateQuery());
    expect(stateWithNoQuery.query).toEqual(initialState.query);
  });

  it('Should handle action addOption', () => {
    const sampleOption = {id: 'option', value: 'value'};
    const sampleOption_1 = {id: 'option_1', value: 'value_1'};
    const stateWithOption = reducer(
      initialState,
      action.addOption(sampleOption.id, sampleOption.value)
    );

    expect(stateWithOption.optionsById).toEqual({
      [sampleOption.id]: [sampleOption.value],
    });

    //When updating the same option
    const stateWithOptionUpdated = reducer(
      stateWithOption,
      action.addOption(sampleOption.id, 'value_updated')
    );
    expect(stateWithOptionUpdated.optionsById).toEqual({
      [sampleOption.id]: [sampleOption.value, 'value_updated'],
    });

    //When adding a new option
    const stateWithNewOption = reducer(
      stateWithOption,
      action.addOption(sampleOption_1.id, sampleOption_1.value)
    );
    expect(stateWithNewOption.optionsById).toEqual({
      [sampleOption.id]: [sampleOption.value],
      [sampleOption_1.id]: [sampleOption_1.value],
    });
  });
});
