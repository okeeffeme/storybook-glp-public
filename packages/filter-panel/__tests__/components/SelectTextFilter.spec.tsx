import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import FilterPanel, {
  FilterType,
  Language,
  Query,
  SelectTextSelectors,
} from '../../src';
import user from '@testing-library/user-event';

const filterId = {
  textFilter: 'textFilter',
  compareSelector: 'compareSelector',
  containsSelector: 'containsSelector',
  customOperatorTextFilter: 'customOperatorTextFilter',
  customOperatorTextFilterWithoutGetter:
    'customOperatorTextFilterWithoutGetter',
};

const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();

type RenderProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};

const startsWithOperator = [
  {
    value: 'startsWith',
    label: 'Starts with',
  },
];

const getCustomOperatorText = (operator) => {
  switch (operator) {
    case 'startsWith':
      return 'starts with';
  }
  return ``;
};
const renderComponent = (props?: RenderProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
  };
  return render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId.textFilter,
          title: 'filter title',
          type: FilterType.SelectText,
          props: {
            options: SelectTextSelectors.equalSelector,
          },
        },
        {
          id: filterId.containsSelector,
          title: 'with contains selector',
          type: FilterType.SelectText,
          props: {
            options: SelectTextSelectors.containsSelector,
          },
        },
        {
          id: filterId.compareSelector,
          title: 'with compare selector',
          type: FilterType.SelectText,
          props: {
            options: SelectTextSelectors.compareSelector,
          },
        },
        {
          id: filterId.customOperatorTextFilter,
          title: 'with getter',
          type: FilterType.SelectText,
          props: {
            options: startsWithOperator,
          },
          getCustomOperatorText,
        },
        {
          id: filterId.customOperatorTextFilterWithoutGetter,
          title: 'without getter',
          type: FilterType.SelectText,
          props: {
            options: startsWithOperator,
          },
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('SelectTextFilter', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('should render pills with equals selector when filter is present', () => {
    renderComponent({
      value: [{id: filterId.textFilter, operator: 'eq', value: '1'}],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.textFilter);
    user.click(screen.getByTestId('input-chip-delete-button'));

    expect(onDelete).toHaveBeenCalled();
  });

  it('should render pills with contains selector when filter is present', () => {
    renderComponent({
      value: [
        {id: filterId.containsSelector, operator: 'eq', value: 'someValue'},
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.containsSelector);
  });

  it('should open the filter with applied values, when clicked on pill', () => {
    const value = '1';
    renderComponent({
      value: [
        {
          id: filterId.compareSelector,
          operator: 'gt',
          value,
        },
      ],
    });

    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.compareSelector);
    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );

    user.click(Pill[0]);
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId.compareSelector
    );
    expect(screen.getByTestId('filter-select-value')).toHaveAttribute(
      'data-filter-select-value',
      'gt'
    );
    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      value
    );
  });

  it('should show pill with custom operator when getter is defined', () => {
    renderComponent({
      value: [
        {
          id: filterId.customOperatorTextFilter,
          operator: 'startsWith',
          value: '1',
          isIdle: false,
        },
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute(
      'data-pills',
      filterId.customOperatorTextFilter
    );
    expect(Pill[0]).toHaveTextContent('with getter starts with 1');
  });

  it('should show pill with custom operator when getter is not defined', () => {
    renderComponent({
      value: [
        {
          id: filterId.customOperatorTextFilterWithoutGetter,
          operator: 'startsWith',
          value: '1',
          isIdle: false,
        },
      ],
    });

    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute(
      'data-pills',
      filterId.customOperatorTextFilterWithoutGetter
    );
    expect(Pill[0]).toHaveTextContent('without getter: 1');
  });

  it('should apply the filter with valid filter value', () => {
    renderComponent({
      value: [{id: filterId.textFilter, operator: 'eq', value: '1'}],
    });
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));
    expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
      'data-filter-list-id',
      filterId.textFilter
    );
    expect(screen.queryByTestId('select-text-filter')).not.toBeInTheDocument();
    user.click(screen.getAllByTestId('filter-list-id')[0]);
    expect(screen.getByTestId('select-text-filter')).toHaveAttribute(
      'data-select-text-filter'
    );
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'text to filter'},
    });
    user.click(screen.getByTestId('filter-footer-apply-button'));
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId.textFilter,
      operator: 'eq',
      value: 'text to filter',
    });
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
  });
});
