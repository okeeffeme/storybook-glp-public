import React from 'react';
import {render, screen} from '@testing-library/react';
import FilterPanel, {
  FilterType,
  Language,
  SelectTextSelectors,
} from '../../src';
import {FilterPanelProps} from '../../src/types';
import user from '@testing-library/user-event';

const filterId = {
  textFilter: 'textFilter',
  customOperatorTextFilter: 'customOperatorTextFilter',
  selectFilter: 'selectFilter',
  dateFilter: 'dateFilter',
  dateAndTimeFilter: 'dateAndTimeFilter',
  multiChoiceFilter: 'multiChoiceFilter',
};

const startsWithOperator = [
  {
    value: 'startsWith',
    label: 'Starts with',
  },
];

const getCustomOperatorText = (operator) => {
  switch (operator) {
    case 'startsWith':
      return 'starts with';
  }
  return ``;
};

const TEST_SELECT_OPTIONS = [
  {label: 'label_select1', value: 'value_select1'},
  {label: 'label_select2', value: 'value_select2'},
];
const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();
const onStatusToggle = jest.fn();
const onChangeActiveFilterId = jest.fn();

type RenderProps = Partial<FilterPanelProps>;
const renderComponent = (props?: RenderProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
    filters: [
      {
        id: filterId.textFilter,
        title: 'text-filter',
        type: FilterType.SelectText,
        props: {
          options: SelectTextSelectors.equalSelector,
        },
      },
      {
        id: filterId.selectFilter,
        title: 'select-filter',
        type: FilterType.SingleChoice,
        props: {
          options: TEST_SELECT_OPTIONS,
        },
      },
      {
        id: filterId.dateFilter,
        title: 'date-filter',
        type: FilterType.Date,
      },
      {
        id: filterId.dateAndTimeFilter,
        title: 'date-and-time-filter',
        type: FilterType.DateAndTime,
      },
      {
        id: filterId.multiChoiceFilter,
        title: 'multi-choice-filter',
        type: FilterType.MultiChoice,
        props: {
          options: TEST_SELECT_OPTIONS,
        },
      },
      {
        id: filterId.customOperatorTextFilter,
        title: 'custom-operator-text-filter',
        type: FilterType.SelectText,
        props: {
          options: startsWithOperator,
        },
        getCustomOperatorText,
      },
    ],
  } as FilterPanelProps;
  return render(
    <FilterPanel {...finalProps}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('Filter panel', () => {
  it('should render with panel closed', () => {
    renderComponent();
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
  });

  it('should toggle drawer when clicked on filter icon, and page should get class modifier', () => {
    renderComponent();

    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel-page-classes')).toHaveClass(
      'comd-filter-panel__page comd-filter-panel__page--drawer-open'
    );
    user.click(screen.getByRole('checkbox'));
    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel-page-classes')).not.toHaveClass(
      'comd-filter-panel__page comd-filter-panel__page--drawer-open'
    );
  });

  it('should not show deactivate button when no onStatusToggle', () => {
    renderComponent({
      value: [
        {id: filterId.textFilter, operator: 'eq', value: '1', isIdle: false},
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.textFilter);
    expect(Pill[0]).not.toHaveAttribute('data-test-deactivate');
  });

  it('should show have deactivate button when onStatusToggle callback', () => {
    renderComponent({
      value: [
        {id: filterId.textFilter, operator: 'eq', value: '1', isIdle: false},
        {id: filterId.selectFilter, value: 'value_select1', isIdle: true},
      ],
      onStatusToggle,
    });

    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.textFilter);
    expect(
      screen.getAllByTestId('test-deactivate-fillter-pills')[0]
    ).toHaveAttribute('data-test-deactivate', 'true');
    expect(Pill[1]).toHaveAttribute('data-pills', filterId.selectFilter);
    expect(
      screen.getAllByTestId('test-deactivate-fillter-pills')[1]
    ).toHaveAttribute('data-test-deactivate', 'false');
  });

  it('should have disabled clear all button when no filters are applied', () => {
    renderComponent();

    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));
    user.click(screen.getByTestId('filter-footer-clear-button'));
    expect(screen.getByTestId('filter-footer-clear-button')).toBeDisabled();
    expect(onDeleteAll).not.toHaveBeenCalled();
  });

  it('should have enabled clear all button when filters are applied', () => {
    renderComponent({
      value: [{id: filterId.textFilter, operator: 'eq', value: '1'}],
    });

    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));

    expect(screen.getByTestId('filter-footer-clear-button')).not.toBeDisabled();
    user.click(screen.getByTestId('filter-footer-clear-button'));
    expect(onDeleteAll).toHaveBeenCalled();
  });

  it('should allow to navigate among filters pill click, when multiple filters are applied', () => {
    renderComponent({
      value: [
        {id: filterId.textFilter, operator: 'eq', value: '1'},
        {id: filterId.selectFilter, value: 'value_select1'},
        {id: filterId.dateAndTimeFilter, label: 'Today', value: 'today'},
      ],
      onChangeActiveFilterId,
    });
    const activeFilter = screen.getByTestId('filter-panel');
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.textFilter);
    expect(Pill[1]).toHaveAttribute('data-pills', filterId.selectFilter);
    expect(Pill[2]).toHaveAttribute('data-pills', filterId.dateAndTimeFilter);

    expect(activeFilter).not.toHaveAttribute(
      'data-active-filter',
      filterId.textFilter
    );
    user.click(Pill[0]);
    expect(activeFilter).toHaveAttribute(
      'data-active-filter',
      filterId.textFilter
    );
    expect(onChangeActiveFilterId).toHaveBeenCalledWith(filterId.textFilter);

    user.click(Pill[1]);
    expect(activeFilter).toHaveAttribute(
      'data-active-filter',
      filterId.selectFilter
    );
    expect(onChangeActiveFilterId).toHaveBeenCalledWith(filterId.selectFilter);

    user.click(Pill[2]);
    expect(activeFilter).toHaveAttribute(
      'data-active-filter',
      filterId.dateAndTimeFilter
    );
    expect(onChangeActiveFilterId).toHaveBeenCalledWith(
      filterId.dateAndTimeFilter
    );
  });

  it('should show the applied filters in list when filters are applied', () => {
    renderComponent({
      value: [
        {id: filterId.textFilter, operator: 'eq', value: 'Example 1'},
        {
          id: filterId.customOperatorTextFilter,
          operator: 'startsWith',
          value: 'Example 1',
        }, // with custom operator
        {id: filterId.selectFilter, value: 'value_select1'}, // without label
        {id: filterId.dateAndTimeFilter, label: 'Today', value: 'today'},
        {
          id: filterId.multiChoiceFilter,
          label: ['label_select1', 'label_select2'],
          value: ['value_select1', 'value_select2'],
        },
      ],
    });

    const textFilterText1 = 'Example 1';
    const textFilterText2 = 'starts with Example 1';
    const selectFilterText = 'value_select1';
    const dateAndTimeFilterText = 'Today';
    const multiChoiceFilterText = 'label_select1, label_select2';

    const activeFilter = screen.getAllByTestId('applied-filter-list-text');
    expect(activeFilter[0]).toHaveAttribute(
      'data-applied-filter-list',
      textFilterText1
    );
    expect(activeFilter[1]).toHaveAttribute(
      'data-applied-filter-list',
      selectFilterText
    );
    expect(activeFilter[2]).toHaveAttribute(
      'data-applied-filter-list',
      dateAndTimeFilterText
    );
    expect(activeFilter[3]).toHaveAttribute(
      'data-applied-filter-list',
      multiChoiceFilterText
    );
    expect(activeFilter[4]).toHaveAttribute(
      'data-applied-filter-list',
      textFilterText2
    );
  });

  it('pills should always show : as the display operator, unless it is a SelectTextFilter', () => {
    renderComponent({
      value: [
        {id: filterId.textFilter, operator: 'gt', value: 'Example 1'},
        {
          id: filterId.customOperatorTextFilter,
          operator: 'startsWith',
          value: 'Example 1',
        }, // with custom operator
        {id: filterId.selectFilter, value: 'value_select1'}, // without label
        {
          id: filterId.dateAndTimeFilter,
          label: 'In Range 2021-03-02 00:00:00 - No End Date',
          operator: 'ge',
          value: {
            ge: new Date('2021-03-01T23:00:00.000Z'),
          },
        },
        {
          id: filterId.multiChoiceFilter,
          label: ['label_select1', 'label_select2'],
          value: ['value_select1', 'value_select2'],
        },
      ],
    });

    const textFilterPillText = 'text-filter > Example 1';
    const customOperatorPillText =
      'custom-operator-text-filter starts with Example 1';
    const selectFilterPillText = 'select-filter: value_select1';
    const dateAndTimeFilterPillText =
      'date-and-time-filter: In Range 2021-03-02 00:00:00 - No End Date';
    const multiChoiceFilterPillText =
      'multi-choice-filter: label_select1, label_select2';
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId.textFilter);
    expect(Pill[0]).toHaveTextContent(textFilterPillText);
    expect(Pill[1]).toHaveAttribute(
      'data-pills',
      filterId.customOperatorTextFilter
    );
    expect(Pill[1]).toHaveTextContent(customOperatorPillText);
    expect(Pill[2]).toHaveAttribute('data-pills', filterId.selectFilter);
    expect(Pill[2]).toHaveTextContent(selectFilterPillText);
    expect(Pill[3]).toHaveAttribute('data-pills', filterId.dateAndTimeFilter);
    expect(Pill[3]).toHaveTextContent(dateAndTimeFilterPillText);
    expect(Pill[4]).toHaveAttribute('data-pills', filterId.multiChoiceFilter);
    expect(Pill[4]).toHaveTextContent(multiChoiceFilterPillText);
  });

  describe('translation', () => {
    it('should render filter panel in Norwegian when Language is set to "NB"', () => {
      renderComponent({language: Language.NB});
      expect(screen.getByRole('checkbox')).toHaveAttribute(
        'id',
        'toggle-filter-panel-visibility'
      );
      user.click(screen.getByRole('checkbox'));
      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(screen.getByText('Filtre')).toBeInTheDocument();
      expect(screen.getByText('SLETT ALT')).toBeInTheDocument();
      expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
        'data-filter-list-id',
        filterId.textFilter
      );
      user.click(screen.getAllByTestId('filter-list-id')[0]);
      expect(screen.getByText('TILBAKE')).toBeInTheDocument();
      expect(screen.getByText('BRUK')).toBeInTheDocument();
    });

    it('should render filter panel in English when Language is set to "EN"', () => {
      renderComponent({language: Language.EN});
      expect(screen.getByRole('checkbox')).toHaveAttribute(
        'id',
        'toggle-filter-panel-visibility'
      );
      user.click(screen.getByRole('checkbox'));
      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(screen.getByText('Filters')).toBeInTheDocument();
      expect(screen.getByText('CLEAR ALL')).toBeInTheDocument();
      expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
        'data-filter-list-id',
        filterId.textFilter
      );
      user.click(screen.getAllByTestId('filter-list-id')[0]);
      expect(screen.getByText('BACK')).toBeInTheDocument();
      expect(screen.getByText('APPLY')).toBeInTheDocument();
    });
  });

  describe('default active filter', () => {
    it('should not make panel open and show any filter as active when defaultActiveFilterId is not specified', () => {
      renderComponent({});

      const activeFilter = screen.getByTestId('filter-panel');
      expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute('data-active-filter');
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.textFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateAndTimeFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.multiChoiceFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.customOperatorTextFilter
      );
    });

    it('should make panel open and show default active filter when defaultActiveFilterId is specified', () => {
      renderComponent({
        defaultActiveFilterId: filterId.selectFilter,
      });

      const activeFilter = screen.getByTestId('filter-panel');

      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
    });

    it('should not change active filter when defaultActiveFilterId is changed and nothing else is touched', () => {
      renderComponent({
        defaultActiveFilterId: filterId.selectFilter,
      });

      const activeFilter = screen.getByTestId('filter-panel');

      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );

      renderComponent({defaultActiveFilterId: filterId.dateFilter});

      expect(screen.getAllByTestId('side-panel')[0]).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );
    });

    it('should not make panel open and show any filter as active when defaultActiveFilterId is changed and user closed panel', () => {
      renderComponent({
        defaultActiveFilterId: filterId.selectFilter,
      });

      const activeFilter = screen.getByTestId('filter-panel');

      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );

      expect(screen.getByRole('checkbox')).toHaveAttribute(
        'id',
        'toggle-filter-panel-visibility'
      );
      user.click(screen.getByRole('checkbox'));
      expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );

      renderComponent({
        defaultActiveFilterId: filterId.dateFilter,
      });

      expect(screen.getAllByTestId('side-panel')[0]).not.toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );
    });

    it('should not make panel open and show any filter as active when defaultActiveFilterId is changed and user closed panel', () => {
      renderComponent({
        defaultActiveFilterId: filterId.selectFilter,
      });

      const activeFilter = screen.getByTestId('filter-panel');

      expect(screen.getByTestId('side-panel')).toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );

      expect(screen.getByRole('checkbox')).toHaveAttribute(
        'id',
        'toggle-filter-panel-visibility'
      );
      user.click(screen.getByRole('checkbox'));

      renderComponent({
        defaultActiveFilterId: filterId.dateFilter,
      });

      expect(screen.getAllByTestId('side-panel')[0]).not.toHaveAttribute(
        'data-panel-drawer',
        'opened'
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.selectFilter
      );
      expect(activeFilter).not.toHaveAttribute(
        'data-active-filter',
        filterId.dateFilter
      );
    });
  });
});
