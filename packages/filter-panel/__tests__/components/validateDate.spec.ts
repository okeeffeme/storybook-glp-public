import moment from 'moment';
import {DateValidState} from '../../src/date-and-time-filter/constants';
import {validateDate} from '../../src/date-and-time-filter/validateDate';

describe('validateDate', () => {
  it('Should return not field if no date pass', () => {
    expect(validateDate(undefined)).toEqual(DateValidState.NOT_FILLED);
  });

  it('Should return valid if date pass the validation', () => {
    const date = moment('2020-12-21T00:00:00.00');
    expect(validateDate(date)).toEqual(DateValidState.VALID);
  });

  it('Should return invalid if date does not pass the validation', () => {
    const date = moment('2020-1-21T00:00:00.00');
    expect(validateDate(date)).toEqual(DateValidState.INVALID);
  });
});
