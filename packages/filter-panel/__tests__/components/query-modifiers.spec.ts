import {getParamsFromQuery} from '../../src/utils/query-modifiers';

describe('getParamsFromQuery utils', () => {
  const filters = [
    {id: 'key1', operator: 'eq', value: '1'},
    {id: 'key2', operator: 'gt', value: '2'},
    {id: 'key3', operator: 'lt', value: '3'},
  ];
  it('should return query from the filter list when id matches ', function () {
    const filter = getParamsFromQuery(filters, 'key1');
    expect(filter).toEqual({id: 'key1', operator: 'eq', value: '1'});
  });

  it('should return empty object when id do not matches', function () {
    const filter = getParamsFromQuery(filters, 'key0');
    expect(filter).toEqual(undefined);
  });
});
