import React from 'react';
import {mount, ReactWrapper} from 'enzyme';
import FilterPanel, {FilterType, Language, Query} from '../../src';

const filterId = 'dateAndTimeFilter';
const filterWithCustomOperatorId = 'filterWithCustomOperatorId';
const filter = {
  value: [
    {
      id: filterId,
      label: 'Today',
      value: 'today',
    },
  ],
};

const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();
const getCustomOperatorText = () => {
  return '=';
};
type MountProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};

const isPanelOpen = (component: ReactWrapper) =>
  component.find('SidePanel').prop('data-panel-drawer') === 'opened';
const Pill = (component: ReactWrapper, id?: string) =>
  component.find(`div[data-pills="${id || filterId}"]`);

const mountComponent = (props?: MountProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
  };

  return mount(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'date-and-time-filter',
          type: FilterType.DateAndTime,
        },
        {
          id: filterWithCustomOperatorId,
          title: 'date-and-time-filter',
          getCustomOperatorText,
          type: FilterType.DateAndTime,
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('date-and-time-filter', () => {
  let dateNowSpy;
  const timeStamp = Date.parse('2020-01-01T08:00:00Z');
  beforeAll(() => {
    jest.clearAllMocks();
    // Mock date used by moment
    dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => timeStamp);
  });

  afterAll(() => {
    // Unlock Time
    dateNowSpy.mockRestore();
  });

  it('should render pills when date filter is present', () => {
    const component = mountComponent({
      value: [
        {id: filterId, label: 'Today', value: 'today'},
        {id: filterWithCustomOperatorId, label: 'Today', value: 'today'},
      ],
    });

    expect(isPanelOpen(component)).toEqual(false);
    expect(Pill(component).exists()).toEqual(true);
    expect(Pill(component).text()).toEqual('date-and-time-filter: Today');
    expect(Pill(component, filterWithCustomOperatorId).text()).toEqual(
      'date-and-time-filter = Today'
    );

    Pill(component)
      .find('button[data-input-chip-delete-button]')
      .simulate('click');
    expect(onDelete).toHaveBeenCalled();
  });

  it('should open the filter in the panel when clicked on the filter pill', () => {
    const component = mountComponent(filter);

    expect(Pill(component).exists()).toEqual(true);
    expect(isPanelOpen(component)).toEqual(false);

    Pill(component).simulate('click');

    expect(isPanelOpen(component)).toEqual(true);
    expect(
      component.find(`[data-active-filter="${filterId}"]`).exists()
    ).toEqual(true);
    expect(component.find(`input#today`).props().checked).toEqual(true);
  });

  it('should be able to apply quick filter', () => {
    const component = mountComponent(filter);
    component
      .find('#toggle-filter-panel-visibility')
      .hostNodes()
      .simulate('change', {
        currentTarget: {
          checked: true,
        },
      });

    expect(
      component.find(`div[data-filter-list-id="${filterId}"]`).exists()
    ).toEqual(true);
    expect(component.find('[data-date-and-time-filter]').exists()).toEqual(
      false
    );
    component.find(`div[data-filter-list-id="${filterId}"]`).simulate('click');
    expect(component.find('[data-date-and-time-filter]').exists()).toEqual(
      true
    );
    component
      .find(`[data-filter-option="last30Days"] input`)
      .simulate('change', {target: {checked: true}});

    component.find('button[data-filter-footer-apply]').simulate('click');
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      label: 'Last 30 days',
      operator: 'ge',
      value: {
        ge: new Date('2019-12-02T08:00:00.000Z'),
      },
    });

    expect(isPanelOpen(component)).toEqual(true);
  });

  it('should be able to apply custom filter', () => {
    const component = mountComponent();
    component
      .find('#toggle-filter-panel-visibility')
      .hostNodes()
      .simulate('change', {
        currentTarget: {
          checked: true,
        },
      });

    component.find(`div[data-filter-list-id="${filterId}"]`).simulate('click');
    expect(component.find('[data-date-and-time-filter]').exists()).toEqual(
      true
    );

    expect(component.find('#custom').exists()).toEqual(false);
    component.find('button#custom_tab').simulate('click');
    expect(component.find('#custom').exists()).toEqual(true);

    component
      .find('input#start-date-picker')
      .simulate('change', {target: {value: '2021-03-02'}});

    component.find('button[data-filter-footer-apply]').simulate('click');

    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      label: 'In Range 2021-03-02 00:00:00 - No End Date',
      operator: 'ge',
      value: {
        ge: new Date('2021-03-01T23:00:00.000Z'),
      },
    });

    component
      .find('input#end-date-picker')
      .simulate('change', {target: {value: '2021-03-12'}});
    component
      .find('input#end-time-picker')
      .simulate('change', {target: {value: '14:00'}});

    component.find('button[data-filter-footer-apply]').simulate('click');

    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      label: 'In Range 2021-03-02 00:00:00 - 2021-03-12 14:00:00',
      operator: 'and',
      value: {
        ge: new Date('2021-03-01T23:00:00.000Z'),
        lt: new Date('2021-03-12T13:00:00.000Z'),
      },
    });
  });
});
