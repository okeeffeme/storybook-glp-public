import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import FilterPanel, {FilterType, Language, Query} from '../../src';
import user from '@testing-library/user-event';

const filterId = 'multiChoiceFilter';
const filterIdWithCustom = 'filterIdWithCustom';
const TEST_SELECT_OPTIONS = [
  {label: 'label_select1', value: 'value_select1'},
  {label: 'label_select2', value: 'value_select2'},
  {label: 'label_select3', value: 'value_select3'},
];
const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();
const getCustomOperatorText = () => 'has';

type RenderProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};

const renderComponent = (props?: RenderProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
  };
  return render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'filter title',
          type: FilterType.MultiChoice,
          props: {
            options: TEST_SELECT_OPTIONS,
          },
        },
        {
          id: filterIdWithCustom,
          title: 'with custom',
          getCustomOperatorText,
          type: FilterType.MultiChoice,
          props: {
            allowCustom: true,
            options: TEST_SELECT_OPTIONS,
          },
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('single-choice-filter', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('should render pills when select filter is present', () => {
    renderComponent({
      value: [
        {
          id: filterId,
          label: ['label_select1', 'custom'],
          operator: 'in',
          value: ['value_select1', 'custom'],
        },
        {
          id: filterIdWithCustom,
          label: ['label_select1', 'custom'],
          operator: 'in',
          value: ['value_select1', 'custom'],
        },
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(Pill[0]).toHaveTextContent('filter title: label_select1, custom');
    expect(Pill[1]).toHaveAttribute('data-pills', filterIdWithCustom);
    expect(Pill[1]).toHaveTextContent('with custom has label_select1, custom');
  });

  it('should open the panel with applied filer when clicked on the pill ', () => {
    renderComponent({
      value: [
        {
          id: filterId,
          label: ['label_select1', 'label_select2', 'custom'],
          operator: 'in',
          value: ['value_select1', 'value_select2', 'custom'],
        },
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(screen.getByTestId('filter-panel')).not.toHaveAttribute(
      'data-active-filter',
      filterId
    );
    user.click(Pill[0]);
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId
    );
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[0]
    ).toHaveAttribute('data-filter-option', 'value_select1');
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[1]
    ).toHaveAttribute('data-filter-option', 'value_select2');
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[2]
    ).toHaveAttribute('data-filter-option', 'value_select3');
  });

  it('should allow the panel to choose multiple options and apply', () => {
    renderComponent();

    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
      'data-filter-list-id',
      filterId
    );
    user.click(screen.getAllByTestId('filter-list-id')[0]);
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId
    );

    expect(
      screen.getAllByTestId('filter-option-multi-choice')[0]
    ).toHaveAttribute('data-filter-option', 'value_select1');
    user.click(screen.getAllByTestId('filter-option-multi-choice')[2]);
    expect(screen.getAllByRole('checkbox')[2]).toHaveAttribute(
      'id',
      'id_value_select3'
    );
    user.click(screen.getAllByRole('checkbox')[2]);
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[2]
    ).toHaveAttribute('data-filter-option', 'value_select3');
    user.click(screen.getByTestId('filter-footer-apply-button'));
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      label: ['label_select3'],
      operator: 'in',
      value: ['value_select3'],
    });
  });

  it('should allow the panel to have custom options', () => {
    renderComponent();

    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));

    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getAllByTestId('filter-list-id')[1]).toHaveAttribute(
      'data-filter-list-id',
      filterIdWithCustom
    );
    user.click(screen.getAllByTestId('filter-list-id')[1]);
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterIdWithCustom
    );

    expect(
      screen.getAllByTestId('filter-option-multi-choice')[0]
    ).toHaveAttribute('data-filter-option', 'value_select1');
    expect(
      screen.getByTestId('custom-option-multi-choice')
    ).toBeInTheDocument();
    expect(screen.getAllByRole('checkbox')[0]).toHaveAttribute(
      'id',
      'id_value_select1'
    );
    user.click(screen.getAllByRole('checkbox')[0]);
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'custom'},
    });
    user.click(screen.getByTestId('button-add-new'));
    user.click(screen.getByTestId('filter-footer-apply-button'));
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[0]
    ).toHaveAttribute('data-filter-option', 'value_select1');
    expect(
      screen.getAllByTestId('filter-option-multi-choice')[3]
    ).toHaveAttribute('data-filter-option', 'custom');
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterIdWithCustom,
      label: ['label_select1', 'custom'],
      operator: 'in',
      value: ['value_select1', 'custom'],
    });
  });
});
