import moment from 'moment';
import {validateAndFormatTime} from '../../src/date-and-time-filter/validateAndFormatTime';
import {Language} from '../../src';

describe('validateAndFormatTime', () => {
  it('should return error, error message and time if no date select', () => {
    expect(validateAndFormatTime(Language.EN)(undefined, '10:35')).toEqual({
      errorTimeText: 'Please select the date first',
      hasTimeError: true,
      time: '10:35',
    });
  });

  it('should return no error, no error message and no time if no date and time select', () => {
    expect(validateAndFormatTime(Language.EN)(undefined, '')).toEqual({
      errorTimeText: '',
      hasTimeError: false,
      time: '',
    });
  });

  it('should return date with 0 hours and 0 minutes if date select and no time', () => {
    const date = moment('2020-12-21T13:35:07.99+01:00');
    const time = validateAndFormatTime(Language.EN)(date, '').time;
    expect(
      typeof time !== 'string' &&
        time.isSame(moment('2020-12-20T23:00:00.000Z'))
    ).toBeTruthy();
  });

  it('should return the error if time is incorrect', () => {
    const date = moment('2020-12-21T13:35:07.99+01:00');
    expect(validateAndFormatTime(Language.EN)(date, '?.:?/')).toEqual({
      errorTimeText: 'Please fill in with valid time',
      hasTimeError: true,
      time: '?.:?/',
    });
    expect(validateAndFormatTime(Language.EN)(date, '9:21')).toEqual({
      errorTimeText: 'Please fill in with valid time',
      hasTimeError: true,
      time: '9:21',
    });
  });

  it('should return correct date if time pass validation', () => {
    const date = moment('2020-12-21T13:35:07.99+01:00');
    const time = validateAndFormatTime(Language.EN)(date, '12:24').time;
    expect(
      typeof time !== 'string' &&
        time.isSame(moment('2020-12-21T11:24:00.000Z'))
    ).toBeTruthy();
  });
});
