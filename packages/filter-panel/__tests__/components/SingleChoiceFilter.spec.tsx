import React from 'react';
import {render, screen} from '@testing-library/react';
import FilterPanel, {FilterType, Language, Query} from '../../src';
import user from '@testing-library/user-event';
const filterId = 'selectFilter';
const filterWithCustomOperatorId = 'filterWithCustomOperatorId';

const TEST_SELECT_OPTIONS = [
  {label: 'label_select1', value: 'value_select1'},
  {label: 'label_select2', value: 'value_select2'},
];
const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();
const getCustomOperatorText = () => {
  return '=';
};
type RenderProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};
const renderComponent = (props?: RenderProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
  };
  return render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'filter title',
          type: FilterType.SingleChoice,
          props: {
            options: TEST_SELECT_OPTIONS,
          },
        },
        {
          id: filterWithCustomOperatorId,
          title: 'filter title',
          getCustomOperatorText,
          type: FilterType.SingleChoice,
          props: {
            options: TEST_SELECT_OPTIONS,
          },
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('single-choice-filter', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('should render pills when select filter is present', () => {
    renderComponent({
      value: [
        {id: filterId, value: 'value_select1'},
        {id: filterWithCustomOperatorId, value: 'value_select1'},
      ],
    });
    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(Pill[0]).toHaveTextContent('filter title: value_select1');
    expect(Pill[1]).toHaveAttribute('data-pills', filterWithCustomOperatorId);
    expect(Pill[1]).toHaveTextContent('filter title = value_select1');

    user.click(screen.getAllByTestId('input-chip-delete-button')[0]);
    expect(onDelete).toHaveBeenCalled();
  });

  it('should open the filter in the panel when clicked on the filter pill', () => {
    const value = 'value_select2';
    renderComponent({
      value: [
        {id: filterId, value},
        {id: filterWithCustomOperatorId, value},
      ],
    });

    const Pill = screen.getAllByTestId('chip');
    expect(Pill[0]).toHaveAttribute('data-pills', filterId);
    expect(screen.getByTestId('side-panel')).not.toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );

    user.click(Pill[0]);

    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterId
    );
    user.click(screen.getAllByTestId('filter-option-single-choice')[0]);
    expect(Pill[1]).toHaveAttribute('data-pills', filterWithCustomOperatorId);
    user.click(Pill[1]);
    expect(screen.getByTestId('filter-panel')).toHaveAttribute(
      'data-active-filter',
      filterWithCustomOperatorId
    );
  });

  it('should render select filter in drawer and apply filter when clicked on "Apply"', () => {
    renderComponent({
      value: [{id: filterId, value: 'value_select1'}],
    });
    expect(screen.getByRole('checkbox')).toHaveAttribute(
      'id',
      'toggle-filter-panel-visibility'
    );
    user.click(screen.getByRole('checkbox'));
    expect(screen.getAllByTestId('filter-list-id')[0]).toHaveAttribute(
      'data-filter-list-id',
      filterId
    );
    expect(
      screen.queryByTestId('select-filter-single-choice')
    ).not.toBeInTheDocument();
    user.click(screen.getAllByTestId('filter-list-id')[0]);
    expect(
      screen.getByTestId('select-filter-single-choice')
    ).toBeInTheDocument();
    user.click(screen.getAllByRole('radio')[1]);
    user.click(screen.getByTestId('filter-footer-apply-button'));
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      value: 'value_select2',
      label: 'label_select2',
    });
    expect(screen.getByTestId('side-panel')).toHaveAttribute(
      'data-panel-drawer',
      'opened'
    );
  });
});
