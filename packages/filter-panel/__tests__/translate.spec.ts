import {translate} from '../src/translate';
import {Language} from '../src/types';

describe('Translation :: ', () => {
  it('should return english text when english is preferred language', () => {
    const text = translate({
      language: Language.EN,
      key: 'panel.footer.clearAll',
    });
    expect(text).toBe('CLEAR ALL');
  });

  it('should return norwegian text when norwegian is preferred language', () => {
    const text = translate({
      language: Language.NB,
      key: 'panel.footer.clearAll',
    });
    expect(text).toBe('SLETT ALT');
  });

  it('should return the translation key when result is an object', () => {
    const text = translate({
      language: Language.EN,
      key: 'panel.footer',
    });
    expect(text).toBe('panel.footer');
  });

  it('should return the translation key when english is preferred language but translation does not exist', () => {
    const text = translate({
      language: Language.EN,
      key: 'this.key.does.not.exist',
    });
    expect(text).toBe('this.key.does.not.exist');
  });

  it('should default to english text when polish is preferred language but translation does not exist', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore  - language prop only accepts the Language type, but for non-TS users, you can pass any string
    const text = translate({language: 'pl', key: 'panel.footer.clearAll'});
    expect(text).toBe('CLEAR ALL');
  });
});
