import {findByRole, getByRole, within} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

export class FilterPanelObject {
  constructor(private element: HTMLElement) {}
  openFilterPill = (pillTitle: string) => {
    const chip = getByRole(this.element, 'button', {
      name: `Date filter: ${pillTitle}`,
    });
    userEvent.click(chip);
    return this.element.querySelector('[data-date-picker=true]');
  };

  apply = () => {
    userEvent.click(this.applyButton());
  };

  applyButton = () => getByRole(this.element, 'button', {name: 'APPLY'});

  async open(title: string) {
    userEvent.click(this.element.querySelector('[data-filter-panel-toggle]')!);
    const labelElement = await within(this.element).findByText(title);
    userEvent.click(labelElement);
    await findByRole(this.element, 'heading', {name: title});
  }
}
