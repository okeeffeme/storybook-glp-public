import FilterPanel, {FilterType} from '../../src';
import {render, screen} from '@testing-library/react';
import {FilterPanelObject} from './FilterPanel';
import React from 'react';
import userEvent from '@testing-library/user-event';
import MockDate from 'mockdate';

const onDeleteAll = jest.fn();
const onDelete = jest.fn();

const renderDateFilterPanel = async () => {
  const onApply = jest.fn();

  const finalProps = {
    value: [],
    onDeleteAll,
    onDelete,
    onApply,
  };

  const res = render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: 'dateFilter',
          title: 'Date Time filter',
          type: FilterType.DateAndTime,
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );

  const filterPanel = new FilterPanelObject(res.container);
  await filterPanel.open('Date Time filter');

  const verifyQuickOption = (label: string, expectedValue: unknown) => {
    userEvent.click(screen.getByRole('radio', {name: label}));
    filterPanel.apply();
    expect(onApply).toHaveBeenLastCalledWith(expectedValue);
  };

  return {
    ...res,
    filterPanel,
    verifyQuickOption,
  };
};

describe('DateTimeFilter quick options', () => {
  beforeAll(() => {
    MockDate.set(new Date('2021-11-24T13:10:00.000Z'));
  });

  it('Quick options should produce expected values', async () => {
    const {verifyQuickOption} = await renderDateFilterPanel();

    verifyQuickOption('Last week', {
      id: 'dateFilter',
      label: 'Last week',
      value: {ge: new Date('2021-11-17T13:10:00.000Z')},
      operator: 'ge',
    });

    verifyQuickOption('Yesterday', {
      id: 'dateFilter',
      label: 'Yesterday',
      value: {
        lt: new Date('2021-11-23T23:00:00.000Z'),
        ge: new Date('2021-11-22T23:00:00.000Z'),
      },
      operator: 'and',
    });

    verifyQuickOption('Today', {
      id: 'dateFilter',
      label: 'Today',
      value: {ge: new Date('2021-11-23T23:00:00.000Z')},
      operator: 'ge',
    });

    verifyQuickOption('Last Hour', {
      id: 'dateFilter',
      label: 'Last Hour',
      value: {ge: new Date('2021-11-24T12:10:00.000Z')},
      operator: 'ge',
    });
  });
});
