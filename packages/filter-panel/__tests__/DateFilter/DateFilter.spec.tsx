import React from 'react';
import {mount, ReactWrapper} from 'enzyme';
import FilterPanel, {
  DateInterval,
  FilterType,
  Language,
  Query,
} from '../../src';

const filterId = 'dateFilter';
const filterWithCustomOperatorId = 'filterWithCustomOperatorId';

const onDeleteAll = jest.fn();
const onApply = jest.fn();
const onDelete = jest.fn();
const getCustomOperatorText = () => {
  return '=';
};

type MountProps = {
  value?: Query[];
  language?: Language;
  onStatusToggle?: (id: string) => void;
};

const sampleFilter: MountProps = {
  value: [
    {
      id: filterId,
      value: {
        period: 'relative',
        interval: DateInterval.Day,
        offset: 0,
        optionId: 'today',
        optionLabel: 'Today',
        startDate: '2020-01-01T08:00:00',
      },
    },
    {
      id: filterWithCustomOperatorId,
      value: {
        period: 'relative',
        interval: DateInterval.Day,
        offset: 0,
        optionId: 'today',
        optionLabel: 'Today',
        startDate: '2020-01-01T08:00:00',
      },
    },
  ],
};

const isPanelOpen = (component: ReactWrapper) =>
  component.find('SidePanel').prop('data-panel-drawer') === 'opened';
const Pill = (component: ReactWrapper, id?: string) =>
  component.find(`div[data-pills="${id || filterId}"]`);

const mountComponent = (props?: MountProps) => {
  const finalProps = {
    onDeleteAll,
    onApply,
    onDelete,
    value: [],
    ...props,
  };
  return mount(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'filter title',
          type: FilterType.Date,
        },
        {
          id: filterWithCustomOperatorId,
          getCustomOperatorText,
          title: 'filter title',
          type: FilterType.Date,
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );
};

describe('date-filter', () => {
  let dateNowSpy;
  const timeStamp = Date.parse('2020-01-01T08:00:00Z');
  beforeAll(() => {
    jest.clearAllMocks();
    // Mock date used by moment
    dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => timeStamp);
  });

  afterAll(() => {
    // Unlock Time
    dateNowSpy.mockRestore();
  });

  it('should render pills when date filter is present', () => {
    const component = mountComponent(sampleFilter);
    expect(Pill(component).exists()).toEqual(true);
    expect(Pill(component).text()).toEqual('filter title: Today');
    expect(Pill(component, filterWithCustomOperatorId).text()).toEqual(
      'filter title = Today'
    );

    Pill(component)
      .find('button[data-input-chip-delete-button]')
      .simulate('click');
    expect(onDelete).toHaveBeenCalled();
  });

  it('should open the filter in the panel when clicked on the filter pill ', () => {
    const component = mountComponent(sampleFilter);

    expect(Pill(component).exists()).toEqual(true);
    expect(isPanelOpen(component)).toEqual(false);

    Pill(component).simulate('click');
    expect(isPanelOpen(component)).toEqual(true);
    expect(
      component.find(`[data-active-filter="${filterId}"]`).exists()
    ).toEqual(true);
    expect(
      component.find(`[data-quick-option="today"] input`).props().checked
    ).toEqual(true);
  });

  it('should render date filter in drawer and apply filter when clicked on "Apply"', () => {
    const component = mountComponent(sampleFilter);
    component
      .find('#toggle-filter-panel-visibility')
      .hostNodes()
      .simulate('change', {
        currentTarget: {
          checked: true,
        },
      });
    expect(
      component.find(`div[data-filter-list-id="${filterId}"]`).exists()
    ).toEqual(true);
    expect(component.find('div[data-date-filter]').exists()).toEqual(false);
    component.find(`div[data-filter-list-id="${filterId}"]`).simulate('click');
    expect(component.find('div[data-date-filter]').exists()).toEqual(true);

    component
      .find(`[data-quick-option="yesterday"] input`)
      .simulate('change', {target: {checked: true}});
    component.find('button[data-filter-footer-apply]').simulate('click');
    expect(onApply).toHaveBeenLastCalledWith({
      id: filterId,
      value: {
        period: 'relative',
        interval: DateInterval.Day,
        offset: -1,
        optionId: 'yesterday',
        optionLabel: 'Yesterday',
        startDate: '2019-12-30T23:00:00.000',
      },
    });
    expect(isPanelOpen(component)).toEqual(true);
  });
});
