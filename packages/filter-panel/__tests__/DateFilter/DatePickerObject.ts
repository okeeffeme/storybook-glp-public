import userEvent from '@testing-library/user-event';

import {screen, within, waitFor, fireEvent} from '@testing-library/react';

const DOWN_ARROW = {keyCode: 40};

class SelectObject {
  constructor(public controlElement) {}

  async selectOption(optionLabel: string) {
    const el = within(this.controlElement).getByRole('textbox');
    await waitFor(() => expect(el).not.toBeDisabled());
    fireEvent.keyDown(el, DOWN_ARROW);
    // The groups appear in portal - therefore we are searching them on the whole screen.
    const nextGroupElement = await screen.findByText(optionLabel);
    userEvent.click(nextGroupElement);
  }

  async clear() {
    const input = within(this.controlElement).getByRole('textbox');
    await waitFor(() => expect(input).not.toBeDisabled());
    const clearButton = this.controlElement.querySelector(
      '[data-test-simple-select-clear-indicator=true]'
    );
    userEvent.click(clearButton);
  }
}

export class DatePickerObject {
  static testId = 'reporting-period-picker';
  constructor(public element) {}

  static getReportingPeriodEditor(parent: HTMLElement) {
    return new DatePickerObject(within(parent).getByTestId(this.testId));
  }

  static async findReportingPeriodEditor(parent: HTMLElement) {
    return new DatePickerObject(await within(parent).findByTestId(this.testId));
  }

  getGroupSelect(): HTMLElement {
    return within(this.element).getByTestId('reporting-period-select');
  }

  getCurrentGroupOptions(): HTMLElement[] {
    return this.element.querySelectorAll('[data-quick-option]');
  }

  getOptionInput(option: HTMLElement): HTMLElement {
    return option.querySelector('[data-text-field-component-input="true"]')!;
  }

  getOptionInputByIndex(index: number): HTMLElement {
    const options = this.getCurrentGroupOptions();
    return this.getOptionInput(options[index]);
  }

  getOptionRadio(option: HTMLElement): HTMLElement {
    return within(option).getByRole('radio');
  }

  getOptionRadioByIndex(index: number): HTMLElement {
    const options = this.getCurrentGroupOptions();
    return this.getOptionRadio(options[index]);
  }

  selectOption(label: string) {
    userEvent.click(within(this.element).getByRole('radio', {name: label}));
  }

  async selectGroup(nextGroup: string) {
    const select = this.getGroupSelect();
    const selectObject = new SelectObject(select);
    await selectObject.selectOption(nextGroup);
  }

  async clearGroup() {
    const select = this.getGroupSelect();
    const selectObject = new SelectObject(select);
    await selectObject.clear();
  }
}
