import React from 'react';
import MockDate from 'mockdate';

import {configure, render, screen, within} from '@testing-library/react';
import {DatePickerObject} from './DatePickerObject';
import FilterPanel, {
  CustomRangeFilterValue,
  DateInterval,
  FilterType,
  Language,
  Query,
} from '../../src';
import {FilterPanelObject} from './FilterPanel';
import {
  DateFilterValue,
  WeekStart,
} from '../../src/components/date-picker/types';
import userEvent from '@testing-library/user-event';
import {DateFilterProps} from '../../src/date-filter/DateFilter';
import {fiscalCalendar} from '../../stories/fiscalCalendar';

type MountProps = {
  value: DateFilterValue;
  language?: Language;
  onStatusToggle?: (id: string) => void;
  onApply: (args: Query) => void;
  props?: DateFilterProps;
};

const filterId = 'dateFilter';

const onDeleteAll = jest.fn();
const onDelete = jest.fn();

configure({testIdAttribute: 'data-test-id'});

function makeCheckOption(
  filterPanel: FilterPanelObject,
  datePicker: DatePickerObject,
  handleChangeMock: jest.Mock
) {
  return function (optionLabel: string, expectedValue: DateFilterValue) {
    datePicker.selectOption(optionLabel);
    filterPanel.apply();
    expect(handleChangeMock).toHaveBeenLastCalledWith({
      id: 'dateFilter',
      value: expectedValue,
    });

    const radio = within(datePicker.element).getByRole('radio', {
      name: optionLabel,
    });
    expect(radio).toBeChecked();
  };
}

const renderDateFilterPanel = (props: MountProps) => {
  const {value, props: filterProps, onApply, ...rest} = props;

  const queryValue: Query[] = value
    ? [
        {
          id: filterId,
          value,
        },
      ]
    : [];

  const finalProps = {
    onDeleteAll,
    onDelete,
    onApply,
    value: queryValue,
    ...rest,
  };

  const res = render(
    <FilterPanel
      {...finalProps}
      filters={[
        {
          id: filterId,
          title: 'Date filter',
          type: FilterType.Date,
          props: filterProps,
        },
      ]}>
      <div data-test-id="dummy-component" />
    </FilterPanel>
  );

  const filterPanel = new FilterPanelObject(res.container);
  const datePicker = new DatePickerObject(
    filterPanel.openFilterPill(value.optionLabel)
  );

  const checkOption = makeCheckOption(
    filterPanel,
    datePicker,
    onApply as jest.Mock
  );
  return {
    ...res,
    checkOption,
    filterPanel,
    datePicker,
  };
};

const today: DateFilterValue = {
  period: 'relative',
  interval: DateInterval.Day,
  offset: 0,
  optionId: 'today',
  optionLabel: 'Today',
  startDate: '2021-11-24T00:00:00.000',
};

const previous10days: DateFilterValue = {
  optionId: 'previousdays',
  period: 'relative',
  interval: DateInterval.Day,
  optionLabel: 'Previous 10 days',
  offset: -10,
  startDate: '2021-11-14T00:00:00.000',
};

describe('DateFilter options', () => {
  beforeAll(() => {
    MockDate.set(new Date('2021-11-24T13:10:00.000Z'));
  });
  it('should have correct values when setting days', () => {
    const handleChange = jest.fn();
    const {checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    checkOption('Today', {
      period: 'relative',
      offset: 0,
      interval: DateInterval.Day,
      optionId: 'today',
      optionLabel: 'Today',
      startDate: '2021-11-23T23:00:00.000',
    });

    checkOption('Yesterday', {
      optionId: 'yesterday',
      period: 'relative',
      interval: DateInterval.Day,
      offset: -1,
      optionLabel: 'Yesterday',
      startDate: '2021-11-22T23:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousdays',
      period: 'relative',
      interval: DateInterval.Day,
      optionLabel: 'Previous 10 days',
      startDate: '2021-11-14T13:10:00.000',
      offset: -10,
    });
  });
  it('should have correct values when setting weeks', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    await datePicker.selectGroup('Weeks');

    checkOption('Calendar week', {
      optionId: 'calendarweek',
      period: 'absolute',
      interval: DateInterval.Week,
      optionLabel: 'Calendar week 48 2021',
      periodValue: 48,
      year: 2021,
      startDate: '2021-11-21T00:00:00.000',
      endDate: '2021-11-28T00:00:00.000',
    });

    checkOption('Current calendar week to date', {
      optionId: 'currentweek',
      period: 'relative',
      interval: DateInterval.Week,
      offset: 0,
      optionLabel: 'Current calendar week to date',
      startDate: '2021-11-20T23:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousweeks',
      period: 'relative',
      interval: DateInterval.Week,
      optionLabel: 'Previous 1 calendar weeks',
      startDate: '2021-11-17T13:10:00.000',
      offset: -1,
    });
  });
  it('should respect weekStart configuration', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
      props: {
        calendarConfig: {
          weekStart: WeekStart.wednesday,
        },
      },
    });

    await datePicker.selectGroup('Weeks');

    checkOption('Calendar week', {
      optionId: 'calendarweek',
      period: 'absolute',
      interval: DateInterval.Week,
      optionLabel: 'Calendar week 48 2021',
      periodValue: 48,
      year: 2021,
      startDate: '2021-11-24T00:00:00.000',
      endDate: '2021-12-01T00:00:00.000',
    });

    checkOption('Current calendar week to date', {
      optionId: 'currentweek',
      period: 'relative',
      interval: DateInterval.Week,
      offset: 0,
      optionLabel: 'Current calendar week to date',
      startDate: '2021-11-23T23:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousweeks',
      period: 'relative',
      interval: DateInterval.Week,
      optionLabel: 'Previous 1 calendar weeks',
      startDate: '2021-11-17T13:10:00.000',
      offset: -1,
    });
  });
  it('should have correct values when setting months', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    await datePicker.selectGroup('Months');

    checkOption('Calendar month', {
      optionId: 'calendarmonth',
      period: 'absolute',
      interval: DateInterval.Month,
      year: 2021,
      periodValue: 11,
      startDate: '2021-11-01T00:00:00.000',
      endDate: '2021-12-01T00:00:00.000',
      optionLabel: 'Calendar month November 2021',
    });

    checkOption('Current month to date', {
      optionId: 'currentmonth',
      period: 'relative',
      interval: DateInterval.Month,
      offset: 0,
      optionLabel: 'Current month to date',
      startDate: '2021-10-31T23:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousmonths',
      period: 'relative',
      interval: DateInterval.Month,
      optionLabel: 'Previous 1 months',
      startDate: '2021-10-24T12:10:00.000',
      offset: -1,
    });
  });
  it('should have correct values when setting quarters', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    await datePicker.selectGroup('Quarters');

    checkOption('Calendar quarter', {
      optionId: 'calendarquarter',
      interval: DateInterval.Quarter,
      period: 'absolute',
      periodValue: 4,
      year: 2021,
      startDate: '2021-10-01T00:00:00.000',
      endDate: '2022-01-01T00:00:00.000',
      optionLabel: 'Calendar quarter Q4 2021',
    });

    checkOption('Current quarter to date', {
      optionId: 'currentquarter',
      period: 'relative',
      interval: DateInterval.Quarter,
      offset: 0,
      optionLabel: 'Current quarter to date',
      startDate: '2021-09-30T22:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousquarters',
      period: 'relative',
      interval: DateInterval.Quarter,
      optionLabel: 'Previous 1 quarters',
      startDate: '2021-08-24T12:10:00.000',
      offset: -1,
    });
  });
  it('should have correct values when setting years', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    await datePicker.selectGroup('Years');

    checkOption('Calendar year', {
      optionId: 'calendaryear',
      period: 'absolute',
      periodValue: 0,
      interval: DateInterval.Year,
      year: 2021,
      startDate: '2021-01-01T00:00:00.000',
      endDate: '2022-01-01T00:00:00.000',
      optionLabel: 'Calendar year 2021',
    });

    checkOption('Current year to date', {
      optionId: 'currentyear',
      period: 'relative',
      interval: DateInterval.Year,
      offset: 0,
      optionLabel: 'Current year to date',
      startDate: '2020-12-31T23:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousyears',
      period: 'relative',
      interval: DateInterval.Year,
      optionLabel: 'Previous 1 years',
      startDate: '2020-11-24T13:10:00.000',
      offset: -1,
    });
  });
  it('should be able to change custom range value', async () => {
    const handleChange = jest.fn();

    const rangeValue: CustomRangeFilterValue = {
      period: 'custom',
      startDate: '2021-07-08T00:00:00.000',
      endDate: '2021-07-10T23:59:59.998',
      optionId: 'Between',
      optionLabel: 'In Range 2021-07-08 - 2021-07-10',
    };

    const {datePicker, filterPanel} = renderDateFilterPanel({
      value: rangeValue,
      onApply: handleChange,
    });
    const select = datePicker.getGroupSelect();
    expect(select).toHaveTextContent('Custom');

    const startInput = screen.getByDisplayValue('2021-07-08');
    userEvent.click(startInput);
    const day1 = await screen.findByText('1');
    userEvent.click(day1);
    filterPanel.apply();

    const endInput = screen.getByDisplayValue('2021-07-10');
    userEvent.click(endInput);
    const day5 = await screen.findByText('5');
    userEvent.click(day5);
    filterPanel.apply();

    userEvent.clear(screen.getByDisplayValue('2021-07-01'));
    filterPanel.apply();
    userEvent.clear(screen.getByDisplayValue('2021-07-05'));
    expect(filterPanel.applyButton()).toBeDisabled();

    expect(select).toHaveTextContent('Custom');
    expect(handleChange).toHaveBeenNthCalledWith(1, {
      id: 'dateFilter',
      value: {
        period: 'custom',
        optionId: 'Between',
        optionLabel: 'In Range 2021-07-01 - 2021-07-10',
        startDate: '2021-07-01T00:00:00.000',
        endDate: '2021-07-10T23:59:59.998',
      },
    });
    expect(handleChange).toHaveBeenNthCalledWith(2, {
      id: 'dateFilter',
      value: {
        period: 'custom',
        optionId: 'Between',
        optionLabel: 'In Range 2021-07-01 - 2021-07-05',
        startDate: '2021-07-01T00:00:00.000',
        endDate: '2021-07-05T23:59:59.998',
      },
    });
    expect(handleChange).toHaveBeenNthCalledWith(3, {
      id: 'dateFilter',
      value: {
        period: 'custom',
        optionId: 'Between',
        endDate: '2021-07-05T23:59:59.998',
        optionLabel: 'In Range No Start Date - 2021-07-05',
      },
    });
  });
});

describe('Working with fiscal periods', () => {
  beforeAll(() => {
    MockDate.set(new Date('2021-11-24T13:10:00.000Z'));
  });

  it('should have correct values when setting weeks', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
      props: {
        calendarConfig: {
          fiscalCalendar,
        },
      },
    });

    await datePicker.selectGroup('Fiscal weeks');

    checkOption('Fiscal week', {
      optionId: 'fiscalWeek',
      isFiscal: true,
      period: 'absolute',
      interval: DateInterval.Week,
      year: 2022,
      periodValue: 43,
      startDate: '2021-11-22T00:00:00.000',
      endDate: '2021-11-28T23:59:59.999',
      optionLabel: 'Fiscal week 43 2022',
    });

    checkOption('Current fiscal week to date', {
      optionId: 'currentFiscalWeek',
      isFiscal: true,
      period: 'relative',
      offset: 0,
      interval: DateInterval.Week,
      optionLabel: 'Current fiscal week to date',
      startDate: '2021-11-22T00:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousFiscalWeeks',
      isFiscal: true,
      period: 'relative',
      interval: DateInterval.Week,
      optionLabel: 'Previous 1 fiscal weeks',
      startDate: '2021-11-15T00:00:00.000',
      offset: -1,
    });
  });

  it('should have correct values when setting months', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
      props: {
        calendarConfig: {
          fiscalCalendar,
        },
      },
    });

    await datePicker.selectGroup('Fiscal months');

    checkOption('Fiscal month', {
      optionId: 'fiscalMonth',
      isFiscal: true,
      period: 'absolute',
      interval: DateInterval.Month,
      year: 2022,
      periodValue: 10,
      startDate: '2021-11-03T00:00:00.000',
      endDate: '2021-11-30T23:59:59.999',
      optionLabel: 'Fiscal month November 2022',
    });

    checkOption('Current fiscal month to date', {
      optionId: 'currentFiscalMonth',
      isFiscal: true,
      period: 'relative',
      offset: 0,
      interval: DateInterval.Month,
      optionLabel: 'Current fiscal month to date',
      startDate: '2021-11-03T00:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousFiscalMonths',
      isFiscal: true,
      period: 'relative',
      interval: DateInterval.Month,
      optionLabel: 'Previous 1 fiscal months',
      startDate: '2021-10-06T00:00:00.000',
      offset: -1,
    });
  });
  it('should have correct values when setting quarters', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
      props: {
        calendarConfig: {
          fiscalCalendar,
        },
      },
    });

    await datePicker.selectGroup('Fiscal quarters');

    checkOption('Fiscal quarter', {
      optionId: 'fiscalQuarter',
      isFiscal: true,
      period: 'absolute',
      interval: DateInterval.Quarter,
      periodValue: 4,
      year: 2022,
      startDate: '2021-11-03T00:00:00.000',
      endDate: '2022-02-01T23:59:59.999',
      optionLabel: 'Fiscal quarter Q4 2022',
    });

    checkOption('Current fiscal quarter to date', {
      optionId: 'currentFiscalQuarter',
      isFiscal: true,
      period: 'relative',
      offset: 0,
      interval: DateInterval.Quarter,
      optionLabel: 'Current fiscal quarter to date',
      startDate: '2021-11-03T00:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousFiscalQuarters',
      isFiscal: true,
      period: 'relative',
      interval: DateInterval.Quarter,
      optionLabel: 'Previous 1 fiscal quarters',
      startDate: '2021-08-04T00:00:00.000',
      offset: -1,
    });
  });
  it('should have correct values when setting years', async () => {
    const handleChange = jest.fn();
    const {datePicker, checkOption} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
      props: {
        calendarConfig: {
          fiscalCalendar,
        },
      },
    });

    await datePicker.selectGroup('Fiscal years');

    checkOption('Fiscal year', {
      optionId: 'fiscalYear',
      isFiscal: true,
      period: 'absolute',
      periodValue: 0,
      interval: DateInterval.Year,
      year: 2022,
      startDate: '2021-02-02T00:00:00.000',
      endDate: '2022-02-01T23:59:59.999',
      optionLabel: 'Fiscal year 2022',
    });

    checkOption('Current fiscal year to date', {
      optionId: 'currentFiscalYear',
      isFiscal: true,
      period: 'relative',
      offset: 0,
      interval: DateInterval.Year,
      optionLabel: 'Current fiscal year to date',
      startDate: '2021-02-02T00:00:00.000',
    });

    checkOption('Previous', {
      optionId: 'previousFiscalYears',
      isFiscal: true,
      period: 'relative',
      interval: DateInterval.Year,
      optionLabel: 'Previous 1 fiscal years',
      startDate: '2020-02-02T00:00:00.000',
      offset: -1,
    });
  });
});

describe('Edge cases and unusual stuff', () => {
  beforeAll(() => {
    MockDate.set(new Date('2021-11-24'));
  });

  it("should select days group when '-N day(s)' value is passed", () => {
    const handleChange = jest.fn();
    const {datePicker} = renderDateFilterPanel({
      value: previous10days,
      onApply: handleChange,
    });

    expect(handleChange).not.toBeCalled();

    const select = datePicker.getGroupSelect();

    expect(select).toHaveTextContent('Days');

    const optionList = datePicker.getCurrentGroupOptions();

    expect(optionList).toHaveLength(3);
    expect(optionList[0]).toHaveTextContent('Today');
    expect(optionList[1]).toHaveTextContent('Yesterday');
    expect(optionList[2]).toHaveTextContent(/Previous.+days/);
    expect(datePicker.getOptionInput(optionList[2])).toHaveValue(10);
    expect(datePicker.getOptionRadio(optionList[2])).toBeChecked();
  });

  it('switching from days to weeks should not trigger onChange callback', async () => {
    const handleChange = jest.fn();
    const {datePicker} = renderDateFilterPanel({
      value: today,
      onApply: handleChange,
    });

    await datePicker.selectGroup('Weeks');
    expect(handleChange).not.toBeCalled();
  });

  it("should allow to select 'Include All Data' group", async () => {
    const handleChange = jest.fn();
    const {datePicker, filterPanel} = renderDateFilterPanel({
      value: today,
      onApply: handleChange,
      props: {
        customOptions: [
          {
            id: 'allData',
            label: 'Include All Data',
          },
        ],
      },
    });
    await datePicker.selectGroup('Include All Data');

    filterPanel.apply();

    expect(handleChange).toHaveBeenCalledWith({
      id: 'dateFilter',
      value: {groupId: 'allData', optionLabel: 'Include All Data'},
    });

    const options = datePicker.getCurrentGroupOptions();
    expect(options).toHaveLength(0);

    const select = datePicker.getGroupSelect();
    expect(select).toHaveTextContent('Include All Data');
  });
});
