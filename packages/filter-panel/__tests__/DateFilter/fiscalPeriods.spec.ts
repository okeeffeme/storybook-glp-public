import {fiscalCalendar} from '../../stories/fiscalCalendar';
import moment from 'moment';
import MockDate from 'mockdate';
import {
  CalendarPeriod,
  DateInterval,
} from '../../src/components/date-picker/types';
import {
  findPeriod,
  findPeriodByInterval,
  getFiscalPeriods,
} from '../../src/components/date-picker/fiscalPeriods';

const formatString = 'YYYY-MM-DD[T]HH:mm:ss.SSS';

const printPeriod = (period?: CalendarPeriod) => {
  if (!period) return undefined;

  return {
    ...period,
    start: period?.start ? period.start.format(formatString) : undefined,
    finish: period?.finish ? period.finish.format(formatString) : undefined,
  };
};

describe('Fiscal periods', () => {
  beforeAll(() => {
    MockDate.set(new Date('2021-11-24T13:10:00.000Z'));
  });

  it('find year period should return fiscal year', () => {
    const periods = getFiscalPeriods(fiscalCalendar, DateInterval.Year, moment);
    const period = findPeriod(periods, moment());
    expect(printPeriod(period)).toEqual({
      periodValue: 0,
      start: '2021-02-02T00:00:00.000',
      finish: '2022-02-01T23:59:59.999',
      year: 2022,
    });
  });

  it('find fiscal month', () => {
    const months = getFiscalPeriods(
      fiscalCalendar,
      DateInterval.Month,
      moment,
      2022
    );
    expect(months.length).toBe(12);
    const currentMonth = findPeriod(months, moment());
    expect(printPeriod(currentMonth)).toEqual({
      start: '2021-11-03T00:00:00.000',
      finish: '2021-11-30T23:59:59.999',
      periodValue: 10,
      year: 2022,
    });
  });

  it('get fiscal quarters', () => {
    const quarters = getFiscalPeriods(
      fiscalCalendar,
      DateInterval.Quarter,
      moment,
      2022
    );
    expect(quarters.map(printPeriod)).toEqual([
      {
        year: 2022,
        periodValue: 1,
        start: '2021-02-02T00:00:00.000',
        finish: '2021-05-04T23:59:59.999',
      },
      {
        year: 2022,
        periodValue: 2,
        start: '2021-05-05T00:00:00.000',
        finish: '2021-08-03T23:59:59.999',
      },
      {
        year: 2022,
        periodValue: 3,
        start: '2021-08-04T00:00:00.000',
        finish: '2021-11-02T23:59:59.999',
      },
      {
        year: 2022,
        periodValue: 4,
        start: '2021-11-03T00:00:00.000',
        finish: '2022-02-01T23:59:59.999',
      },
    ]);
  });

  it('find period by interval with week specified', () => {
    const period = findPeriodByInterval(
      fiscalCalendar,
      DateInterval.Week,
      moment,
      moment()
    );

    expect(printPeriod(period)).toEqual({
      periodValue: 43,
      start: '2021-11-21T00:00:00.000',
      finish: '2021-11-27T23:59:59.999',
      year: 2022,
    });
  });
});
