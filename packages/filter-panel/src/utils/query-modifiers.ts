import {Query} from '../types';

export const getParamsFromQuery = (
  filters: Query[],
  filterId: string
): Query | undefined => filters.find(({id}) => id === filterId);
