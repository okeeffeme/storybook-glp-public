import texts from './locale';
import {Language} from './types';
import {developerNotice} from '@jotunheim/react-utils';

const DEFAULT_LANGUAGE = Language.EN;

type Locale = Record<string, unknown>;

type getTranslationProps = {
  path: string;
  language: Language;
};

const getTranslation = ({
  path,
  language,
}: getTranslationProps): string | undefined => {
  const translations = texts[language];

  const text = path
    .split('.')
    .reduce(
      (acc: Locale | string, current) => acc && acc[current],
      translations
    );

  if (!text) {
    developerNotice(
      `filter-panel: translation key "${path}" could not be found for "${language}" language.`
    );
  }

  if (typeof text === 'object') {
    developerNotice(
      `filter-panel: translation key "${path}" results in an object for "${language}" language.`
    );
    return path;
  }

  return typeof text === 'string' ? text : undefined;
};

export const translate = ({
  key,
  language,
}: {
  key: string;
  language: Language;
}): string =>
  getTranslation({path: key, language}) ||
  getTranslation({path: key, language: DEFAULT_LANGUAGE}) ||
  key;

export default translate;
