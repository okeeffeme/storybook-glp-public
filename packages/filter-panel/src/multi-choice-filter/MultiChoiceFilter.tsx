import React, {useContext, useEffect, useMemo, useState} from 'react';
import {CheckBox} from '@jotunheim/react-toggle';
import {TextField} from '@jotunheim/react-text-field';
import {Button} from '@jotunheim/react-button';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Fieldset, FieldsetGroup} from '@jotunheim/react-fieldset';
import {bemFactory} from '@jotunheim/react-themes';

import FilterContainer from '../components/FilterContainer';
import FilterHelpText from '../components/FilterHelpText';
import {MultiChoiceFilterProps, MultiChoiceValue, Option} from './types';
import {getParamsFromQuery} from '../utils/query-modifiers';
import {setFilterValid, updateQuery, addOption} from '../state/actions';
import FilterContext, {FilterDispatchContext} from '../context';

import useOptions from './useOptions';
import translate from '../translate';

import classNames from './MultiChoiceFilter.module.css';
import {BusyDots} from '@jotunheim/react-busy-dots';

const ENTER_KEY = 13;

const {block, element} = bemFactory(
  'comd-multi-choice-custom-option',
  classNames
);

const MultiChoiceFilter = ({
  options: passedOptions,
  isLoading,
  allowCustom = false,
  customPlaceholder,
  customHelpText,
  onValidateCustom = () => '',
  ...rest
}: MultiChoiceFilterProps) => {
  const {queries, activeFilterId, language} = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const options = useOptions({passedOptions});

  const prevValue = useMemo(() => {
    const matchedFilter = getParamsFromQuery(queries, activeFilterId);

    return options.filter(
      (option) =>
        Array.isArray(matchedFilter?.value) &&
        matchedFilter?.value.includes(option.value)
    );
  }, [queries, activeFilterId, options]);

  const [selectedOptions, setSelectedOptions] = useState(prevValue);

  useEffect(() => {
    dispatch(setFilterValid(!!selectedOptions?.length));
  }, [selectedOptions, dispatch]);

  useEffect(() => {
    if (selectedOptions?.length) {
      const newQuery = selectedOptions.reduce(
        (query: {label: string[]; value: MultiChoiceValue}, option: Option) => {
          query.value.push(option.value);
          query.label.push(option.label);

          return query;
        },
        {value: [], label: []}
      );

      dispatch(
        updateQuery({
          id: activeFilterId,
          value: newQuery.value,
          label: newQuery.label,
          operator: 'in',
        })
      );
    } else {
      dispatch(updateQuery());
    }
  }, [activeFilterId, dispatch, selectedOptions]);

  const handleItemSelection = (isChecked, option) => {
    if (isChecked) {
      setSelectedOptions([...selectedOptions, option]);
    } else {
      setSelectedOptions(
        selectedOptions.filter(({value}) => value !== option.value)
      );
    }
  };

  const [newOptionValue, setNewOptionValue] = useState<string>('');
  const [errorForCustom, setErrorForCustom] = useState<string>('');
  const customExists = () =>
    options.some((o) => {
      return String(o.value).toUpperCase() === newOptionValue.toUpperCase();
    });

  const isAddDisabled = !newOptionValue || !!errorForCustom || customExists();

  const handleAddKeyword = () => {
    if (isAddDisabled) {
      return;
    }

    const newOption = {
      value: newOptionValue,
      label: newOptionValue,
    };

    dispatch(addOption(activeFilterId, newOptionValue));

    setSelectedOptions([...selectedOptions, newOption]);

    setNewOptionValue('');
  };

  return (
    <FilterContainer
      data-multi-choice-filter=""
      {...extractDataAriaIdProps(rest)}>
      <FieldsetGroup layoutStyle={Fieldset.LayoutStyle.Vertical}>
        {allowCustom && (
          <Fieldset
            layoutStyle={Fieldset.LayoutStyle.Vertical}
            data-custom-option=""
            data-testid="custom-option-multi-choice">
            <div className={block()}>
              <TextField
                value={newOptionValue}
                placeholder={customPlaceholder}
                onChange={(value) => {
                  setNewOptionValue(value);
                  setErrorForCustom(onValidateCustom(value));
                }}
                error={!!newOptionValue && !!errorForCustom}
                helperText={errorForCustom}
                onKeyPress={(e) => {
                  if (e.which === ENTER_KEY) {
                    handleAddKeyword();
                  }
                }}
              />
              <div className={element('add-button')}>
                <Button
                  disabled={isAddDisabled}
                  onClick={handleAddKeyword}
                  data-testid="button-add-new"
                  data-button-add-new="">
                  {translate({language, key: 'panel.filter.addNew'})}
                </Button>
              </div>
            </div>
            {customHelpText && (
              <FilterHelpText>{customHelpText}</FilterHelpText>
            )}
          </Fieldset>
        )}

        <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
          {options.map(({value, label}) => (
            <CheckBox
              key={`key_${value}`}
              id={`id_${value}`}
              checked={
                !!selectedOptions.find(
                  ({value: selectedOptionValue}) =>
                    selectedOptionValue === value
                )
              }
              onChange={(isChecked) =>
                handleItemSelection(isChecked, {value, label})
              }
              data-testid="filter-option-multi-choice"
              data-filter-option={value}>
              {label || value}
            </CheckBox>
          ))}
          {isLoading && <BusyDots />}
        </Fieldset>
      </FieldsetGroup>
    </FilterContainer>
  );
};

export default MultiChoiceFilter;
