export type MultiChoiceValue = Array<string | number>;

export type Option = {
  value: string | number;
  label: string;
};

export type MultiChoiceFilterProps = {
  options: Option[];
  allowCustom?: boolean;
  customPlaceholder?: string;
  customHelpText?: string;
  onValidateCustom?: (value: string) => string;
  isLoading?: boolean;
};
