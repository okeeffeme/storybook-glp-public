import {useContext, useEffect, useState} from 'react';

import {Option} from './types';
import FilterContext from '../context';

const createMergedOptions = (
  customOptionValues = <string[]>[],
  passedOptions = []
) => {
  const customOptions = customOptionValues.map((o) => ({
    value: o,
    label: o,
  }));

  return [...passedOptions, ...customOptions];
};

const useOptions = ({passedOptions}) => {
  const {optionsById, activeFilterId} = useContext(FilterContext);
  const [options, setOptions] = useState<Option[]>(
    createMergedOptions(optionsById[activeFilterId], passedOptions)
  );

  useEffect(() => {
    const mergedOptions = createMergedOptions(
      optionsById[activeFilterId],
      passedOptions
    );

    setOptions(mergedOptions);
  }, [activeFilterId, optionsById, passedOptions]);

  return options;
};

export default useOptions;
