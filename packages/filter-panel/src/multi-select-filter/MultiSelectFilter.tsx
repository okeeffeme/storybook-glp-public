import React, {useContext, useEffect, useMemo, useState} from 'react';
import {Select} from '@jotunheim/react-select';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import FilterContainer from '../components/FilterContainer';

import FilterContext, {FilterDispatchContext} from '../context';
import {MultiChoiceValue} from '../multi-choice-filter/types';
import {getParamsFromQuery} from '../utils/query-modifiers';
import {setFilterValid, updateQuery} from '../state/actions';
import {Option, MultiSelectFilterProps} from './types';

const MultiSelectFilter = ({
  options,
  loadOptions,
  allowCustom = false,
  customPlaceholder,
  ...rest
}: MultiSelectFilterProps) => {
  const {queries, activeFilterId} = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const prevValue = useMemo(() => {
    const matchedFilter = getParamsFromQuery(queries, activeFilterId);
    return Array.isArray(matchedFilter?.value)
      ? matchedFilter?.value.map((v, idx) => {
          return {
            value: v,
            label: (matchedFilter && matchedFilter?.label?.[idx]) ?? String(v),
          };
        })
      : [];
  }, [queries, activeFilterId]);
  const [selectedOption, setSelectedOption] = useState(prevValue);

  useEffect(() => {
    dispatch(setFilterValid(!!selectedOption?.length));
  }, [selectedOption, dispatch]);

  useEffect(() => {
    if (selectedOption?.length) {
      const query = selectedOption.reduce(
        (q: {label: string[]; value: MultiChoiceValue}, option: Option) => {
          q.value.push(option.value);
          q.label.push(option.label);
          return q;
        },
        {value: [], label: []}
      );
      dispatch(
        updateQuery({
          id: activeFilterId,
          value: query.value,
          label: query.label,
          operator: 'in',
        })
      );
    } else {
      dispatch(updateQuery());
    }
  }, [activeFilterId, dispatch, selectedOption]);

  return (
    <FilterContainer
      {...extractDataAriaIdProps(rest)}
      data-multi-select-filter=""
      data-testid="multi-select-filter">
      <Select
        isMulti={true}
        isClearable={true}
        isCreatable={allowCustom}
        value={selectedOption}
        options={options}
        loadOptions={loadOptions}
        createPrefix={customPlaceholder}
        onChange={setSelectedOption}
        data-testid="react-select-filter"
        data-react-select-filter=""
      />
    </FilterContainer>
  );
};

export default MultiSelectFilter;
