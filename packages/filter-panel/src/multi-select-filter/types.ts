export type Option = {
  value: string | number;
  label: string;
};

type LoadOptionsHandler = (
  inputValue: string,
  prevOptions: Option[],
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  additional?: any
) => Promise<{
  options: Option[];
  hasMore: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  additional?: any;
}>;

export type MultiSelectFilterProps = {
  allowCustom?: boolean;
  customPlaceholder?: string;
  options?: Option[];
  loadOptions?: LoadOptionsHandler;
};
