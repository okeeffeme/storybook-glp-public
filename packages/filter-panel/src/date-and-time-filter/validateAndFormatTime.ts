import moment from 'moment';
import {Language} from '../../src';
import translate from '../translate';

const timeRegExp = /^([01]?[0-9]{1}|2[0-3]{1}):[0-5]{1}[0-9]{1}$/;

type validateAndFormatTimeReturnValue = {
  hasTimeError: boolean;
  errorTimeText: string;
  time: string | moment.Moment;
};

export const validateAndFormatTime =
  (language: Language) =>
  (
    dateValue?: moment.Moment,
    time?: string
  ): validateAndFormatTimeReturnValue => {
    if (!dateValue) {
      return time
        ? {
            hasTimeError: true,
            errorTimeText: translate({
              language,
              key: 'dateAndTimeFilter.validation.noDateError',
            }),
            time,
          }
        : {hasTimeError: false, errorTimeText: '', time: ''};
    }

    if (dateValue && !time) {
      dateValue.set('hour', 0);
      dateValue.set('minute', 0);
      dateValue.set('second', 0);
      dateValue.set('millisecond', 0);
      return {hasTimeError: false, errorTimeText: '', time: dateValue};
    }

    const timePattern = '00:00'.split('');
    if (time) {
      for (let i = 0; i < time.length; i++) {
        timePattern[i] = time[i];
      }
    }

    if (timePattern.join('').match(timeRegExp) && time) {
      const [hours, minutes] = time.split(':');

      dateValue.set('hour', +hours);
      dateValue.set('minute', +minutes);
      dateValue.set('second', 0);
      dateValue.set('millisecond', 0);

      return {hasTimeError: false, errorTimeText: '', time: dateValue};
    } else {
      return {
        hasTimeError: true,
        errorTimeText: translate({
          language,
          key: 'dateAndTimeFilter.validation.invalidTime',
        }),
        time: time || '',
      };
    }
  };
