import moment from 'moment';
import {DateValidState, DefaultDateFormat} from './constants';

export const validateDate = (date?: moment.Moment) => {
  return !date
    ? DateValidState.NOT_FILLED
    : moment(date, DefaultDateFormat.DateTime, true).isValid()
    ? DateValidState.VALID
    : DateValidState.INVALID;
};
