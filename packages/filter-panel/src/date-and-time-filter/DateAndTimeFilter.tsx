import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import moment from 'moment';
import camelCase from 'lodash/camelCase';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import DateAndTimePicker from '../components/date-and-time-picker';
import FilterContext, {FilterDispatchContext} from '../context';
import {DateValidState, predefinedQuickDateOptions} from './constants';
import {computeDatesQuickFilterQuery} from './computeDatesQuickFilterQuery';
import {computeDateCustomFilterQuery} from './computeDatesCustomFilterQuery';
import {formatCustomLabel} from './formatCustomLabel';
import {validateAndFormatTime} from './validateAndFormatTime';
import {validateDate} from './validateDate';
import translate from '../translate';
import {setFilterValid, updateQuery} from '../state/actions';
import {getParamsFromQuery} from '../utils/query-modifiers';
import {FilterType} from '../components/date-and-time-picker/types';

type QuickFilterQuery = {
  id: string;
  label: string;
  value: string;
};

type CustomFilterQueryValue = {
  startDate?: moment.Moment;
  endDate?: moment.Moment;
  startTime?: string;
  endTime?: string;
};

const DateAndTimeFilter = ({...rest}) => {
  const {queries, language, activeFilterId} = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const prevValue = useMemo(
    () => getParamsFromQuery(queries, activeFilterId),
    [queries, activeFilterId]
  );

  const defaultQuickOption =
    (prevValue?.label && camelCase(prevValue.label)) || '';
  const [quickSelectedOption, setQuickSelectedOption] =
    useState(defaultQuickOption);
  const [customSelectedValue, setCustomSelectedValue] = useState(
    prevValue?.value as CustomFilterQueryValue
  );

  const [hasStartTimeError, setHasStartTimeError] = useState(false);
  const [startTimeErrorText, setStartTimeErrorText] = useState('');
  const [hasEndTimeError, setHasEndTimeError] = useState(false);
  const [endTimeErrorText, setEndTimeErrorText] = useState('');
  const [hasStartDateError, setHasStartDateError] = useState<DateValidState>(
    DateValidState.NOT_FILLED
  );
  const [errorStartDateText, setStartErrorDateText] = useState('');
  const [hasEndDateError, setHasEndDateError] = useState<DateValidState>(
    DateValidState.NOT_FILLED
  );
  const [errorEndDateText, setEndErrorDateText] = useState('');

  const computeCustomStartDates = useCallback(
    (startDate?: moment.Moment, startTime?: string) => {
      const {hasTimeError, errorTimeText, time} = validateAndFormatTime(
        language
      )(startDate, startTime);
      const hasErrorDate = validateDate(startDate);
      setStartTimeErrorText(errorTimeText);

      if (hasTimeError) {
        setHasStartTimeError(true);
        return;
      }

      setHasStartDateError(hasErrorDate);
      if (hasErrorDate === DateValidState.INVALID) {
        translate({language, key: 'dateAndTimeFilter.validation.invalidDate'});
        return;
      }

      setHasStartTimeError(false);
      setStartErrorDateText('');

      if (startDate) {
        const newCustomStartDate =
          time === ''
            ? moment(startDate.hours(0).minutes(0).seconds(0).milliseconds(0))
            : moment(
                startDate
                  .hours((time as moment.Moment).hour())
                  .minutes((time as moment.Moment).minute())
                  .seconds(0)
                  .milliseconds(0)
              );
        return newCustomStartDate;
      }
    },
    [language]
  );

  const computeCustomEndDates = useCallback(
    (endDate?: moment.Moment, endTime?: string) => {
      const {hasTimeError, errorTimeText, time} = validateAndFormatTime(
        language
      )(endDate, endTime);
      const hasErrorDate = validateDate(endDate);
      setEndTimeErrorText(errorTimeText);

      if (hasTimeError) {
        setHasEndTimeError(true);
        return;
      }

      setHasEndDateError(hasErrorDate);
      if (hasErrorDate === DateValidState.INVALID) {
        setEndErrorDateText(
          translate({language, key: 'dateAndTimeFilter.validation.invalidDate'})
        );
        return;
      }

      setHasEndTimeError(false);
      setEndErrorDateText('');

      if (endDate) {
        const newCustomEndDate =
          time === ''
            ? moment(endDate.hours(0).minutes(0).seconds(0).milliseconds(0))
            : moment(
                endDate
                  .hours((time as moment.Moment).hour())
                  .minutes((time as moment.Moment).minute())
                  .seconds(0)
                  .milliseconds(0)
              );
        return newCustomEndDate;
      }
    },
    [language]
  );

  const handleQuickValueChange = useCallback(
    (filterQuery: QuickFilterQuery) => {
      setQuickSelectedOption(filterQuery.value);
      dispatch(
        updateQuery({
          id: filterQuery.id,
          label: filterQuery.label,
          value: '',
          ...computeDatesQuickFilterQuery(filterQuery.value),
        })
      );
    },
    [dispatch]
  );

  const handleCustomValueChange = useCallback(
    (id: string, filterQuery: CustomFilterQueryValue) => {
      setCustomSelectedValue({
        startDate: filterQuery.startDate,
        endDate: filterQuery.endDate,
        startTime: filterQuery.startTime,
        endTime: filterQuery.endTime,
      });

      const customStartDate = computeCustomStartDates(
        filterQuery.startDate,
        filterQuery.startTime
      );
      const customEndDate = computeCustomEndDates(
        filterQuery.endDate,
        filterQuery.endTime
      );
      dispatch(
        updateQuery({
          id,
          label: formatCustomLabel(language)(customStartDate, customEndDate),
          ...computeDateCustomFilterQuery(customStartDate, customEndDate),
        })
      );
    },
    [computeCustomStartDates, dispatch, language, computeCustomEndDates]
  );

  useEffect(() => {
    if (
      hasStartDateError === DateValidState.INVALID ||
      hasStartTimeError ||
      hasEndDateError === DateValidState.INVALID ||
      hasEndTimeError ||
      (hasStartDateError === DateValidState.NOT_FILLED &&
        hasEndDateError === DateValidState.NOT_FILLED)
    ) {
      dispatch(setFilterValid(false));
    } else {
      dispatch(setFilterValid(true));
    }
  }, [
    hasStartTimeError,
    hasEndTimeError,
    hasEndDateError,
    hasStartDateError,
    dispatch,
  ]);

  useEffect(() => {
    if (quickSelectedOption) {
      dispatch(setFilterValid(true));
    }
  }, [quickSelectedOption, dispatch]);

  return (
    <div
      data-date-and-time-filter=""
      data-testid="date-and-time-filter"
      {...extractDataAriaIdProps(rest)}>
      <DateAndTimePicker
        id={activeFilterId}
        onQuickValueChange={handleQuickValueChange}
        onCustomValueChange={handleCustomValueChange}
        quickSelectedOption={quickSelectedOption}
        customSelectedOption={customSelectedValue}
        quickFilterOptions={predefinedQuickDateOptions(language)}
        defaultFilterType={FilterType.Quick}
        hasStartTimeError={hasStartTimeError}
        startTimeErrorText={startTimeErrorText}
        hasEndTimeError={hasEndTimeError}
        endTimeErrorText={endTimeErrorText}
        hasStartDateError={hasStartDateError === DateValidState.INVALID}
        errorStartDateText={errorStartDateText}
        hasEndDateError={hasEndDateError === DateValidState.INVALID}
        errorEndDateText={errorEndDateText}
      />
    </div>
  );
};

export default DateAndTimeFilter;
