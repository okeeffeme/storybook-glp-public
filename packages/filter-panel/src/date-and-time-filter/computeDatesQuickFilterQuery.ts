import moment from 'moment';

import {DateOptions} from './constants';
import {DateAndTimeFilterValue} from '../components/date-picker/types';

type ComputeDatesQuickFilterQuery = {
  operator: string;
  value: DateAndTimeFilterValue;
};
export const computeDatesQuickFilterQuery = (
  optionId: string
): ComputeDatesQuickFilterQuery | undefined => {
  if (optionId === DateOptions.LastHour) {
    return {
      operator: 'ge',
      value: {
        ge: new Date(Date.parse(moment().subtract(1, 'hours').format())),
      },
    };
  }

  if (optionId === DateOptions.Today) {
    return {
      operator: 'ge',
      value: {
        ge: new Date(
          Date.parse(
            moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format()
          )
        ),
      },
    };
  }

  if (optionId === DateOptions.Yesterday) {
    return {
      operator: 'and',
      value: {
        lt: new Date(
          Date.parse(
            moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format()
          )
        ),
        ge: new Date(
          Date.parse(
            moment(moment().format('YYYY-MM-DD') + ' 00:00:00')
              .subtract(1, 'days')
              .format()
          )
        ),
      },
    };
  }

  if (optionId === DateOptions.LastWeek) {
    return {
      operator: 'ge',
      value: {
        ge: new Date(Date.parse(moment().subtract(7, 'days').format())),
      },
    };
  }

  if (optionId === DateOptions.Last30Days) {
    return {
      operator: 'ge',
      value: {
        ge: new Date(Date.parse(moment().subtract(30, 'days').format())),
      },
    };
  }
};
