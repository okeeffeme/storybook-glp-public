import moment from 'moment';
import {DateAndTimeFilterValue} from '../components/date-picker/types';

export const computeDateCustomFilterQuery = (
  startDate?: moment.Moment,
  endDate?: moment.Moment
): {operator: string; value: DateAndTimeFilterValue} => {
  if (startDate && !endDate) {
    return {
      operator: 'ge',
      value: {
        ge: new Date(Date.parse(moment(startDate).format())),
      },
    };
  }

  if (!startDate && endDate) {
    return {
      operator: 'lt',
      value: {
        lt: new Date(Date.parse(moment(endDate).format())),
      },
    };
  }

  if (startDate && endDate) {
    return {
      operator: 'and',
      value: {
        lt: endDate && new Date(Date.parse(endDate.format())),
        ge: startDate && new Date(Date.parse(startDate.format())),
      },
    };
  }

  return {
    operator: 'lt',
    value: {
      lt: new Date(Date.parse(moment().format())),
    },
  };
};
