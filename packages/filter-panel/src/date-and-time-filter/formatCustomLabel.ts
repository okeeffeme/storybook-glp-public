import moment from 'moment';
import {DefaultDateFormat} from './constants';
import translate from './../translate';
import {Language} from './../types';

export const formatCustomLabel = (language: Language = Language.EN) => (
  startDate?: moment.Moment,
  endDate?: moment.Moment
) => {
  return `${translate({
    language,
    key: 'dateAndTimeFilter.customLabel.inRange',
  })} ${
    startDate
      ? startDate.format(DefaultDateFormat.DateTime)
      : `${translate({
          language,
          key: 'dateAndTimeFilter.customLabel.noStartDate',
        })}`
  } - ${
    endDate
      ? endDate.format(DefaultDateFormat.DateTime)
      : `${translate({
          language,
          key: 'dateAndTimeFilter.customLabel.noEndDate',
        })}`
  }`;
};
