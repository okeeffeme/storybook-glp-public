import {Language} from './../types';
import translate from './../translate';

export type QuickDateOptions = {
  id: string;
  label: string;
  value: string;
};

export const predefinedQuickDateOptions = (
  language: Language
): QuickDateOptions[] => [
  {
    id: 'last30Days',
    label: translate({
      language,
      key: 'dateAndTimeFilter.quickOptions.last30Days',
    }),
    value: 'last30Days',
  },
  {
    id: 'lastWeek',
    label: translate({
      language,
      key: 'dateAndTimeFilter.quickOptions.lastWeek',
    }),
    value: 'lastWeek',
  },
  {
    id: 'yesterday',
    label: translate({
      language,
      key: 'dateAndTimeFilter.quickOptions.yesterday',
    }),
    value: 'yesterday',
  },
  {
    id: 'today',
    label: translate({language, key: 'dateAndTimeFilter.quickOptions.today'}),
    value: 'today',
  },
  {
    id: 'lastHour',
    label: translate({
      language,
      key: 'dateAndTimeFilter.quickOptions.lastHour',
    }),
    value: 'lastHour',
  },
];

export enum DateOptions {
  Last30Days = 'last30Days',
  LastWeek = 'lastWeek',
  Yesterday = 'yesterday',
  Today = 'today',
  LastHour = 'lastHour',
}

export enum DefaultDateFormat {
  Date = 'YYYY-MM-DD',
  DateTime = 'YYYY-MM-DD HH:mm:ss',
}

export enum DateValidState {
  NOT_FILLED,
  VALID,
  INVALID,
}
