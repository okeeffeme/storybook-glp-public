import * as actionTypes from './action-types';
import {FilterPanelState} from '../types';
import Action from './action-types';

export const initialState: FilterPanelState = {
  isActive: false,
  header: '',
  query: {id: '', operator: '', value: ''},
  isFilterValid: false,
  optionsById: {},
};

const reducer = (state: FilterPanelState, action: Action): FilterPanelState => {
  switch (action.type) {
    case actionTypes.SET_IS_FILTER_VALID: {
      return {
        ...state,
        isFilterValid: action.payload,
      };
    }
    case actionTypes.SET_FILTER: {
      if (!action.payload) {
        return {
          ...state,
          header: initialState.header,
        };
      }
      return {
        ...state,
        header: action.payload.header,
      };
    }
    case actionTypes.TOGGLE_FILTER_PANEL: {
      if (!action.payload) {
        return {
          ...state,
          isActive: initialState.isActive,
          header: initialState.header,
          query: initialState.query,
        };
      }
      return {
        ...state,
        isActive: true,
      };
    }
    case actionTypes.UPDATE_QUERY: {
      if (!action.payload) {
        return {
          ...state,
          query: initialState.query,
        };
      }
      return {
        ...state,
        query: action.payload,
      };
    }

    case actionTypes.ADD_OPTION: {
      const current = state.optionsById[action.payload.id] ?? [];
      return {
        ...state,
        optionsById: {
          ...state.optionsById,
          [action.payload.id]: [...current, action.payload.value],
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
