import {Query, SetFilterArgs} from '../types';

export const TOGGLE_FILTER_PANEL = 'TOGGLE_FILTER_PANEL';
export const UPDATE_QUERY = 'UPDATE_QUERY';
export const SET_FILTER = 'SET_FILTER';
export const SET_PILLS = 'SET_PILLS';
export const SET_IS_FILTER_VALID = 'SET_IS_FILTER_VALID';
export const ADD_OPTION = 'ADD_OPTION';
export const SET_ACTIVE_FILTER = 'SET_ACTIVE_FILTER';

export type SetPillsPayload = {id: string; text: string};

type SetFilter = {
  type: typeof SET_FILTER;
  payload?: SetFilterArgs;
};

type ToggleFilterPanel = {
  type: typeof TOGGLE_FILTER_PANEL;
  payload: boolean;
};

type UpdateQuery = {
  type: typeof UPDATE_QUERY;
  payload?: Query;
};

type SetFilteredValid = {
  type: typeof SET_IS_FILTER_VALID;
  payload: boolean;
};

type AddOption = {
  type: typeof ADD_OPTION;
  payload: {
    id: string;
    value: string;
  };
};

type SetActiveFilter = {
  type: typeof SET_ACTIVE_FILTER;
  payload: string;
};

type Action =
  | SetFilter
  | UpdateQuery
  | SetFilteredValid
  | ToggleFilterPanel
  | SetActiveFilter
  | AddOption;

export default Action;
