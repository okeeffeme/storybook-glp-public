import {Query, SetFilterArgs} from '../types';
import Action, {
  ADD_OPTION,
  SET_FILTER,
  UPDATE_QUERY,
  SET_IS_FILTER_VALID,
  TOGGLE_FILTER_PANEL,
} from './action-types';

export const setFilter = (payload?: SetFilterArgs): Action => ({
  type: SET_FILTER,
  payload,
});

export const updateQuery = (payload?: Query): Action => ({
  type: UPDATE_QUERY,
  payload,
});

export const toggleFilterPanel = (payload: boolean): Action => ({
  type: TOGGLE_FILTER_PANEL,
  payload,
});

export const setFilterValid = (payload: boolean): Action => ({
  type: SET_IS_FILTER_VALID,
  payload,
});

export const addOption = (id: string, value: string): Action => ({
  type: ADD_OPTION,
  payload: {
    id,
    value,
  },
});
