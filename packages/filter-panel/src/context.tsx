import React from 'react';
import {FilterContextType, Language} from './types';
import Action from './state/action-types';

const FilterContext = React.createContext<FilterContextType>({
  filters: [],
  queries: [],
  language: Language.EN,
  activeFilterId: '',
  isFilterValid: false,
  onDeleteAll: () => {},
  onDelete: () => {},
  onApplyFilter: () => {},
  onTogglePanel: () => {},
  onChangeActiveFilterId: () => {},
  optionsById: {},
  isActive: false,
  header: '',
});

export const FilterDispatchContext = React.createContext<
  React.Dispatch<Action>
>(() => {});
export default FilterContext;
