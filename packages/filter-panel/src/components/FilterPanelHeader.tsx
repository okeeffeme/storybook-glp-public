import {filter, Icon} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';

import ToggleButton from '@jotunheim/react-toggle-button';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import cn from 'classnames';
import React, {useContext} from 'react';
import FilterContext from '../context';
import translate from '../translate';

import classNames from './FilterPanelHeader.module.css';

import FilterPills from './FilterPills';

const {block} = bemFactory('comd-filter-panel-header', classNames);

interface FilterPanelHeaderProps {
  className?: string | undefined;
}

const FilterPanelHeader: React.FC<FilterPanelHeaderProps> = ({
  className,
  children,
  ...props
}) => {
  const {filters, language, onTogglePanel, isActive} =
    useContext(FilterContext);

  if (!filters) {
    return null;
  }

  const tooltipText = isActive
    ? translate({language, key: 'panel.header.iconTooltipClose'})
    : translate({language, key: 'panel.header.iconTooltipOpen'});

  return (
    <div
      className={cn(block(), className)}
      {...extractDataAriaIdProps(props)}
      data-filter-panel-toolbar="">
      <ToggleButton
        onChange={onTogglePanel}
        tooltipText={tooltipText}
        data-testid="filter-panel-visibility"
        id="toggle-filter-panel-visibility"
        checked={isActive}
        data-filter-panel-toggle=""
        icon={<Icon path={filter} />}
      />
      <FilterPills />
      <div>{children}</div>
    </div>
  );
};

export default FilterPanelHeader;
