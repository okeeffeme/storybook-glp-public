import moment, {Moment} from 'moment';
import {DateFilterValue, PredefinedDateFilterValue} from './valueTypes';
import {OptionView} from './usePredifinedOptions/usePredefinedOptions';

export * from './valueTypes';

export type LocaleMoment = typeof moment;

export enum DateInterval {
  Day = 'day',
  Week = 'week',
  Month = 'month',
  Quarter = 'quarter',
  Year = 'year',
}

export interface FiscalCalendarMonth {
  start: string;
  finish: string;
}
export interface FiscalCalendarYear {
  number: number;
  months: FiscalCalendarMonth[];
}
export interface FiscalCalendar {
  firstCalendarMonth: number;
  years: FiscalCalendarYear[];
}

export interface CalendarPeriod {
  year: number;
  periodValue: number;
  start: Moment;
  finish: Moment;
}

export type ViewProps<
  TFilterValue extends DateFilterValue = PredefinedDateFilterValue,
  TOption extends OptionConfig = OptionConfig
> = {
  option: TOption;
  disabled: boolean;
  selectedValue?: TFilterValue;
  calendarConfig?: CalendarConfig;
  onChangeHandler: (value?: Omit<TFilterValue, 'optionId'>) => void;
};

interface BasicOptionConfig {
  id: string;
  groupId: string;
  useFiscalCalendar?: boolean;
  period: 'absolute' | 'relative' | 'custom';
}

export interface RadioOptionConfig extends BasicOptionConfig {
  label: string;
  period: 'relative';
  dateInterval: DateInterval;
}

export interface PreviousOptionConfig extends BasicOptionConfig {
  label: string;
  viewType: 'inputField';
  suffix: string;
  period: 'relative';
  dateInterval: DateInterval;
}

export interface AbsolutePeriodOptionConfig extends BasicOptionConfig {
  label: string;
  viewType: 'select';
  period: 'absolute';
  dateInterval: DateInterval;
}

export interface DateRangePickerOptionConfig extends BasicOptionConfig {
  id: string;
  groupId: string;
  period: 'custom';
}

export type OptionConfig =
  | RadioOptionConfig
  | AbsolutePeriodOptionConfig
  | PreviousOptionConfig
  | DateRangePickerOptionConfig;

export const isAbsolutePeriodOptionConfig = (
  config: OptionConfig
): config is AbsolutePeriodOptionConfig =>
  (config as AbsolutePeriodOptionConfig).viewType === 'select';

export const isPreviousOptionConfig = (
  config: OptionConfig
): config is PreviousOptionConfig =>
  (config as PreviousOptionConfig).viewType === 'inputField';

export const isDateRangeOptionConfig = (
  config: OptionConfig
): config is DateRangePickerOptionConfig => config.period === 'custom';

export type Option = OptionConfig & {
  view: OptionView;
};

export type DateFilterGroup = {
  id: string;
  label: string;
  options?: OptionConfig[];
};

export type CustomOption = {
  id: string;
  label: string;
};

export type SelectorItem = {
  label: string | number;
  value: string | number;
};

export type DatePartSelectData = {
  id: string;
  data: SelectorItem[];
  styleModifier: string;
  getValue: (value?: DateFilterValue) => string | number;
};

export type DateAndTimeFilterValue = {
  lt?: Date;
  ge?: Date;
};

export enum WeekRule {
  firstDay = 'firstDay',
  firstFullWeek = 'firstFullWeek',
  firstFourDayWeek = 'firstFourDayWeek',
}

export enum WeekStart {
  monday = 'monday',
  tuesday = 'tuesday',
  wednesday = 'wednesday',
  thursday = 'thursday',
  friday = 'friday',
  saturday = 'saturday',
  sunday = 'sunday',
}

export type CalendarConfig = {
  weekStart?: WeekStart;
  weekRule?: WeekRule;
  fiscalCalendar?: FiscalCalendar;
};

export type OptionFilterPredicate = (option: OptionConfig) => boolean;
