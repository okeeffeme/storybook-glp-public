import React from 'react';
import {Select} from '@jotunheim/react-select';
import translate from '../../translate';
import {Language} from '../../index';
import {DateFilterGroup} from './types';

type GroupSelectProps = {
  label?: string;
  groupId?: string;
  groups: DateFilterGroup[];
  onGroupChange: (group: DateFilterGroup) => void;
  onClear: () => void;
  isClearable: boolean;
  disabled: boolean;
  placeholder?: string;
  language: Language;
};

export const GroupSelect = ({
  label,
  groupId,
  groups,
  onGroupChange,
  onClear,
  isClearable,
  disabled,
  placeholder,
  language,
}: GroupSelectProps) => {
  const handleGroupChange = (groupId?: string) => {
    if (groupId == null) {
      onClear();
    } else {
      const group = groups.find((x) => x.id === groupId);
      if (group) onGroupChange(group);
    }
  };

  return (
    <div data-test-id="reporting-period-select">
      <Select
        label={label || translate({key: 'dateFilter.groupSelect', language})}
        onChange={handleGroupChange}
        value={groupId}
        isClearable={isClearable}
        placeholder={placeholder}
        isSearchable={false}
        disabled={disabled}
        options={groups.map((group) => ({value: group.id, label: group.label}))}
      />
    </div>
  );
};
