import {LocaleSpecification} from 'moment';
import {CalendarConfig} from '../types';

const weekDayNumber = {
  monday: 1,
  tuesday: 2,
  wednesday: 3,
  thursday: 4,
  friday: 5,
  saturday: 6,
  sunday: 0,
};
const weekRuleOffset = {
  firstday: 1,
  firstfourdayweek: 4,
  firstfullweek: 7,
};

const getWeekDayNumber = (localeValue: number, weekDay?: string): number => {
  if (!weekDay) return localeValue;
  const n = weekDayNumber[weekDay.toLowerCase()];
  return n !== undefined ? n : localeValue;
};

const getFirstWeekValue = (
  localeValue: number,
  dow: number,
  weekRule?: string
): number => {
  const n = weekRule
    ? weekRuleOffset[weekRule.toLowerCase()] || localeValue
    : localeValue;
  return 7 + dow - n;
};

export const buildLocaleSpecFromCalendarConfig = (
  params: CalendarConfig
): LocaleSpecification => {
  const {weekStart, weekRule} = params;
  const localeDow = 1;
  const localeWeekOffset = 1;

  const dow = getWeekDayNumber(localeDow, weekStart);
  const doy = getFirstWeekValue(localeWeekOffset, dow, weekRule);
  return {week: {dow, doy}};
};
