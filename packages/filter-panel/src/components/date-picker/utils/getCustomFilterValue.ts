import moment from 'moment';
import {CustomRangeFilterValue} from '../valueTypes';
import {defaultDateFormat, localDateTimeFormat} from '../constants';
import translate from '../../../translate';
import {Language} from '../../../types';

export const getCustomFilterValue =
  (language: Language) =>
  (
    newStartDate: moment.Moment | null,
    newEndDate: moment.Moment | null
  ): CustomRangeFilterValue => {
    const startDate =
      newStartDate &&
      moment(newStartDate.hours(0).minutes(0).seconds(0).milliseconds(0));
    const endDate =
      newEndDate &&
      moment(newEndDate.hours(23).minutes(59).seconds(59).milliseconds(998));

    return {
      startDate: startDate ? startDate.format(localDateTimeFormat) : undefined,
      endDate: endDate ? endDate.format(localDateTimeFormat) : undefined,
      period: 'custom',
      optionId: 'Between',
      optionLabel: `${translate({
        key: 'dateAndTimeFilter.customLabel.inRange',
        language,
      })} ${
        startDate
          ? startDate.format(defaultDateFormat.date)
          : translate({
              key: 'dateAndTimeFilter.customLabel.noStartDate',
              language,
            })
      } - ${
        endDate
          ? endDate.format(defaultDateFormat.date)
          : translate({
              key: 'dateAndTimeFilter.customLabel.noEndDate',
              language,
            })
      }`,
    };
  };
