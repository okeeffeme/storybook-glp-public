import defaultMoment, {LocaleSpecification} from 'moment';
import {LocaleMoment} from '../types';

let counter = 0;
const getUniqueLocaleName = () => `localeMoment-${++counter}`;

export const getLocaleMoment = (
  localeSpecification?: LocaleSpecification
): LocaleMoment => {
  if (!localeSpecification) return defaultMoment;

  const localeName = getUniqueLocaleName();
  const globalLocale = defaultMoment.locale();
  defaultMoment.defineLocale(
    localeName,
    JSON.parse(JSON.stringify(localeSpecification))
  );
  defaultMoment.locale(globalLocale);

  const moment = (...args: Parameters<typeof defaultMoment>) => {
    const momentInstance = defaultMoment(...args);
    momentInstance.locale(localeName);
    return momentInstance;
  };

  return Object.assign(moment, defaultMoment);
};
