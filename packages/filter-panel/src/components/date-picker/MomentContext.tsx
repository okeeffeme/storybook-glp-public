import React from 'react';
import moment from 'moment';

export const MomentContext = React.createContext(moment);
