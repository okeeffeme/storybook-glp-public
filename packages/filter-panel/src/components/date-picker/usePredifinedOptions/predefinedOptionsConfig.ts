import {DateInterval, OptionFilterPredicate, OptionConfig} from '../types';
import {Language} from '../../../index';
import translate from '../../../translate';

export type MakePredefinedOptionsConfigParams = {
  language: Language;
  groupIds: string[];
  optionFilter?: OptionFilterPredicate;
};

export const makePredefinedOptionsConfig = ({
  optionFilter,
  groupIds,
  language,
}: MakePredefinedOptionsConfigParams): OptionConfig[] => {
  const previousLabel = translate({
    key: `dateFilter.quickOptions.previous.label`,
    language,
  });

  const tLabel = (optionKey: string) =>
    translate({key: `dateFilter.quickOptions.${optionKey}`, language});

  const tSuffix = (suffix: string) =>
    translate({key: `dateFilter.quickOptions.previous.${suffix}`, language});

  const options: OptionConfig[] = [
    {
      id: 'today',
      groupId: 'days',
      dateInterval: DateInterval.Day,
      label: tLabel('today'),
      period: 'relative',
    },
    {
      id: 'yesterday',
      groupId: 'days',
      dateInterval: DateInterval.Day,
      label: tLabel('yesterday'),
      period: 'relative',
    },
    {
      id: 'previousdays',
      groupId: 'days',
      label: previousLabel,
      suffix: tSuffix('days'),
      viewType: 'inputField',
      dateInterval: DateInterval.Day,
      period: 'relative',
    },
    {
      id: 'calendarweek',
      groupId: 'weeks',
      label: tLabel('calendarWeek'),
      viewType: 'select',
      dateInterval: DateInterval.Week,
      period: 'absolute',
    },
    {
      id: 'currentweek',
      groupId: 'weeks',
      label: tLabel('currentWeek'),
      dateInterval: DateInterval.Week,
      period: 'relative',
    },
    {
      id: 'previousweeks',
      groupId: 'weeks',
      label: previousLabel,
      suffix: tSuffix('weeks'),
      viewType: 'inputField',
      dateInterval: DateInterval.Week,
      period: 'relative',
    },
    {
      id: 'calendarmonth',
      groupId: 'months',
      label: tLabel('calendarMonth'),
      viewType: 'select',
      dateInterval: DateInterval.Month,
      period: 'absolute',
    },
    {
      id: 'currentmonth',
      groupId: 'months',
      label: tLabel('currentMonth'),
      dateInterval: DateInterval.Month,
      period: 'relative',
    },
    {
      id: 'previousmonths',
      groupId: 'months',
      label: previousLabel,
      suffix: tSuffix('months'),
      viewType: 'inputField',
      dateInterval: DateInterval.Month,
      period: 'relative',
    },
    {
      id: 'calendarquarter',
      groupId: 'quarters',
      label: tLabel('calendarQuarter'),
      viewType: 'select',
      dateInterval: DateInterval.Quarter,
      period: 'absolute',
    },
    {
      id: 'currentquarter',
      groupId: 'quarters',
      label: tLabel('currentQuarter'),
      dateInterval: DateInterval.Quarter,
      period: 'relative',
    },
    {
      id: 'previousquarters',
      groupId: 'quarters',
      label: previousLabel,
      suffix: tSuffix('quarters'),
      viewType: 'inputField',
      dateInterval: DateInterval.Quarter,
      period: 'relative',
    },
    {
      id: 'calendaryear',
      groupId: 'years',
      label: tLabel('calendarYear'),
      viewType: 'select',
      dateInterval: DateInterval.Year,
      period: 'absolute',
    },
    {
      id: 'currentyear',
      groupId: 'years',
      label: tLabel('currentYear'),
      dateInterval: DateInterval.Year,
      period: 'relative',
    },
    {
      id: 'previousyears',
      groupId: 'years',
      label: previousLabel,
      suffix: tSuffix('years'),
      viewType: 'inputField',
      dateInterval: DateInterval.Year,
      period: 'relative',
    },
    {
      id: 'fiscalWeek',
      groupId: 'fiscalWeeks',
      label: tLabel('fiscalWeek'),
      viewType: 'select',
      dateInterval: DateInterval.Week,
      useFiscalCalendar: true,
      period: 'absolute',
    },
    {
      id: 'currentFiscalWeek',
      groupId: 'fiscalWeeks',
      label: tLabel('currentFiscalWeek'),
      useFiscalCalendar: true,
      dateInterval: DateInterval.Week,
      period: 'relative',
    },
    {
      id: 'previousFiscalWeeks',
      groupId: 'fiscalWeeks',
      label: previousLabel,
      suffix: tSuffix('fiscalWeeks'),
      viewType: 'inputField',
      useFiscalCalendar: true,
      dateInterval: DateInterval.Week,
      period: 'relative',
    },
    {
      id: 'fiscalMonth',
      groupId: 'fiscalMonths',
      label: tLabel('fiscalMonth'),
      viewType: 'select',
      dateInterval: DateInterval.Month,
      useFiscalCalendar: true,
      period: 'absolute',
    },
    {
      id: 'currentFiscalMonth',
      groupId: 'fiscalMonths',
      label: tLabel('currentFiscalMonth'),
      useFiscalCalendar: true,
      dateInterval: DateInterval.Month,
      period: 'relative',
    },
    {
      id: 'previousFiscalMonths',
      groupId: 'fiscalMonths',
      label: previousLabel,
      suffix: tSuffix('fiscalMonths'),
      viewType: 'inputField',
      dateInterval: DateInterval.Month,
      useFiscalCalendar: true,
      period: 'relative',
    },
    {
      id: 'fiscalQuarter',
      groupId: 'fiscalQuarters',
      label: tLabel('fiscalQuarter'),
      viewType: 'select',
      dateInterval: DateInterval.Quarter,
      useFiscalCalendar: true,
      period: 'absolute',
    },
    {
      id: 'currentFiscalQuarter',
      groupId: 'fiscalQuarters',
      label: tLabel('currentFiscalQuarter'),
      dateInterval: DateInterval.Quarter,
      useFiscalCalendar: true,
      period: 'relative',
    },
    {
      id: 'previousFiscalQuarters',
      groupId: 'fiscalQuarters',
      label: previousLabel,
      suffix: tSuffix('fiscalQuarters'),
      viewType: 'inputField',
      dateInterval: DateInterval.Quarter,
      useFiscalCalendar: true,
      period: 'relative',
    },
    {
      id: 'fiscalYear',
      groupId: 'fiscalYears',
      label: tLabel('fiscalYear'),
      viewType: 'select',
      dateInterval: DateInterval.Year,
      useFiscalCalendar: true,
      period: 'absolute',
    },
    {
      id: 'currentFiscalYear',
      groupId: 'fiscalYears',
      label: tLabel('currentFiscalYear'),
      dateInterval: DateInterval.Year,
      useFiscalCalendar: true,
      period: 'relative',
    },
    {
      id: 'previousFiscalYears',
      groupId: 'fiscalYears',
      label: previousLabel,
      suffix: tSuffix('fiscalYears'),
      viewType: 'inputField',
      useFiscalCalendar: true,
      dateInterval: DateInterval.Year,
      period: 'relative',
    },
    {
      id: 'Between',
      period: 'custom',
      groupId: 'custom',
    },
  ];

  const validOptions = options.filter((option) => {
    if (!groupIds.includes(option.groupId)) return false;

    return optionFilter ? optionFilter(option) : true;
  });

  return validOptions;
};
