import {useMemo} from 'react';
import {
  isAbsolutePeriodOptionConfig,
  isDateRangeOptionConfig,
  isPreviousOptionConfig,
  OptionConfig,
} from '../types';
import {OptionPreviousPeriod, OptionRadioButton} from '../index';
import {OptionAbsolutePeriod} from '../Options/AbsolutePeriod/OptionAbsolutePeriod';
import {
  makePredefinedOptionsConfig,
  MakePredefinedOptionsConfigParams,
} from './predefinedOptionsConfig';
import CustomRangePicker from '../CustomRangePicker';

export const getOptionView = (optionConfig: OptionConfig) => {
  if (isPreviousOptionConfig(optionConfig)) {
    return OptionPreviousPeriod;
  }
  if (isAbsolutePeriodOptionConfig(optionConfig)) {
    return OptionAbsolutePeriod;
  }
  if (isDateRangeOptionConfig(optionConfig)) {
    return CustomRangePicker;
  }
  return OptionRadioButton;
};

export type OptionView = ReturnType<typeof getOptionView>;

export const usePredefinedOptions = (
  params: MakePredefinedOptionsConfigParams
) => {
  const predefinedOptions = useMemo(() => {
    const optionsConfig = makePredefinedOptionsConfig(params);

    return optionsConfig.map((optionConfig) => {
      return {
        ...optionConfig,
        view: getOptionView(optionConfig),
      };
    });
  }, [params]);

  return predefinedOptions;
};
