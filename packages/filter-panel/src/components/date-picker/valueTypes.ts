import {DateInterval} from './types';

export interface RelativePeriodFilterValue {
  period: 'relative';
  interval: DateInterval;
  offset: number;
  optionId: string;
  startDate?: string;
  endDate?: string;
  optionLabel: string;
  isFiscal?: boolean;
}

export interface AbsolutePeriodValue {
  period: 'absolute';
  year: number;
  interval: DateInterval;
  periodValue: number;
}

export interface AbsolutePeriodFilterValue extends AbsolutePeriodValue {
  optionId: string;
  isFiscal?: boolean;
  startDate?: string;
  endDate?: string;
  optionLabel: string;
}

export interface CustomRangeFilterValue {
  period: 'custom';
  optionId: 'Between';
  optionLabel: string;
  startDate?: string;
  endDate?: string;
}

export type CustomGroupFilterValue = {
  groupId: string;
  optionLabel: string;
};

export type PredefinedDateFilterValue =
  | RelativePeriodFilterValue
  | AbsolutePeriodFilterValue
  | CustomRangeFilterValue;

export type DateFilterValue =
  | PredefinedDateFilterValue
  | CustomGroupFilterValue;

export const isPredefinedOptionValue = (
  value: DateFilterValue | undefined
): value is PredefinedDateFilterValue =>
  (value as PredefinedDateFilterValue)?.optionId != null;

export const isAbsolutePeriodValue = (
  v: DateFilterValue | undefined
): v is AbsolutePeriodFilterValue =>
  isPredefinedOptionValue(v) && v.period === 'absolute';

export const isRelativePeriodValue = (
  v: DateFilterValue | undefined
): v is RelativePeriodFilterValue =>
  isPredefinedOptionValue(v) && v.period === 'relative';

export const isCustomRangePeriodValue = (
  v: DateFilterValue | undefined
): v is CustomRangeFilterValue =>
  isPredefinedOptionValue(v) && v.period === 'custom';

export const isCustomGroupValue = (
  v: DateFilterValue | undefined
): v is CustomGroupFilterValue => v != null && !isPredefinedOptionValue(v);
