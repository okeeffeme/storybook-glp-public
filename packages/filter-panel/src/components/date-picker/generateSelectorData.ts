import moment from 'moment';
import {FiscalCalendar} from './types';

// Extracted out of utils.ts to avoid circular dependency: utils <-> constants

export const generateYearsSelectorData = (yearsCount, formatter = (x) => x) => {
  const startYear = moment().add(1, 'year');

  return Array.from({length: yearsCount}, () => {
    const year = startYear.subtract(1, 'year').year();
    return {
      label: formatter(year),
      value: year,
    };
  });
};

export const generateFiscalYearSelectorData = (
  calendar: FiscalCalendar,
  formatter = (x) => x
) =>
  calendar.years
    .map((year) => ({
      label: formatter(year.number),
      value: year.number,
    }))
    .sort((v1, v2) => v2.value - v1.value);

export const generateQuartersSelectorData = (formatter = (x) => `Q${x}`) => {
  return Array.from({length: 4}, (_, i) => {
    return {
      label: formatter(i + 1),
      value: i + 1,
    };
  });
};
export const generateMonthsSelectorData = () => {
  return Array.from({length: 12}, (_, i) => {
    return {
      label: moment().month(i).format('MMMM'),
      value: i + 1,
    };
  });
};

export const generateWeeksSelectorData = (
  weeksCount: number,
  formatter = (x) => `W${x}`
) => {
  return Array.from({length: weeksCount}, (_, i) => {
    return {
      label: formatter(i + 1),
      value: i + 1,
    };
  });
};
