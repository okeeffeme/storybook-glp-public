import DatePicker from '@jotunheim/react-date-picker';
import moment from 'moment';
import React from 'react';

import {CustomRangeFilterValue} from './types';
import translate from '../../translate';
import FilterHelpText from '../FilterHelpText';

import Fieldset from '@jotunheim/react-fieldset';
import {defaultDateFormat} from './constants';
import {Language} from '../../types';
import {getCustomFilterValue} from './utils/getCustomFilterValue';

type CustomRangePickerProps = {
  selectedValue?: CustomRangeFilterValue;
  onChangeHandler: (value: CustomRangeFilterValue) => void;
  format?: string;
  disabled: boolean;
  language: Language;
};

export const getDateRangeMomentsByValue = (value?: CustomRangeFilterValue) => {
  return {
    endDate: value && value.endDate ? moment(value.endDate) : undefined,
    startDate: value && value.startDate ? moment(value.startDate) : undefined,
  };
};

const CustomRangePicker = ({
  selectedValue,
  onChangeHandler,
  format = defaultDateFormat.date,
  language,
}: CustomRangePickerProps) => {
  const {startDate, endDate} = getDateRangeMomentsByValue(selectedValue);

  const handleStartDateChange = (startDate: moment.Moment | null) => {
    onChangeHandler(getCustomFilterValue(language)(startDate, endDate || null));
  };

  const handleEndDateChange = (endDate: moment.Moment | null) => {
    onChangeHandler(getCustomFilterValue(language)(startDate || null, endDate));
  };

  return (
    <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
      <DatePicker
        id="start-date-picker"
        date={startDate}
        maxDate={endDate}
        format={format}
        onChange={handleStartDateChange}
        showLabel={true}
        label={translate({
          language,
          key: 'dateAndTimeFilter.customPicker.from',
        })}
      />
      <DatePicker
        id="end-date-picker"
        date={endDate}
        minDate={startDate}
        format={format}
        onChange={handleEndDateChange}
        showLabel={true}
        label={translate({language, key: 'dateAndTimeFilter.customPicker.to'})}
      />
      <FilterHelpText>
        {translate({
          language,
          key: 'dateAndTimeFilter.customPicker.filterHint',
        })}
      </FilterHelpText>
    </Fieldset>
  );
};
export default CustomRangePicker;
