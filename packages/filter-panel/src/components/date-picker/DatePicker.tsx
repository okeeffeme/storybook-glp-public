import React, {useState, memo, useMemo} from 'react';

import {
  CustomOption,
  CalendarConfig,
  DateFilterGroup,
  DateFilterValue,
  Option,
  isCustomGroupValue,
  OptionFilterPredicate,
} from './types';
import {GroupSelect} from './GroupSelect';
import {GroupContent} from './GroupContent';
import {usePredefinedOptions} from './usePredifinedOptions/usePredefinedOptions';
import {Language} from '../../index';
import translate from '../../translate';
import {defaultGroupIds, fiscalGroupIds} from './constants';
import {buildLocaleSpecFromCalendarConfig} from './utils/buildLocaleSpecFromCalendarConfig';
import {getLocaleMoment} from './utils/getLocaleMoment';
import {MomentContext} from './MomentContext';

type GetSelectedGroupParams = {
  groups: DateFilterGroup[];
  selectedValue?: DateFilterValue;
};

const getInitialGroupId = ({
  groups,
  selectedValue,
}: GetSelectedGroupParams): string | undefined => {
  if (selectedValue === undefined) return undefined;

  if (isCustomGroupValue(selectedValue)) return selectedValue.groupId;

  const matchingGroup = groups.find((group) =>
    (group.options ?? []).some((option) => option.id === selectedValue.optionId)
  );

  // Custom range selector do not have corresponding option and produces value with id of 'Between'
  if (!matchingGroup && selectedValue?.optionId === 'Between') {
    return 'custom';
  }
  return matchingGroup?.id;
};

const mergeGroups = (
  groupIds: string[],
  customOptions: CustomOption[],
  allOptions: Option[],
  language: Language
): DateFilterGroup[] => [
  ...customOptions,
  ...groupIds.map((groupId) => {
    return {
      id: groupId,
      label: translate({key: `dateFilter.groupOption.${groupId}`, language}),
      options: allOptions.filter((option) => option.groupId === groupId),
    };
  }),
];

type DatePickerProps = {
  label?: string;
  customOptions?: CustomOption[];
  groupPlaceholder?: string;
  selectedValue: DateFilterValue | undefined;
  onValueChange: (value?: DateFilterValue) => void;
  isClearable?: boolean;
  disabled?: boolean;
  // Passing language as prop, because component may be used separately, without DateFilter context.
  language: Language;
  calendarConfig?: CalendarConfig;
  optionFilter?: OptionFilterPredicate;
};

export const DatePicker = memo<DatePickerProps>(
  ({
    label,
    selectedValue,
    onValueChange,
    customOptions = [],
    isClearable = false,
    disabled = false,
    groupPlaceholder,
    language,
    calendarConfig,
    optionFilter,
  }) => {
    const localeMoment = useMemo(() => {
      const config = calendarConfig
        ? buildLocaleSpecFromCalendarConfig(calendarConfig)
        : undefined;
      return getLocaleMoment(config);
    }, [calendarConfig]);

    const groupIds = calendarConfig?.fiscalCalendar
      ? fiscalGroupIds
      : defaultGroupIds;

    const usePredefinedOptionsParams = useMemo(
      () => ({
        language,
        groupIds,
        optionFilter,
      }),
      [groupIds, language, optionFilter]
    );

    const validOptions = usePredefinedOptions(usePredefinedOptionsParams);
    const validGroupIds = groupIds.filter((groupId) =>
      validOptions.some((option) => option.groupId === groupId)
    );

    const groupsWithCustomOptions = mergeGroups(
      validGroupIds,
      customOptions,
      validOptions,
      language
    );

    const initialGroup = getInitialGroupId({
      groups: groupsWithCustomOptions,
      selectedValue,
    });

    const [groupId, setGroupId] = useState(initialGroup);

    const groupOptions = validOptions.filter(
      (option) => option.groupId === groupId
    );

    const handleGroupChange = (group: DateFilterGroup) => {
      setGroupId(group.id);
      const groupOptions = validOptions.filter(
        (option) => option.groupId === group.id
      );
      // We treat only groups without any options as value.
      if (groupOptions.length === 0)
        onValueChange({
          groupId: group.id,
          optionLabel: group.label,
        });
    };

    const handleClear = () => {
      setGroupId(undefined);
      onValueChange();
    };

    return (
      <MomentContext.Provider value={localeMoment}>
        <div data-date-picker={true}>
          <GroupSelect
            label={label}
            disabled={disabled}
            isClearable={isClearable}
            placeholder={groupPlaceholder}
            language={language}
            groups={groupsWithCustomOptions}
            groupId={groupId}
            onGroupChange={handleGroupChange}
            onClear={handleClear}
          />
          {groupId && (
            <GroupContent
              language={language}
              groupId={groupId}
              options={groupOptions}
              disabled={disabled}
              calendarConfig={calendarConfig}
              selectedValue={selectedValue}
              onValueChanged={onValueChange}
            />
          )}
        </div>
      </MomentContext.Provider>
    );
  }
);
