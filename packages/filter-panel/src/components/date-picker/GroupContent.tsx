import React from 'react';

import {CalendarConfig, DateFilterValue, Option} from './types';

import OptionWrapper from './Options/OptionWrapper';
import Fieldset from '@jotunheim/react-fieldset';

import classNames from './GroupContent.module.css';
import {bemFactory} from '@jotunheim/react-themes';
import {Language} from '../../types';

export type GroupContentProps = {
  groupId: string;
  options: Option[];
  selectedValue?: DateFilterValue;
  onValueChanged: (value: DateFilterValue | undefined) => void;
  disabled: boolean;
  language: Language;
  calendarConfig?: CalendarConfig;
};

const {block} = bemFactory('comd-date-quick-filter-group', classNames);

export const GroupContent = ({
  options,
  disabled,
  onValueChanged,
  selectedValue,
  language,
  calendarConfig,
}: GroupContentProps) => {
  if (options.length === 0) return null;

  return (
    <div className={block()} data-test-id="date-group">
      <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
        {options.map((option) => (
          <OptionWrapper
            key={option.id}
            option={option}
            selectedValue={selectedValue}
            onValueChanged={onValueChanged}
            disabled={disabled}
            calendarConfig={calendarConfig}
            language={language}
          />
        ))}
      </Fieldset>
    </div>
  );
};
