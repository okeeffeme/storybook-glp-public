import React, {useContext, useMemo} from 'react';

import {getFiscalWeekSelectorData, getYearSelectorData} from '../../constants';
import {
  AbsolutePeriodOptionConfig,
  ViewProps,
  CalendarConfig,
  LocaleMoment,
  AbsolutePeriodFilterValue,
  DateInterval,
} from '../../types';
import {AbsolutePeriodValue} from '../../valueTypes';
import OptionSelect from './OptionSelect';
import {getStartEndDates} from './getStartEndDates';
import {MomentContext} from '../../MomentContext';

const getDateFilterValue = (
  option: AbsolutePeriodOptionConfig,
  value: AbsolutePeriodValue,
  moment: LocaleMoment,
  calendarConfig?: CalendarConfig
) => ({
  ...value,
  ...getStartEndDates(value, moment, calendarConfig),
  optionLabel: `${option.label} ${value.periodValue} ${value.year}`,
});

const OptionFiscalWeekSelect = ({
  selectedValue,
  option,
  disabled,
  onChangeHandler,
  calendarConfig,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const moment = useContext(MomentContext);
  const selectors = useMemo(() => {
    if (!calendarConfig) throw new Error('Fiscal calendar config is required');

    const yearsData = getYearSelectorData({moment, calendarConfig});
    return [
      getFiscalWeekSelectorData({
        moment,
        calendarConfig,
        year: selectedValue?.year ?? Number(yearsData.getValue()),
      }),
      yearsData,
    ];
  }, [calendarConfig, moment, selectedValue?.year]);

  const [weekData, yearsData] = selectors;

  const selected: AbsolutePeriodValue = selectedValue
    ? selectedValue
    : {
        period: 'absolute',
        interval: DateInterval.Week,
        year: Number(yearsData.getValue()),
        periodValue: Number(weekData.getValue()),
      };

  const buildHandleSelectChange = (selectorId: string) => (value: string) => {
    const newSelected = {...selected};

    if (selectorId === 'year') {
      newSelected.year = Number(value);
    }

    if (selectorId === 'week') {
      newSelected.periodValue = Number(value);
    }

    onChangeHandler(
      getDateFilterValue(option, newSelected, moment, calendarConfig)
    );
  };

  const handleFocus = () => {
    if (disabled) return;

    onChangeHandler(
      getDateFilterValue(option, selected, moment, calendarConfig)
    );
  };
  return (
    <OptionSelect
      disabled={disabled}
      selectors={selectors}
      selectedValue={selectedValue}
      option={option}
      handleFocus={handleFocus}
      buildHandleSelectChange={buildHandleSelectChange}
      calendarConfig={calendarConfig}
    />
  );
};

export default OptionFiscalWeekSelect;
