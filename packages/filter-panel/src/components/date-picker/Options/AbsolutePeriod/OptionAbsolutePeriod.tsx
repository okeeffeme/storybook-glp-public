import {
  AbsolutePeriodFilterValue,
  AbsolutePeriodOptionConfig,
  ViewProps,
} from '../../types';
import OptionFiscalWeekSelect from './OptionFiscalWeekSelect';
import React from 'react';
import {
  OptionMonthSelect,
  OptionQuarterSelect,
  OptionWeekSelect,
  OptionYearSelect,
} from '../../index';

const absolutePeriodOptionComponentByDateInterval = {
  month: OptionMonthSelect,
  year: OptionYearSelect,
  quarter: OptionQuarterSelect,
  week: OptionWeekSelect,
};
export const OptionAbsolutePeriod = ({
  onChangeHandler,
  selectedValue,
  option,
  disabled,
  calendarConfig,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const Component =
    option.id === 'fiscalWeek'
      ? OptionFiscalWeekSelect
      : absolutePeriodOptionComponentByDateInterval[option.dateInterval];

  return (
    <Component
      onChangeHandler={onChangeHandler}
      option={option}
      label={option.label}
      selectedValue={selectedValue}
      calendarConfig={calendarConfig}
      disabled={disabled}
    />
  );
};
