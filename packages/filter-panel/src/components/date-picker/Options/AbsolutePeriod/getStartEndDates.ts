import {AbsolutePeriodValue} from '../../valueTypes';
import {CalendarConfig, DateInterval, LocaleMoment} from '../../types';
import {localDateTimeFormat} from '../../constants';
import {getFiscalPeriods} from '../../fiscalPeriods';

export const getStartEndDates = (
  value: AbsolutePeriodValue,
  moment: LocaleMoment,
  calendar?: CalendarConfig
) => {
  const periods = calendar?.fiscalCalendar
    ? getFiscalPeriods(
        calendar.fiscalCalendar,
        value.interval,
        moment,
        value.year
      )
    : [];
  const period = periods.find((x) =>
    value.interval === 'year'
      ? x.year === value.year
      : x.periodValue === value.periodValue
  );
  if (period) {
    return {
      startDate: period.start.format(localDateTimeFormat),
      endDate: period.finish.format(localDateTimeFormat),
    };
  }

  let baseDate: moment.Moment;
  switch (value.interval) {
    case DateInterval.Week:
    case DateInterval.Quarter:
      baseDate = moment({year: value.year})
        .add(value.periodValue - 1, value.interval)
        .startOf(value.interval);
      break;
    case DateInterval.Month:
      baseDate = moment.utc({
        day: 1,
        // Our selector use 1-12 representation for months, but moment is using 0-11, adjusting for that
        month: value.periodValue - 1,
        year: value.year,
      });
      break;
    default:
      baseDate = moment.utc({
        day: 1,
        [value.interval]: value.periodValue,
        year: value.year,
      });
  }

  return {
    startDate: baseDate.format(localDateTimeFormat),
    endDate: baseDate.add(1, value.interval).format(localDateTimeFormat),
  };
};
