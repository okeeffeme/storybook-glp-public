import Calendar, {withMoment} from '@jotunheim/react-calendar';
import Popover from '@jotunheim/react-popover';
import TextField from '@jotunheim/react-text-field';
import {Radio} from '@jotunheim/react-toggle';
import noop from 'lodash/noop';
import React, {useContext, useState} from 'react';
import cn from 'classnames';
import Icon, {menuDown} from '@jotunheim/react-icons';
import {PlacementTypes} from '@jotunheim/react-portal-trigger';
import classNames from './OptionSelect.module.css';
import {bemFactory} from '@jotunheim/react-themes';
import {AbsolutePeriodFilterValue} from '../../valueTypes';
import {
  AbsolutePeriodOptionConfig,
  DateInterval,
  LocaleMoment,
  ViewProps,
} from '../../types';
import {localDateTimeFormat} from '../../constants';
import {MomentContext} from '../../MomentContext';
import {getStartEndDates} from './getStartEndDates';

const popoverConfig = {
  portalZIndex: 1,
  className: 'popover',
  contentClassName: 'popover-content',
  open: false,
  noArrow: true,
  closeOnTriggerClick: false,
  closeOnOutsideClick: true,
  placement: PlacementTypes.topStart,
};

const endOfWeek = (date: moment.Moment) =>
  date.hours(23).minutes(59).seconds(59).milliseconds(998).endOf('week');

const startOfWeek = (date: moment.Moment) =>
  date.hours(0).minutes(0).seconds(0).milliseconds(0).startOf('week');

const {block, element} = bemFactory('comd-date-option-select', classNames);

const getStartOfWeekMoment = (
  moment: LocaleMoment,
  value?: AbsolutePeriodFilterValue
): moment.Moment =>
  value
    ? moment(
        getStartEndDates(
          {
            period: 'absolute',
            interval: DateInterval.Week,
            year: value.year,
            periodValue: value.periodValue,
          },
          moment
        ).startDate
      )
    : startOfWeek(moment());

const MomentAwareCalendar = withMoment(Calendar);

const OptionWeekSelect = ({
  selectedValue,
  option,
  onChangeHandler,
  disabled,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const moment = useContext(MomentContext);

  const [show, setShow] = useState(false);

  const startOfWeekMoment = getStartOfWeekMoment(moment, selectedValue);

  const selectedDateRange = {
    start: startOfWeekMoment.toDate(),
    end: moment(startOfWeekMoment.endOf('week')).toDate(),
  };

  const handleToggle = () => {
    setShow(true);
  };

  const handleChange = (selectedDate: moment.Moment) => {
    const year = selectedDate.year();
    const week = selectedDate.week();
    const baseDate = selectedDate.startOf('week');
    const optionLabel = `${option.label} ${week} ${year}`;
    onChangeHandler({
      optionLabel,
      period: 'absolute',
      interval: DateInterval.Week,
      periodValue: week,
      year,
      startDate: baseDate.format(localDateTimeFormat),
      endDate: baseDate.add(1, 'week').format(localDateTimeFormat),
    });
  };

  const handleSelectChange = (selectedDate: Date) => {
    setShow(false);
    const selectedMoment = endOfWeek(moment(selectedDate));
    handleChange(selectedMoment);
  };

  const handleFocus = () => {
    handleChange(startOfWeekMoment);
  };

  return (
    <div className={block()}>
      <Radio
        value={option.id}
        checked={!!selectedValue}
        onChange={handleFocus}
        disabled={disabled}>
        {option.label}
      </Radio>
      <div tabIndex={0}>
        <Popover
          {...popoverConfig}
          open={show}
          hasPadding={false}
          content={
            <MomentAwareCalendar
              includeWeeks={true}
              moment={moment}
              onSelect={handleSelectChange}
              date={startOfWeekMoment}
              selectedRange={selectedDateRange}
              disabled={disabled}
            />
          }>
          <div className={element('value')} onClick={handleToggle}>
            <div
              className={cn(element('selector'), element('selector', 'week'))}>
              <TextField
                onChange={noop}
                value={startOfWeekMoment.week()}
                suffix={<Icon path={menuDown} />}
                disabled={disabled}
              />
            </div>
            <div
              className={cn(element('selector'), element('selector', 'year'))}>
              <TextField
                onChange={noop}
                value={startOfWeekMoment.year()}
                suffix={<Icon path={menuDown} />}
                disabled={disabled}
              />
            </div>
          </div>
        </Popover>
      </div>
    </div>
  );
};

export default OptionWeekSelect;
