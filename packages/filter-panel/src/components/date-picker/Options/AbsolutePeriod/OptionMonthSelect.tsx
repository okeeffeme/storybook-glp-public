import React, {useContext, useMemo} from 'react';

import {getMonthSelectorData, getYearSelectorData} from '../../constants';
import OptionSelect from './OptionSelect';
import {
  AbsolutePeriodOptionConfig,
  ViewProps,
  CalendarConfig,
  AbsolutePeriodFilterValue,
  DateInterval,
} from '../../types';
import {AbsolutePeriodValue} from '../../valueTypes';

import {getStartEndDates} from './getStartEndDates';
import {MomentContext} from '../../MomentContext';

const getMonthNameByNumber = (month: number, monthsData) => {
  const {label = ''} = monthsData.data.find((x) => x.value === month);
  return label;
};

const OptionMonthSelect = ({
  selectedValue,
  option,
  onChangeHandler,
  disabled,
  calendarConfig,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const moment = useContext(MomentContext);

  const monthSelectorData = useMemo(
    () => getMonthSelectorData({calendarConfig, moment}),
    [calendarConfig, moment]
  );
  const yearSelectorData = useMemo(
    () => getYearSelectorData({calendarConfig, moment}),
    [calendarConfig, moment]
  );

  const selected: AbsolutePeriodValue = selectedValue
    ? selectedValue
    : {
        period: 'absolute',
        interval: DateInterval.Month,
        year: Number(yearSelectorData.getValue()),
        periodValue: Number(monthSelectorData.getValue()),
      };

  const getLabel = (
    option: AbsolutePeriodOptionConfig,
    value: AbsolutePeriodValue
  ) => {
    return `${option.label} ${getMonthNameByNumber(
      value.periodValue,
      monthSelectorData
    )} ${value.year}`;
  };

  const getDateFilterValue = (
    option: AbsolutePeriodOptionConfig,
    value: AbsolutePeriodValue,
    calendarConfig?: CalendarConfig
  ) => ({
    ...value,
    ...getStartEndDates(value, moment, calendarConfig),
    optionLabel: getLabel(option, value),
  });

  const handleSelectChange = (selectorId: string) => (value: string) => {
    const newSelected = {...selected};

    if (selectorId === 'year') {
      newSelected.year = Number(value);
    }

    if (selectorId === 'month') {
      newSelected.periodValue = Number(value);
    }
    onChangeHandler(getDateFilterValue(option, newSelected, calendarConfig));
  };

  const handleFocus = () => {
    if (disabled) return;

    onChangeHandler(getDateFilterValue(option, selected, calendarConfig));
  };

  return (
    <OptionSelect
      disabled={disabled}
      selectors={[monthSelectorData, yearSelectorData]}
      selectedValue={selectedValue}
      option={option}
      handleFocus={handleFocus}
      buildHandleSelectChange={handleSelectChange}
    />
  );
};

export default OptionMonthSelect;
