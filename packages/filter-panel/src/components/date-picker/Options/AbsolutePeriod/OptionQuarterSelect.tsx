import React, {useContext, useMemo} from 'react';

import {getQuarterSelectorData, getYearSelectorData} from '../../constants';
import OptionSelect from './OptionSelect';
import {AbsolutePeriodFilterValue, AbsolutePeriodValue} from '../../valueTypes';
import {
  AbsolutePeriodOptionConfig,
  ViewProps,
  CalendarConfig,
  LocaleMoment,
  DateInterval,
} from '../../types';
import {getStartEndDates} from './getStartEndDates';
import {MomentContext} from '../../MomentContext';

const getLabel = (
  option: AbsolutePeriodOptionConfig,
  value: AbsolutePeriodValue
) => {
  return `${option.label} Q${value.periodValue} ${value.year}`;
};

const getDateFilterValue = (
  option: AbsolutePeriodOptionConfig,
  value: AbsolutePeriodValue,
  moment: LocaleMoment,
  calendarConfig?: CalendarConfig
) => ({
  ...value,
  ...getStartEndDates(value, moment, calendarConfig),
  optionLabel: getLabel(option, value),
});

const OptionQuarterSelect = ({
  selectedValue,
  onChangeHandler,
  option,
  disabled,
  calendarConfig,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const moment = useContext(MomentContext);

  const quarterSelectorData = useMemo(
    () => getQuarterSelectorData({moment, calendarConfig}),
    [calendarConfig, moment]
  );

  const yearSelectorData = useMemo(
    () => getYearSelectorData({moment, calendarConfig}),
    [calendarConfig, moment]
  );

  const selected = selectedValue ?? {
    period: 'absolute',
    interval: DateInterval.Quarter,
    periodValue: Number(quarterSelectorData.getValue()),
    year: Number(yearSelectorData.getValue()),
  };

  const buildHandleSelectChange = (selectorId: string) => (value: string) => {
    const newSelected = {...selected};

    if (selectorId === 'year') {
      newSelected.year = Number(value);
    }

    if (selectorId === 'quarter') {
      newSelected.periodValue = Number(value);
    }

    onChangeHandler(
      getDateFilterValue(option, newSelected, moment, calendarConfig)
    );
  };

  const handleFocus = () => {
    onChangeHandler(
      getDateFilterValue(option, selected, moment, calendarConfig)
    );
  };

  return (
    <OptionSelect
      selectors={[quarterSelectorData, yearSelectorData]}
      selectedValue={selectedValue}
      option={option}
      handleFocus={handleFocus}
      buildHandleSelectChange={buildHandleSelectChange}
      disabled={disabled}
    />
  );
};

export default OptionQuarterSelect;
