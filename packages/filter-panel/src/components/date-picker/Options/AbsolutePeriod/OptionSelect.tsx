import classNames from './OptionSelect.module.css';

import {Select} from '@jotunheim/react-select';
import {Radio} from '@jotunheim/react-toggle';
import cn from 'classnames';
import React from 'react';

import {
  AbsolutePeriodOptionConfig,
  CalendarConfig,
  DateFilterValue,
  DatePartSelectData,
  SelectorItem,
} from '../../types';
import {bemFactory} from '@jotunheim/react-themes';

const renderSelectOptions = (items: SelectorItem[]) =>
  items.map((item) => (
    <Select.Option key={item.value} value={item.value}>
      {item.label}
    </Select.Option>
  ));

export type OptionSelectProps = {
  selectors: DatePartSelectData[];
  selectedValue?: DateFilterValue;
  option: AbsolutePeriodOptionConfig;
  buildHandleSelectChange: (selectorId: string) => (value: string) => void;
  handleFocus: () => void;
  disabled?: boolean;
  calendarConfig?: CalendarConfig;
};

const getValue = (
  selector: DatePartSelectData,
  checked: boolean,
  value?: DateFilterValue
) => {
  return checked ? selector.getValue(value) : selector.getValue();
};

const {block, element} = bemFactory('comd-date-option-select', classNames);

const OptionSelect = ({
  selectors,
  selectedValue,
  option,
  buildHandleSelectChange,
  handleFocus,
  disabled,
}: OptionSelectProps) => {
  const checked = !!selectedValue;

  return (
    <div className={block()}>
      <Radio
        value={option.id}
        checked={checked}
        disabled={disabled}
        onChange={handleFocus}>
        {option.label}
      </Radio>
      <div className={element('inner')} tabIndex={0} onFocus={handleFocus}>
        {selectors.map((selector) => (
          <div
            className={cn(
              element('selector'),
              element('selector', selector.styleModifier)
            )}
            key={selector.id}>
            <Select
              value={getValue(selector, checked, selectedValue)}
              onChange={buildHandleSelectChange(selector.id)}
              disabled={disabled}>
              {renderSelectOptions(selector.data)}
            </Select>
          </div>
        ))}
      </div>
    </div>
  );
};

export default OptionSelect;
