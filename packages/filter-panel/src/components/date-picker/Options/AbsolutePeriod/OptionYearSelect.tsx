import React, {useContext, useMemo} from 'react';

import {getYearSelectorData} from '../../constants';
import {
  AbsolutePeriodFilterValue,
  AbsolutePeriodOptionConfig,
  CalendarConfig,
  DateInterval,
  LocaleMoment,
  ViewProps,
} from '../../types';
import {AbsolutePeriodValue} from '../../valueTypes';
import OptionSelect from './OptionSelect';
import {getStartEndDates} from './getStartEndDates';
import {MomentContext} from '../../MomentContext';

const getLabel = (
  option: AbsolutePeriodOptionConfig,
  value: AbsolutePeriodValue
) => {
  return `${option.label} ${value.year}`;
};

const getDateFilterValue = (
  option: AbsolutePeriodOptionConfig,
  value: AbsolutePeriodValue,
  moment: LocaleMoment,
  calendarConfig?: CalendarConfig
) => ({
  ...value,
  ...getStartEndDates(value, moment, calendarConfig),
  optionLabel: getLabel(option, value),
});

const OptionYearSelect = ({
  selectedValue,
  option,
  onChangeHandler,
  calendarConfig,
}: ViewProps<AbsolutePeriodFilterValue, AbsolutePeriodOptionConfig>) => {
  const moment = useContext(MomentContext);

  const yearSelectorData = useMemo(
    () => getYearSelectorData({moment, calendarConfig}),
    [calendarConfig, moment]
  );

  const selected = selectedValue ?? {
    period: 'absolute',
    periodValue: 0,
    interval: DateInterval.Year,
    year: Number(yearSelectorData.getValue()),
  };

  const buildHandleSelectChange = (selectorId: string) => (value: string) => {
    const newSelected = {...selected};

    if (selectorId === 'year') {
      newSelected.year = Number(value);
    }

    onChangeHandler(
      getDateFilterValue(option, newSelected, moment, calendarConfig)
    );
  };

  const handleFocus = () => {
    onChangeHandler(
      getDateFilterValue(option, selected, moment, calendarConfig)
    );
  };

  return (
    <OptionSelect
      selectors={[yearSelectorData]}
      selectedValue={selectedValue}
      option={option}
      buildHandleSelectChange={buildHandleSelectChange}
      handleFocus={handleFocus}
    />
  );
};

export default OptionYearSelect;
