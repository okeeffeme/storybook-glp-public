import cn from 'classnames';
import noop from 'lodash/noop';
import React, {useCallback} from 'react';

import {CalendarConfig, isPredefinedOptionValue, Option} from '../types';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './OptionWrapper.module.css';
import {DateFilterValue} from '../valueTypes';
import {Language} from '../../../../src';

type OptionWrapperProps = {
  // We expect that correct mapping of options was done, using any to avoid type errors
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  option: Omit<Option, 'view'> & {view: React.FC<any>};
  selectedValue?: DateFilterValue;
  onValueChanged: (value?: DateFilterValue) => void;
  disabled: boolean;
  calendarConfig?: CalendarConfig;
  language: Language;
};

const {block, modifier} = bemFactory(
  'comd-date-quick-filter-option',
  classNames
);

const OptionWrapper = ({
  option,
  selectedValue,
  onValueChanged = noop,
  disabled,
  calendarConfig,
  language,
}: OptionWrapperProps) => {
  const onChangeHandler = useCallback(
    (value: Omit<DateFilterValue, 'optionId'> | undefined) => {
      if (disabled) return;

      if (value == null) {
        onValueChanged();
        return;
      }

      const patchedValue = {
        optionId: option.id,
        isFiscal: option.useFiscalCalendar,
        ...value,
      } as DateFilterValue;

      onValueChanged(patchedValue);
    },
    [disabled, onValueChanged, option.id, option.useFiscalCalendar]
  );

  const value =
    isPredefinedOptionValue(selectedValue) &&
    option.id === selectedValue.optionId
      ? selectedValue
      : undefined;

  const optionContent = option.view({
    option,
    selectedValue: value,
    onChangeHandler,
    disabled,
    language,
    calendarConfig: option.useFiscalCalendar
      ? calendarConfig
      : {...calendarConfig, fiscalCalendar: undefined},
  });

  if (option.period === 'custom') {
    return optionContent;
  }

  return (
    <div
      className={cn(block(), {[modifier('disabled')]: disabled})}
      data-quick-option={option.id}>
      {optionContent}
    </div>
  );
};

export default OptionWrapper;
