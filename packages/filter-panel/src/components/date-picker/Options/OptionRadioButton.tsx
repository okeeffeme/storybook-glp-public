import {Radio} from '@jotunheim/react-toggle';
import React, {useContext} from 'react';

import {RelativePeriodFilterValue} from '../valueTypes';
import {
  CalendarConfig,
  LocaleMoment,
  RadioOptionConfig,
  ViewProps,
} from '../types';
import {localDateTimeFormat} from '../constants';
import {MomentContext} from '../MomentContext';
import {findPeriodByInterval} from '../fiscalPeriods';

const getStartDate = (
  option: RadioOptionConfig,
  moment: LocaleMoment,
  calendarConfig?: CalendarConfig
): string => {
  const timeUnit = option.dateInterval;
  if (option.id === 'yesterday') {
    return moment()
      .startOf(timeUnit)
      .add(-1, timeUnit)
      .utc()
      .format(localDateTimeFormat);
  }

  const period = calendarConfig?.fiscalCalendar
    ? findPeriodByInterval(
        calendarConfig.fiscalCalendar,
        timeUnit,
        moment,
        moment()
      )
    : undefined;
  return period
    ? period.start.format(localDateTimeFormat)
    : moment().startOf(timeUnit).utc().format(localDateTimeFormat);
};

const OptionRadioButton = ({
  option,
  selectedValue,
  onChangeHandler,
  calendarConfig,
  disabled,
}: ViewProps<RelativePeriodFilterValue, RadioOptionConfig>) => {
  const moment = useContext(MomentContext);

  const handleClick = () => {
    const startDate = getStartDate(option, moment, calendarConfig);
    onChangeHandler({
      period: 'relative',
      offset: option.id === 'yesterday' ? -1 : 0,
      interval: option.dateInterval,
      optionLabel: option.label,
      startDate,
    });
  };

  return (
    <Radio
      value={option.id}
      checked={option.id === selectedValue?.optionId}
      onChange={handleClick}
      disabled={disabled}
      data-quick-option-default={option.id}>
      {option.label}
    </Radio>
  );
};

export default OptionRadioButton;
