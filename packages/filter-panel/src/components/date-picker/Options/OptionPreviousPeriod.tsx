import classNames from './OptionPreviousPeriod.module.css';

import TextField from '@jotunheim/react-text-field';
import {Radio} from '@jotunheim/react-toggle';
import React, {useContext, useState} from 'react';
import {
  DateFilterValue,
  LocaleMoment,
  ViewProps,
  DateInterval,
  FiscalCalendar,
  PreviousOptionConfig,
  RelativePeriodFilterValue,
} from '../types';
import {bemFactory} from '@jotunheim/react-themes';
import {localDateTimeFormat} from '../constants';
import {isRelativePeriodValue} from '../valueTypes';
import {MomentContext} from '../MomentContext';
import {findPeriodIndex, getFiscalPeriods} from '../fiscalPeriods';

interface BuildRelativePeriodParams {
  offset: number;
  interval: DateInterval;
  moment: LocaleMoment;
  fiscalCalendar: FiscalCalendar;
}

export function getRelativeFiscalPeriodStart({
  offset,
  interval,
  moment,
  fiscalCalendar,
}: BuildRelativePeriodParams) {
  const periods = getFiscalPeriods(fiscalCalendar, interval, moment);
  const ind = findPeriodIndex(periods, moment());
  if (ind === -1) {
    throw new Error(`Current date is missing in calendar`);
  }

  const fromOffset = offset <= 0 ? offset : 1;
  const toOffset = offset >= 0 ? offset : -1;

  const fromPeriod = periods[ind + fromOffset];
  const toPeriod = periods[ind + toOffset];
  if (!fromPeriod || !toPeriod) {
    throw new Error(`Period is missing in calendar`);
  }

  const from = moment(fromPeriod.start).format(localDateTimeFormat);

  return from;
}

const getStartDate = (
  option: PreviousOptionConfig,
  offsetValue: string,
  moment: LocaleMoment,
  fiscalCalendar?: FiscalCalendar
) => {
  const timeUnit = option.dateInterval;
  if (fiscalCalendar)
    return getRelativeFiscalPeriodStart({
      offset: -offsetValue,
      fiscalCalendar,
      moment,
      interval: timeUnit,
    });

  return moment().add(-offsetValue, timeUnit).utc().format(localDateTimeFormat);
};

const {block, element} = bemFactory('comd-date-option-input-field', classNames);

export const getPreselectedOption = (
  selectedValue: DateFilterValue | undefined
): string => {
  if (isRelativePeriodValue(selectedValue)) {
    // For previous period, the offset is negative, but we display positive numbers in control. Invert the sign.
    return (-1 * selectedValue.offset).toString();
  }

  return '1';
};

const OptionPreviousPeriod = ({
  selectedValue,
  onChangeHandler,
  option,
  calendarConfig,
  disabled,
}: ViewProps<RelativePeriodFilterValue, PreviousOptionConfig>) => {
  const moment = useContext(MomentContext);

  const [inputValue, setInputValue] = useState(
    getPreselectedOption(selectedValue)
  );

  const handleInputChange = (value: string) => {
    setInputValue(value);
    const n = parseInt(value, 10);
    if (n == null || Number.isNaN(n)) {
      onChangeHandler(undefined);
      return;
    }
    const timeUnit = option.dateInterval;

    onChangeHandler({
      period: 'relative',
      interval: timeUnit,
      optionLabel: `${option.label} ${value} ${option.suffix}`,
      startDate: getStartDate(
        option,
        value,
        moment,
        calendarConfig?.fiscalCalendar
      ),
      offset: -n,
    });
  };

  const handleOptionSelected = () => {
    handleInputChange(inputValue);
  };

  return (
    <div className={block()} data-quick-option-field={option.id}>
      <Radio
        value={option.id}
        disabled={disabled}
        checked={!!selectedValue}
        onChange={handleOptionSelected}>
        {option.label}
      </Radio>
      <div className={element('label-wrapper')} onClick={handleOptionSelected}>
        <div className={element('input')}>
          <TextField
            value={inputValue}
            onChange={handleInputChange}
            onFocus={handleOptionSelected}
            min={1}
            type={'number'}
            disabled={disabled}
          />
        </div>
        <span className={element('label')}>{option.suffix}</span>
      </div>
    </div>
  );
};

export default OptionPreviousPeriod;
