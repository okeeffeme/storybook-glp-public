import {
  CalendarConfig,
  DateInterval,
  DatePartSelectData,
  LocaleMoment,
} from './types';

import {
  generateFiscalYearSelectorData,
  generateMonthsSelectorData,
  generateQuartersSelectorData,
  generateWeeksSelectorData,
  generateYearsSelectorData,
} from './generateSelectorData';
import {isAbsolutePeriodValue} from './valueTypes';
import {findPeriod, getFiscalPeriods} from './fiscalPeriods';

export const localDateTimeFormat = 'YYYY-MM-DD[T]HH:mm:ss.SSS';

export const defaultDateFormat = {
  date: 'YYYY-MM-DD',
  dateTime: 'YYYY-MM-DD HH:mm:ss.SSS',
};

type GetDefaultValueParams = {
  calendarConfig?: CalendarConfig;
  interval: DateInterval;
  moment: LocaleMoment;
};

const getDefaultValue = ({
  calendarConfig,
  interval,
  moment,
}: GetDefaultValueParams) => {
  if (calendarConfig?.fiscalCalendar) {
    const fiscalYears = getFiscalPeriods(
      calendarConfig.fiscalCalendar,
      DateInterval.Year,
      moment
    );
    const currentFiscalYear = findPeriod(fiscalYears, moment());

    if (interval === 'year') return currentFiscalYear?.year ?? moment().year();

    const periods = getFiscalPeriods(
      calendarConfig.fiscalCalendar,
      interval,
      moment,
      currentFiscalYear?.year
    );
    const currentPeriod = findPeriod(periods, moment());
    return currentPeriod?.periodValue ?? 0;
  } else {
    const now = moment();
    switch (interval) {
      case 'year':
        return now.year();
      case 'month':
        return now.month() + 1;
      case 'quarter':
        return now.quarter();
      case 'week':
        return now.week();
    }
  }
  throw Error('Not implemented');
};

interface GetSelectorDataParams {
  moment: LocaleMoment;
  calendarConfig?: CalendarConfig;
}

export const getQuarterSelectorData = ({
  moment,
}: GetSelectorDataParams): DatePartSelectData => ({
  id: 'quarter',
  data: generateQuartersSelectorData(),
  styleModifier: 'quarter',
  getValue: (value) =>
    isAbsolutePeriodValue(value)
      ? value.periodValue
      : getDefaultValue({interval: DateInterval.Quarter, moment}),
});

export const getMonthSelectorData = ({
  moment,
  calendarConfig,
}: GetSelectorDataParams): DatePartSelectData => {
  const monthSelectorData = {
    id: 'month',
    data: generateMonthsSelectorData(),
    styleModifier: 'month',
    getValue: (value) =>
      isAbsolutePeriodValue(value)
        ? value.periodValue
        : getDefaultValue({
            calendarConfig,
            moment,
            interval: DateInterval.Month,
          }),
  };

  if (!calendarConfig?.fiscalCalendar) {
    return monthSelectorData;
  }

  // Reordering the month array to follow firstCalendarMonth rule
  const {firstCalendarMonth} = calendarConfig.fiscalCalendar;
  const correctedMonthSelectorData = [
    ...monthSelectorData.data.slice(firstCalendarMonth - 1),
    ...monthSelectorData.data.slice(0, firstCalendarMonth - 1),
  ].map((data, index) => ({
    value: index + 1,
    label: data.label,
  }));

  return {
    ...monthSelectorData,
    data: correctedMonthSelectorData,
  };
};

export const getYearSelectorData = ({
  moment,
  calendarConfig,
}: GetSelectorDataParams): DatePartSelectData => {
  return {
    id: 'year',
    data: calendarConfig?.fiscalCalendar
      ? generateFiscalYearSelectorData(calendarConfig.fiscalCalendar)
      : generateYearsSelectorData(20),
    styleModifier: 'year',
    getValue: (value) =>
      isAbsolutePeriodValue(value)
        ? value.year
        : getDefaultValue({
            calendarConfig,
            moment,
            interval: DateInterval.Year,
          }),
  };
};

interface GetFiscalWeekSelectorDataParams extends GetSelectorDataParams {
  year: number;
}

export const getFiscalWeekSelectorData = ({
  moment,
  calendarConfig,
  year,
}: GetFiscalWeekSelectorDataParams): DatePartSelectData => {
  if (!calendarConfig?.fiscalCalendar) {
    throw new Error('Fiscal calendar must be defined');
  }

  const weeks = getFiscalPeriods(
    calendarConfig.fiscalCalendar,
    DateInterval.Week,
    moment,
    year
  );
  const data = generateWeeksSelectorData(weeks.length);
  return {
    id: 'week',
    data,
    styleModifier: 'week',
    getValue: (value) =>
      isAbsolutePeriodValue(value)
        ? value.periodValue
        : getDefaultValue({
            calendarConfig,
            moment,
            interval: DateInterval.Week,
          }),
  };
};

export const defaultGroupIds = [
  'days',
  'weeks',
  'months',
  'quarters',
  'years',
  'custom',
];

export const fiscalGroupIds = [
  'fiscalWeeks',
  'fiscalMonths',
  'fiscalQuarters',
  'fiscalYears',
  ...defaultGroupIds,
];
