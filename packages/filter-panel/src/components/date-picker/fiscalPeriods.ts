import {Moment} from 'moment';

import {
  CalendarPeriod,
  DateInterval,
  FiscalCalendar,
  LocaleMoment,
} from './types';

const startOfFirstWeek = (start: Moment) => {
  while (start.weekday() > 0) {
    start.subtract(1, 'day');
  }
  return start;
};

const getYearPeriods = (
  {years}: FiscalCalendar,
  moment: LocaleMoment,
  yearNumber?: number
): CalendarPeriod[] => {
  const periods: CalendarPeriod[] = [];
  for (const year of years) {
    if (yearNumber != null && year.number !== yearNumber) {
      continue;
    }
    const start = moment(year.months[0].start);
    const finish = moment(year.months[year.months.length - 1].finish);
    periods.push({year: year.number, periodValue: 0, start, finish});
  }
  return periods;
};

const getQuarterPeriods = (
  {years}: FiscalCalendar,
  moment: LocaleMoment,
  yearNumber?: number
): CalendarPeriod[] => {
  const periods: CalendarPeriod[] = [];
  for (const year of years) {
    if (yearNumber != null && year.number !== yearNumber) {
      continue;
    }
    for (let n = 0; n < 4; n += 1) {
      const start = moment(year.months[n * 3].start);
      const finish = moment(year.months[n * 3 + 2].finish);
      periods.push({year: year.number, periodValue: n + 1, start, finish});
    }
  }
  return periods;
};

const getMonthPeriods = (
  {years}: FiscalCalendar,
  moment: LocaleMoment,
  yearNumber?: number
): CalendarPeriod[] => {
  const periods: CalendarPeriod[] = [];
  for (const year of years) {
    if (yearNumber != null && year.number !== yearNumber) {
      continue;
    }
    for (let n = 0; n < 12; n += 1) {
      const start = moment(year.months[n].start);
      const finish = moment(year.months[n].finish);
      periods.push({year: year.number, periodValue: n + 1, start, finish});
    }
  }
  return periods;
};

const getWeekPeriods = (
  {years}: FiscalCalendar,
  moment: LocaleMoment,
  yearNumber?: number
): CalendarPeriod[] => {
  const periods: CalendarPeriod[] = [];
  for (const year of years) {
    if (yearNumber != null && year.number !== yearNumber) {
      continue;
    }
    const firstMonth = year.months[0];
    const lastMonth = year.months[year.months.length - 1];
    const firstDayOfYear = moment(firstMonth.start);
    const lastDayOfYear = moment(lastMonth.finish);
    const start = startOfFirstWeek(moment(firstDayOfYear));

    let n = 0;
    while (start.isBefore(lastDayOfYear)) {
      const finish = moment(start).endOf('week');
      periods.push({
        year: year.number,
        periodValue: n + 1,
        start: start.isBefore(firstDayOfYear) ? firstDayOfYear : moment(start),
        finish: finish.isAfter(lastDayOfYear) ? lastDayOfYear : finish,
      });
      start.add(1, 'week');
      n += 1;
    }
  }
  return periods;
};

export const getFiscalPeriods = (
  calendar: FiscalCalendar,
  interval: DateInterval,
  moment: LocaleMoment,
  yearNumber?: number
): CalendarPeriod[] => {
  switch (interval) {
    case 'year':
      return getYearPeriods(calendar, moment, yearNumber);
    case 'quarter':
      return getQuarterPeriods(calendar, moment, yearNumber);
    case 'month':
      return getMonthPeriods(calendar, moment, yearNumber);
    case 'week':
      return getWeekPeriods(calendar, moment, yearNumber);
    default:
      return [];
  }
};

export const findPeriodIndex = (
  sortedArray: CalendarPeriod[],
  value: Moment
): number => {
  let start = 0;
  let end = sortedArray.length - 1;

  while (start <= end) {
    const middle = Math.floor((start + end) / 2);
    const period = sortedArray[middle];

    if (value.isBetween(period.start, period.finish, undefined, '[]')) {
      // found the key
      return middle;
    } else if (value.isAfter(period.finish)) {
      // continue searching to the right
      start = middle + 1;
    } else {
      // continue searching to the left
      end = middle - 1;
    }
  }
  // key wasn't found
  return -1;
};

export const findPeriod = (
  sortedArray: CalendarPeriod[],
  value: Moment
): CalendarPeriod | undefined => {
  const ind = findPeriodIndex(sortedArray, value);
  return ind >= 0 ? sortedArray[ind] : undefined;
};

export const findPeriodByInterval = (
  fiscalCalendar: FiscalCalendar,
  interval: DateInterval,
  moment: LocaleMoment,
  value: Moment
): CalendarPeriod | undefined => {
  const fiscalYears = getFiscalPeriods(
    fiscalCalendar,
    DateInterval.Year,
    moment
  );
  const year = findPeriod(fiscalYears, value);
  if (interval === 'year') return year;
  const periods = getFiscalPeriods(
    fiscalCalendar,
    interval,
    moment,
    year?.year
  );
  const period = findPeriod(periods, value);
  return period;
};
