import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import React, {
  ReactElement,
  useEffect,
  useMemo,
  useContext,
  useCallback,
} from 'react';
import cn from 'classnames';
import {CSSTransition} from 'react-transition-group';

import {SidePanel} from '@jotunheim/react-side-panel';
import {bemFactory} from '@jotunheim/react-themes';
import {transitions} from '@jotunheim/global-styles';

import PanelDrawerFooter from './PanelDrawerFooter';
import PanelDrawerHeader from './PanelDrawerHeader';
import FilterList from './FilterList';

import {setFilter} from '../state/actions';
import FilterContext, {FilterDispatchContext} from '../context';

import classNames from './PanelDrawer.module.css';
import {FilterType} from '../types';
import SingleChoiceFilter from '../single-choice-filter/SingleChoiceFilter';
import SelectTextFilter from '../select-text-filter/SelectTextFilter';
import DateFilter from '../date-filter/DateFilter';
import DateAndTimeFilter from '../date-and-time-filter/DateAndTimeFilter';
import MultiChoiceFilter from '../multi-choice-filter/MultiChoiceFilter';
import MultiSelectFilter from '../multi-select-filter/MultiSelectFilter';

const {block, element, modifier} = bemFactory(
  'comd-filter-panel-drawer',
  classNames
);

interface PanelDrawerProps {
  className?: string | undefined;
}

const assertUnreachable = (val: never) => {
  throw new Error(
    `Did not expect to get here: Did you forget to handle the value ${val}?`
  );
};

const PanelDrawer: React.FC<PanelDrawerProps> = ({className, ...props}) => {
  const {
    filters,
    activeFilterId,
    onTogglePanel,
    isActive,
    header,
    onChangeActiveFilterId,
  } = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const filter = useMemo<ReactElement | undefined>(() => {
    const matchedFilter = filters?.find(({id}) => id === activeFilterId);

    if (matchedFilter) {
      switch (matchedFilter.type) {
        case FilterType.SingleChoice:
          return <SingleChoiceFilter {...matchedFilter.props} />;

        case FilterType.SelectText:
          return <SelectTextFilter {...matchedFilter.props} />;

        case FilterType.Date:
          return <DateFilter {...matchedFilter.props} />;

        case FilterType.DateAndTime:
          return <DateAndTimeFilter />;

        case FilterType.MultiChoice:
          return <MultiChoiceFilter {...matchedFilter.props} />;

        case FilterType.MultiSelect:
          return <MultiSelectFilter {...matchedFilter.props} />;

        default:
          assertUnreachable(matchedFilter);
      }

      throw new Error('unknown filter type');
    }
  }, [filters, activeFilterId]);

  useEffect(() => {
    if (!activeFilterId) {
      dispatch(setFilter());
      return;
    }

    const activeFilter = filters?.find(({id}) => id === activeFilterId);
    if (activeFilter) {
      dispatch(setFilter({header: activeFilter.title}));
    }
  }, [activeFilterId, dispatch, header, filters]);

  const listItemStyle = useMemo(
    () => cn(element('body-items'), {[element('hide-list')]: !!filter}),
    [filter]
  );

  const nodeRef = React.useRef(null);

  const handleBack = useCallback(() => {
    onChangeActiveFilterId('');
  }, [onChangeActiveFilterId]);

  if (!filters) {
    return null;
  }

  return (
    <CSSTransition
      in={isActive}
      nodeRef={nodeRef}
      timeout={transitions.EnterDuration}
      classNames={{
        enterActive: modifier('enter-active'),
        enterDone: modifier('enter-done'),
      }}>
      <div
        ref={nodeRef}
        className={cn(block(), className)}
        {...extractDataAriaIdProps(props)}>
        <SidePanel data-panel-drawer={isActive ? 'opened' : 'closed'}>
          <div className={element('content')}>
            <PanelDrawerHeader
              header={header}
              isFilterShown={Boolean(filter)}
              toggleActive={onTogglePanel}
              toggleBack={handleBack}
            />
            <div className={element('body')}>
              <div className={listItemStyle}>
                <FilterList items={filters} />
              </div>
              {/* To support multiple instance of same type of filter. Each should have unique key. */}
              {filter && React.cloneElement(filter, {key: activeFilterId})}
            </div>
            <PanelDrawerFooter
              toggleBack={handleBack}
              isFilterShown={Boolean(filter)}
            />
          </div>
        </SidePanel>
      </div>
    </CSSTransition>
  );
};

export default PanelDrawer;
