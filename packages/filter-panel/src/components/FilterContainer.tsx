import React from 'react';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import classNames from './FilterContainer.module.css';

const {block} = bemFactory('comd-filter-panel-list', classNames);

const FilterContainer = ({children, ...rest}: {children: React.ReactNode}) => (
  <div className={block()} {...extractDataAndAriaProps(rest)}>
    {children}
  </div>
);

export default FilterContainer;
