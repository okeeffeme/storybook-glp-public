import React, {useContext, useMemo} from 'react';

import FilterContext from '../context';
import {Filter} from '../types';
import FilterItem from './Filter';

type FilterListProps = {
  items: Filter[];
};

const FilterList = ({items}: FilterListProps) => {
  const {queries} = useContext(FilterContext);

  const ListView = useMemo<React.ReactElement<typeof FilterItem>[]>(() => {
    return items.map((itemProps, index) => {
      const {id} = itemProps;
      const appliedFilter = queries.find((item) => item.id === id);

      return (
        <FilterItem key={index} {...itemProps} appliedFilter={appliedFilter} />
      );
    });
  }, [items, queries]);

  return <>{ListView}</>;
};

export default React.memo(FilterList);
