import React, {useContext} from 'react';
import {Button} from '@jotunheim/react-button';
import FilterContext from '../context';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './PanelDrawerFooter.module.css';
import translate from '../translate';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';

const {block} = bemFactory('comd-filter-panel-drawer-footer', classNames);

type FilterFooterProps = {
  isFilterShown: boolean;
  toggleBack: () => void;
};

const FilterFooter = ({isFilterShown, toggleBack}: FilterFooterProps) => {
  const {onDeleteAll, queries, language, isFilterValid, onApplyFilter} =
    useContext(FilterContext);

  return (
    <div className={block()} data-filter-footer="">
      <Fieldset layoutStyle={LayoutStyle.Horizontal}>
        {isFilterShown ? (
          <>
            <Button
              onClick={toggleBack}
              appearance={Button.appearances.secondaryNeutral}
              data-filter-footer-back="">
              {translate({language, key: 'panel.footer.back'})}
            </Button>
            <Button
              onClick={onApplyFilter}
              data-testid="filter-footer-apply-button"
              appearance={Button.appearances.primarySuccess}
              disabled={!isFilterValid}
              data-filter-footer-apply="">
              {translate({language, key: 'panel.footer.apply'})}
            </Button>
          </>
        ) : (
          <Button
            onClick={onDeleteAll}
            disabled={!queries?.length}
            appearance={Button.appearances.secondaryNeutral}
            data-testid="filter-footer-clear-button"
            data-filter-footer-clear="">
            {translate({language, key: 'panel.footer.clearAll'})}
          </Button>
        )}
      </Fieldset>
    </div>
  );
};

export default FilterFooter;
