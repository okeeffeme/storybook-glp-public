import React, {useContext, useMemo} from 'react';

import Chip from '@jotunheim/react-chip';
import {bemFactory} from '@jotunheim/react-themes';
import Icon, {eye, eyeOff} from '@jotunheim/react-icons';

import {getAppliedFilterTextModifier} from '../getAppliedFilterTextModifier';
import FilterContext, {FilterDispatchContext} from '../context';
import {Pill, Filter, Language, Query} from '../types';
import translate from '../translate';

import classNames from './FilterPills.module.css';
import {toggleFilterPanel} from '../state/actions';

const {block} = bemFactory('comd-filter-panel-header-pills', classNames);

type TooltipText = {
  deletePills: string;
  deactivatePills: string;
  activatePills: string;
};

const getAppliedPillList = (
  queries: Query[],
  filters: Filter[],
  language: Language
): Pill[] => {
  const pills: Pill[] = [];
  for (const query of queries) {
    for (const filter of filters) {
      if (filter.id === query.id) {
        const text = getAppliedFilterTextModifier({
          type: filter.type,
          appliedFilter: query,
          title: filter.title,
          language,
          withoutTitle: false,
          getCustomOperatorText: filter.getCustomOperatorText,
        });
        pills.push({
          id: filter.id,
          text,
          isIdle: query.isIdle,
        });
      }
    }
  }
  return pills;
};

const FilterPills = () => {
  const {
    filters,
    queries,
    onDelete,
    onStatusToggle,
    language,
    activeFilterId,
    onChangeActiveFilterId,
  } = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);
  const tooltipText = useMemo<TooltipText>(
    () => ({
      deletePills: translate({
        language,
        key: 'panel.header.filterDeleteTooltip',
      }),
      deactivatePills: translate({
        language,
        key: 'panel.header.filterDeactivateTooltip',
      }),
      activatePills: translate({
        language,
        key: 'panel.header.filterActivateTooltip',
      }),
    }),
    [language]
  );
  const handleOnPillClick = (id) => {
    if (activeFilterId !== id) {
      onChangeActiveFilterId(id);
      dispatch(toggleFilterPanel(true));
    }
  };

  const handleOnDeactivate = (id) => {
    if (!onStatusToggle) return null;
    onStatusToggle(id);
  };

  const activePills = useMemo(
    () => getAppliedPillList(queries, filters, language),
    [queries, filters, language]
  );

  return (
    <div className={block()} data-filter-panel-pills="">
      {activePills.map((current) => {
        const isActive = !onStatusToggle || !current.isIdle;

        return (
          <Chip
            key={current.id}
            onClick={() => handleOnPillClick(current.id)}
            isActive={isActive}
            showDeleteButton={true}
            deleteButtonTooltip={tooltipText.deletePills}
            data-pills={current.id}
            actions={
              onStatusToggle
                ? [
                    {
                      icon: (
                        <Icon
                          path={isActive ? eye : eyeOff}
                          data-testid="test-deactivate-fillter-pills"
                          data-test-deactivate={isActive}
                        />
                      ),
                      handler: () => handleOnDeactivate(current.id),
                      tooltip: isActive
                        ? tooltipText.deactivatePills
                        : tooltipText.activatePills,
                    },
                  ]
                : []
            }
            onDelete={() => onDelete(current.id)}>
            {current.text}
          </Chip>
        );
      })}
    </div>
  );
};

export default FilterPills;
