import React, {useContext, useMemo} from 'react';
import cn from 'classnames';
import {SidePanel} from '@jotunheim/react-side-panel';
import {Icon, close, chevronLeft} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './PanelDrawerHeader.module.css';
import FilterContext from '../context';
import translate from '../translate';

const {block, element} = bemFactory(
  'comd-filter-panel-drawer-header',
  classNames
);

type PanelDrawerHeaderProps = {
  isFilterShown: boolean;
  toggleActive: () => void;
  toggleBack: () => void;
  header: string;
};

const PanelDrawerHeader = ({
  isFilterShown,
  toggleActive,
  toggleBack,
  header,
}: PanelDrawerHeaderProps) => {
  const {language} = useContext(FilterContext);
  const backButtonClasses = cn(element('back'), {
    [element('back', 'hide')]: !isFilterShown,
  });
  const defaultHeader = useMemo(
    () => translate({language, key: 'panel.header.title'}),
    [language]
  );

  return (
    <SidePanel.Header className={block()}>
      <div className={backButtonClasses}>
        <IconButton onClick={toggleBack} data-panel-drawer-header-back="">
          <Icon path={chevronLeft} />
        </IconButton>
      </div>
      <h3 className={element('title')} data-panel-drawer-header="">
        {header || defaultHeader}
      </h3>
      <div className={element('close')}>
        <IconButton onClick={toggleActive} data-panel-drawer-header-close="">
          <Icon path={close} />
        </IconButton>
      </div>
    </SidePanel.Header>
  );
};

export default PanelDrawerHeader;
