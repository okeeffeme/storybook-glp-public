import React from 'react';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';

import classNames from './FilterHelpText.module.css';

const {block} = bemFactory('comd-filter-help-text', classNames);

const FilterHelpText = ({children, ...rest}: {children: React.ReactNode}) => (
  <div className={block()} {...extractDataAndAriaProps(rest)}>
    {children}
  </div>
);

export default FilterHelpText;
