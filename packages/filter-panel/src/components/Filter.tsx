import React, {useContext} from 'react';

import {SidePanel} from '@jotunheim/react-side-panel';
import {Icon, chevronRight} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';

import FilterContext from '../context';
import {getAppliedFilterTextModifier} from '../getAppliedFilterTextModifier';
import {FilterType, GetCustomOperatorText, Query} from '../types';

import classNames from './Filter.module.css';

const {block, element} = bemFactory('comd-filter', classNames);

type FilterProps = {
  id: string;
  title: string;
  type: FilterType;
  appliedFilter?: Query;
  getCustomOperatorText?: GetCustomOperatorText;
};

const Filter = ({
  id,
  title,
  type,
  appliedFilter,
  getCustomOperatorText,
}: FilterProps) => {
  const {language, onChangeActiveFilterId} = useContext(FilterContext);

  let appliedFilterListText = '';

  if (appliedFilter) {
    appliedFilterListText = getAppliedFilterTextModifier({
      type,
      appliedFilter,
      title,
      language,
      withoutTitle: true,
      getCustomOperatorText,
    });
  }

  return (
    <SidePanel.Content
      onClick={() => onChangeActiveFilterId(id)}
      data-filter-list-id={id}
      data-testid="filter-list-id">
      <div className={block()}>
        <span className={element('title')}>{title}</span>
        {appliedFilter && (
          <span
            className={element('applied-filter')}
            data-testid="applied-filter-list-text"
            data-applied-filter-list={appliedFilterListText}>
            {appliedFilterListText}
          </span>
        )}
        <div className={element('icon')}>
          <Icon path={chevronRight} />
        </div>
      </div>
    </SidePanel.Content>
  );
};

export default Filter;
