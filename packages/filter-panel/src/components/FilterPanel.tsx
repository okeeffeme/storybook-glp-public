import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import cn from 'classnames';
import React from 'react';

import PanelDrawer from './PanelDrawer';
import FilterPanelHeader from './FilterPanelHeader';
import FilterPanelContainer from './FilterPanelContainer';

import FilterContext from '../context';
import {FilterPanelProps} from '../types';

import classNames from './FilterPanel.module.css';

const {block, element} = bemFactory('comd-filter-panel', classNames);

const FilterPanel = ({children, headerContent, ...props}: FilterPanelProps) => (
  <FilterPanelContainer {...props}>
    <FilterContext.Consumer>
      {({activeFilterId, isActive}) => (
        <div
          className={block()}
          data-testid="filter-panel"
          {...extractDataAriaIdProps(props)}
          data-active-filter={activeFilterId || false}>
          <PanelDrawer />
          <div
            data-testid="filter-panel-page-classes"
            className={cn(element('page'), {
              [element('page', 'drawer-open')]: isActive,
            })}>
            <FilterPanelHeader>{headerContent}</FilterPanelHeader>
            <div className={element('content')}>{children}</div>
          </div>
        </div>
      )}
    </FilterContext.Consumer>
  </FilterPanelContainer>
);

export default FilterPanel;
