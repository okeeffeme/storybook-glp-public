import DatePicker from '@jotunheim/react-date-picker';
import {bemFactory} from '@jotunheim/react-themes';
import moment from 'moment';
import React, {useCallback, useContext} from 'react';

import classNames from './DateAndTimeCustomPicker.module.css';
import TimePicker from './TimePicker';
import {CustomSelectedValue} from './DateAndTimePicker';
import FilterContext from '../../context';
import translate from '../../translate';
import {Fieldset, FieldsetGroup} from '@jotunheim/react-fieldset';
import FilterHelpText from '../FilterHelpText';
import {defaultDateFormat} from '../date-picker/constants';

type DateCustomPickerProps = {
  format?: string;
  formatTime?: string;
  id: string;
  onValueChange: (id: string, value: CustomSelectedValue) => void;
  selectedValue?: CustomSelectedValue;
  hasStartTimeError?: boolean;
  startTimeErrorText?: string;
  hasEndTimeError?: boolean;
  endTimeErrorText?: string;
  hasStartDateError?: boolean;
  errorStartDateText?: string;
  hasEndDateError?: boolean;
  errorEndDateText?: string;
};

const {element} = bemFactory('comd-date-custom-picker', classNames);

const DateAndTimeCustomPicker = ({
  id,
  format = defaultDateFormat.date,
  onValueChange,
  selectedValue,
  hasStartTimeError,
  startTimeErrorText,
  hasEndTimeError,
  endTimeErrorText,
  hasStartDateError,
  errorStartDateText,
  hasEndDateError,
  errorEndDateText,
}: DateCustomPickerProps) => {
  const {language} = useContext(FilterContext);
  const onStartDateSelected = useCallback(
    (date: moment.Moment | null) => {
      if (date) {
        onValueChange(id, {
          startDate: date,
          endDate: selectedValue?.endDate,
          startTime: '',
          endTime: selectedValue?.endTime,
        });
      } else {
        onValueChange(id, {
          startDate: undefined,
          endDate: selectedValue?.endDate,
          startTime: '',
          endTime: selectedValue?.endTime,
        });
      }
    },
    [selectedValue, onValueChange, id]
  );

  const onEndDateSelected = useCallback(
    (endDateInput: moment.Moment | null) => {
      if (endDateInput) {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: endDateInput,
          startTime: selectedValue?.startTime,
          endTime: '',
        });
      } else {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: undefined,
          startTime: selectedValue?.startTime,
          endTime: '',
        });
      }
    },
    [onValueChange, selectedValue, id]
  );

  const onStartTimeSelected = useCallback(
    (startTimeInput?: string) => {
      if (startTimeInput) {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: selectedValue?.endDate,
          startTime: startTimeInput,
          endTime: selectedValue?.endTime,
        });
      } else {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: selectedValue?.endDate,
          startTime: undefined,
          endTime: selectedValue?.endTime,
        });
      }
    },
    [onValueChange, id, selectedValue]
  );

  const onEndTimeSelected = useCallback(
    (endTimeInput?: string) => {
      if (endTimeInput) {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: selectedValue?.endDate,
          startTime: selectedValue?.startTime,
          endTime: endTimeInput,
        });
      } else {
        onValueChange(id, {
          startDate: selectedValue?.startDate,
          endDate: selectedValue?.endDate,
          startTime: selectedValue?.startTime,
          endTime: undefined,
        });
      }
    },
    [selectedValue, onValueChange, id]
  );

  return (
    <FieldsetGroup
      layoutStyle={Fieldset.LayoutStyle.Vertical}
      datat-testid="date-custom-picker">
      <Fieldset
        layoutStyle={Fieldset.LayoutStyle.Horizontal}
        title={translate({
          language,
          key: 'dateAndTimeFilter.customPicker.from',
        })}>
        <div className={element('item')}>
          <DatePicker
            id="start-date-picker"
            data-testid="start-date-picker"
            date={selectedValue?.startDate}
            maxDate={selectedValue?.endDate}
            format={format}
            onChange={onStartDateSelected}
            hasError={hasStartDateError}
            errorText={errorStartDateText}
          />
        </div>
        <div className={element('item')}>
          <TimePicker
            defaultValue={selectedValue?.startDate}
            onChange={onStartTimeSelected}
            hasError={hasStartTimeError}
            errorText={startTimeErrorText}
            id="start-time-picker"
          />
        </div>
      </Fieldset>
      <Fieldset
        layoutStyle={Fieldset.LayoutStyle.Horizontal}
        title={translate({language, key: 'dateAndTimeFilter.customPicker.to'})}>
        <div className={element('item')}>
          <DatePicker
            id="end-date-picker"
            date={selectedValue?.endDate}
            minDate={selectedValue?.startDate}
            format={format}
            onChange={onEndDateSelected}
            hasError={hasEndDateError}
            errorText={errorEndDateText}
          />
        </div>
        <div className={element('item')}>
          <TimePicker
            onChange={onEndTimeSelected}
            defaultValue={selectedValue?.endDate}
            hasError={hasEndTimeError}
            errorText={endTimeErrorText}
            id="end-time-picker"
          />
        </div>
      </Fieldset>
      <FilterHelpText>
        {translate({
          language,
          key: 'dateAndTimeFilter.customPicker.filterHint',
        })}
      </FilterHelpText>
    </FieldsetGroup>
  );
};

export default DateAndTimeCustomPicker;
