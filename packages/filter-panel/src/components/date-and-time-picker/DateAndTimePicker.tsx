import React from 'react';
import moment from 'moment';
import DatePickerWithTabs from './DatePickerWithTabs';
import DateAndTimeQuickFilter from './DateAndTimeQuickFilter';
import DateAndTimeCustomPicker from './DateAndTimeCustomPicker';
import {DateAndTimeQuickSelectedOption, FilterType} from './types';

export interface QuickDateOptions {
  id: string;
  label: string;
  value: string;
}

export type CustomSelectedValue = {
  startDate?: moment.Moment;
  endDate?: moment.Moment;
  startTime?: string;
  endTime?: string;
};

type DateAndTimePicker = {
  id: string;
  onQuickValueChange: (value: DateAndTimeQuickSelectedOption) => void;
  onCustomValueChange: (id: string, value: CustomSelectedValue) => void;
  quickSelectedOption?: string;
  customSelectedOption?: CustomSelectedValue;
  quickFilterOptions: QuickDateOptions[];
  defaultFilterType?: FilterType;
  hasStartTimeError?: boolean;
  startTimeErrorText?: string;
  hasEndTimeError?: boolean;
  endTimeErrorText?: string;
  hasStartDateError?: boolean;
  errorStartDateText?: string;
  hasEndDateError?: boolean;
  errorEndDateText?: string;
};

const DateAndTimePicker = ({
  id,
  onQuickValueChange,
  onCustomValueChange,
  quickSelectedOption,
  customSelectedOption,
  quickFilterOptions,
  defaultFilterType,
  hasStartTimeError,
  startTimeErrorText,
  hasEndTimeError,
  endTimeErrorText,
  hasStartDateError,
  errorStartDateText,
  hasEndDateError,
  errorEndDateText,
}: DateAndTimePicker) => {
  const dateQuickFilter = (
    <DateAndTimeQuickFilter
      options={quickFilterOptions}
      id={id}
      onValueChange={onQuickValueChange}
      selectedOption={quickSelectedOption}
    />
  );

  const dateCustomFilter = (
    <DateAndTimeCustomPicker
      id={id}
      onValueChange={onCustomValueChange}
      selectedValue={customSelectedOption}
      hasStartTimeError={hasStartTimeError}
      startTimeErrorText={startTimeErrorText}
      hasEndTimeError={hasEndTimeError}
      endTimeErrorText={endTimeErrorText}
      hasStartDateError={hasStartDateError}
      errorStartDateText={errorStartDateText}
      hasEndDateError={hasEndDateError}
      errorEndDateText={errorEndDateText}
    />
  );

  return (
    <div data-date-and-time-picker="">
      <DatePickerWithTabs
        dateQuickFilter={dateQuickFilter}
        dateCustomFilter={dateCustomFilter}
        defaultFilterType={defaultFilterType}
      />
    </div>
  );
};

export default DateAndTimePicker;
