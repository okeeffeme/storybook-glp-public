import React, {useCallback, useContext, useEffect, useState} from 'react';
import moment from 'moment';
import TextField from '@jotunheim/react-text-field';
import {Icon, calendarClock} from '@jotunheim/react-icons';
import FilterContext from '../../context';
import translate from '../../translate';

type TimePickerProps = {
  required?: boolean;
  onChange: (time?: string) => void;
  placeholder?: string;
  defaultValue?: moment.Moment;
  hasError?: boolean;
  errorText?: string;
  id?: string;
};

const TimePicker = (props: TimePickerProps) => {
  const {
    required,
    onChange,
    placeholder = 'HH:MM',
    defaultValue,
    hasError,
    errorText,
    id,
  } = props;
  const [timeValue, setTimeValue] = useState('');
  const {language} = useContext(FilterContext);

  useEffect(() => {
    defaultValue?.hours() && defaultValue?.minutes()
      ? setTimeValue(
          `${defaultValue?.hours()}`.padStart(2, '0') +
            ':' +
            `${defaultValue?.minutes()}`
        )
      : setTimeValue('');
  }, [defaultValue]);

  const handleChange = useCallback(
    (timeValue: string) => {
      setTimeValue(timeValue);
      onChange(timeValue);
    },
    [onChange]
  );

  return (
    <TextField
      onChange={handleChange}
      value={timeValue}
      label={translate({language, key: 'dateAndTimeFilter.time'})}
      placeholder={placeholder}
      required={required}
      error={hasError}
      helperText={errorText || ''}
      prefix={<Icon path={calendarClock} />}
      id={id}
    />
  );
};

export default TimePicker;
