import React, {useCallback} from 'react';
import {Radio} from '@jotunheim/react-toggle';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';

import {DateAndTimeQuickSelectedOption} from './types';
import {QuickDateOptions} from './DateAndTimePicker';

import FilterContainer from '../FilterContainer';

type DateAndTimeQuickFilterProps = {
  options: QuickDateOptions[];
  id?: string;
  onValueChange: (value: DateAndTimeQuickSelectedOption) => void;
  selectedOption?: string;
};

const DateAndTimeQuickFilter = ({
  id = '',
  options,
  onValueChange,
  selectedOption,
}: DateAndTimeQuickFilterProps) => {
  const handleOptionChange = useCallback(
    (value: string) => {
      const selectedOption = options.find((option) => option.value === value);
      selectedOption?.value &&
        onValueChange({
          id,
          label: selectedOption?.label,
          value: selectedOption.value,
        });
    },
    [onValueChange, id, options]
  );

  return (
    <FilterContainer>
      <Fieldset data-select-filter="" layoutStyle={LayoutStyle.Vertical}>
        {options.map(({value, label}) => (
          <Radio
            data-testid="filter-option"
            data-filter-option={value}
            key={value}
            id={value}
            checked={selectedOption === value}
            onChange={() => handleOptionChange(value)}>
            {label}
          </Radio>
        ))}
      </Fieldset>
    </FilterContainer>
  );
};

export default DateAndTimeQuickFilter;
