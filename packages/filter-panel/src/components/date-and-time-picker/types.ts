export enum FilterType {
  Quick = 'quick',
  Custom = 'custom',
}

export type DateAndTimeQuickSelectedOption = {
  id: string;
  label: string;
  value: string;
};
