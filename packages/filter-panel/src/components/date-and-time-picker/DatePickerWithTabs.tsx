import Tabs from '@jotunheim/react-tabs';
import React, {useContext, useState} from 'react';
import {FilterType} from './types';
import translate from '../../translate';
import FilterContext from '../../context';

type DatePickerWithTabsProps = {
  dateQuickFilter: React.ReactElement;
  dateCustomFilter: React.ReactElement;
  defaultFilterType?: FilterType;
};

const DatePickerWithTabs = ({
  dateQuickFilter,
  dateCustomFilter,
  defaultFilterType = FilterType.Quick,
}: DatePickerWithTabsProps) => {
  const [selectedTabId, setSelectedTabId] = useState<string>(defaultFilterType);
  const {language} = useContext(FilterContext);

  return (
    <Tabs
      selectedTabId={selectedTabId}
      onChange={undefined}
      onTabClicked={(tabId) => setSelectedTabId(tabId)}
      data-date-picker-tabs="">
      <Tabs.Tab
        id="quick"
        hasPadding={false}
        title={translate({language, key: 'dateFilter.tabs.quickFilter'})}>
        {dateQuickFilter}
      </Tabs.Tab>
      <Tabs.Tab
        id="custom"
        title={translate({language, key: 'dateFilter.tabs.customFilter'})}>
        {dateCustomFilter}
      </Tabs.Tab>
    </Tabs>
  );
};

export default DatePickerWithTabs;
