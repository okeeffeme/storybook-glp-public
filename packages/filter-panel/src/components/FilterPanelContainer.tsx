import React, {useCallback, useMemo, useReducer} from 'react';
import {useMount} from 'react-use';
import {useUncontrolled} from 'uncontrollable';

import FilterContext, {FilterDispatchContext} from '../context';
import {toggleFilterPanel} from '../state/actions';
import reducer, {initialState} from '../state/reducer';
import {
  UncontrollableFilterPanelContainerProps,
  Language,
  FilterPanelContainerProps,
} from '../types';
import FilterPanelHeader from './FilterPanelHeader';
import PanelDrawer from './PanelDrawer';

const FilterPanelContainer = ({
  language = Language.EN,
  children,
  value,
  filters,
  onApply,
  onDelete,
  onDeleteAll,
  onStatusToggle,
  activeFilterId,
  onChangeActiveFilterId,
}: FilterPanelContainerProps) => {
  const [{isActive, query, header, isFilterValid, optionsById}, dispatch] =
    useReducer(reducer, initialState);

  useMount(() => {
    if (activeFilterId) {
      dispatch(toggleFilterPanel(true));
    }
  });

  const handleClearAll = useCallback(() => {
    dispatch(toggleFilterPanel(false));
    onDeleteAll();
  }, [onDeleteAll]);

  const handleOnFilterApply = useCallback(() => {
    if (isFilterValid) {
      onApply(query);
    }
  }, [onApply, query, isFilterValid]);

  const togglePanelDisplay = useCallback(() => {
    dispatch(toggleFilterPanel(!isActive));
    onChangeActiveFilterId('');
  }, [isActive, onChangeActiveFilterId]);

  const filterPanelContext = useMemo(
    () => ({
      filters,
      queries: value,
      language,
      optionsById,
      isFilterValid,
      activeFilterId: activeFilterId || '',
      onDelete,
      onStatusToggle,
      onDeleteAll: handleClearAll,
      onApplyFilter: handleOnFilterApply,
      onTogglePanel: togglePanelDisplay,
      onChangeActiveFilterId,
      isActive,
      header,
    }),
    [
      filters,
      value,
      language,
      optionsById,
      isFilterValid,
      activeFilterId,
      onDelete,
      onStatusToggle,
      handleClearAll,
      handleOnFilterApply,
      togglePanelDisplay,
      onChangeActiveFilterId,
      isActive,
      header,
    ]
  );

  return (
    <FilterContext.Provider value={filterPanelContext}>
      <FilterDispatchContext.Provider value={dispatch}>
        {children}
      </FilterDispatchContext.Provider>
    </FilterContext.Provider>
  );
};

interface FilterPanelContainerType
  extends React.FC<UncontrollableFilterPanelContainerProps> {
  Drawer: typeof PanelDrawer;
  Toolbar: typeof FilterPanelHeader;
}

const UncontrolledOrControlledFilterPanelContainer = (
  props: UncontrollableFilterPanelContainerProps
) => {
  const filterPanelContainerProps = useUncontrolled(props, {
    activeFilterId: 'onChangeActiveFilterId',
  }) as FilterPanelContainerProps;

  return (
    <FilterPanelContainer {...filterPanelContainerProps}>
      {props.children}
    </FilterPanelContainer>
  );
};

UncontrolledOrControlledFilterPanelContainer.Drawer = PanelDrawer;
UncontrolledOrControlledFilterPanelContainer.Toolbar = FilterPanelHeader;

export default UncontrolledOrControlledFilterPanelContainer as FilterPanelContainerType;
