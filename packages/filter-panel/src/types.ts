import React from 'react';
import {
  DateAndTimeFilterValue,
  DateFilterValue,
} from './components/date-picker/types';
import {DateFilterProps} from './date-filter/DateFilter';

import {
  MultiChoiceFilterProps,
  MultiChoiceValue,
} from './multi-choice-filter/types';
import {MultiSelectFilterProps} from './multi-select-filter/types';
import {SelectTextFilterProps} from './select-text-filter/types';
import {SingleChoiceFilterProps} from './single-choice-filter/types';

export enum Language {
  NB = 'nb',
  EN = 'en',
}

export enum FilterType {
  DateAndTime = 'DateAndTime',
  Date = 'Date',
  MultiChoice = 'MultiChoice',
  MultiSelect = 'MultiSelect',
  SelectText = 'SelectText',
  SingleChoice = 'SingleChoice',
}

export type Query = {
  id: string;
  operator?: string;
  value:
    | string
    | number
    | DateFilterValue
    | DateAndTimeFilterValue
    | MultiChoiceValue;
  label?: string | string[];
  isIdle?: boolean;
};

export type SetFilterArgs = {
  header: string;
};

export type Pill = {
  id: string;
  text: string;
  isIdle?: boolean;
};

export type GetCustomOperatorText = (operator?: string) => string;

export type GetAppliedFilterTextModifier = {
  type: FilterType;
  appliedFilter: Query;
  title: string;
  language: Language;
  withoutTitle: boolean;
  getCustomOperatorText?: GetCustomOperatorText;
};

export type Filter = {
  id: string;
  title: string;
  getCustomOperatorText?: GetCustomOperatorText;
} & (
  | {
      type: FilterType.DateAndTime;
    }
  | {
      type: FilterType.Date;
      props?: DateFilterProps;
    }
  | {
      type: FilterType.MultiSelect;
      props: MultiSelectFilterProps;
    }
  | {
      type: FilterType.MultiChoice;
      props: MultiChoiceFilterProps;
    }
  | {
      type: FilterType.SelectText;
      props: SelectTextFilterProps;
    }
  | {
      type: FilterType.SingleChoice;
      props: SingleChoiceFilterProps;
    }
);

type Disunion<Controlled, Uncontrolled> =
  | (Controlled & {[p in keyof Uncontrolled]?: undefined})
  | (Uncontrolled & {[p in keyof Controlled]?: undefined})
  | {[p in keyof (Controlled & Uncontrolled)]?: undefined};

type ControlledActiveFilterIdProps = {
  activeFilterId: string;
  onChangeActiveFilterId: (id: string) => void;
};

type UncontrolledActiveFilterIdProps = {
  defaultActiveFilterId: string;
};

type FilterPanelContainerCommonProps = {
  children: React.ReactNode;
  onApply: (args: Query) => void;
  onDeleteAll: () => void;
  language?: Language;
  filters: Filter[];
  value: Query[];
  onDelete: (args: string) => void;
  onStatusToggle?: (id: string) => void;
};

export type FilterPanelContainerProps = FilterPanelContainerCommonProps &
  ControlledActiveFilterIdProps;

export type UncontrollableFilterPanelContainerProps =
  FilterPanelContainerCommonProps &
    Disunion<UncontrolledActiveFilterIdProps, ControlledActiveFilterIdProps>;

export type FilterPanelProps = UncontrollableFilterPanelContainerProps & {
  headerContent?: React.ReactNode;
};

export type FilterPanelState = {
  isActive: boolean;
  query: Query;
  header: string;
  isFilterValid: boolean;
  optionsById: Record<string, string[]>;
};

export type FilterContextType = {
  filters: Filter[];
  queries: Query[];
  language: Language;
  activeFilterId: string;
  onDeleteAll: () => void;
  isFilterValid: boolean;
  onDelete: (id: string) => void;
  onApplyFilter: () => void;
  onChangeActiveFilterId: (id: string) => void;
  onTogglePanel: () => void;
  onStatusToggle?: (id: string) => void;
  optionsById: Record<string, string[]>;
  isActive: boolean;
  header: string;
};
