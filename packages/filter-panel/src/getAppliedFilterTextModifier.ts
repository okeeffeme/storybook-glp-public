import compact from 'lodash/compact';

import {
  FilterType,
  GetAppliedFilterTextModifier,
  Language,
  Query,
} from './types';
import {DateFilterValue} from './components/date-picker/types';
import translate from './translate';

const equalSign = 'selectTextFilter.equalsSign';

const getSign = (
  operator: GetAppliedFilterTextModifier['appliedFilter']['operator']
) => {
  return (
    {
      eq: equalSign,
      contains: 'selectTextFilter.containsSign',
      lt: 'selectTextFilter.lessThanSign',
      gt: 'selectTextFilter.greaterThanSign',
    }[operator || 'eq'] || equalSign
  );
};

const getPrefix = (operatorText, title, withoutTitle, language) => {
  const defaultSign = operatorText === equalSign;
  const operator = translate({key: operatorText, language});

  if (withoutTitle && defaultSign) {
    return '';
  } else if (withoutTitle) {
    return `${operator} `;
  } else if (defaultSign) {
    return `${title}${operator} `;
  } else {
    return `${title} ${operator} `;
  }
};

const getMultiChoiceFilterValue = (query: Query): string => {
  const label = compact(query.label);
  const value = Array.isArray(query.value) && compact(query.value);
  const pillValues = label.length ? label : value;
  const reducedValue = pillValues.reduce((result, val) => {
    return val ? `${result}, ${val}` : result.toString();
  });
  return reducedValue;
};

const getValue = (type: FilterType, appliedFilter: Query) => {
  switch (type) {
    case FilterType.SingleChoice:
      return `${appliedFilter.label || appliedFilter.value}`;

    case FilterType.SelectText:
      return `${appliedFilter.value}`;

    case FilterType.Date:
      return `${(appliedFilter.value as DateFilterValue).optionLabel}`;

    case FilterType.DateAndTime:
      return `${appliedFilter.label}`;

    case FilterType.MultiChoice:
    case FilterType.MultiSelect:
      return getMultiChoiceFilterValue(appliedFilter);

    default:
      return '';
  }
};

export const getAppliedFilterTextModifier = ({
  type,
  appliedFilter,
  title,
  language = Language.EN,
  withoutTitle = false,
  getCustomOperatorText = getSign,
}: GetAppliedFilterTextModifier): string => {
  const operator =
    type === FilterType.SelectText
      ? getCustomOperatorText?.(appliedFilter.operator)
      : getCustomOperatorText?.(equalSign);
  const prefix = getPrefix(operator, title, withoutTitle, language);
  const value = getValue(type, appliedFilter);

  return `${prefix}${value}`;
};
