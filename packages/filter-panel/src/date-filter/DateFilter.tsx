import React, {useContext, useEffect, useMemo, useState} from 'react';
import FilterContext, {FilterDispatchContext} from '../context';
import {Query} from '../types';
import {setFilterValid, updateQuery} from '../state/actions';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {DatePicker} from '../components/date-picker';
import {
  CalendarConfig,
  CustomOption,
  DateFilterValue,
  isCustomRangePeriodValue,
  OptionFilterPredicate,
} from '../components/date-picker/types';

import FilterContainer from '../components/FilterContainer';

const getPreselectedOption = (
  filters: Query[],
  id: string
): DateFilterValue | undefined => {
  const filter = filters.find(({id: filterId}) => filterId === id);
  return filter?.value as DateFilterValue;
};

export type DateFilterProps = {
  calendarConfig?: CalendarConfig;
  customOptions?: CustomOption[];
  optionFilter?: OptionFilterPredicate;
  [key: string]: unknown;
};

const isValidDateFilterValue = (
  value: DateFilterValue | undefined
): boolean => {
  if (value == null) return false;

  if (isCustomRangePeriodValue(value) && !value.startDate && !value.endDate)
    return false;

  return true;
};

const DateFilter = ({
  calendarConfig,
  customOptions = [],
  optionFilter,
  ...rest
}: DateFilterProps) => {
  const {queries, language, activeFilterId} = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const selectedOption = useMemo(
    () => getPreselectedOption(queries, activeFilterId),
    [queries, activeFilterId]
  );
  const [localSelectedItem, setLocalSelectedItem] = useState<
    DateFilterValue | undefined
  >(selectedOption);

  useEffect(() => {
    // Overwrite local filter state when filter is deleted via filter pill
    setLocalSelectedItem(selectedOption);
  }, [setLocalSelectedItem, queries, activeFilterId, selectedOption]);

  useEffect(() => {
    dispatch(setFilterValid(isValidDateFilterValue(localSelectedItem)));
  }, [localSelectedItem, dispatch]);

  useEffect(() => {
    if (localSelectedItem) {
      dispatch(updateQuery({id: activeFilterId, value: localSelectedItem}));
    }
  }, [localSelectedItem, dispatch, activeFilterId]);

  const handleOnOptionClicked = (value?: DateFilterValue) => {
    setLocalSelectedItem(value);
  };

  return (
    <FilterContainer
      data-date-filter=""
      {...extractDataAriaIdProps(rest)}
      data-testid="date-filter">
      <DatePicker
        language={language}
        onValueChange={handleOnOptionClicked}
        selectedValue={localSelectedItem}
        customOptions={customOptions}
        calendarConfig={calendarConfig}
        optionFilter={optionFilter}
      />
    </FilterContainer>
  );
};

export default DateFilter;
