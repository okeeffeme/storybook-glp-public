type Selector = {
  value: string | number;
  label: string;
};

export type SingleChoiceFilterProps = {
  options: Selector[];
  isLoading?: boolean;
};
