import React, {useContext, useEffect, useMemo, useState} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {Radio} from '@jotunheim/react-toggle';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';

import FilterContext, {FilterDispatchContext} from '../context';
import {getParamsFromQuery} from '../utils/query-modifiers';
import {setFilterValid, updateQuery} from '../state/actions';
import FilterContainer from '../components/FilterContainer';
import {SingleChoiceFilterProps} from './types';
import {BusyDots} from '@jotunheim/react-busy-dots';

export type Selector = {
  value: string | number;
  label: string;
};

const SingleChoiceFilter = ({
  options,
  isLoading,
  ...rest
}: SingleChoiceFilterProps) => {
  const {queries, activeFilterId} = useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);

  const prevValue = useMemo(
    () => getParamsFromQuery(queries, activeFilterId),
    [queries, activeFilterId]
  );

  const [selectedValue, setSelectedValue] = useState(prevValue?.value);

  useEffect(() => {
    // Update Query and validate the form when any item is selected.
    if (selectedValue !== null && selectedValue !== undefined) {
      const selectedOption = options.find(
        (option) => option.value === selectedValue
      );
      dispatch(
        updateQuery({
          id: activeFilterId,
          value: selectedValue,
          label: selectedOption?.label,
        })
      );
      dispatch(setFilterValid(true));
    }
  }, [selectedValue, dispatch, activeFilterId, options]);

  return (
    <FilterContainer
      data-select-filter={activeFilterId}
      data-testid="select-filter-single-choice"
      {...extractDataAriaIdProps(rest)}>
      <Fieldset layoutStyle={LayoutStyle.Vertical}>
        {options.map(({value, label}) => (
          <Radio
            data-testid="filter-option-single-choice"
            data-filter-option={value}
            key={`key_${value}`}
            id={`id_${value}`}
            checked={selectedValue === value}
            onChange={() => setSelectedValue(value)}>
            {label}
          </Radio>
        ))}
        {isLoading && <BusyDots />}
      </Fieldset>
    </FilterContainer>
  );
};

export default SingleChoiceFilter;
