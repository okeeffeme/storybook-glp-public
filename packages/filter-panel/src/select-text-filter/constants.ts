import {Selector} from './types';

const equalSelector: Selector = [
  {value: 'eq', label: 'selectTextFilter.equals'},
];

const containsSelector: Selector = [
  {value: 'contains', label: 'selectTextFilter.contains'},
];

const compareSelector: Selector = [
  {value: 'eq', label: 'selectTextFilter.equals'},
  {value: 'gt', label: 'selectTextFilter.greaterThan'},
  {value: 'lt', label: 'selectTextFilter.lessThan'},
];

export const selectors = {equalSelector, compareSelector, containsSelector};
