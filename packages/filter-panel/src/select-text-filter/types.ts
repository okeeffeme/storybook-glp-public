type SelectorItem = {
  value: string;
  label: string;
  operator?: string;
};

export type Selector = SelectorItem[];

export type SelectTextFilterProps = {
  options: Selector;
  numberOnly?: boolean;
  id?: string;
};
