import React, {useContext, useState, useEffect, useMemo} from 'react';

import Select from '@jotunheim/react-select';
import TextField from '@jotunheim/react-text-field';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import Fieldset, {LayoutStyle} from '@jotunheim/react-fieldset';

import FilterContainer from '../components/FilterContainer';

import FilterContext, {FilterDispatchContext} from '../context';
import {getParamsFromQuery} from '../utils/query-modifiers';
import {setFilterValid, updateQuery} from '../state/actions';
import translate from '../translate';
import {SelectTextFilterProps} from './types';

const ENTER_KEY = 13;

const SelectTextFilter = ({
  options,
  numberOnly = false,
  ...rest
}: SelectTextFilterProps) => {
  const {queries, onApplyFilter, language, activeFilterId} =
    useContext(FilterContext);
  const dispatch = useContext(FilterDispatchContext);
  const prevValue = useMemo(
    () => getParamsFromQuery(queries, activeFilterId),
    [queries, activeFilterId]
  );
  const [operator, setOperator] = useState(
    prevValue?.operator || options[0].value
  );
  const [textValue, setTextValue] = useState(
    (prevValue?.value as string) || ''
  );
  const [textError, setTextError] = useState('');

  useEffect(() => {
    if (textValue) {
      dispatch(
        updateQuery({id: activeFilterId, operator, value: textValue.trim()})
      );
    } else {
      dispatch(updateQuery());
    }
  }, [operator, dispatch, activeFilterId, textValue]);

  useEffect(() => {
    if (
      textValue &&
      (prevValue?.operator !== operator || prevValue?.value !== textValue) &&
      !textError
    ) {
      dispatch(setFilterValid(true));
    } else {
      dispatch(setFilterValid(false));
    }
  }, [operator, dispatch, textValue, prevValue, textError]);

  useEffect(() => {
    if (textValue && numberOnly) {
      const error = /^\d+$/.test(textValue)
        ? ''
        : translate({language, key: 'panel.notAValidNumberError'});
      setTextError(error);
    } else {
      setTextError('');
    }
  }, [operator, numberOnly, textValue, setTextError, language]);

  return (
    <FilterContainer
      {...extractDataAriaIdProps(rest)}
      data-select-text-filter=""
      data-testid="select-text-filter">
      <Fieldset layoutStyle={LayoutStyle.Vertical}>
        {options.length > 0 && (
          <Select
            readOnly={options.length === 1}
            value={operator}
            onChange={setOperator}
            isMulti={false}
            options={options.map((option) => ({
              label: translate({key: option.label, language}),
              value: option.value,
              operator: option.operator,
            }))}
            data-testid="filter-select-value"
            data-filter-select-value={operator}
          />
        )}
        <TextField
          autoFocus={true}
          required={true}
          error={Boolean(textError)}
          helperText={textError}
          value={textValue}
          align="left"
          onKeyPress={(e) => {
            if (e.which === ENTER_KEY) {
              onApplyFilter();
            }
          }}
          onChange={setTextValue}
        />
      </Fieldset>
    </FilterContainer>
  );
};

export default SelectTextFilter;
