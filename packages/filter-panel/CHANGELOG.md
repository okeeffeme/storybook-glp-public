# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.70&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.71&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.70&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.70&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.68&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.69&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.67&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.68&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.66&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.67&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.65&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.66&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.64&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.65&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.63&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.64&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.62&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.63&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.61&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.62&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.60&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.61&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.59&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.60&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.58&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.59&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.57&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.58&targetRepoId=1246) (2023-02-27)

### Bug Fixes

- adding data-testid Filter-panel components ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([1184a4c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1184a4c02c96d705e86deb3dae64efcf9abc196f))
- adding data-testids in Filter-Panel components ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([c441a93](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c441a93179d2c91058578727b571dc762878b08d))
- adding data-testids in Filter-Panel components ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([6143b88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6143b88704339fd3870bba4c92385e3d878352fa))
- remove attribute MultiChoiceFilter ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([792aca6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/792aca6d3024ed52421c46d7aefc117b98db764d))

## [2.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.56&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.57&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.55&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.56&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.54&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.55&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.53&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.54&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.52&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.53&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.51&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.52&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.50&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.51&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.49&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.50&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.48&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.49&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.47&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.48&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.46&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.47&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.45&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.46&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.44&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.45&targetRepoId=1246) (2023-01-27)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.43&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.44&targetRepoId=1246) (2023-01-27)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.42&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.43&targetRepoId=1246) (2023-01-25)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.41&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.42&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.38&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.41&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.36&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.40&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.36&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.39&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.36&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.38&targetRepoId=1246) (2023-01-18)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.36&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.37&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.35&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.36&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.34&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.35&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.33&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.34&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.32&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.33&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.31&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.32&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.30&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.31&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.29&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.30&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.28&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.29&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.27&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.28&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.24&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.27&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.24&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.26&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.24&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.25&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.23&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.24&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.22&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.23&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.21&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.22&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.19&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.20&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.18&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.19&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.17&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.18&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.16&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.17&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.15&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.16&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.14&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.15&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.13&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.14&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.12&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.11&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.12&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.10&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.11&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.8&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.9&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.5&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.6&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.4&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.5&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.3&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.4&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.2&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.3&targetRepoId=1246) (2022-07-12)

### Bug Fixes

- stop federating unused components ([NPM-1040](https://jiraosl.firmglobal.com/browse/NPM-1040)) ([e54b864](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e54b864af94acbc92a3ad1605334f11dd963e48b))

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.1&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-filter-panel

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-filter-panel@2.0.0&sourceBranch=refs/tags/@jotunheim/react-filter-panel@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-filter-panel

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.12&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.13&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.11&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.12&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.10&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.11&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.9&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.10&targetRepoId=1246) (2022-06-02)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.8&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.9&targetRepoId=1246) (2022-05-30)

### Bug Fixes

- more efficient lodash import, reduce bundle size ([NPM-1020](https://jiraosl.firmglobal.com/browse/NPM-1020)) ([fa8ed95](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fa8ed9521e44770e1568f73dd6e9dd3bbbbe8a84))

## [1.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.7&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.8&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.6&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.7&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.5&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.6&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.5&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.2&targetRepoId=1246) (2022-04-21)

### Bug Fixes

- Move moment context into datepicker component ([NPM-852](https://jiraosl.firmglobal.com/browse/NPM-852)) ([76ffbff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/76ffbff5d8261392dd59cf52c2e7a550b134df5a))

## [1.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.2.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.2.1&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-filter-panel

## 1.2.0

Date filter

- UI reworked around select component
- Custom options support
- Fiscal calendar support
- Custom week rules support
- Extracted DatePicker component so it can be used separately

## [1.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.1.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.1.4&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.1.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.1.3&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.1.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.1.2&targetRepoId=1246) (2022-03-22)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [1.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.1.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.1.1&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [1.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.0.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.1.0&targetRepoId=1246) (2022-03-04)

### Bug Fixes

- change data attribute in MultiChoiceFilter to prevent a collision with MultiSelectFilter data attribute ([NPM-857](https://jiraosl.firmglobal.com/browse/NPM-857)) ([4e9f771](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4e9f771716238f4ab964de7cadf2b657bfcf7ce7))

### Features

- add support of controllable and uncontrollable behavoiur to filter panel ([NPM-857](https://jiraosl.firmglobal.com/browse/NPM-857)) ([2b770e4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2b770e49404a4621b9f8f831f1a47c0ac599badf))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@1.0.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.0.1&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.8.8&sourceBranch=refs/tags/@confirmit/react-filter-panel@1.0.0-alpha-NPM-973.0&targetRepoId=1246) (2022-03-01)

### Features

- Switch to a FilterType approach, no longer expect users to supply the actual jsx elements ([NPM-973])

  - filters are now defined like this instead (for example):
    ` { id: 'filter1', title: 'Filter 1', type: FilterType.SelectText, props: { options: [] } }`

- Export the Filter type, and the FilterType enum

### BREAKING CHANGES

- No longer export SelectTextFilter, MultiChoiceFilter, etc, users should set a .type on the filters (exposed via FilterType).
- No longer export SelectTextFlter.selectors, this is exported via SelectTextSelectors instead.

## [0.8.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.8.7&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.8.8&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.8.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.8.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.8.5&targetRepoId=1246) (2022-02-18)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.8.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.8.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.8.3&targetRepoId=1246) (2022-02-16)

### Bug Fixes

- Add more tests before migration to new component ([NPM-852](https://jiraosl.firmglobal.com/browse/NPM-852)) ([7c7f9e7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7c7f9e72db8183f1ebb85983184d8fa9f20ebb7b))

## [0.8.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.8.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.8.1&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.8.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.7.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.8.0&targetRepoId=1246) (2022-02-07)

### Features

- export federated Filter Panel ([NPM-920](https://jiraosl.firmglobal.com/browse/NPM-920)) ([ade7fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ade7fc665035869e9b793bfa95e04efd275775b9))

# [0.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.6.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.7.0&targetRepoId=1246) (2022-02-03)

### Bug Fixes

- remove FilterPanel.Filter export ([NPM-923](https://jiraosl.firmglobal.com/browse/NPM-923)) ([e2b1de2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e2b1de22a452b4ca56541beef8ea457cefb747f8))

### BREAKING CHANGES

- remove FilterPanel.Filter export (NPM-923)

## [0.6.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.6.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.6.3&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.6.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.6.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.6.3&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.6.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.6.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.6.2&targetRepoId=1246) (2022-01-31)

### Bug Fixes

- fix types for MultiSelectFilter ([NPM-922](https://jiraosl.firmglobal.com/browse/NPM-922)) ([a248d8a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a248d8a410c1316e31560f8730893ded14146992))

## [0.6.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.6.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.6.1&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.8&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.6.0&targetRepoId=1246) (2022-01-28)

### Features

- add loadOptions prop to MultiSelectFilter ([NPM-915](https://jiraosl.firmglobal.com/browse/NPM-915)) ([3da1e8c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3da1e8c7658514d2db7849354c6c0c94a545bae2))

## [0.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.7&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.8&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.6&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.7&targetRepoId=1246) (2022-01-20)

### Bug Fixes

- Padding adjustment for panel filter component ([NPM-770](https://jiraosl.firmglobal.com/browse/NPM-770)) ([c7fd8ae](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c7fd8ae4d8079a6d0e56cffb8a53310329a83526))

## [0.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.5&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.6&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.5&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.4&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-filter-panel

## 0.5.3 (2021-12-30)

### Bug Fixes

- Correcting bad assumption that pills should match operator, this should only be the case for SelectTextFilter

## [0.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.2&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.5.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.1&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.6&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.5.0&targetRepoId=1246) (2021-12-16)

### Features

- add defaultActiveFilterId to open filter panel by default and show certain filter ([NPM-900](https://jiraosl.firmglobal.com/browse/NPM-900)) ([7394741](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/73947415b82212a275eae308fbcdeec460f809a3))

## [0.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.5&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.6&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.5&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.4&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.3&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.2&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.4.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.1&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.3.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.4.0&targetRepoId=1246) (2021-11-10)

### Features

- Adding currently applied filter label to filter list ([NPM-856](https://jiraosl.firmglobal.com/browse/NPM-856)) ([167c209](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/167c2096672e6fe997f1d83b30cc06f6f340e3ba))
- Adding tests ([NPM-856](https://jiraosl.firmglobal.com/browse/NPM-856)) ([bff4cd6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bff4cd6dbc6f51246cbdbcedcdb15a5fee16f1be))

## [0.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.3.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.3.4&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.3.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.3.3&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.3.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.3.1&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.35&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.3.0&targetRepoId=1246) (2021-10-06)

### Features

- add the ability to render the filter bar and filter drawer separately from the filter panel ([NPM-854](https://jiraosl.firmglobal.com/browse/NPM-854)) ([e826fd5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e826fd58fad8aec9c98653ed4e7d03785039108c))

## [0.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.34&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.35&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.33&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.34&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.32&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.33&targetRepoId=1246) (2021-09-23)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.31&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.32&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.30&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.31&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.29&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.30&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.28&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.29&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.27&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.28&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.26&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.27&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.25&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.26&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.24&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.25&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.23&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.24&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.22&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.23&targetRepoId=1246) (2021-09-06)

### Bug Fixes

- handle case where time argument is undefined ([NPM-828](https://jiraosl.firmglobal.com/browse/NPM-828)) ([bb94dc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb94dc6270316983285470b401963c604f6d91d9))

## [0.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.21&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.22&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.20&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.21&targetRepoId=1246) (2021-08-09)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.19&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.20&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.18&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.19&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.17&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.18&targetRepoId=1246) (2021-07-27)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.16&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.17&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.15&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.16&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.14&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.15&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.13&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.14&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.12&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.13&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.10&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.11&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.9&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.10&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.8&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.9&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.7&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.8&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.6&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.7&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.5&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.6&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.5&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.4&targetRepoId=1246) (2021-06-02)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.3&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.2&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.2.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.1&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-filter-panel

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.19&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.2.0&targetRepoId=1246) (2021-05-26)

### Bug Fixes

- add translations for single text filter operators ([NPM-793](https://jiraosl.firmglobal.com/browse/NPM-793)) ([58d0726](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/58d0726be6a948da74d1587b7b9451fb49965683))

### Features

- add contains selector to SingleTextFilter ([NPM-793](https://jiraosl.firmglobal.com/browse/NPM-793)) ([fbf8f79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fbf8f791b318ae81ba2d6ba3e6636f424b076960))

## [0.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.18&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.17&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.18&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.16&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.17&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.14&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.15&targetRepoId=1246) (2021-05-05)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.13&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.14&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.12&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.13&targetRepoId=1246) (2021-04-22)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.11&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.12&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.10&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.11&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.9&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.10&targetRepoId=1246) (2021-04-14)

### Bug Fixes

- remove Title Case on labels and titles ([NPM-761](https://jiraosl.firmglobal.com/browse/NPM-761)) ([0213c34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0213c343d273533335a3a64b1c736f4b3823e2d1))

## [0.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.8&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.9&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.7&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.8&targetRepoId=1246) (2021-04-09)

### Bug Fixes

- get rid of React warning with newer versions ([NPM-758](https://jiraosl.firmglobal.com/browse/NPM-758)) ([0397387](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0397387a802c021171c3323fca0c515b4da74b00))

## [0.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.6&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.7&targetRepoId=1246) (2021-04-08)

### Bug Fixes

- if language is set to an unsupported language, filter panel UI will now correctly fallback to English translations ([NPM-755](https://jiraosl.firmglobal.com/browse/NPM-755)) ([66a41d7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/66a41d7d6794a58ee3d2f985f55f79d255952f33))

## [0.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.5&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.6&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.4&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.5&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.3&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.4&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.2&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.3&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.1&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.2&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-filter-panel

## [0.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-filter-panel@0.1.0&sourceBranch=refs/tags/@confirmit/react-filter-panel@0.1.1&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-filter-panel

## CHANGELOG

### v0.0.1

- Initial version
