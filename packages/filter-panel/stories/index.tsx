import React, {useState} from 'react';
import moment from 'moment';
import {select} from '@storybook/addon-knobs';
import {storiesOf} from '@storybook/react';
import Switch from '../../switch/src';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import FilterPanel, {
  DateInterval,
  DatePicker,
  FilterPanelContainer,
  Language,
  Query,
  SelectTextSelectors,
} from '../src';
import {Option} from '../src/multi-select-filter/types';
import {Filter, FilterType} from '../src/types';
import {fiscalCalendar} from './fiscalCalendar';
import {
  DateFilterValue,
  WeekRule,
  WeekStart,
} from '../src/components/date-picker/types';

const SelectOptions = [
  {
    label: 'Completed',
    value: 0,
  },
  {
    label: 'Error',
    value: 1,
  },
  {
    label: 'InProgress',
    value: 2,
  },
];

type FilterState = {
  id: string;
  options: Option[];
  isLoading: boolean;
};

const getCustomOperatorText = () => {
  return '=';
};

const Wrapper = ({children}: {children: React.ReactNode}) => (
  <div style={{height: '100vh'}}>{children}</div>
);

const filters: Filter[] = [
  {
    id: 'SelectTextContains',
    title: 'Text contains filter',
    type: FilterType.SelectText as const,
    props: {
      options: SelectTextSelectors.containsSelector,
    },
  },
  {
    id: 'SelectText',
    title: 'Text filter',
    type: FilterType.SelectText as const,
    props: {
      numberOnly: false,
      options: SelectTextSelectors.equalSelector,
    },
  },
  {
    id: 'SelectTextCustom',
    title: 'Text filter (with Custom pills)',
    getCustomOperatorText,
    type: FilterType.SelectText as const,
    props: {
      numberOnly: false,
      options: SelectTextSelectors.equalSelector,
    },
  },
  {
    id: 'SelectTextNumberOnly',
    title: 'Number filter',
    type: FilterType.SelectText as const,
    props: {
      numberOnly: true,
      options: SelectTextSelectors.compareSelector,
    },
  },
  {
    id: 'MultiChoice',
    title: 'Multi Choice filter',
    type: FilterType.MultiChoice as const,
    props: {
      options: SelectOptions,
      allowCustom: true,
      customPlaceholder: 'New Keyword',
      customHelpText:
        'Select keywords from list, or type to enter custom keywords.',
      onValidateCustom: (text) => {
        if (text.indexOf('<') > -1) {
          return 'Contains invalid character';
        }
        return '';
      },
    },
  },
  {
    id: 'SingleChoice',
    title: 'Single Choice filter',
    type: FilterType.SingleChoice as const,
    props: {
      options: SelectOptions,
    },
  },
  {
    id: 'MultiSelect',
    title: 'Multi Select filter',
    type: FilterType.MultiSelect as const,
    props: {
      allowCustom: true,
      customPlaceholder: 'Add custom: ',
      options: SelectOptions,
    },
  },
  {
    id: 'DateTimeFilter',
    title: 'Date Time filter',
    type: FilterType.DateAndTime as const,
  },
  {
    id: 'DateTimeFilter Custom Operator',
    title: 'Date Time filter with custom operator',
    getCustomOperatorText,
    type: FilterType.DateAndTime as const,
  },
  {
    id: 'DateFilter',
    title: 'Date filter',
    type: FilterType.Date as const,
  },
  {
    id: 'DateFilter-config',
    title: 'DateFilter (week start on Wed)',
    type: FilterType.Date as const,
    props: {
      calendarConfig: {
        weekStart: WeekStart.wednesday,
        weekRule: WeekRule.firstFourDayWeek,
      },
    },
  },
  {
    id: 'DateFilterFiscal',
    title: 'Date filter with Fiscal calendar',
    type: FilterType.Date as const,
    props: {
      calendarConfig: {
        fiscalCalendar,
      },
    },
  },
  {
    id: 'DateFilterWithCustomGroups',
    title: 'Date filter with custom groups',
    type: FilterType.Date as const,
    props: {
      customOptions: [{id: 'myGroup', label: 'End of season'}],
    },
  },
  {
    id: 'DateFilterWithAbsoluteOptionsHidden',
    title: 'Date filter with absolute options hidden',
    type: FilterType.Date as const,
    props: {
      customOptions: [{id: 'myGroup', label: 'End of season'}],
      optionFilter: (option) => option.period !== 'absolute',
    },
  },
];

storiesOf('Components/filter-panel', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [filterValue, setFilterValue] = useState<Query[]>([
      {
        id: 'DateFilterFiscal',
        value: {
          interval: DateInterval.Day,
          offset: 0,
          optionId: 'today',
          optionLabel: 'Today',
          period: 'relative',
          startDate: '2021-11-24T00:00:00.000',
        },
      },
    ]);

    const handleOnApply = (filter) => {
      const otherFilters = filterValue.filter(({id}) => id !== filter.id);

      setFilterValue([filter, ...otherFilters]);
    };

    const handleDeleteAll = () => {
      setFilterValue([]);
    };

    const handleDelete = (filterId) => {
      const newFilterList = filterValue.filter(({id}) => id !== filterId);
      setFilterValue(newFilterList);
    };

    const handleStatusToggle = (filterId) => {
      const updatedFilterList = filterValue.map((filter) => {
        if (filter.id === filterId) {
          return {...filter, isIdle: !filter.isIdle};
        }
        return filter;
      });
      setFilterValue(updatedFilterList);
    };

    return (
      <FilterPanel
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore  - language prop only accepts the Language type, but for non-TS users, you can pass any string
        language={select(
          'language',
          {...Language, 'non-translated': 'non-translated'},
          Language.EN
        )}
        onApply={handleOnApply}
        onStatusToggle={handleStatusToggle}
        value={filterValue}
        onDeleteAll={handleDeleteAll}
        filters={filters}
        onDelete={handleDelete}>
        {filterValue.length ? 'Filter(s) applied' : 'No Filters applied'}
      </FilterPanel>
    );
  })
  .add('Render the filter bar and filter drawer separately', () => {
    const [filterValue, setFilterValue] = useState<Query[]>([
      {
        id: 'DateTimeFilter',
        label: 'Today',
        operator: 'ge',
        value: {
          ge: new Date(
            Date.parse(
              moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format()
            )
          ),
        },
      },
    ]);

    const handleOnApply = (filter) => {
      const otherFilters = filterValue.filter(({id}) => id !== filter.id);

      setFilterValue([filter, ...otherFilters]);
    };

    const handleDeleteAll = () => {
      setFilterValue([]);
    };

    const handleDelete = (filterId) => {
      const newFilterList = filterValue.filter(({id}) => id !== filterId);
      setFilterValue(newFilterList);
    };

    const handleStatusToggle = (filterId) => {
      const updatedFilterList = filterValue.map((filter) => {
        if (filter.id === filterId) {
          return {...filter, isIdle: !filter.isIdle};
        }
        return filter;
      });
      setFilterValue(updatedFilterList);
    };

    return (
      <Wrapper>
        <FilterPanelContainer
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore  - language prop only accepts the Language type, but for non-TS users, you can pass any string
          language={select(
            'language',
            {...Language, 'non-translated': 'non-translated'},
            Language.EN
          )}
          onApply={handleOnApply}
          onStatusToggle={handleStatusToggle}
          value={filterValue}
          onDeleteAll={handleDeleteAll}
          filters={filters}
          onDelete={handleDelete}>
          <div style={{display: 'flex'}}>
            <FilterPanelContainer.Drawer />
            <div style={{width: '100%'}}>
              <FilterPanelContainer.Toolbar />
              <div style={{padding: '20px'}}>
                {filterValue.length
                  ? 'Filter(s) applied'
                  : 'No Filters applied'}
              </div>
            </div>
          </div>
        </FilterPanelContainer>
      </Wrapper>
    );
  })
  .add('With active by default filter', () => {
    const [filterValue, setFilterValue] = useState<Query[]>([
      {
        id: 'DateTimeFilter',
        label: 'Today',
        operator: 'ge',
        value: {
          ge: new Date(
            Date.parse(
              moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format()
            )
          ),
        },
      },
    ]);

    const handleOnApply = (filter) => {
      const otherFilters = filterValue.filter(({id}) => id !== filter.id);

      setFilterValue([filter, ...otherFilters]);
    };

    const handleDeleteAll = () => {
      setFilterValue([]);
    };

    const handleDelete = (filterId) => {
      const newFilterList = filterValue.filter(({id}) => id !== filterId);
      setFilterValue(newFilterList);
    };

    const handleStatusToggle = (filterId) => {
      const updatedFilterList = filterValue.map((filter) => {
        if (filter.id === filterId) {
          return {...filter, isIdle: !filter.isIdle};
        }
        return filter;
      });
      setFilterValue(updatedFilterList);
    };

    return (
      <Wrapper>
        <FilterPanel
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore - language prop only accepts the Language type, but for non-TS users, you can pass any string
          language={select(
            'language',
            {...Language, 'non-translated': 'non-translated'},
            Language.EN
          )}
          onApply={handleOnApply}
          onStatusToggle={handleStatusToggle}
          value={filterValue}
          onDeleteAll={handleDeleteAll}
          filters={filters}
          onDelete={handleDelete}
          defaultActiveFilterId={filters[0].id}>
          {filterValue.length ? 'Filter(s) applied' : 'No Filters applied'}
        </FilterPanel>
      </Wrapper>
    );
  })
  .add('Async filters loading', () => {
    const [filterValue, setFilterValue] = useState<Query[]>([]);
    const [filtersState, setFiltersState] = useState<FilterState[]>([
      {id: 'MultiChoice', options: [], isLoading: false},
      {id: 'SingleChoice', options: [], isLoading: false},
    ]);
    const [activeFilterId, setActiveFilterId] = useState('');

    const fetchOptionsAPI = (): Promise<Option[]> =>
      new Promise((resolve) => setTimeout(() => resolve(SelectOptions), 1500));

    const setOptions = async (id: string) => {
      setFiltersState((prevState) =>
        prevState.map((filterState) =>
          filterState.id === id
            ? {...filterState, isLoading: true}
            : filterState
        )
      );
      const options = await fetchOptionsAPI();
      setFiltersState((prevState) =>
        prevState.map((filterState) =>
          filterState.id === id
            ? {...filterState, options, isLoading: false}
            : filterState
        )
      );
    };

    const filters = [
      {
        id: 'MultiChoice',
        title: 'Multi Choice filter',
        type: FilterType.MultiChoice as const,
        props: {
          options: filtersState.find(({id}) => id === 'MultiChoice')
            ?.options as Option[],
          isLoading: filtersState.find(({id}) => id === 'MultiChoice')
            ?.isLoading,
          allowCustom: true,
          customPlaceholder: 'New Keyword',
          customHelpText:
            'Select keywords from list, or type to enter custom keywords.',
          onValidateCustom: (text) => {
            if (text.indexOf('<') > -1) {
              return 'Contains invalid character';
            }
            return '';
          },
        },
      },
      {
        id: 'SingleChoice',
        title: 'Single Choice filter',
        type: FilterType.SingleChoice as const,
        props: {
          options: filtersState.find(({id}) => id === 'SingleChoice')
            ?.options as Option[],
          isLoading: filtersState.find(({id}) => id === 'SingleChoice')
            ?.isLoading,
        },
      },
    ];

    const handleOnChangeActiveFilter = (id: string) => {
      if (!filtersState.find((s) => s.id === id)?.options.length) {
        setOptions(id);
      }
      setActiveFilterId(id);
    };

    const handleOnApply = (filter) => {
      const otherFilters = filterValue.filter(({id}) => id !== filter.id);

      setFilterValue([filter, ...otherFilters]);
    };

    const handleDeleteAll = () => {
      setFilterValue([]);
    };

    const handleDelete = (filterId) => {
      const newFilterList = filterValue.filter(({id}) => id !== filterId);
      setFilterValue(newFilterList);
    };

    const handleStatusToggle = (filterId) => {
      const updatedFilterList = filterValue.map((filter) => {
        if (filter.id === filterId) {
          return {...filter, isIdle: !filter.isIdle};
        }
        return filter;
      });
      setFilterValue(updatedFilterList);
    };

    return (
      <Wrapper>
        <FilterPanel
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore  - language prop only accepts the Language type, but for non-TS users, you can pass any string
          language={select(
            'language',
            {...Language, 'non-translated': 'non-translated'},
            Language.EN
          )}
          onApply={handleOnApply}
          onStatusToggle={handleStatusToggle}
          value={filterValue}
          onDeleteAll={handleDeleteAll}
          filters={filters}
          onDelete={handleDelete}
          activeFilterId={activeFilterId}
          onChangeActiveFilterId={handleOnChangeActiveFilter}>
          {filterValue.length ? 'Filter(s) applied' : 'No Filters applied'}
        </FilterPanel>
      </Wrapper>
    );
  })
  .add('With loadOptions on MultiSelect', () => {
    const fetchOptionsAPI = (
      inputValue,
      prevOptions,
      {page}
    ): Promise<{
      options: Option[];
      hasMore: boolean;
      additional: {page: number};
    }> =>
      new Promise((resolve) => {
        setTimeout(() => {
          resolve({
            options: [SelectOptions[page - 1]],
            hasMore: page !== SelectOptions.length,
            additional: {
              page: page + 1,
            },
          });
        }, 1000);
      });

    const multiSelectFilterWithLoadOptions = {
      id: 'MultiSelectWithLoadOptions',
      title: 'Multi Select filter with loadOptions',
      type: FilterType.MultiSelect as const,
      props: {
        allowCustom: true,
        customPlaceholder: 'Add custom: ',
        loadOptions: fetchOptionsAPI,
      },
    };

    const [filterValue, setFilterValue] = useState<Query[]>([
      {
        id: 'DateTimeFilter',
        label: 'Today',
        operator: 'ge',
        value: {
          ge: new Date(
            Date.parse(
              moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format()
            )
          ),
        },
      },
    ]);

    const handleOnApply = (filter) => {
      const otherFilters = filterValue.filter(({id}) => id !== filter.id);

      setFilterValue([filter, ...otherFilters]);
    };

    const handleDeleteAll = () => {
      setFilterValue([]);
    };

    const handleDelete = (filterId) => {
      const newFilterList = filterValue.filter(({id}) => id !== filterId);
      setFilterValue(newFilterList);
    };

    const handleStatusToggle = (filterId) => {
      const updatedFilterList = filterValue.map((filter) => {
        if (filter.id === filterId) {
          return {...filter, isIdle: !filter.isIdle};
        }
        return filter;
      });
      setFilterValue(updatedFilterList);
    };

    return (
      <Wrapper>
        <FilterPanel
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore - language prop only accepts the Language type, but for non-TS users, you can pass any string
          language={select(
            'language',
            {...Language, 'non-translated': 'non-translated'},
            Language.EN
          )}
          onApply={handleOnApply}
          onStatusToggle={handleStatusToggle}
          value={filterValue}
          onDeleteAll={handleDeleteAll}
          filters={[multiSelectFilterWithLoadOptions]}
          onDelete={handleDelete}>
          {filterValue.length ? 'Filter(s) applied' : 'No Filters applied'}
        </FilterPanel>
      </Wrapper>
    );
  })
  .add('DatePicker component can be used separately', () => {
    const [isClearable, setIsClearable] = useState(true);
    const [disabled, setDisabled] = useState(false);
    const [fiscal, setFiscal] = useState(false);
    const [value, setValue] = useState<DateFilterValue | undefined>();
    return (
      <div>
        <Switch
          label="Is Clearable"
          checked={isClearable}
          onChange={(e) => {
            setIsClearable(e.target.checked);
          }}
        />
        <Switch
          label="Disabled"
          checked={disabled}
          onChange={(e) => {
            setDisabled(e.target.checked);
          }}
        />
        <Switch
          label="Fiscal calendar"
          checked={fiscal}
          onChange={(e) => {
            setFiscal(e.target.checked);
          }}
        />
        <DatePicker
          language={Language.EN}
          onValueChange={setValue}
          selectedValue={value}
          isClearable={isClearable}
          disabled={disabled}
          customOptions={[{id: 'my', label: 'My custom option'}]}
          calendarConfig={fiscal ? {fiscalCalendar} : undefined}
        />

        {value && (
          <div>
            <div>Current value</div>
            {JSON.stringify(value)}
          </div>
        )}
      </div>
    );
  });
