# Jotunheim React Filter Panel

[Changelog](./CHANGELOG.md)

Filter panel on a page ( or Tables ).

## Description

`FilterPanel` is high-level component, that helps to build filter drawer and toolbar from scratch.
`FilterPanelContainer` is low-level component, that provides only context. For building visual elements use `FilterPanelContainer.Drawer` and `FilterPanelContainer.Toolbar`.
