# Jotunheim React Badge

[Changelog](./CHANGELOG.md)

Notification badges appear overlapping or near the item to which they refer and contain a numeric count. These badges have a maximum value of 9, and higher counts are represented as '9+'. When the count is 0, the badge is not displayed.

On hover, the cursor should change to a pointer to indicate the badge is clickable.
