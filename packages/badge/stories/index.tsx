import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import Icon, {bellCircle} from '../../icons/src/index';
import {IconButton} from '../../button/src';
import {number} from '@storybook/addon-knobs';

import Badge from '../src';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{padding: '24px'}}>{children}</div>
);

storiesOf('Components/Badge', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default with knobs', () => (
    <Container>
      <Badge value={number('value', 10)}>
        <IconButton>
          <Icon path={bellCircle} />
        </IconButton>
      </Badge>
    </Container>
  ));
