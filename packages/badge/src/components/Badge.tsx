import React, {ReactNode} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import formatValue from '../utils/formatValue';

import classNames from './Badge.module.css';

const {block, element} = bemFactory('comd-badge', classNames);

export const Badge = ({children, value, ...rest}: BadgeProps) => {
  return (
    <div
      className={block()}
      data-testid="badge"
      {...extractDataAriaIdProps(rest)}>
      {children}
      {value > 0 && (
        <span className={element('overlay')} data-react-badge={true}>
          {formatValue(value)}
        </span>
      )}
    </div>
  );
};

export type BadgeProps = {
  children: ReactNode;
  value: number;
};

export default Badge;
