import Badge from './components/Badge';

export type {BadgeProps} from './components/Badge';
export {Badge} from './components/Badge';

export default Badge;
