import {BADGE_MAX_VALUE} from './constants';

const formatValue = (value, max = BADGE_MAX_VALUE) => {
  if (value > max) {
    return `${max}+`;
  }
  return value;
};

export default formatValue;
