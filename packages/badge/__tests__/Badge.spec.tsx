import React from 'react';
import {render, screen} from '@testing-library/react';
import Badge from '../src';
import {IconButton} from '../../button/src';
import Icon, {bellCircle} from '../../icons/src';
import {BADGE_MAX_VALUE} from '../src/utils/constants';

describe('Badge', () => {
  it('should render badge with text of 9', () => {
    const maxValue = BADGE_MAX_VALUE;
    const expectedString = `${BADGE_MAX_VALUE}`;
    render(
      <Badge value={maxValue}>
        <IconButton>
          <Icon path={bellCircle} />
        </IconButton>
      </Badge>
    );

    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
    expect(screen.getByText(expectedString)).toBeInTheDocument();
  });

  it('should render badge with text of 9+', () => {
    const exceedsMaxValue = BADGE_MAX_VALUE + 1;
    const expectedString = `${BADGE_MAX_VALUE}+`;
    render(
      <Badge value={exceedsMaxValue}>
        <IconButton>
          <Icon path={bellCircle} />
        </IconButton>
      </Badge>
    );
    expect(screen.getByText(expectedString)).toBeInTheDocument();
  });

  it('should not render badge if value is 0', () => {
    render(
      <Badge value={0}>
        <IconButton>
          <Icon path={bellCircle} />
        </IconButton>
      </Badge>
    );
    expect(screen.queryByText('data-react-badge')).not.toBeInTheDocument();
  });
});
