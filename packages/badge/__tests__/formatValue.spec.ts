import {BADGE_MAX_VALUE} from '../src/utils/constants';
import formatValue from '../src/utils/formatValue';

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

describe('formatValue', () => {
  it('should return max value', () => {
    const maxValue = BADGE_MAX_VALUE;
    expect(formatValue(maxValue)).toBe(maxValue);
  });

  it('if value exceeds max value, should return a string with text of {BADGE_MAX_VALUE+}', () => {
    const exceedsMaxValue = BADGE_MAX_VALUE + 1;
    const expectedString = `${BADGE_MAX_VALUE}+`;
    expect(formatValue(exceedsMaxValue)).toBe(expectedString);
  });

  it('should return valid value', () => {
    const value = getRandomInt(BADGE_MAX_VALUE);
    expect(formatValue(value)).toBe(value);
  });

  it('should accept custom max value', () => {
    const customMaxValue = getRandomInt(BADGE_MAX_VALUE) + BADGE_MAX_VALUE;
    const value = getRandomInt(customMaxValue);
    expect(formatValue(value, customMaxValue)).toBe(value);
  });

  it('if value exceeds max value, should return a string with text of {customMaxValue+}', () => {
    const customMaxValue = getRandomInt(BADGE_MAX_VALUE) + BADGE_MAX_VALUE;
    const value = customMaxValue + 1;
    const expectedString = `${customMaxValue}+`;
    expect(formatValue(value, customMaxValue)).toBe(expectedString);
  });
});
