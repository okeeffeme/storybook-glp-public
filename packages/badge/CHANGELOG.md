# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@4.0.3&sourceBranch=refs/tags/@jotunheim/react-badge@4.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-badge

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@4.0.2&sourceBranch=refs/tags/@jotunheim/react-badge@4.0.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@4.0.1&sourceBranch=refs/tags/@jotunheim/react-badge@4.0.2&targetRepoId=1246) (2022-10-18)

### Bug Fixes

- fix Badge to use css instead of scss ([NPM-1105](https://jiraosl.firmglobal.com/browse/NPM-1105)) ([6e8990e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6e8990eb57fdad233d44c8e8950063fd67d481fa))

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@4.0.0&sourceBranch=refs/tags/@jotunheim/react-badge@4.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-badge

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@3.0.1&sourceBranch=refs/tags/@jotunheim/react-badge@4.0.0&targetRepoId=1246) (2022-10-11)

### Features

- convert Badge to TypeScript ([NPM-1083](https://jiraosl.firmglobal.com/browse/NPM-1083)) ([576bcc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/576bcc6bef61e7d716be77fa0e3603f8b18961e6))
- remove className prop of Badge ([NPM-927](https://jiraosl.firmglobal.com/browse/NPM-927)) ([442fcc5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/442fcc5276285eed6ca1bbf77202823ae207e487))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-badge@3.0.0&sourceBranch=refs/tags/@jotunheim/react-badge@3.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-badge

## 3.0.0 (2022-06-29)

### Bug Fixes

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [2.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-badge@2.0.4&sourceBranch=refs/tags/@confirmit/react-badge@2.0.5&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-badge@2.0.3&sourceBranch=refs/tags/@confirmit/react-badge@2.0.4&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-badge

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-badge@2.0.2&sourceBranch=refs/tags/@confirmit/react-badge@2.0.3&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-badge@2.0.1&sourceBranch=refs/tags/@confirmit/react-badge@2.0.2&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-badge

## [2.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-badge@2.0.0...@confirmit/react-badge@2.0.1) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-badge

## CHANGELOG

### v2.0.0

- **BREAKING**: Moving Dot and Counter to seperate packages, `@confirmit/react-dot` and `@confirmit/react-counter` respectively
- Refactor: Moving `formatValue` to `@confirmit/react-utils`.

### v1.1.0

- Feat: Adding Counter

### v1.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v0.2.0

- Render only badge children when value prop is 0, undefined, or null

### v0.1.0

- Removing package version in class names.

### v0.0.1

- Added Badge component. Badge in this version only supports IconButton with standard 24px Icon size as child
- Added Dot component
