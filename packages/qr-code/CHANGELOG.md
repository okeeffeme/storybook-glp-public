# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-qr-code@2.0.3&sourceBranch=refs/tags/@jotunheim/react-qr-code@2.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-qr-code

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-qr-code@2.0.2&sourceBranch=refs/tags/@jotunheim/react-qr-code@2.0.3&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-qr-code

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-qr-code@2.0.1&sourceBranch=refs/tags/@jotunheim/react-qr-code@2.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-qr-code

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-qr-code@2.0.0&sourceBranch=refs/tags/@jotunheim/react-qr-code@2.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-qr-code

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-qr-code@1.1.0&sourceBranch=refs/tags/@confirmit/react-qr-code@1.1.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [1.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-qr-code@1.0.2&sourceBranch=refs/tags/@confirmit/react-qr-code@1.1.0&targetRepoId=1246) (2022-05-05)

### Features

- add ability to download QR code image ([AUT-6206](https://jiraosl.firmglobal.com/browse/AUT-6206)) ([5192cf3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5192cf3c03833854570125a771a36d50997067d9))

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-qr-code@1.0.1&sourceBranch=refs/tags/@confirmit/react-qr-code@1.0.2&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-qr-code

## CHANGELOG

### v1.0.1

- Updating qrcode package to v1.5.0

### v0.0.1

- Initial version
