import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import Button from '../../button/src';

import QrCode, {downloadQrCode} from '../src';

storiesOf('Components/qr-code', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => (
    <>
      <QrCode value={'https://www.confirmit.com/'} />
      <QrCode
        value={'https://www.confirmit.com/'}
        background={'#ffb700'}
        foreground={'#ff0000'}
        size={256}
        padding={2}
      />
    </>
  ))
  .add('Download to browser', () => {
    return (
      <>
        <Button
          onClick={() => {
            downloadQrCode({
              value: 'www.forsta.com',
              fileName: 'code.png',
              size: 1000,
            });
          }}>
          Download png
        </Button>
      </>
    );
  });
