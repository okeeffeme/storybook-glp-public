import React from 'react';
import {render, screen} from '@testing-library/react';
import {QrCode as Component} from '../../src/components/QrCode';
import QRCode from 'qrcode';

const mockedToDataURL = (value, opts, cb) => cb(undefined, value);

jest.spyOn(QRCode, 'toDataURL').mockImplementation(mockedToDataURL);

describe('Jotunheim React Qr Code :: ', () => {
  it('render qrCode two times, image is properly re-rendered', () => {
    let value = 'https://www.confirmit.com';
    const {rerender} = render(<Component value={value} />);

    expect(screen.queryByTestId('qrCode')).toHaveAttribute('src', value);

    value = 'https://www.forsta.com';

    rerender(<Component value={value} />);
    expect(screen.queryByTestId('qrCode')).toHaveAttribute(
      'src',
      'https://www.forsta.com'
    );
  });
});
