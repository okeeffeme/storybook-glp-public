# Jotunheim React Qr Code

[Changelog](./CHANGELOG.md)

This component is based on `qrcode` package. Given a string, it will generate a QR code image.

## Available Props and default values are

background: (string) "#ffffff"

foreground: (string) "#000000"

padding: (number) 0

size: (number) 100
