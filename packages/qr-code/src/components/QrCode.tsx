import React, {AriaAttributes, useEffect, useState} from 'react';
import cn from 'classnames';
import qrcodeLib from 'qrcode';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

const {block} = bemFactory({
  baseClassName: 'comd-qr-code',
});

export const defaultBackground = '#ffffff';
export const defaultForeground = '#000000';
export const defaultLevel = 'M';
export const defaultPadding = 0;
export const defaultSize = 128;

export type QrCodeProps = {
  value: string;
  background?: string;
  foreground?: string;
  level?: 'L' | 'M' | 'Q' | 'H';
  padding?: number;
  size?: number;
} & AriaAttributes;

export const QrCode = ({
  value,
  background = defaultBackground,
  foreground = defaultForeground,
  level = defaultLevel,
  padding = defaultPadding,
  size = defaultSize,
  ...restProps
}: QrCodeProps) => {
  const classes = cn(block());

  const [dataUrl, setDataUrl] = useState('');

  useEffect(() => {
    qrcodeLib.toDataURL(
      value,
      {
        type: 'image/png',
        level: level,
        margin: padding,
        width: size,
        color: {
          dark: foreground,
          light: background,
        },
      },
      function (error, url: string) {
        if (error) throw error;
        setDataUrl(url);
      }
    );
  }, [background, foreground, level, padding, size, value]);

  return (
    <div className={classes} {...extractDataAriaIdProps(restProps)}>
      <img data-testid="qrCode" src={dataUrl} aria-hidden="true" />
    </div>
  );
};

export default QrCode;
