import qrcodeLib from 'qrcode';
import {
  defaultBackground,
  defaultForeground,
  defaultLevel,
  defaultPadding,
  defaultSize,
  QrCodeProps,
} from './QrCode';

const downloadFileByUrl = ({url, fileName = 'qrcode.png'}) => {
  const element = document.createElement('a');
  element.setAttribute('href', url);
  element.setAttribute('download', fileName);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
};

type DownloadQrCodeProps = QrCodeProps & {fileName?: string};

const downloadQrCode = ({
  value,
  background = defaultBackground,
  foreground = defaultForeground,
  level = defaultLevel,
  padding = defaultPadding,
  size = defaultSize,
  fileName,
}: DownloadQrCodeProps) => {
  qrcodeLib.toDataURL(
    value,
    {
      type: 'image/png',
      level: level,
      margin: padding,
      width: size,
      color: {
        dark: foreground,
        light: background,
      },
    },
    function (err, url) {
      if (err) throw err;
      downloadFileByUrl({url: url, fileName});
    }
  );
};

export default downloadQrCode;
