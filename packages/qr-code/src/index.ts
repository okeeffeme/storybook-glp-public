import QrCode from './components/QrCode';
import downloadQrCode from './components/DownloadQrCode';

export {downloadQrCode};
export default QrCode;
