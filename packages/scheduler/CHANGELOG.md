# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.7&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.8&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.7&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.7&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.5&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.6&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.4&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.5&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.3&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.4&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.2&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.3&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.2&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.4.0&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.1&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-scheduler

# [12.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.22&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.4.0&targetRepoId=1246) (2023-03-10)

### Features

- add prop to hide recurrence ([NPM-1273](https://jiraosl.firmglobal.com/browse/NPM-1273)) ([fe80794](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fe80794f2d7a0695def1db2ebd4b938025eaabc4))

## [12.3.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.21&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.22&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.20&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.21&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.19&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.20&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.18&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.19&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.17&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.18&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.16&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.17&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.15&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.16&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.14&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.15&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.13&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.14&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.12&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.13&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.11&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.12&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.10&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.11&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.9&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.10&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.8&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.9&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.7&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.8&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.6&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.7&targetRepoId=1246) (2023-01-27)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.5&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.6&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.4&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.5&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.2&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.4&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.2&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.3&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.2&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- add test id for Scheduler component within scheduler package ([NPM-1216](https://jiraosl.firmglobal.com/browse/NPM-1216)) ([4a7098d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4a7098db8fa81a917a588d0dbbb88ef20badeb60))

## [12.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.3.0&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.1&targetRepoId=1246) (2023-01-18)

**Note:** Version bump only for package @jotunheim/react-scheduler

# [12.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.2.3&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.3.0&targetRepoId=1246) (2023-01-16)

### Features

- expose summary text builder function, expose some types ([NPM-1174](https://jiraosl.firmglobal.com/browse/NPM-1174)) ([404e333](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/404e33393e9192d18d856716debb8ae631ef592b))

## [12.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.2.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.2.3&targetRepoId=1246) (2023-01-12)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.2.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.2.2&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.2.0&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.2.1&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-scheduler

# [12.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.33&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.2.0&targetRepoId=1246) (2023-01-10)

### Features

- Migrate react-scheduler from js to ts ([NPM-1222](https://jiraosl.firmglobal.com/browse/NPM-1222)) ([0b2e8f0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0b2e8f0a8202e88c6f9a6165387b8cf567752eaf))

## [12.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.32&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.33&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.31&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.32&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.30&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.31&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.29&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.30&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.28&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.29&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.27&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.28&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.26&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.27&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.23&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.26&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.23&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.25&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.23&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.24&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.22&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.23&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.21&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.22&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.20&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.21&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.18&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.19&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.17&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.18&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.16&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.15&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.14&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.13&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.12&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.11&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.10&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.9&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.7&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.4&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.3&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.2&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-scheduler

## [12.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.2&targetRepoId=1246) (2022-07-12)

### Bug Fixes

- stop federating unused components ([NPM-1040](https://jiraosl.firmglobal.com/browse/NPM-1040)) ([e54b864](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e54b864af94acbc92a3ad1605334f11dd963e48b))

## [12.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.1.0&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-scheduler

# [12.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.0.1&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.1.0&targetRepoId=1246) (2022-07-04)

### Features

- add read only mode to scheduer component ([NPM-1037](https://jiraosl.firmglobal.com/browse/NPM-1037)) ([d957a9a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d957a9abb9e2301acc3e59f617b49027ac2700a8))

## [12.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scheduler@12.0.0&sourceBranch=refs/tags/@jotunheim/react-scheduler@12.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-scheduler

# 12.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [11.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.19&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.20&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.18&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.19&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.17&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.18&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.16&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.17&targetRepoId=1246) (2022-06-02)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.15&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.16&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [11.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.14&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.15&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.13&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.14&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.12&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.13&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.10&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.11&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.9&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.10&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.8&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.9&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.7&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.8&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.6&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.7&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.5&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.6&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.1.0&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.1&targetRepoId=1246) (2022-02-08)

**Note:** Version bump only for package @confirmit/react-scheduler

# [11.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.31&sourceBranch=refs/tags/@confirmit/react-scheduler@11.1.0&targetRepoId=1246) (2022-02-07)

### Features

- federate Scheduler ([NPM-920](https://jiraosl.firmglobal.com/browse/NPM-920)) ([292704c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/292704c2ed8a65a59e447661a442d6838bfb1a62))

## [11.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.30&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.31&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.29&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.30&targetRepoId=1246) (2022-01-28)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.28&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.29&targetRepoId=1246) (2022-01-25)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.27&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.28&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.26&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.27&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.25&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.26&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.24&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.25&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.23&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.24&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.22&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.23&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.21&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.22&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.20&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.21&targetRepoId=1246) (2021-12-07)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.19&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.20&targetRepoId=1246) (2021-11-30)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.18&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.19&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.17&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.18&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.16&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.17&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [11.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.15&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.16&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.13&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.14&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.12&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.13&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.11&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.12&targetRepoId=1246) (2021-09-27)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.10&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.11&targetRepoId=1246) (2021-09-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.9&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.10&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.8&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.9&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.7&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.8&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.6&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.7&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.5&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.6&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.4&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.5&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.3&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.4&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.2&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.3&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.1&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.2&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-scheduler

## [11.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@11.0.0&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.1&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-scheduler

# [11.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.57&sourceBranch=refs/tags/@confirmit/react-scheduler@11.0.0&targetRepoId=1246) (2021-07-27)

### Bug Fixes

- date format for date fields has been changed to DD MMM YYYY to be aligned with the Design System spec. ([NPM-497](https://jiraosl.firmglobal.com/browse/NPM-497)) ([c2065fe](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c2065fe57f7ad4760c6e052a3ebc07537afb44c1))

### BREAKING CHANGES

- The default format for scheduler has changed from L to DD MMM YYYY. This may cause issues f.ex in tests where they expect the date to be formatted in a certain way.

## [10.1.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.56&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.57&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.55&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.56&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.54&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.55&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.53&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.54&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.52&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.53&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.51&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.52&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.50&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.51&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.49&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.50&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.48&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.49&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.47&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.48&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.46&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.47&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.45&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.46&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.44&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.45&targetRepoId=1246) (2021-06-02)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.43&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.44&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.42&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.43&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.41&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.42&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.40&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.41&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.39&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.40&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.38&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.39&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.36&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.37&targetRepoId=1246) (2021-05-12)

### Bug Fixes

- move call to onValidationChange to separate useEffect to avoid React warning about 'Cannot update a component from inside the function body of a different component.' ([NPM-767](https://jiraosl.firmglobal.com/browse/NPM-767)) ([3108fb1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3108fb1789aacb7c558d5d90252b9510a9590d99))

## [10.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.35&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.36&targetRepoId=1246) (2021-05-05)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.34&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.35&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.33&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.34&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.32&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.33&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.31&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.32&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.30&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.31&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [10.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.29&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.30&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.28&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.29&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.27&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.28&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.26&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.27&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.25&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.26&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.24&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.25&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.23&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.24&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.22&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.23&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.21&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.22&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.20&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.21&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.19&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.20&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.18&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.19&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.17&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.18&targetRepoId=1246) (2021-03-09)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.16&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.17&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.15&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.16&targetRepoId=1246) (2021-02-22)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.14&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.15&targetRepoId=1246) (2021-02-19)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.12&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.13&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.11&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.12&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.9&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.10&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.8&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.9&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.7&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.8&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.5&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.6&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.4&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.5&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.3&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.4&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.2&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.3&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.1&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.2&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.1.0&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.1&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-scheduler

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.17&sourceBranch=refs/tags/@confirmit/react-scheduler@10.1.0&targetRepoId=1246) (2020-12-14)

### Bug Fixes

- also check startDateTime when modifying time, to be able to check if combination of date and time is before minDateTime ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([f4d835b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f4d835bb86ee93db683404d020bbb1ffaa72509b))
- change date and time validation, so date only validates date, and time validates both time and date, in cases of minDate is passed ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([75b571a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/75b571ace938a82c3e8c987783900b4069c6d0c8))
- do not try to add time to the date unless the time is correctly defined, to avoid throwing exception when changing time input ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([4013bee](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4013bee4bbd1d0396fde699ee1985eb5531d10a0))
- initial weekday when not set via prop did not select correct day of week in cases where moment was initialized in a different language than English ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([56c6bcb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/56c6bcb3743e813cc649007e6578f3c631f8c38c))
- typo in translation key ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([b5618d7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b5618d79286918af7f0a66fa9248de64e91376f2))
- weekday names in summary was not able to be translated ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([6859006](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6859006fd091fcfe4039d4cb45405b0fad9abf6c))

### Features

- minDateTime and showValidationErrors props ([NPM-647](https://jiraosl.firmglobal.com/browse/NPM-647)) ([fea761e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fea761ef672e9711670aa758076bb2d944bc6546))

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.16&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.15&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.14&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.13&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.12&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.11&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.10&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.9&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.6&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.3&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.2&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-scheduler

## [10.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@10.0.1&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-scheduler

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@9.0.1&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@9.0.1&sourceBranch=refs/tags/@confirmit/react-scheduler@10.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@9.0.1&sourceBranch=refs/tags/@confirmit/react-scheduler@9.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-scheduler

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.28&sourceBranch=refs/tags/@confirmit/react-scheduler@9.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [8.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.27&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.28&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.26&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.27&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.24&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.25&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.23&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.24&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.20&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.21&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.19&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.20&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.17&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.18&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.13&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.14&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.12&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.13&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.10&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.11&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.9&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.10&targetRepoId=1246) (2020-09-17)

### Bug Fixes

- always call onChange callback in scheduler so that even if form is invalid the updated value is passed to the consumer. ([NPM-536](https://jiraosl.firmglobal.com/browse/NPM-536)) ([069b064](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/069b06452fa369fd5527bdd6ed8d543fa0171e3f)
- remove extra spacing on summary ([NPM-536](https://jiraosl.firmglobal.com/browse/NPM-536)) ([00fb531](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/00fb531e920a5dbe73f964fb3e70d2da890b009e)

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.6&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.7&targetRepoId=1246) (2020-09-15)

### Bug Fixes

- resolve styles collision ([NPM-529](https://jiraosl.firmglobal.com/browse/NPM-529)) ([642d65f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/642d65f67cff3037cdffd3e19e072bcca75877f8)

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scheduler@8.0.5&sourceBranch=refs/tags/@confirmit/react-scheduler@8.0.6&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@8.0.2...@confirmit/react-scheduler@8.0.3) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@8.0.0...@confirmit/react-scheduler@8.0.1) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-scheduler

## [8.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@7.0.19...@confirmit/react-scheduler@8.0.0) (2020-08-20)

- **BREAKING**: `onScheduleChanged` renamed to `onScheduleChange`, `onValidationChanged` renamed to `onValidationChange`
- `onValidationChange` is called with `{isValid: Boolean}`
- Added material theme
- Added `texts` prop for localized texts (default provided)
- Added `showDateTimes` prop (defaults `true`)
- Recurrence modes object exposed as part of Scheduler as Scheduler.recurrenceModes
- `minDate` prop usage removed from DatePicker dependency

## [7.0.23](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@7.0.22...@confirmit/react-scheduler@7.0.23) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-scheduler

## [7.0.19](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@7.0.18...@confirmit/react-scheduler@7.0.19) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-scheduler

## [7.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-scheduler@7.0.17...@confirmit/react-scheduler@7.0.18) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-scheduler

## CHANGELOG

## v7.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-scheduler`

### v6.1.0

- Removing package version in class names.

### v6.0.5

- Fix: weekDays should not be mutated when creating summary text

### v6.0.0

- **BREAKING**:
  - Major UI changes to @confirmit/react-text-field

### v5.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v4.1.33

- Fix: update sort function to always return 1, -1 or 0. Otherwise the result of sort is unpredictable.

### v4.0.0

- **BREAKING**
  - Removed themes/default.js and themes/material.js. Theme should be set via new Context API, see confirmit-themes readme.
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v3.1.5

- chore: make peerDependencies less strict

### v3.1.0

- Fix: (default-theme) - adjust styles to match Bootstrap style.
- Fix: (default-theme) - theme styles not applied in some cases.
- Refactor: Use TextField component instead of input html element with custom styling.
- Chore: update to latest date-picker and confirmit-react-components package

### v3.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v2.0.13

- Fix: add missing peerDependency (moment)

### v2.0.0

- ?

### v1.0.19

- Fix: explicitly import and export to avoid uglifyjs issue

### v1.0.8

- Fix: hide end date and do not trigger end date validation for one-time mode

### v1.0.7

- Fix bug in test

### v0.0.1

- Initial version
