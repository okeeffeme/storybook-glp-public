import {InputName, RecurrenceMode, WeekDay} from '../../src/types';
import {defaultSchedulerTexts, validateSchedulerProps} from '../../src';

describe('validateSchedulerProps', () => {
  describe('OneTime recurrence', () => {
    it('should generate errors if startDateTime is missing', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.OneTime,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
      });
    });

    it('should generate errors if startDateTime is incorrect', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.OneTime,
        startDateTime: 'No idea',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
      });
    });

    it('should not generate errors startDateTime and time are missing and showDateTimes is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.OneTime,
        showDateTimes: false,
      });

      expect(errors).toBeUndefined();
    });

    it('should generate errors if startDateTime is earlier than minDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.OneTime,
        startDateTime: '2023-01-23T10:20',
        minDateTime: '2023-01-24',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.time]: defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
      });
    });

    it('should not generate errors if props are valid', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.OneTime,
        startDateTime: '2023-01-23T10:20',
      });

      expect(errors).toBeUndefined();
    });
  });

  describe('Hourly recurrence', () => {
    it('should generate errors if startDateTime, endDateTime and intervalHour are missing', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalHour]: defaultSchedulerTexts.invalidIntervalError,
      });
    });

    it('should generate errors if startDateTime, endDateTime and intervalHour are incorrect', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
        startDateTime: 'No idea',
        endDateTime: 'No idea',
        intervalHour: -1,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalHour]: defaultSchedulerTexts.invalidIntervalError,
      });
    });

    it('should not generate errors if startDateTime, endDateTime are missing and showDateTimes is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
        showDateTimes: false,
        intervalHour: 6,
      });

      expect(errors).toBeUndefined();
    });

    it('should generate errors if startDateTime and endDateTime are earlier than minDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalHour: 6,
        minDateTime: '2023-01-25T10:20',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.time]: defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.endDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
      });
    });

    it('should generate errors if startDateTime is after endDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
        startDateTime: '2023-01-25T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalHour: 6,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidStartDateError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidEndDateError,
      });
    });

    it('should not generate errors if props are valid', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Hourly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalHour: 6,
      });

      expect(errors).toBeUndefined();
    });
  });

  describe('Daily recurrence', () => {
    it('should generate errors is  startDateTime, endDateTime and intervalDay are missing', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalDay]: defaultSchedulerTexts.invalidIntervalError,
      });
    });

    it('should generate errors is  startDateTime, endDateTime and intervalDay are incorrect', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
        startDateTime: 'No idea',
        endDateTime: 'No idea',
        intervalDay: -1,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalDay]: defaultSchedulerTexts.invalidIntervalError,
      });
    });

    it('should not generate errors if startDateTime, endDateTime are missing and showDateTimes is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
        showDateTimes: false,
        intervalDay: 6,
      });

      expect(errors).toBeUndefined();
    });

    it('should generate errors if startDateTime and endDateTime are earlier than minDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalDay: 6,
        minDateTime: '2023-01-25T10:20',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.time]: defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.endDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
      });
    });

    it('should generate errors if startDateTime is after endDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-20T10:20',
        intervalDay: 6,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidStartDateError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidEndDateError,
      });
    });

    it('should not generate errors if props are valid', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Daily,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalDay: 6,
      });

      expect(errors).toBeUndefined();
    });
  });

  describe('Weekly recurrence', () => {
    it('should generate errors if startDateTime, endDateTime, weekDays and intervalWeek are missing', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.weekDays]: defaultSchedulerTexts.invalidWeekDaysError,
        [InputName.intervalWeek]:
          defaultSchedulerTexts.invalidWeekIntervalError,
      });
    });

    it('should generate errors if startDateTime, endDateTime, weekDays and intervalWeek are incorrect', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: 'No idea',
        endDateTime: 'No idea',
        // cast type to bypass TS type validation
        weekDays: ['No a week day'] as unknown as WeekDay[],
        intervalWeek: -1,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.weekDays]: defaultSchedulerTexts.invalidWeekDaysError,
        [InputName.intervalWeek]:
          defaultSchedulerTexts.invalidWeekIntervalError,
      });
    });

    it('should not generate errors startDateTime, endDateTime are missing and showDateTimes is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        showDateTimes: false,
        weekDays: ['Monday'],
        intervalWeek: 6,
      });

      expect(errors).toBeUndefined();
    });

    it('should not generate errors if weekDays are missing and showWeekDays is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        showWeekDays: false,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalWeek: 6,
      });

      expect(errors).toBeUndefined();
    });

    it('should generate errors if startDateTime and endDateTime are earlier than minDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalWeek: 1,
        weekDays: ['Monday', 'Friday'],
        minDateTime: '2023-01-25T10:20',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.time]: defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.endDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
      });
    });

    it('should generate errors if startDateTime is after endDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-22T10:20',
        intervalWeek: 1,
        weekDays: ['Monday', 'Friday'],
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidStartDateError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidEndDateError,
      });
    });

    it('should not generate errors if props are valid', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalWeek: 1,
        weekDays: ['Monday', 'Friday'],
      });

      expect(errors).toBeUndefined();
    });
  });

  describe('Monthly recurrence', () => {
    it('should generate errors if startDateTime, endDate, intervalMonth and intervalMonthDay are missing', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalMonth]: defaultSchedulerTexts.invalidIntervalError,
        [InputName.intervalMonthDay]:
          defaultSchedulerTexts.invalidMonthDayError,
      });
    });

    it('should generate errors if startDateTime, endDate, intervalMonth and intervalMonthDay are incorrect', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        startDateTime: 'No idea',
        endDateTime: 'No idea',
        intervalMonth: -1,
        intervalMonthDay: -1,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.time]: defaultSchedulerTexts.invalidTimeError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidDateError,
        [InputName.intervalMonth]: defaultSchedulerTexts.invalidIntervalError,
        [InputName.intervalMonthDay]:
          defaultSchedulerTexts.invalidMonthDayError,
      });
    });

    it('should not generate errors if startDateTime, endDateTime are missing and showDateTimes is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        showDateTimes: false,
        intervalMonth: 1,
        intervalMonthDay: 2,
      });

      expect(errors).toBeUndefined();
    });

    it('should not generate errors if intervalMonthDay is missing and showDayOfMonth is false', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        showDayOfMonth: false,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalMonth: 1,
      });

      expect(errors).toBeUndefined();
    });

    it('should generate errors intervalMonthDay is bigger than 31', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalMonth: 1,
        intervalMonthDay: 41,
      });

      expect(errors).toEqual({
        [InputName.intervalMonthDay]:
          defaultSchedulerTexts.invalidMonthDayError,
      });
    });

    it('should generate errors startDateTime and endDateTime are earlier than minDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalMonth: 1,
        intervalMonthDay: 13,
        minDateTime: '2023-01-25T10:20',
      });

      expect(errors).toEqual({
        [InputName.startDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.time]: defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
        [InputName.endDateTime]:
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime,
      });
    });

    it('should generate errors if startDateTime is after endDateTime', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-20T10:20',
        intervalMonth: 1,
        intervalMonthDay: 13,
      });

      expect(errors).toEqual({
        [InputName.startDateTime]: defaultSchedulerTexts.invalidStartDateError,
        [InputName.endDateTime]: defaultSchedulerTexts.invalidEndDateError,
      });
    });

    it('should not generate errors if props are valid', () => {
      const errors = validateSchedulerProps({
        recurrenceMode: RecurrenceMode.Monthly,
        startDateTime: '2023-01-23T10:20',
        endDateTime: '2023-01-24T10:20',
        intervalMonth: 1,
        intervalMonthDay: 13,
      });

      expect(errors).toBeUndefined();
    });
  });
});
