import {getSummaryText} from '../../src';
import {RecurrenceMode, SchedulerProps} from '../../src/types';
import {defaultSchedulerTexts} from '../../src/constants';

describe('Summary builders', () => {
  const state: SchedulerProps = {
    recurrenceMode: RecurrenceMode.OneTime,
    startDateTime: '2020-08-13T12:30:00.000-07:00',
    endDateTime: '2021-08-13T12:30:00.000-07:00',
    intervalHour: 12,
    intervalDay: 1,
    intervalWeek: 2,
    intervalMonth: 3,
    intervalMonthDay: 15,
    weekDays: ['Monday', 'Wednesday', 'Friday'],
    showDateTimes: true,
    showWeekDays: true,
    showDayOfMonth: true,
  };

  describe('One time summary', () => {
    it('should return one time summary', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.OneTime,
      });
      expect(summary).toBe('Run once at 21:30 on 13 Aug 2020');
    });

    it('should return one time summary without dates', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.OneTime,
        showDateTimes: false,
      });
      expect(summary).toBe('Run once');
    });
  });

  describe('Hourly summary', () => {
    it('Returns correct summary with date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Hourly,
      });

      expect(summary).toBe(
        'Every 12 hour(s) from 21:30 on 13 Aug 2020 until 13 Aug 2021'
      );
    });

    it('Returns correct summary without date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Hourly,
        showDateTimes: false,
      });

      expect(summary).toBe('Every 12 hour(s)');
    });
  });

  describe('Daily summary', () => {
    it('Returns correct summary with date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Daily,
        showDateTimes: true,
      });

      expect(summary).toBe(
        'Every 1 day(s) at 21:30 from 13 Aug 2020 until 13 Aug 2021'
      );
    });

    it('Returns correct summary without date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Daily,
        showDateTimes: false,
      });
      expect(summary).toBe('Every 1 day(s)');
    });
  });

  describe('Weekly summary', () => {
    it('Returns correct summary with date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2020-08-13T12:30:00.000-07:00',
        endDateTime: '2021-08-13T12:30:00.000-07:00',
        intervalWeek: 2,
        weekDays: ['Monday', 'Wednesday', 'Friday'],
        showDateTimes: true,
        showWeekDays: true,
      });

      expect(summary).toBe(
        'Every 2 week(s) on Monday, Wednesday and Friday at 21:30 from 13 Aug 2020 until 13 Aug 2021'
      );
    });

    it('Returns correct summary without date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2020-08-13T12:30:00.000-07:00',
        endDateTime: '2021-08-13T12:30:00.000-07:00',
        intervalWeek: 2,
        weekDays: ['Monday', 'Wednesday', 'Friday'],
        showDateTimes: false,
        showWeekDays: true,
      });

      expect(summary).toBe('Every 2 week(s) on Monday, Wednesday and Friday');
    });

    it('Returns correct summary with translations', () => {
      const translatedTexts = {
        mondayLong: 'Mandag',
        tuesdayLong: 'Tirsdag',
        wednesdayLong: 'Onsdag',
        thursdayLong: 'Torsdag',
        fridayLong: 'Fredag',
        saturdayLong: 'Lørdag',
        sundayLong: 'Søndag',
        summaryPrefix: 'Hver',
        weeks: 'uke',
        on: 'på',
        and: 'og',
      };
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2020-08-13T12:30:00.000-07:00',
        endDateTime: '2021-08-13T12:30:00.000-07:00',
        intervalWeek: 2,
        weekDays: ['Monday', 'Wednesday', 'Friday'],
        showDateTimes: false,
        showWeekDays: true,
        texts: {...defaultSchedulerTexts, ...translatedTexts},
      });

      expect(summary).toBe('Hver 2 uke på Mandag, Onsdag og Fredag');
    });

    it('Returns correct summary without week days', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Weekly,
        startDateTime: '2020-08-13T12:30:00.000-07:00',
        endDateTime: '2021-08-13T12:30:00.000-07:00',
        intervalWeek: 2,
        weekDays: ['Monday', 'Wednesday', 'Friday'],
        showDateTimes: false,
        showWeekDays: false,
      });

      expect(summary).toBe('Every 2 week(s)');
    });
  });

  describe('Monthly summary', () => {
    it('Returns correct summary with date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Monthly,
        showDateTimes: true,
        showDayOfMonth: true,
      });

      expect(summary).toBe(
        'Every 3 month(s) on day 15 at 21:30 from 13 Aug 2020 until 13 Aug 2021'
      );
    });

    it('Returns correct summary without date times', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Monthly,
        showDateTimes: false,
        showDayOfMonth: true,
      });

      expect(summary).toBe('Every 3 month(s) on day 15');
    });

    it('Returns correct summary without day of month', () => {
      const summary = getSummaryText({
        ...state,
        recurrenceMode: RecurrenceMode.Monthly,
        showDateTimes: false,
        showDayOfMonth: false,
      });

      expect(summary).toBe('Every 3 month(s)');
    });
  });
});
