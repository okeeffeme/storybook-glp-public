import moment from 'moment';
import {
  getTime,
  getDate,
  getSummaryDate,
  appendTimeToDate,
  autoFormatTime,
  isDateABeforeDateB,
  isDateASameOrAfterDateB,
  isDateAndTimeASameOrAfterDateAndTimeB,
} from '../../src/utils/date-helpers';

describe('Date helpers :: ', () => {
  it('getTime returns time in the correct format', () => {
    const date = moment().hours(12).minutes(30).toISOString(true);
    const formattedTime = getTime(date);

    expect(formattedTime).toBe('12:30');
  });

  it('getDate returns date in the correct format', () => {
    const date = moment()
      .year(2020)
      .month(0)
      .date(15)
      .hours(12)
      .minutes(30)
      .toISOString(true);
    const formattedDate = getDate(date);

    expect(formattedDate).toBe('15 Jan 2020');
  });

  it('getSummaryDate returns date in correct format', () => {
    const date = moment()
      .year(2020)
      .month(0)
      .date(15)
      .hours(12)
      .minutes(30)
      .toISOString(true);
    const formattedDate = getSummaryDate(date);

    expect(formattedDate).toBe('15 Jan 2020');
  });

  describe('appendTimeToDate :: ', () => {
    it('should append time to date when using : as separator', () => {
      const date = moment()
        .year(2020)
        .month(0)
        .date(15)
        .hours(0)
        .minutes(0)
        .toISOString(true);
      const timeString = '12:00';
      const appendedDateTime = appendTimeToDate(date, timeString);

      const expectedDateTime = moment(date)
        .hours(12)
        .minutes(0)
        .toISOString(true);

      expect(appendedDateTime).toBe(expectedDateTime);
    });

    it('should append time to date when using . as a separator', () => {
      const date = moment()
        .year(2020)
        .month(0)
        .date(15)
        .hours(0)
        .minutes(0)
        .toISOString(true);
      const timeString = '12.00';
      const appendedDateTime = appendTimeToDate(date, timeString);

      const expectedDateTime = moment(date)
        .hours(12)
        .minutes(0)
        .toISOString(true);

      expect(appendedDateTime).toBe(expectedDateTime);
    });

    it('should return original time when input does not contain a : or . separators', () => {
      const date = moment()
        .year(2020)
        .month(0)
        .date(15)
        .hours(5)
        .minutes(31)
        .toISOString(true);
      const timeString = '0123';
      const appendedDateTime = appendTimeToDate(date, timeString);

      const expectedDateTime = moment(date)
        .hours(5)
        .minutes(31)
        .toISOString(true);

      expect(appendedDateTime).toBe(expectedDateTime);
    });
  });

  describe('autoFormatTime :: ', () => {
    it('should add : as a separator when 4 digits are added', () => {
      const input = '1234';
      const expectedOutput = '12:34';

      expect(autoFormatTime(input)).toBe(expectedOutput);
    });

    it('should not modify input when . is used as separator', () => {
      const input = '12.34';
      const expectedOutput = '12.34';

      expect(autoFormatTime(input)).toBe(expectedOutput);
    });

    it('should not modify input when : is used as separator', () => {
      const input = '12:34';

      expect(autoFormatTime(input)).toBe(input);
    });

    it('should not modify input when : is used as separator even if text is added', () => {
      const input = '12:aa';

      expect(autoFormatTime(input)).toBe(input);
    });

    it('should not modify input when . is used as separator even if text is added', () => {
      const input = 'A%.00';

      expect(autoFormatTime(input)).toBe(input);
    });

    it('should not modify input when only 2 characters are added', () => {
      const input = '10';
      const expectedOutput = '10';

      expect(autoFormatTime(input)).toBe(expectedOutput);
    });

    it('should not modify input when only 3 characters are added', () => {
      const input = '123';
      const expectedOutput = '123';

      expect(autoFormatTime(input)).toBe(expectedOutput);
    });
  });

  describe('isDateABeforeDateB :: ', () => {
    it('should return false when dateA is after dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(31)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateABeforeDateB({dateA, dateB});
      expect(result).toBe(false);
    });

    it('should return true when dateA is before dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(31)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateABeforeDateB({dateA, dateB});
      expect(result).toBe(true);
    });
  });

  describe('isDateASameOrAfterDateB :: ', () => {
    it('should return true when dateA is after dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(2)
        .hours(1)
        .minutes(29)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateASameOrAfterDateB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return true when dateA is same as dateB, even when time on A is before B', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(29)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateASameOrAfterDateB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return true when dateA is same as dateB, even when time on A is after B', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(31)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateASameOrAfterDateB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return false when dateA is before dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(0)
        .hours(1)
        .minutes(29)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateASameOrAfterDateB({dateA, dateB});
      expect(result).toBe(false);
    });
  });

  describe('isDateAndTimeASameOrAfterDateAndTimeB :: ', () => {
    it('should return true when dateA is after dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(2)
        .hours(1)
        .minutes(29)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateAndTimeASameOrAfterDateAndTimeB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return true when dateA is after dateB, just by a minute', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(31)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateAndTimeASameOrAfterDateAndTimeB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return true when dateA is the same as dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateAndTimeASameOrAfterDateAndTimeB({dateA, dateB});
      expect(result).toBe(true);
    });

    it('should return false when dateA is before dateB', () => {
      const dateA = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(29)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const dateB = moment()
        .year(2020)
        .month(1)
        .date(1)
        .hours(1)
        .minutes(30)
        .seconds(0)
        .milliseconds(0)
        .toISOString(true);

      const result = isDateAndTimeASameOrAfterDateAndTimeB({dateA, dateB});
      expect(result).toBe(false);
    });
  });
});
