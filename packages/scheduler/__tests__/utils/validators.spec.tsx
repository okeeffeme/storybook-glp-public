import {
  validateDate,
  validateTime,
  validateInterval,
  validateWeekInterval,
  validateMonthDay,
  validateWeekDays,
  validateStartDate,
  validateEndDate,
} from '../../src/utils/validators';
import {defaultSchedulerTexts as texts} from '../../src/constants';

describe('Validators :: ', () => {
  describe('validateDate :: ', () => {
    it('validateDate does not return error when date is correctly defined', () => {
      const date = '2020-08-13T10:19:54.249-07:00';
      const error = validateDate(date, texts);

      expect(error).toBe(undefined);
    });

    it('validateDate does not return error when date is after minDateTime', () => {
      const minDateTime = '2020-08-10T10:19:54.249-07:00';
      const date = '2020-08-13T10:19:54.249-07:00';
      const error = validateDate(date, texts, minDateTime);

      expect(error).toBe(undefined);
    });

    it('validateDate returns correct error message when date is incorrectly defined', () => {
      const date = 'not-a-date';
      const error = validateDate(date, texts);

      expect(error).toBe(texts.invalidDateError);
    });

    it('validateDate returns correct error message when date is before minDateTime', () => {
      const minDateTime = '2020-08-20T10:19:54.249-07:00';
      const date = '2020-08-13T10:19:54.249-07:00';
      const error = validateDate(date, texts, minDateTime);

      expect(error).toBe(texts.invalidDateTimeErrorMinDateTime);
    });
  });

  describe('validateTime :: ', () => {
    it('validateTime returns correct error message on incorrect time format', () => {
      const time = '25,00';
      const error = validateTime(time, texts);

      expect(error).toBe(texts.invalidTimeError);
    });

    it('validateTime returns correct error message on incorrect minute format', () => {
      const time = '12:0';
      const error = validateTime(time, texts);

      expect(error).toBe(texts.invalidTimeError);
    });

    it('validateTime returns correct error message on hours greater than 24', () => {
      const time = '25:00';
      const error = validateTime(time, texts);

      expect(error).toBe(texts.invalidTimeError);
    });

    it('validateTime returns correct error message on minutes greater than 59', () => {
      const time = '00:70';
      const error = validateTime(time, texts);

      expect(error).toBe(texts.invalidTimeError);
    });

    it('validateTime returns no error message when time is correctly defined with : as separator', () => {
      const time = '12:00';
      const error = validateTime(time, texts);

      expect(error).toBe(undefined);
    });

    it('validateTime returns no error message when time is correctly defined with . as separator', () => {
      const time = '12.00';
      const error = validateTime(time, texts);

      expect(error).toBe(undefined);
    });
  });

  it('validateInterval returns correct error message on invalid interval', () => {
    const interval = 0;
    const error = validateInterval(interval, texts);

    expect(error).toBe(texts.invalidIntervalError);
  });

  it('validateInterval returns no error message on valid interval', () => {
    const interval = 1;
    const error = validateInterval(interval, texts);

    expect(error).toBe(undefined);
  });

  it('validateWeekInterval returns correct error message on invalid week interval', () => {
    const interval = 53;
    const error = validateWeekInterval(interval, texts);

    expect(error).toBe(texts.invalidWeekIntervalError);
  });

  it('validateWeekInterval returns no error message on valid week interval', () => {
    const interval = 52;
    const error = validateWeekInterval(interval, texts);

    expect(error).toBe(undefined);
  });

  it('validateMonthDay returns correct error message on invalid month day interval', () => {
    const interval = 32;
    const error = validateMonthDay(interval, texts);

    expect(error).toBe(texts.invalidMonthDayError);
  });

  it('validateMonthDay returns no error message on valid month day interval', () => {
    const interval = 31;
    const error = validateMonthDay(interval, texts);

    expect(error).toBe(undefined);
  });

  describe('validateWeekDays :: ', () => {
    it('validateWeekDays returns correct error message on invalid week days', () => {
      const days = ['Invalid'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(texts.invalidWeekDaysError);
    });

    it('validateWeekDays returns error message on valid week days with all lowercase', () => {
      const days = ['monday'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(texts.invalidWeekDaysError);
    });

    it('validateWeekDays returns error message on valid week days with all uppercase', () => {
      const days = ['MONDAY'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(texts.invalidWeekDaysError);
    });

    it('validateWeekDays returns error message when more than one day defined, and incorrect casing', () => {
      const days = ['MONDAY', 'tuesday'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(texts.invalidWeekDaysError);
    });

    it('validateWeekDays returns no error message on valid week days with capital case', () => {
      const days = ['Monday'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(undefined);
    });

    it('validateWeekDays returns no error message on multiple week days with capital case', () => {
      const days = ['Monday', 'Tuesday'];
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const error = validateWeekDays(days, texts);

      expect(error).toBe(undefined);
    });
  });

  it('validateStartDate returns correct error message when start date is after end date', () => {
    const startDate = '2020-08-13T12:30:00.000-07:00';
    const endDate = '2019-08-13T12:30:00.000-07:00';
    const error = validateStartDate(startDate, texts, endDate);

    expect(error).toBe(texts.invalidStartDateError);
  });

  it('validateStartDate returns no error message when start date is before end date', () => {
    const startDate = '2019-08-13T12:30:00.000-07:00';
    const endDate = '2020-08-13T12:30:00.000-07:00';
    const error = validateStartDate(startDate, texts, endDate);

    expect(error).toBe(undefined);
  });

  it('validateEndDate returns correct error message when end date is before start date', () => {
    const endDate = '2019-08-13T12:30:00.000-07:00';
    const startDate = '2020-08-13T12:30:00.000-07:00';
    const error = validateEndDate(endDate, texts, startDate);

    expect(error).toBe(texts.invalidEndDateError);
  });

  it('validateEndDate returns no error message when end date is after start date', () => {
    const endDate = '2020-08-12T12:30:00.000-07:00';
    const startDate = '2019-08-13T12:30:00.000-07:00';
    const error = validateEndDate(endDate, texts, startDate);

    expect(error).toBe(undefined);
  });
});
