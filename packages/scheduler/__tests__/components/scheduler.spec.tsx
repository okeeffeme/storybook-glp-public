import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';

import Scheduler from '../../src/components/scheduler';
import moment from 'moment';
import {RecurrenceMode} from '../../src/types';
import {defaultSchedulerTexts} from '../../src/constants';

const invalidDate = '2020-08-13T35:19:54.249-07:00';
const startDateTime = '2020-08-13T12:30:00.000-07:00';
const endDateTime = '2021-08-13T12:30:00.000-07:00';

describe('Scheduler', () => {
  it('Sets startDateTime and recurrenceMode if they are not defined', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler
        recurrenceMode={RecurrenceMode.OneTime}
        startDateTime={startDateTime}
        onScheduleChange={onScheduleChangeHandler}
      />
    );

    expect(screen.getByText(/never/i)).toBeInTheDocument();
    expect(screen.getByTestId('date-time-picker')).toBeInTheDocument();
  });

  it('Does not render date time inputs if showDateTimes is false', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler
        showDateTimes={false}
        onScheduleChange={onScheduleChangeHandler}
      />
    );

    expect(screen.queryByTestId('date-time-picker')).not.toBeInTheDocument();
  });

  it('Should not render date time inputs if readOnly is true', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler readOnly={true} onScheduleChange={onScheduleChangeHandler} />
    );

    expect(screen.queryByTestId('date-time-picker')).not.toBeInTheDocument();
  });

  it('Should render start date time input if readOnly and showDateTimes are not set up', () => {
    const onScheduleChangeHandler = jest.fn();

    render(<Scheduler onScheduleChange={onScheduleChangeHandler} />);

    expect(screen.queryByTestId('date-time-picker')).toBeInTheDocument();
  });

  it('Should render end date time input if readOnly and showDateTimes are not set up and recurrenceMode exists but not equal OneTime', () => {
    const onScheduleChangeHandler = jest.fn();

    render(<Scheduler onScheduleChange={onScheduleChangeHandler} />);

    expect(screen.queryByTestId('date-time-picker')).toBeInTheDocument();
  });

  it('Should not render recurrence picker if readOnly is true', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler
        readOnly={true}
        intervalWeek={3}
        recurrenceMode={RecurrenceMode.Weekly}
        onScheduleChange={onScheduleChangeHandler}
      />
    );

    expect(screen.queryByTestId('date-time-picker')).not.toBeInTheDocument();
  });

  it('Should render recurrence picker if readOnly is not set up', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler
        recurrenceMode={RecurrenceMode.Weekly}
        onScheduleChange={onScheduleChangeHandler}
      />
    );

    expect(screen.getByTestId('recurrence-input')).toBeInTheDocument();
  });

  it('Does not render weekDays input if showWeekDays is false', () => {
    const onScheduleChangeHandler = jest.fn();

    render(
      <Scheduler
        showWeekDays={false}
        recurrenceMode={RecurrenceMode.Weekly}
        onScheduleChange={onScheduleChangeHandler}
      />
    );

    fireEvent.click(screen.getByTestId('recurrence-input'));
    fireEvent.click(screen.getByText(/weekly/i));

    expect(screen.queryByTestId('week-days-input')).not.toBeInTheDocument();
  });

  describe('One Time recurrence', () => {
    it('Renders all valid values', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.OneTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={startDateTime}
          endDateTime={undefined}
        />
      );

      expect(screen.getByText(/never/i)).toBeInTheDocument();
      expect(screen.getByTestId('date-time-picker')).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('should show validation error when date is not defined correctly', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.OneTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={invalidDate}
        />
      );

      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
      expect(
        screen.getByText(defaultSchedulerTexts.invalidDateError)
      ).toBeInTheDocument();
      expect(
        screen.getByText(defaultSchedulerTexts.invalidTimeError)
      ).toBeInTheDocument();
    });

    it('should show validation errors when startDateTime is before minDateTime', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.OneTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={startDateTime}
          minDateTime="2020-08-20T12:30:00.000-07:00"
        />
      );

      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
      expect(
        screen.getAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(2);
      expect(
        screen.getAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(2);
    });

    it('should not show validation errors when startDateTime is equal to minDateTime', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.OneTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={startDateTime}
          minDateTime="2020-08-10T12:30:00.000-07:00"
        />
      );

      expect(
        screen.queryAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(0);
      expect(
        screen.queryAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(0);
      expect(
        screen.queryByText(defaultSchedulerTexts.invalidDateError)
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText(defaultSchedulerTexts.invalidTimeError)
      ).not.toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('should not show validation errors when startDateTime is after minDateTime', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.OneTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={startDateTime}
          minDateTime="2020-08-10T12:30:00.000-07:00"
        />
      );

      expect(
        screen.queryAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(0);
      expect(
        screen.queryAllByText(
          defaultSchedulerTexts.invalidDateTimeErrorMinDateTime
        ).length
      ).toBe(0);
      expect(
        screen.queryByText(defaultSchedulerTexts.invalidDateError)
      ).not.toBeInTheDocument();
      expect(
        screen.queryByText(defaultSchedulerTexts.invalidTimeError)
      ).not.toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('should set minDateTime on start and end DatePicker when minDateTime is set', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Daily}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
          startDateTime={startDateTime}
          minDateTime="2020-08-20T00:00:00.000+00:00"
        />
      );

      expect(screen.getAllByTestId('simple-text-field').length).toBe(4);
    });

    it('should not set minDateTime on start and end DatePicker when minDateTime is not set', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Daily}
          startDateTime={startDateTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.invalidStartDateError)
      ).toBeInTheDocument();
    });

    it('should not render recurrence section when enabledRecurrence is false', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          enableRecurrence={false}
          startDateTime={startDateTime}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.queryByText(defaultSchedulerTexts.repeats)
      ).not.toBeInTheDocument();
    });
  });
  describe('Hourly recurrence', () => {
    it('Renders all valid values', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Hourly}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          intervalHour={5}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.Hourly)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('Renders validation error', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Hourly}
          startDateTime={startDateTime}
          intervalHour={0}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.invalidIntervalError)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({
        isValid: false,
      });
    });
  });

  describe('Daily recurrence', () => {
    it('Renders all valid values', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Daily}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          intervalDay={2}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(screen.getByText(defaultSchedulerTexts.Daily)).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('Renders validation error', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Daily}
          startDateTime={startDateTime}
          intervalDay={0}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.invalidIntervalError)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
    });
  });

  describe('Weekly recurrence', () => {
    it('Correctly sets weekDays to current day in expected format when moment locale is initialized with norwegian locale', () => {
      const currentLocale = moment.locale();
      moment.locale('nb');

      const onValidationChangeHandler = jest.fn();
      const onScheduleChangeHandler = jest.fn(({weekDays}) => {
        expect(weekDays.length).toBe(1);
        expect(weekDays[0]).toBe('Thursday');
        moment.locale(currentLocale);
      });

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Weekly}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          intervalWeek={1}
          weekDays={['Monday']}
          texts={{
            Weekly: 'Ukentlig',
          }}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(screen.getByText(/ukentlig/i)).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('Renders all valid values', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Weekly}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          intervalWeek={1}
          weekDays={['Monday', 'Wednesday']}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(screen.getByText(/weekly/i)).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('Renders validation error', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Weekly}
          startDateTime={startDateTime}
          intervalWeek={53}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.invalidWeekDaysError)
      ).toBeInTheDocument();
      expect(
        screen.getByText(defaultSchedulerTexts.invalidWeekIntervalError)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
    });
  });

  describe('Monthly recurrence', () => {
    it('Renders all valid values', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Monthly}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          intervalMonth={3}
          intervalMonthDay={1}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.Monthly)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: true});
    });

    it('Renders validation error', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Monthly}
          startDateTime={startDateTime}
          intervalMonthDay={50}
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
    });

    it('should show validation error when startDateTime is after endDateTime', () => {
      const onScheduleChangeHandler = jest.fn();
      const onValidationChangeHandler = jest.fn();

      render(
        <Scheduler
          recurrenceMode={RecurrenceMode.Monthly}
          startDateTime="2020-08-20T12:30:00.000-07:00"
          endDateTime="2020-08-10T12:30:00.000-07:00"
          onScheduleChange={onScheduleChangeHandler}
          onValidationChange={onValidationChangeHandler}
        />
      );

      expect(
        screen.getByText(defaultSchedulerTexts.invalidStartDateError)
      ).toBeInTheDocument();
      expect(
        screen.getByText(defaultSchedulerTexts.invalidEndDateError)
      ).toBeInTheDocument();
      expect(onValidationChangeHandler).toHaveBeenCalledWith({isValid: false});
    });
  });
});
