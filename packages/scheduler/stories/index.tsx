import React, {useCallback, useReducer} from 'react';
import {storiesOf} from '@storybook/react';
import {boolean} from '@storybook/addon-knobs';
import {produce} from 'immer';
import moment from 'moment';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Scheduler from '../src/components/scheduler';

const TODAY = Date.now();
const MIN_DATE = moment(TODAY)
  .subtract(3, 'days')
  .seconds(0)
  .milliseconds(0)
  .toISOString(true);

const initialState = {
  startDateTime: moment(TODAY).format(),
};

const reducer = produce((draft, {payload}) => {
  Object.keys(payload).forEach((key) => {
    draft[key] = payload[key];
  });
});

storiesOf('Components/scheduler', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const handleScheduleChange = useCallback((field) => {
      dispatch({
        payload: field,
      });
    }, []);

    return (
      <div style={{padding: '16px'}}>
        <Scheduler
          recurrenceMode={state.recurrenceMode}
          intervalHour={state.intervalHour}
          intervalDay={state.intervalDay}
          intervalMonth={state.intervalMonth}
          intervalMonthDay={state.intervalMonthDay}
          intervalWeek={state.intervalWeek}
          weekDays={state.weekDays}
          startDateTime={state.startDateTime}
          endDateTime={state.endDateTime}
          onScheduleChange={handleScheduleChange}
          showDateTimes={boolean('showDateTimes', true)}
          showWeekDays={boolean('showWeekDays', true)}
          showDayOfMonth={boolean('showDayOfMonth', true)}
          showValidationErrors={boolean('showValidationErrors', true)}
          minDateTime={MIN_DATE}
          onValidationChange={({isValid}) =>
            console.log('Scheduler state: ', {isValid})
          }
        />
      </div>
    );
  })
  .add('Recurrence Hidden', () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const handleScheduleChange = useCallback((field) => {
      dispatch({
        payload: field,
      });
    }, []);

    return (
      <div style={{padding: '16px'}}>
        <Scheduler
          enableRecurrence={false}
          startDateTime={state.startDateTime}
          onScheduleChange={handleScheduleChange}
          showValidationErrors={boolean('showValidationErrors', true)}
          minDateTime={MIN_DATE}
          onValidationChange={({isValid}) =>
            console.log('Scheduler state: ', {isValid})
          }
        />
      </div>
    );
  });
