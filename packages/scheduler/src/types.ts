export interface SchedulerTexts {
  OneTime?: string;
  Hourly?: string;
  Daily?: string;
  Weekly?: string;
  Monthly?: string;
  at?: string;
  from?: string;
  repeats?: string;
  until?: string;
  every?: string;
  starts?: string;
  ends?: string;
  ofEvery?: string;
  summaryOncePrefix?: string;
  on?: string;
  summaryPrefix?: string;
  hours?: string;
  months?: string;
  repeatsOn?: string;
  day?: string;
  days?: string;
  and?: string;
  summary?: string;
  weeks?: string;
  invalidTimeError?: string;
  invalidDateError?: string;
  invalidEndDateError?: string;
  invalidIntervalError?: string;
  invalidStartDateError?: string;
  invalidWeekIntervalError?: string;
  invalidMonthDayError?: string;
  invalidWeekDaysError?: string;
  invalidDateTimeErrorMinDateTime?: string;
  monday?: string;
  tuesday?: string;
  wednesday?: string;
  thursday?: string;
  friday?: string;
  saturday?: string;
  sunday?: string;
  mondayLong?: string;
  tuesdayLong?: string;
  wednesdayLong?: string;
  thursdayLong?: string;
  fridayLong?: string;
  saturdayLong?: string;
  sundayLong?: string;
}

export type WeekDay =
  | 'Monday'
  | 'Tuesday'
  | 'Wednesday'
  | 'Thursday'
  | 'Friday'
  | 'Saturday'
  | 'Sunday';

export enum RecurrenceMode {
  OneTime = 'OneTime',
  Hourly = 'Hourly',
  Daily = 'Daily',
  Weekly = 'Weekly',
  Monthly = 'Monthly',
}

export enum InputName {
  startDateTime = 'startDateTime',
  endDateTime = 'endDateTime',
  intervalHour = 'intervalHour',
  intervalDay = 'intervalDay',
  intervalWeek = 'intervalWeek',
  intervalMonth = 'intervalMonth',
  intervalMonthDay = 'intervalMonthDay',
  weekDays = 'weekDays',
  time = 'time',
  recurrenceMode = 'recurrenceMode',
}

type SchedulerConditionalProps =
  | {
      enableRecurrence?: false;
      recurrenceMode?: never;
    }
  | {
      enableRecurrence?: true;
      recurrenceMode?: RecurrenceMode;
    };

type SchedulerBaseProps = {
  startDateTime?: string;
  endDateTime?: string;
  intervalHour?: number;
  intervalDay?: number;
  intervalWeek?: number;
  weekDays?: WeekDay[];
  intervalMonthDay?: number;
  intervalMonth?: number;
  minDateTime?: string;
  showDateTimes?: boolean;
  showWeekDays?: boolean;
  showDayOfMonth?: boolean;
  texts?: SchedulerTexts;
};
export type SchedulerProps = SchedulerConditionalProps & SchedulerBaseProps;
