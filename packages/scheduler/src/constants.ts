import {WeekDay, SchedulerTexts} from './types';

export const baseClassName = 'comd-scheduler';
export const defaultTimeFormat = 'HH:mm';

export const defaultSchedulerTexts: Required<SchedulerTexts> = {
  // recurrence options
  OneTime: 'Never',
  Hourly: 'Hourly',
  Daily: 'Daily',
  Weekly: 'Weekly',
  Monthly: 'Monthly',
  // interval postfix
  hours: 'hour(s)',
  days: 'day(s)',
  weeks: 'week(s)',
  months: 'month(s)',
  day: 'day',
  every: 'every',
  ofEvery: 'of every',
  // interval label
  repeats: 'Repeats',
  repeatsOn: 'Repeats on',
  // date labels
  starts: 'Starts',
  ends: 'Ends',
  // week days for toggle buttons
  monday: 'Mon',
  tuesday: 'Tue',
  wednesday: 'Wed',
  thursday: 'Thu',
  friday: 'Fri',
  saturday: 'Sat',
  sunday: 'Sun',
  // long week day names for summary
  mondayLong: 'Monday',
  tuesdayLong: 'Tuesday',
  wednesdayLong: 'Wednesday',
  thursdayLong: 'Thursday',
  fridayLong: 'Friday',
  saturdayLong: 'Saturday',
  sundayLong: 'Sunday',
  // error messages
  invalidDateError: 'Please enter a valid date',
  invalidStartDateError:
    'Please enter a start date that is before the end date',
  invalidDateTimeErrorMinDateTime:
    'Please enter a date and time combination that is not in the past',
  invalidEndDateError: 'Please enter an end date that is after the start date',
  invalidTimeError:
    'Please enter time using one of the following formats: 12:00 or 12.00',
  invalidIntervalError: 'Please enter a number greater than 1',
  invalidWeekIntervalError: 'Weeks can only contain numbers from 1 to 52',
  invalidMonthDayError: 'Day of month can only contain numbers from 1 to 31',
  invalidWeekDaysError: 'Please select day(s) of the week',
  // summary
  summary: 'Summary',
  summaryPrefix: 'Every',
  summaryOncePrefix: 'Run once',
  from: 'from',
  at: 'at',
  on: 'on',
  until: 'until',
  and: 'and',
};

export const validWeekDays: WeekDay[] = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];
