import {defaultSchedulerTexts} from '../constants';
import {
  validateDate,
  validateEndDate,
  validateInterval,
  validateMonthDay,
  validateStartDate,
  validateTime,
  validateWeekDays,
  validateWeekInterval,
} from './validators';
import {RecurrenceMode, SchedulerTexts, WeekDay, InputName} from '../types';

export const getValidator =
  ({
    recurrenceMode,
    startDateTime,
    endDateTime,
    minDateTime = '',
    texts = defaultSchedulerTexts,
  }: {
    recurrenceMode: RecurrenceMode;
    startDateTime: string;
    endDateTime: string;
    minDateTime?: string;
    texts?: SchedulerTexts;
  }) =>
  (
    name: string,
    value: string | number | WeekDay[] | undefined
  ): string | undefined => {
    let error: string | undefined;

    switch (name) {
      case InputName.startDateTime: {
        error = validateDate(value as string, texts, minDateTime);
        if (!error && recurrenceMode !== RecurrenceMode.OneTime) {
          error = validateStartDate(value as string, texts, endDateTime);
        }
        break;
      }
      case InputName.endDateTime: {
        error = validateDate(value as string, texts, minDateTime);
        if (!error && recurrenceMode !== RecurrenceMode.OneTime) {
          error = validateEndDate(value as string, texts, startDateTime);
        }
        break;
      }
      case InputName.time:
        error = validateTime(
          value as string,
          texts,
          minDateTime,
          startDateTime
        );
        break;
      case InputName.intervalHour:
      case InputName.intervalDay:
      case InputName.intervalMonth:
        error = validateInterval(value as string | number, texts);
        break;
      case InputName.intervalWeek:
        error = validateWeekInterval(value as string | number, texts);
        break;
      case InputName.intervalMonthDay:
        error = validateMonthDay(value as string | number, texts);
        break;
      case InputName.weekDays:
        error = validateWeekDays(value as WeekDay[], texts);
        break;
    }
    return error;
  };
