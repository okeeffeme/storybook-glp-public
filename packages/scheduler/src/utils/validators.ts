import {validWeekDays} from '../constants';
import {
  isValidDate,
  isDateABeforeDateB,
  isDateASameOrAfterDateB,
  isDateAndTimeASameOrAfterDateAndTimeB,
} from './date-helpers';

import type {SchedulerTexts, WeekDay} from '../types';

const MIN_INTERVAL = 1;
const MAX_WEEK_INTERVAL = 52;
const MAX_MONTH_DAY_INTERVAL = 31;

export const validateDate = (
  value: string,
  texts: SchedulerTexts,
  minDateTime?: string
) => {
  if (
    minDateTime &&
    !isDateASameOrAfterDateB({
      dateA: value,
      dateB: minDateTime,
    })
  ) {
    return texts.invalidDateTimeErrorMinDateTime;
  }

  if (!isValidDate(value)) {
    return texts.invalidDateError;
  }
};

export const validateStartDate = (
  value: string,
  texts: SchedulerTexts,
  endDateTime: string
) => {
  if (!isDateABeforeDateB({dateA: value, dateB: endDateTime})) {
    return texts.invalidStartDateError;
  }
};

export const validateEndDate = (
  value: string,
  texts: SchedulerTexts,
  startDateTime: string
) => {
  if (isDateABeforeDateB({dateA: value, dateB: startDateTime})) {
    return texts.invalidEndDateError;
  }
};

export const validateTime = (
  value: string,
  texts: SchedulerTexts,
  minDateTime?: string,
  startDateTime?: string
) => {
  if (
    minDateTime &&
    !isDateAndTimeASameOrAfterDateAndTimeB({
      dateA: startDateTime,
      dateB: minDateTime,
    })
  ) {
    return texts.invalidDateTimeErrorMinDateTime;
  }

  const [hours, minutes] =
    value.indexOf(':') >= 0 ? value.split(':') : value.split('.');

  if (!hours || !minutes || minutes.length < 2) {
    return texts.invalidTimeError;
  }

  if (
    !Number.isInteger(Number(hours)) ||
    !Number.isInteger(Number(minutes)) ||
    Number(hours) > 23 ||
    Number(minutes) > 59
  ) {
    return texts.invalidTimeError;
  }
};

export const validateInterval = (
  value: string | number,
  texts: SchedulerTexts
) => {
  if (
    (typeof value === 'string' && !value.length) ||
    !Number.isInteger(Number(value)) ||
    value < MIN_INTERVAL
  ) {
    return texts.invalidIntervalError;
  }
};

export const validateWeekInterval = (
  value: string | number,
  texts: SchedulerTexts
) => {
  if (
    (typeof value === 'string' && !value.length) ||
    !Number.isInteger(Number(value)) ||
    value < MIN_INTERVAL ||
    value > MAX_WEEK_INTERVAL
  ) {
    return texts.invalidWeekIntervalError;
  }
};

export const validateMonthDay = (
  value: string | number,
  texts: SchedulerTexts
) => {
  if (
    (typeof value === 'string' && !value.length) ||
    !Number.isInteger(Number(value)) ||
    value < MIN_INTERVAL ||
    value > MAX_MONTH_DAY_INTERVAL
  ) {
    return texts.invalidMonthDayError;
  }
};

export const validateWeekDays = (value: WeekDay[], texts: SchedulerTexts) => {
  const invalidWeekDays = value.filter((day) => validWeekDays.includes(day));

  if (!invalidWeekDays.length) {
    return texts.invalidWeekDaysError;
  }
};
