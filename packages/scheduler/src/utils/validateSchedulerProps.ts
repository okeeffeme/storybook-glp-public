import {RecurrenceMode, InputName, SchedulerProps} from '../types';
import {getValidator} from './getValidator';
import moment from 'moment';

const filterInputNames = ({
  inputNames,
  showDateTimes,
  showWeekDays,
  showDayOfMonth,
}: {
  inputNames: InputName[];
  showDateTimes?: boolean;
  showWeekDays?: boolean;
  showDayOfMonth?: boolean;
}) => {
  let filteredInputNames = inputNames;

  if (!showDateTimes) {
    filteredInputNames = filteredInputNames.filter(
      (inputName) =>
        ![
          InputName.startDateTime,
          InputName.time,
          InputName.endDateTime,
        ].includes(inputName)
    );
  }

  if (!showWeekDays) {
    filteredInputNames = filteredInputNames.filter(
      (inputName) => ![InputName.weekDays].includes(inputName)
    );
  }

  if (!showDayOfMonth) {
    filteredInputNames = filteredInputNames.filter(
      (inputName) => ![InputName.intervalMonthDay].includes(inputName)
    );
  }

  return filteredInputNames;
};

const inputNamesByRecurrenceMode = {
  [RecurrenceMode.OneTime]: [InputName.startDateTime, InputName.time],
  [RecurrenceMode.Hourly]: [
    InputName.startDateTime,
    InputName.time,
    InputName.intervalHour,
    InputName.endDateTime,
  ],
  [RecurrenceMode.Daily]: [
    InputName.startDateTime,
    InputName.time,
    InputName.intervalDay,
    InputName.endDateTime,
  ],
  [RecurrenceMode.Weekly]: [
    InputName.startDateTime,
    InputName.time,
    InputName.intervalWeek,
    InputName.weekDays,
    InputName.endDateTime,
  ],
  [RecurrenceMode.Monthly]: [
    InputName.startDateTime,
    InputName.time,
    InputName.intervalMonth,
    InputName.intervalMonthDay,
    InputName.endDateTime,
  ],
};

export const validateSchedulerProps = ({
  recurrenceMode = RecurrenceMode.OneTime,
  minDateTime,
  startDateTime = '',
  endDateTime = '',
  weekDays = [],
  intervalHour,
  intervalMonth,
  intervalMonthDay,
  intervalWeek,
  intervalDay,
  texts,
  showDateTimes = true,
  showDayOfMonth = true,
  showWeekDays = true,
}: SchedulerProps): Record<string, string> | undefined => {
  const validator = getValidator({
    recurrenceMode,
    minDateTime,
    startDateTime,
    endDateTime,
    texts,
  });
  const errors: Record<string, string> = {};
  const inputNames = filterInputNames({
    inputNames: inputNamesByRecurrenceMode[recurrenceMode],
    showWeekDays,
    showDayOfMonth,
    showDateTimes,
  });

  const mStartDateTime = moment(startDateTime);
  const [startDate, startTime] = mStartDateTime.isValid()
    ? [mStartDateTime.format('YYYY-MM-DD'), mStartDateTime.format('HH:mm')]
    : ['', ''];

  const valueByInputName = {
    [InputName.startDateTime]: startDate,
    [InputName.time]: startTime,
    [InputName.endDateTime]: endDateTime,
    [InputName.intervalWeek]: intervalWeek,
    [InputName.weekDays]: weekDays,
    [InputName.intervalMonth]: intervalMonth,
    [InputName.intervalMonthDay]: intervalMonthDay,
    [InputName.intervalHour]: intervalHour,
    [InputName.intervalDay]: intervalDay,
  };

  let hasErrors = false;
  for (const inputName of inputNames) {
    const error = validator(inputName, valueByInputName[inputName]);
    if (error) {
      hasErrors = true;
      errors[inputName] = error;
    }
  }

  return hasErrors ? errors : undefined;
};
