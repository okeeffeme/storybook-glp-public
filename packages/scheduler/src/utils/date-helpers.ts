import moment from 'moment';
import {DefaultDateFormat} from '@jotunheim/react-date-picker';

import {defaultTimeFormat} from '../constants';

interface DateRange {
  dateA: string | undefined;
  dateB: string | undefined;
}

export const getTime = (dateTime?: string) =>
  moment(dateTime).format(defaultTimeFormat);

export const getDate = (dateTime?: string) =>
  moment(dateTime).format(DefaultDateFormat);

export const getSummaryDate = (dateTime?: string) =>
  moment(dateTime).format(DefaultDateFormat);

export const appendTimeToDate = (dateTime: string, time: string) => {
  const [hours, minutes] =
    time.indexOf(':') >= 0 ? time.split(':') : time.split('.');

  if (hours && minutes) {
    return moment(dateTime)
      .hour(Number(hours))
      .minute(Number(minutes))
      .toISOString(true);
  }

  return moment(dateTime).toISOString(true);
};

export const isValidDate = (dateTime: string) => moment(dateTime).isValid();

export const stringifyDate = (dateTime: string) =>
  moment(dateTime).toISOString(true);

export const isDateABeforeDateB = ({dateA, dateB}: DateRange) =>
  moment(dateA).isBefore(dateB);

export const isDateASameOrAfterDateB = ({dateA, dateB}: DateRange) =>
  moment(dateA).isSameOrAfter(moment(dateB).startOf('day'));

export const isDateAndTimeASameOrAfterDateAndTimeB = ({
  dateA,
  dateB,
}: DateRange) => moment(dateA).isSameOrAfter(moment(dateB));

export const autoFormatTime = (value: string) => {
  if (value.length > 5) {
    return value;
  }

  const hasColonSeparator = value.indexOf(':') >= 0;
  const hasPeriodSeparator = value.indexOf('.') >= 0;

  if (!hasColonSeparator && !hasPeriodSeparator && value.match(/[^0-9.]/g)) {
    return value;
  }

  const [hours, minutes] = hasColonSeparator
    ? value.split(':')
    : hasPeriodSeparator
    ? value.split('.')
    : [value.substring(0, 2), value.substring(2)];

  if (hasPeriodSeparator) {
    return `${hours}.${minutes}`;
  }
  if (hasColonSeparator || value.length > 3) {
    return `${hours}:${minutes}`;
  }

  return value;
};
