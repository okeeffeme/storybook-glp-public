import {
  RecurrenceMode,
  SchedulerProps,
  SchedulerTexts,
  WeekDay,
} from '../types';
import {getSummaryDate, getTime} from './date-helpers';
import {defaultSchedulerTexts} from '../constants';

interface StartDateTime {
  startDateTime?: string;
}

interface DatePicker extends StartDateTime {
  endDateTime?: string;
}

interface HourSummary extends DatePicker {
  intervalHour?: number;
}

interface DailySummary extends DatePicker {
  intervalDay?: number;
}

interface WeeklySummary extends DatePicker {
  intervalWeek?: number;
  weekDays?: WeekDay[];
}

interface MonthSummary extends DatePicker {
  intervalMonthDay?: number;
  intervalMonth?: number;
}

interface ShowDateTime {
  showDateTimes?: boolean;
}

interface ShowWeek extends ShowDateTime {
  showWeekDays?: boolean;
}

interface ShowMonth extends ShowDateTime {
  showDayOfMonth?: boolean;
}

const buildRange = (
  startDateTime?: string,
  endDateTime?: string,
  texts: SchedulerTexts = defaultSchedulerTexts
) =>
  `${texts.at} ${getTime(startDateTime)} ${texts.from} ${getSummaryDate(
    startDateTime
  )} ${texts.until} ${getSummaryDate(endDateTime)}`;

const buildOneTimeSummary = (
  {startDateTime}: StartDateTime,
  {showDateTimes}: ShowDateTime,
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  return showDateTimes
    ? `${texts.summaryOncePrefix} ${texts.at} ${getTime(startDateTime)} ${
        texts.on
      } ${getSummaryDate(startDateTime)}`
    : texts.summaryOncePrefix;
};

const buildHourlySummary = (
  {startDateTime, endDateTime, intervalHour}: HourSummary,
  {showDateTimes}: ShowDateTime,
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  const intervalSummary = `${texts.summaryPrefix} ${intervalHour} ${texts.hours}`;
  const rangeSummary = `${texts.from} ${getTime(startDateTime)} ${
    texts.on
  } ${getSummaryDate(startDateTime)} ${texts.until} ${getSummaryDate(
    endDateTime
  )}`;
  return showDateTimes ? `${intervalSummary} ${rangeSummary}` : intervalSummary;
};

const buildDailySummary = (
  {startDateTime, endDateTime, intervalDay}: DailySummary,
  {showDateTimes}: ShowDateTime,
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  const intervalSummary = `${texts.summaryPrefix} ${intervalDay} ${texts.days}`;
  const rangeSummary = buildRange(startDateTime, endDateTime, texts);

  return showDateTimes ? `${intervalSummary} ${rangeSummary}` : intervalSummary;
};

const mapWeekdaysToTranslatedWeekdays = (
  input: WeekDay[],
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  return input.map((day) => texts[`${day.toLowerCase()}Long`]);
};

const stringifyWeekDays = (
  weekDays?: WeekDay[],
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  if (!weekDays) return '';

  const translatedWeekDays = mapWeekdaysToTranslatedWeekdays(weekDays, texts);
  if (translatedWeekDays.length === 1) {
    return translatedWeekDays[0];
  }

  const separator = ', ';
  const weekDaysString = translatedWeekDays.join(separator);
  const lastIndex = weekDaysString.lastIndexOf(separator);

  return `${weekDaysString.substring(0, lastIndex)} ${
    texts.and
  } ${weekDaysString.substring(
    lastIndex + separator.length,
    weekDaysString.length
  )}`;
};

const buildWeeklySummary = (
  {startDateTime, endDateTime, intervalWeek, weekDays}: WeeklySummary,
  {showDateTimes, showWeekDays}: ShowWeek,
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  const intervalSummary = showWeekDays
    ? `${texts.summaryPrefix} ${intervalWeek} ${texts.weeks} ${
        texts.on
      } ${stringifyWeekDays(weekDays, texts)}`
    : `${texts.summaryPrefix} ${intervalWeek} ${texts.weeks}`;
  const rangeSummary = buildRange(startDateTime, endDateTime, texts);

  return showDateTimes ? `${intervalSummary} ${rangeSummary}` : intervalSummary;
};

const buildMonthlySummary = (
  {startDateTime, endDateTime, intervalMonthDay, intervalMonth}: MonthSummary,
  {showDateTimes, showDayOfMonth}: ShowMonth,
  texts: SchedulerTexts = defaultSchedulerTexts
) => {
  const intervalSummary = showDayOfMonth
    ? `${texts.summaryPrefix} ${intervalMonth} ${texts.months} ${texts.on} ${texts.day} ${intervalMonthDay}`
    : `${texts.summaryPrefix} ${intervalMonth} ${texts.months}`;
  const rangeSummary = buildRange(startDateTime, endDateTime, texts);

  return showDateTimes ? `${intervalSummary} ${rangeSummary}` : intervalSummary;
};

export const getSummaryText = ({
  recurrenceMode,
  startDateTime,
  texts = defaultSchedulerTexts,
  showDateTimes,
  intervalHour,
  endDateTime,
  intervalDay,
  weekDays,
  intervalWeek,
  intervalMonth,
  intervalMonthDay,
  showWeekDays,
  showDayOfMonth,
}: SchedulerProps) => {
  switch (recurrenceMode) {
    case RecurrenceMode.OneTime:
      return buildOneTimeSummary({startDateTime}, {showDateTimes}, texts);
    case RecurrenceMode.Hourly:
      return buildHourlySummary(
        {startDateTime, endDateTime, intervalHour},

        {showDateTimes},
        texts
      );
    case RecurrenceMode.Daily:
      return buildDailySummary(
        {startDateTime, endDateTime, intervalDay},

        {showDateTimes},
        texts
      );
    case RecurrenceMode.Weekly:
      return buildWeeklySummary(
        {startDateTime, endDateTime, intervalWeek, weekDays},
        {
          showDateTimes,
          showWeekDays,
        },
        texts
      );
    case RecurrenceMode.Monthly:
      return buildMonthlySummary(
        {startDateTime, endDateTime, intervalMonthDay, intervalMonth},
        {
          showDateTimes,
          showDayOfMonth,
        },
        texts
      );
  }
};
