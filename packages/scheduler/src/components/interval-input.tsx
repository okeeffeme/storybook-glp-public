import React, {useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import useForm from './use-form';
import {baseClassName} from '../constants';
import SchedulerContext from './scheduler-context';

import {TextField} from '@jotunheim/react-text-field';

import classNames from './scheduler.module.css';

interface OnChangeHandler {
  field: string;
  value: number | string | string[];
}

interface IntervalInputProps {
  value: number;
  onChange: (obj: OnChangeHandler) => void;
  name: string;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const IntervalInput: FC<IntervalInputProps> = ({name, value, onChange}) => {
  const {disabled, showValidationErrors} = useContext(SchedulerContext);

  const {internalValue, validationError, onChangeCb} = useForm({
    name,
    value,
    onChange,
  });

  const hasError = showValidationErrors && Boolean(validationError);
  const errorText = hasError ? validationError : null;

  return (
    <div className={element('text-input')}>
      <TextField
        type="number"
        value={internalValue}
        onChange={onChangeCb}
        disabled={disabled}
        error={hasError}
        helperText={errorText}
        data-scheduler-input={name}
        data-testid="date-interval-input"
      />
    </div>
  );
};

export default IntervalInput;
