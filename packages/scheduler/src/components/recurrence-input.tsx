import React, {useContext, useMemo, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import useForm from './use-form';
import {baseClassName} from '../constants';
import SchedulerContext from './scheduler-context';

import Select from '@jotunheim/react-select';

import classNames from './scheduler.module.css';
import {RecurrenceMode} from '../types';

interface OnChangeHandler {
  field: string;
  value: number | string | string[];
}

interface RecurrenceInputProps {
  value: string;
  onChange: (obj: OnChangeHandler) => void;
  name: string;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const RecurrenceInput: FC<RecurrenceInputProps> = ({value, onChange, name}) => {
  const {disabled, texts, showDateTimes} = useContext(SchedulerContext);

  const {internalValue, onChangeCb} = useForm({
    name,
    value,
    onChange,
  });

  const recurrenceOptions = useMemo(() => {
    return Object.keys(RecurrenceMode)
      .filter((mode) => showDateTimes || mode !== RecurrenceMode.OneTime)
      .map((mode) => ({
        label: texts[mode] || RecurrenceMode[mode],
        value: mode,
      }));
  }, [showDateTimes, texts]);

  return (
    <div className={element('wide-input')}>
      <Select
        value={internalValue}
        label={texts.repeats}
        onChange={onChangeCb}
        options={recurrenceOptions}
        disabled={disabled}
        data-scheduler-input={name}
        data-testid="recurrence-input"
      />
    </div>
  );
};

export default RecurrenceInput;
