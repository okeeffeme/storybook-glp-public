import React, {useContext, FC} from 'react';

import {bemFactory} from '@jotunheim/react-themes';

import {baseClassName} from '../constants';
import SchedulerContext from './scheduler-context';
import type {WeekDay} from '../types';

import RecurrenceInput from './recurrence-input';
import IntervalInput from './interval-input';
import WeekDaysInput from './week-days-input';

import classNames from './scheduler.module.css';
import {InputName, RecurrenceMode} from '../types';

type OnChangeHandler = (params: {
  field: string;
  value: number | string | string[];
}) => void;

interface RecurrencePickerProps {
  onChange: OnChangeHandler;
  weekDays: WeekDay[];
  recurrenceMode?: RecurrenceMode;
  intervalHour?: number;
  intervalDay?: number;
  intervalWeek?: number;
  intervalMonth?: number;
  intervalMonthDay?: number;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const RecurrencePicker: FC<RecurrencePickerProps> = ({
  recurrenceMode = 'OneTime',
  onChange,
  intervalHour = 1,
  intervalDay = 1,
  intervalWeek = 1,
  weekDays = [],
  intervalMonth = 1,
  intervalMonthDay = 1,
}) => {
  const {texts, showWeekDays, showDayOfMonth} = useContext(SchedulerContext);

  return (
    <>
      <div className={element('row')}>
        <RecurrenceInput
          name={InputName.recurrenceMode}
          value={recurrenceMode}
          onChange={onChange}
        />
        {recurrenceMode === RecurrenceMode.Hourly && (
          <>
            <span className={element('text')}>{texts.every}</span>
            <IntervalInput
              name={InputName.intervalHour}
              value={intervalHour}
              onChange={onChange}
            />
            <span className={element('text')}>{texts.hours}</span>
          </>
        )}

        {recurrenceMode === RecurrenceMode.Daily && (
          <>
            <span className={element('text')}>{texts.every}</span>
            <IntervalInput
              name={InputName.intervalDay}
              value={intervalDay}
              onChange={onChange}
            />
            <span className={element('text')}>{texts.days}</span>
          </>
        )}

        {recurrenceMode === RecurrenceMode.Weekly && (
          <>
            <span className={element('text')}>{texts.every}</span>
            <IntervalInput
              name={InputName.intervalWeek}
              value={intervalWeek}
              onChange={onChange}
            />
            <span className={element('text')}>{texts.weeks}</span>
            {showWeekDays && (
              <div className={element('row')}>
                <WeekDaysInput
                  name={InputName.weekDays}
                  value={weekDays}
                  onChange={onChange}
                />
              </div>
            )}
          </>
        )}

        {recurrenceMode === RecurrenceMode.Monthly && (
          <>
            {showDayOfMonth ? (
              <>
                <span className={element('text')}>{texts.day}</span>
                <IntervalInput
                  name={InputName.intervalMonthDay}
                  value={intervalMonthDay}
                  onChange={onChange}
                />
                <span className={element('text')}>{texts.ofEvery}</span>
              </>
            ) : (
              <span className={element('text')}>{texts.every}</span>
            )}
            <IntervalInput
              name={InputName.intervalMonth}
              value={intervalMonth}
              onChange={onChange}
            />
            <span className={element('text')}>{texts.months}</span>
          </>
        )}
      </div>
    </>
  );
};

export default RecurrencePicker;
