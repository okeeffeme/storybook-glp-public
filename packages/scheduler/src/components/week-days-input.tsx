import React, {useContext, useMemo, FC} from 'react';
import {ToggleButton, ToggleButtonGroup} from '@jotunheim/react-toggle-button';

import {validWeekDays} from '../constants';
import SchedulerContext from './scheduler-context';
import useForm from './use-form';
import type {WeekDay} from '../types';

const formatDefaultWeekDay = (day) => {
  return day.substring(0, 3);
};

const weekDaySortMap = validWeekDays.map((day, i) => ({[day]: i}));

interface OnChangeHandler {
  field: string;
  value: number | string | string[];
}

interface WeekDaysInputProps {
  value: WeekDay[];
  onChange: (obj: OnChangeHandler) => void;
  name: string;
}

const WeekDaysInput: FC<WeekDaysInputProps> = ({
  name,
  value = [],
  onChange,
}) => {
  const {disabled, texts, showValidationErrors} = useContext(SchedulerContext);

  const {internalValue, validationError, onChangeCb} = useForm({
    name,
    value,
    onChange,
  });

  const weekDayOptions = useMemo(() => {
    return validWeekDays.map((day) => ({
      label: texts[day] || formatDefaultWeekDay(day),
      value: day,
      checked: internalValue.includes(day),
    }));
  }, [internalValue, texts]);

  const handleWeekDayChange = (e) => {
    const field = e.target.id;
    const checked = e.target.checked;

    const newWeekDays = validWeekDays
      .reduce((acc, day) => {
        if (checked && field === day) {
          acc.push(day);
        } else if (field !== day && internalValue.includes(day)) {
          acc.push(day);
        }
        return acc;
      }, [] as WeekDay[])
      .sort((dayA, dayB) => {
        const a = weekDaySortMap[dayA];
        const b = weekDaySortMap[dayB];
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }
        return 0;
      });

    onChangeCb(newWeekDays);
  };

  const hasError = showValidationErrors && Boolean(validationError);
  const errorText = hasError ? validationError : null;

  return (
    <ToggleButtonGroup
      label={texts.repeatsOn}
      error={hasError}
      helperText={errorText}
      data-scheduler-input={name}
      data-testid="week-days-input">
      {weekDayOptions.map((option) => (
        <ToggleButton
          key={option.value}
          checked={option.checked}
          onChange={handleWeekDayChange}
          text={option.label}
          id={option.value}
          disabled={disabled}
          data-scheduler-week-day-input={option.value}
        />
      ))}
    </ToggleButtonGroup>
  );
};

export default WeekDaysInput;
