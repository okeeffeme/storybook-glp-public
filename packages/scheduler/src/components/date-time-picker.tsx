import React, {useCallback, useState, FC} from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {isValidDate, appendTimeToDate, getTime} from '../utils/date-helpers';
import {baseClassName} from '../constants';

import DateInput from './date-input';
import TimeInput from './time-input';

import classNames from './scheduler.module.css';
import moment from 'moment';
import {InputName} from '../types';

interface OnChangeHandler {
  field: string;
  value: string;
}

interface DateTimePickerProps {
  onChange: (obj: OnChangeHandler) => void;
  label?: string;
  name: string;
  dateTime?: string;
  showTime?: boolean;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const DateTimePicker: FC<DateTimePickerProps> = ({
  onChange,
  label,
  name,
  dateTime = moment().toISOString(),
  showTime = true,
  ...rest
}) => {
  const [time, setTime] = useState(
    isValidDate(dateTime) ? getTime(dateTime) : ''
  );

  const handleDateChange = useCallback(
    ({value}) => {
      const dateWithTime = appendTimeToDate(value, time);
      onChange({
        field: name,
        value: dateWithTime,
      });

      return dateWithTime;
    },
    [name, time, onChange]
  );

  const handleTimeChange = useCallback(
    ({value}) => {
      setTime(value);
      if (isValidDate(dateTime)) {
        onChange({
          field: name,
          value: appendTimeToDate(dateTime, value),
        });
      }
    },
    [name, dateTime, onChange]
  );

  return (
    <div
      {...extractDataAndAriaProps(rest)}
      className={element('row')}
      data-testid="date-time-picker"
      data-scheduler-input={name}>
      <DateInput
        label={label}
        onChange={handleDateChange}
        value={dateTime}
        name={name}
      />
      {showTime && (
        <TimeInput
          onChange={handleTimeChange}
          value={isValidDate(dateTime) ? getTime(dateTime) : ''}
          name={InputName.time}
        />
      )}
    </div>
  );
};

export default DateTimePicker;
