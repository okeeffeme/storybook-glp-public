import React, {useContext, FC} from 'react';
import {bemFactory} from '@jotunheim/react-themes';
import {TextField} from '@jotunheim/react-text-field';

import useForm from './use-form';
import {baseClassName} from '../constants';
import SchedulerContext from './scheduler-context';

import classNames from './scheduler.module.css';

interface TimeInputProps {
  value: string;
  onChange: (input: string) => void;
  name: string;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const TimeInput: FC<TimeInputProps> = ({value, onChange, name}) => {
  const {disabled, showValidationErrors} = useContext(SchedulerContext);

  const {internalValue, validationError, onChangeCb} = useForm({
    name,
    value,
    onChange,
  });

  const hasError = showValidationErrors && Boolean(validationError);
  const errorText = hasError ? validationError : null;

  return (
    <div className={element('text-input')}>
      <TextField
        type="text"
        placeholder="hh:mm"
        value={internalValue}
        maxLength={5}
        onChange={onChangeCb}
        disabled={disabled}
        error={hasError}
        helperText={errorText}
        data-scheduler-time-input={true}
      />
    </div>
  );
};

export default TimeInput;
