import React, {useContext, FC} from 'react';
import {baseClassName} from '../constants';
import {getSummaryText} from '../utils/summary-builders';
import type {WeekDay} from '../types';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';

import SchedulerContext from './scheduler-context';

import classNames from './scheduler.module.css';
import moment from 'moment';
import {RecurrenceMode} from '../types';

interface SummaryProps {
  weekDays: WeekDay[];
  hasValidationError: boolean;
  recurrenceMode: RecurrenceMode;
  startDateTime?: string;
  endDateTime?: string;
  intervalHour?: number;
  intervalDay?: number;
  intervalWeek?: number;
  intervalMonth?: number;
  intervalMonthDay?: number;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const Summary: FC<SummaryProps> = ({
  recurrenceMode,
  hasValidationError,
  startDateTime = moment().toISOString(),
  endDateTime = moment().toISOString(),
  intervalHour = 1,
  intervalDay = 1,
  intervalWeek = 1,
  weekDays,
  intervalMonthDay = 1,
  intervalMonth = 1,
}) => {
  const {texts, showDateTimes, showWeekDays, showDayOfMonth} =
    useContext(SchedulerContext);

  if (hasValidationError) {
    return null;
  }

  return (
    <div
      data-testid="scheduler-summary"
      className={element('row')}
      data-scheduler-summary={true}>
      <span className={cn(element('summary'), element('text'))}>
        {texts.summary}:{' '}
        {getSummaryText({
          recurrenceMode,
          texts,
          showDateTimes,
          showWeekDays,
          showDayOfMonth,
          weekDays,
          intervalMonthDay,
          intervalMonth,
          intervalDay,
          intervalHour,
          intervalWeek,
          startDateTime,
          endDateTime,
        })}
      </span>
    </div>
  );
};

export default Summary;
