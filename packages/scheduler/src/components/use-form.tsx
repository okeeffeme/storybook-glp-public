import {useRef, useCallback, useState, useEffect, useContext} from 'react';

import {InputName} from '../types';
import {stringifyDate, autoFormatTime} from '../utils/date-helpers';
import SchedulerContext from './scheduler-context';
import {getValidator} from '../utils/getValidator';

const useForm = ({name, value, onChange}) => {
  const {
    texts,
    onValidationError,
    recurrenceMode,
    startDateTime = '',
    minDateTime = '',
    endDateTime = '',
  } = useContext(SchedulerContext);

  const [internalValue, setInternalValue] = useState(value);
  const firstRender = useRef(true);
  const [validationError, setValidationError] = useState<string | undefined>(
    undefined
  );

  const validate = useCallback(
    (name, value) => {
      return getValidator({
        startDateTime,
        endDateTime,
        recurrenceMode,
        minDateTime,
        texts,
      })(name, value);
    },
    [texts, startDateTime, endDateTime, recurrenceMode, minDateTime]
  );

  useEffect(() => {
    // validate input on first render
    if (firstRender.current) {
      firstRender.current = false;

      const error = validate(name, value);
      onValidationError({[name]: error});
      if (error) {
        setValidationError(error);
      } else {
        setValidationError(undefined);
      }
    }
  }, [onValidationError, validate, name, value]);

  useEffect(() => {
    if (!internalValue && !validationError) {
      setInternalValue(value);
    }
  }, [value, validationError, internalValue]);

  useEffect(() => {
    if (
      name === InputName.startDateTime ||
      name === InputName.endDateTime ||
      name === InputName.time
    ) {
      const error = validate(name, internalValue);
      onValidationError({[name]: error});
      if (error) {
        setValidationError(error);
      } else {
        setValidationError(undefined);
      }
    }
  }, [
    name,
    startDateTime,
    endDateTime,
    validate,
    internalValue,
    onValidationError,
  ]);

  const onChangeCb = (value) => {
    if (name === InputName.startDateTime || name === InputName.endDateTime) {
      value = stringifyDate(value);

      const updatedVal = onChange({field: name, value});

      setInternalValue(updatedVal);
    } else {
      if (name.includes('interval')) {
        value = value.length ? Number(value) : '';
      }
      if (name === InputName.time) {
        value = autoFormatTime(value);
      }
      onChange({field: name, value});

      setInternalValue(value);
    }

    const error = validate(name, value);
    onValidationError({[name]: error});

    if (error) {
      setValidationError(error);
    } else {
      setValidationError(undefined);
    }
  };

  return {
    internalValue,
    validationError,
    onChangeCb,
  };
};

export default useForm;
