import React, {
  useCallback,
  useMemo,
  useRef,
  useEffect,
  useState,
  FC,
} from 'react';
import moment from 'moment';
import SchedulerContext, {SchedulerContextState} from './scheduler-context';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import type {SchedulerProps} from '../types';

import DateTimePicker from './date-time-picker';
import RecurrencePicker from './recurrence-picker';
import Summary from './summary';
import {defaultSchedulerTexts, validWeekDays} from '../constants';
import {RecurrenceMode, InputName} from '../types';

const defaultInterval = 1;
const noop = () => {};

const getIsValid = (errors) =>
  Object.values(errors).filter(Boolean).length === 0;

type HandleScheduleChangeFn = (params: {
  field: string;
  value: number | string | string[];
}) => void;

const Scheduler: FC<
  SchedulerProps & {
    onScheduleChange?: (onScheduleChange: {
      [p: string]: number | string | string[];
    }) => void;
    onValidationChange?: ({isValid: boolean}) => void;
    showValidationErrors?: boolean;
    disabled?: boolean;
    readOnly?: boolean;
  }
> & {recurrenceModes: typeof RecurrenceMode} = ({
  showDateTimes = true,
  showWeekDays = true,
  showDayOfMonth = true,
  showValidationErrors = true,
  recurrenceMode = RecurrenceMode.OneTime,
  minDateTime,
  startDateTime,
  endDateTime,
  intervalHour,
  intervalDay,
  intervalWeek,
  intervalMonth,
  intervalMonthDay,
  weekDays = [],
  onScheduleChange,
  onValidationChange = noop,
  texts,
  disabled = false,
  readOnly = false,
  enableRecurrence = true,
  ...rest
}) => {
  const [validationErrors, setValidationErrors] = useState({});
  const prevIsValid = useRef(false); // set to false by default, so that onValidationChange will trigger on first render (given that the validation is valid)

  const mergedTexts = useMemo(
    () => ({
      ...defaultSchedulerTexts,
      ...texts,
    }),
    [texts]
  );

  useEffect(() => {
    const isValid = getIsValid(validationErrors);

    if (isValid !== prevIsValid.current) {
      onValidationChange({
        isValid,
      });
    }

    prevIsValid.current = isValid;
  }, [onValidationChange, validationErrors]);

  const schedulerContext = useMemo<SchedulerContextState>(() => {
    const handleValidationError = (field) => {
      setValidationErrors((prevErrors) => ({
        ...prevErrors,
        ...field,
      }));
    };

    return {
      texts: mergedTexts,
      disabled,
      showDateTimes,
      showWeekDays,
      showDayOfMonth,
      onValidationError: handleValidationError,
      startDateTime,
      minDateTime,
      endDateTime,
      recurrenceMode,
      showValidationErrors,
    };
  }, [
    mergedTexts,
    showDateTimes,
    showWeekDays,
    showDayOfMonth,
    disabled,
    startDateTime,
    minDateTime,
    endDateTime,
    recurrenceMode,
    showValidationErrors,
  ]);

  // set defaults
  useEffect(() => {
    const changes: Record<string, string | number> = {};

    if (showDateTimes && !startDateTime) {
      changes.startDateTime = moment().toISOString(true);
    }
    if (!recurrenceMode) {
      changes.recurrenceMode = showDateTimes
        ? RecurrenceMode.OneTime
        : RecurrenceMode.Hourly;
    }
    if (
      changes.recurrenceMode === RecurrenceMode.Hourly &&
      intervalHour === undefined
    ) {
      changes.intervalHour = defaultInterval;
    }

    if (Object.values(changes).length && onScheduleChange) {
      onScheduleChange(changes);
    }
  }, [
    showDateTimes,
    startDateTime,
    intervalHour,
    recurrenceMode,
    onScheduleChange,
  ]);

  const handleScheduleChange: HandleScheduleChangeFn = useCallback(
    ({field, value}) => {
      const changes = {[field]: value};

      if (field === InputName.recurrenceMode) {
        setValidationErrors({});
        if (value === RecurrenceMode.Hourly && intervalHour === undefined) {
          changes.intervalHour = defaultInterval;
        }
        if (value === RecurrenceMode.Daily && intervalDay === undefined) {
          changes.intervalDay = defaultInterval;
        }
        if (value === RecurrenceMode.Weekly && intervalWeek === undefined) {
          changes.intervalWeek = defaultInterval;
        }
        if (value === RecurrenceMode.Weekly && !weekDays.length) {
          // Get the initial day this way instead of via .format('dddd'), since moment may be initialized in a different language
          // causing format to return a day of week in a different language
          // isoWeekday starts at 1, so need to subtract 1
          changes.weekDays = [
            validWeekDays[moment(startDateTime).isoWeekday() - 1],
          ];
        }
        if (value === RecurrenceMode.Monthly && intervalMonth === undefined) {
          changes.intervalMonth = defaultInterval;
        }
        if (
          value === RecurrenceMode.Monthly &&
          intervalMonthDay === undefined
        ) {
          changes.intervalMonthDay = moment(startDateTime).date();
        }
        if (!endDateTime) {
          changes.endDateTime = moment(startDateTime)
            .add(1, 'year')
            .hours(0)
            .minutes(0)
            .toISOString(true);
        }
      }
      if (onScheduleChange) {
        onScheduleChange(changes);
      }
    },
    [
      onScheduleChange,
      intervalHour,
      intervalDay,
      intervalWeek,
      weekDays.length,
      intervalMonth,
      intervalMonthDay,
      startDateTime,
      endDateTime,
    ]
  );

  return (
    <SchedulerContext.Provider value={schedulerContext}>
      <div data-testid="scheduler" {...extractDataAriaIdProps(rest)}>
        {!readOnly && showDateTimes && (
          <DateTimePicker
            label={schedulerContext.texts.starts}
            dateTime={startDateTime}
            onChange={handleScheduleChange}
            name={InputName.startDateTime}
          />
        )}
        {!readOnly && enableRecurrence && (
          <RecurrencePicker
            recurrenceMode={recurrenceMode}
            onChange={handleScheduleChange}
            intervalHour={intervalHour}
            intervalDay={intervalDay}
            intervalWeek={intervalWeek}
            weekDays={weekDays}
            intervalMonth={intervalMonth}
            intervalMonthDay={intervalMonthDay}
          />
        )}
        {!readOnly &&
          enableRecurrence &&
          showDateTimes &&
          recurrenceMode &&
          recurrenceMode !== RecurrenceMode.OneTime && (
            <DateTimePicker
              label={schedulerContext.texts.ends}
              dateTime={endDateTime}
              onChange={handleScheduleChange}
              showTime={false}
              name={InputName.endDateTime}
            />
          )}
        <Summary
          recurrenceMode={recurrenceMode}
          intervalHour={intervalHour}
          intervalDay={intervalDay}
          intervalWeek={intervalWeek}
          weekDays={weekDays}
          intervalMonth={intervalMonth}
          intervalMonthDay={intervalMonthDay}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          hasValidationError={!getIsValid(validationErrors)}
        />
      </div>
    </SchedulerContext.Provider>
  );
};

Scheduler.recurrenceModes = RecurrenceMode;

export default Scheduler;
