import React, {useContext, FC} from 'react';
import moment from 'moment';
import {bemFactory} from '@jotunheim/react-themes';
import SchedulerContext from './scheduler-context';
import useForm from './use-form';
import {baseClassName} from '../constants';

import DatePicker from '@jotunheim/react-date-picker';

import classNames from './scheduler.module.css';

interface DateInputProps {
  value: string;
  onChange: (change: string) => void;
  label?: string;
  name: string;
}

const {element} = bemFactory({baseClassName: baseClassName, classNames});

const DateInput: FC<DateInputProps> = ({value, onChange, label, name}) => {
  const {disabled, minDateTime, showValidationErrors} =
    useContext(SchedulerContext);

  const {internalValue, validationError, onChangeCb} = useForm({
    name,
    value,
    onChange,
  });

  const hasError = showValidationErrors && Boolean(validationError);
  const errorText = hasError ? validationError : undefined;

  const getMinDate = () => (minDateTime ? moment(minDateTime) : undefined);

  return (
    <div className={element('wide-input')}>
      <DatePicker
        label={label}
        showLabel={true}
        date={moment(internalValue)}
        onChange={onChangeCb}
        disabled={disabled}
        hasError={hasError}
        errorText={errorText}
        minDate={getMinDate()}
        data-scheduler-date-input={true}
      />
    </div>
  );
};

export default DateInput;
