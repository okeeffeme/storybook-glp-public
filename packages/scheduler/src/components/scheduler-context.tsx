import React from 'react';
import type {SchedulerTexts} from '../types';

import {defaultSchedulerTexts} from '../constants';
import moment from 'moment';
import {RecurrenceMode} from '../types';

export interface SchedulerContextState {
  onValidationError: (obj: Record<string, string | undefined>) => void;

  disabled: boolean;
  showDateTimes: boolean;
  showDayOfMonth: boolean;
  showWeekDays: boolean;
  showValidationErrors: boolean;
  recurrenceMode: RecurrenceMode;
  startDateTime?: string;
  endDateTime?: string;
  minDateTime?: string;
  texts: SchedulerTexts;
}

const SchedulerContext = React.createContext<SchedulerContextState>({
  disabled: false,
  showDateTimes: false,
  showWeekDays: false,
  showDayOfMonth: false,
  startDateTime: moment().toISOString(),
  endDateTime: moment().toISOString(),
  minDateTime: moment().toISOString(),
  recurrenceMode: RecurrenceMode.OneTime,
  onValidationError: () => null,
  showValidationErrors: false,
  texts: defaultSchedulerTexts,
});

export default SchedulerContext;
