import React from 'react';
import {render, screen} from '@testing-library/react';
import {SurveyNodeBadges} from '../../src/components/SurveyNodeBadges';

import {alphaMBoxOutline, alphaVBoxOutline, eyeOff} from '../../../icons/src';

describe('Survey Node :: SurveyNodeBadges :: ', () => {
  it('should not show any badges when no badges is set', () => {
    render(<SurveyNodeBadges />);

    expect(screen.queryByTestId('survey-node-badge')).not.toBeInTheDocument();
  });

  it('should show correct amount of icon badges when multiple badges added', () => {
    render(
      <SurveyNodeBadges
        badges={[
          {
            tooltip: 'Hidden',
            iconPath: eyeOff,
          },
          {
            tooltip: 'Masking',
            iconPath: alphaMBoxOutline,
          },
          {
            tooltip: 'Validation',
            iconPath: alphaVBoxOutline,
          },
        ]}
      />
    );

    expect(screen.getByTestId('survey-node-badge').children.length).toBe(3);
  });

  it('should show correct Icon', () => {
    render(
      <SurveyNodeBadges
        badges={[
          {
            tooltip: 'Hidden',
            iconPath: eyeOff,
          },
        ]}
      />
    );

    expect(
      screen.getByTestId('survey-node-badge').querySelector('path')
    ).toHaveAttribute('d', eyeOff);
  });

  it('should render data attribute', () => {
    render(
      <SurveyNodeBadges
        badges={[
          {
            tooltip: 'Hidden',
            iconPath: eyeOff,
            ['data-testid']: 'hidden',
          },
          {
            tooltip: 'Masking',
            iconPath: alphaMBoxOutline,
            ['data-testid']: 'masking',
          },
        ]}
      />
    );

    const hiddenBadge = screen.getByTestId('hidden');
    const maskingBadge = screen.getByTestId('masking');

    expect(hiddenBadge.querySelector('path')).toHaveAttribute('d', eyeOff);
    expect(maskingBadge.querySelector('path')).toHaveAttribute(
      'd',
      alphaMBoxOutline
    );
  });
});
