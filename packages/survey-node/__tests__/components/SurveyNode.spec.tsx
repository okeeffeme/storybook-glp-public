import React from 'react';
import {render, screen} from '@testing-library/react';

import {SurveyNode, CustomQuestionIcon, NodeTypes} from '../../src';

describe('Survey Node :: SurveyNode :: ', () => {
  it('should show SurveyNode when minimum required props are given', () => {
    render(<SurveyNode nodeType={NodeTypes.SingleChoice} label="Label" />);

    expect(screen.getByText(/label/i)).toBeInTheDocument();
    expect(screen.getByTestId('SingleChoice')).toBeInTheDocument();
    expect(screen.queryByTestId('survey-node-badge')).not.toBeInTheDocument();
  });

  it('should show SurveyNode with badges when badges props are given', () => {
    render(
      <SurveyNode
        nodeType={NodeTypes.SingleChoice}
        label="Label"
        badges={[{tooltip: 'Hidden', iconPath: 'path'}]}
      />
    );

    expect(screen.getByText(/label/i)).toBeInTheDocument();
    expect(screen.getByTestId('SingleChoice')).toBeInTheDocument();
    expect(screen.queryByTestId('survey-node-badge')).toBeInTheDocument();
  });

  it('should call renderCustomQuestionIcon callback with correct props', () => {
    const renderer = jest.fn();

    render(
      <SurveyNode
        nodeType={NodeTypes.SingleChoice}
        label="Label"
        renderIcon={(props) => {
          renderer(props);
          return <CustomQuestionIcon {...props} />;
        }}
      />
    );

    expect(
      screen.getByTestId('custom-survey-node-icon-default')
    ).toBeInTheDocument();
    expect(screen.queryByTestId('SingleChoice')).not.toBeInTheDocument();
    expect(renderer).toBeCalledWith({
      iconSize: 24,
      nodeType: NodeTypes.SingleChoice,
    });
  });
});
