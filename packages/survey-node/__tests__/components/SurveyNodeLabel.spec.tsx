import React from 'react';
import {render, screen} from '@testing-library/react';

import {SurveyNodeLabel} from '../../src/components/SurveyNodeLabel';

describe('Survey Node :: SurveyNodeLabel :: ', () => {
  it('should render Label with only label prop', () => {
    render(<SurveyNodeLabel label="Label" />);

    expect(screen.getByText(/label/i)).toBeInTheDocument();
  });

  it('should render Label with name and label prop', () => {
    render(<SurveyNodeLabel name="Name" label="Label" />);

    expect(screen.getByText(/label/i)).toBeInTheDocument();
    expect(screen.getByText(/name/i)).toBeInTheDocument();
  });
});
