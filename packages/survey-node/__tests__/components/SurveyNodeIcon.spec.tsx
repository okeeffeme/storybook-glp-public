import React from 'react';
import {render, screen} from '@testing-library/react';

import {SurveyNodeIcon} from '../../src/components/SurveyNodeIcon';
import {
  single,
  open,
  multiOpen,
  info,
  calendarToday,
  fileTree,
  multi,
  numeric,
  multiOpenNumeric,
  trophyVariant,
  grid,
  multiGrid,
  grid3D,
  imageOutline,
  mapMarker,
  barcodeScan,
  folder,
  file,
  cube,
  cubeOutline,
  sync,
  condition,
  scriptTextOutline,
  formatPageBreak,
  directions,
  alertOctagon,
  email,
  finance,
  video,
  microphone,
  formatListBulletedSquare,
  phone,
  chartPie,
  noteText,
} from '../../../icons/src';
import {colors} from '../../../global-styles/src';
import {NodeTypes} from '../../src';

describe('Survey Node :: SurveyNodeIcon :: ', () => {
  it('should override default fill when passing fill', () => {
    render(<SurveyNodeIcon nodeType={NodeTypes.Text} fill="green" />);

    const icon = screen.getByTestId('Text');

    expect(icon).toHaveAttribute('fill', 'green');
    expect(icon.querySelector('path')).toHaveAttribute('d', open);
  });

  describe('nodeType icons :: ', () => {
    it('should show correct icon and fill for NodeTypes.Text nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Text} />);

      const icon = screen.getByTestId('Text');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', open);
    });

    it('should show correct icon and fill for NodeTypes.TextList nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.TextList} />);

      const icon = screen.getByTestId('TextList');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', multiOpen);
    });

    it('should show correct icon and fill for NodeTypes.Info nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Info} />);

      const icon = screen.getByTestId('Info');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', info);
    });

    it('should show correct icon and fill for NodeTypes.Date nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Date} />);

      const icon = screen.getByTestId('Date');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', calendarToday);
    });

    it('should show correct icon and fill for NodeTypes.SingleChoice nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.SingleChoice} />);

      const icon = screen.getByTestId('SingleChoice');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', single);
    });

    it('should show correct icon and fill for NodeTypes.Hierarchy nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Hierarchy} />);

      const icon = screen.getByTestId('Hierarchy');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', fileTree);
    });

    it('should show correct icon and fill for NodeTypes.MultiChoice nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.MultiChoice} />);

      const icon = screen.getByTestId('MultiChoice');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', multi);
    });

    it('should show correct icon and fill for NodeTypes.Numeric nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Numeric} />);

      const icon = screen.getByTestId('Numeric');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', numeric);
    });

    it('should show correct icon and fill for NodeTypes.NumericList nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.NumericList} />);

      const icon = screen.getByTestId('NumericList');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', multiOpenNumeric);
    });

    it('should show correct icon and fill for NodeTypes.Ranking nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Ranking} />);

      const icon = screen.getByTestId('Ranking');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', trophyVariant);
    });

    it('should show correct icon and fill for NodeTypes.Grid nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Grid} />);

      const icon = screen.getByTestId('Grid');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', grid);
    });

    it('should show correct icon and fill for NodeTypes.MultiChoiceGrid nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.MultiChoiceGrid} />);

      const icon = screen.getByTestId('MultiChoiceGrid');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', multiGrid);
    });

    it('should show correct icon and fill for NodeTypes.Grid3d nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Grid3d} />);

      const icon = screen.getByTestId('Grid3d');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', grid3D);
    });

    it('should show correct icon and fill for NodeTypes.Image nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Image} />);

      const icon = screen.getByTestId('Image');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', imageOutline);
    });

    it('should show correct icon and fill for NodeTypes.Geolocation nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Geolocation} />);

      const icon = screen.getByTestId('Geolocation');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', mapMarker);
    });

    it('should show correct icon and fill for NodeTypes.Barcode nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Barcode} />);

      const icon = screen.getByTestId('Barcode');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', barcodeScan);
    });

    it('should show correct icon and fill for NodeTypes.Folder nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Folder} />);

      const icon = screen.getByTestId('Folder');

      expect(icon).toHaveAttribute('fill', colors.Product17Yellow);
      expect(icon.querySelector('path')).toHaveAttribute('d', folder);
    });

    it('should show correct icon and fill for NodeTypes.Page nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Page} />);

      const icon = screen.getByTestId('Page');

      expect(icon).toHaveAttribute('fill', colors.Product06Turquoise);
      expect(icon.querySelector('path')).toHaveAttribute('d', file);
    });

    it('should show correct icon and fill for NodeTypes.Block nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Block} />);

      const icon = screen.getByTestId('Block');

      expect(icon).toHaveAttribute('fill', colors.Product01GreenDark);
      expect(icon.querySelector('path')).toHaveAttribute('d', cube);
    });

    it('should show correct icon and fill for NodeTypes.CallBlock nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.CallBlock} />);

      const icon = screen.getByTestId('CallBlock');

      expect(icon).toHaveAttribute('fill', colors.Product01GreenDark);
      expect(icon.querySelector('path')).toHaveAttribute('d', cubeOutline);
    });

    it('should show correct icon and fill for NodeTypes.Loop nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Loop} />);

      const icon = screen.getByTestId('Loop');

      expect(icon).toHaveAttribute('fill', colors.Product06Turquoise);
      expect(icon.querySelector('path')).toHaveAttribute('d', sync);
    });

    it('should show correct icon and fill for NodeTypes.Script nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Script} />);

      const icon = screen.getByTestId('Script');

      expect(icon).toHaveAttribute('fill', colors.Product11OrangeDark);
      expect(icon.querySelector('path')).toHaveAttribute(
        'd',
        scriptTextOutline
      );
    });

    it('should show correct icon and fill for NodeTypes.PageBreak nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.PageBreak} />);

      const icon = screen.getByTestId('PageBreak');

      expect(icon).toHaveAttribute('fill', colors.Product06Turquoise);
      expect(icon.querySelector('path')).toHaveAttribute('d', formatPageBreak);
    });

    it('should show correct icon and fill for NodeTypes.Directive nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Directive} />);

      const icon = screen.getByTestId('Directive');

      expect(icon).toHaveAttribute('fill', colors.Product10Purple);
      expect(icon.querySelector('path')).toHaveAttribute('d', directions);
    });

    it('should show correct icon and fill for NodeTypes.Stop nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Stop} />);

      const icon = screen.getByTestId('Stop');

      expect(icon).toHaveAttribute('fill', colors.Danger);
      expect(icon.querySelector('path')).toHaveAttribute('d', alertOctagon);
    });

    it('should show correct icon and fill for NodeTypes.Email nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Email} />);

      const icon = screen.getByTestId('Email');

      expect(icon).toHaveAttribute('fill', colors.Product06Turquoise);
      expect(icon.querySelector('path')).toHaveAttribute('d', email);
    });

    it('should show correct icon and fill for NodeTypes.Chart nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Chart} />);

      const icon = screen.getByTestId('Chart');

      expect(icon).toHaveAttribute('fill', colors.Product08PurpleDark);
      expect(icon.querySelector('path')).toHaveAttribute('d', finance);
    });

    it('should show correct icon and fill for NodeTypes.Video nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Video} />);

      const icon = screen.getByTestId('Video');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', video);
    });

    it('should show correct icon and fill for NodeTypes.Audio nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Audio} />);

      const icon = screen.getByTestId('Audio');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', microphone);
    });

    it('should show correct icon and fill for NodeTypes.List nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.List} />);

      const icon = screen.getByTestId('List');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute(
        'd',
        formatListBulletedSquare
      );
    });

    it('should show correct icon and fill for NodeTypes.Telephone nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Telephone} />);

      const icon = screen.getByTestId('Telephone');

      expect(icon).toHaveAttribute('fill', colors.Product13Orange);
      expect(icon.querySelector('path')).toHaveAttribute('d', phone);
    });

    it('should show correct icon and fill for NodeTypes.Quota nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Quota} />);

      const icon = screen.getByTestId('Quota');

      expect(icon).toHaveAttribute('fill', colors.PrimaryAccent);
      expect(icon.querySelector('path')).toHaveAttribute('d', chartPie);
    });

    it('should show correct icon and fill for NodeTypes.Condition nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Condition} />);

      const icon = screen.getByTestId('Condition');

      expect(icon).toHaveAttribute('fill', colors.Product13Orange);
      expect(icon.querySelector('path')).toHaveAttribute('d', condition);
    });

    it('should show correct icon and fill for NodeTypes.Note nodeType', () => {
      render(<SurveyNodeIcon nodeType={NodeTypes.Note} />);

      const icon = screen.getByTestId('Note');

      expect(icon).toHaveAttribute('fill', colors.Product17Yellow);
      expect(icon.querySelector('path')).toHaveAttribute('d', noteText);
    });
  });
});
