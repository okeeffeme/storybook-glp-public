import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';

import {CustomQuestionIcon} from '../../src/components/CustomQuestionIcon';

describe('Survey Node :: CustomQuestionIcon :: ', () => {
  it('should show loading dots when isLoading prop is true', () => {
    render(<CustomQuestionIcon isLoading={true} />);

    expect(
      screen.queryByTestId('custom-survey-node-icon-default')
    ).not.toBeInTheDocument();
  });

  it('should show default image icon when iconUrl is not set', () => {
    render(<CustomQuestionIcon />);

    expect(
      screen.getByTestId('custom-survey-node-icon-default')
    ).toBeInTheDocument();
  });

  it('should show broken image icon when img tag triggers error', () => {
    render(<CustomQuestionIcon iconUrl="some-url" />);

    const icon = screen.getByTestId('custom-survey-node-icon');

    expect(icon).toBeInTheDocument();
    expect(
      screen.queryByTestId('custom-survey-node-icon-broken')
    ).not.toBeInTheDocument();

    fireEvent.error(icon);

    expect(icon).not.toBeInTheDocument();
    expect(
      screen.queryByTestId('custom-survey-node-icon-broken')
    ).toBeInTheDocument();
  });

  it('should show img tag when iconUrl is set and no error is triggered', () => {
    render(<CustomQuestionIcon iconUrl="some-url" />);

    expect(screen.getByTestId('custom-survey-node-icon')).toBeInTheDocument();
    expect(
      screen.queryByTestId('custom-survey-node-icon-loading')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByTestId('custom-survey-node-icon-broken')
    ).not.toBeInTheDocument();
  });
});
