import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text, number, select} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {accountBox, eyeOff} from '../../icons/src';

import {
  SurveyNode,
  SurveyNodeIcon,
  CustomQuestionIcon,
  SurveyNodeBadges,
  SurveyNodeLabel,
  NodeTypes,
} from '../src';

import {DEFAULT_NODE_ICON_SIZE} from '../src/constants';

import customIcon from './custom-icon.png';

storiesOf('Components/survey-node', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('SurveyNode', () => (
    <SurveyNode
      name={text('name', 'Q5')}
      label={text('label', 'single')}
      nodeType={
        select(
          'nodeType',
          Object.keys(NodeTypes),
          Object.keys(NodeTypes)[0]
        ) as NodeTypes
      }
      badges={[
        {
          iconPath: eyeOff,
          tooltip: 'Hidden',
        },
        {
          iconPath: accountBox,
          tooltip: 'Background variable',
        },
      ]}
      iconSize={number('iconSize', DEFAULT_NODE_ICON_SIZE)}
    />
  ))
  .add('SurveyNode with Custom Question icon handling', () => (
    <SurveyNode
      name={text('name', 'Q5')}
      label={text('label', 'single')}
      nodeType={
        select(
          'nodeType',
          Object.keys(NodeTypes),
          Object.keys(NodeTypes)[0]
        ) as NodeTypes
      }
      badges={[
        {
          iconPath: eyeOff,
          tooltip: 'Hidden',
        },
        {
          iconPath: accountBox,
          tooltip: 'Background variable',
        },
      ]}
      iconSize={number('iconSize', DEFAULT_NODE_ICON_SIZE)}
      renderIcon={({iconSize, nodeType}) => {
        if (boolean('use custom question', true)) {
          return (
            <CustomQuestionIcon
              isLoading={boolean('isLoading', false)}
              iconUrl={text('iconUrl', customIcon)}
              iconSize={iconSize}
            />
          );
        }
        return <SurveyNodeIcon nodeType={nodeType} iconSize={iconSize} />;
      }}
    />
  ));

storiesOf('Components/survey-node/icon', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('SurveyNodeIcon', () => (
    <SurveyNodeIcon
      nodeType={
        select(
          'nodeType',
          Object.keys(NodeTypes),
          Object.keys(NodeTypes)[0]
        ) as NodeTypes
      }
      fill={text('fill', '')}
      iconSize={number('iconSize', 48)}
    />
  ))
  .add('All nodeType types', () => {
    return (
      <div>
        {Object.keys(NodeTypes).map((type) => {
          return (
            <SurveyNode
              key={type}
              label={type}
              nodeType={NodeTypes[type]}
              iconSize={number('iconSize', 48)}
            />
          );
        })}
      </div>
    );
  });

storiesOf('Components/survey-node/badge', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('SurveyNodeBadges', () => (
    <SurveyNodeBadges
      badges={[
        {
          iconPath: eyeOff,
          tooltip: 'Hidden',
          ['data-test-badge-type']: 'hidden',
        },
        {
          iconPath: accountBox,
          tooltip: 'Background variable',
          ['data-test-badge-type']: 'background variable',
        },
      ]}
    />
  ));

storiesOf('Components/survey-node/label', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('SurveyNodeLabel', () => (
    <SurveyNodeLabel
      name={text('name', 'Q5')}
      label={text('label', 'single')}
    />
  ));
