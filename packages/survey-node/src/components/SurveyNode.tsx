import React from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {SurveyNodeIcon, SurveyNodeIconProps} from './SurveyNodeIcon';
import {SurveyNodeLabel, SurveyNodeLabelProps} from './SurveyNodeLabel';
import {SurveyNodeBadges, SurveyNodeBadgesProps} from './SurveyNodeBadges';
import {DEFAULT_NODE_ICON_SIZE} from '../constants';

import classNames from './SurveyNode.module.css';

const {block, element} = bemFactory('comd-survey-node', classNames);

export type SurveyNodeProps = {
  isLoading?: boolean;
  renderIcon?: ({nodeType, iconSize}: SurveyNodeIconProps) => JSX.Element;
} & SurveyNodeLabelProps &
  SurveyNodeIconProps &
  SurveyNodeBadgesProps;

const defaultIconRenderer = ({nodeType, iconSize}: SurveyNodeIconProps) => {
  return <SurveyNodeIcon nodeType={nodeType} iconSize={iconSize} />;
};

/**
 * Returns a SurveyNodeIcon followed by SurveyNodeLabel and SurveyNodeBadges if badges object is set.
 *
 * @param name the first part of the title, will be displayed in bold weight font
 * @param label the second part of the title, will be displayed in normal weight font
 * @param nodeType the icon type to render
 * @callback renderIcon this should only be needed if you have to deal with custom question icons
 * @param iconSize defaults to 24
 * @param badges array of badge objects to be shown on the right side
 */
export const SurveyNode = ({
  name,
  label,
  nodeType,
  renderIcon = defaultIconRenderer,
  badges,
  iconSize = DEFAULT_NODE_ICON_SIZE,
  ...rest
}: SurveyNodeProps) => {
  return (
    <div
      className={block()}
      data-testid="survey-node"
      {...extractDataAriaIdProps(rest)}>
      <div className={element('icon')}>{renderIcon({nodeType, iconSize})}</div>

      <SurveyNodeLabel name={name} label={label} />

      {Boolean(badges) && (
        <div className={element('badges')}>
          <SurveyNodeBadges badges={badges} />
        </div>
      )}
    </div>
  );
};
