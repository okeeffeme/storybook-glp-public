import React from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './SurveyNodeLabel.module.css';

const {block, element} = bemFactory('comd-survey-node-label', classNames);

export type SurveyNodeLabelProps = {
  name?: string;
  label: string;
};

/**
 * @param name the first part of the title, will be displayed in bold weight font
 * @param label the second part of the title, will be displayed in normal weight font
 */
export const SurveyNodeLabel = ({
  name,
  label,
  ...rest
}: SurveyNodeLabelProps) => {
  return (
    <span
      data-testid="survey-node-label"
      title={label}
      className={block()}
      {...extractDataAriaIdProps(rest)}>
      {Boolean(name) && (
        <span data-test-node-name={name} className={element('name')}>
          {`${name} `}
        </span>
      )}
      <span data-testid={label} className={element('label')}>
        {label}
      </span>
    </span>
  );
};
