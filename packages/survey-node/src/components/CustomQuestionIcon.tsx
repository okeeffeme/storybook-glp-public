import React, {useState} from 'react';

import BusyDots from '@jotunheim/react-busy-dots';
import Icon, {
  imageBrokenVariant,
  defaultCustomQuestion,
} from '@jotunheim/react-icons';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import {DEFAULT_NODE_ICON_SIZE} from '../constants';

export type CustomQuestionIconProps = {
  iconUrl?: string | null;
  isLoading?: boolean;
  iconSize?: number | string;
};

/**
 * Returns an img tag with the src set to iconUrl, BusyDots component when loading or Icons when iconUrl resource cannot be loaded or is not set.
 * If iconUrl is not set, it will fall back to defaultCustomQuestion icon.
 * If iconUrl resource fails to load, it will fall back to imageBrokenVariant icon.
 *
 * @param iconUrl path to resource for custom question icon
 * @param isLoading show loading dots instead of icon
 * @param iconSize defaults to 24px
 */
export const CustomQuestionIcon = ({
  iconUrl,
  isLoading = false,
  iconSize = DEFAULT_NODE_ICON_SIZE,
  ...rest
}: CustomQuestionIconProps) => {
  const [imageBroken, setImageBroken] = useState(false);
  const filteredRestProps = extractDataAriaIdProps(rest);

  if (isLoading) {
    return (
      <BusyDots
        data-testid="custom-survey-node-icon-loading"
        {...filteredRestProps}
      />
    );
  }

  if (!iconUrl) {
    return (
      <Icon
        path={defaultCustomQuestion}
        data-testid="custom-survey-node-icon-default"
        {...filteredRestProps}
      />
    );
  }

  if (imageBroken) {
    return (
      <Icon
        path={imageBrokenVariant}
        data-testid="custom-survey-node-icon-broken"
        {...filteredRestProps}
      />
    );
  }

  return (
    <img
      src={iconUrl}
      width={iconSize}
      height={iconSize}
      onError={() => setImageBroken(true)}
      data-testid="custom-survey-node-icon"
      {...filteredRestProps}
    />
  );
};
