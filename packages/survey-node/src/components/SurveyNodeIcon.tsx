import React from 'react';
import cn from 'classnames';

import {colors} from '@jotunheim/global-styles';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Icon, {
  single,
  open,
  multiOpen,
  info,
  calendarToday,
  fileTree,
  multi,
  numeric,
  multiOpenNumeric,
  trophyVariant,
  maxdiff,
  grid,
  multiGrid,
  grid3D,
  imageOutline,
  mapMarker,
  barcodeScan,
  folder,
  file,
  cube,
  cubeOutline,
  sync,
  condition,
  scriptTextOutline,
  formatPageBreak,
  directions,
  alertOctagon,
  email,
  finance,
  video,
  microphone,
  formatListBulletedSquare,
  phone,
  chartPie,
  noteText,
} from '@jotunheim/react-icons';

import {DEFAULT_NODE_ICON_SIZE} from '../constants';
import {NodeTypes} from '../NodeTypes';

import classNames from './SurveyNodeIcon.module.css';

export type SurveyNodeIconProps = {
  nodeType: NodeTypes;
  iconSize?: number | string;
  fill?: string;
};

const {block} = bemFactory('comd-survey-node-icon', classNames);

const getFill = ({nodeType}) => {
  switch (nodeType) {
    case NodeTypes.Text:
    case NodeTypes.TextList:
    case NodeTypes.Info:
    case NodeTypes.Date:
    case NodeTypes.SingleChoice:
    case NodeTypes.Hierarchy:
    case NodeTypes.MultiChoice:
    case NodeTypes.Numeric:
    case NodeTypes.NumericList:
    case NodeTypes.Ranking:
    case NodeTypes.MaxDiff:
    case NodeTypes.Grid:
    case NodeTypes.MultiChoiceGrid:
    case NodeTypes.Grid3d:
    case NodeTypes.Image:
    case NodeTypes.Geolocation:
    case NodeTypes.Barcode:
    case NodeTypes.Video:
    case NodeTypes.Audio:
    case NodeTypes.List:
    case NodeTypes.Quota:
      return colors.PrimaryAccent;

    case NodeTypes.Page:
    case NodeTypes.Loop:
    case NodeTypes.Email:
    case NodeTypes.PageBreak:
      return colors.Product06Turquoise;

    case NodeTypes.Note:
    case NodeTypes.Folder:
      return colors.Product17Yellow;

    case NodeTypes.Block:
    case NodeTypes.CallBlock:
      return colors.Product01GreenDark;

    case NodeTypes.Condition:
    case NodeTypes.Telephone:
      return colors.Product13Orange;

    case NodeTypes.Script:
      return colors.Product11OrangeDark;

    case NodeTypes.Directive:
      return colors.Product10Purple;

    case NodeTypes.Stop:
      return colors.Danger;

    case NodeTypes.Chart:
      return colors.Product08PurpleDark;
  }

  return colors.TextSecondary;
};

/**
 * Returns an Icon component for the given nodeType, or null if the nodeType is not recognized.
 *
 * @param nodeType
 * @param iconSize defaults to 24
 */
export const SurveyNodeIcon = ({
  nodeType,
  iconSize = DEFAULT_NODE_ICON_SIZE,
  fill,
  ...rest
}: SurveyNodeIconProps) => {
  const iconProps = {
    ['data-testid']: nodeType,
    fill: fill ? fill : getFill({nodeType}),
    size: iconSize,
    className: cn(block()),
    ...extractDataAriaIdProps(rest),
  };

  switch (nodeType) {
    case NodeTypes.Text:
      return <Icon path={open} {...iconProps} />;
    case NodeTypes.TextList:
      return <Icon path={multiOpen} {...iconProps} />;
    case NodeTypes.Info:
      return <Icon path={info} {...iconProps} />;
    case NodeTypes.Date:
      return <Icon path={calendarToday} {...iconProps} />;
    case NodeTypes.SingleChoice:
      return <Icon path={single} {...iconProps} />;
    case NodeTypes.Hierarchy:
      return <Icon path={fileTree} {...iconProps} />;
    case NodeTypes.MultiChoice:
      return <Icon path={multi} {...iconProps} />;
    case NodeTypes.Numeric:
      return <Icon path={numeric} {...iconProps} />;
    case NodeTypes.NumericList:
      return <Icon path={multiOpenNumeric} {...iconProps} />;
    case NodeTypes.Ranking:
      return <Icon path={trophyVariant} {...iconProps} />;
    case NodeTypes.MaxDiff:
      return <Icon path={maxdiff} {...iconProps} />;
    case NodeTypes.Grid:
      return <Icon path={grid} {...iconProps} />;
    case NodeTypes.MultiChoiceGrid:
      return <Icon path={multiGrid} {...iconProps} />;
    case NodeTypes.Grid3d:
      return <Icon path={grid3D} {...iconProps} />;
    case NodeTypes.Image:
      return <Icon path={imageOutline} {...iconProps} />;
    case NodeTypes.Geolocation:
      return <Icon path={mapMarker} {...iconProps} />;
    case NodeTypes.Barcode:
      return <Icon path={barcodeScan} {...iconProps} />;
    case NodeTypes.Folder:
      return <Icon path={folder} {...iconProps} />;
    case NodeTypes.Page:
      return <Icon path={file} {...iconProps} />;
    case NodeTypes.Block:
      return <Icon path={cube} {...iconProps} />;
    case NodeTypes.CallBlock:
      return <Icon path={cubeOutline} {...iconProps} />;
    case NodeTypes.Loop:
      return <Icon path={sync} {...iconProps} />;
    case NodeTypes.Script:
      return <Icon path={scriptTextOutline} {...iconProps} />;
    case NodeTypes.PageBreak:
      return <Icon path={formatPageBreak} {...iconProps} />;
    case NodeTypes.Directive:
      return <Icon path={directions} {...iconProps} />;
    case NodeTypes.Stop:
      return <Icon path={alertOctagon} {...iconProps} />;
    case NodeTypes.Email:
      return <Icon path={email} {...iconProps} />;
    case NodeTypes.Chart:
      return <Icon path={finance} {...iconProps} />;
    case NodeTypes.Video:
      return <Icon path={video} {...iconProps} />;
    case NodeTypes.Audio:
      return <Icon path={microphone} {...iconProps} />;
    case NodeTypes.List:
      return <Icon path={formatListBulletedSquare} {...iconProps} />;
    case NodeTypes.Telephone:
      return <Icon path={phone} {...iconProps} />;
    case NodeTypes.Quota:
      return <Icon path={chartPie} {...iconProps} />;
    case NodeTypes.Condition:
      return <Icon path={condition} {...iconProps} />;
    case NodeTypes.Note:
      return <Icon path={noteText} {...iconProps} />;
    default:
      return null;
  }
};
