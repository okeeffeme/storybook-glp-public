import React from 'react';

import Icon from '@jotunheim/react-icons';
import Tooltip from '@jotunheim/react-tooltip';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

import {BADGE_ICON_SIZE} from '../constants';

import classNames from './SurveyNodeBadges.module.css';

const {block, element} = bemFactory('comd-survey-node-badges', classNames);

export type SurveyNodeBadgesProps = {
  badges?: Array<Badges>;
};

export type Badges = {
  iconPath: string;
  tooltip: string;

  // prevent TS complaining about adding extra props like data, aria and id-attributes
  [x: string]: string;
};

/**
 * Returns a node with some Icons and a description shown in a Tooltip, or null if no badges object is passed
 *
 * @param badges array of badge objects
 */
export const SurveyNodeBadges = ({badges}: SurveyNodeBadgesProps) => {
  if (!badges) {
    return null;
  }

  return (
    <div data-testid="survey-node-badge" className={block()}>
      {badges.map(({tooltip, iconPath, ...rest}, idx) => (
        <Tooltip key={idx} content={tooltip}>
          <div className={element('badge')} {...extractDataAriaIdProps(rest)}>
            <Icon path={iconPath} size={BADGE_ICON_SIZE} />
          </div>
        </Tooltip>
      ))}
    </div>
  );
};
