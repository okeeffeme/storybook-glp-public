# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.1.3&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.1.4&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.1.3&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.1.3&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.1.1&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.1.2&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.1.0&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.1.1&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-survey-node

# [2.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.41&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.1.0&targetRepoId=1246) (2023-03-17)

### Features

- add MaxDiff survey node icon ([e47df97](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e47df97e3f48bc4336c9831d0700ee2e5f45a2d9))

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.40&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.41&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.39&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.40&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.38&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.39&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.37&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.38&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.36&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.37&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.35&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.36&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.34&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.35&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.33&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.34&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.32&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.33&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.31&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.32&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.30&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.31&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.28&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.30&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.28&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.27&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.28&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.26&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.25&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.24&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.23&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.21&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.23&targetRepoId=1246) (2022-12-22)

### Bug Fixes

- add test ids for survey-node components ([NPM-1217](https://jiraosl.firmglobal.com/browse/NPM-1217)) ([2eac4a0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2eac4a085723c7f8e4294429f72f621b17dbe8fa))

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.21&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.18&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.18&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.18&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.17&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.16&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.14&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.13&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.14&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.12&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.11&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.10&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.9&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.8&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.6&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.3&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.2&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.1&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-survey-node

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-survey-node@2.0.0&sourceBranch=refs/tags/@jotunheim/react-survey-node@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-survey-node

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@1.0.3&sourceBranch=refs/tags/@confirmit/react-survey-node@1.0.4&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-survey-node

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@1.0.2&sourceBranch=refs/tags/@confirmit/react-survey-node@1.0.3&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-survey-node

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@1.0.1&sourceBranch=refs/tags/@confirmit/react-survey-node@1.0.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@1.0.0&sourceBranch=refs/tags/@confirmit/react-survey-node@1.0.1&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-survey-node

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.22&sourceBranch=refs/tags/@confirmit/react-survey-node@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Release version 1.0.0 ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.5.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.21&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.22&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.20&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.21&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.19&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.20&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.18&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.19&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.17&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.18&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.15&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.16&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.14&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.15&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.13&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.14&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.12&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.13&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.11&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.12&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.10&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.11&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.9&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.10&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.5.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.8&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.9&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.6&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.7&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.5&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.6&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.4&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.5&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.3&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.4&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.2&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.3&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.1&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.2&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.5.0&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.1&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-survey-node

# [0.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.7&sourceBranch=refs/tags/@confirmit/react-survey-node@0.5.0&targetRepoId=1246) (2021-07-28)

### Features

- allow passing aria, data and id attributes to SurveyNodeBadges ([NPM-836](https://jiraosl.firmglobal.com/browse/NPM-836)) ([3780828](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/37808289dec8bd1582d2d0745ec86fd5c2d836da))

## [0.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.6&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.7&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.5&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.6&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.4&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.5&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.3&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.4&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.2&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.3&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.1&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.2&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.4.0&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.1&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-survey-node

# [0.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.3.0&sourceBranch=refs/tags/@confirmit/react-survey-node@0.4.0&targetRepoId=1246) (2021-06-02)

### Features

- expose fill prop to allow override default color on SurveyNodeIcon ([NPM-797](https://jiraosl.firmglobal.com/browse/NPM-797)) ([140d788](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/140d7885f45beafb3855a855536b69686dafd428))

# [0.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.33&sourceBranch=refs/tags/@confirmit/react-survey-node@0.3.0&targetRepoId=1246) (2021-06-01)

### Features

- add 'Note' nodeType icon ([NPM-796](https://jiraosl.firmglobal.com/browse/NPM-796)) ([ec75e07](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec75e0775ff5c63cc670352b9088dc346ae53b62))

## [0.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.32&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.33&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.31&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.32&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.30&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.31&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.29&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.30&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.27&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.28&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.26&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.27&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.25&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.26&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.24&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.25&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.23&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.24&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.22&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.23&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.21&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.22&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.20&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.21&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.19&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.20&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.17&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.18&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.16&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.17&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.15&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.16&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.14&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.15&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.13&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.12&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.11&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.10&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.9&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.6&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.3&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.2&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.2.1&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-survey-node

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.11&sourceBranch=refs/tags/@confirmit/react-survey-node@0.2.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [0.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.10&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.11&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.9&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.10&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.6&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.7&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.5&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.6&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.4&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.5&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-survey-node

## [0.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-survey-node@0.1.1&sourceBranch=refs/tags/@confirmit/react-survey-node@0.1.2&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-survey-node

# 0.1.0 (2020-09-18)

### Bug Fixes

- update dependency version ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([aa55c40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/aa55c402d7e72e79eb8ee26ff776c8bdfb056e96)
- update deps ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([4057031](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/40570316777b0ca57cbba7d51a970f3f297b0d10)
- **survey-node:** remove unused propType and update node icon to be the latest version for Page ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([ddf1e36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ddf1e3622ce058d6dc67ab7889a2bb47a7aee599)
- **survey-node:** update node icon colors to latest spec ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([ab89e23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ab89e237b5b5ce084a5725fdc1784ccfe15e1691)
- update dependencies ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([4ddff3e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4ddff3e13c2bee9324a1de928eed782f89bd50d6)

### Features

- **survey-node:** use defaultCustomQuestion icon when no iconUrl is provided ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([ad45b7a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ad45b7a1c575cedaba146ade606bb43d4ce5b513)
- initial version of survey-node package ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([18a23e1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/18a23e1ce9a3501ef8df4ef35fce329b62830b71)

### v0.0.1

- Initial version
