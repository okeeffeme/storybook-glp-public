# Jotunheim React Survey Node

[Changelog](./CHANGELOG.md)

Contains Survey Node Icons and Survey Node Label with some metadata shown as badges

In most cases, you will use `SurveyNode` export, which will render icon, label and badges.

This package also exports `SurveyNodeIcon`, `CustomQuestionIcon`, `SurveyNodeLabel` and `SurveyNodeBadges` if you need to render these things separately. `NodeTypes` is an enum with all known node types that this component supports. You need to map your apps definitions of node types to the types in this package.

`renderIcon` render prop on `SurveyNode` should only be needed when you have to deal with Custom Question icons.

## Reference implementation

TODO: update these links after this component is used in SD...

For a full-fledged example, you can see the implementation in Survey Designer at these locations:

- [custom node icon](https://stashosl.firmglobal.com/projects/APP/repos/confirmit.surveydesigner.client/browse/www/js/app/components/icons/custom-node-icon.jsx)
- [custom question reducer](https://stashosl.firmglobal.com/projects/APP/repos/confirmit.surveydesigner.client/browse/www/js/app/modules/questionnaire/reducers/custom-questions.js)
- [node item](https://stashosl.firmglobal.com/projects/APP/repos/confirmit.surveydesigner.client/browse/www/js/app/components/piping-dialog/node-item.jsx)

## Basic usage, without handling custom node types

```jsx
import {SurveyNode} from '@jotunheim/react-survey-node';

const Node = ({name, label, nodeType, badges}) => {
  return (
    <SurveyNode name={name} label={label} nodeType={nodeType} badges={badges} />
  );
};
```

## Usage with custom node types, and a wrapper that is connected to a store

```jsx
import {SurveyNode, SurveyNodeIcon} from '@jotunheim/react-survey-node';

import CustomNodeIconWrapper from './CustomNodeIconWrapper';

const Node = ({name, label, isLoading, type, customQuestionId, badges}) => {
  return (
    <SurveyNode
      name={name}
      label={label}
      nodeType={type}
      iconSize={24}
      badges={badges}
      renderIcon={({nodeType, iconSize}) => {
        if (Boolean(customQuestionId)) {
          return (
            <CustomNodeIconWrapper
              iconSize={iconSize}
              isLoading={isLoading}
              customQuestionId={customQuestionId}
            />
          );
        }
        return <SurveyNodeIcon iconSize={iconSize} nodeType={nodeType} />;
      }}
    />
  );
};
```

```jsx
import {CustomQuestionIcon} from '@jotunheim/react-survey-node';

const CustomNodeIconWrapper = ({isLoading, iconUrl, iconSize, ...rest}) => {
  return (
    <CustomQuestionIcon
      isLoading={isLoading}
      iconUrl={iconUrl}
      iconSize={iconSize}
      {...rest}
    />
  );
};

const mapStateToProps = (state, {customQuestionId}) => {
  return {
    isLoading: SELECTORS.CUSTOMQUESTIONS.getListIsLoading(state),
    iconUrl: SELECTORS.CUSTOMQUESTIONS.getListIconUrl(state, customQuestionId),
  };
};

export default connect(mapStateToProps)(CustomNodeIconWrapper);
```

## Usage for standalone parts

### Icon

```jsx
import {SurveyNodeIcon} from '@jotunheim/react-survey-node';

const NodeIcon = ({iconSize, nodeType}) => (
  <SurveyNodeIcon iconSize={iconSize} nodeType={nodeType} />
);
```

### CustomQuestionIcon

```jsx
import {CustomQuestionIcon} from '@jotunheim/react-survey-node';

const CustomNodeIconWrapper = ({isLoading, iconUrl, iconSize, ...rest}) => {
  return (
    <CustomQuestionIcon
      isLoading={isLoading}
      iconUrl={iconUrl}
      iconSize={iconSize}
      {...rest}
    />
  );
};
```

### Label

```jsx
import {SurveyNodeLabel} from '@jotunheim/react-survey-node';

const Label = ({name, label}) => <SurveyNodeLabel name={name} label={label} />;
```

### Badges

```jsx
import {SurveyNodeBadges} from '@jotunheim/react-survey-node';

const Label = ({badges}) => (
  <SurveyNodeBadges
    badges={[
      {
        iconPath: eyeOff,
        tooltip: 'Hidden',
        ['data-test-badge-type']: 'hidden', // optional data attributes can be added
      },
      {
        iconPath: accountBox,
        tooltip: 'Background variable',
        ['data-test-badge-type']: 'background variable', // optional data attributes can be added
      },
    ]}
  />
);
```
