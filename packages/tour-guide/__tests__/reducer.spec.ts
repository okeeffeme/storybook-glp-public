import reducer, {getInitialState} from '../src/state/reducer';
import {ActionType} from '../src/state/action-types';
import {selectors} from '../src/state/selectors';

const defaultSettings = {
  steps: ['step1', 'step2', 'step3'],
  open: true,
};

const dispatchUpdateItemVisibility = (steps, state) => {
  return steps.reduce((state, step) => {
    return reducer(state, {
      type: ActionType.UpdateItemVisibility,
      payload: {
        stepName: step,
      },
    });
  }, state);
};

describe('State Update Utils ::', () => {
  it('Should update visible step', () => {
    const initialState = getInitialState(defaultSettings.steps);
    expect(initialState).toStrictEqual({
      currentStepIndex: 0,
      zIndex: null,
      tourSteps: defaultSettings.steps,
      visibleSteps: [],
    });

    const state0 = reducer(initialState, {
      type: ActionType.UpdateItemVisibility,
      payload: {
        stepName: 'step1',
      },
    });

    expect(selectors.getValidSteps(state0)).toEqual(['step1']);

    const state1 = reducer(state0, {
      type: ActionType.UpdateItemVisibility,
      payload: {
        stepName: 'step2',
      },
    });

    expect(selectors.getValidSteps(state1)).toEqual(['step1', 'step2']);
  });

  it('Should increment and decrement current step', () => {
    const initialState = getInitialState(defaultSettings.steps);
    const newState = dispatchUpdateItemVisibility(
      ['step1', 'step2', 'step3'],
      initialState
    );

    expect(selectors.getValidSteps(newState)).toEqual([
      'step1',
      'step2',
      'step3',
    ]);

    const dispatchIncrement = (state) =>
      reducer(state, {
        type: ActionType.Increment,
      });

    const dispatchDecrement = (state) =>
      reducer(state, {
        type: ActionType.Decrement,
      });

    const newState1 = dispatchIncrement(initialState);
    expect(newState1.currentStepIndex).toBe(1);

    const newState2 = dispatchIncrement(newState1);
    expect(newState2.currentStepIndex).toBe(2);

    const newState3 = dispatchDecrement(newState2);
    expect(newState3.currentStepIndex).toBe(1);
  });

  it('Should update current step when goTo dispatch', () => {
    const initialState = getInitialState(defaultSettings.steps);

    const newState = reducer(initialState, {
      type: ActionType.GoTo,
      payload: {
        stepIndex: 2,
      },
    });

    expect(newState.currentStepIndex).toBe(2);
  });

  it('Should reset current step to 0 when close dispatch', () => {
    const initialState = getInitialState(defaultSettings.steps);

    const newState0 = reducer(initialState, {
      type: ActionType.GoTo,
      payload: {
        stepIndex: 2,
      },
    });

    expect(newState0.currentStepIndex).toBe(2);

    const newState1 = reducer(initialState, {
      type: ActionType.Close,
    });

    expect(newState1.currentStepIndex).toBe(0);
  });
});
