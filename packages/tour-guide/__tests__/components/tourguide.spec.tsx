import React, {useState} from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {TourGuide} from '../../src';
import Context from '../../src/context';

jest.useFakeTimers();

const TEST_TOUR = 'test_tour';

const defaultProps = {
  onGoToNext: () => {},
  onGoToPrevious: () => {},
  onItemRendered: () => {},
  onClose: () => {},
  tour: TEST_TOUR,
  steps: ['step1', 'step2', 'step3'],
};

interface TourGuideStructureProps {
  isOpen: boolean;
  onReset: () => void;
  onClose: () => void;
}

const TourGuideStructure: React.FC<TourGuideStructureProps> = ({
  isOpen,
  onReset,
  onClose,
}) => (
  <TourGuide {...defaultProps} open={isOpen} onClose={onClose}>
    <TourGuide.Item
      tour={TEST_TOUR}
      step={'step1'}
      title={'step1'}
      content={'step1 content'}
      placement={'bottom'}>
      <span data-testid="step1-button">button 1</span>
    </TourGuide.Item>
    <TourGuide.Item
      tour={TEST_TOUR}
      step={'step2'}
      title={'step2'}
      content={'step2 content'}
      placement={'bottom'}>
      <span data-testid="step2-button">button 2</span>
    </TourGuide.Item>
    <TourGuide.Item
      tour={TEST_TOUR}
      step={'step3'}
      title={'step3'}
      content={'step3 content'}
      placement={'bottom'}>
      <span data-testid="step3-button">button 3</span>
    </TourGuide.Item>
    <TourGuide.ResetItem
      tour={TEST_TOUR}
      content={'click here to reset'}
      placement={'bottom'}>
      <div data-testid="reset-child" data-reset-child>
        something
      </div>
    </TourGuide.ResetItem>
    <button onClick={onReset} data-testid="tour-guide-reset">
      reset
    </button>
  </TourGuide>
);

const handleOnReset = jest.fn((setter) => {
  setter(true);
});

const handleOnClose = jest.fn((setter) => {
  setter(false);
});

const NavigationTourGuide = () => {
  const [isOpen, setIsOpen] = useState(true);

  return (
    <TourGuideStructure
      isOpen={isOpen}
      onReset={() => handleOnReset(setIsOpen)}
      onClose={() => handleOnClose(setIsOpen)}
    />
  );
};

describe('Jotunheim React Tour Guide :: ', () => {
  it('should have backdrop when displaying item 2', () => {
    render(
      <TourGuide.Item
        tour={TEST_TOUR}
        step={'step2'}
        title={'step2'}
        content={'step2 content'}
        placement={'bottom'}>
        <span data-testid="step2-button">button 2</span>
      </TourGuide.Item>,
      {
        wrapper: ({children}) => (
          <Context.Provider
            value={{
              ...defaultProps,
              open: true,
              currentStepIndex: 1,
              backdropZIndex: 1,
              popoverZIndex: 2,
            }}>
            {children}
          </Context.Provider>
        ),
      }
    );

    const transitionPortals = screen.getAllByTestId('transition-portal');

    expect(transitionPortals.length).toBe(2);
    expect(transitionPortals[0]).toHaveAttribute(
      'data-tour-guide-backdrop',
      'step2'
    );
  });

  describe('navigation', () => {
    it('Should navigate from item 1 to item 2', () => {
      render(<NavigationTourGuide />);

      userEvent.click(screen.getByTestId('tour-guide-next'));

      const tourGuidePopovers = screen.getAllByTestId(
        'data-tour-guide-popover'
      );

      expect(tourGuidePopovers[0]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
      expect(tourGuidePopovers[1]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
      expect(screen.getByText('step1')).toBeInTheDocument();
      expect(screen.getByText('step2')).toBeInTheDocument();
    });

    it('Should navigate from item 2 to item 3', () => {
      render(<NavigationTourGuide />);

      userEvent.click(screen.getByTestId('tour-guide-next'));
      userEvent.click(screen.getAllByTestId('tour-guide-next')[1]);

      const tourGuidePopovers = screen.getAllByTestId(
        'data-tour-guide-popover'
      );

      expect(tourGuidePopovers[0]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
      expect(tourGuidePopovers[1]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
      expect(tourGuidePopovers[2]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
    });

    it('Should navigate from item 3 back to item 2', () => {
      render(<NavigationTourGuide />);

      userEvent.click(screen.getByTestId('tour-guide-next'));
      userEvent.click(screen.getAllByTestId('tour-guide-next')[1]);
      userEvent.click(screen.getAllByTestId('tour-guide-previous')[2]);

      const tourGuidePopovers = screen.getAllByTestId(
        'data-tour-guide-popover'
      );

      expect(tourGuidePopovers[0]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
      expect(tourGuidePopovers[1]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
      expect(tourGuidePopovers[2]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
    });

    it('Should call onClose when tour guide is closed and close all items', () => {
      render(<NavigationTourGuide />);

      userEvent.click(screen.getByTestId('tour-guide-close'));

      expect(handleOnClose).toHaveBeenCalledTimes(1);

      const tourGuidePopover = screen.getByTestId('data-tour-guide-popover');

      expect(tourGuidePopover).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'false'
      );
    });

    it('Should call onReset when reset button is clicked and reopen the first item', () => {
      render(<NavigationTourGuide />);

      userEvent.click(screen.getByText('reset'));

      expect(handleOnReset).toHaveBeenCalledTimes(1);

      const tourGuidePopovers = screen.getAllByTestId(
        'data-tour-guide-popover'
      );

      expect(tourGuidePopovers.length).toBe(1);
      expect(tourGuidePopovers[0]).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
    });
  });

  describe('reset', () => {
    const onReset = jest.fn((setter) => {
      setter(true);
    });

    const onClose = jest.fn((setter) => {
      setter(false);
    });

    const ResetTourGuide = () => {
      const [isOpen, setIsOpen] = useState(true);

      return (
        <TourGuideStructure
          isOpen={isOpen}
          onReset={() => onReset(setIsOpen)}
          onClose={() => onClose(setIsOpen)}
        />
      );
    };

    it('should show reset item when tour is closed', () => {
      render(<ResetTourGuide />);
      expect(screen.getByTestId('data-tour-guide-popover')).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );

      userEvent.click(screen.getByTestId('tour-guide-close'));

      expect(
        screen.getAllByTestId('data-tour-guide-popover')[0]
      ).toHaveAttribute('data-tour-guide-popover-is-active', 'false');
    });

    it('should show reset again when tour is closed again', () => {
      render(<ResetTourGuide />);

      expect(screen.getByTestId('data-tour-guide-popover')).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
      expect(screen.getByTestId('reset-child')).not.toHaveAttribute(
        'data-portal-trigger-active'
      );

      userEvent.click(screen.getByTestId('tour-guide-close'));

      expect(screen.getByTestId('reset-child')).toHaveAttribute(
        'data-portal-trigger-active'
      );
      expect(
        screen.getAllByTestId('data-tour-guide-popover')[0]
      ).toHaveAttribute('data-tour-guide-popover-is-active', 'false');
    });

    it('should show tour again when reset', () => {
      render(<ResetTourGuide />);

      expect(screen.getByTestId('data-tour-guide-popover')).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );

      userEvent.click(screen.getByTestId('tour-guide-reset'));

      expect(
        screen.getAllByTestId('data-tour-guide-popover')[0]
      ).toHaveAttribute('data-tour-guide-popover-is-active', 'true');
    });

    it('should hide tour and reset should NOT be displayed again when closed again', () => {
      render(<ResetTourGuide />);

      expect(screen.getByTestId('data-tour-guide-popover')).toHaveAttribute(
        'data-tour-guide-popover-is-active',
        'true'
      );
      expect(screen.getByTestId('reset-child')).not.toHaveAttribute(
        'data-portal-trigger-active',
        ''
      );

      userEvent.click(screen.getByTestId('tour-guide-close'));

      expect(screen.getByTestId('reset-child')).toHaveAttribute(
        'data-portal-trigger-active',
        ''
      );
      expect(
        screen.getAllByTestId('data-tour-guide-popover')[0]
      ).toHaveAttribute('data-tour-guide-popover-is-active', 'false');
    });
  });

  describe('Interaction with document.body', () => {
    it('should not add className to body if not open while mounting', () => {
      render(
        <TourGuide {...defaultProps} open={false}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('');
    });

    it('should add className to body once opened', () => {
      const {rerender} = render(
        <TourGuide {...defaultProps} open={false}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('');

      rerender(
        <TourGuide {...defaultProps} open={true}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('comd-tour-guide__root-body');
    });

    it('should add className to body if open while mounting', () => {
      render(
        <TourGuide {...defaultProps} open={true}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('comd-tour-guide__root-body');
    });

    it('should remove className from body once closed', () => {
      const {rerender} = render(
        <TourGuide {...defaultProps} open={true}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('comd-tour-guide__root-body');

      rerender(
        <TourGuide {...defaultProps} open={false}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('');
    });

    it('should remove className from body once unmounted', () => {
      const {unmount} = render(
        <TourGuide {...defaultProps} open={true}>
          <TourGuide.Item
            tour={TEST_TOUR}
            step={'step1'}
            title={'step1'}
            content={'step1 content'}
            placement={'bottom'}>
            <div>something</div>
          </TourGuide.Item>
        </TourGuide>
      );

      expect(document.body.className).toBe('comd-tour-guide__root-body');

      unmount();

      expect(document.body.className).toBe('');
    });
  });
});
