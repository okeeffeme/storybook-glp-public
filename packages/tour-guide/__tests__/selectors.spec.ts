import {selectors} from '../src/state/selectors';

const state = {
  currentStepIndex: 2,
  zIndex: 10,
  tourSteps: ['step0', 'step1', 'step2', 'step3', 'step4'],
  visibleSteps: ['step0', 'step3', 'step4'],
};

describe('State Selectors Utils ::', () => {
  it('Should get correct currentStepIx', () => {
    const currentStepIndex = selectors.getCurrentStepIndex(state);

    expect(currentStepIndex).toEqual(2);
  });

  it('Should get correct zIndex', () => {
    const tourSteps = selectors.getZIndex(state);
    expect(tourSteps).toEqual(10);
  });

  it('Should get correct visible steps', () => {
    const visibleSteps = selectors.getVisibleSteps(state);
    expect(visibleSteps).toEqual(['step0', 'step3', 'step4']);
  });

  it('Should get correct tour steps', () => {
    const tourSteps = selectors.getTourSteps(state);
    expect(tourSteps).toEqual(['step0', 'step1', 'step2', 'step3', 'step4']);
  });
});
