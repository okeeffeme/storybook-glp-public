import React, {useCallback, useState} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {TourGuide} from '../src';
import Button from '../../button/src';

const TOUR_NAME = 'storybook-guide';

const Story = () => {
  const [isTourOpen, setIsTourOpen] = useState(true);

  const handleUpdate = useCallback(
    (isOpen) => {
      setIsTourOpen(isOpen);
    },
    [setIsTourOpen]
  );

  const removeLocalStorageForTourGuide = () => {
    localStorage.removeItem(`cf:tour-guide-dismissed`);
  };

  return (
    <TourGuide
      tour={TOUR_NAME}
      steps={['step1', 'step2', 'step3', 'step4', 'step5']}
      open={isTourOpen}
      onClose={() => {
        handleUpdate(false);
      }}>
      {false && (
        <TourGuide.Item
          title="Step 4 title"
          tour={TOUR_NAME}
          step={'step4'}
          placement={'bottom-end'}>
          <div
            style={{
              backgroundColor: 'aliceblue',
              width: '150px',
              height: '200px',
            }}>
            I am a box
          </div>
        </TourGuide.Item>
      )}
      <TourGuide.ResetItem
        tour={TOUR_NAME}
        placement={'bottom-end'}
        title={'You can go over the walk-through anytime'}
        content={
          <div>
            Just click <b>reset walk-through</b> in the help menu to start again
            or visit our knowledge base to learn more about this application.
          </div>
        }>
        <div
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            backgroundColor: 'red',
            width: '40px',
            height: '36px',
          }}
        />
      </TourGuide.ResetItem>
      <div>
        <TourGuide.Item
          tour={TOUR_NAME}
          step={'step1'}
          placement={'bottom-end'}
          title={'This is the title of step 1'}
          content={'step 1.'}>
          <span>
            <Button>Step 1</Button>
          </span>
        </TourGuide.Item>
        <TourGuide.Item
          tour={TOUR_NAME}
          step={'step2'}
          placement={'bottom-start'}
          title={
            'The cutout is not showing in this item because the child ref is not exposed'
          }
          content={
            <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          }>
          <Button>Step 2</Button>
        </TourGuide.Item>
        <TourGuide.Item
          tour={TOUR_NAME}
          step={'step3'}
          placement={'bottom'}
          title={'This is the title of step 3'}
          content={
            <>
              <div>Step 3</div>
              <div>You can close the Tour Guide any time.</div>
            </>
          }>
          <span>
            <Button>Step 3</Button>
          </span>
        </TourGuide.Item>
      </div>
      <Button onClick={() => handleUpdate(true)}>Reset Tour Guide</Button>
      <div>
        <Button onClick={removeLocalStorageForTourGuide}>
          Delete localstorage item (to see the last step again)
        </Button>
      </div>
    </TourGuide>
  );
};

storiesOf('Components/tour-guide', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    return <Story />;
  });
