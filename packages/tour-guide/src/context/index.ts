import {createContext} from 'react';

interface State {
  onGoToNext: () => void;
  onGoToPrevious: () => void;
  onItemRendered: (stepName: string) => void;
  onClose: () => void;
  open: boolean;
  steps: string[];
  popoverZIndex: number;
  backdropZIndex: number;
  tour?: string;
  currentStepIndex: number;
}

export default createContext<State>({
  onGoToNext: () => {},
  onGoToPrevious: () => {},
  onItemRendered: () => {},
  onClose: () => {},
  tour: undefined,
  open: false,
  steps: [],
  currentStepIndex: 0,
  popoverZIndex: 0,
  backdropZIndex: 0,
});
