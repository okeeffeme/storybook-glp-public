import {ActionType, TourGuideAction} from './action-types';

export type State = {
  currentStepIndex: number;
  zIndex: number | null;
  tourSteps: string[];
  visibleSteps: string[];
};

export const getInitialState = (steps: string[]) => {
  return {
    currentStepIndex: 0,
    zIndex: null,
    tourSteps: steps,
    visibleSteps: [],
  };
};

export default function reducer(state: State, action: TourGuideAction) {
  switch (action.type) {
    case ActionType.Increment: {
      return {...state, currentStepIndex: state.currentStepIndex + 1};
    }

    case ActionType.Decrement: {
      return {...state, currentStepIndex: state.currentStepIndex - 1};
    }

    case ActionType.GoTo: {
      const {stepIndex} = action.payload;

      return {
        ...state,
        currentStepIndex: stepIndex,
      };
    }

    case ActionType.UpdateItemVisibility: {
      const {stepName} = action.payload;
      if (!state.visibleSteps.includes(stepName)) {
        const visibleSteps = [...state.visibleSteps, stepName];

        return {...state, visibleSteps};
      }
      return state;
    }

    case ActionType.UpdateZIndex: {
      const {zIndex} = action.payload;
      return {
        ...state,
        zIndex,
      };
    }

    case ActionType.Close: {
      return {...state, currentStepIndex: 0};
    }

    default:
      return state;
  }
}
