import type {State} from './reducer';

export const selectors = {
  getValidSteps: (state: State) =>
    state.tourSteps.filter((step) => state.visibleSteps.includes(step)),

  getVisibleSteps: (state: State) => state.visibleSteps,

  getCurrentStepIndex: (state: State) => state.currentStepIndex,

  getTourSteps: (state: State) => state.tourSteps,

  getZIndex: (state: State) => state.zIndex,
};
