export enum ActionType {
  Increment = 'Increment',
  Decrement = 'Decrement',
  GoTo = 'GoTo',
  UpdateItemVisibility = 'UpdateItemVisibility',
  UpdateZIndex = 'UpdateZIndex',
  Close = 'Close',
}

export interface IncrementAction {
  type: ActionType.Increment;
}

export interface DecrementAction {
  type: ActionType.Decrement;
}

export interface GoToAction {
  type: ActionType.GoTo;
  payload: {stepIndex: number};
}

export interface UpdateItemVisibilityAction {
  type: ActionType.UpdateItemVisibility;
  payload: {stepName: string};
}

export interface UpdateZIndexAction {
  type: ActionType.UpdateZIndex;
  payload: {zIndex: number | null};
}

export interface CloseAction {
  type: ActionType.Close;
}

export type TourGuideAction =
  | CloseAction
  | UpdateZIndexAction
  | DecrementAction
  | GoToAction
  | IncrementAction
  | UpdateItemVisibilityAction;
