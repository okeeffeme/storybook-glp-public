import React, {FC, ReactNode} from 'react';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import cn from 'classnames';

import Header from './header';
import NavigationControl from './navigation-control';

import componentThemes from '../themes';
import TourGuideContext from '../context';

interface ItemContentProps {
  title: string;
  content: string | ReactNode;
  navigationNextText: string;
  navigationPrevText: string;
  abortText: string;
}

const ItemContent: FC<ItemContentProps> = ({
  title,
  content,
  navigationNextText,
  navigationPrevText,
  abortText,
  ...rest
}) => {
  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);
  const {element} = bemFactory({baseClassName, classNames});
  const classes = cn(element('item-content'));
  const {onClose} = React.useContext(TourGuideContext);

  return (
    <div className={classes} {...extractDataAriaIdProps(rest)}>
      <Header title={title} onClose={onClose} />
      <div className={cn(element('content'))}>{content}</div>
      <div className={cn(element('footer'))}>
        <NavigationControl
          abortText={abortText}
          prevButtonText={navigationPrevText}
          nextButtonText={navigationNextText}
        />
      </div>
    </div>
  );
};

export default ItemContent;
