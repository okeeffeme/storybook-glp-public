import React, {
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useRef,
  FC,
  PropsWithChildren,
} from 'react';

import {bemFactory, useTheme} from '@jotunheim/react-themes';
import {useZIndexStack} from '@jotunheim/react-contexts';
import {
  addClassNameToDOMNode,
  removeClassNameFromDOMNode,
} from '@jotunheim/react-utils';

import Item, {ItemProps} from './item';
import ResetItem, {ResetItemProps} from './reset-item';
import TourGuideContext from '../context';
import TourGuideReducer, {getInitialState} from '../state/reducer';
import {ActionType} from '../state/action-types';
import {selectors} from '../state/selectors';
import componentThemes from '../themes';

interface ScrollBarProps {
  bodyClassName: string;
}

interface TourGuideProps {
  tour: string;
  steps: string[];
  onClose: () => void;
  open?: boolean;
}

const getValidSteps = (steps: string[], visibleSteps: string[]) =>
  steps.filter((step) => visibleSteps.includes(step));

const bodyIsOverflowing = () =>
  document.body.scrollHeight > document.documentElement.clientHeight;

const getScrollbarWidth = () => {
  const scrollDiv = document.createElement('div');
  scrollDiv.style.cssText =
    'position: absolute; z-index: -1; opacity: 0; width: 50px; height: 50px; overflow: scroll;';
  document.body.appendChild(scrollDiv);

  const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);

  return scrollbarWidth;
};

const setScrollbar = ({bodyClassName}: ScrollBarProps) => {
  if (bodyIsOverflowing()) {
    document.body.style.paddingRight = `${getScrollbarWidth()}px`;
  }
  addClassNameToDOMNode(document.body, bodyClassName);
};

const resetScrollbar = ({bodyClassName}: ScrollBarProps) => {
  document.body.style.paddingRight = '';
  removeClassNameFromDOMNode(document.body, bodyClassName);
};

export const TourGuide: FC<PropsWithChildren<TourGuideProps>> & {
  Item: FC<ItemProps>;
  ResetItem: FC<ResetItemProps>;
} = ({children, tour, open = false, steps, onClose}) => {
  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);
  const {element} = bemFactory(baseClassName, classNames);
  const [state, dispatch] = useReducer(
    TourGuideReducer,
    getInitialState(steps)
  );
  const currentStepIndex = selectors.getCurrentStepIndex(state);
  const zIndex = selectors.getZIndex(state);
  const tourSteps = selectors.getTourSteps(state);
  const visibleSteps = selectors.getVisibleSteps(state);
  const validSteps = useMemo(
    () => getValidSteps(tourSteps, visibleSteps),
    [tourSteps, visibleSteps]
  );

  const onGoToNext = useCallback(() => {
    dispatch({type: ActionType.Increment});
  }, [dispatch]);

  const onGoToPrevious = useCallback(() => {
    dispatch({type: ActionType.Decrement});
  }, [dispatch]);

  const onItemRendered = useCallback(
    (stepName) => {
      dispatch({type: ActionType.UpdateItemVisibility, payload: {stepName}});
    },
    [dispatch]
  );

  // This effect needs to be a singleton and so has to be handled in the parent here.
  const bodyClassName = element('root-body');
  const prevOpen = useRef<boolean>(open);
  useEffect(() => {
    if (open) {
      setScrollbar({bodyClassName});
    }

    if (prevOpen.current && !open) {
      dispatch({
        type: ActionType.Close,
      });
    }

    prevOpen.current = open;

    return () => {
      if (open) {
        resetScrollbar({bodyClassName});
      }
    };
  }, [open, bodyClassName]);

  const zIndexStack = useZIndexStack();
  useEffect(() => {
    if (open) {
      const zIndex = zIndexStack.obtain();
      dispatch({
        type: ActionType.UpdateZIndex,
        payload: {
          zIndex,
        },
      });

      return () => {
        zIndexStack.release(zIndex);
        dispatch({
          type: ActionType.UpdateZIndex,
          payload: {
            zIndex: null,
          },
        });
      };
    }
  }, [open, zIndexStack]);

  const contextValue = useMemo(
    () => ({
      onGoToNext,
      onGoToPrevious,
      onItemRendered,
      onClose,
      tour,
      open,
      steps: validSteps,
      currentStepIndex,
      backdropZIndex: zIndex || 0,
      popoverZIndex: (zIndex || 0) + 1,
    }),
    [
      onGoToNext,
      onGoToPrevious,
      onItemRendered,
      onClose,
      tour,
      open,
      validSteps,
      currentStepIndex,
      zIndex,
    ]
  );

  return (
    <TourGuideContext.Provider value={contextValue}>
      {children}
    </TourGuideContext.Provider>
  );
};

TourGuide.Item = Item;
TourGuide.ResetItem = ResetItem;

export default TourGuide;
