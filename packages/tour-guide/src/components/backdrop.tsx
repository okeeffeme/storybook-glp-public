import React, {useEffect, FC, MutableRefObject} from 'react';
import TransitionPortal from '@jotunheim/react-transition-portal';
import {useForceUpdate, useDebounceCallback} from '@jotunheim/react-utils';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import cn from 'classnames';

import componentThemes from '../themes';

const DEBOUNCE_TIMER = 100;

const DEFAULT_CUTOUT_POSITION = {x: 0, y: 0, width: 0, height: 0};

interface BackdropProps {
  open: boolean;
  step: string;
  zIndex: number;
  overlayRef?: MutableRefObject<HTMLElement | null>;
}

const Backdrop: FC<BackdropProps> = ({open, step, zIndex, overlayRef}) => {
  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);
  const forceUpdate = useForceUpdate();
  const debouncedForceUpdate = useDebounceCallback(
    forceUpdate,
    DEBOUNCE_TIMER,
    {leading: true, trailing: true}
  );
  const {element} = bemFactory({baseClassName, classNames});

  const {x, y, width, height} =
    overlayRef && overlayRef.current
      ? overlayRef.current.getBoundingClientRect()
      : DEFAULT_CUTOUT_POSITION;

  // We need to trigger a component refresh when window resizes so absolute positioned element stays in the correct position.
  useEffect(() => {
    const handleOnResize = () => debouncedForceUpdate();
    if (open) {
      window.addEventListener('resize', handleOnResize);
    } else {
      window.removeEventListener('resize', handleOnResize);
    }

    return () => window.removeEventListener('resize', handleOnResize);
  }, [debouncedForceUpdate, open]);

  const maskId = `${step}-backdrop-mask-id`;

  const transitionClassNames = {
    appear: element('backdrop', 'appear'),
    appearActive: element('backdrop', 'appear-active'),
    enter: element('backdrop', 'enter'),
    enterActive: element('backdrop', 'enter-active'),
    exit: element('backdrop', 'leave'),
    exitActive: element('backdrop', 'leave-active'),
  };

  return (
    <TransitionPortal open={open} transitionClassNames={transitionClassNames}>
      <svg
        className={cn(element('backdrop-container'))}
        width={'100%'}
        height={'100%'}
        style={{zIndex}}
        data-tour-guide-backdrop={step}>
        <mask id={maskId}>
          <rect
            className={cn(element('backdrop-mask-fill'))}
            width={'100%'}
            height={'100%'}
          />
          <rect
            className={cn(element('backdrop-mask-element'))}
            x={x}
            y={y}
            width={width}
            height={height}
          />
        </mask>
        <rect
          width={'100%'}
          height={'100%'}
          className={cn(element('backdrop-fill'))}
          mask={`url(#${maskId})`}
        />
      </svg>
    </TransitionPortal>
  );
};

export default Backdrop;
