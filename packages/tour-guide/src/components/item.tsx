import React, {useLayoutEffect, useRef, FC, ReactNode} from 'react';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import Popover, {Placement, TriggerElement} from '@jotunheim/react-popover';

import TourGuideContext from '../context';
import ItemContent from './item-content';
import Backdrop from './backdrop';

export interface ItemProps {
  children: TriggerElement;
  tour: string;
  step: string;
  placement: Placement;
  title?: string;
  content?: string | ReactNode;
  navigationNextText?: string;
  navigationPrevText?: string;
  abortText?: string;
}

const Item: FC<ItemProps> = ({
  step,
  tour,
  placement,
  children,
  title = '',
  content = '',
  navigationNextText = 'next',
  navigationPrevText = 'previous',
  abortText = 'done',
  ...rest
}) => {
  const {
    tour: contextTour,
    open,
    steps,
    currentStepIndex,
    popoverZIndex,
    backdropZIndex,
    onItemRendered,
  } = React.useContext(TourGuideContext);

  if (tour !== contextTour) {
    throw new Error(
      `Tour ${tour} in Item does not match tour ${contextTour} in context.`
    );
  }

  const overlayRef = useRef<HTMLElement | null>(null);
  const shouldDisplayItem = open && steps[currentStepIndex ?? 0] === step;

  // Callback when element is mounted to DOM
  useLayoutEffect(() => {
    onItemRendered(step);
  }, [onItemRendered, step]);

  return (
    <>
      <Backdrop
        zIndex={backdropZIndex}
        open={shouldDisplayItem}
        step={step}
        overlayRef={overlayRef}
      />
      <Popover
        data-testid="data-tour-guide-popover"
        ref={overlayRef}
        open={shouldDisplayItem}
        portalZIndex={popoverZIndex}
        content={
          <ItemContent
            title={title}
            navigationPrevText={navigationPrevText}
            navigationNextText={navigationNextText}
            abortText={abortText}
            content={content}
            data-testid={`tour-guide-${step}`}
            data-tour-guide-step={step}
          />
        }
        placement={placement}
        trigger={Popover.triggerTypes.none}
        closeOnOutsideClick={false}
        data-tour-guide-popover-is-active={shouldDisplayItem}
        {...extractDataAriaIdProps(rest)}>
        {children}
      </Popover>
    </>
  );
};

export default Item;
