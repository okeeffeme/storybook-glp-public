import React, {CSSProperties, FC, PropsWithChildren} from 'react';
import cn from 'classnames';
import {IconButton} from '@jotunheim/react-button';
import Icon, {close} from '@jotunheim/react-icons';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import componentThemes from '../themes';

interface HeaderProps {
  title?: string;
  style?: CSSProperties;
  className?: string;
  onClose?: () => void;
}

const Header: FC<PropsWithChildren<HeaderProps>> = ({
  title,
  className,
  style,
  onClose,
}) => {
  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);

  const {element} = bemFactory({baseClassName, classNames});
  const classes = cn(element('header'), className);

  return (
    <div className={classes} style={style} data-tour-guide-header>
      <span>{title}</span>
      {onClose && (
        <IconButton onClick={onClose}>
          <Icon
            path={close}
            data-button="close"
            data-testid="tour-guide-close"
          />
        </IconButton>
      )}
    </div>
  );
};

export default Header;
