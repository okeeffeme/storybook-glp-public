import React, {
  useState,
  useEffect,
  useContext,
  useRef,
  useCallback,
  FC,
  ReactNode,
} from 'react';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import cn from 'classnames';
import Popover, {Placement, TriggerElement} from '@jotunheim/react-popover';
import Button from '@jotunheim/react-button';

import Header from './header';
import TourGuideContext from '../context';

import componentThemes from '../themes';

const DISMISSED_TOURS_KEY = 'cf:tour-guide-dismissed';

export interface ResetItemProps {
  children: TriggerElement;
  tour: string;
  title?: string;
  content?: string | ReactNode;
  buttonText?: string;
  placement?: Placement;
}

const getDismissedTours = () => {
  const dismissed = window.localStorage.getItem(DISMISSED_TOURS_KEY);
  return dismissed ? JSON.parse(dismissed) : {};
};

const checkIsDismissed = (tour: string) => getDismissedTours()[tour] === true;

const storeIsDismissed = (tour: string) => {
  const dismissed = getDismissedTours();
  dismissed[tour] = true;
  window.localStorage.setItem(DISMISSED_TOURS_KEY, JSON.stringify(dismissed));
};

const ResetItem: FC<ResetItemProps> = ({
  tour,
  title = '',
  content = '',
  children,
  buttonText = 'Got it',
  placement = 'bottom-end',
  ...rest
}) => {
  const {open, tour: contextTour, popoverZIndex} = useContext(TourGuideContext);

  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);
  const {element} = bemFactory({baseClassName, classNames});

  if (tour !== contextTour) {
    throw new Error(
      `Tour ${tour} in ResetItem does not match tour ${contextTour} in context.`
    );
  }

  const prevOpen = useRef<boolean>(open);

  const [isDismissed, setIsDismissed] = useState(checkIsDismissed(tour));
  const [tourClosed, setTourClosed] = useState(false);

  const onDismiss = useCallback(() => {
    storeIsDismissed(tour);
    setIsDismissed(true);
  }, [tour, setIsDismissed]);

  useEffect(() => {
    if (prevOpen.current && !open) {
      // show reset when closing the tour
      setTourClosed(true);
    } else if (!prevOpen.current && open) {
      // hide reset when re-opening the tour
      setTourClosed(false);
    }
    prevOpen.current = open;
  }, [open]);

  const shouldDisplayReset = tourClosed && !isDismissed;

  return (
    <Popover
      open={shouldDisplayReset}
      portalZIndex={popoverZIndex}
      content={
        <div className={cn(element('reset-item'))}>
          <Header title={title} />
          <div className={cn(element('reset-content'))}>{content}</div>
          <div className={cn(element('reset-footer'))}>
            <Button
              appearance={Button.appearances.primarySuccess}
              onClick={onDismiss}
              data-button="dismiss"
              data-testid="dismiss-button">
              {buttonText}
            </Button>
          </div>
        </div>
      }
      trigger={Popover.triggerTypes.none}
      placement={placement}
      closeOnOutsideClick={false}
      data-tour-guide-popover-is-active={shouldDisplayReset}
      {...extractDataAriaIdProps(rest)}>
      {children}
    </Popover>
  );
};

export default ResetItem;
