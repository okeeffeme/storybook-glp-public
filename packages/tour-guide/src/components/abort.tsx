import React, {useContext, FC} from 'react';
import {Button} from '@jotunheim/react-button';

import TourGuideContext from '../context';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';

interface AbortProps {
  buttonText: string;
}

const Abort: FC<AbortProps> = ({buttonText, ...rest}) => {
  const {onClose} = useContext(TourGuideContext);

  return (
    <Button
      data-button="abort"
      appearance={Button.appearances.primarySuccess}
      onClick={onClose}
      {...extractDataAriaIdProps(rest)}>
      {buttonText}
    </Button>
  );
};

export default Abort;
