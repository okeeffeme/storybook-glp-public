import React, {FC} from 'react';
import {Button} from '@jotunheim/react-button';
import {bemFactory, useTheme} from '@jotunheim/react-themes';

import TourGuideContext from '../context';
import componentThemes from '../themes';

import Abort from './abort';

interface NavigationControlProps {
  className?: string;
  prevButtonText?: string;
  nextButtonText?: string;
  abortText?: string;
}

const NavigationControl: FC<NavigationControlProps> = ({
  prevButtonText,
  nextButtonText,
  abortText = '',
}) => {
  const {baseClassName, classNames} = useTheme('tourGuide', componentThemes);
  const {element} = bemFactory({baseClassName, classNames});
  const {onGoToNext, onGoToPrevious, steps, currentStepIndex} =
    React.useContext(TourGuideContext);

  const canGoToNext = currentStepIndex < steps.length - 1;
  const canGoToPrevious = currentStepIndex > 0;
  const isLastStep = currentStepIndex === steps.length - 1;

  return (
    <>
      {canGoToPrevious && (
        <span className={element('navigation-button')}>
          <Button
            data-testid="tour-guide-previous"
            data-button="previous"
            appearance={Button.appearances.secondaryNeutral}
            onClick={onGoToPrevious}>
            {prevButtonText}
          </Button>
        </span>
      )}
      {canGoToNext && (
        <span className={element('navigation-button')}>
          <Button
            data-testid="tour-guide-next"
            data-button="next"
            appearance={Button.appearances.primarySuccess}
            onClick={onGoToNext}>
            {nextButtonText}
          </Button>
        </span>
      )}
      {isLastStep && (
        <span className={element('navigation-button')}>
          <Abort buttonText={abortText} />
        </span>
      )}
    </>
  );
};

export default NavigationControl;
