import classNames from './css/tour-guide.module.css';

export default {
  tourGuide: {
    baseClassName: 'comd-tour-guide',
    classNames,
  },
};
