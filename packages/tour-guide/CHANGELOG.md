# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.9&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.10&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.9&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.9&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.7&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.8&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.6&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.7&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.5&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.6&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.4&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.5&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.3&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.4&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.2&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.3&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.1&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.2&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.1.0&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.1&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-tour-guide

# [6.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.35&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.1.0&targetRepoId=1246) (2023-02-10)

### Bug Fixes

- fixing ScrollWrapper components ([NPM-1206](https://jiraosl.firmglobal.com/browse/NPM-1206)) ([14350d3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/14350d3f18f68aa9fd6e9832a13d58cc337d8d73))

### Features

- convert tour-guide package to TypScript ([NPM-1245](https://jiraosl.firmglobal.com/browse/NPM-1245)) ([e068553](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e068553e9b167bc1057006bc36a7318a1aadb27b))

## [6.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.34&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.35&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.33&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.34&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.32&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.33&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.31&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.32&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.30&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.31&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.29&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.30&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.27&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.27&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.28&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.26&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.27&targetRepoId=1246) (2023-01-11)

### Bug Fixes

- add test id for Header component within tour-guide package ([NPM-1219](https://jiraosl.firmglobal.com/browse/NPM-1219)) ([7c7c707](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7c7c707206441e04dcb6de435df6ab94f70b689d))
- add test id for Item component within tour-guide package ([NPM-1219](https://jiraosl.firmglobal.com/browse/NPM-1219)) ([661aa8a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/661aa8a22a1de9ae7007b45132cdab412f387908))
- add test id for NavigationControl component within tour-guide package ([NPM-1219](https://jiraosl.firmglobal.com/browse/NPM-1219)) ([7c3ccb3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7c3ccb35709c4f4af4913b6e596450734e0b6217))
- add test id for ResetItem component within tour-guide package ([NPM-1219](https://jiraosl.firmglobal.com/browse/NPM-1219)) ([8b370d0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8b370d0935470ad6e1dc3f19b43ddf6519d32244))
- migrate tests from Enzyme to RTL for tour-guide package ([NPM-1219](https://jiraosl.firmglobal.com/browse/NPM-1219)) ([07ac446](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/07ac44690f6d32fa7405714b40f059a2987792fb))

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.25&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.26&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.24&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.25&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.23&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.24&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.21&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.23&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.21&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.18&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.18&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.18&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.17&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.16&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.14&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.13&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.14&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.12&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.11&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.10&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.9&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.8&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.6&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.3&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.2&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.1&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-tour-guide

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-tour-guide@6.0.0&sourceBranch=refs/tags/@jotunheim/react-tour-guide@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-tour-guide

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.77&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.78&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.76&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.77&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.75&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.76&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.74&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.75&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.73&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.74&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.71&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.72&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.70&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.71&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.69&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.70&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.68&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.69&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.67&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.68&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.67](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.66&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.67&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.63&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.64&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.62&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.63&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.61&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.62&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.60&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.61&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.59&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.60&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.58&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.59&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.57&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.58&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.56&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.57&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.54&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.55&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.53&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.54&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.52&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.53&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.51&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.52&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.50&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.51&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.49&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.50&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.48&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.49&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.47&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.48&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.46&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.47&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.45&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.46&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.44&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.45&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.43&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.44&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.42&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.43&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.41&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.42&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.40&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.41&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.39&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.40&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.38&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.39&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.37&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.38&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.36&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.37&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.35&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.36&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.34&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.35&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.33&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.34&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.31&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.32&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.30&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.31&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.29&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.30&targetRepoId=1246) (2021-04-12)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.28&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.29&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.27&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.28&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.26&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.27&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.25&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.26&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.24&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.25&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.23&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.24&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.22&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.23&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.21&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.22&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.19&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.20&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.18&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.19&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.17&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.18&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.16&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.17&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.15&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.16&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.14&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.13&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.12&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.11&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.10&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.9&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.7&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.4&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.3&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@5.0.2&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-tour-guide

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.24&sourceBranch=refs/tags/@confirmit/react-tour-guide@5.0.0&targetRepoId=1246) (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- confirmit/react-z-index peer is no more required ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([1c6b3d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1c6b3d84112c9ecd50d485adb1ba087e8b97001c))

### BREAKING CHANGES

- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.
- @confirmit/react-contexts is new peer dependency

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.23&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.24&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.22&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.23&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.21&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.22&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.18&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.19&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.17&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.18&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.16&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.17&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.13&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.14&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.11&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.12&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-tour-guide@4.0.9&sourceBranch=refs/tags/@confirmit/react-tour-guide@4.0.10&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.8](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tour-guide@4.0.7...@confirmit/react-tour-guide@4.0.8) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tour-guide@4.0.5...@confirmit/react-tour-guide@4.0.6) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-tour-guide

## [4.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tour-guide@4.0.0...@confirmit/react-tour-guide@4.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-tour-guide

# [4.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-tour-guide@3.0.15...@confirmit/react-tour-guide@4.0.0) (2020-08-12)

### Features

- update peer dependency ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([af67759](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/af6775973192059412df38fb295f699afad1f55e))

### BREAKING CHANGES

- peerDependency `@confirmit/react-transition-portal": "^1.3.0"` is required

## CHANGELOG

## v3.0.2

- Fix: added missing margin between navigation buttons

## v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-tour-guide`

### v2.1.0

- Removing package version in class names.

### v2.0.8

- Refactor: Change from `type` to `appearance` for button

### v2.0.3

- Fix:
  - added missing zIndex in popover and backdrop

### v2.0.0

- **BREAKING**:

  - Remove prop "navigateToLastOnClose", this is now solely controlled by ResetItem
  - TourGuide.Item: removed props showNavigationControl and showAbort

- Feat:

  - New ResetItem introduced, to be used to show where to potentially restart the guide from
  - Next/Done buttons are now primary, and no longer change locations

- Fix:
  - styles for scrollbar adjustment are now properly applied
  - scrollbar adjustment now works correctly when guide is open on initial render

### v1.0.0

- **BREAKING**:
  - Require a new peerDependency `@confirmit/react-transition-portal`
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.1

- Initial version
