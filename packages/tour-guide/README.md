# Jotunheim React Tour Guide

## Usage

```js
<TourGuide tour="tourName" steps={['step1', 'step2', 'step3']} open={true}>
  <TourGuide.Item
    tour="tourName"
    step="step1"
    title="title of step 1"
    placement="end"
    content={<div>content 1</div>}>
    <div>button 1</div>
  </TourGuide.Item>
  <TourGuide.Item
    tour="tourName"
    step="step2"
    title="title of step 2"
    placement="bottom-end"
    content={<div>content 2</div>}>
    <div>button 2</div>
  </TourGuide.Item>
  <TourGuide.Item
    tour="tourName"
    step="step3"
    title="title of step 3"
    placement="start"
    content={<div>content 3</div>}>
    <div>button 3</div>
  </TourGuide.Item>
</TourGuide>
```

For backdrop cutout to properly render, the child ref needs to be exposed. If child ref is missing, a full screen backdrop is rendered instead.
