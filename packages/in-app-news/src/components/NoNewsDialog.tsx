import React from 'react';

import {Dialog} from '@jotunheim/react-dialog';
import {Button} from '@jotunheim/react-button';

export const NoNewsMessage = ({
  isOpen,
  onClose = () => {},
  noNewsMessage,
  closeButtonText,
}: NoNewsMessageProps) => (
  <Dialog open={isOpen} size="large" onHide={onClose}>
    <Dialog.Header
      onCloseButtonClick={onClose}
      title={noNewsMessage}
      data-testid="no-news-dialog-header"
    />
    <Dialog.Body />
    <Dialog.Footer>
      <Button data-close-button="" onClick={onClose}>
        {closeButtonText}
      </Button>
    </Dialog.Footer>
  </Dialog>
);

type NoNewsMessageProps = {
  isOpen: boolean;
  onClose?: () => void;
  noNewsMessage: string;
  closeButtonText: string;
};
