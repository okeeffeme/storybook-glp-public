import React, {AriaAttributes, useCallback, useEffect, useState} from 'react';

import Pager from '@jotunheim/react-pager';
import {Dialog} from '@jotunheim/react-dialog';
import {Carousel} from '@jotunheim/react-carousel';
import {Button} from '@jotunheim/react-button';
import {BusyDots} from '@jotunheim/react-busy-dots';
import {arePropsEqual, extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {NoNewsMessage} from './NoNewsDialog';

import classNames from './InAppNews.module.css';

const {element} = bemFactory('comd-in-app-news', classNames);

function InAppNews({
  loadingText = 'Loading',
  onActiveNewsShown,
  news = [],
  locale = 'en-US',
  isLoading = false,
  noNewsMessage = 'No news available',
  closeButtonText = 'Got it',
  onClose,
  isOpen,
  ...rest
}: InAppNewsProps) {
  const [activePageIndex, setActivePageIndex] = useState(0);

  const handlePageChange = useCallback((page) => {
    setActivePageIndex(page - 1);
  }, []);

  const formatDate = (date) => {
    return new Date(date).toLocaleString(locale, {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
  };

  useEffect(() => {
    onActiveNewsShown?.(news[activePageIndex]);
  }, [news, activePageIndex, onActiveNewsShown]);

  const shouldShowNoNewsMessage = news.length === 0 && isLoading === false;

  if (shouldShowNoNewsMessage) {
    return (
      <NoNewsMessage
        noNewsMessage={noNewsMessage}
        isOpen={isOpen}
        onClose={onClose}
        closeButtonText={closeButtonText}
      />
    );
  }

  return (
    <Dialog
      data-testid="in-app-news"
      open={isOpen}
      keyboard={true}
      onHide={onClose}
      size="large"
      {...extractDataAriaIdProps(rest)}>
      <Dialog.Header
        onCloseButtonClick={onClose}
        title={
          isLoading ? (
            <span>{loadingText}</span>
          ) : (
            <div className={element('title-date')}>
              <span
                className={element('title')}
                dangerouslySetInnerHTML={{
                  __html: news[activePageIndex].title,
                }}
              />
              <time className={element('date')} data-testid="inappnews-date">
                ({formatDate(news[activePageIndex].date)})
              </time>
            </div>
          )
        }
      />
      <Dialog.Body hasPadding={false}>
        <div className={element('body')}>
          {isLoading ? (
            <div style={{textAlign: 'center', paddingTop: '50px'}}>
              <BusyDots />
            </div>
          ) : (
            <Carousel
              showPagination={false}
              activeIndex={activePageIndex}
              onActiveIndexChanged={setActivePageIndex}>
              {news.map(({id, ingress, body}) => {
                return (
                  <div
                    data-carousel-item=""
                    key={id}
                    data-testid="inappnews-carousel-item">
                    <div
                      className={element('content-ingress')}
                      dangerouslySetInnerHTML={{__html: ingress}}
                    />
                    <div dangerouslySetInnerHTML={{__html: body}} />
                  </div>
                );
              })}
            </Carousel>
          )}
        </div>
      </Dialog.Body>
      <Dialog.Footer>
        <div className={element('footer')}>
          {news.length > 1 && (
            <Pager
              page={activePageIndex + 1}
              pageSize={1}
              totalCount={news.length}
              onPageChanged={handlePageChange}
              data-testid="inappnews-pager"
            />
          )}
          <div className={element('close-btn')}>
            <Button
              data-close-button
              onClick={onClose}
              data-testid="close-button">
              {closeButtonText}
            </Button>
          </div>
        </div>
      </Dialog.Footer>
    </Dialog>
  );
}

export type News = {
  id: number;
  title: string;
  ingress: string;
  body: string;
  date: string;
  isRead?: number;
};

export type InAppNewsProps = {
  news?: News[];
  isOpen: boolean;
  isLoading?: boolean;
  noNewsMessage?: string;
  loadingText?: string;
  closeButtonText?: string;
  locale?: string;
  onClose?: () => void;
  onActiveNewsShown?: (news: News) => void;
} & AriaAttributes;

const InAppNewsMemo = React.memo(
  InAppNews,
  arePropsEqual(['onActiveNewsShown', 'onClose'])
);

export {InAppNewsMemo as InAppNews};
