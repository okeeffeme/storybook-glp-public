import {InAppNews, News, InAppNewsProps} from './components/InAppNews';

export {InAppNews};
export type {News, InAppNewsProps};

export default InAppNews;
