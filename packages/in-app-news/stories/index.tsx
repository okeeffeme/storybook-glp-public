/* eslint-disable no-console */

import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {Button} from '../../button/src';
import {InAppNews} from '../src/components/InAppNews';

import {news} from './news';

storiesOf('Components/InAppNews', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Multiple News', () => {
    const [isOpen, setIsOpen] = React.useState(true);

    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <InAppNews
          news={news.multipleNews}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
          onActiveNewsShown={(newsItem) => {
            console.log(newsItem); // get ID and mark as read
          }}
        />
      </div>
    );
  })
  .add('Norwegian formatted date', () => {
    const [isOpen, setIsOpen] = React.useState(true);

    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <InAppNews
          locale="nb-NO"
          news={news.multipleNews}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
        />
      </div>
    );
  })
  .add('Single News', () => {
    const [isOpen, setIsOpen] = React.useState(true);

    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <InAppNews
          news={news.singleNews}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
        />
      </div>
    );
  })
  .add('No News', () => {
    const [isOpen, setIsOpen] = React.useState(true);

    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <InAppNews
          news={[]}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
        />
      </div>
    );
  })
  .add('Loading News', () => {
    const [isOpen, setIsOpen] = React.useState(true);

    return (
      <div>
        <Button onClick={() => setIsOpen(true)}>Open</Button>
        <InAppNews
          news={[]}
          isLoading={true}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
        />
      </div>
    );
  });
