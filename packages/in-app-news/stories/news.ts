export const news = {
  multipleNews: [
    {
      id: 1,
      title:
        'Here is your <b>first news</b> title and this title is very very very very very very very very very very very very very very very very very very long',
      ingress: '<u>This is an ingress</u>',
      body: '<p>Far far away, behind the word mountains, far from the countries <b>Vokalia</b> and <b>Consonantia</b>, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>',
      date: '2018-11-21T16:25:29.143Z',
      isRead: 1,
    },
    {
      id: 20,
      title: 'Dummy title',
      ingress:
        '<i>I should be incapable of drawing a single stroke at the present moment</i>',
      body: '<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p><p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p><p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p><p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p><p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p><p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.</p>',
      date: '2018-11-09T16:25:29.143Z',
      isRead: 0,
    },
    {
      id: 301,
      title: 'Last news title',
      ingress:
        'Long title. The European languages are members of the same family. Their separate existence is a myth.',
      body: '<p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p>',
      date: '2018-11-05T16:25:29.143Z',
      isRead: 0,
    },
    {
      id: 400,
      title: 'With huge image',
      ingress:
        'INGRESS:Long title. The European languages are members of the same family. Their separate existence is a myth. END INGRESS',
      body: '<p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p><p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be</p><img alt="" src="https://placekitten.com/2560/1440/" />',
      date: '2018-09-01T16:25:29.143Z',
      isRead: 1,
    },
  ],
  singleNews: [
    {
      id: 900,
      title: 'There is only one news for you this time',
      ingress: 'Navigation and pagination <b>should not be visible</b>',
      body: '<p>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps.</p><p>Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz.</p><p>Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim.</p>',
      date: '2018-11-20T16:25:29.143Z',
      isRead: 0,
    },
  ],
};
