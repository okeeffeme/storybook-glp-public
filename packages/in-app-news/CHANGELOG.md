# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [13.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.42&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.43&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.42&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.42&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.40&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.41&targetRepoId=1246) (2023-04-03)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.39&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.40&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.38&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.39&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.37&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.38&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.36&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.37&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.35&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.36&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.34&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.35&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.33&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.34&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [13.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.32&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.33&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.31&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.32&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.30&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.31&targetRepoId=1246) (2023-02-20)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.28&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.30&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.28&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.29&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.27&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.28&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.26&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.27&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.25&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.26&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.24&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.25&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.23&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.24&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.22&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.23&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.21&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.22&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.20&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.21&targetRepoId=1246) (2023-01-27)

### Bug Fixes

- adding data-testids to In-app-news components ([NPM-1195](https://jiraosl.firmglobal.com/browse/NPM-1195)) ([b03875e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b03875e0cfcc74fa2100e79987f6d9adaf90d764))
- renaming data-testids in In-app-news components ([NPM-1195](https://jiraosl.firmglobal.com/browse/NPM-1195)) ([6d0c554](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6d0c554534846d21809e594f95f130adb7a0f6c3))

## [13.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.19&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.20&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.18&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.19&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.16&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.18&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.16&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.15&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.16&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.14&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.15&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.13&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.14&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.12&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.13&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.11&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.12&targetRepoId=1246) (2022-12-28)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.10&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.8&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.8&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.9&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.5&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.8&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.5&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.7&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.5&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.6&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.4&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.5&targetRepoId=1246) (2022-11-23)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.3&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.2&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [13.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@13.0.0&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- remove unused properties from Dialog ([NPM-1096](https://jiraosl.firmglobal.com/browse/NPM-1096)) ([f0fef72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f0fef722adc9090d1c583ede5a62de9989aad4af))

# [13.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.13&sourceBranch=refs/tags/@jotunheim/react-in-app-news@13.0.0&targetRepoId=1246) (2022-10-11)

### Features

- convert InAppNews to TypeScript ([NPM-1093](https://jiraosl.firmglobal.com/browse/NPM-1093)) ([d157837](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d157837055cf7898a0e0a4dc12bac57a5b13eee0))
- remove className prop of InAppNews ([NPM-940](https://jiraosl.firmglobal.com/browse/NPM-940)) ([5d9fb52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5d9fb52ee0e8b68f139b7eba7fa22a1e2729eaa7))
- remove default theme from InAppNews ([NPM-1072](https://jiraosl.firmglobal.com/browse/NPM-1072)) ([bd0c074](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bd0c0747677bb098c59509dd8a265cd03c9fabbf))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support

feat: remove default theme from InAppNews (NPM-1062)

- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [12.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.12&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.11&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.10&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.9&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.8&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.6&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.3&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.2&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.1&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-in-app-news

## [12.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-in-app-news@12.0.0&sourceBranch=refs/tags/@jotunheim/react-in-app-news@12.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-in-app-news

# 12.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [11.0.92](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.91&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.92&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.91](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.90&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.91&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.90](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.89&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.90&targetRepoId=1246) (2022-06-21)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.88&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.89&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [11.0.88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.87&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.88&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.87](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.86&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.87&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.84&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.85&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.84](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.83&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.84&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.83](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.82&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.83&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.82](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.81&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.82&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.81](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.80&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.81&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.80](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.79&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.80&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.76](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.75&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.76&targetRepoId=1246) (2022-02-03)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.75](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.74&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.75&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.73&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.74&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.72&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.73&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.71&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.72&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.70&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.71&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.69&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.70&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.68&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.69&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [11.0.68](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.67&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.68&targetRepoId=1246) (2021-10-20)

### Bug Fixes

- replace @confirmit/react-modal with @confirmit/react-dialog because of package renaming ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([cb47cac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cb47cac4cebcc454b6e83b9d461f168b5edf5311))

## [11.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.65&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.66&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.64&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.65&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.63&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.64&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.62&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.63&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.61&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.62&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.60&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.61&targetRepoId=1246) (2021-09-08)

### Bug Fixes

- stretch body content to 100% height when modal changes to 100% width or 100% height ([NPM-625](https://jiraosl.firmglobal.com/browse/NPM-625)) ([42af731](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/42af7319684c8e268687f924ab8d9dec365dcead))

## [11.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.59&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.60&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.58&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.59&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.57&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.58&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.56&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.57&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.55&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.56&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.54&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.55&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.53&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.54&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.52&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.53&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.51&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.52&targetRepoId=1246) (2021-07-07)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.50&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.51&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.49&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.50&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.48&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.49&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.47&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.48&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.46&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.47&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.45&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.46&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.44&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.45&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.43&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.44&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.42&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.43&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.41&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.42&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.40&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.41&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.39&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.40&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.37&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.38&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.36&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.37&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.35&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.36&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [11.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.34&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.35&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.33&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.34&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.32&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.33&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.31&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.32&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.30&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.31&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.29&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.30&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.28&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.29&targetRepoId=1246) (2021-03-10)

### Bug Fixes

- adjust title and date in in-app-news dialog after modal updates ([NPM-632](https://jiraosl.firmglobal.com/browse/NPM-632)) ([ec826a8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec826a86549bd0416c97bb784505ad8c8059ee20))

## [11.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.27&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.28&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.26&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.27&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.25&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.23&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.24&targetRepoId=1246) (2021-02-10)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.22&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.23&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.21&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.22&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.20&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.21&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.19&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.20&targetRepoId=1246) (2021-01-20)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.18&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.16&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.15&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.14&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.15&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.13&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.12&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.11&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.12&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.10&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.11&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.7&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.8&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.4&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.5&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.3&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.4&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [11.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@11.0.2&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.3&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-in-app-news

# [11.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.48&sourceBranch=refs/tags/@confirmit/react-in-app-news@11.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [10.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.47&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.48&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.46&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.47&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.45&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.46&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.42&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.43&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.41&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.42&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.40&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.41&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.37&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.38&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.35&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.36&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-in-app-news@10.0.33&sourceBranch=refs/tags/@confirmit/react-in-app-news@10.0.34&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.32](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-in-app-news@10.0.31...@confirmit/react-in-app-news@10.0.32) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.30](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-in-app-news@10.0.29...@confirmit/react-in-app-news@10.0.30) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.26](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-in-app-news@10.0.25...@confirmit/react-in-app-news@10.0.26) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-in-app-news@10.0.21...@confirmit/react-in-app-news@10.0.22) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-in-app-news

## [10.0.21](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-in-app-news@10.0.20...@confirmit/react-in-app-news@10.0.21) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-in-app-news

## CHANGELOG

## v10.0.17

- Fix: Always showed a scrollbar after upgrades to modal to handle scrolling automatically. In-app-news has a separate scroll body because of how the carousel is created.

## v10.0.14

- Fix: Remove required requirement for `isRead` prop

## v10.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-in-app-news`

### v9.1.0

- Removing package version in class names.

### v9.0.2

- Fix: position of close button was too close to edge

### v9.0.0

- No external changes

### v8.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v7.0.0

- **BREAKING**
  - Requires confirmit-themes 3.1 or newer

### v6.0.0

- _missing changelog_

### v5.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v4.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v3.0.0

- **BREAKING** Theming support, changed to default module export instead of named `{ConfirmitInAppNews}` export
- **BREAKING** `data` prop renamed to `news`
- **BREAKING** `open` prop renamed to `isOpen`
- **BREAKING** `title` prop removed - title is set internally.
- **BREAKING** shape of objects in `news` array has been updated to support new props `isRead` and `date`, and all props are required.
- **BREAKING** `onActiveNewsShown` signature changed - will now only return the current item shown, instead of the entire `news` array and the index of the current item shown.
- Feat: Show date of news item in header
- Feat: New props:
  - `isLoading`: bool - set to true when fetching items from server, and back to false when done loading
  - `loadingText`: string - text to appear in header of modal if user tries to open modal while `isLoading === true`
  - `noNewsMessage`: string - text to appear in header of modal if user opens modal but there are no news available
  - `locale`: string - which locale to use when formatting the date in the header (f.ex `en-US`, or `nb-NO`) etc. Defaults to `en-US`

### v2.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only

### v1.0.0

- precompiled css is used in components (might be a breaking change for webpack scss and css loaders)
