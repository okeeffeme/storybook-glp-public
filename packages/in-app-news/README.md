# Jotunheim React In App News

## Usage

### News

To enable in-app-news overlay, make a request to Confirmit.News.Service f.ex like so `${server}/api/Confirmit.News.Service/inappnews?applications=SurveyDesigner` and pass the results array to the `news` prop.

To get already read and expired news items, you can do a request to Confirmit.News.Service with the parameters `includeRead` and `includeExpired` - f.ex: `${server}/api/Confirmit.News.Service/inappnews?applications=SurveyDesigner&includeRead=true&includeExpired=true`. This can be done when the user clicks the News dropdown in `onOpenNews` in confirmit-app-header component (available after v6.0.0). This way the app doesnt have to initially load all news.

The app needs to ensure that items are marked as read, this can be done on the `onActiveNewsShown` callback. This will include the current read item, and here you can check for `isRead === 0`, and do a post to Confirmit.News.Service with the read news `id` value, f.ex `${server}/api/Confirmit.News.Service/inappnews/${id}/read`,`
