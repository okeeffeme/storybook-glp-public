import React from 'react';
import {render, screen} from '@testing-library/react';
import {InAppNews} from '../../src/components/InAppNews';
import userEvent from '@testing-library/user-event';

const data = [
  {
    id: 101,
    title: '<b>News title</b>',
    ingress: '<u>News ingress</u>',
    body: '<p>News body</p>foo',
    isRead: 0,
    date: '2018-11-09T16:25:29.143Z',
  },
  {
    id: 202,
    title: 'Title 2',
    ingress: 'Ingress 2',
    body: 'Body 2',
    isRead: 1,
    date: '2018-11-09T16:25:29.143Z',
  },
  {
    id: 303,
    title: 'Title 3',
    ingress: 'Ingress 3',
    body: 'Body 3',
    isRead: 0,
    date: '2018-11-09T16:25:29.143Z',
  },
];

describe('InAppNews', () => {
  it('should render with multiple news', () => {
    render(<InAppNews news={data} isOpen={true} />);
    expect(
      screen.queryByTestId('no-news-dialog-header')
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('busy-dots')).not.toBeInTheDocument();
    expect(screen.getByTestId('close-button')).toHaveAttribute(
      'data-close-button',
      'true'
    );
    expect(screen.getAllByTestId('inappnews-carousel-item')).toHaveLength(3);
    expect(screen.getByTestId('inappnews-pager')).toBeInTheDocument();
  });

  it('should render with one news item', () => {
    render(<InAppNews news={[data[0]]} isOpen={true} />);

    expect(
      screen.queryByTestId('no-news-dialog-header')
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('busy-dots')).not.toBeInTheDocument();
    expect(screen.getAllByTestId('inappnews-carousel-item')).toHaveLength(1);
    expect(screen.queryAllByTestId('inappnews-pager')).toHaveLength(0);
  });

  it('should return the current news item in callback onActiveSlideShown when in-app-news dialog is displayed', (done) => {
    const handler = (newsItem) => {
      expect(newsItem).toEqual(data[0]);
      done();
    };

    render(<InAppNews news={data} isOpen={true} onActiveNewsShown={handler} />);
  });

  it('should call onClose handler when close button is clicked', (done) => {
    const handler = () => {
      done();
    };

    render(<InAppNews news={data} isOpen={true} onClose={handler} />);
    userEvent.click(screen.getByTestId('close-button'));
  });

  it('should show no news message if there are no news', () => {
    render(<InAppNews news={[]} isOpen={true} />);

    expect(screen.getByTestId('no-news-dialog-header')).toHaveTextContent(
      'No news available'
    );
    expect(screen.queryByTestId('busy-dots')).not.toBeInTheDocument();
  });

  it('should show loading when news are loading', () => {
    render(<InAppNews news={[]} isLoading={true} isOpen={true} />);

    expect(
      screen.queryByTestId('no-news-dialog-header')
    ).not.toBeInTheDocument();
    expect(screen.queryByTestId('busy-dots')).toBeInTheDocument();
  });
  it('should correctly format dates with default locale', () => {
    render(<InAppNews news={[data[1]]} isOpen={true} />);

    expect(screen.getByTestId('inappnews-date')).toHaveTextContent(
      'November 9, 2018'
    );
  });
});
