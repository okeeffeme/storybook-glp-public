# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.1.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.46&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.47&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.46&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.46&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.44&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.45&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.43&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.44&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.42&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.43&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.41&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.42&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.40&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.41&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.39&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.40&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [4.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.38&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.39&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.37&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.38&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.36&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.37&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- InputWrapper background out of bounds ([NPM-1248](https://jiraosl.firmglobal.com/browse/NPM-1248)) ([3142885](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/314288566db998d15b409779561e56d2fbd68de8))

## [4.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.35&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.36&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.34&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.35&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.33&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.34&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.32&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.33&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.31&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.32&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.30&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.31&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.28&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.30&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.28&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.27&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.28&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.26&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.25&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.24&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.23&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.22&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.23&targetRepoId=1246) (2022-12-20)

### Bug Fixes

- Extract from InputWrapper into variables, using to set min-height on TextArea, setting height to auto ([HUB-9719](https://jiraosl.firmglobal.com/browse/HUB-9719)) ([00d047a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/00d047a25b04b7e82ce8f9b5f9465090d41e69d1))

## [4.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.21&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.18&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.18&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.18&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.17&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.16&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.14&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.13&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.14&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- add data-testid attribute to clear button ([NPM-1091](https://jiraosl.firmglobal.com/browse/NPM-1091)) ([efb3466](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/efb34662833452bb82903a54807bea0966bff833))

## [4.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.12&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.11&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.10&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.11&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.9&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.10&targetRepoId=1246) (2022-09-13)

### Bug Fixes

- revert label wrapper to div as brakes MultiSelect ([NPM-819](https://jiraosl.firmglobal.com/browse/NPM-819)) ([489285e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/489285e1e2e06db683a9f586f2a71647cb3f87d1))

## [4.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.8&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.9&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- focus input if prefix or suffix components are clicked ([NPM-819](https://jiraosl.firmglobal.com/browse/NPM-819)) ([b71f188](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b71f188afc569ed7f1926d0e105033d43cd26bb3))

## [4.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.7&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.8&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- focus input if prefix or suffix components are clicked ([NPM-819](https://jiraosl.firmglobal.com/browse/NPM-819)) ([ccc57d8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ccc57d8665f0357d4680cb8526c29a6ce3d11121))

## [4.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.6&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.7&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.5&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.6&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.3&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.4&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.0&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.1&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.3&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.1.0&targetRepoId=1246) (2022-08-09)

### Features

- pass id of TextField to InputWrapper as htmlFor to be used in label ([NPM-1048](https://jiraosl.firmglobal.com/browse/NPM-1048)) ([3f1ddf5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3f1ddf514f5fd237979b8817fef894476504496b))

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.2&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.1&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.0&sourceBranch=refs/tags/@jotunheim/react-input-wrapper@4.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-input-wrapper

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.3.2&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.3.3&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.3.1&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.3.2&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.3.0&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.3.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [3.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.22&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.3.0&targetRepoId=1246) (2022-05-20)

### Features

- adding css variables to InputWrapper for background color and input text color ([NPM-1005](https://jiraosl.firmglobal.com/browse/NPM-1005)) ([2fab9c5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2fab9c5062ac8d4d0c54006d7f7e3c05c2fd8d08))

## [3.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.21&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.22&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.20&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.21&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.18&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.19&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.17&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.18&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.16&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.17&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.15&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.16&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.14&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.15&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.13&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.14&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.10&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.11&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.9&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.10&targetRepoId=1246) (2022-01-18)

### Bug Fixes

- Make sure default browser cursor takes effect on disabled inputs ([NPM-889](https://jiraosl.firmglobal.com/browse/NPM-889)) ([9a35f13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9a35f13c4c3732e10f34c6b75789646a25a38065))

## [3.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.8&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.9&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.7&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.8&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.6&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.7&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.5&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.6&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.4&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.5&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.3&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.4&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [3.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.2&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.3&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.2.0&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.1&targetRepoId=1246) (2021-09-30)

### Bug Fixes

- search field should have a gray background (input-wrapper "transparentBackground" prop doesn't work as intended) ([NPM-877](https://jiraosl.firmglobal.com/browse/NPM-877)) ([32b9e8b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/32b9e8b77eac5f598de88a0256ecaaaaee2df5ba))

# [3.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.39&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.2.0&targetRepoId=1246) (2021-09-16)

### Features

- support LayoutProvider for setting width of inputs ([NPM-571](https://jiraosl.firmglobal.com/browse/NPM-571)) ([865b641](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/865b641df37eee06235ec1f397adf513e4ffec7e))

## [3.1.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.38&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.39&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.37&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.38&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.36&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.37&targetRepoId=1246) (2021-09-10)

### Bug Fixes

- add border radius to input wrapper, so it shares the border radius with the inner components, to make sure it looks correct on colored backgrounds ([NPM-467](https://jiraosl.firmglobal.com/browse/NPM-467)) ([819b0ae](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/819b0ae1f32c472a2138c0308721675b9dea8a75))
- add gradient background to avoid 1px white border above inputfields which will be visible when using a non-white background ([NPM-467](https://jiraosl.firmglobal.com/browse/NPM-467)) ([46fa009](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/46fa0091b44263d68d7b468bccc7402fc4707eda))
- adjust top offset of border position, so entire border is visible when focused, and in a parent with overflow: hidden ([NPM-467](https://jiraosl.firmglobal.com/browse/NPM-467)) ([8d41527](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8d41527eacbafcf92aea7ae8a27b55e6aec0fd07))

## [3.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.35&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.36&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.34&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.35&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.33&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.34&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.32&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.33&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.31&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.32&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.30&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.31&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.29&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.30&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.28&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.29&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.27&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.28&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.26&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.27&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.25&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.26&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.24&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.25&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.23&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.24&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.22&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.23&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.21&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.22&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.20&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.21&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.19&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.20&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.18&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.17&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.18&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.15&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.16&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.14&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.15&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.13&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.14&targetRepoId=1246) (2021-04-14)

### Bug Fixes

- remove Title Case on labels and titles ([NPM-761](https://jiraosl.firmglobal.com/browse/NPM-761)) ([0213c34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0213c343d273533335a3a64b1c736f4b3823e2d1))

## [3.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.12&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.13&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [3.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.11&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.12&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.10&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.11&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.9&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.10&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.8&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.9&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.7&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.8&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.6&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.7&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.5&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.3&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.4&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.2&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.3&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.1&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.2&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.1.0&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.1&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-input-wrapper

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.17&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.1.0&targetRepoId=1246) (2021-01-18)

### Features

- show error message when exceeding max length ([NPM-573](https://jiraosl.firmglobal.com/browse/NPM-573)) ([092197c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/092197c828bba3eea084d9616674d27a6110c556))

## [3.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.16&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.15&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.16&targetRepoId=1246) (2020-12-11)

### Bug Fixes

- textfield, textarea, colorpicker, datepicker, select, etc. now correctly uses text-transform: capitalize on labels ([NPM-653](https://jiraosl.firmglobal.com/browse/NPM-653)) ([49f65c6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/49f65c6c8fa0fc58f363c4216575d7d06fe04ba9))

## [3.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.14&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.13&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.12&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.11&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.10&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.9&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.6&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.3&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.2&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@3.0.1&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-input-wrapper

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@2.0.1&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.0&targetRepoId=1246) (2020-11-13)

### Bug Fixes

- disable length information block, if maxLength is undefined ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([6610f7d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6610f7de61277a52a8abfc1583be2f1734a907b0))
- remove default value for maxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([f3580c0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f3580c03ad291eccbd0c959197f18930008c9ec7))

### BREAKING CHANGES

- remove default value for maxLength (NPM-592)

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@2.0.1&sourceBranch=refs/tags/@confirmit/react-input-wrapper@3.0.0&targetRepoId=1246) (2020-11-13)

### Bug Fixes

- disable length information block, if maxLength is undefined ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([6610f7d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6610f7de61277a52a8abfc1583be2f1734a907b0))
- remove default value for maxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([f3580c0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f3580c03ad291eccbd0c959197f18930008c9ec7))

### BREAKING CHANGES

- remove default value for maxLength (NPM-592)

# [2.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.23&sourceBranch=refs/tags/@confirmit/react-input-wrapper@2.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [1.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.22&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.23&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([36cceb9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/36cceb9659c02f207382b7ae39f4eb4bebedb80d))

## [1.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.21&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.22&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- truncate input field labels if the field is too narrow to display the entire label ([NPM-427](https://jiraosl.firmglobal.com/browse/NPM-427)) ([69606ac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/69606ac1c9904e90551cc7373735537faddddf3e))
- use correct color for prefix and suffix text ([NPM-521](https://jiraosl.firmglobal.com/browse/NPM-521)) ([a5180ad](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a5180adf593dc6f7aba4ee811f7461bd705bc6b2))

## [1.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.20&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.21&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.19&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.20&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.16&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.17&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.15&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.16&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.14&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.15&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.11&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.12&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.10&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.11&targetRepoId=1246) (2020-09-28)

### Bug Fixes

- change fallback positions for helperTexts on Input fields, to avoid issues where right position is blocked, and it will fallback to bottom, potentially covering the Menu options in the Select component. Changed positions for all Inputs for consistency. ([NPM-542](https://jiraosl.firmglobal.com/browse/NPM-542)) ([c173049](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c1730499cdae33acfdc4be8c7c813616f08ad29d)

## [1.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.8&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.9&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-input-wrapper@1.1.6&sourceBranch=refs/tags/@confirmit/react-input-wrapper@1.1.7&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-input-wrapper@1.1.4...@confirmit/react-input-wrapper@1.1.5) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.1.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-input-wrapper@1.1.2...@confirmit/react-input-wrapper@1.1.3) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-input-wrapper

# [1.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-input-wrapper@1.0.18...@confirmit/react-input-wrapper@1.1.0) (2020-08-16)

### Features

- Added `readOnly` prop

## [1.0.18](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-input-wrapper@1.0.17...@confirmit/react-input-wrapper@1.0.18) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## [1.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-input-wrapper@1.0.16...@confirmit/react-input-wrapper@1.0.17) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-input-wrapper

## CHANGELOG

### v1.0.8

- Fix: Inputs should have white background

### v1.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v0.2.0

- Feat: Helper text is moved to inside a tooltip

### v0.1.3

- Typescript fix - `value` property type is extended - it corresponds to the `@confirmit/react-select` value type

### v0.1.0

- Removing package version in class names.

### v0.0.1

- Initial version
