# Jotunheim React Input Wrapper

### **This component is for internal use (inside this repo) ONLY**

## DESCRIPTION

React Input Wrapper serves as our wrapper component for all of our input components. This includes (but is not limited to)
styling for borders, labels, helper text, maxLength, any prefixes or suffixes.
