import cn from 'classnames';
import React, {useContext, useState} from 'react';
import {StrictModifiers} from '@popperjs/core';

import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import Icon, {close} from '@jotunheim/react-icons';
import {IconButton} from '@jotunheim/react-button';
import Tooltip from '@jotunheim/react-tooltip';
import {InputWidth, LayoutContext} from '@jotunheim/react-contexts';

import classNames from './InputWrapper.module.css';

type Option = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
  value: number | string;
  label: number | string;
};

type InputWrapperProps = {
  active: boolean;
  transparentBackground?: boolean;
  children: React.ReactNode;
  value: string | number | Option | Option[];
  showBorder?: boolean;
  className?: string;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  hasValue?: boolean;
  helperText?: React.ReactNode;
  label?: string;
  maxLength?: number;
  maxLengthErrorText?: string;
  onClear?: () => void;
  prefix?: React.ReactNode;
  required?: boolean;
  showClear?: boolean;
  showMaxLength?: boolean;
  suffix?: React.ReactNode;
  htmlFor?: string;
};

const modifiers: StrictModifiers[] = [
  {
    name: 'flip',
    options: {fallbackPlacements: ['right', 'left', 'top', 'bottom']},
  },
];

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-input-wrapper',
  classNames,
});

const InputWrapper = ({
  active = false,
  transparentBackground = false,
  className = '',
  children,
  disabled = false,
  readOnly = false,
  error = false,
  hasValue,
  helperText = '',
  label = '',
  maxLength,
  maxLengthErrorText = 'Max length exceeded',
  onClear = () => {},
  prefix = null,
  required = false,
  showBorder = true,
  showClear = false,
  showMaxLength = false,
  suffix = null,
  value = '',
  htmlFor,
  ...rest
}: InputWrapperProps) => {
  const [isHovering, setIsHovering] = useState(false);
  const {inputWidth} = useContext(LayoutContext);

  const createSuffix = () => {
    if (!suffix && !showClear) {
      return null;
    }

    return (
      <>
        {suffix}
        {showClear && hasValue && !readOnly && !disabled && (
          <IconButton
            onClick={(e) => {
              e.preventDefault();
              onClear();
            }}
            data-testid="input-wrapper-clear"
            data-input-wrapper-clear>
            <Icon path={close} />
          </IconButton>
        )}
      </>
    );
  };

  const defaultHasValue = () => {
    return value != null && String(value).length > 0;
  };

  const renderDecorator = (decorator, className) => {
    return decorator && <span className={className}>{decorator}</span>;
  };

  hasValue = hasValue === undefined ? defaultHasValue() : hasValue;

  const normalizedLabel = required && label ? `${label} *` : label;

  if (showMaxLength && !maxLength) {
    console.warn(
      'input-wrapper: Information about length is disabled because maxLength is not set.'
    );
  }

  const hasMaxLengthError =
    showMaxLength && maxLength && String(value).length > maxLength;

  if (hasMaxLengthError) {
    error = true;
    helperText = maxLengthErrorText;
  }

  return (
    <div
      data-testid="input-wrapper"
      className={cn(block(), className, {
        [modifier('transparent-background')]: transparentBackground,
        [modifier('max600-min80')]: inputWidth === InputWidth.Max600Min80,
      })}
      {...extractDataAndAriaProps(rest)}>
      {/* This is our label */}
      {label && (
        <label
          className={cn(element('label'), {
            [element('label', 'active')]: active,
            [element('label', 'disabled')]: disabled && !readOnly,
            [element('label', 'readOnly')]: readOnly,
            [element('label', 'error')]: error && !disabled,
            [element('label', 'shrink')]:
              (active || prefix || hasValue) && !readOnly,
            [element('label', 'readOnly-shrink')]:
              (active || prefix || hasValue) && readOnly,
          })}
          htmlFor={htmlFor}
          data-input-wrapper-label=""
          data-testid="input-wrapper-label">
          {normalizedLabel}
        </label>
      )}

      {/* This is the helper text, wrapped in tooltip */}
      <Tooltip
        open={error || active || isHovering}
        content={
          helperText && (
            <span data-input-wrapper-helper-text={true}>{helperText}</span>
          )
        }
        type={error ? 'error' : 'info'}
        placement={error ? 'bottom' : 'right'}
        modifiers={modifiers}
        onToggle={setIsHovering}>
        <div
          className={cn(element('input-container'), {
            [element('input-container', 'disabled')]: disabled || readOnly,
          })}>
          {/* This is for any icon or element before the input */}
          {renderDecorator(
            prefix,
            cn(element('prefix-decorator'), {
              [element('prefix-decorator', 'readOnly')]: readOnly,
            })
          )}

          {/* This is the input */}
          {children}

          {/* This is for any icon or element after the input */}
          {renderDecorator(createSuffix(), element('suffix-decorator'))}

          {/* This is our "border", The nested span is used for the "notched" spacing for the label */}
          <fieldset
            className={cn(element('border'), {
              [element('border', 'active')]: active,
              [element('border', 'disabled')]: disabled && !readOnly,
              [element('border', 'readOnly')]: readOnly,
              [element('border', 'error')]: error && !disabled,
              [element('border', 'show-border')]: showBorder,
            })}
            data-input-wrapper-border="">
            <legend
              className={cn(element('border-notch'), {
                [element('border-notch', 'active')]:
                  active || prefix || hasValue,
              })}>
              {/* If label is not provided set a zero-width character to avoid weird artifacts */}
              {label ? (
                <span className={element('border-notch-text')}>
                  {normalizedLabel}
                </span>
              ) : (
                <span dangerouslySetInnerHTML={{__html: '&#8203;'}} />
              )}
            </legend>
          </fieldset>
        </div>
      </Tooltip>

      {/* This is for showing maxLength */}
      {showMaxLength && maxLength && (
        <div
          className={cn(element('max-length'), {
            [element('max-length', 'error')]: hasMaxLengthError,
          })}
          data-input-wrapper-max-length="">
          {String(value).length} / {maxLength}
        </div>
      )}
    </div>
  );
};

export default InputWrapper;
