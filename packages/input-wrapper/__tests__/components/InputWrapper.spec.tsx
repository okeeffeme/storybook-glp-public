import React from 'react';
import {render, screen} from '@testing-library/react';
import InputWrapper from '../../src/components/InputWrapper';
import {LayoutContext, InputWidth} from '../../../contexts/src';

// const SELECTORS = {
//   inputWrapperLabel: 'label[data-input-wrapper-label]',
// };

describe('Jotunheim React Input Wrapper :: ', () => {
  const defaultRequiredProps = {
    active: false,
    value: '',
  };
  const Render = (props) => {
    return render(
      <InputWrapper {...defaultRequiredProps} {...props} data-testid="wrapper">
        <div />
      </InputWrapper>
    );
  };

  describe('Label', () => {
    it('should render on the child: disabled', () => {
      Render({label: 'label', disabled: true});

      expect(screen.getByTestId('input-wrapper-label')).toHaveTextContent(
        'label'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render on the child: not active and no value', () => {
      Render({label: 'label'});

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML('label');
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render shrunk: disabled', () => {
      Render({
        label: 'label',
        disabled: true,
        value: 'yo',
      });

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML('label');
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render shrunk: not active and with value', () => {
      Render({label: 'label', value: 'yo'});

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML('label');
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render shrunk: active and with value', () => {
      Render({
        label: 'label',
        active: true,
        value: 'yo',
      });

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML('label');
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render shrunk: not active, no value and with prefix', () => {
      Render({label: 'label', prefix: <span />});

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML('label');
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--active'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--disabled'
      );
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--shrink'
      );
    });

    it('should render with * for required', () => {
      Render({label: 'label', required: true});

      expect(screen.getByTestId('input-wrapper-label')).toContainHTML(
        'label *'
      );
    });
  });

  describe('Decorators', () => {
    it('should render prefix', () => {
      Render({
        label: 'label',
        prefix: <div data-prefix />,
      });
      expect(screen.getByRole('group').hasAttribute('data-prefix'));
    });

    it('should render suffix', () => {
      Render({
        label: 'label',
        suffix: <div data-suffix />,
      });
      expect(screen.getByRole('group').hasAttribute('data-suffix'));
    });

    it('should render suffix with showClear and no value', () => {
      Render({
        label: 'label',
        showClear: true,
        suffix: <div data-suffix />,
        value: '',
      });
      expect(screen.getByRole('group').hasAttribute('data-suffix'));
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should render suffix with showClear and value', () => {
      Render({
        label: 'label',
        showClear: true,
        suffix: <div data-suffix />,
        value: 'yo',
      });
      expect(screen.getByRole('group').hasAttribute('data-suffix'));
      expect(screen.queryByRole('button')).toBeInTheDocument();
    });

    it('should not render suffix with showClear and value', () => {
      Render({
        label: 'label',
        showClear: false,
        suffix: <div data-suffix />,
        value: 'yo',
      });
      expect(screen.getByRole('group').hasAttribute('data-suffix'));
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should not render showClear when disabled', () => {
      Render({
        label: 'label',
        disabled: true,
        showClear: true,
        suffix: <div data-suffix />,
        value: 'yo',
      });

      expect(screen.getByRole('group').hasAttribute('data-suffix'));
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });

    it('should not render showClear when readOnly', () => {
      Render({
        label: 'label',
        readOnly: true,
        showClear: true,
        suffix: <div data-suffix />,
        value: 'yo',
      });

      expect(screen.getByRole('group').hasAttribute('data-suffix'));
      expect(screen.queryByRole('button')).not.toBeInTheDocument();
    });
  });

  describe('Border', () => {
    it('should render border - active', () => {
      Render({
        label: 'label',
        active: true,
      });

      expect(screen.getByRole('group')).toHaveClass(
        'comd-input-wrapper__border--active'
      );
    });

    it('should render border - disabled', () => {
      Render({
        label: 'label',
        disabled: true,
      });

      expect(screen.getByRole('group')).toHaveClass(
        'comd-input-wrapper__border--disabled'
      );
    });

    it('should render border - error', () => {
      Render({
        label: 'label',
        error: true,
      });

      expect(screen.getByRole('group')).toHaveClass(
        'comd-input-wrapper__border--error'
      );
    });

    it('should render border - error and disabled', () => {
      Render({
        label: 'label',
        error: true,
        disabled: true,
      });

      expect(screen.getByRole('group')).not.toHaveClass(
        'comd-input-wrapper__border--error'
      );
      expect(screen.getByRole('group')).toHaveClass(
        'comd-input-wrapper__border--disabled'
      );
    });

    it('should render notch - active', () => {
      Render({
        label: 'label',
        active: true,
      });
      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('comd-input-wrapper__border-notch--active')
      ).toBeTruthy();
    });

    it('should render notch - prefix', () => {
      Render({
        label: 'label',
        prefix: <div />,
      });
      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('comd-input-wrapper__border-notch--active')
      ).toBeTruthy();
    });

    it('should render notch - hasValue', () => {
      Render({
        label: 'label',
        hasValue: true,
      });
      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('comd-input-wrapper__border-notch--active')
      ).toBeTruthy();
    });

    it('should not render notch', () => {
      Render({
        label: 'label',
      });
      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('comd-input-wrapper__border-notch--active')
      ).toBeTruthy();
    });

    it('should render border notch with zero-width character', () => {
      Render({
        label: '',
        hasValue: true,
      });
      expect(screen.getByRole('group')).toContainHTML('');
    });
  });

  describe('Helper text', () => {
    it('should render helper text - active', () => {
      Render({
        label: 'label',
        active: true,
        helperText: 'helper text',
      });
      expect(screen.getByText(/helper text/i)).toBeInTheDocument();
    });

    it('should render helper text - active and disabled', () => {
      Render({
        label: 'label',
        active: true,
        disabled: true,
        helperText: 'helper text',
      });

      expect(screen.getByText(/helper text/i)).toBeInTheDocument();
    });

    it('should not render helper text - inactive', () => {
      Render({
        label: 'label',
        helperText: 'helper text',
      });

      expect(screen.getByRole('group')).not.toHaveClass(
        'data-input-wrapper-helper-text'
      );
    });

    it('should render helper text - error and inactive', () => {
      Render({
        label: 'label',
        error: true,
        helperText: 'helper text',
      });

      expect(screen.getByText(/helper text/i)).toBeInTheDocument();
    });

    it('should render helper text - error and disabled', () => {
      Render({
        label: 'label',
        error: true,
        disabled: true,
        helperText: 'helper text',
      });
      expect(screen.getByText(/helper text/i)).toBeInTheDocument();
    });
  });

  describe('Max length', () => {
    it('should render max length', () => {
      Render({
        maxLength: 55,
        showMaxLength: true,
        value: 'hello',
      });

      expect(screen.getByText('5 / 55')).toBeInTheDocument();
    });

    it('should render error when exceeding maxLength', () => {
      Render({
        label: 'label',
        error: false,
        helperText: 'helper text',
        maxLength: 10,
        showMaxLength: true,
        value: 'too long text here',
        maxLengthErrorText: 'Custom max length error',
      });

      expect(screen.getByTestId('input-wrapper-label')).toHaveClass(
        'comd-input-wrapper__label--error'
      );
      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('comd-input-wrapper__max-length--error')
      ).toBeTruthy();
      expect(screen.getByText(/Custom max length error/)).toBeInTheDocument();
    });

    it('should not render error when not exceeding maxLength', () => {
      Render({
        label: 'label',
        error: false,
        helperText: 'helper text',
        maxLength: 10,
        showMaxLength: true,
        value: 'short text',
        maxLengthErrorText: 'Custom max length error',
      });
      expect(screen.getByTestId('input-wrapper-label')).not.toHaveClass(
        '.comd-input-wrapper__label--error'
      );

      expect(
        screen
          .getByRole('group')
          .getElementsByClassName('.comd-input-wrapper__max-length--error')
      ).toHaveLength(0);

      expect(screen.getByRole('group')).not.toHaveAttribute(
        'data-input-wrapper-helper-text'
      );
    });
  });

  describe('Layout', () => {
    it('should render without layout classNames by default', () => {
      render(
        <InputWrapper data-testid="input-wrapper" {...defaultRequiredProps}>
          <div />
        </InputWrapper>
      );

      expect(screen.getByTestId('input-wrapper')).not.toHaveClass(
        'comd-input-wrapper--max600-min80'
      );
    });

    it('should render with max600-min80 className by if specified on context', () => {
      render(
        <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
          <InputWrapper data-testid="input-wrapper" {...defaultRequiredProps}>
            <div />
          </InputWrapper>
        </LayoutContext.Provider>
      );

      expect(screen.getByTestId('input-wrapper')).toHaveClass(
        'comd-input-wrapper--max600-min80'
      );
    });
  });
});
