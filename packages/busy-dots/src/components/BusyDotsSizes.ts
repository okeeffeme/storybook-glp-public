export enum BusyDotsSizes {
  base = 'base',
  large = 'large',
  extraLarge = 'extra-large',
}
