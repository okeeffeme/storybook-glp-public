import React, {AriaAttributes} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {BusyDotsSizes} from './BusyDotsSizes';
import classNames from './BusyDots.module.css';

const {block, element, modifier} = bemFactory('comd-busy-dots', classNames);

type BusyDotsProps = {
  size?: BusyDotsSizes | string;
} & AriaAttributes;

export const BusyDots = ({size, ...rest}: BusyDotsProps) => {
  const classes = cn(block(), {
    [(size && modifier(size)) || '']: !!size,
  });

  const dot = <div className={element('dot')} />;

  return (
    <div
      className={classes}
      data-busy-dots=""
      data-testid="busy-dots"
      {...extractDataAndAriaProps(rest)}>
      {dot}
      {dot}
      {dot}
    </div>
  );
};

BusyDots.sizes = BusyDotsSizes;

export default BusyDots;
