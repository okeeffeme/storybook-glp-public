import BusyDots from './components/BusyDots';
import {BusyDotsSizes} from './components/BusyDotsSizes';

export {BusyDots, BusyDotsSizes};
export default BusyDots;
