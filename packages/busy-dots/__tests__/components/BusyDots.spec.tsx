import React from 'react';
import {render, screen} from '@testing-library/react';

import {BusyDots} from '../../src/components/BusyDots';

describe('BusyDots', () => {
  it('should render with default props', () => {
    render(<BusyDots />);

    expect(screen.getByTestId('busy-dots')).toMatchSnapshot();
  });

  it('should render with large size', () => {
    render(<BusyDots size={BusyDots.sizes.large} />);

    expect(
      screen
        .getByTestId('busy-dots')
        .classList.contains('comd-busy-dots--large')
    ).toBe(true);
  });

  it('should render with data- attribute', () => {
    render(<BusyDots data-custom-attribute="some-value" />);

    expect(screen.getByTestId('busy-dots')).toHaveAttribute(
      'data-custom-attribute',
      'some-value'
    );
  });

  it('should render with aria- attribute', () => {
    render(<BusyDots aria-disabled={true} />);

    expect(screen.getByTestId('busy-dots')).toHaveAttribute('aria-disabled');
  });
});
