import React from 'react';
import {storiesOf} from '@storybook/react';

import BusyDots from '../src/components/BusyDots';

storiesOf('Components/BusyDots', module)
  .add('basic', () => <BusyDots />)
  .add('large size', () => <BusyDots size="large" />)
  .add('extra large size', () => <BusyDots size="extra-large" />);
