# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@5.0.3&sourceBranch=refs/tags/@jotunheim/react-busy-dots@5.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-busy-dots

## [5.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@5.0.2&sourceBranch=refs/tags/@jotunheim/react-busy-dots@5.0.3&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([fcca72f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fcca72f5b833efd0506b1967ae75f4b260085f98))

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@5.0.1&sourceBranch=refs/tags/@jotunheim/react-busy-dots@5.0.2&targetRepoId=1246) (2022-12-22)

### Bug Fixes

- add test id for BusyDots component ([NPM-1172](https://jiraosl.firmglobal.com/browse/NPM-1172)) ([f90c341](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f90c3418ce5326d32b4886b292b5d1c007920f97))

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@5.0.0&sourceBranch=refs/tags/@jotunheim/react-busy-dots@5.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-busy-dots

# [5.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@4.0.1&sourceBranch=refs/tags/@jotunheim/react-busy-dots@5.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of BusyDots ([NPM-928](https://jiraosl.firmglobal.com/browse/NPM-928)) ([2a4a5cb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2a4a5cb4e3cf5ad4bbd52668546e7522dddf2b04))
- remove default theme from BusyDots ([NPM-1063](https://jiraosl.firmglobal.com/browse/NPM-1063)) ([8cf00cf](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8cf00cf880f33e63024f17936a2a56053d4c33e2))
- remove default theme from BusyDots ([NPM-1063](https://jiraosl.firmglobal.com/browse/NPM-1063)) ([fb6a24a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb6a24a6c32efb627af895902ef01c7df56e6015))

### BREAKING CHANGES

- As part of NPM-1062 we discard default theme support
- As part of NPM-1062 we discard default theme support
- As part of NPM-925 we remove className props from components.

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-busy-dots@4.0.0&sourceBranch=refs/tags/@jotunheim/react-busy-dots@4.0.1&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-busy-dots

## 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-busy-dots@3.0.5&sourceBranch=refs/tags/@confirmit/react-busy-dots@3.0.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-busy-dots@3.0.4&sourceBranch=refs/tags/@confirmit/react-busy-dots@3.0.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-busy-dots

## [3.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-busy-dots@3.0.3&sourceBranch=refs/tags/@confirmit/react-busy-dots@3.0.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [3.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-busy-dots@3.0.2&sourceBranch=refs/tags/@confirmit/react-busy-dots@3.0.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-busy-dots

## [3.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-busy-dots@3.0.1...@confirmit/react-busy-dots@3.0.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-busy-dots

## CHANGELOG

### v3.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v2.2.0

- Removing package version in class names.

### v2.1.0

- Typescript support

### v2.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v1.0.0

- **BREAKING** - Removed props: baseClassName, classNames

### v0.0.4

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
