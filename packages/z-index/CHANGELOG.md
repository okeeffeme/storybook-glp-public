# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-z-index@3.0.1&sourceBranch=refs/tags/@confirmit/react-z-index@3.0.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-z-index@3.0.0&sourceBranch=refs/tags/@confirmit/react-z-index@3.0.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-z-index@2.1.2&sourceBranch=refs/tags/@confirmit/react-z-index@3.0.0&targetRepoId=1246) (2020-11-11)

### Features

- remove ZIndexStackContext, useZIndexStack and withZIndexStack from package ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([8bcc68a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8bcc68a52ef8d7bb3812b9e0fa85f5e0c6d43558))

### BREAKING CHANGES

- ZIndexStackContext, useZIndexStack and withZIndexStack are moved to @confirmit/react-contexts

## [2.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-z-index@2.1.1&sourceBranch=refs/tags/@confirmit/react-z-index@2.1.2&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-z-index

## [2.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-z-index@2.1.0...@confirmit/react-z-index@2.1.1) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-z-index

## CHANGELOG

### v2.1.0

- Feat: Typescript support

## v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: New package name: `@confirmit/react-z-index`

### v1.2.1

- Fix: Move confirmit-react-utils to `dependencies`

### v1.2.0

- Feat: React context `ZIndexStackContext` is added to provide the single instance of zIndexStack factory throughout the application

### v1.1.0

- Feat: new method `obtainFixed(index)` - enables you to obtain a fixed z-index even if it is already taken, or the next auto available index is actually higher.

### v1.0.0

- Initial version, moved from `confirmit-react-components` package
