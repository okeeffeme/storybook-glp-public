import {zindexStackFactory} from '../../src/ZIndexStack';

describe('zindex-provider :: ', () => {
  it('should be created with default values', () => {
    const zindexStack = zindexStackFactory();

    expect(zindexStack).toMatchSnapshot();
  });

  it('should obtain next two zindecies', () => {
    const zindexStack = zindexStackFactory();

    expect(zindexStack.obtain()).toBe(10);
    expect(zindexStack.obtain()).toBe(20);
  });

  it('should obtain next two zindecies then release first one and obtain new one', () => {
    const zindexStack = zindexStackFactory();

    const first = zindexStack.obtain();
    zindexStack.obtain();

    zindexStack.release(first);

    expect(zindexStack.obtain()).toBe(30);
  });

  it('should obtain next two zindecies then release second one and obtain new one', () => {
    const zindexStack = zindexStackFactory();

    zindexStack.obtain();
    const second = zindexStack.obtain();
    zindexStack.release(second);

    expect(zindexStack.obtain()).toBe(20);
  });

  it('should obtain zindecies with custom step', () => {
    const zindexStack = zindexStackFactory();

    expect(zindexStack.obtain(20)).toBe(20);
    expect(zindexStack.obtain(1050)).toBe(1070);
    expect(zindexStack.obtain()).toBe(1080);
  });

  it('should not release if zindex is not obtained', () => {
    const zindexStack = zindexStackFactory();

    zindexStack.release(zindexStack.initial + 10 * zindexStack.step);
  });

  it('should set initial and step', () => {
    const zindexStack = zindexStackFactory();
    zindexStack.initial = 1000;
    zindexStack.step = 200;

    expect(zindexStack).toMatchSnapshot();
  });

  describe('fixed :: ', () => {
    it('should allow to obtain fixed index', () => {
      const zindexStack = zindexStackFactory();

      expect(zindexStack.obtainFixed(100)).toBe(100);
    });

    it('should allow to obtain fixed index, even when that index is already taken', () => {
      const zindexStack = zindexStackFactory();
      const first = zindexStack.obtain();

      expect(zindexStack.obtainFixed(first)).toBe(10);
    });

    it('should obtain next available in stack after obtainFixed', () => {
      const zindexStack = zindexStackFactory();
      const fixed = zindexStack.obtainFixed(100);

      expect(zindexStack.obtainFixed(fixed)).toBe(100);
      expect(zindexStack.obtain()).toBe(110);
    });

    it('should allow multiple obtainFixed with same value', () => {
      const zindexStack = zindexStackFactory();
      const fixed1 = zindexStack.obtainFixed(100);
      const fixed2 = zindexStack.obtainFixed(100);

      expect(zindexStack.obtainFixed(fixed1)).toBe(100);
      expect(zindexStack.obtainFixed(fixed2)).toBe(100);
    });

    it('should release obtainFixed with same value', () => {
      const zindexStack = zindexStackFactory();
      const fixed1 = zindexStack.obtainFixed(100);
      const fixed2 = zindexStack.obtainFixed(100);

      expect(zindexStack.obtainFixed(fixed1)).toBe(100);
      expect(zindexStack.obtainFixed(fixed2)).toBe(100);

      zindexStack.release(fixed1);
      expect(zindexStack.obtain()).toBe(10);

      zindexStack.release(fixed2);
      expect(zindexStack.obtain()).toBe(20);
    });
  });
});
