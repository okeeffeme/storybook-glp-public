# Jotunheim React Z Index

Keep track of z-indexes and provide the next available z-index. Useful for displaying dropdowns within modals f.ex.

This should be used as a `peerDependency`, because it needs to share a 'global state' to be able to provide the correct numbers.

## Usage

### Standard usage with auto assigned z-indexes

```javascript
import zIndexStack from '@jotunheim/react-z-index';

// get the next available number:
const zIndex = zIndexStack.obtain();

// or get a number with a custom step.
// This might not give back 100, depending on what the current highest z-index is
const zIndexSpecific = zIndexStack.obtain(100);

// release the indexes when you are done with them (f.ex when closing modal):
zIndexStack.release(zIndex);
zIndexStack.release(zIndexSpecific);
```

### With fixed index

This can be useful in scenarios where you want to ensure that A is shown below B, regardless of the order they were created, and regardless of other items that might have been created.

Examples:
You have two modals, A and B. You always want A to be shown below B. You also have tooltips in both modals, and need them to work properly. This could be solved by setting the z-index of A and B directly in CSS, but that would get very complex once you want tooltips/dropdowns/etc to also behave properly.

If you set A to have z-index 500, and B to have z-index 600, this will work fine. Tooltips and dropdowns will behave as normal, and get the next available z-index (610 if both A and B are open).

#### When using the same z-index multiple times, there are some things to beware of:

You could potentially get into issues if you set A and B both to z-index 500. Now tooltips would get 510 as the next available, which is fine. If you close B (and release 500), tooltips in A would break since the next available z-index would be 10.

```javascript
import zIndexStack from '@jotunheim/react-z-index';

// get a number with a custom step.
// This will always return 100, even if it was already taken.
const zIndexFixed = zIndexStack.obtainFixed(100);

// If you set the same fixed z-index multiple times, it will still only appear once
// in the stack. This means that the first one to release it will make it available again,
// and will allow auto obtained z-indexes to start at a lower number.
zIndexStack.release(zIndexFixed);
```
