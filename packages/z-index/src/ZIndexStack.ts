/*
The idea of zIndexStack is to provide correct zIndex to portals.
So nested modals, popovers, dropdowns, tooltips and etc can work together without any tweaking from outside

Current formula: topIndex + step
Default step is 10
Step can be passed to obtain method (like Modal does)

Example:
Tooltip obtains 0 + 10 = 10
Modal obtains 1050 + 10 = 1060
Tooltip in Modal obtains 1060 + 10 = 1070
Modal in  Modal obtains 1060 + 1050 = 2110
Tooltip in Modal in Modal obtains 2110 + 10


 */
export const zindexStackFactory = () => {
  let initial = 0;
  let step = 10;

  const indicies = new Map<number, boolean>();

  return {
    get initial(): number {
      return initial;
    },
    set initial(value: number) {
      initial = value;
    },
    get step(): number {
      return step;
    },
    set step(value: number) {
      step = value;
    },
    obtain(nextStep: number = step): number {
      const topIndexInStack = indicies.size
        ? Math.max(...indicies.keys())
        : initial;
      const nextIndex = topIndexInStack + nextStep;
      indicies.set(nextIndex, true);

      return nextIndex;
    },
    release(index: number) {
      if (!indicies.has(index)) return;

      indicies.delete(index);
    },
    obtainFixed(index: number): number {
      indicies.set(index, true);
      return index;
    },
  };
};

export default zindexStackFactory();
