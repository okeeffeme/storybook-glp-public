import zIndexStack, {zindexStackFactory} from './ZIndexStack';

type ZIndexStack = typeof zIndexStack;

export {zIndexStack as default, zIndexStack, zindexStackFactory};

export type {ZIndexStack};
