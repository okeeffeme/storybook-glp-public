import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import TextArea from '../../src/components/TextArea';

describe('Jotunheim React Textarea :: ', () => {
  it('renders', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getAllByRole('textbox');

    expect(textArea.length).toBe(1);
  });

  it('should add error modifier to border class', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      error: true,
      helperText: 'Invalid value',
    };

    const {container} = render(<TextArea {...props} />);

    expect(
      container.querySelector('.comd-input-wrapper__border--error')
    ).toBeTruthy();
  });

  it('should add resizable modifier when resizable enabled', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      error: true,
      helperText: 'Invalid value',
      resizable: true,
    };

    const {container} = render(<TextArea {...props} />);

    expect(container.querySelectorAll('.comd-textarea--resizable').length).toBe(
      1
    );
  });

  it('should display placeholder text when focused', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Test placeholder text',
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');

    expect(textArea).toHaveProperty('placeholder', 'Test placeholder text');
  });

  it('should display blank placeholder text when NOT focused', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Test placeholder text',
      label: 'Hide placeholder',
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');

    expect(textArea).toHaveProperty('placeholder', '');
  });

  it('should display placeholder text when label is NOT defined', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Test placeholder text',
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');

    expect(textArea).toHaveProperty('placeholder', props.placeholder);
  });

  it('should display blank placeholder text with no placeholder prop', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');

    expect(textArea).toHaveProperty('placeholder', '');
  });

  it('should display label and placeholder text when focused', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
      label: 'Label text',
      placeholder: 'Placeholder text',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');
    const label = screen.getAllByText(/label text/i);

    fireEvent.focus(textArea);

    expect(textArea).toHaveProperty('placeholder', props.placeholder);
    expect(label[0].textContent).toEqual(props.label);
    expect(label[1].textContent).toEqual(props.label);
  });

  it('should display label only when NOT focused', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
      label: 'Label text',
      placeholder: 'Placeholder text',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');
    const label = screen.getAllByText(/label text/i);

    expect(label[0].textContent).toEqual(props.label);
    expect(label[1].textContent).toEqual(props.label);
    expect(textArea).toHaveProperty('placeholder', '');
  });

  it('should NOT display any label or placeholder if no props provided', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');
    const label = screen.queryAllByText(/label text/i);

    expect(label.length).toBe(0);
    expect(textArea).toHaveProperty('placeholder', '');
  });

  it('should not add resizable modifier when resizable disabled', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      error: true,
      helperText: 'Invalid value',
      resizable: false,
    };

    const {container} = render(<TextArea {...props} />);

    const textArea = screen.getAllByRole('textbox');

    expect(textArea.length).toBe(1);
    expect(container.querySelectorAll('.comd-textarea--resizable').length).toBe(
      0
    );
  });

  it('should not show helper text if inactive', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      helperText: 'Invalid value',
    };

    const {container} = render(<TextArea {...props} />);

    expect(
      container.querySelectorAll('[data-input-wrapper-helper-text]').length
    ).toBe(0);
  });

  it('should show helper text if defined and active', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      helperText: 'Invalid value',
    };

    render(<TextArea {...props} />);

    const textArea = screen.getByRole('textbox');

    fireEvent.focus(textArea);

    expect(screen.getByText(/invalid value/i)).toBeInTheDocument();
  });

  it('should show helper text if error', () => {
    const props = {
      onChange: jest.fn(),
      placeholder: 'Placeholder',
      value: 'Value',
      helperText: 'Invalid value',
      error: true,
    };

    render(<TextArea {...props} />);

    const helperText = screen.getByText(/invalid value/i);

    expect(helperText.textContent).toEqual('Invalid value');
  });

  it('should have particular classnames and props if disabled', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
      disabled: true,
    };

    render(<TextArea data-testid="text-area" {...props} />);

    const textArea = screen.getByTestId('text-area');
    const textBox = screen.getByRole('textbox');

    expect(textBox).toHaveProperty('disabled');
    expect(textBox.classList.contains('comd-textarea--disabled')).toBeTruthy();
    expect(
      textArea.querySelectorAll('.comd-input-wrapper__border--disabled').length
    ).toBe(1);
  });

  it('should have no particular classnames and props if not disabled', () => {
    const props = {
      onChange: jest.fn(),
      value: 'Value',
    };

    render(<TextArea data-testid="text-area" {...props} />);

    const textArea = screen.getByTestId('text-area');
    const textBox = screen.getByRole('textbox');

    expect(textBox).toHaveProperty('disabled');
    expect(textBox.classList.contains('comd-textarea--disabled')).toBeFalsy();
    expect(
      textArea.classList.contains('comd-input-wrapper__border--disable')
    ).toBeFalsy();
  });

  describe('Max length :: ', () => {
    it('should not add maxLength by default', () => {
      render(<TextArea label="Label" onChange={jest.fn()} />);

      expect(screen.getByRole('textbox')).not.toHaveProperty('maxlength');
    });

    it('should add maxLength prop to input when specified', () => {
      render(<TextArea label={'Label'} maxLength={10} onChange={jest.fn()} />);

      expect(screen.getByRole('textbox')).not.toHaveProperty('maxlength');
    });

    it('should not add maxLength prop to input when specified, when showMaxLength is also enabled', () => {
      render(
        <TextArea
          label="Label"
          maxLength={10}
          showMaxLength={true}
          onChange={jest.fn()}
        />
      );

      expect(screen.getByRole('textbox')).not.toHaveProperty('maxlength');
    });
  });
});
