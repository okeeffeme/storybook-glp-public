# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.16&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.17&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.16&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.16&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.14&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.15&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.13&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.14&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.12&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.13&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.11&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.12&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.10&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.11&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.9&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.10&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.8&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.9&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.6&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.8&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- import types in separate type import for pager, switch and textarea package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([28a09c2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/28a09c2cd05cbcd3745b6e0c8fa1ff7ba861ccff))

## [10.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.6&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.7&targetRepoId=1246) (2023-02-17)

### Bug Fixes

- import types in separate type import for pager, switch and textarea package ([NPM-1250](https://jiraosl.firmglobal.com/browse/NPM-1250)) ([9c1b367](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9c1b3674319da58da589a71e1d3eea1769cc4718))

## [10.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.5&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.6&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.4&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.5&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.3&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.4&targetRepoId=1246) (2023-02-09)

### Bug Fixes

- add missing props ([NPM-1201](https://jiraosl.firmglobal.com/browse/NPM-1201)) ([d96d1bc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d96d1bcddeae4f40da6c08cb107f5e11a248a06e))

## [10.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.2&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.3&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.1&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.2&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.1.0&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.1&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-textarea

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.19&sourceBranch=refs/tags/@jotunheim/react-textarea@10.1.0&targetRepoId=1246) (2023-01-31)

### Features

- convert textarea package to TypeScript ([NPM-1244](https://jiraosl.firmglobal.com/browse/NPM-1244)) ([888287e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/888287e4aafb89c5752d768f1b5f07f78ffb1225))

## [10.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.18&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.19&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.17&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.18&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.16&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.17&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.14&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.16&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.14&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.15&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.13&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.14&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.12&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.13&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.11&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.12&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.10&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.11&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.9&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.8&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.9&targetRepoId=1246) (2022-12-20)

### Bug Fixes

- Extract from InputWrapper into variables, using to set min-height on TextArea, setting height to auto ([HUB-9719](https://jiraosl.firmglobal.com/browse/HUB-9719)) ([00d047a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/00d047a25b04b7e82ce8f9b5f9465090d41e69d1))

## [10.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.7&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.4&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.4&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.4&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.3&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.2&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-textarea

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@10.0.0&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.1&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-textarea

# [10.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.17&sourceBranch=refs/tags/@jotunheim/react-textarea@10.0.0&targetRepoId=1246) (2022-10-11)

### Features

- remove className prop of TextArea ([NPM-959](https://jiraosl.firmglobal.com/browse/NPM-959)) ([a7ab332](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a7ab332fda9dab17e0b29a90326de357e4dc2c7b))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.16&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.15&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.14&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.13&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.12&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.13&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.11&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.10&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.9&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.7&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.4&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.3&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.2&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.1&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-textarea

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-textarea@9.0.0&sourceBranch=refs/tags/@jotunheim/react-textarea@9.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-textarea

# 9.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [8.0.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.65&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.66&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.64&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.65&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.63&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.64&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.62&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.63&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.61&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.62&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.60&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.61&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.58&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.59&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.57&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.58&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.56&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.57&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.55&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.56&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.54&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.55&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.53&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.54&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.50&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.51&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.49&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.50&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.48&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.49&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.47&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.48&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.46&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.47&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.45&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.46&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.44&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.45&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.43&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.44&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [8.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.42&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.43&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.40&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.41&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.39&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.40&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.38&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.39&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.37&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.38&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.36&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.37&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.35&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.36&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.34&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.35&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.33&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.34&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.32&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.33&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.31&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.32&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.30&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.31&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.29&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.30&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.28&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.29&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.27&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.28&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.26&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.27&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.25&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.26&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.24&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.25&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.23&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.24&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.22&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.23&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.21&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.22&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.20&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.21&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.19&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.20&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.18&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.19&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.17&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.18&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.15&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.16&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.14&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.15&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.13&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.14&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.12&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.13&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.11&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.12&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.10&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.11&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.9&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.10&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.8&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.9&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.7&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.8&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.6&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.7&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.5&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.3&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.4&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.2&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.3&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.1&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.2&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-textarea

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@8.0.0&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.1&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-textarea

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.17&sourceBranch=refs/tags/@confirmit/react-textarea@8.0.0&targetRepoId=1246) (2021-01-18)

### Features

- Show error message when exceeding max length ([NPM-573](https://jiraosl.firmglobal.com/browse/NPM-573)) ([190a282](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/190a2823b613e8c279dd61d3f1780f461f3e5d15))

### BREAKING CHANGES

- If both maxLength and showMaxLength are specified, input will allow the value to exceed the maxLength, and show an error message instead.

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.16&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.15&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.14&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.13&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.12&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.11&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.10&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.9&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.6&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.3&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.2&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-textarea

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@7.0.1&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-textarea

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@6.0.1&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@6.0.1&sourceBranch=refs/tags/@confirmit/react-textarea@7.0.0&targetRepoId=1246) (2020-11-13)

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@6.0.1&sourceBranch=refs/tags/@confirmit/react-textarea@6.0.2&targetRepoId=1246) (2020-11-13)

**Note:** Version bump only for package @confirmit/react-textarea

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.37&sourceBranch=refs/tags/@confirmit/react-textarea@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [5.1.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.36&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.37&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- accept React.node in helperText ([NPM-587](https://jiraosl.firmglobal.com/browse/NPM-587)) ([9eb08b4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9eb08b41e46ab66b67de5222256664265d6fea6e))

## [5.1.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.35&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.36&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.34&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.35&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.33&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.34&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.30&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.31&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.29&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.30&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.28&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.29&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.25&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.26&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.24&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.25&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.22&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.23&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-textarea@5.1.20&sourceBranch=refs/tags/@confirmit/react-textarea@5.1.21&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.19](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-textarea@5.1.18...@confirmit/react-textarea@5.1.19) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-textarea@5.1.16...@confirmit/react-textarea@5.1.17) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.11](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-textarea@5.1.10...@confirmit/react-textarea@5.1.11) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-textarea

## [5.1.10](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-textarea@5.1.9...@confirmit/react-textarea@5.1.10) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-textarea

## CHANGELOG

### v5.1.1

- Fix: Inputs should have white background

### v5.1.0

- Feat: value will now accept either a string or number as a prop.

### v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v4.2.0

- Feat: Helper text is moved to inside a tooltip

### v4.1.6

- Fix: Use aria-required attribute on DOM input node instead of required, to avoid built-in browser message

- Removing package version in class names.

### v4.1.0

- Removing package version in class names.

### v4.0.12

- Fix: Provide extra breathing room at the top of the textarea so scrollable content does not overlap the label

### v4.0.4

- Fix: Refactor how bemFactory method is called, pass in object of baseClassName and classNames instead of full object returned from useTheme

### v4.0.2

- Fix: Align classname was incorrect
- Feat: Add `onKeyDown` prop

### v4.0.1

- Fix: Update text color

### v4.0.0

- **BREAKING**:
  - Use new InputWrapper component with updated DS styles
  - Deprecated props: `height`, `charLimit`, `showCharLimit`
  - Added props (that support the features from the deprecated props): `rows`, `maxLength`, `showMaxLength`

### v3.0.5

- Fix: Label no longer uses Placeholder text as fallback
- Fix: Label now accepts "required" prop

### v3.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v2.1.0

- Feat: Add ref property (forwardRef) to the textarea fields
- Feat: Add onBlur handler

### v2.0.0

- **BREAKING**
  - TextArea component will now take 100% width, to be similar to our other input fields
  - Resizing enabled by default
  - Default theme is removed as it was not really implemented. TextArea will always use Material theme
- Fix: Label does not cover textarea scrollbar anymore
- Fix: Keep same padding-top value at all times to avoid textarea changing height when empty and changing between focused and not focused, and height is set to auto (default)
- Feat: Support vertical resizing of textarea with `resizable` prop. This is `true` by default

### v1.0.0

- **BREAKING** - Removed props: baseClassName, classNames, ErrorRenderer

### v0.0.3

- Feat: Add data tags

### v0.0.2

- **BREAKING CHANGE OF APPEARANCE** font-size of helper text is the same size as helper text of TextField.
- **BREAKING CHANGE OF APPEARANCE** Error border is 1px, hovering and focusing leave error color but 2px. Similar to TextField
- Feat: Moved under @confirmit scope
- Feat: "disabled" property added
- Bugfix: "placeholder" property no more required

## CHANGELOG of confirmit-textarea

### v0.1.0

- Feat: new props `error` and `helperText` can be used to show validation message.

### v0.0.1

- Initial version
