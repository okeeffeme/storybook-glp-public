import TextArea from './components/TextArea';
import type {TextAreaProps} from './components/TextArea';

export {TextArea, TextAreaProps};
export default TextArea;
