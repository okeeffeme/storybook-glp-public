import React, {
  FocusEventHandler,
  KeyboardEventHandler,
  ReactNode,
  useState,
} from 'react';

import InputWrapper from '@jotunheim/react-input-wrapper';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory, useTheme} from '@jotunheim/react-themes';
import cn from 'classnames';

import componentThemes from '../themes';

const noop = () => {};

export interface TextAreaProps {
  onChange: (value: string) => void;
  id?: string;
  name?: string;
  isRequired?: boolean;
  value?: number | string;
  align?: string;
  autoFocus?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  height?: number | string;
  helperText?: ReactNode;
  label?: string;
  onBlur?: FocusEventHandler | undefined;
  onFocus?: FocusEventHandler | undefined;
  onKeyDown?: KeyboardEventHandler;
  maxLength?: number;
  placeholder?: string;
  required?: boolean;
  resizable?: boolean;
  rows?: number;
  showMaxLength?: boolean;
  maxLengthErrorText?: string;
}

const TextArea = React.forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (
    {
      align = 'left',
      autoFocus = false,
      disabled = false,
      readOnly = false,
      error,
      helperText,
      label,
      onBlur = noop,
      onFocus = noop,
      onChange,
      onKeyDown,
      maxLength,
      maxLengthErrorText,
      placeholder,
      required = false,
      resizable = false,
      rows,
      showMaxLength = false,
      value = '',
      ...rest
    },
    ref
  ) => {
    const [active, setActive] = useState(false);
    const {baseClassName, classNames} = useTheme('textArea', componentThemes);
    const {block, modifier} = bemFactory({baseClassName, classNames});

    return (
      <InputWrapper
        active={active}
        disabled={disabled}
        readOnly={readOnly}
        error={error}
        helperText={helperText}
        label={label}
        maxLength={maxLength}
        maxLengthErrorText={maxLengthErrorText}
        required={required}
        showMaxLength={showMaxLength}
        value={value}
        {...extractDataAndAriaProps(rest)}>
        <textarea
          autoFocus={autoFocus}
          className={cn(block(), modifier(`align-${align}`), {
            [modifier('disabled')]: disabled && !readOnly,
            [modifier('readOnly')]: readOnly,
            [modifier('resizable')]: resizable && !disabled && !readOnly,
          })}
          disabled={disabled || readOnly}
          maxLength={showMaxLength ? undefined : maxLength}
          onBlur={(e) => {
            setActive(false);
            onBlur(e);
          }}
          onFocus={(e) => {
            setActive(true);
            onFocus(e);
          }}
          onChange={(e) => onChange(e.target.value)}
          onKeyDown={onKeyDown}
          placeholder={!active && !!label ? '' : placeholder}
          ref={ref}
          aria-required={required}
          rows={rows}
          value={value}
          data-textarea-input=""
        />
      </InputWrapper>
    );
  }
);

TextArea.displayName = 'TextArea';

export default TextArea;
