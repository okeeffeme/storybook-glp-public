import classNames from './css/textarea.module.css';

export default {
  textArea: {
    baseClassName: 'comd-textarea',
    classNames,
  },
};
