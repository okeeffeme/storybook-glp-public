import React, {useState, useRef} from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {boolean, number, select, text} from '@storybook/addon-knobs';

import TextArea from '../src/components/TextArea';
import {LayoutContext, InputWidth} from '../../contexts/src';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

const TextAreaWithBlur = () => {
  const textarea = useRef<HTMLTextAreaElement | null>(null);
  const div = useRef<HTMLDivElement | null>(null);

  const [value, setValue] = useState('');

  return (
    <>
      <TextArea
        onChange={setValue}
        value={value}
        ref={textarea}
        onBlur={() => {
          if (div.current && textarea.current) {
            div.current.innerHTML = textarea.current.value;
          }
        }}
      />
      <br />
      <h4>Text below will update to textarea value when you blur the field</h4>
      <div ref={div} />
    </>
  );
};

storiesOf('Components/textarea', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('TextArea', () => {
    const props = {
      align: select('align', ['left', 'right'], 'left'),
      autoFocus: boolean('autoFocus', false),
      maxLength: number('maxLength', 64),
      maxLengthErrorText: text('maxLengthErrorText', 'Max length exceeded'),
      rows: number('rows', 2),
      label: text('label', 'Label'),
      placeholder: text('placeholder', 'Placeholder'),
      showMaxLength: boolean('showMaxLength', false),
      error: boolean('error', false),
      helperText: text('helperText', ''),
      disabled: boolean('disabled', false),
      readOnly: boolean('readOnly', false),
      resizable: boolean('resizable', true),
      required: boolean('required', true),
    };

    const [value, setValue] = useState('');

    return (
      <div style={{padding: 50, backgroundColor: '#f1f1f1'}}>
        <TextArea
          onChange={(value) => {
            setValue(value);
            action('onChange')(value);
          }}
          value={value}
          {...props}
        />
      </div>
    );
  })
  .add('TextArea with max length limit', () => {
    const [value, setValue] = React.useState('');

    return (
      <div style={{padding: 50, backgroundColor: '#f1f1f1'}}>
        <TextArea
          onChange={(value) => {
            setValue(value);
            action('onChange')(value);
          }}
          value={value}
          required={true}
          resizable={true}
          maxLength={24}
          showMaxLength={true}
          maxLengthErrorText={'Max length exceeded'}
        />
      </div>
    );
  })
  .add('TextArea with ref and onBlur', () => {
    return (
      <div style={{margin: 50}}>
        <TextAreaWithBlur />
      </div>
    );
  })
  .add('With Layout Wrapper (max600min80), showing max width', () => {
    const [value, setValue] = useState('');

    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 800, padding: 40, background: '#f1f1f1'}}>
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
        </div>
      </LayoutContext.Provider>
    );
  })
  .add('With Layout Wrapper (max600min80), showing min width', () => {
    const [value, setValue] = useState('');

    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 60, padding: 40, background: '#f1f1f1'}}>
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
          <TextArea
            label={'With fixed width'}
            placeholder={'Placeholder'}
            value={value}
            onChange={setValue}
          />
        </div>
      </LayoutContext.Provider>
    );
  });
