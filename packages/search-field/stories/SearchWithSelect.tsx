import React from 'react';

import Search, {Option, SearchFieldProps} from '../src/components/SearchField';

export const searchOptions = [
  {
    label: 'Name',
    value: 'name',
  },
  {
    label: 'Email',
    value: 'email',
  },
  {
    label: 'Username',
    value: 'username',
  },
];

export const SearchWithSelect = ({
  minimizable = false,
  searchOptionsWidth,
}: Partial<SearchFieldProps>) => {
  const [state, setState] = React.useState<{
    value: string | number | Option | undefined;
    searchOption: string;
  }>({
    value: '',
    searchOption: searchOptions[0].value,
  });

  return (
    <div
      style={{
        padding: '50px',
        backgroundColor: '#f1f1f1',
      }}>
      <div style={{padding: '20px'}}>
        <Search
          minimizable={minimizable}
          onChange={(value) => {
            setState({value, searchOption: state.searchOption});
          }}
          onSearchOptionChange={(searchOption) =>
            setState({value: state.value, searchOption})
          }
          value={state.value}
          placeholder={'Search'}
          searchOption={state.searchOption}
          searchOptions={searchOptions}
          searchOptionsWidth={searchOptionsWidth}
        />
      </div>
    </div>
  );
};

export default SearchWithSelect;
