import React, {useState, useRef} from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {boolean, number, text} from '@storybook/addon-knobs';
import Search, {Option} from '../src/components/SearchField';
import {LayoutContext, InputWidth} from '../../contexts/src';
import Button from '../../button/src/index';
import {Dialog} from '../../dialog/src';
import SearchWithOptions, {
  searchOptions,
  refineSearchOption,
} from './SearchWithOptions';
import SearchWithSelect from './SearchWithSelect';

const ControlledSearch = (props) => {
  const [isOpen, onToggle] = React.useState(true);

  return <Search {...props} isOpen={isOpen} onToggle={onToggle} />;
};

storiesOf('Components/search-field', module)
  .add('Default: Minimized', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    return (
      <div
        style={{
          padding: '50px',
          backgroundColor: '#f1f1f1',
        }}>
        <div style={{padding: '20px'}}>
          <Search
            placeholder={text('Placeholder', 'Placeholder')}
            value={value}
            onChange={(val) => {
              setValue(val);
              action('onChange')(val);
            }}
          />
        </div>
      </div>
    );
  })
  .add('Controlled isOpen state', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    return (
      <div
        style={{
          padding: '50px',
          backgroundColor: '#f1f1f1',
        }}>
        <ControlledSearch
          placeholder={text('Placeholder', 'Placeholder')}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          value={value}
        />
      </div>
    );
  })
  .add('Uncontrolled, open from the start', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    return (
      <div style={{padding: '20px'}}>
        <Search
          defaultIsOpen={true}
          autoFocus={boolean('autoFocus', true)}
          disabled={boolean('disabled', false)}
          placeholder={text('Placeholder', 'Placeholder')}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          value={value}
        />
      </div>
    );
  })
  .add('Always Open', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    return (
      <div style={{padding: '20px'}}>
        <Search
          autoFocus={boolean('autoFocus', true)}
          minimizable={false}
          placeholder={text('Placeholder', 'Placeholder')}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          value={value}
        />
      </div>
    );
  })
  .add('In Modal Body: Minimized', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    return (
      <Dialog open={true}>
        <Dialog.Body>
          <Search
            autoFocus={boolean('autoFocus', true)}
            placeholder={text('Placeholder', 'Placeholder')}
            onChange={(val) => {
              setValue(val);
              action('onChange')(val);
            }}
            value={value}
          />
        </Dialog.Body>
      </Dialog>
    );
  })
  .add('In Modal Body: Always Open', () => {
    const [value, setValue] = useState<string | number | Option | undefined>(
      ''
    );
    return (
      <Dialog open={true}>
        <Dialog.Body>
          <Search
            autoFocus={boolean('autoFocus', true)}
            minimizable={false}
            placeholder={text('Placeholder', 'Placeholder')}
            onChange={(val) => {
              setValue(val);
              action('onChange')(val);
            }}
            value={value}
          />
        </Dialog.Body>
      </Dialog>
    );
  })
  .add('With search button', () => {
    const [value, setValue] = useState<string | number | Option | undefined>(
      ''
    );
    return (
      <div style={{padding: '20px'}}>
        <Search
          placeholder={text('Placeholder', 'Placeholder')}
          value={value}
          shouldSearchOnEnterKeyPress={true}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          onSearch={() => {
            console.log('search performed with value:', value);
          }}
        />
      </div>
    );
  })
  .add('Search with options: showing top N items', () => {
    const topN = 5;
    const onLoadOptions = (inputValue) => {
      const strToFind = (inputValue || '').toLowerCase();
      let filteredOptions = searchOptions.filter(({label}) =>
        label.toLowerCase().includes(strToFind)
      );

      if (filteredOptions.length > topN) {
        filteredOptions = [
          ...filteredOptions.slice(0, topN),
          refineSearchOption,
        ];
      }

      return Promise.resolve({
        options: filteredOptions,
        hasMore: false,
        additional: {
          page: 0,
        },
      });
    };

    const formatOptionLabel = (highlightedOption, option) =>
      option.isDisabled ? option.label : highlightedOption;

    return (
      <SearchWithOptions
        formatOptionLabel={formatOptionLabel}
        loadOptions={onLoadOptions}
      />
    );
  })
  .add('Search with options: lazy-loading of items', () => {
    const pageSize = 10;
    const onLoadOptions = (inputValue, previousOptions, {page}) => {
      const start = (page - 1) * pageSize;
      const strToFind = (inputValue || '').toLowerCase();
      const filteredOptions = searchOptions
        .filter(({label}) => label.toLowerCase().includes(strToFind))
        .slice(start, start + pageSize);

      return Promise.resolve({
        options: filteredOptions,
        hasMore: filteredOptions.length > 0,
        additional: {
          page: page + 1,
        },
      });
    };

    return <SearchWithOptions loadOptions={onLoadOptions} />;
  })
  .add('Search with field select: minimizable', () => (
    <SearchWithSelect minimizable={true} searchOptionsWidth={'125px'} />
  ))
  .add('Search with field select: always open', () => (
    <SearchWithSelect minimizable={false} searchOptionsWidth={'125px'} />
  ))
  .add('Search with field select with default select width', () => (
    <SearchWithSelect />
  ))
  .add('Search with custom maxLength', () => {
    const [value, setValue] = useState<undefined | string | number | Option>(
      ''
    );

    return (
      <div style={{padding: '20px'}}>
        <Search
          autoFocus={boolean('autoFocus', true)}
          minimizable={false}
          placeholder={text('Placeholder', 'Placeholder')}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          value={value}
          maxLength={number('maxLength', 10)}
          showMaxLength={boolean('showMaxLength', true)}
        />
      </div>
    );
  })
  .add('Focus with ref', () => {
    const [value, setValue] = React.useState<
      string | number | Option | undefined
    >('');

    const inputRef = useRef<HTMLInputElement | null>(null);

    return (
      <div style={{padding: '20px'}}>
        <Search
          autoFocus={false}
          minimizable={false}
          placeholder={text('Placeholder', 'Autofocus disabled')}
          ref={inputRef}
          onChange={(val) => {
            setValue(val);
            action('onChange')(val);
          }}
          value={value}
        />
        <Button
          onClick={() => {
            inputRef.current?.focus();
          }}>
          Focus search input
        </Button>
      </div>
    );
  })
  .add('With Layout Wrapper (max600min80), showing max width', () => {
    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 800, padding: 40, background: '#f1f1f1'}}>
          <ControlledSearch />
          <ControlledSearch />
          <ControlledSearch />
        </div>
      </LayoutContext.Provider>
    );
  })
  .add('With Layout Wrapper (max600min80), showing min width', () => {
    return (
      <LayoutContext.Provider value={{inputWidth: InputWidth.Max600Min80}}>
        <div style={{width: 60, padding: 40, background: '#f1f1f1'}}>
          <ControlledSearch />
          <ControlledSearch />
          <ControlledSearch />
        </div>
      </LayoutContext.Provider>
    );
  });
