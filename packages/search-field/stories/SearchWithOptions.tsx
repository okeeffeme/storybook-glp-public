import {boolean, text} from '@storybook/addon-knobs';
import {action} from '@storybook/addon-actions';
import React from 'react';

import Search, {Option, SearchFieldProps} from '../src/components/SearchField';

const letters = new Array(26)
  .fill(null)
  .map((v, idx) => (idx + 10).toString(36).toUpperCase());
const stringOfAlphabet = letters.join('');

const searchOptions = letters.map((v, idx) => ({
  label: stringOfAlphabet.substring(0, idx + 1),
  value: v,
}));

const refineSearchOption = {
  label: 'To see more, please refine your search',
  value: 'refine_search',
  isDisabled: true,
};

const SearchWithOptions = (props: Partial<SearchFieldProps>) => {
  const [value, setValue] = React.useState<
    string | number | Option | undefined
  >(undefined);

  return (
    <div style={{padding: '20px'}}>
      <Search
        autoFocus={boolean('autoFocus', true)}
        minimizable={false}
        placeholder={text('Placeholder', 'Placeholder')}
        onChange={(val) => {
          setValue(val);
          action('onChange')(val);
        }}
        value={value}
        isOptionDisabled={({isDisabled}) => !!isDisabled}
        {...props}
      />
    </div>
  );
};

export {searchOptions, refineSearchOption, SearchWithOptions};
export default SearchWithOptions;
