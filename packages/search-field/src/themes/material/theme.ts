import classNames from './css/SearchField.module.css';

export default {
  search: {
    baseClassName: 'comd-search',
    classNames,
  },
};
