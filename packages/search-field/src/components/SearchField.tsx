import React, {
  useEffect,
  useRef,
  useState,
  forwardRef,
  AriaAttributes,
  FocusEventHandler,
  ReactNode,
} from 'react';
import cn from 'classnames';
import {useUncontrolled} from 'uncontrollable';

import HighlightTerm from '@jotunheim/react-highlight-term';
import {Button, IconButton} from '@jotunheim/react-button';
import Icon, {magnify, arrowLeft} from '@jotunheim/react-icons';
import InputWrapper from '@jotunheim/react-input-wrapper';
import SimpleTextField from '@jotunheim/react-simple-text-field';
import SimpleSelect, {AsyncSelect} from '@jotunheim/react-simple-select';
import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {useTheme, bemFactory} from '@jotunheim/react-themes';

import ValueContainer from './ValueContainer';

import componentThemes from '../themes';

type SearchFieldThemeProps = {
  baseClassName: string;
  classNames: ClassNames;
};

export type Option = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
  value: number | string;
  label: number | string;
};

type AdditionalInfo = {
  inputValue: string;
  context: 'menu' | 'value';
  selectValue?: Option | Array<Option> | null;
};

type LoadedOptionsMetaData = {
  page: number;
};

type LoadedOptions = {
  options: Option[];
  hasMore: boolean;
  additional: LoadedOptionsMetaData;
};

type LoadOptionsFunction = (
  inputValue: string,
  previousOptions: Option[],
  options: LoadedOptionsMetaData
) => Promise<LoadedOptions>;

export type SearchFieldProps = {
  onChange?: (value?: string | number | Option) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  minimizable?: boolean;
  placeholder?: string;
  value?: string | number | Option;
  loadOptions?: LoadOptionsFunction;
  filterOption?: (option: Option, inputValue: string) => boolean;
  inputRef?: React.Ref<HTMLInputElement>;
  isOptionDisabled?: (option: Option) => boolean;
  formatOptionLabel?: (
    highlightedOption: ReactNode,
    option: Option,
    additionalInfo: AdditionalInfo
  ) => ReactNode;
  noOptionsMessage?: ({inputValue}: {inputValue: string}) => ReactNode | null;
  onFocus?: FocusEventHandler;
  onBlur?: FocusEventHandler;
  searchOption?: string;
  searchOptions?: Option[];
  onSearchOptionChange?: (value: string) => void;
  searchOptionsWidth?: string;
  shouldSearchOnEnterKeyPress?: boolean;
  searchButtonText?: string;
  onSearch?: () => void | undefined;
  maxLength?: number;
  showMaxLength?: boolean;
} & AriaAttributes;

type UncontrolledProps = {
  isOpen: boolean;
  onToggle: (isOpen: boolean) => void;
};

type ControlledSearchFieldProps = SearchFieldProps &
  Partial<UncontrolledProps> & {
    defaultIsOpen?: boolean;
  };

type UncontrolledSearchFieldProps = SearchFieldProps & UncontrolledProps;

const noopEventHandler = () => {};

export const SearchField = ({
  isOpen,
  autoFocus = true,
  minimizable = true,
  disabled = false,
  placeholder = '',
  value = '',
  filterOption,
  isOptionDisabled,
  noOptionsMessage,
  onChange = noopEventHandler,
  onFocus = noopEventHandler,
  onBlur = noopEventHandler,
  onToggle,
  formatOptionLabel,
  loadOptions,
  searchOption,
  searchOptions,
  onSearchOptionChange = noopEventHandler,
  searchOptionsWidth = '120px',
  shouldSearchOnEnterKeyPress = false,
  searchButtonText = 'Search',
  onSearch = noopEventHandler,
  maxLength,
  showMaxLength,
  inputRef,
  ...rest
}: UncontrolledSearchFieldProps) => {
  const {baseClassName, classNames} = useTheme<SearchFieldThemeProps>(
    'search',
    componentThemes
  );
  const {block, element} = bemFactory({baseClassName, classNames});

  const [active, setActive] = useState(false);

  const myInputRef = useRef<HTMLInputElement>(null);
  inputRef = inputRef || myInputRef;

  const hasValue = value != null && value !== '';
  const hasOptions = !!loadOptions;
  const hasSearchField =
    searchOption && searchOptions && searchOptions.length > 0;

  // Transition makes input lose focus
  useEffect(() => {
    if (
      autoFocus &&
      isOpen &&
      inputRef &&
      typeof inputRef === 'object' &&
      inputRef.current
    ) {
      inputRef.current.focus();
    }
  }, [autoFocus, isOpen, inputRef]);

  const handleOnToggle = () => onToggle(!isOpen);

  const searchClassNames = cn(block());
  const searchInputClassNames = cn(element('container'), {
    [element('container', 'visible')]: isOpen || !minimizable,
  });

  const handleFocus = (event) => {
    setActive(true);
    onFocus(event);
  };

  const handleBlur = (event) => {
    setActive(false);
    onBlur(event);
  };

  const handleKeyDown = (event) => {
    if (event.which === 27) {
      if (value) {
        onChange('');
      } else if (minimizable) {
        onToggle(false);
      }
    } else if (event.which === 13 && shouldSearchOnEnterKeyPress) {
      onSearch();
    }
  };

  const handleFormatOptionLabel = (
    option: Option,
    additionalInfo: AdditionalInfo
  ) => {
    const {inputValue} = additionalInfo;
    const highlightedOption = (
      <HighlightTerm searchTerm={inputValue} text={String(option.label)} />
    );

    return formatOptionLabel
      ? formatOptionLabel(highlightedOption, option, additionalInfo)
      : highlightedOption;
  };

  const handleClear = () => {
    onChange('');
    if (inputRef && typeof inputRef === 'object' && inputRef.current) {
      inputRef.current.focus();
    }
  };

  const renderPrefix = () => (
    <React.Fragment>
      {hasSearchField && (
        <div
          className={element('select')}
          data-testid="simple-select-search"
          data-value={searchOption}
          style={{
            width: searchOptionsWidth,
          }}>
          <SimpleSelect
            options={searchOptions}
            value={searchOption}
            isSearchable={false}
            onChange={onSearchOptionChange}
          />
          <div className={element('select-border')} />
        </div>
      )}
      <Icon path={magnify} />
    </React.Fragment>
  );

  const shouldShowSearchButton =
    shouldSearchOnEnterKeyPress && ((minimizable && isOpen) || !minimizable);

  return (
    <div
      className={searchClassNames}
      data-testid="search-field"
      {...extractDataAriaIdProps(rest)}>
      {minimizable && !isOpen && (
        <div
          className={element('toggle-icon')}
          data-testid="search-field-toggle-icon">
          <IconButton
            onClick={handleOnToggle}
            data-search-toggle-icon
            data-testid="search-toggle-icon-button">
            <Icon path={magnify} />
          </IconButton>
        </div>
      )}
      <div
        className={searchInputClassNames}
        data-testid="search-field-container">
        {minimizable && isOpen && (
          <div
            className={element('back-icon')}
            data-testid="search-field-back-icon">
            <IconButton
              onClick={handleOnToggle}
              data-search-back-icon
              data-testid="search-back-icon-button">
              <Icon path={arrowLeft} />
            </IconButton>
          </div>
        )}
        <div
          className={element('input-container')}
          data-testid="search-field-input-container">
          <InputWrapper
            transparentBackground={true}
            active={active}
            disabled={disabled}
            value={value}
            hasValue={hasValue}
            showBorder={active}
            showClear={!hasOptions && hasValue}
            prefix={!hasOptions && renderPrefix()}
            onClear={handleClear}
            maxLength={maxLength}
            showMaxLength={showMaxLength}>
            {hasOptions ? (
              <AsyncSelect
                value={value}
                autoFocus={autoFocus}
                isSearchable={true}
                isClearable={true}
                filterOption={filterOption}
                placeholder={placeholder}
                isOptionDisabled={isOptionDisabled}
                noOptionsMessage={noOptionsMessage}
                formatOptionLabel={handleFormatOptionLabel}
                onBlur={handleBlur}
                onFocus={handleFocus}
                onChange={onChange}
                components={{DropdownIndicator: null, ValueContainer}}
                loadOptions={loadOptions}
                disabled={disabled}
              />
            ) : (
              <SimpleTextField
                autoFocus={autoFocus}
                onChange={onChange}
                hasPrefix={true}
                hasSuffix={hasValue}
                placeholder={placeholder}
                onBlur={handleBlur}
                onFocus={handleFocus}
                onKeyDown={handleKeyDown}
                ref={inputRef}
                value={value as string}
                maxLength={maxLength}
                disabled={disabled}
              />
            )}
          </InputWrapper>
        </div>
        {shouldShowSearchButton && (
          <div className={element('search-button-wrapper')}>
            <Button
              disabled={disabled}
              appearance={Button.appearances.primaryNeutral}
              onClick={onSearch}
              data-testid="search-button">
              {searchButtonText}
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

const UncontrolledSearchField = forwardRef(
  function UncontrolledSearchFieldWrapper(
    props: ControlledSearchFieldProps,
    ref: React.Ref<HTMLInputElement>
  ) {
    const uncontrolledProps = useUncontrolled(props, {
      isOpen: 'onToggle',
    }) as UncontrolledSearchFieldProps;

    return <SearchField {...uncontrolledProps} inputRef={ref} />;
  }
);

export default UncontrolledSearchField;
