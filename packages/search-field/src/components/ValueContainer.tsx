import React, {CSSProperties} from 'react';
import {components} from '@jotunheim/react-simple-select';
import Icon, {magnify} from '@jotunheim/react-icons';

const searchIconStyles: CSSProperties = {
  display: 'flex',
  position: 'absolute',
  top: '50%',
  left: 12,
  transform: 'translateY(-50%)',
};

const ValueContainer = ({children, getStyles, ...props}) => {
  const getStylesOverridden = (...props) => {
    const styles = getStyles(...props);

    return {
      ...styles,
      paddingLeft: 42,
    };
  };

  return (
    <components.ValueContainer {...props} getStyles={getStylesOverridden}>
      {!!children && (
        <div style={searchIconStyles}>
          <Icon path={magnify} />
        </div>
      )}
      {children}
    </components.ValueContainer>
  );
};

export default ValueContainer;
