# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [9.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.39&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.40&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.39&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.39&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.37&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.38&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.36&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.37&targetRepoId=1246) (2023-03-29)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.35&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.36&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.34&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.35&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.33&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.34&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.32&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.33&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.31&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.32&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [9.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.30&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.31&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.29&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.30&targetRepoId=1246) (2023-02-27)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.28&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.29&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.27&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.28&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.26&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.27&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.25&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.26&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.24&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.25&targetRepoId=1246) (2023-02-07)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.23&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.24&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.22&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.23&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.21&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.22&targetRepoId=1246) (2023-01-31)

### Bug Fixes

- adding data-testids to SearchField component ([NPM-1207](https://jiraosl.firmglobal.com/browse/NPM-1207)) ([c15c51a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c15c51a729b566ef97cbedc2444387078437adad))

## [9.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.20&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.21&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.19&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.20&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.18&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.19&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.16&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.18&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.16&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.17&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.15&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.16&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.14&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.15&targetRepoId=1246) (2023-01-10)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.13&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.14&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.12&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.13&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.11&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.12&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.10&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.11&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.9&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.10&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.8&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.9&targetRepoId=1246) (2022-12-20)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.7&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.8&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.7&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.6&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.4&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.5&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.3&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.4&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.2&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.3&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-search-field

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@9.0.0&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.1&targetRepoId=1246) (2022-10-13)

### Bug Fixes

- fix HighlightTerm value type ([NPM-1099](https://jiraosl.firmglobal.com/browse/NPM-1099)) ([e750b40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e750b4072e5b9a990cffb4a9efce2d731d45b634))

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.17&sourceBranch=refs/tags/@jotunheim/react-search-field@9.0.0&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of IconButton from SearchField ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([4814c73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4814c736564379adb2c96e206200da8e84edfe91))

### Features

- remove className prop of SearchField ([NPM-948](https://jiraosl.firmglobal.com/browse/NPM-948)) ([cbd1784](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cbd178478f4bc30970c828de1181091f9e7fe350))

### BREAKING CHANGES

- As part of NPM-925 we remove className props from components.

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.16&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.17&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.15&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.16&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.14&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.15&targetRepoId=1246) (2022-09-15)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.13&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.14&targetRepoId=1246) (2022-09-13)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.12&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.13&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.11&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.12&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.10&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.11&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.9&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.10&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.7&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.8&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.4&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.5&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.3&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.4&targetRepoId=1246) (2022-08-09)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.2&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.1&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-search-field

## [8.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-search-field@8.0.0&sourceBranch=refs/tags/@jotunheim/react-search-field@8.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-search-field

# 8.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [7.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.39&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.40&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.38&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.39&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.37&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.38&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [7.2.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.36&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.37&targetRepoId=1246) (2022-05-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.35&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.36&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.34&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.35&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.32&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.33&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.31&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.32&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.30&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.31&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.29&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.30&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.28&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.29&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.27&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.28&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.24&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.25&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.23&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.24&targetRepoId=1246) (2022-01-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.22&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.23&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.21&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.22&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.20&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.21&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.19&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.20&targetRepoId=1246) (2021-12-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.18&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.19&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.17&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.18&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.16&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.17&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [7.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.15&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.16&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.13&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.14&targetRepoId=1246) (2021-10-07)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.12&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.13&targetRepoId=1246) (2021-09-30)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.11&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.12&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.10&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.11&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.9&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.10&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.8&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.9&targetRepoId=1246) (2021-09-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.7&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.8&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.6&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.7&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.5&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.6&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.4&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.5&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.3&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.4&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.2&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.3&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.1&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.2&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.2.0&sourceBranch=refs/tags/@confirmit/react-search-field@7.2.1&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-search-field

## 7.2.0

- ability to forward ref to input field

## [7.1.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.29&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.30&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.28&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.29&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.27&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.28&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.26&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.27&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.25&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.26&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.24&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.25&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.23&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.24&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.22&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.23&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.21&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.22&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.20&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.21&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.19&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.20&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.18&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.19&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.16&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.17&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.15&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.16&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.14&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.15&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.13&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.14&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.12&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.13&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [7.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.11&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.12&targetRepoId=1246) (2021-04-07)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.10&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.11&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.9&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.10&targetRepoId=1246) (2021-03-26)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.8&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.9&targetRepoId=1246) (2021-03-25)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.7&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.8&targetRepoId=1246) (2021-03-24)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.6&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.7&targetRepoId=1246) (2021-03-19)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.5&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.6&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.4&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.3&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.4&targetRepoId=1246) (2021-03-16)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.2&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.3&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.1&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.2&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.1.0&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.1&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-search-field

# [7.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.27&sourceBranch=refs/tags/@confirmit/react-search-field@7.1.0&targetRepoId=1246) (2021-03-03)

### Features

- add ability to disable search field ([NPM-735](https://jiraosl.firmglobal.com/browse/NPM-735)) ([ed54c2e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ed54c2e327f016ff14e763af6044045610d4dbea))

## [7.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.26&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.27&targetRepoId=1246) (2021-02-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.25&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.26&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.24&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.25&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.23&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.24&targetRepoId=1246) (2021-02-12)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.21&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.22&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.20&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.21&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.19&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.20&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.18&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.19&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.17&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.18&targetRepoId=1246) (2021-01-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.16&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.17&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.15&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.16&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.14&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.15&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.13&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.14&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.12&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.11&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.10&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.11&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.9&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.6&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.3&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.2&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-search-field

## [7.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@7.0.1&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-search-field

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@6.0.1&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.0&targetRepoId=1246) (2020-11-13)

### chore

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([85d1fc9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d1fc9964d98f708850fc1e7bf3b5fef9914968))

### Features

- add properties maxValue, showMaxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([058856e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/058856ef4974557e4e3c19b41327a973df8831bb))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@6.0.1&sourceBranch=refs/tags/@confirmit/react-search-field@7.0.0&targetRepoId=1246) (2020-11-13)

### Features

- add properties maxValue, showMaxLength ([NPM-592](https://jiraosl.firmglobal.com/browse/NPM-592)) ([058856e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/058856ef4974557e4e3c19b41327a973df8831bb))

### BREAKING CHANGES

- remove default value of maxLength property of @confirmit/react-input-wrapper, @confirmit/react-simple-text-field. It affects any text input used by this package. (NPM-592)

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.11&sourceBranch=refs/tags/@confirmit/react-search-field@6.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [5.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.10&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.11&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.9&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.10&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.8&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.9&targetRepoId=1246) (2020-10-26)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.7&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.8&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.4&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.5&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.3&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.4&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.1.1&sourceBranch=refs/tags/@confirmit/react-search-field@5.1.2&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-search-field

## v5.1.0

- Feat: Add optional search button
- New props added:
  - shouldSearchOnEnterKeyPress
  - searchButtonText
  - onSearch

## [5.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.0.38&sourceBranch=refs/tags/@confirmit/react-search-field@5.0.39&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.0.37&sourceBranch=refs/tags/@confirmit/react-search-field@5.0.38&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.0.35&sourceBranch=refs/tags/@confirmit/react-search-field@5.0.36&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-search-field@5.0.33&sourceBranch=refs/tags/@confirmit/react-search-field@5.0.34&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.32](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-search-field@5.0.31...@confirmit/react-search-field@5.0.32) (2020-09-08)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.30](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-search-field@5.0.29...@confirmit/react-search-field@5.0.30) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.27](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-search-field@5.0.26...@confirmit/react-search-field@5.0.27) (2020-08-21)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.23](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-search-field@5.0.22...@confirmit/react-search-field@5.0.23) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-search-field

## [5.0.22](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-search-field@5.0.21...@confirmit/react-search-field@5.0.22) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-search-field

## CHANGELOG

### v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v4.2.4

- Typescript fix for `onChange` function

### v4.2.2

- Typescript fixes:
  - `LoadOptionsFunction` returns a promise
  - `value` property type is extended - it corresponds to the `@confirmit/react-select` value type
  - use `useUncontrolled` hook to preserve types of the exported component
  - a fix in the storybook to prevent instant list reloading

### v4.2.0

- Feat: Search with selector variation
- New props added:
  - searchOption
  - searchOptions
  - onSearchOptionChange
  - searchFieldWidth
- Selector is controlled and displays when searchOption and searchOptions are present

### v4.1.0

- Removing package version in class names.

### v4.0.0

- **BREAKING**:
  - Make block element take up 100% width to align with other input field style architecture.

### v3.1.0

- Feat: Escape will clear value, and close field when minimizable is enabled

### v3.0.0

- **BREAKING**:

  - Search field open state can now be controlled or uncontrolled
  - new props added:
    - defaultIsOpen (uncontrolled)
    - isOpen (controlled)
    - onToggle
  - prop renamed:
    - minimize => minimizable

### v2.1.2

- Fix bug with exporting types

### v2.1.0

- Typescript support
- Search field can now accept `loadOptions` property that allow to pass options asynchronously
- New props added, mostly relate to option list modification (for more info, check react-select props api):
  - filterOption,
  - isOptionDisabled,
  - noOptionsMessage,
  - formatOptionLabel,
  - loadOptions,
  - onFocus,
  - onBlur

### v2.0.0

- **BREAKING**:

  - Updated to follow new DS specs by virtue of using the new InputWrapper component in combination with SimpleTextField.
  - Removed default theme styling.

### v1.1.0

- Remove passing in borderClassName to TextField as it's no longer supported.
- Adjust Icon being passed in as prefix.
- Updated to new DS specs due to TextField updates.

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v0.0.8

Fix: Updated react-text-field, making use of showClear prop

### v0.0.5

Feat: Add some left-padding for the search icon

### v0.0.1

- Initial version
