import React from 'react';
import PropTypes from 'prop-types';
import {fireEvent, render, screen} from '@testing-library/react';
import {theme, ThemeContext} from '../../../themes/src/index';
import SearchField from '../../src/components/SearchField';
import {searchOptions} from '../../stories/SearchWithSelect';
import user from '@testing-library/user-event';

const themeContextWrapper = ({children}) => (
  <ThemeContext.Provider value={theme.themeNames.material}>
    {children}
  </ThemeContext.Provider>
);

const renderSearchField = (props) => {
  return render(<SearchField {...props} />);
};

themeContextWrapper.propTypes = {children: PropTypes.any};

describe('Jotunheim React Search :: ', () => {
  it('renders always open', () => {
    const onChange = jest.fn();

    const {rerender} = render(
      <SearchField minimizable={false} value="" onChange={onChange} />
    );
    expect(screen.getByTestId('search-field')).toHaveClass('comd-search');
    expect(screen.getByTestId('search-field-container')).toHaveClass(
      'comd-search__container--visible'
    );
    expect(
      screen.queryByTestId('search-field-back-icon')
    ).not.toBeInTheDocument();
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 't'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'te'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'tes'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'test'},
    });

    expect(onChange).toHaveBeenCalledTimes(4);

    rerender(
      <SearchField onChange={onChange} minimizable={false} value="test" />
    );
    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      'test'
    );
  });

  it('should disable input when disabled', () => {
    const onChange = jest.fn();

    renderSearchField({onChange, disabled: true, value: ''});
    expect(screen.getByTestId('simple-text-field')).toBeDisabled();
  });

  it('should not disable input when not disabled', () => {
    const onChange = jest.fn();

    renderSearchField({onChange, disabled: false, value: ''});

    expect(screen.getByTestId('simple-text-field')).not.toBeDisabled();
  });

  it('renders minimized', () => {
    const onChange = jest.fn();
    const {rerender} = render(<SearchField value="" onChange={onChange} />);

    expect(screen.getByTestId('search-field')).toHaveClass('comd-search');
    expect(screen.getByTestId('search-field-toggle-icon')).toHaveClass(
      'comd-search__toggle-icon'
    );
    expect(screen.queryByTestId('search-field-container')).not.toHaveClass(
      'comd-search__container--visible'
    );
    expect(
      screen.queryByTestId('search-field-back-icon')
    ).not.toBeInTheDocument();

    user.click(screen.getByTestId('search-toggle-icon-button'));

    expect(screen.getByTestId('search-field-container')).toHaveClass(
      'comd-search__container--visible'
    );
    expect(
      screen.queryByTestId('search-field-toggle-icon')
    ).not.toBeInTheDocument();
    expect(screen.getByTestId('search-field-back-icon')).toHaveClass(
      'comd-search__back-icon'
    );

    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 't'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'te'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'tes'},
    });
    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'test'},
    });

    expect(onChange).toHaveBeenCalledTimes(4);

    rerender(<SearchField value="test" onChange={onChange} />);
    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      'test'
    );

    user.click(screen.getByTestId('search-back-icon-button'));

    expect(screen.queryByTestId('search-field-container')).not.toHaveClass(
      'comd-search__container--visible'
    );
    expect(screen.getByTestId('search-field-toggle-icon')).toHaveClass(
      'comd-search__toggle-icon'
    );
    expect(
      screen.queryByTestId('search-field-back-icon')
    ).not.toBeInTheDocument();
  });

  it('clearing input should keep focus', () => {
    const onChange = jest.fn();
    const {rerender} = render(
      <SearchField onChange={onChange} minimizable={false} value="" />
    );

    expect(screen.getByTestId('search-field')).toHaveClass('comd-search');

    fireEvent.change(screen.getByTestId('simple-text-field'), {
      target: {value: 'test'},
    });

    expect(onChange).toHaveBeenCalledTimes(1);

    rerender(
      <SearchField onChange={onChange} minimizable={false} value="test" />
    );
    expect(screen.getByTestId('simple-text-field')).toHaveAttribute(
      'value',
      'test'
    );

    user.click(screen.getByRole('button'));
    expect(onChange).toHaveBeenCalledWith('');
    expect(screen.getByTestId('simple-text-field')).toHaveFocus();
  });

  it('renders text-field when no options provided', () => {
    const onChange = jest.fn();
    renderSearchField({onChange, minimize: false, value: ''});

    expect(screen.getAllByTestId('simple-text-field')).toHaveLength(1);
    expect(screen.queryAllByTestId('react-select')).toHaveLength(0);
  });

  it('renders simple-select when loadOptions is passed', () => {
    const onChange = jest.fn();
    const loadOptions = jest.fn();
    renderSearchField({
      onChange,
      loadOptions,
      minimize: false,
      value: '',
    });
    expect(screen.getAllByTestId('react-select')).toHaveLength(1);
    expect(screen.queryAllByTestId('simple-text-field')).toHaveLength(0);
  });

  it('onToggle should trigger on maximized and minimized', () => {
    const onToggle = jest.fn();
    renderSearchField({onToggle, value: ''});

    user.click(screen.getByTestId('search-toggle-icon-button'));
    expect(onToggle.mock.calls[0][0]).toBe(true);

    user.click(screen.getByRole('button'));
    expect(onToggle.mock.calls[1][0]).toBe(false);
  });

  it('onChange should trigger with empty value on Escape key if value is set', () => {
    const onChange = jest.fn();
    const onToggle = jest.fn();
    renderSearchField({onChange, onToggle, value: 'test'});

    fireEvent.keyDown(screen.getByRole('textbox'), {
      key: 'Escape',
      keyCode: 27,
    });
    expect(onChange.mock.calls[0][0]).toBe('');
    expect(onToggle).not.toHaveBeenCalled();
  });

  it('onToggle should trigger with false on Escape key if no value', () => {
    const onChange = jest.fn();
    const onToggle = jest.fn();
    renderSearchField({onToggle, onChange, value: ''});

    // Maximize search
    fireEvent.keyDown(screen.getByRole('textbox'), {
      key: 'Escape',
      keyCode: 27,
    });

    expect(onChange).not.toHaveBeenCalled();
    expect(onToggle.mock.calls[0][0]).toBe(false);
  });

  it('onToggle should not trigger with false on Escape key if no value, when not minimizable', () => {
    const onChange = jest.fn();
    const onToggle = jest.fn();
    renderSearchField({
      onToggle,
      onChange,
      value: '',
      minimizable: false,
    });

    fireEvent.keyDown(screen.getByRole('textbox'), {
      key: 'Escape',
      keyCode: 27,
    });

    expect(onChange).not.toHaveBeenCalled();
    expect(onToggle).not.toHaveBeenCalled();
  });

  it('should not render search field selector when select props are not defined', () => {
    renderSearchField({
      minimizable: false,
      onChange: jest.fn(),
      value: '',
      placeholder: 'Search',
    });
    expect(screen.getByTestId('search-field')).toHaveClass('comd-search');
    expect(
      screen.queryByTestId('simple-select-search')
    ).not.toBeInTheDocument();
  });

  it('should render search field selector when select props defined', () => {
    const onSearchOptionChange = jest.fn();
    const onChange = jest.fn();
    const {rerender} = render(
      <SearchField
        minimizable={false}
        onChange={onChange}
        value=""
        searchOption="name"
        onSearchOptionChange={onSearchOptionChange}
        searchOptions={searchOptions}
        searchOptionsWidth="126px"
        placeholder="Search"
      />
    );
    expect(screen.getByTestId('search-field')).toHaveClass('comd-search');
    expect(screen.getByTestId('simple-select-search')).toBeInTheDocument();

    rerender(
      <SearchField
        minimizable={false}
        onChange={onChange}
        value=""
        searchOption={searchOptions[2].value}
        onSearchOptionChange={onSearchOptionChange}
        searchOptions={searchOptions}
        searchOptionsWidth="126px"
        placeholder="Search"
      />
    );
    expect(screen.getByTestId('simple-select-search')).toHaveAttribute(
      'data-value',
      searchOptions[2].value
    );
  });

  it('onSearch should trigger only when not minimized and shouldSearchOnEnterKeyPress is true', () => {
    const onToggle = jest.fn();
    const onSearch = jest.fn();
    const {rerender} = render(
      <SearchField
        onToggle={onToggle}
        onSearch={onSearch}
        shouldSearchOnEnterKeyPress={true}
        value=""
      />
    );
    expect(screen.queryByTestId('search-button')).not.toBeInTheDocument();

    user.click(screen.getByTestId('search-toggle-icon-button'));

    expect(screen.getByTestId('search-button')).toBeInTheDocument();

    rerender(
      <SearchField
        onToggle={onToggle}
        onSearch={onSearch}
        shouldSearchOnEnterKeyPress={true}
        value="test"
      />
    );
    fireEvent.keyDown(screen.getByRole('textbox'), {keyCode: 13});

    expect(onSearch).toHaveBeenCalledTimes(1);
  });

  it('onSearch should trigger on Enter when not minimized, shouldSearchOnEnterKeyPress is true, and non-empty value ', () => {
    const onSearch = jest.fn();

    const {rerender} = render(
      <SearchField
        onSearch={onSearch}
        shouldSearchOnEnterKeyPress={true}
        value="test"
      />
    );
    rerender(
      <SearchField
        onSearch={onSearch}
        shouldSearchOnEnterKeyPress={true}
        value="test"
        minimizable={false}
      />
    );

    expect(screen.getByTestId('search-button')).toBeInTheDocument();
    fireEvent.keyDown(screen.getByTestId('simple-text-field'), {keyCode: 13});
    expect(onSearch).toHaveBeenCalledTimes(1);
  });

  it('onSearch should not trigger when shouldSearchOnEnterKeyPress is false', () => {
    const onSearch = jest.fn();
    renderSearchField({
      onSearch,
      minimizable: false,
      value: 'test',
    });

    fireEvent.keyDown(screen.getByRole('textbox'), {keyCode: 13});
    expect(screen.queryByTestId('search-button')).not.toBeInTheDocument();
    expect(onSearch).not.toBeCalled();
  });

  it('should allow onSearch to trigger with empty search field', () => {
    const onSearch = jest.fn();
    renderSearchField({
      onSearch,
      minimizable: false,
      value: '',
      shouldSearchOnEnterKeyPress: true,
    });
    fireEvent.keyDown(screen.getByRole('textbox'), {keyCode: 13});

    expect(onSearch).toHaveBeenCalledTimes(1);
  });
});
