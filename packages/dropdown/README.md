# Jotunheim React Dropdown

`Dropdown` is based on `PortalTrigger` component, so check its [README](../portal-trigger/README.md).

### Changelog

[Changelog](./CHANGELOG.md)
