# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.1.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.25&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.26&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.25&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.25&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.23&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.24&targetRepoId=1246) (2023-04-03)

### Bug Fixes

- made the dropdown item be full width when it is inside another dropdown ([NPM-1279](https://jiraosl.firmglobal.com/browse/NPM-1279)) ([b4e8e9e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b4e8e9ef0a35080ba6c0d785bb60e850f4d030c1))

## [10.1.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.22&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.23&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.21&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.22&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.20&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.21&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.19&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.20&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.18&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.19&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.17&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.18&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.16&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.17&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.15&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.16&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.14&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.15&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.13&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.14&targetRepoId=1246) (2023-01-31)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.12&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.13&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.11&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.12&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.10&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.11&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.8&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.10&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.8&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.9&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.7&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.8&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.6&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.7&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.5&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.6&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.3&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.5&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.3&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.4&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.0&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.3&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.0&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.2&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.1.0&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.1&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-dropdown

# [10.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.18&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.1.0&targetRepoId=1246) (2022-11-23)

### Features

- export DropdownMenuProps props ([NPM-1102](https://jiraosl.firmglobal.com/browse/NPM-1102)) ([0e61248](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0e61248094f983bfa44bdcee8636f5e038bbb6c7))

## [10.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.17&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.16&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.14&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.13&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.14&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.12&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.11&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.10&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.11&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([fff3d78](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fff3d78fdac975e4caf84fcbe0caa3f11dbbb3f3))

## [10.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.9&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.10&targetRepoId=1246) (2022-09-08)

### Bug Fixes

- update uncontrollable to 7.2.1 version ([NPM-1028](https://jiraosl.firmglobal.com/browse/NPM-1028)) ([db75a6c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db75a6c621cc1a578de0c444aed18bec1a2a7ae0))

## [10.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.8&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.6&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.3&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.2&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.1&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-dropdown

## [10.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-dropdown@10.0.0&sourceBranch=refs/tags/@jotunheim/react-dropdown@10.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-dropdown

# 10.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [9.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@9.0.1&sourceBranch=refs/tags/@confirmit/react-dropdown@9.0.2&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-dropdown

## [9.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@9.0.0&sourceBranch=refs/tags/@confirmit/react-dropdown@9.0.1&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-dropdown

# [9.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.11&sourceBranch=refs/tags/@confirmit/react-dropdown@9.0.0&targetRepoId=1246) (2022-06-21)

API is changed and simplified.

- There is no longer a MenuLinkItem and a MenuTextItem, there is instead just a MenuItem.
  If a href is set on the MenuItem, then it be displayed as an anchor.

- A new CustomMenuItem is introduced, for the scenarios where we want Switches etc inside menu items.

```jsx
<Dropdown
  menu={
    <Dropdown.Menu>
      <Dropdown.MenuItem />
      <Dropdown.MenuItem href="/some-url/" />
      <Dropdown.CustomMenuItem>
        <Switch onChange={handleChange} />
      </Dropdown.CustomMenuItem>
    </Dropdown.Menu>
  }>
  <Button>Click</Button>
</Dropdown>
```

### BREAKING CHANGES

- remove all className props
- highlight on over and close on click is on by default for all MenuItem usages (NPM-972)
- Remove support for closeOnPortalClick. (NPM-972)

## [8.5.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.10&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.11&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [8.5.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.9&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.10&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.8&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.9&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.6&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.7&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.5&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.6&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.4&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.5&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.3&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.4&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.5.2&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.3&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-dropdown

# 8.5.1

### Bug Fixes

- handle clicks directly on LI elements to avoid complexity around event propagation ([SVD-1881](https://jiraosl.firmglobal.com/browse/SVD-1881)) ([12b59dc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/12b59dc94aa8e2250a1fc92b1bd1841aa1106124))
- stop propagation when click on dropdown option ([SVD-1881](https://jiraosl.firmglobal.com/browse/SVD-1881)) ([db9f1c8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/db9f1c8a6271d35918f572befa13efdef00299f1))

# [8.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.12&sourceBranch=refs/tags/@confirmit/react-dropdown@8.5.0&targetRepoId=1246) (2022-02-03)

### Features

- new prop to conditionally close menu items, instead of always closing when click is from within a link ([NPM-923](https://jiraosl.firmglobal.com/browse/NPM-923)) ([246907f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/246907fbb9e4029b780041e81bf8fcfc6bdfce6a))

## [8.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.11&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.12&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.10&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.11&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.9&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.10&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.8&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.9&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.7&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.8&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.6&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.7&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.5&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.6&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [8.4.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.4&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.5&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.2&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.1&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.2&targetRepoId=1246) (2021-09-14)

### Bug Fixes

- update Dropdown.Divider spacing to match Design System spec ([NPM-559](https://jiraosl.firmglobal.com/browse/NPM-559)) ([80777a6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/80777a675ae35d71595208c20cd004189bf38828))
- update Dropdown.Header style to match Design System spec ([NPM-559](https://jiraosl.firmglobal.com/browse/NPM-559)) ([5921e58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5921e5881525a2306d2191ec2e28570a131329ac))

## [8.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.4.0&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.1&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-dropdown

# [8.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.8&sourceBranch=refs/tags/@confirmit/react-dropdown@8.4.0&targetRepoId=1246) (2021-09-13)

### Bug Fixes

- do not call onClick on MenuTextItem when item is disabled, but allow pointer-events to go through so tooltips can be displayed on disabled items ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([7b93932](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7b9393223ab1d8e734b2270d01203f6f223c1a9a))
- layout of MenuLinkItem with iconPath was incorrect ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([e4c0c31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e4c0c310562cbffbd92847cada64eca1398c21da))

### Features

- support iconPath on SubMenuTriggerItem ([NPM-820](https://jiraosl.firmglobal.com/browse/NPM-820)) ([9237b17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9237b17ff4bdbe779d5da533917be765aeb42d10))

## [8.3.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.7&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.8&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.6&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.7&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.5&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.6&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.4&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.5&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.3&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.4&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.2&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.3&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.1&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.2&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.3.0&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.1&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-dropdown

# [8.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.19&sourceBranch=refs/tags/@confirmit/react-dropdown@8.3.0&targetRepoId=1246) (2021-07-07)

### Features

- support iconPath in MenuTextItem and MenuLinkItem ([712d6da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/712d6da4629ac78c823ffc5af845e2d33ce61fff))

## [8.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.18&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.19&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.17&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.18&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.16&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.17&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.15&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.16&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.14&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.15&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.13&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.14&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.12&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.13&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.11&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.12&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.10&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.11&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.8&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.9&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.7&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.8&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.6&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.7&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [8.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.5&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.6&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.4&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.5&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.3&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.4&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.2&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.3&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.1&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.2&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.2.0&sourceBranch=refs/tags/@confirmit/react-dropdown@8.2.1&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-dropdown

# [8.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.19&sourceBranch=refs/tags/@confirmit/react-dropdown@8.1.0&targetRepoId=1246) (2021-02-11)

### Features

- Dropdown.Menu now supports a ref (now using forwardRef)

# [8.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.19&sourceBranch=refs/tags/@confirmit/react-dropdown@8.1.0&targetRepoId=1246) (2021-02-10)

### Features

- support sub-menus in dropdown ([NPM-454](https://jiraosl.firmglobal.com/browse/NPM-454)) ([2e29b34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2e29b348cfde68fe21e4c7ea6224c7cd0fef5558))

## [8.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.18&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.19&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.17&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.18&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.16&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.17&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.15&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.16&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.14&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.15&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.13&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.14&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.12&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.13&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.11&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.12&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.10&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.11&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.9&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.10&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.6&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.7&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.3&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.4&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.2&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.3&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-dropdown

## [8.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@8.0.1&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.2&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-dropdown

# [8.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@7.0.1&sourceBranch=refs/tags/@confirmit/react-dropdown@8.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- use SimpleLink as link component. ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([70c6fc6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/70c6fc66e591517275e65be695c787ec215a9841))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- SimpleLink requires @confirmit/react-contexts to be installed

## [7.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@7.0.0&sourceBranch=refs/tags/@confirmit/react-dropdown@7.0.1&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-dropdown

## [7.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.20&sourceBranch=refs/tags/@confirmit/react-dropdown@7.0.0&targetRepoId=1246) (2020-10-26)

### Features

- bind portal position to viewport instead of scroll parent ([NPM-581](https://jiraosl.firmglobal.com/browse/NPM-581)) ([f7e2dd3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f7e2dd37ddd8e9b0f9682b34ed96b0167b0fe19a))

### BREAKING CHANGES

- There are no api changes, but change of how portal trigger tooltip is rendered.
  It now uses viewport instead scroll parent to determine its position, which in most cases a desired behaviour.
  If you need to use the old behaviour you can add:

```
const flipModifier: StrictModifiers = {
  name: 'flip',
  options: {
    altBoundary: true,
  },
};
const modifiers = useMemo(() => { return [flipModifier]}, [flipModifier])
return <Dropdown modifiers={modifiers} />
```

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.17&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.18&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.16&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.17&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.15&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.16&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.12&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.13&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.10&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.11&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-dropdown@6.0.8&sourceBranch=refs/tags/@confirmit/react-dropdown@6.0.9&targetRepoId=1246) (2020-09-10)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.6](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-dropdown@6.0.5...@confirmit/react-dropdown@6.0.6) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-dropdown

## [6.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-dropdown@6.0.0...@confirmit/react-dropdown@6.0.1) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-dropdown

# [6.0.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-dropdown@5.0.13...@confirmit/react-dropdown@6.0.0) (2020-08-12)

### Features

- rework dropdown to align PortalTrigger consumers api ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([ba9250c](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/ba9250c08e2d23c9d6adb46116c79ca2f99d4896))

### BREAKING CHANGES

- - Dropdown`API is aligned with other`PortalTrigger`consumer components, like`Tooltip`and`Popover`.Child of the dropdown behave as toggle element.`Dropdown` has menu property.

* Dropdown.popperModifiers`replaced with`Dropdown.modifiers` property.
  See https://popper.js.org/docs/v2/modifiers/.
* Dropdown.Menu` element should be passed to Dropdown.menu property (it was
  children previously).
* Children element is now considered as "toggle" component and must support `ref`.
* Dropdown.Toggle`component has been removed. Any component which support`ref` can be "toggle" component.

```jsx
<Dropdown
  menu={
    <Dropdown.Menu>
      <Dropdown.MenuTextItem />
      <Dropdown.MenuLinkItem />
    </Dropdown.Menu>
  }>
  <Button>Click</Button>
</Dropdown>
```

- MenuDivider does not pass down `...rest` properties except for data- and aria- attributes.
- MenuHeader does not pass down `...rest` properties except for data- and aria- attributes.
- MenuTextItem does not pass down `...rest` properties except for data- and aria- attributes.
- MenuLinkItem does not pass down `...rest` properties except for data- and aria- attributes.
- MenuLinkItem assigns data- and aria- attributes to `li` element
  instead of `a` (aligning with other menu items).

## CHANGELOG

### v5.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v4.1.0

- Removing package version in class names.

### v4.0.7

- Fix: upgrade uncontrollable, and make it local dep, to remove warnings about deprecated code

### v4.0.6

- Fix: Add prop-type for onClick on DropdownMenu, to remove warning
- Fix: Remove invalid props warning for DropdownToggle, since we cannot know which props to support due to componentClass approach

### v4.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.

### v3.0.2

- Fix: Max height set to 50vh for the dropdown menu. If there are many options in the list, it will now start to scroll when exceeding 50vh.

### v3.0.0

- **BREAKING** - Removed props: baseClassName, classNames

      	- DropdownMenu changed to function component (no longer supports ref)

      	- DropdownToggle changed to function component (no longer supports ref)

      	- MenuDivider changed to function component (no longer supports ref)
      	- MenuDivider, removed props: baseClassName, classNames

      	- MenuHeader changed to function component (no longer supports ref)
      	- MenuHeader, removed props: baseClassName, classNames

      	- MenuLinkItem changed to function component (no longer supports ref)
      	- MenuLinkItem, removed props: baseClassName, classNames, checkIcon

      	- MenuTextItem changed to function component (no longer supports ref)
      	- MenuTextItem, removed props: baseClassName, classNames

### v2.0.0

- **BREAKING**
  - Removed `onPlacementChange` prop
  - Removed `defaultPlacement` prop - you should use `placement` prop instead.

### v1.0.0

- **BREAKING**
  - Removed deprecated prop "theme" (this removes the "undefined" className on the ul tags)

### v0.2.0

- Feat: Add `disabled` prop to `MenuLinkItem` and `MenuTextItem`. In `MenuLinkItem`, `onClick` callback will not be called when `disabled` prop is `true`.

### v0.1.1

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
