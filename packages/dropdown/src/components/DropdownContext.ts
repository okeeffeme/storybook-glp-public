import {createContext} from 'react';

interface DropdownContext {
  itemClicked: (closeOnClick: boolean) => void;
}

const context = createContext<DropdownContext>({
  itemClicked: () => {},
});

export default context;
