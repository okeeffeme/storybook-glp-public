import React, {MouseEventHandler, ReactNode, useContext} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Icon, {check} from '@jotunheim/react-icons';
import SimpleLink from '@jotunheim/react-simple-link';

import DropdownContext from './DropdownContext';
import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

const preventDefault: MouseEventHandler = (e) => {
  e.stopPropagation();
  e.preventDefault();
};

interface MenuItemProps {
  href?: string;
  target?: string;
  onClick?: MouseEventHandler;
  iconPath?: string;
  children: ReactNode;
  selectable?: boolean;
  selected?: boolean;
  disabled?: boolean;
  closeOnClick?: boolean;
}

const {element} = bemFactory({baseClassName: baseMenuClassName, classNames});

export const MenuItem = ({
  href,
  target,
  onClick,
  iconPath,
  children,
  selectable = false,
  selected = false,
  disabled = false,
  closeOnClick = true,
  ...rest
}: MenuItemProps) => {
  const {itemClicked} = useContext(DropdownContext);
  const isLink = !!href;

  const liClasses = cn(element('li'), {
    [element('li', 'disabled')]: disabled,
  });

  const itemClasses = cn(element('item'), {
    [element('item', 'selected')]: selected,
  });

  const onItemClick = (e) => {
    onClick?.(e);

    // this enables consumers to prevent closing the menu items when clicked by calling e.preventDefault
    if (!e.defaultPrevented) {
      itemClicked(closeOnClick);
    }
  };

  const content = (
    <>
      {selectable && (
        <Icon
          data-testid="dropdown-menu-item__selectable"
          path={check}
          className={element('selected-icon')}
          size="16"
        />
      )}
      {iconPath && (
        <span
          className={element('item-icon')}
          data-testid="dropdown-menu-item__icon">
          <Icon path={iconPath} size={16} />
        </span>
      )}
    </>
  );

  return (
    <li
      data-testid="dropdown-menu-item"
      className={liClasses}
      {...extractDataAndAriaProps(rest)}
      onClick={disabled ? preventDefault : onItemClick}>
      {isLink ? (
        <SimpleLink
          data-testid="dropdown-menu-item__anchor"
          target={target}
          className={itemClasses}
          href={href}>
          {content}
          {children}
        </SimpleLink>
      ) : (
        <span className={itemClasses} data-testid="dropdown-menu-item__text">
          {content}
          {children}
        </span>
      )}
    </li>
  );
};

export default MenuItem;
