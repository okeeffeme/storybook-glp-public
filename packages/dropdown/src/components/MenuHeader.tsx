import React, {ReactNode} from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

interface MenuHeaderProps {
  children: ReactNode;
}

const {element} = bemFactory({baseClassName: baseMenuClassName, classNames});

export const MenuHeader = ({children, ...rest}: MenuHeaderProps) => {
  return (
    <li
      data-testid="dropdown-menu-header"
      className={element(`li`)}
      {...extractDataAndAriaProps(rest)}>
      <div className={element(`header`)}>{children}</div>
    </li>
  );
};

export default MenuHeader;
