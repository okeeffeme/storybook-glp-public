import React, {ForwardedRef, ReactNode} from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

export interface DropdownMenuProps {
  children: ReactNode;
}

export const DropdownMenu: ForwardRefDropdownMenu = React.forwardRef(
  (
    {children, ...rest}: DropdownMenuProps,
    ref: ForwardedRef<HTMLUListElement>
  ) => {
    const {block} = bemFactory({baseClassName: baseMenuClassName, classNames});

    return (
      <ul
        data-testid="dropdown-menu-items"
        ref={ref}
        className={block()}
        {...extractDataAndAriaProps(rest)}>
        {children}
      </ul>
    );
  }
) as ForwardRefDropdownMenu;

type ForwardRefDropdownMenu = React.ForwardRefExoticComponent<
  DropdownMenuProps & React.RefAttributes<HTMLElement>
>;

export default DropdownMenu;
