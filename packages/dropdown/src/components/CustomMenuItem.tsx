import React, {ReactNode, useContext} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import DropdownContext from './DropdownContext';
import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

interface CustomMenuItemProps {
  children: ReactNode;
  closeOnClick?: boolean;
  highlightOnHover?: boolean;
}

const {element} = bemFactory({baseClassName: baseMenuClassName, classNames});

export const CustomMenuItem = ({
  children,
  closeOnClick = false,
  highlightOnHover = false,
  ...rest
}: CustomMenuItemProps) => {
  const {itemClicked} = useContext(DropdownContext);

  const liClasses = cn(element('li'));

  const onItemClick = (e) => {
    // this enables consumers to prevent closing the menu items when clicked by calling e.preventDefault
    if (!e.defaultPrevented) {
      itemClicked(closeOnClick);
    }
  };

  return (
    <li
      data-testid="dropdown-menu-item"
      className={liClasses}
      {...extractDataAndAriaProps(rest)}
      onClick={onItemClick}>
      <span
        data-testid="dropdown-menu-item__custom"
        className={cn(element('custom-item'), {
          [element(`custom-item`, 'hover')]: highlightOnHover,
        })}>
        <div className={element('text')}>{children}</div>
      </span>
    </li>
  );
};

export default CustomMenuItem;
