import React from 'react';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

const {element} = bemFactory({baseClassName: baseMenuClassName, classNames});

export const MenuDivider = ({...rest}) => {
  return (
    <li
      data-testid="dropdown-menu-divider"
      className={element(`divider`)}
      {...extractDataAndAriaProps(rest)}
    />
  );
};

export default MenuDivider;
