import React, {ReactElement, ReactNode} from 'react';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import {PlacementTypes, TriggerTypes} from '@jotunheim/react-portal-trigger';
import Icon from '@jotunheim/react-icons';

import Dropdown from './Dropdown';
import DropdownMenu, {DropdownMenuProps} from './DropdownMenu';
import {baseMenuClassName} from '../constants';

import classNames from './css/dropdown.module.css';

interface SubMenuTriggerItemProps {
  menu: ReactElement<DropdownMenuProps, typeof DropdownMenu>;
  children: ReactNode;
  iconPath?: string;
  disabled?: boolean;
}

const {element} = bemFactory({baseClassName: baseMenuClassName, classNames});

export const SubMenuTriggerItem = ({
  children,
  disabled = false,
  menu,
  iconPath,
  ...rest
}: SubMenuTriggerItemProps) => {
  const classes = cn(element('li'), {
    [element('li', 'disabled')]: disabled,
  });

  return (
    <li
      data-testid="dropdown-menu-submenu"
      className={classes}
      {...extractDataAndAriaProps(rest)}>
      <Dropdown
        data-testid="dropdown-menu-submenu__dropdown"
        menu={menu}
        trigger={TriggerTypes.hover}
        placement={PlacementTypes.leftStart}>
        <div
          data-testid="dropdown-menu-submenu__wrapper"
          className={element('submenu-wrapper')}>
          {iconPath && (
            <span
              className={element('item-icon')}
              data-testid="dropdown-menu-item__icon">
              <Icon path={iconPath} size={16} />
            </span>
          )}
          {children}
        </div>
      </Dropdown>
    </li>
  );
};

export default SubMenuTriggerItem;
