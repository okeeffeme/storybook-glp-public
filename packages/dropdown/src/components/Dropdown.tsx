import React, {forwardRef, ReactElement, useCallback, useMemo} from 'react';
import {useUncontrolledProp} from 'uncontrollable';
import cn from 'classnames';

import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import PortalTrigger, {
  Placement,
  PopperRef,
  StrictModifiers,
  TriggerElement,
  TriggerRef,
  TriggerTypes,
} from '@jotunheim/react-portal-trigger';

import DropdownMenu, {DropdownMenuProps} from './DropdownMenu';
import MenuDivider from './MenuDivider';
import MenuHeader from './MenuHeader';
import CustomMenuItem from './CustomMenuItem';
import MenuItem from './MenuItem';
import SubMenuTriggerItem from './SubMenuTriggerItem';
import DropdownContext from './DropdownContext';
import {baseClassName} from '../constants';

import classNames from './css/dropdown.module.css';

type DropdownTriggerElement = TriggerElement &
  ReactElement<{
    className?: string;
  }>;

interface DropdownProps {
  portalZIndex?: number;
  onToggle?: (open: boolean) => void;
  open?: boolean;
  trigger?: TriggerTypes;
  defaultOpen?: boolean;
  menu: ReactElement<DropdownMenuProps, typeof DropdownMenu>;
  children: DropdownTriggerElement;
  placement?: Placement;
  modifiers?: StrictModifiers[];
  popperRef?: PopperRef;
}

type ForwardRefDropdown = React.ForwardRefExoticComponent<
  DropdownProps & React.RefAttributes<HTMLElement | null>
> & {
  Menu: typeof DropdownMenu;
  MenuItem: typeof MenuItem;
  MenuHeader: typeof MenuHeader;
  MenuDivider: typeof MenuDivider;
  CustomMenuItem: typeof CustomMenuItem;
  SubMenuTriggerItem: typeof SubMenuTriggerItem;
};

const {block, element} = bemFactory({baseClassName, classNames});

export const Dropdown: ForwardRefDropdown = forwardRef(
  (props: DropdownProps, ref: TriggerRef) => {
    const {
      modifiers,
      placement = 'bottom-start',
      trigger = TriggerTypes.click,
      defaultOpen = false,
      portalZIndex,
      children,
      menu,
      popperRef,
      ...rest
    } = props;
    const [open, onToggle] = useUncontrolledProp(
      props.open,
      defaultOpen,
      props.onToggle
    );

    const itemClicked = useCallback(
      (closeOnClick) => {
        if (closeOnClick) {
          onToggle(false);
        }
      },
      [onToggle]
    );

    const context = useMemo(
      () => ({
        itemClicked,
      }),
      [itemClicked]
    );

    const singleChild = React.Children.only(children);
    const triggerClone = React.cloneElement(singleChild, {
      className: cn(element('toggle'), singleChild.props.className),
    });

    return (
      <DropdownContext.Provider value={context}>
        <div className={block()} {...extractDataAndAriaProps(rest)}>
          <PortalTrigger
            ref={ref}
            popperRef={popperRef}
            overlay={menu}
            trigger={trigger}
            transitionClassNames={baseClassName}
            placement={placement}
            modifiers={modifiers}
            open={open}
            onToggle={onToggle}
            animation={false}
            portalZIndex={portalZIndex}>
            {triggerClone as TriggerElement}
          </PortalTrigger>
        </div>
      </DropdownContext.Provider>
    );
  }
) as ForwardRefDropdown;

Dropdown.Menu = DropdownMenu;
Dropdown.MenuItem = MenuItem;
Dropdown.MenuHeader = MenuHeader;
Dropdown.MenuDivider = MenuDivider;
Dropdown.CustomMenuItem = CustomMenuItem;
Dropdown.SubMenuTriggerItem = SubMenuTriggerItem;

Dropdown.displayName = 'Dropdown';

export default Dropdown;
