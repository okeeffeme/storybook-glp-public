import React from 'react';
import {render, fireEvent} from '@testing-library/react';

import Dropdown from '../../src/components/Dropdown';
import {pencil} from '../../../icons/src';

const subMenu = (
  <Dropdown.Menu data-testid="my-dropdown-sub-menu">
    <Dropdown.MenuItem>Sub Item 1</Dropdown.MenuItem>
    <Dropdown.MenuItem>Sub Item 2</Dropdown.MenuItem>
    <Dropdown.MenuItem>Sub Item 3</Dropdown.MenuItem>
  </Dropdown.Menu>
);

const disabledClassName = 'comd-dd-menu__li--disabled';

describe('Sub Menu Trigger Item', () => {
  it('should render trigger item', async () => {
    const wrapper = render(
      <Dropdown.SubMenuTriggerItem menu={subMenu}>
        Item Text
      </Dropdown.SubMenuTriggerItem>
    );

    // check that sub-menu items are not rendered initially
    expect(wrapper.queryAllByTestId('dropdown-menu-item').length).toBe(0);

    const subMenuWrapper = wrapper.getByTestId(
      'dropdown-menu-submenu__wrapper'
    );

    // trigger hover and see wait for sub-menu to be rendered
    fireEvent.mouseEnter(subMenuWrapper);
    await wrapper.findByTestId('my-dropdown-sub-menu');

    // check that sub-menu items are now rendered
    expect(wrapper.queryAllByTestId('dropdown-menu-item').length).toBe(3);
  });

  it('should render trigger item with disabled class when disabled', () => {
    const wrapper = render(
      <Dropdown.SubMenuTriggerItem menu={subMenu} disabled={true}>
        Disabled trigger item
      </Dropdown.SubMenuTriggerItem>
    );

    expect(wrapper.getByTestId('dropdown-menu-submenu').className).toContain(
      disabledClassName
    );
  });

  it('should not render trigger item with disabled class when not disabled', () => {
    const wrapper = render(
      <Dropdown.SubMenuTriggerItem menu={subMenu} disabled={false}>
        Disabled trigger item
      </Dropdown.SubMenuTriggerItem>
    );

    expect(
      wrapper.getByTestId('dropdown-menu-submenu').className
    ).not.toContain(disabledClassName);
  });

  it('should render Icon when iconPath is defined', () => {
    const wrapper = render(
      <Dropdown.SubMenuTriggerItem menu={subMenu} iconPath={pencil}>
        Link
      </Dropdown.SubMenuTriggerItem>
    );

    expect(
      wrapper.queryByTestId('dropdown-menu-item__icon')
    ).toBeInTheDocument();
  });

  it('should not render Icon when iconPath is not defined', () => {
    const wrapper = render(
      <Dropdown.SubMenuTriggerItem menu={subMenu}>
        Link
      </Dropdown.SubMenuTriggerItem>
    );

    expect(
      wrapper.queryByTestId('dropdown-menu-item__icon')
    ).not.toBeInTheDocument();
  });
});
