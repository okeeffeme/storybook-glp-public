import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Dropdown from '../../src/components/Dropdown';
import DropdownContext from '../../src/components/DropdownContext';

describe('Dropdown Menu', () => {
  it('should render menu items', () => {
    const wrapper = render(
      <Dropdown.Menu>
        <Dropdown.MenuHeader>header</Dropdown.MenuHeader>
        <Dropdown.MenuItem>text</Dropdown.MenuItem>
      </Dropdown.Menu>
    );

    expect(wrapper.getByTestId('dropdown-menu-items')).toBeInTheDocument();
    expect(wrapper.getByTestId('dropdown-menu-header')).toBeInTheDocument();
    expect(wrapper.getByTestId('dropdown-menu-item')).toBeInTheDocument();
  });

  it('should call itemClicked if MenuItem is clicked', () => {
    const context = {
      itemClicked: jest.fn(),
    };
    const wrapper = render(
      <DropdownContext.Provider value={context}>
        <Dropdown.Menu>
          <Dropdown.MenuHeader>header</Dropdown.MenuHeader>
          <Dropdown.MenuItem>
            <div data-testid="click-me">text</div>
          </Dropdown.MenuItem>
        </Dropdown.Menu>
      </DropdownContext.Provider>
    );

    const menuItem = wrapper.getByTestId('click-me');
    userEvent.click(menuItem);

    expect(context.itemClicked).toBeCalled();
  });

  it('should not call itemClicked if MenuItem is clicked and e.preventDefault is called', () => {
    const context = {
      itemClicked: jest.fn(),
    };
    const wrapper = render(
      <DropdownContext.Provider value={context}>
        <Dropdown.Menu>
          <Dropdown.MenuHeader>header</Dropdown.MenuHeader>
          <Dropdown.MenuItem onClick={(e) => e.preventDefault()}>
            <div data-testid="click-me">text</div>
          </Dropdown.MenuItem>
        </Dropdown.Menu>
      </DropdownContext.Provider>
    );

    const menuItem = wrapper.getByTestId('click-me');
    userEvent.click(menuItem);

    expect(context.itemClicked).not.toBeCalled();
  });
});
