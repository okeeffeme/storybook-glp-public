import React from 'react';
import {render} from '@testing-library/react';

import Dropdown from '../../src/components/Dropdown';

describe('Menu Divider', () => {
  it('should render divider', () => {
    const wrapper = render(<Dropdown.MenuDivider />);

    expect(wrapper.getByTestId('dropdown-menu-divider')).toBeInTheDocument();
  });
});
