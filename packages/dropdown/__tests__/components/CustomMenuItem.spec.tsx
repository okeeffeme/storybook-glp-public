import React from 'react';
import {render} from '@testing-library/react';

import Dropdown from '../../src/components/Dropdown';
import Switch from '../../../switch/src';

describe('Custom Menu Item', () => {
  it('should render custom item', () => {
    const wrapper = render(
      <Dropdown.CustomMenuItem>
        <Switch data-testid="custom-switch" />
      </Dropdown.CustomMenuItem>
    );

    expect(wrapper.getByTestId('custom-switch')).toBeInTheDocument();
  });

  it('should render text item with hover class when using highlightOnHover prop', () => {
    const wrapper = render(
      <Dropdown.CustomMenuItem highlightOnHover={true}>
        Text with css
      </Dropdown.CustomMenuItem>
    );

    expect(
      wrapper.getByTestId('dropdown-menu-item__custom').className
    ).toContain('comd-dd-menu__custom-item--hover');
  });

  it('should not render text item with hover class when highlightOnHover is false', () => {
    const wrapper = render(
      <Dropdown.CustomMenuItem highlightOnHover={false}>
        Text with css
      </Dropdown.CustomMenuItem>
    );

    expect(
      wrapper.getByTestId('dropdown-menu-item__custom').className
    ).not.toContain('comd-dd-menu__custom-item--hover');
  });
});
