import React from 'react';
import {render} from '@testing-library/react';

import Dropdown from '../../src/components/Dropdown';

describe('Menu Header', () => {
  it('should render header', () => {
    const wrapper = render(<Dropdown.MenuHeader>Header</Dropdown.MenuHeader>);

    expect(wrapper.getByTestId('dropdown-menu-header')).toBeInTheDocument();
  });
});
