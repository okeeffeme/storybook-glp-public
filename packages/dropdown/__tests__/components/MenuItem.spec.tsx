import React from 'react';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Dropdown from '../../src/components/Dropdown';
import {pencil} from '../../../icons/src';

describe('MenuItem :: ', () => {
  describe('with href :: ', () => {
    it('should render link item', () => {
      const wrapper = render(
        <Dropdown.MenuItem href="/link">Link</Dropdown.MenuItem>
      );

      expect(
        wrapper.queryByTestId('dropdown-menu-item__anchor')
      ).toBeInTheDocument();

      expect(
        wrapper.queryByTestId('[data-testid="dropdown-menu-item__text"]')
      ).not.toBeInTheDocument();
    });

    it('should render selectable link item', () => {
      const wrapper = render(
        <Dropdown.MenuItem href="/link" selectable>
          selectable
        </Dropdown.MenuItem>
      );

      expect(wrapper.getByTestId('dropdown-menu-item').className).not.toContain(
        'comd-dd-menu__item--selected'
      );
      expect(
        wrapper.queryByTestId('dropdown-menu-item__selectable')
      ).toBeInTheDocument();
    });

    it('should render selectable link item which is already selected', () => {
      const wrapper = render(
        <Dropdown.MenuItem href="/link" selectable={true} selected={true}>
          selectable and selected
        </Dropdown.MenuItem>
      );

      expect(
        wrapper.getByTestId('dropdown-menu-item__anchor').className
      ).toContain('comd-dd-menu__item--selected');
      expect(
        wrapper.queryByTestId('dropdown-menu-item__selectable')
      ).toBeInTheDocument();
    });
  });

  describe('without href :: ', () => {
    it('should render text item', () => {
      const wrapper = render(<Dropdown.MenuItem>Text</Dropdown.MenuItem>);

      expect(
        wrapper.queryByTestId('dropdown-menu-item__text')
      ).toBeInTheDocument();

      expect(
        wrapper.queryByTestId('dropdown-menu-item__anchor')
      ).not.toBeInTheDocument();
    });

    it('should render selectable text item', () => {
      const wrapper = render(
        <Dropdown.MenuItem selectable>selectable</Dropdown.MenuItem>
      );

      expect(wrapper.getByTestId('dropdown-menu-item').className).not.toContain(
        'comd-dd-menu__item--selected'
      );
      expect(
        wrapper.queryByTestId('dropdown-menu-item__selectable')
      ).toBeInTheDocument();
    });

    it('should render selectable text item which is already selected', () => {
      const wrapper = render(
        <Dropdown.MenuItem selectable={true} selected={true}>
          selectable and selected
        </Dropdown.MenuItem>
      );

      expect(
        wrapper.getByTestId('dropdown-menu-item__text').className
      ).toContain('comd-dd-menu__item--selected');
      expect(
        wrapper.queryByTestId('dropdown-menu-item__selectable')
      ).toBeInTheDocument();
    });
  });

  it('should call click whenever item is clicked', () => {
    const click = jest.fn();
    const wrapper = render(
      <Dropdown.MenuItem onClick={click} href="/link">
        Link
      </Dropdown.MenuItem>
    );

    userEvent.click(wrapper.getByTestId('dropdown-menu-item'));

    expect(click).toBeCalled();
  });

  it('should not call click whenever item is clicked and item is disabled', () => {
    const click = jest.fn();
    const wrapper = render(
      <Dropdown.MenuItem onClick={click} href="/link" disabled={true}>
        Link
      </Dropdown.MenuItem>
    );

    userEvent.click(wrapper.getByTestId('dropdown-menu-item'));

    expect(click).not.toBeCalled();
  });

  it('should render Icon when iconPath is defined', () => {
    const wrapper = render(
      <Dropdown.MenuItem iconPath={pencil}>Link</Dropdown.MenuItem>
    );

    expect(
      wrapper.queryByTestId('dropdown-menu-item__icon')
    ).toBeInTheDocument();
  });

  it('should not render Icon when iconPath is not defined', () => {
    const wrapper = render(<Dropdown.MenuItem>Link</Dropdown.MenuItem>);

    expect(
      wrapper.queryByTestId('dropdown-menu-item__icon')
    ).not.toBeInTheDocument();
  });
});
