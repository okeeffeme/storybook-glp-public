import React from 'react';
import {render} from '@testing-library/react';

import {Dropdown} from '../../src/components/Dropdown';

describe('Dropdown', () => {
  it('should render custom toggle', () => {
    const wrapper = render(
      <Dropdown
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuItem>Text</Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <button data-testid="my-dropdown-toggle">
          <span>custom toggle</span>
        </button>
      </Dropdown>
    );

    expect(wrapper.getByTestId('my-dropdown-toggle')).toBeInTheDocument();
  });

  it('should render as open', () => {
    const wrapper = render(
      <Dropdown
        open={true}
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuItem>Text</Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <button>Toggle</button>
      </Dropdown>
    );

    expect(wrapper.getByTestId('dropdown-menu-items')).toBeInTheDocument();
  });

  it('should render as closed', () => {
    const wrapper = render(
      <Dropdown
        open={false}
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuItem>Text</Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <button>Toggle</button>
      </Dropdown>
    );

    expect(
      wrapper.queryByTestId('dropdown-menu-items')
    ).not.toBeInTheDocument();
  });
});
