import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import Dropdown from '../src/components/Dropdown';
import Tooltip from '../../tooltip/src';

import {Button} from '../../button/src';
import Truncate from '../../truncate/src';
import Icon, {
  openInNew,
  menuDown,
  trashBin,
  pencil,
  eye,
} from '../../icons/src';
import {ToggleButton, ToggleButtonGroup} from '../../toggle-button/src';

const WithPaddingTop = (props) => (
  <div style={{paddingTop: '350px'}} {...props} />
);
const TallContainer = (props) => <div style={{height: '1000px'}} {...props} />;

storiesOf('Components/dropdown', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('basic', () => (
    <TallContainer>
      <Dropdown
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>
              <Truncate>Super long long long long long header</Truncate>
            </Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem selectable selected>
              Selectable Item
            </Dropdown.MenuItem>
            <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
            <Dropdown.MenuItem href="#">
              Link Item
              <Icon path={openInNew} size="16" />
            </Dropdown.MenuItem>
            <Dropdown.MenuItem
              onClick={() => {
                alert('hello');
              }}>
              click to show alert
            </Dropdown.MenuItem>
            <Dropdown.MenuItem
              disabled={true}
              onClick={() => {
                alert('hello');
              }}>
              click to show alert (disabled)
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <button>Dropdown - default</button>
      </Dropdown>
    </TallContainer>
  ))
  .add('with custom types', () => {
    const [mode, setMode] = React.useState('edit');
    const [mode2, setMode2] = React.useState('edit');

    return (
      <Dropdown
        menu={
          <Dropdown.Menu>
            <Dropdown.CustomMenuItem>
              <span>Stays open when clicked</span>
              <ToggleButtonGroup>
                <ToggleButton
                  id={'edit'}
                  checked={mode === 'edit'}
                  onChange={() => {
                    setMode('edit');
                  }}
                  text={'Edit'}
                  tooltipText={'Edit thing'}
                  icon={<Icon path={pencil} />}
                />
                <ToggleButton
                  id={'preview'}
                  checked={mode === 'preview'}
                  onChange={() => {
                    setMode('preview');
                  }}
                  text={'Preview'}
                  tooltipText={'Preview thing'}
                  icon={<Icon path={eye} />}
                />
              </ToggleButtonGroup>
            </Dropdown.CustomMenuItem>
            <Dropdown.CustomMenuItem
              closeOnClick={true}
              highlightOnHover={true}>
              <span>Close when clicked</span>
              <ToggleButtonGroup>
                <ToggleButton
                  id={'edit'}
                  checked={mode2 === 'edit'}
                  onChange={() => {
                    setMode2('edit');
                  }}
                  text={'Edit'}
                  tooltipText={'Edit thing'}
                  icon={<Icon path={pencil} />}
                />
                <ToggleButton
                  id={'preview'}
                  checked={mode2 === 'preview'}
                  onChange={() => {
                    setMode2('preview');
                  }}
                  text={'Preview'}
                  tooltipText={'Preview thing'}
                  icon={<Icon path={eye} />}
                />
              </ToggleButtonGroup>
            </Dropdown.CustomMenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem closeOnClick={false}>
              Stay open when clicked
            </Dropdown.MenuItem>
            <Dropdown.MenuItem>Close on click</Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <Button appearance={Button.appearances.primarySuccess}>
          <span style={{marginRight: '1ch'}}>dd</span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>
    );
  })
  .add('with sub-menu', () => (
    <Dropdown
      menu={
        <Dropdown.Menu>
          <Dropdown.MenuItem>Item 1</Dropdown.MenuItem>
          <Dropdown.MenuItem>Item 2</Dropdown.MenuItem>
          <Dropdown.SubMenuTriggerItem
            menu={
              <Dropdown.Menu>
                <Dropdown.MenuItem>Sub Item 1</Dropdown.MenuItem>
                <Dropdown.MenuItem>Sub Item 2</Dropdown.MenuItem>
                <Dropdown.MenuItem iconPath={trashBin}>
                  Delete
                </Dropdown.MenuItem>
              </Dropdown.Menu>
            }>
            Item 3 - with sub-menu
          </Dropdown.SubMenuTriggerItem>
        </Dropdown.Menu>
      }>
      <Button appearance={Button.appearances.tertiary}>Open menu</Button>
    </Dropdown>
  ))
  .add('Multiple dropdown components', () => (
    <div>
      <Dropdown
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem selectable selected>
              Selectable Item
            </Dropdown.MenuItem>
            <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
            <Dropdown.MenuItem href="#">
              Link Item
              <Icon path={openInNew} size="16" />
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <Button appearance={Button.appearances.primarySuccess}>
          <span style={{marginRight: '1ch'}}>1</span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>

      <Dropdown
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem selectable selected>
              Selectable Item
            </Dropdown.MenuItem>
            <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
            <Dropdown.MenuItem href="#">
              Link Item
              <Icon path={openInNew} size="16" />
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <Button appearance={Button.appearances.primarySuccess}>
          <span style={{marginRight: '1ch'}}>2</span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>
    </div>
  ))
  .add('open right', () => (
    <Dropdown
      placement="right"
      menu={
        <Dropdown.Menu>
          <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
          <Dropdown.MenuDivider />
          <Dropdown.MenuItem selectable selected>
            Selectable Item
          </Dropdown.MenuItem>
          <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
          <Dropdown.MenuDivider />
          <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
          <Dropdown.MenuItem href="#">
            Link Item
            <Icon path={openInNew} size="16" />
          </Dropdown.MenuItem>
        </Dropdown.Menu>
      }>
      <Button appearance={Button.appearances.primarySuccess}>
        <span style={{marginRight: '1ch'}}>Dropdown - open right</span>
        <Icon path={menuDown} />
      </Button>
    </Dropdown>
  ))
  .add('open top', () => (
    <WithPaddingTop>
      <Dropdown
        placement="top"
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem selectable selected>
              Selectable Item
            </Dropdown.MenuItem>
            <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
            <Dropdown.MenuItem href="#">
              Link Item
              <Icon path={openInNew} size="16" />
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <Button appearance={Button.appearances.primarySuccess}>
          <span style={{marginRight: '1ch'}}>Dropdown - open top</span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>
    </WithPaddingTop>
  ))
  .add('open top and right', () => (
    <WithPaddingTop>
      <Dropdown
        placement="top-end"
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem selectable selected>
              Selectable Item
            </Dropdown.MenuItem>
            <Dropdown.MenuItem selectable>Selectable Item</Dropdown.MenuItem>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
            <Dropdown.MenuItem href="#">
              Link Item
              <Icon path={openInNew} size="16" />
            </Dropdown.MenuItem>
          </Dropdown.Menu>
        }>
        <Button appearance={Button.appearances.primarySuccess}>
          <span style={{marginRight: '1ch'}}>
            Dropdown - open top and right
          </span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>
    </WithPaddingTop>
  ))
  .add('open by default', () => (
    <Dropdown
      defaultOpen={true}
      menu={
        <Dropdown.Menu>
          <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
          <Dropdown.MenuDivider />
          <Dropdown.MenuItem selectable selected closeOnClick={false}>
            Selectable Item
          </Dropdown.MenuItem>
          <Dropdown.MenuItem selectable closeOnClick={false}>
            Selectable Item
          </Dropdown.MenuItem>
          <Dropdown.MenuDivider />
          <Dropdown.MenuItem>Text Item</Dropdown.MenuItem>
          <Dropdown.MenuItem href="#">
            Link Item
            <Icon path={openInNew} size="16" />
          </Dropdown.MenuItem>
        </Dropdown.Menu>
      }>
      <Button>
        <span style={{marginRight: '1ch'}}>Dropdown - open by default</span>
        <Icon path={menuDown} />
      </Button>
    </Dropdown>
  ))
  .add('with item icon', () => {
    return (
      <Dropdown
        defaultOpen={true}
        menu={
          <Dropdown.Menu>
            <Dropdown.MenuHeader>Header</Dropdown.MenuHeader>
            <Dropdown.MenuDivider />
            <Dropdown.MenuItem>No icon (MenuItem)</Dropdown.MenuItem>
            <Dropdown.MenuItem iconPath={trashBin}>
              Delete (MenuItem)
            </Dropdown.MenuItem>
            <Dropdown.MenuItem iconPath={trashBin} disabled={true}>
              <Tooltip content="Tooltip inside disabled link item">
                <div>Delete (MenuItem)</div>
              </Tooltip>
            </Dropdown.MenuItem>

            <Dropdown.MenuItem>No icon (MenuItem)</Dropdown.MenuItem>
            <Dropdown.MenuItem iconPath={trashBin}>
              Delete (MenuItem)
            </Dropdown.MenuItem>
            <Dropdown.CustomMenuItem>
              <Tooltip content="Tooltip inside custom item">
                <div>Delete (MenuItem)</div>
              </Tooltip>
            </Dropdown.CustomMenuItem>

            <Dropdown.MenuItem>
              No icon (MenuItem - highlightOnHover)
            </Dropdown.MenuItem>
            <Dropdown.MenuItem iconPath={trashBin}>
              Delete (MenuItem - highlightOnHover)
            </Dropdown.MenuItem>
            <Dropdown.CustomMenuItem>
              <Tooltip content="tooltip inside custom item">
                <div>Delete (MenuItem - highlightOnHover)</div>
              </Tooltip>
            </Dropdown.CustomMenuItem>
          </Dropdown.Menu>
        }>
        <Button>
          <span style={{marginRight: '1ch'}}>Dropdown - open by default</span>
          <Icon path={menuDown} />
        </Button>
      </Dropdown>
    );
  });
