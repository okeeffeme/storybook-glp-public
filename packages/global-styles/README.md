# Global styling for the material design based theme

[Changelog](./CHANGELOG.md)

The package contains base styles, normalize styles, global variables, functions and mixins.

Some variables are exported and available to use from JavaScript. See Usage section for how to do this.

## Usage

### Get solid variant of an RGBA color

This is useful in scenarios where you need a color defined in the color specs, but you need it without transparency (f.ex for tooltips).

```scss
// my-styles.scss

@import '../node_modules/global-styles/src/configuration'; // change path to what you need

.my-style-with-opacity {
  background: $TextPrimary; // results in rgba(18, 24, 33, 0.87)
}

.my-style-with-solid-color {
  background: solid-color(
    $TextPrimary
  ); // results in #31363E, which is visually the same as rgba(18, 24, 33, 0.87) when shown on a white background
}
```

### Use SASS variables in your scss files:

```scss
// my-styles.scss

@import '../node_modules/global-styles/src/configuration'; // change path to what you need

.my-classname {
  background: $Danger;
}

// compiled css:
.my-classname {
  background: #eb0052;
}
```

### Use variables in your js files:

This requires that you have a setup of css-loader in webpack. Variable exports are written in typescript and type-casted
to its type. (css variables are strings by default).

**IMPORTANT** Do not import variables directly from `.css` or `.scss` files as internal structure of package can be changed.

```js
import {colors, responsive} from '@jotunheim/global-styles';

const dangerColor = colors.Danger;
const smallResolution = responsive.Small;
```

### Use CSS variables in your sass files:

This file should be imported into all apps, **but only once**.

**IMPORTANT** CSS variable functionality should NOT be used without consulting the Design System Team.

```js
@import '../node_modules/global-styles/src/configuration'; // change path to what you need

//overriding in scss
--ds-color-base-constrast-low: rgba(255, 255, 255, 60);
--ds-color-base-primary: #{$ContrastLow};

.my-classname {
  background: var(--ds-color-base-constrast-low);
}
```

Changing the variable value can also be done with JS during runtime, via `document.querySelector('.some-scope').style.setAttribute('--ds-color-base-contrast-low', 'black')`.

CSS variables are also exported and are available to use in your JS files - for more information, follow the instructions in "Use variables in your js files" above.
