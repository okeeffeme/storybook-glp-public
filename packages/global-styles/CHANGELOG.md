# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/global-styles@6.0.0&sourceBranch=refs/tags/@jotunheim/global-styles@6.0.1&targetRepoId=1246) (2023-03-02)

### Bug Fixes

- use math.div for division in sass ([NPM-991](https://jiraosl.firmglobal.com/browse/NPM-991)) ([25ba59a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/25ba59addb39dddc535e8dc54dc2d11b851cc5bd))

# [6.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-alert@8.0.4&sourceBranch=refs/tags/@jotunheim/react-alert@8.1.0&targetRepoId=1246) (2022-08-17)

- Use @jotunheim namespace for global-styles ([NPM-1050](https://jiraosl.firmglobal.com/browse/NPM-1050)) ([55d414e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/55d414e2d7608caadef6113447be8605f196a961))

## [5.9.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.9.0&sourceBranch=refs/tags/@confirmit/global-styles@5.9.1&targetRepoId=1246) (2022-07-15)

### Bug Fixes

- remove ie11only mixin, ie 11 support is no longer needed (only smarthub is using the mixin) ([NPM-822](https://jiraosl.firmglobal.com/browse/NPM-822)) ([94355d4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/94355d4dd29b8b5764dbc70602856f6580c8d5d4))

# [5.9.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.8.1&sourceBranch=refs/tags/@confirmit/global-styles@5.9.0&targetRepoId=1246) (2022-05-18)

### Features

- Adding css variables to global-styles ([NPM-885](https://jiraosl.firmglobal.com/browse/NPM-885)) ([6ff8b16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ff8b16f65d6887d77d90cb4c992e94cfb281f71))
- Adding opacities to global-styles ([9041b5a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9041b5af0715336aaaef0488e71fc265c0637713))
- Adding Transparent as a Sass Variable ([NPM-885](https://jiraosl.firmglobal.com/browse/NPM-885)) ([2e7bf63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2e7bf633c004957959b6092582d264d21f189b2f))

## [5.8.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.8.0&sourceBranch=refs/tags/@confirmit/global-styles@5.8.1&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/global-styles

# [5.8.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.7.0&sourceBranch=refs/tags/@confirmit/global-styles@5.8.0&targetRepoId=1246) (2021-06-03)

### Features

- Add Passive colour to shared components ([NPM-786](https://jiraosl.firmglobal.com/browse/NPM-786)) ([6195d23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6195d23256d155550eff94e6d466d4a42a6a31a7))

# [5.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.6.0&sourceBranch=refs/tags/@confirmit/global-styles@5.7.0&targetRepoId=1246) (2021-05-27)

### Features

- new colour for passive text analytics added ([NPM-759](https://jiraosl.firmglobal.com/browse/NPM-759)) ([844f2eb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/844f2ebc6c7f8b01afdf5f8f0f116b254865c2ac))

# [5.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.5.0&sourceBranch=refs/tags/@confirmit/global-styles@5.6.0&targetRepoId=1246) (2021-03-17)

### Features

- add ie11only mixin to easily add ie 11 only css overrides/fallbacks ([NPM-730](https://jiraosl.firmglobal.com/browse/NPM-730)) ([d714258](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d7142580da223256c07f62b447eb095231d39fa6))

# [5.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.4.2&sourceBranch=refs/tags/@confirmit/global-styles@5.5.0&targetRepoId=1246) (2021-02-01)

### Features

- Add color for hover of deactivated chips ([NPM-352](https://jiraosl.firmglobal.com/browse/NPM-352)) ([a06729b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a06729bff5791db8c354adbe0dd760aabdc4027f))

## [5.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.4.1&sourceBranch=refs/tags/@confirmit/global-styles@5.4.2&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/global-styles

## [5.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.4.0&sourceBranch=refs/tags/@confirmit/global-styles@5.4.1&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- set correct exit duration for transitions ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([bad9709](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bad9709b148f2e82f97e2b40ecb8c97b85ce0182))

# [5.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/global-styles@5.3.0&sourceBranch=refs/tags/@confirmit/global-styles@5.4.0&targetRepoId=1246) (2020-09-18)

### Features

- **global-styles:** add Product color variables ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([94d905f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/94d905fb3862309b34232cd5db922a911b5c89a4)

# [5.3.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/global-styles@5.2.0...@confirmit/global-styles@5.3.0) (2020-08-16)

### Features

- export variables as javascript ([NPM-481](https://jiraosl.firmglobal.com/browse/NPM-481)) ([b281f3f](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/b281f3fb6993cff26271e02251201d398ad99e5e))

## CHANGELOG

### v5.2.0

- Feat: Transition variables `$EnterDuration` and `$ExitDuration` are exported to be accessible by JS.

### v5.1.0

- Feat: Transition variables:
  - `$EnterDuration`
  - `$ExitDuration`
  - `$Easing-Decelerated` (from https://material.io/design/motion/speed.html#easing)

### v5.0.0

- Refactor: New package name: `@confirmit/global-styles`

### v4.2.0

- Feat: Change `$Safe` color value from #39a63a to #39a739 (according to the latest mockups on sketch)

### v4.1.0

- Feat: `solid-color` function. This function will return the HEX equivalent color from a given RGBA color when shown on a white background.

### v4.0.0

- **BREAKING**:
  - Move all exported variables to `exports` folder. If you are using some scss variables in JS, you need to update the import path from `variables` to `exports`. If you do not use scss variables in JS, there is no breaking change for you. This was mainly done to avoid including them when importing `_configuration`, since that would cause the exported values being present in every css file that imported the `_configuration` file, causing a lot of duplication. This also caused issues with some logic in `bemBuilder`.

### v3.1.0

- Feat: Add `comd-click-ripple` mixin, used for ripple effect on click

### v3.0.0

- **BREAKING**:
  - `_color-palette.scss` is now dependent on `_colors.scss`, so if you import `_color-palette.scss` anywhere directly, you need to also import `_colors.scss`. If you import `_configuration.scss`, you do not need to do any changes.
  - colors have been updated to match Design System spec
  - Removed mixins:
    - `comd-chart-section-shadow`. Not in use.
    - `comd-overlay-box-shadow`. Not in use.
    - `comd-visually-hidden`. Not in use.
    - `comd-blue-border`. Was not used consistently, and should instead be part of the shared component inputs.
- Feat: Some variables are available from JS, by utilizing [css modules `:export`](https://github.com/css-modules/icss#export). See usage section for details. Currently, only breakpoints, margins and color variables are exported.
- Feat: Spacing variable map:
  - `$Spacing`, with values `Small` (8), `Medium` (16) and `Large` (24). Usage: `#{map-get($Spacing, 'Small')}`
- Feat: New color names
  - Should be easier to remember the new names. Old names are still availalble, but is only kept for backwards compatibility. The old names might be removed in the future. Please update to new names when you come across them in areas you are working on.
  - `$comd-grey-saturation-sc-pt1` -> `$BaseColor`
  - `$comd-grey-saturation-sc-pt3` -> `$Background2`
  - `$comd-grey-saturation-sc-pt4` -> `$ChartNeutral`
  - `$comd-grey-saturation-sc-pt5` -> `$Background3`
  - `$comd-grey-saturation-sc-pt6` -> `$Background3`
  - `$comd-grey-saturation-sc-pt7` -> `$Background3`
  - `$comd-grey-saturation-sc-pt8` -> `$Disabled`
  - `$comd-grey-saturation-sc-pt1-op1` -> `$TextPrimary`
  - `$comd-grey-saturation-sc-pt1-op2` -> `$TextSecondary`
  - `$comd-grey-saturation-sc-pt1-op3` -> `$TextDisabled`
  - `$comd-grey-saturation-sc-pt1-op4` -> `$Disabled`
  - `$comd-grey-saturation-sc-pt1-op5` -> `$Background1`
  - `$comd-grey-saturation-sc-pt1-op6` -> `$Background1`
  - `$comd-grey-saturation-sc-pt1-op7` -> `$Background1`
  - `$comd-black-sc-op1` -> `$TextPrimary`
  - `$comd-black-sc-op2` -> `$TextSecondary`
  - `$comd-black-sc-op3` -> `$TextDisabled`
  - `$comd-color-blue-saturation-sc-pt1` -> `$PrimaryAccent`
  - `$comd-color-blue-saturation-sc-pt2` -> `$PrimaryAccent`
  - `$comd-color-blue-saturation-sc-pt3` -> `$PrimaryAccent`
  - `$comd-color-blue-saturation-sc-pt4` -> `$Disabled`
  - `$comd-color-blue-saturation-sc-pt5` -> `$Interactive`
  - `$comd-color-blue-saturation-sc-pt6` -> `$Interactive`
  - `$comd-color-green-saturation-sc-pt1` -> `$Safe`
  - `$comd-color-green-saturation-sc-pt2` -> `$Safe`
  - `$comd-color-light-sc-pt1` -> `$SafeBackground`
  - `$comd-color-light-sc-pt2` -> `$WarningBackground`
  - `$comd-color-light-sc-pt3` -> `$DangerBackground`
  - `$comd-color-warning` -> `$Warning`
  - `$comd-color-error` -> `$Danger`
  - `$comd-color-danger` -> `$Danger`
  - `$comd-black-sc` -> `$ContrastHigh`
  - `$comd-white-sc` -> `$ContrastLow`

### v2.10.0

- Feat: Add `$comd-text-field-transition-duration` and `$comd-text-field-transition-timing` variables

### v2.9.0

- Feat: Add variable for estimated scrollbar size `$comd-scrollbar-width-estimate`

### v2.8.0

- Feat: Add `$comd-text-field-transition`

### v2.7.4

- Feat: Added `$comd-color-low-intensity-sc-p6`

### v2.7.0

- Feat: Add responsive breakpoints and mixins

### v2.6.1

- Feat: updated `$comd-color-warning`

### v2.6.0

- Feat: add `$comd-color-blue-saturation-sc-pt4` blue color variant

### v2.5.0

- Feat: add `$comd-grey-saturation-sc-pt8` grey color variant

### v2.4.0

- Feat: adding new heatmap color variables from the new Design System. Leaving old vars for legacy.

### v2.3.0

- Feat: Black and white variables added to \_color-palatte.scss; \$comd-color-blue-saturation-sc-pt3 for Pulse bar blue and comd-color-warning also added.

### v2.2.8

- Bug: Added `main: ./src/_configuration.scss` to package.json to allow usage with Confirmit.AppStudio run local-packages

### v2.2.0

- Feat: The SCSS function `number` added. Now the SCSS function `rem` is used `number` to make the value of type [number](https://sass-lang.com/documentation/file.SASS_REFERENCE.html#data_types) instead of `string`. So, you can define constants in `rems` and use it like `$two: rem(2); $three: $two + rem(1);`.

### v2.1.2

- Chore: Change how docs are generated, and how docs are referenced in storybook so it is easier to host within storybook.

### v2.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only
- Consider replacing confirmit-r2-styles with confirmit-global-styles-material. In that case r2- prefix should be changed to comd- prefix
