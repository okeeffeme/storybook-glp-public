import * as colors from './exports/colors';
import * as opacities from './exports/opacities';
import * as responsive from './exports/responsive';
import * as spacing from './exports/spacing';
import * as transitions from './exports/transitions';

export {colors, opacities, responsive, spacing, transitions};
