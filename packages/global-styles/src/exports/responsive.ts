import responsive from './responsive.css';

export const Small = Number(responsive.Small);
export const Medium = Number(responsive.Medium);
export const Large = Number(responsive.Large);
