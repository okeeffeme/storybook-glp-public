import opacities from './opacities.css';

export const Primary = Number(opacities.primaryOpacity);
export const Secondary = Number(opacities.secondaryOpacity);
export const Tertiary = Number(opacities.tertiaryOpacity);
export const Quaternary = Number(opacities.quaternaryOpacity);
export const Quinary = Number(opacities.quinaryOpacity);
export const Alt = Number(opacities.altOpacity);
export const Zero = Number(opacities.zeroOpacity);
