import transitions from './transitions.css';

export const ExitDuration = Number(transitions.ExitDuration);
export const EnterDuration = Number(transitions.EnterDuration);
