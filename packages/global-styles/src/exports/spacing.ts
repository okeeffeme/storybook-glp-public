import spacing from './spacing.css';

export const Small = Number(spacing.Small);
export const Medium = Number(spacing.Medium);
export const Large = Number(spacing.Large);
