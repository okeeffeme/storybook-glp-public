/*
 ---------------
  TYPOGRAPHY
 ---------------
*/

////
/// @group Typography
/// @since 1.0
////

/// Font-family: Source Sans Pro
/// @example
/// .foo {
///   font-family: $comd-font-family;
/// }
/// @type List
$comd-font-family: 'Source Sans Pro', 'Arial', 'Helvetica', 'Geneva', sans-serif;

/// Base font size: 14px
/// @type Number
$comd-base-font-size: 14;

// ----------------------------------
//  Font sizes
// ----------------------------------

/// List of font sizes use in our applications, in px
/// @example
/// .foo {
///   font-size: $comd-font-sizes('xx-small');
/// }
/// @type Map
/// @prop {unit} xx-small [11px]
/// @prop {unit} x-small [12px]
/// @prop {unit} small [13px]
/// @prop {unit} medium [$comd-base-font-size] depends on $comd-base-font-size variable, and it represents 14px
/// @prop {unit} large [15px]
/// @prop {unit} x-large [16px]
/// @prop {unit} xx-large [18px]
/// @prop {unit} xxx-large [20px]
/// @prop {unit} xxxx-large [21px]
/// @prop {unit} xxxxx-large [23px]
/// @prop {unit} xxxxxx-large [24px]
/// @prop {unit} super-large [30px]
/// @prop {unit} x-super-large [32px]
$comd-font-sizes: (
        'xxx-small': 10,
        'xx-small': 11,
        'x-small': 12,
        'small': 13,
        'medium': $comd-base-font-size,
        'large': 15,
        'x-large': 16,
        'xx-large': 18,
        'xxx-large': 20,
        'xxxx-large': 21,
        'xxxxx-large': 23,
        'xxxxxx-large': 24,
        'super-large': 30,
        'x-super-large': 32
);

// ----------------------------------
//  Line heights
// ----------------------------------

/// List of line-heights used throughout Confirmit material design based applications. They are unitless line-heights.
///
/// The unitless value is calculated based on the font-size and line-height in px, as taken from the design mock-ups:
///
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-size: 24px; </br> and </br> line-height: 28px; </br> will give </br> 28 / 24 = 1,16 unitless line-height</p>
/// @example
/// .foo {
///   line-height: $comd-line-heights('xxx-small');
/// }
/// @type Map
/// @prop {unitless} xxx-small [1.23]
/// @prop {unitless} xx-small [1.25]
/// @prop {unitless} x-small [1.26]
/// @prop {unitless} small [1.27]
/// @prop {unitless} medium [1.28]
/// @prop {unitless} large [1.29]
/// @prop {unitless} x-large [1.3]
/// @prop {unitless} xx-large [1.4]
$comd-line-heights: (
        'xxx-small': 1.23,
        'xx-small': 1.25,
        'x-small': 1.26,
        'small': 1.27,
        'medium': 1.28,
        'large': 1.29,
        'x-large': 1.3,
        'xx-large': 1.4
);

// ----------------------------------
//  Font weights
// ----------------------------------

/// List of font weights used throughout Confirmit material design based applications
/// @example
/// .foo {
///   font-weight: $comd-line-heights('regular');
/// }
/// @type Map
/// @prop {number} light [300]
/// @prop {number} regular [400]
/// @prop {number} semibold [600]
/// @prop {number} bold [700]
$comd-font-weights: ('light': 300, 'regular': 400, 'semibold': 600, 'bold': 700);

// ----------------------------------
//  Predefined fonts
// ----------------------------------
// This is a shorthand for defining fonts, which translates to -> font: font-weight font-seize/line-height font-family;

/// <p style="color: #586069; border-bottom: 1px solid #e6ebf1; line-height: 2;">Body text font</p>
/// <p>This font is set on the body tag, which makes that is the default for all text in Confirmit material design based applications.</p>
/// <p>font: 400 14px/18px;</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 14px; <br/> line-height: 18px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-body-text: comd-font(comd-font-weight(), comd-font-size('medium', 'rem'), comd-line-height());

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>navigation links</li><li>column headers in Comments widget</li><li>simple message of a modal dialog</li></ul>
/// <p>font: 400 16px/20px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 16px; <br/> line-height: 20px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-navigation-text: comd-font(comd-font-weight('regular'), comd-font-size('x-large', 'rem'), comd-line-height('xx-small'));

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>results display in the key metric widgets like KPI widget and Response Rate widget</li></ul>
/// <p>font: 300 32px/40px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 300; <br/> font-size: 32px; <br/> line-height: 40px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-graph-results-text: comd-font(
                comd-font-weight('light'),
                comd-font-size('x-super-large', 'rem'),
                comd-line-height('xx-small')
);

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>legend text, on the x/y axes of the Revenue Risk Assessment widget</li><li>tooltip text</li></ul>
/// <p>font: 400 12px/15px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 12px; <br/> line-height: 15px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-graph-label-text-v1: comd-font(comd-font-weight('regular'), comd-font-size('x-small', 'rem'), comd-line-height('xx-small'));

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>legend text for the bars and levels of the Revenue Risk Assessment widget</li></ul>
/// <p>font: 400 11px/14px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 11px; <br/> line-height: 14px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-graph-label-text-v2: comd-font(comd-font-weight('regular'), comd-font-size('xx-small', 'rem'), comd-line-height('small'));

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>text on the bars of the Revenue Risk Assessment widget</li></ul>
/// <p>font: 600 13px/17px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 13px; <br/> line-height: 17px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-graph-label-text-v3: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('small', 'rem'),
                comd-line-height('x-large')
);

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>displaying the type of the metrics (LTR, OSAT, HEALTH) in widgets</li><li>in Revenu Risk Assessment for color legend text</li></ul>
/// <p>font: 600 12px/15px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 12px; <br/> line-height: 15px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-metrics-type-text: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('x-small', 'rem'),
                comd-line-height('xx-small')
);

/// <p>H2 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>dashboard name</li><li>some text in the widgets showing average values</li></ul>
/// <p>font: 400 30px/38px </p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 30px; <br/> line-height: 38px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h2-v1: comd-font(
                comd-font-weight('regular'),
                comd-font-size('super-large', 'rem'),
                comd-line-height('small')
);

/// <p>H2 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>reports list page title</li></ul>
/// <p>font: 400 23px/29px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 23px; <br/> line-height: 29px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h2-v2: comd-font(
                comd-font-weight('regular'),
                comd-font-size('xxxxx-large', 'rem'),
                comd-line-height('x-small')
);

/// <p>H2 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>dashboard name</li><li>some text in the widgets showing average values, on smaller screens (both tablet and mobile)</li></ul>
/// <p>font: 400 24px/31px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 24px; <br/> line-height: 31px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h2-small-screens: comd-font(
                comd-font-weight('regular'),
                comd-font-size('xxxxxx-large', 'rem'),
                comd-line-height('large')
);

/// <p>H3 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>widgets name</li><li>some of the NPS numbers (the ones in the list widgets like Comments, Recent Responses, Key Accounts.)</li></ul>
/// <p>font: 600 18px/23px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 18px; <br/> line-height: 23px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h3-v1: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('xx-large', 'rem'),
                comd-line-height('x-small')
);

/// <p>H3 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>accounts summary card titles</li></ul>
/// <p>font: 600 16px/20px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 16px; <br/> line-height: 20px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h3-v2: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('x-large', 'rem'),
                comd-line-height('xx-small')
);

/// <p>H3 type header</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>Title of the modal window</li></ul>
/// <p>font: 600 20px/24px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 20px; <br/> line-height: 24px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h3-v3: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('xxx-large', 'rem'),
                comd-line-height('xx-large')
);

/// <p>H4 type header</p>
/// <p>font: 600 14px/18px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 14px; <br/> line-height: 18px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h4-v1: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('medium', 'rem'),
                comd-line-height('medium')
);

/// <p>H4 type header</p>
/// <p>font: 600 15px/19px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 600; <br/> font-size: 15px; <br/> line-height: 19px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-h4-v2: comd-font(
                comd-font-weight('semibold'),
                comd-font-size('large', 'rem'),
                comd-line-height('x-small')
);

/// <p>H4 type header</p>
/// <p>font: 400 14px/18px</p>
/// needs to be removed as it has the same values as $comd-body-text which is inherited from the body
$comd-h4-v3: comd-font(comd-font-weight('regular'), comd-font-size('medium', 'rem'), comd-line-height('medium'));

/// <p>font: 400 15px/19px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 15px; <br/> line-height: 19px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-subtitle-text-v1: comd-font(
                comd-font-weight('regular'),
                comd-font-size('large', 'rem'),
                comd-line-height('x-small')
);

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>rating numbers in Key Accounts</li></ul>
/// <p>font: 400 21px/27px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 21px; <br/> line-height: 27px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-numbers-text-v1: comd-font(
                comd-font-weight('regular'),
                comd-font-size('xxxx-large', 'rem'),
                comd-line-height('medium')
);

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>responded/not responded nr in Response Rate widget.</li></ul>
/// <p>font: 400 20px/25px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 20px; <br/> line-height: 25px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-numbers-text-v2: comd-font(
                comd-font-weight('regular'),
                comd-font-size('xxx-large', 'rem'),
                comd-line-height('xx-small')
);

/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>small tooltip text</li></ul>
/// <p>font: 400 12px/16px</p>
/// <p style="border-left: 0.25em solid #dfe2e5; padding: 0 1em;">font-weight: 400; <br/> font-size: 12px; <br/> line-height: 16px;</p>
/// @type List
/// @see {function} comd-font
/// @see {function} comd-font-weight
/// @see {function} comd-font-size
/// @see {function} comd-line-height
$comd-tooltip-small-text: comd-font(
                comd-font-weight('regular'),
                comd-font-size('x-small', 'rem'),
                comd-line-height('x-large')
);
