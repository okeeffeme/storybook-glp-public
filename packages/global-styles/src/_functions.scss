@use "sass:math";
@import 'variables/typography';

////
/// @group Functions
/// @since 1.0
////

/// Power function
/// @param {Number} $x
/// @param {Number} $n
/// @return {Number}
@function pow($x, $n) {
  $ret: 1;

  @if $n >= 0 {
    @for $i from 1 through $n {
      $ret: $ret * $x;
    }
  } @else { /* stylelint-disable-line block-closing-brace-newline-after */
    @for $i from $n to 0 {
      $ret: $ret / $x;
    }
  }

  @return $ret;
}

/// <p>toFixed() function in Sass</p>
/// <p>The function reduces the number of digits to 3.</p>
/// @author Hugo Giraudel
/// @source https://css-tricks.com/snippets/sass/fix-number-n-digits/
/// @param {Number} $float - Number to format
/// @param {Number} $digits [3] - Number of digits to leave
/// @return {Number}
@function to-fixed($float, $digits: 3) {
  $sass-precision: 5;

  @if $digits > $sass-precision {
    @warn 'Sass sets default precision to #{$sass-precision} digits, and there is no way to change that for now.' + 'The returned number will have #{$sass-precision} digits, even if you asked for `#{$digits}`.' + 'See https://github.com/sass/sass/issues/1122 for further information.';
  }

  $pow: pow(10, $digits);
  @return math.div(round($float * $pow), $pow);
}

/// Function used to get value from a map
/// @param {Map} $map
/// @param {Key} $key
@function get-map-value($map, $key) {
  @if map-has-key($map, $key) {
    @return map-get($map, $key);
  } @else { /* stylelint-disable-line block-closing-brace-newline-after */
    @warn 'Unknown key: #{$key} in map';
  }
}


@function _length($number, $unit) {
  $strings: 'px' 'cm' 'mm' '%' 'ch' 'in' 'em' 'rem' 'pt' 'pc' 'ex' 'vw' 'vh' 'vmin' 'vmax';
  $units: 1px 1cm 1mm 1% 1ch 1in 1em 1rem 1pt 1pc 1ex 1vw 1vh 1vmin 1vmax;
  $index: index($strings, $unit);

  @if not $index {
    @warn 'Unknown unit `#{$unit}`.';
    @return false;
  }

  @return $number * nth($units, $index);
}

/// <p>number() function in Sass</p>
/// <p>The function converts string to number (e.g. 1.2, 13, -10rem, 10px)</p>
/// @author Hugo Giraudel
/// @source https://hugogiraudel.com/2014/01/15/sass-string-to-number/
/// @param {String} $string - String to parse
/// @return {Number}
@function number($string) {
  // Matrices
  $strings: '0' '1' '2' '3' '4' '5' '6' '7' '8' '9';
  $numbers: 0 1 2 3 4 5 6 7 8 9;

  // Result
  $result: 0;
  $minus: false;
  $divider: 0;

  // Looping through all characters
  @for $i from 1 through str-length($string) {
    $character: str-slice($string, $i, $i);
    $index: index($strings, $character);

    @if $character == '-' {
      $minus: true;
    } @else if $character == '.' {
      $divider: 1;
    } @else {

      @if not $index {
        $result: if($minus, $result * -1, $result);
        @return _length($result, str-slice($string, $i));
      }

      $number: nth($numbers, $index);

      @if $divider == 0 {
        $result: $result * 10;
      }
        // Decimal dot has been found
      @else {
        // Move the decimal dot to the left
        $divider: $divider * 10;
        $number: math.div($number, $divider);
      }

      $result: $result + $number;
    }

  }

  @return if($minus, $result * -1, $result);
}

/// <p>Function to transform from CSS px unit into em or rem unit. From absolute to realtive unit.</p>
/// @source https://confluence.firmglobal.com/pages/viewpage.action?pageId=48995284
/// @param {Number} $val - Number to format, which is a px value
/// @param {Unit} $unit - Relative CSS unit to transform the number into, which is em or rem
/// @require to-fixed
/// @return {Number} - a number in em or rem unit
@function comd-to-relative-unit ($val, $unit) {
  //the value should be the size in px and the unit should be em or rem
  @return #{to-fixed(math.div($val, $comd-base-font-size))}#{$unit}; // to-fix function reduces the number of decimals to 3 (default)
}

/// <p> Shortcut to convert pixels to rem </p>
/// @param {Number} $val - Number to format, which is a px value
/// @return {Number} - a number in rem unit
@function rem($val) {
  @return number(comd-to-relative-unit($val, rem));
}

/// <p>Font-size function</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>setting font-size CSS prop of a font</li></ul>
/// @param {Value} $value [medium] - Value from the $comd-font-sizes map
/// @param {Unit} $unit [px] - Transform from px
/// @require get-map-value
/// @require to-fixed
/// @require $comd-font-sizes
/// @require $comd-base-font-size
/// @return {Number} - a value from the $comd-font-sizes map
@function comd-font-size ($value: 'medium', $unit: 'px') {
  $val: get-map-value($comd-font-sizes, $value);
  @if ($unit == 'em' or $unit == 'rem') {
    @return #{to-fixed(math.div($val, $comd-base-font-size))}#{$unit}; // to-fix function reduces the number of decimals to 2 (default)
  }
  @return #{$val}#{$unit};
}

/// <p>Line-height function</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>setting line-height CSS prop of a font</li></ul>
/// @param {Value} $value [medium] - Value from the $comd-line-heights map
/// @require get-map-value
/// @return {Number} - a value from the $comd-line-heights map
@function comd-line-height($value: 'medium') {
  @return get-map-value($comd-line-heights, $value);
}

/// <p>Font-weight function</p>
/// <p style="text-decoration: underline;">Used for/in:</p>
/// <ul><li>setting font-weight prop of a font</li></ul>
/// @param {Value} $value [regular] - Value from the $comd-font-weights map
/// @require get-map-value
/// @return {Number} - a value from the $comd-font-weights map
@function comd-font-weight($value: 'regular') {
  @return get-map-value($comd-font-weights, $value);
}

/// <p>Font function</p>
/// <p>Function used to compose the font proprety, in a shorthand form: </p>
/// <p>font: font-weight font-seize/line-height font-family;</p>
/// @param {Number} $font-weight [comd-font-weight] - font-weight CSS prop
/// @param {Number} $font-size [comd-font-size('medium', 'rem')] - font-size CSS prop
/// @param {Number} $line-height [comd-line-height] - line-height CSS prop
/// @param {Number} $font-family [$comd-font-family] - font-family CSS prop
/// @require comd-font-weight
/// @require comd-font-size
/// @require comd-line-height
/// @require $comd-font-family
/// @return {List} - font-weight font-seize/line-height font-family values for the CSS font prop
@function comd-font($font-weight: comd-font-weight, $font-size: comd-font-size('medium', 'rem'), $line-height: comd-line-height, $font-family: $comd-font-family) {
  @return #{$font-weight $font-size} / #{$line-height $font-family};
}

// Convert a RGBA color with opacity, to the HEX equivalent withouth opacity as shown on a white background
// Fex:
/**
$ColorWithTransparency: rgba(18,24,33,0.87); // The HEX equivalent, when shown on a white background is #31363E
$ColorWithoutTransparency: solid-color($ColorWithTransparency); // #31363E
*/
@function solid-color($rgba, $background: #fff) {
  @return mix(rgb(red($rgba), green($rgba), blue($rgba)), $background, alpha($rgba) * 100%);
}
