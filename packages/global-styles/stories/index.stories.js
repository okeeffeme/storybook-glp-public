import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import {colors, opacities} from '../src';

import {Color} from './color';
import {Select} from '../../select/src';

storiesOf('Components/global-styles', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Colors', () => {
    return (
      <div style={{display: 'grid', gridAutoRows: '1fr', gridGap: '16px'}}>
        <Color
          color={colors.TextPrimary}
          name="TextPrimary"
          description="Primary Text, Primary Neutral Button, Hover/Active Icon Button"
          exampleUses={[
            'Text in the filter panel',
            'Numbers in widgets',
            'Widget title',
            'Table text',
            'Navigation text color',
            'Text of the drop-downs',
            'Headers and titles',
          ]}
        />

        <Color
          color={colors.TextSecondary}
          name="TextSecondary"
          description="Secondary Text, Default Icon Button"
          exampleUses={[
            'Small tool tips background',
            'Table header text',
            'Light grey text in the widgets (chart text in the Response Rate widget; item date in the Recent Responses; text of KPI widget; Account value in Key Accounts widget)',
            'Filter choices displayed in vertical filter panel',
            'Dates',
          ]}
        />

        <Color
          color={colors.TextDisabled}
          name="TextDisabled"
          description="Disabled Text, Background of Overlay"
        />

        <Color
          color={colors.Disabled}
          name="Disabled"
          description="Divider Lines, Chips, Disabled Buttons, Disabled Icon Button"
        />

        <Color
          color={colors.Background1}
          name="Background1"
          description="Icon Hover Circle, Hover Background for Items in Menus and Table Rows, Search Field, Background of Accounts Summary Cards "
        />

        <Color
          color={colors.Background2}
          name="Background2"
          description="Screen Background, Lines Between Header Bars"
        />

        <Color
          color={colors.Background3}
          name="Background3"
          description="Report SideBar"
        />

        <Color
          color={colors.PrimaryAccent}
          name="PrimaryAccent"
          description="Secondary Buttons, Links, Tab Underlines, Selected Tree, Chart Elements"
        />

        <Color
          color={colors.SecondaryAccent}
          name="SecondaryAccent"
          description="Dark accent blue (hover for chips, tags, buttons)"
        />

        <Color
          color={colors.Interactive}
          name="Interactive"
          description="Drag and Drop Indicator, Informational Alert BG, Selected Menu Item"
        />

        <Color
          color={colors.Safe}
          name="Safe"
          description="Primary Safe Button, Success Banner, Active State of Check Boxes and Radio Buttons"
        />

        <Color
          color={colors.SafeBackground}
          name="SafeBackground"
          description="Success Alert BG"
        />

        <Color
          color={colors.Warning}
          name="Warning"
          description="Warning Banner Background"
        />

        <Color
          color={colors.WarningBackground}
          name="WarningBackground"
          description="Warning Alert BG"
        />

        <Color
          color={colors.Danger}
          name="Danger"
          description="Primary Danger Buttons (i.e. Delete), Errors, Background of Error Panels, High Risk Label"
        />

        <Color
          color={colors.DangerBackground}
          name="DangerBackground"
          description="Background of Error Chips and Alerts"
        />

        <Color
          color={colors.Highlight}
          name="Highlight"
          description="Favorites Icon Selected & Search Results Character Matches"
        />

        <Color
          color={colors.PassiveBackground}
          name="PassiveBackground"
          description="NPS Passive Table Cell Background"
          exampleUses={[
            'Tag component background for Text Analytics width passive score',
          ]}
        />

        <Color
          color={colors.ChartNeutral}
          name="ChartNeutral"
          description="Graphs and charts neutral color, background in Bars-Based-Graphs (i.e thermometers)"
        />

        <Color
          color={colors.ContrastHigh}
          name="ContrastHigh"
          description="Useful for creating gradients"
        />

        <Color
          color={colors.ContrastLow}
          name="ContrastLow"
          description="Useful for creating gradients"
        />

        <Color
          color={colors.Transparent}
          name="Transparent"
          description="Fully transparent, useful for creating gradients"
        />
      </div>
    );
  })
  .add('Product Colors', () => {
    return (
      <div style={{display: 'grid', gridAutoRows: '1fr', gridGap: '16px'}}>
        <Color color={colors.Product01GreenDark} name="Product01GreenDark" />

        <Color
          color={colors.Product02Green}
          name="Product02Green"
          description="Currently used in SmartHub for hub icons"
        />

        <Color
          color={colors.Product03GreenLight}
          name="Product03GreenLight"
          description="SmarHub for hub and survey icons"
        />

        <Color
          color={colors.Product04TurquoiseDark}
          name="Product04TurquoiseDark"
        />

        <Color
          color={colors.Product05TurquoiseDark}
          name="Product05TurquoiseDark"
        />

        <Color
          color={colors.Product06Turquoise}
          name="Product06Turquoise"
          description="SmarHub for survey icons"
        />

        <Color color={colors.Product07PurpleDark} name="Product07PurpleDark" />

        <Color color={colors.Product08PurpleDark} name="Product08PurpleDark" />

        <Color
          color={colors.Product09Purple}
          name="Product09Purple"
          description="SmarHub for report icons"
        />

        <Color
          color={colors.Product10Purple}
          name="Product10Purple"
          description="SmarHub for report icons"
        />

        <Color color={colors.Product11OrangeDark} name="Product11OrangeDark" />

        <Color color={colors.Product12OrangeDark} name="Product12OrangeDark" />

        <Color color={colors.Product13Orange} name="Product13Orange" />

        <Color color={colors.Product14Orange} name="Product14Orange" />

        <Color color={colors.Product15YellowDark} name="Product15YellowDark" />

        <Color color={colors.Product16YellowDark} name="Product16YellowDark" />

        <Color color={colors.Product17Yellow} name="Product17Yellow" />
      </div>
    );
  })
  .add('Opacities', () => {
    const [opacity, setOpacity] = useState(opacities.Alt);
    return (
      <>
        <h3>
          We've added Opacity variables to use in in your SASS and JS files
        </h3>
        <p>
          To use in SASS, make use of the <code>rgba</code> function:{' '}
          <code>rgba($BaseColor, $primaryOpacity)</code>.
        </p>
        <p>
          The example in this story makes use of the css property{' '}
          <code>opacity</code>;{' '}
          <b>this generally should not be used in practice</b>, as{' '}
          <code>opacity</code> affects all child elements within the parent.
          There's example text within the sample color below to demonstrate
          this, see if you can find it.
        </p>
        <br />
        <p>
          To use in JS files, see the <b>Use variables in your js files</b>{' '}
          section of the README.
        </p>
        <div
          style={{
            margin: '20px 0',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <div
            style={{
              background: 'black',
              opacity: opacity,
              width: '200px',
            }}>
            <p>Example Child Element</p>
          </div>
          <Select
            onChange={setOpacity}
            value={opacity}
            options={Object.entries(opacities).map((val) => {
              return {
                label: `opacities.${val[0]}, ${val[1]}`,
                value: val[1],
              };
            })}
          />
        </div>
      </>
    );
  });
