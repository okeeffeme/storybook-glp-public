import React from 'react';

import Tooltip from '../../tooltip/src';

/* eslint-disable react/prop-types */
export const Color = ({color, name, description, exampleUses}) => {
  return (
    <div style={{margin: '16px', display: 'flex'}}>
      <Tooltip content="Click to copy SASS variable">
        <button
          onClick={() => {
            navigator.clipboard.writeText(`$${name}`);
          }}
          style={{
            background: color,
            width: '200px',
            height: '120px',
            position: 'relative',
            flex: 'none',
          }}>
          <code
            style={{
              position: 'absolute',
              bottom: '8px',
              right: '8px',
              color: '#fff',
              textShadow: '0 0 3px black',
            }}>
            {color}
          </code>
        </button>
      </Tooltip>
      <div style={{margin: '0 8px'}}>
        <h1>{name}</h1>
        <div></div>
        <div>{description}</div>
        {exampleUses && exampleUses.length > 0 && (
          <React.Fragment>
            <div>Example uses:</div>
            <ul
              style={{
                listStyleType: 'disc',
                listStylePosition: 'inside',
                marginTop: '8px',
              }}>
              {exampleUses.map((ex) => {
                return <li key={ex}>{ex}</li>;
              })}
            </ul>
          </React.Fragment>
        )}
      </div>
    </div>
  );
};
