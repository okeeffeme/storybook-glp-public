# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.17&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.18&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.17&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.17&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.15&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.16&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.14&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.15&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.13&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.14&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.12&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.13&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.11&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.12&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.10&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.11&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [4.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.9&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.10&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.8&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.9&targetRepoId=1246) (2023-02-27)

### Bug Fixes

- adding data-testid Toggle ([NPM-1192](https://jiraosl.firmglobal.com/browse/NPM-1192)) ([e6727b2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e6727b2c5f313eb25123ba4408c7a714edbe73f5))

## [4.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.7&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.8&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.6&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.7&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.5&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.6&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.4&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.5&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.3&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.4&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.2&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.3&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.0&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.2&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.1.0&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.1&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-toggle

# [4.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.27&sourceBranch=refs/tags/@jotunheim/react-toggle@4.1.0&targetRepoId=1246) (2023-01-11)

### Features

- add test id for LabelHelpWrapper component ([NPM-1115](https://jiraosl.firmglobal.com/browse/NPM-1115)) ([228e878](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/228e87834c97687be4c8e56a6db906a9d302a18c))

## [4.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.26&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.27&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.25&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.26&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.24&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.25&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.22&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.24&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.22&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.23&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.22&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.21&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.19&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.20&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.18&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.19&targetRepoId=1246) (2022-11-23)

### Bug Fixes

- replace MouseEvent with ChangeEvent type in onChange handler ([NPM-1102](https://jiraosl.firmglobal.com/browse/NPM-1102)) ([5ec3745](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5ec37452f732d7e2ac34213e65994e13374a517b))

## [4.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.17&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.16&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.14&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.13&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.14&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.12&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.11&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.10&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.9&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.8&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.6&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.3&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.2&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.1&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-toggle

## [4.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-toggle@4.0.0&sourceBranch=refs/tags/@jotunheim/react-toggle@4.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-toggle

# 4.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [3.6.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.6.4&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.5&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.6.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.6.3&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.4&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.6.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.6.2&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.3&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [3.6.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.6.1&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.2&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.6.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.6.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.1&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-toggle

# [3.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.8&sourceBranch=refs/tags/@confirmit/react-toggle@3.6.0&targetRepoId=1246) (2022-04-06)

### Features

- Make id property optional in Switch, Toggle and ToggleButton components ([NPM-994](https://jiraosl.firmglobal.com/browse/NPM-994)) ([b3d2356](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b3d23568079a5e7f4d1e813436a0141858e6a54e))

## [3.5.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.7&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.8&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.6&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.7&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.5&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.6&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.3&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.4&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.2&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.3&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.1&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.2&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.5.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.5.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.1&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-toggle

# [3.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.4.1&sourceBranch=refs/tags/@confirmit/react-toggle@3.5.0&targetRepoId=1246) (2021-12-07)

### Features

- add width property to Toggle component ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([0bb3eb0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0bb3eb0dcab26d376b1549f968f611a66cfc3009))

## [3.4.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.4.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.4.1&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-toggle

# [3.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.3.3&sourceBranch=refs/tags/@confirmit/react-toggle@3.4.0&targetRepoId=1246) (2021-11-23)

### Features

- add iconAlignment property ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([fb4aca6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb4aca623f0608a2b027fb781cb842027ec0e8c3))

## [3.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.3.2&sourceBranch=refs/tags/@confirmit/react-toggle@3.3.3&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [3.3.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.3.1&sourceBranch=refs/tags/@confirmit/react-toggle@3.3.2&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-toggle

# [3.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.2.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.3.0&targetRepoId=1246) (2021-09-20)

### Features

- pass extra event argument in onChange handler ([NPM-453](https://jiraosl.firmglobal.com/browse/NPM-453)) ([1b07be8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1b07be814d8f0876441d069bfbe546bacc82a80a))

# [3.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.21&sourceBranch=refs/tags/@confirmit/react-toggle@3.2.0&targetRepoId=1246) (2021-09-16)

### Features

- remove Inherited state from Toggle component (Checkbox and Radio) ([NPM-790](https://jiraosl.firmglobal.com/browse/NPM-790)) ([fee58bd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fee58bdd54ae9ecca4e51f88ca2be363ecd4e0a2))

## [3.1.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.20&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.21&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.19&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.20&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.18&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.19&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.17&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.18&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.16&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.17&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.15&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.16&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.14&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.15&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.13&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.14&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.12&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.13&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.11&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.12&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.10&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.11&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.9&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.10&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.8&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.9&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.7&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.8&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.6&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.7&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.5&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.6&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.4&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.5&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.3&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.4&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.2&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.3&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-toggle

## [3.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.1.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.1&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-toggle

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@3.0.0&sourceBranch=refs/tags/@confirmit/react-toggle@3.1.0&targetRepoId=1246) (2021-04-22)

### Features

- introduce helperText prop for Radio and Checkbox components ([NPM-598](https://jiraosl.firmglobal.com/browse/NPM-598)) ([8308d01](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8308d01707a14bc1e8f409c7abc0e5df0bd1bba0))

# [3.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.63&sourceBranch=refs/tags/@confirmit/react-toggle@3.0.0&targetRepoId=1246) (2021-04-20)

### Features

- "checked" prop for checkbox now supports a ToggleState (On/Off/Inherited/Indeterminate), in addition to true or false ([NPM-471](https://jiraosl.firmglobal.com/browse/NPM-471)) ([2b7e24c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2b7e24c8acd4e9241e80bf632ef5c735fb295417))
- add more possible states to checkboxes ([NPM-471](https://jiraosl.firmglobal.com/browse/NPM-471)) ([85d207b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85d207ba4abdf5105d1843ac95a21ef0b2ab3556))
- Add ref support and convert to TS ([NPM-471](https://jiraosl.firmglobal.com/browse/NPM-471)) ([0741d54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0741d54a67713006eea916e8277b24d34e6438bf))

### BREAKING CHANGES

- - remove "className" prop

* remove "iconLabelClassName" prop
* remove default Toggle export, only Checkbox and Radio are exported
* remove theme support, now always renders in material design

## [2.0.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.62&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.63&targetRepoId=1246) (2021-04-14)

### Bug Fixes

- remove Title Case on labels and titles ([NPM-761](https://jiraosl.firmglobal.com/browse/NPM-761)) ([0213c34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0213c343d273533335a3a64b1c736f4b3823e2d1))

## [2.0.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.61&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.62&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [2.0.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.60&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.61&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.60](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.59&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.60&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.59](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.58&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.59&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.57&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.58&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.56&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.57&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.55&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.56&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.54&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.55&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.53&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.54&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.52&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.53&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.51&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.52&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.50](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.49&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.50&targetRepoId=1246) (2020-12-11)

### Bug Fixes

- radio and checkboxes now correctly uses text-transform: capitalize on labels ([NPM-653](https://jiraosl.firmglobal.com/browse/NPM-653)) ([b6bc149](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b6bc14903ea2cc4d5c5dc1bcf6897931fc2d55d4))

## [2.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.48&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.49&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.47&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.48&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.46&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.47&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.45&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.46&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.43&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.44&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.40&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.41&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.39&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.40&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.38&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.39&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.36&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.37&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.33&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.34&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.32&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.33&targetRepoId=1246) (2020-10-13)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.31&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.32&targetRepoId=1246) (2020-10-06)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.28&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.29&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-toggle@2.0.26&sourceBranch=refs/tags/@confirmit/react-toggle@2.0.27&targetRepoId=1246) (2020-09-18)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.20](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle@2.0.19...@confirmit/react-toggle@2.0.20) (2020-09-02)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.17](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle@2.0.16...@confirmit/react-toggle@2.0.17) (2020-08-21)

### Bug Fixes

- change exports to follow es6 spec ([NPM-376](https://jiraosl.firmglobal.com/browse/NPM-376)) ([eb30ef5](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/eb30ef5e165766901f7bd41c6646f99a875acc9b))

## [2.0.14](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle@2.0.13...@confirmit/react-toggle@2.0.14) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-toggle

## [2.0.13](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-toggle@2.0.12...@confirmit/react-toggle@2.0.13) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-toggle

## CHANGELOG

### v2.0.10

- Fix: Incorrect label color.

### v2.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Refactor: restructure folder names in repository.

### v1.1.0

- Removing package version in class names.

### v1.0.7

- Fix: Add basic focus style

### v1.0.0

- **BREAKING**:
  - Need new minor version of confirmit-themes peerDependency
  - Package version as css class name is added to blocks and elements at compile time.
  - Removed props: "baseClassName", "classNames", "onIcon", "offIcon"
  - No longer supports `uncontrollable`:
    - `onChange` handler must be provided

### v0.0.7

- Refactor: Move variables from the deprecated `confirmit-css-standards` package into this project.

### v0.0.1

- Initial version
