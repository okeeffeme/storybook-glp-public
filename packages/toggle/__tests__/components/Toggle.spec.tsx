import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {Toggle, ToggleState, ToggleType} from '../../src/components/Toggle';
import {
  checkboxOff,
  checkboxOn,
  minusBox,
  radioButtonOff,
  radioButtonOn,
} from '../../../icons/src';

describe('Toggle', () => {
  it('should render with checkbox', () => {
    const onChange = jest.fn();

    render(<Toggle onChange={onChange} type={ToggleType.Checkbox} />);

    const input = screen.getByRole('checkbox');
    const icon = screen.getByTestId('icon');

    expect(input).toHaveAttribute('value', '');
    expect(icon.querySelector('path')).toHaveAttribute('d', checkboxOff);
  });

  it('should render as unchecked with ToggleState=Off', () => {
    const onChange = jest.fn();

    render(
      <Toggle checked={false} onChange={onChange} type={ToggleType.Checkbox} />
    );

    const input = screen.getByRole('checkbox');
    const icon = screen.getByTestId('icon');

    expect(input).toHaveAttribute('value', '');
    expect(icon.querySelector('path')).toHaveAttribute('d', checkboxOff);
  });

  it('should render help-icon-wrapper when helperText is set', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        type={ToggleType.Checkbox}
        helperText="help text"
      />
    );

    const labelHelpWrapper = screen.getAllByTestId('label-help-wrapper');

    expect(labelHelpWrapper.length).toBe(1);
  });

  it('should not render help-icon-wrapper when helperText is not set', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        type={ToggleType.Checkbox}
      />
    );

    const labelHelpWrapper = screen.queryByTestId('label-help-wrapper');

    expect(labelHelpWrapper).not.toBeInTheDocument();
  });

  it('should render with radio', () => {
    const onChange = jest.fn();

    render(
      <Toggle onChange={onChange} type={ToggleType.Radio}>
        radio
      </Toggle>
    );

    const input = screen.getByRole('radio');
    const icon = screen.getByTestId('icon');

    expect(input).toHaveAttribute('value', '');
    expect(icon.querySelector('path')).toHaveAttribute('d', radioButtonOff);
  });

  it('should render with no children', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        type={ToggleType.Checkbox}
      />
    );

    const labelHelpWrapper = screen.queryByTestId('label-help-wrapper');

    expect(labelHelpWrapper).not.toBeInTheDocument();
  });

  it('should render with children', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        type={ToggleType.Checkbox}>
        checkbox
      </Toggle>
    );

    const toggle = screen.getByTestId('toggle');
    const input = screen.getByRole('checkbox');
    const labelHelpWrapper = screen.queryAllByTestId('label-help-wrapper');

    expect(toggle.children[0]).toBe(toggle.querySelector('input'));
    expect(toggle.children[1]).toBe(toggle.querySelector('label'));
    expect(input).toHaveAttribute('value', '');
    expect(labelHelpWrapper.length).toBe(1);
  });

  it('should render with labelOnLeft = true', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        labelOnLeft={true}
        type={ToggleType.Checkbox}>
        label on the left
      </Toggle>
    );

    const toggle = screen.getByTestId('toggle');
    const content = screen.getByText(/label on the left/i);

    expect(toggle.classList.contains('comd-toggle--left')).toBeTruthy();
    expect(content).toBeInTheDocument();
  });

  it('should render checkbox with checked = true', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        checked
        type={ToggleType.Checkbox}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(icon.querySelector('path')).toHaveAttribute('d', checkboxOn);
  });

  it('should render checkbox with checked = On', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        checked={ToggleState.On}
        type={ToggleType.Checkbox}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(icon.querySelector('path')).toHaveAttribute('d', checkboxOn);
  });

  it('should render radio with checked = true', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        data-testid="toggle"
        onChange={onChange}
        checked={true}
        type={ToggleType.Radio}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(icon.querySelector('path')).toHaveAttribute('d', radioButtonOn);
  });

  it('should call onChange with true when input is changed when checked=false', async () => {
    let checked = false;

    const onChange = jest.fn((val) => (checked = val));

    render(
      <Toggle checked={checked} type={ToggleType.Checkbox} onChange={onChange}>
        changing
      </Toggle>
    );

    const checkbox = screen.getByRole('checkbox');

    expect(checked).toBe(false);

    userEvent.click(checkbox);

    expect(checked).toBe(true);
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should call onChange with false when input is changed when checked=true', () => {
    let checked = true;

    const onChange = jest.fn((val) => (checked = val));

    render(
      <Toggle
        id="toggle"
        checked={checked}
        type={ToggleType.Checkbox}
        onChange={onChange}>
        changing
      </Toggle>
    );

    const checkbox = screen.getByRole('checkbox');

    expect(checked).toBe(true);

    userEvent.click(checkbox);

    expect(checked).toBe(false);
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should call onChange with current value when input is changed when using ToggleState', () => {
    let checked = ToggleState.On;

    const onChange = jest.fn((val) => (checked = val));

    render(
      <Toggle
        id="toggle"
        checked={checked}
        type={ToggleType.Checkbox}
        onChange={onChange}>
        changing
      </Toggle>
    );

    const checkbox = screen.getByRole('checkbox');

    userEvent.click(checkbox);

    expect(checked).toBe(ToggleState.On);
    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should render with disabled radio', () => {
    const onChange = jest.fn();

    render(
      <Toggle onChange={onChange} disabled={true} type={ToggleType.Radio} />
    );

    const input = screen.getByRole('radio');

    expect(input).toHaveAttribute('disabled');
  });

  it('should render checkbox with Indeterminate icon', () => {
    const onChange = jest.fn();

    render(
      <Toggle
        onChange={onChange}
        checked={ToggleState.Indeterminate}
        type={ToggleType.Checkbox}
      />
    );

    const icon = screen.getByTestId('icon');

    expect(icon.querySelector('path')).toHaveAttribute('d', minusBox);
  });
});
