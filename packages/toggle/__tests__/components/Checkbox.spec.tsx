import React from 'react';
import {render, screen} from '@testing-library/react';
import {CheckBox} from '../../src/components/Checkbox';

describe('CheckBox', () => {
  it('should render with default props', () => {
    const onChange = jest.fn();

    render(<CheckBox data-testid="checkbox" onChange={onChange} />);

    expect(
      screen.getByTestId('checkbox').querySelector('[type=checkbox]')
    ).toBeInTheDocument();
  });
});
