import React from 'react';
import {render, screen} from '@testing-library/react';
import {LabelHelpWrapper} from '../../src/components/LabelHelpWrapper';

describe('LabelHelpWrapper :: ', () => {
  it('should render when there are visible children', () => {
    render(
      <LabelHelpWrapper>
        <div id="test">test</div>
      </LabelHelpWrapper>
    );

    expect(screen.getAllByText(/test/i).length).toBe(1);
  });

  it('should not render when there are no visible children', () => {
    render(
      <LabelHelpWrapper data-testid="label-help-wrapper">
        {null}
      </LabelHelpWrapper>
    );

    expect(screen.queryByTestId('label-help-wrapper')).not.toBeInTheDocument();
  });
});
