import React from 'react';
import {render, screen} from '@testing-library/react';
import {Radio} from '../../src/components/Radio';

describe('Radio', () => {
  it('should render with default props', () => {
    const onChange = jest.fn();

    render(<Radio data-testid="radio" onChange={onChange} />);

    expect(
      screen.getByTestId('radio').querySelector('[type=radio]')
    ).toBeInTheDocument();
  });
});
