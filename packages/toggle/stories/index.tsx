import React from 'react';
import {storiesOf} from '@storybook/react';
import {text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import {Radio, CheckBox, ToggleState, ToggleIconAlignment} from '../src/index';

/* eslint-disable-next-line */
const Container = ({children}) => (
  <div style={{display: 'flex', flexDirection: 'column', gap: 16}}>
    {children}
  </div>
);

storiesOf('Components/toggle', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('radio', () => {
    const [value, setValue] = React.useState('radio1');

    return (
      <Container>
        <Radio
          id="radio1"
          checked={value === 'radio1'}
          onChange={() => setValue('radio1')}>
          Label on right
        </Radio>
        <Radio
          id="radio3"
          checked={value === 'radio3'}
          onChange={() => setValue('radio3')}
          helperText={text('helperText', 'help text')}>
          Label on right with long label and helperText Label on right with long
          label and helperText Label on right with long label and helperText
          Label on right with long label and helperText Label on right with long
          label and helperText
        </Radio>
        <Radio
          id="radio7_1"
          checked={value === 'radio7_1'}
          onChange={() => setValue('radio7_1')}
          helperText={text('helperText', 'help text')}>
          Icon with start alignment with long label and helperText Icon with
          start alignment with long label and helperText Icon with start
          alignment with long label and helperText Icon with start alignment
          with long label and helperText
        </Radio>
        <Radio
          id="radio7_2"
          iconAlignment={ToggleIconAlignment.center}
          checked={value === 'radio7_2'}
          onChange={() => setValue('radio7_2')}
          helperText={text('helperText', 'help text')}>
          Icon with center alignment with long label and helperText Icon with
          center alignment with long label and helperText Icon with center
          alignment with long label and helperText Icon with center alignment
          with long label and helperText
        </Radio>
        <Radio
          id="radio7_3"
          iconAlignment={ToggleIconAlignment.end}
          checked={value === 'radio7_3'}
          onChange={() => setValue('radio7_3')}
          helperText={text('helperText', 'help text')}>
          Icon with end alignment with long label and helperText Icon with end
          alignment with long label and helperText Icon with end alignment with
          long label and helperText Icon with end alignment with long label and
          helperText
        </Radio>
        <Radio
          id="radio2"
          labelOnLeft
          checked={value === 'radio2'}
          onChange={() => setValue('radio2')}>
          Label on left
        </Radio>
        <Radio
          id="radio2_2"
          labelOnLeft
          checked={value === 'radio2_2'}
          helperText="help text"
          onChange={() => setValue('radio2_2')}>
          Label on left with helperText
        </Radio>
        <div>Radio with no label</div>
        <Radio
          id="radio4"
          checked={value === 'radio4'}
          onChange={() => setValue('radio4')}
        />
        <div>Radio with helperText and no label</div>
        <Radio
          id="radio5"
          checked={value === 'radio5'}
          onChange={() => setValue('radio5')}
          helperText="help text"
        />
        <div>Radio with labelOnLeft, helperText and no label</div>
        <Radio
          id="radio6"
          labelOnLeft
          checked={value === 'radio6'}
          onChange={() => setValue('radio6')}
          helperText="help text"
        />
      </Container>
    );
  })
  .add('checkbox', () => (
    <Container>
      <CheckBox id="checkbox1" checked onChange={() => {}}>
        Label on right
      </CheckBox>
      <CheckBox
        id="checkbox1_1"
        helperText="help text"
        checked
        onChange={() => {}}>
        Label on right with really long label and helperText Label on right with
        really long label and helperText Label on right with really long label
        and helperText Label on right with really long label and helperText
        Label on right with really long label and helperText
      </CheckBox>
      <CheckBox
        id="checkbox9_1"
        helperText="help text"
        checked
        onChange={() => {}}>
        Icon with start alignment with long label and helperText Icon with start
        alignment with long label and helperText Icon with start alignment with
        long label and helperText Icon with start alignment with long label and
        helperText
      </CheckBox>
      <CheckBox
        id="checkbox9_2"
        iconAlignment={ToggleIconAlignment.center}
        helperText="help text"
        checked
        onChange={() => {}}>
        Icon with center alignment with long label and helperText Icon with
        center alignment with long label and helperText Icon with center
        alignment with long label and helperText Icon with center alignment with
        long label and helperText
      </CheckBox>
      <CheckBox
        id="checkbox9_3"
        iconAlignment={ToggleIconAlignment.end}
        helperText="help text"
        checked
        onChange={() => {}}>
        Icon with end alignment with long label and helperText Icon with end
        alignment with long label and helperText Icon with end alignment with
        long label and helperText Icon with end alignment with long label and
        helperText
      </CheckBox>
      <CheckBox id="checkbox2" checked={false} labelOnLeft onChange={() => {}}>
        Label on left
      </CheckBox>
      <CheckBox id="checkbox3" checked disabled onChange={() => {}}>
        Checked and Disabled
      </CheckBox>
      <CheckBox id="checkbox4" disabled onChange={() => {}}>
        Unchecked and Disabled
      </CheckBox>
      <CheckBox
        id="checkbox7"
        checked={ToggleState.Indeterminate}
        onChange={() => {}}>
        Indeterminate state
      </CheckBox>
      <CheckBox
        id="checkbox8"
        disabled
        checked={ToggleState.Indeterminate}
        onChange={() => {}}>
        Indeterminate and Disabled
      </CheckBox>
    </Container>
  ))
  .add('controlled checkbox', () => {
    const [isChecked1, setIsChecked1] = React.useState(true);
    const [isChecked2, setIsChecked2] = React.useState(false);
    const [isChecked3, setIsChecked3] = React.useState<
      boolean | ToggleState.Indeterminate
    >(ToggleState.Indeterminate);
    return (
      <Container>
        <CheckBox
          id="checkbox1"
          checked={isChecked1}
          onChange={() => setIsChecked1(!isChecked1)}>
          Label on right
        </CheckBox>
        <CheckBox
          id="checkbox2"
          helperText="help text"
          checked={isChecked2}
          onChange={() => setIsChecked2(!isChecked2)}>
          Label on right with really long label and helperText Label on right
          with really long label and helperText Label on right with really long
          label and helperText Label on right with really long label and
          helperText Label on right with really long label and helperText
        </CheckBox>
        <CheckBox
          id="checkbox3"
          checked={isChecked3}
          onChange={() => setIsChecked3(!isChecked3)}>
          Indeterminate state
        </CheckBox>
      </Container>
    );
  })

  .add('disabled', () => (
    <Container>
      <CheckBox id="checkbox1" disabled onChange={() => {}}>
        disabled checkbox
      </CheckBox>
      <CheckBox id="checkbox2" disabled checked onChange={() => {}}>
        disabled checked checkbox
      </CheckBox>
      <Radio id="radio1" disabled onChange={() => {}}>
        Disabled radio
      </Radio>
      <Radio id="radio2" disabled checked onChange={() => {}}>
        Disabled checked radio
      </Radio>
    </Container>
  ));
