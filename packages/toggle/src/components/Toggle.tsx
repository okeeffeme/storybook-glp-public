import React, {
  AriaAttributes,
  forwardRef,
  ReactNode,
  Ref,
  ChangeEventHandler,
  ChangeEvent,
} from 'react';
import cn from 'classnames';
import {bemFactory} from '@jotunheim/react-themes';
import {extractDataAndAriaProps, useUuid} from '@jotunheim/react-utils';
import Tooltip from '@jotunheim/react-tooltip';

import Icon, {
  checkboxOff,
  checkboxOn,
  minusBox,
  radioButtonOff,
  radioButtonOn,
  InformationIcon,
} from '@jotunheim/react-icons';

import LabelHelpWrapper from './LabelHelpWrapper';

import classNames from './Toggle.module.css';

export enum ToggleType {
  Radio = 'radio',
  Checkbox = 'checkbox',
}

export enum ToggleState {
  On = 1,
  Off = 0,
  Indeterminate = 'Indeterminate',
}

export enum ToggleIconAlignment {
  start = 'start',
  center = 'center',
  end = 'end',
}

export enum ToggleWidth {
  auto = 'auto',
  full = 'full',
}

export type CommonToggleProps = {
  id?: string;
  name?: string;
  value?: string;
  checked?: boolean | ToggleState;
  onChange: (
    checked: boolean | ToggleState,
    event: ChangeEvent<HTMLInputElement>
  ) => void;
  children?: ReactNode;
  disabled?: boolean;
  labelOnLeft?: boolean;
  helperText?: ReactNode;
  iconAlignment?: ToggleIconAlignment;
  width?: ToggleWidth;
} & AriaAttributes;

type ToggleProps = CommonToggleProps & {
  type: ToggleType;
};

const getIcons = (type: ToggleType) => {
  return {
    checkbox: {
      onIcon: <Icon path={checkboxOn} />,
      offIcon: <Icon path={checkboxOff} />,
      indeterminateIcon: <Icon path={minusBox} />,
    },
    radio: {
      onIcon: <Icon path={radioButtonOn} />,
      offIcon: <Icon path={radioButtonOff} />,
      indeterminateIcon: <Icon path={radioButtonOff} />,
    },
  }[type];
};

const ZeroWidthSpaceChar = () => <React.Fragment>&#8203;</React.Fragment>;

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-toggle',
  classNames,
});

export const Toggle = forwardRef(
  (
    {
      id,
      type,
      name,
      value,
      checked = false,
      onChange,
      children,
      disabled,
      labelOnLeft = false,
      helperText,
      iconAlignment = ToggleIconAlignment.start,
      width = ToggleWidth.auto,
      ...rest
    }: ToggleProps,
    ref: Ref<HTMLDivElement>
  ) => {
    const uuid = useUuid();
    id = id ?? uuid;
    const {onIcon, offIcon, indeterminateIcon} = getIcons(type);

    const classes = cn(block(), {
      [type && modifier(type)]: !!type,
      [modifier('left')]: labelOnLeft,
      [modifier('disabled')]: disabled,
      [modifier(`${width}-width`)]: width !== ToggleWidth.auto,
    });

    let icon = checked === true ? onIcon : offIcon;

    switch (checked) {
      case ToggleState.On:
        icon = onIcon;
        break;

      case ToggleState.Indeterminate:
        icon = indeterminateIcon;
        break;
    }

    const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
      // pass the current value if its a string, otherwise pass the opposite (back compat)
      const arg = typeof checked === 'boolean' ? !checked : checked;

      onChange(arg, e);
    };

    const iconLabel = (
      <label
        htmlFor={id}
        className={cn(
          element('icon'),
          element('icon', `align-${iconAlignment}`),
          {
            [element('icon', 'filled')]: Boolean(checked),
          }
        )}>
        {icon}
      </label>
    );

    return (
      <div
        ref={ref}
        className={classes}
        data-testid={`toggle-${type}`}
        {...extractDataAndAriaProps(rest)}>
        <input
          id={id}
          type={type}
          data-testid="input-checked"
          value={value}
          className={element('input')}
          checked={checked === true || checked === ToggleState.On}
          disabled={disabled}
          onChange={handleChange}
          name={name}
        />
        {!labelOnLeft && iconLabel}

        <LabelHelpWrapper>
          {!!children && (
            <label htmlFor={id} className={element('text')}>
              {children}
            </label>
          )}
          {helperText && (
            <React.Fragment>
              <span className={element('nbsp')}>&nbsp;</span>
              <span
                className={element('help-icon-wrapper')}
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                }}>
                <ZeroWidthSpaceChar />
                <Tooltip content={helperText}>
                  <InformationIcon />
                </Tooltip>
              </span>
            </React.Fragment>
          )}
        </LabelHelpWrapper>

        {labelOnLeft && iconLabel}
      </div>
    );
  }
);
