import React from 'react';
import {Toggle, ToggleType, CommonToggleProps} from './Toggle';

export const Radio = (props: CommonToggleProps) => (
  <Toggle {...props} type={ToggleType.Radio} />
);

export default Radio;
