import React from 'react';
import {Toggle, ToggleType, CommonToggleProps} from './Toggle';

export const CheckBox = (props: CommonToggleProps) => (
  <Toggle {...props} type={ToggleType.Checkbox} />
);

export default CheckBox;
