import React from 'react';

import {hasVisibleChildren} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './Toggle.module.css';

const {element} = bemFactory({
  baseClassName: 'comd-toggle',
  classNames,
});

export const LabelHelpWrapper = ({children}) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return (
    <span
      data-testid="label-help-wrapper"
      className={element('label-help-wrapper')}>
      {children}
    </span>
  );
};

export default LabelHelpWrapper;
