import React from 'react';
import {render, screen} from '@testing-library/react';
import {ColorPicker} from '../../src/components/ColorPicker';

jest.mock('../../src/utils/getIsColorPickerSupported', () => ({
  getIsColorPickerSupported: jest.fn(),
}));
describe('Jotunheim React Color Picker :: ', () => {
  it('renders', () => {
    render(<ColorPicker />);
    expect(screen.getByTestId('trigger')).toBeInTheDocument();
  });

  it('does not render color-input when color picker unsupported', () => {
    render(<ColorPicker />);
    expect(screen.queryByTestId('color-input')).not.toBeInTheDocument();
  });

  it('does display selected-color when color picker unsupported', () => {
    render(<ColorPicker color={'green'} />);
    expect(screen.getByTestId('selected-color')).toHaveStyle(
      'backgroundColor : green'
    );
  });

  it('has disabled trigger when color picker unsupported', () => {
    render(<ColorPicker />);
    expect(screen.getByRole('button')).toHaveAttribute('disabled');
  });

  it('does update selected-color and color-input', () => {
    const {rerender} = render(<ColorPicker color={'red'} />);
    rerender(<ColorPicker color={'green'} />);
    expect(screen.getByTestId('selected-color')).toHaveStyle(
      'backgroundColor : green'
    );
  });
});
