import React from 'react';
import {render, screen} from '@testing-library/react';
import {ColorPicker} from '../../src/components/ColorPicker';

jest.mock('../../src/utils/getIsColorPickerSupported', () => ({
  getIsColorPickerSupported: jest.fn(() => true),
}));

describe('Jotunheim React Color Picker :: ', () => {
  it('renders', () => {
    render(<ColorPicker />);
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('does render color-input when color picker supported', () => {
    render(<ColorPicker />);
    expect(screen.getByTestId('color-input')).toBeInTheDocument();
  });

  it('does display selected-color when color picker supported', () => {
    render(<ColorPicker color={'green'} />);
    expect(screen.getByTestId('selected-color')).toHaveStyle(
      'background-color: green;'
    );
  });

  it('has enabled trigger when color picker supported', () => {
    render(<ColorPicker />);
    expect(screen.getByTestId('trigger')).not.toHaveAttribute('disabled');
  });

  it('does update selected-color and color-input', () => {
    const {rerender} = render(<ColorPicker color={'red'} />);
    rerender(<ColorPicker color={'green'} />);
    expect(screen.getByTestId('selected-color')).toHaveStyle(
      'background-color: green;'
    );
  });
});
