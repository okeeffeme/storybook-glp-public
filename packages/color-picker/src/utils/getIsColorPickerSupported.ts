let isColorPickerSupported: boolean | null = null;

export const getIsColorPickerSupported = () => {
  if (isColorPickerSupported === null) {
    const dummyColorInput = document.createElement('input');

    dummyColorInput.setAttribute('type', 'color');
    dummyColorInput.value = 'invalid';
    isColorPickerSupported = dummyColorInput.value !== 'invalid';
  }

  return isColorPickerSupported;
};
