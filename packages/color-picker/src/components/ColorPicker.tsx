import React, {ReactElement, ReactNode} from 'react';
import cn from 'classnames';
import TextField from '@jotunheim/react-text-field';

import {getIsColorPickerSupported} from '../utils/getIsColorPickerSupported';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './ColorPicker.module.css';

const {block, element} = bemFactory('comd-color-picker', classNames);

const DEFAULT_DISPLAYED_COLOR = '#ffffff';

const isColorPickerSupported = getIsColorPickerSupported();

export const ColorPicker = ({
  disabled,
  readOnly,
  required,
  name,
  onBlur,
  onChange,
  onPaste,
  color,
  label,
  helperText,
  error,
  showClear,
  ...rest
}: ColorPickerProps) => {
  const classes = cn(block());
  const colorRef = React.useRef<HTMLInputElement>(null);
  const inputRef = React.useRef<HTMLInputElement>(null);
  const showPicker = () => {
    colorRef.current?.click();
  };
  const handleChange = (value) => onChange?.(value, name);
  const displayedColor = color || DEFAULT_DISPLAYED_COLOR;
  const selectedColorStyle = {
    backgroundColor: displayedColor,
  };

  const inputs: ReactElement[] = [];

  if (isColorPickerSupported) {
    inputs.push(
      <input
        tabIndex={-1}
        key="picker"
        ref={colorRef}
        type="color"
        data-testid="color-input"
        value={displayedColor}
        className={element('colorpicker')}
        onChange={(e) => {
          inputRef.current?.focus();
          handleChange(e.target.value);
        }}
        data-colorpicker="color-input"
      />
    );
  }

  const textFieldPrefixIcon = (
    <button
      tabIndex={-1}
      key="selected"
      className={cn(element('colorpicker-trigger'), {
        [element('colorpicker-trigger', 'colorpicker-supported')]:
          isColorPickerSupported,
      })}
      onClick={showPicker}
      data-testid="trigger"
      disabled={disabled || readOnly || !isColorPickerSupported}
      data-colorpicker="trigger">
      <div
        className={element('selected-color')}
        style={selectedColorStyle}
        data-colorpicker="selected-color"
        data-testid="selected-color"
      />
    </button>
  );

  inputs.push(
    <TextField
      key="hex"
      type="text"
      ref={inputRef}
      error={error}
      helperText={helperText}
      disabled={disabled}
      readOnly={readOnly}
      required={required}
      value={color}
      label={label}
      name={name}
      onBlur={onBlur}
      onChange={handleChange}
      onPaste={onPaste}
      prefix={textFieldPrefixIcon}
      showClear={showClear}
      data-colorpicker="colorhex"
    />
  );

  return (
    <div
      className={classes}
      data-testid="color-picker"
      {...extractDataAriaIdProps(rest)}>
      <div className={element('input-wrapper')}>{inputs}</div>
    </div>
  );
};

type ColorPickerProps = {
  helperText?: ReactNode;
  error?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  name?: string;
  onBlur?: () => void;
  onChange?: (color: string, name?: string) => void;
  onPaste?: () => void;
  color?: string;
  label?: string;
  showClear?: boolean;
};

export default ColorPicker;
