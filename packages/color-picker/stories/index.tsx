import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, text} from '@storybook/addon-knobs';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import ColorPicker from '../src/components/ColorPicker';

storiesOf('Components/ColorPicker', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => {
    const [state, setState] = React.useState('#FFAB02');

    return (
      <div
        style={{
          padding: '50px',
          height: '400px',
          backgroundColor: '#f1f1f1',
        }}>
        <div style={{width: 400}}>
          <ColorPicker
            name={text('name', 'control name')}
            helperText={text('helperText', 'helper text')}
            error={boolean('error', false)}
            label={text('label', 'input label')}
            disabled={boolean('disabled', false)}
            readOnly={boolean('readOnly', false)}
            required={boolean('required', false)}
            showClear={boolean('showClear', false)}
            color={state}
            onChange={(newColor) => {
              setState(newColor);
            }}
          />
        </div>
      </div>
    );
  });
