import React, {ReactNode} from 'react';
import debounce from 'lodash/debounce';
import {storiesOf} from '@storybook/react';
import {boolean, object, select} from '@storybook/addon-knobs';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import AutosaveNotification, {
  AUTOSAVE_SAVING_DELAY,
  AUTOSAVE_STATUS,
} from '../src';
import {defaultTexts} from '../src/components/AutosaveNotification';
import {TextField} from '../../text-field/src';

type AppStateType = {
  status?: AUTOSAVE_STATUS;
  showUndo?: boolean;
  undoEnabled?: boolean;
  onUndo?: () => void;
  onSave?: () => void;
  autoSaveOn?: boolean;
  showAutoSaveToggle?: boolean;
  onToggleAutoSave?: (autoSaveOn) => void;
};

type AppPropsType = {
  showAutoSaveToggle?: boolean;
  children: (AppStateType) => ReactNode;
};

const App = class extends React.Component<AppPropsType, AppStateType> {
  private readonly _debouncedSave: ReturnType<typeof debounce>;

  constructor(props) {
    super(props);

    this._debouncedSave = debounce(this.save, AUTOSAVE_SAVING_DELAY).bind(this);

    this.state = {
      autoSaveOn: true,
      showAutoSaveToggle: this.props.showAutoSaveToggle,
      onToggleAutoSave: this.onToggleAutoSave,
      onSave: this.save,
    };
  }

  onToggleAutoSave = (autoSaveOn) => {
    this.setState({
      autoSaveOn,
    });
  };

  save = () => {
    this.setState({
      status: AUTOSAVE_STATUS.saving,
    });

    // do some save call here
    // wait for response from server about save, and set to saved
    setTimeout(() => {
      this.setState({
        status: AUTOSAVE_STATUS.notChanged,
      });
    }, 2000);
  };

  onChange = () => {
    this.setState({
      status: AUTOSAVE_STATUS.changed,
    });

    if (this.state.autoSaveOn) {
      this._debouncedSave();
    }
  };

  render() {
    return (
      <div>
        <div style={{marginBottom: '20px'}}>
          <h1>Manually trigger state changes</h1>
          <button
            onClick={() =>
              this.setState({
                status: AUTOSAVE_STATUS.changed,
                undoEnabled: true,
              })
            }>
            Toggle "changes made" (default)
          </button>

          <button
            onClick={() => this.setState({status: AUTOSAVE_STATUS.saving})}>
            Toggle "saving..."
          </button>

          <button
            onClick={() =>
              this.setState({
                status: AUTOSAVE_STATUS.notChanged,
                undoEnabled: false,
              })
            }>
            Toggle "saved/not changed" (default)
          </button>

          <button
            onClick={() =>
              this.setState({
                status: AUTOSAVE_STATUS.notChanged,
                undoEnabled: true,
                onUndo: () => {
                  alert('you clicked undo!');
                },
              })
            }>
            Toggle "saved/not changed" with undo
          </button>
        </div>

        <div>
          <h1>Autosave on change example</h1>
          <input
            placeholder="type to trigger autosave"
            onChange={this.onChange}
          />
          <input
            placeholder="type to trigger autosave"
            onChange={this.onChange}
          />
        </div>

        <div style={{padding: 20, margin: 40, border: '1px solid #ccc'}}>
          {this.props.children(this.state)}
        </div>
      </div>
    );
  }
};

const Container = ({children}) => (
  <div style={{padding: 20, margin: 40, border: '1px solid #ccc'}}>
    {children}
  </div>
);

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
App.displayName = 'DummyApp';

storiesOf('Components/autosave-notification', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default - with knobs', () => {
    return (
      <Container>
        <AutosaveNotification
          status={select('status', AUTOSAVE_STATUS, AUTOSAVE_STATUS.notChanged)}
          showUndo={boolean('showUndo', true)}
          showRedo={boolean('showRedo', true)}
          undoDisabled={boolean('undoDisabled', false)}
          redoDisabled={boolean('redoDisabled', false)}
          showAutoSaveToggle={boolean('showAutoSaveToggle', false)}
          autoSaveOn={boolean('autoSaveOn', true)}
          showDivider={boolean('showDivider', true)}
          texts={object('texts', {
            ...defaultTexts,
            lastSaved: 'Last saved today at 11.05 AM',
          })}
        />
      </Container>
    );
  })
  .add('With manual save', () => {
    const [status, setStatus] = React.useState(AUTOSAVE_STATUS.notChanged);
    const [saveDate, setSaveDate] = React.useState<Date>(new Date());
    const [text, setText] = React.useState('');

    const onSave = React.useCallback(() => {
      setStatus(AUTOSAVE_STATUS.saving);

      setTimeout(() => {
        setSaveDate(new Date());
        setStatus(AUTOSAVE_STATUS.notChanged);
      }, 2000);
    }, []);

    const onChange = React.useCallback((v) => {
      setText(v);
      setStatus(AUTOSAVE_STATUS.changed);
    }, []);

    const texts = React.useMemo(
      () => ({
        lastSaved: `Last saved today at ${saveDate.getMinutes()}:${saveDate.getSeconds()}`,
      }),
      [saveDate]
    );

    return (
      <Container>
        <AutosaveNotification
          status={status}
          autoSaveOn={false}
          onSave={onSave}
          texts={texts}
        />
        <br />
        <TextField onChange={onChange} value={text} label={'Name'} />
      </Container>
    );
  })
  .add('Without auto-save toggle enabled', () => {
    const texts = React.useMemo(
      () => ({
        lastSaved: 'Last saved today at 11.05 AM',
      }),
      []
    );

    return (
      <Container>
        <App>
          {(state) => <AutosaveNotification {...state} texts={texts} />}
        </App>
      </Container>
    );
  })
  .add('With auto-save toggle enabled', () => {
    const texts = React.useMemo(
      () => ({
        lastSaved: 'Last saved today at 11.05 AM',
      }),
      []
    );

    return (
      <Container>
        <App showAutoSaveToggle={true}>
          {(state) => <AutosaveNotification {...state} texts={texts} />}
        </App>
      </Container>
    );
  });
