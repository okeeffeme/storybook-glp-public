# Jotunheim React Autosave Notification

A tiny autosave notification that can be used to show when auto-save is happening and when the data is saved.
