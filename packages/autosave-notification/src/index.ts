import AutosaveToaster from './components/AutosaveNotification';
import {AUTOSAVE_STATUS} from './types';

export {AUTOSAVE_STATUS};

export const AUTOSAVE_SAVING_DELAY = 1500;

export default AutosaveToaster;
