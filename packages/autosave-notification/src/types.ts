import {ReactNode} from 'react';

export const SAVED_STATUS = 'saved';

export enum AUTOSAVE_STATUS {
  saving = 'saving',
  changed = 'changed',
  notChanged = 'not-changed',
}

export type AutosaveTexts = {
  saveButton?: ReactNode;
  undoButton?: ReactNode;
  redoButton?: ReactNode;
  lastSaved?: string;
  changedMessage?: string;
  savedMessage?: string;
  savingMessage?: string;
  autoSaveToggle?: string;
  autoSaveOff?: string;
};
