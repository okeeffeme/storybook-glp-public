import {AUTOSAVE_STATUS, AutosaveTexts, SAVED_STATUS} from './types';

export const getMessage = (messages: AutosaveTexts, status, autoSaveOn) => {
  if (!autoSaveOn && status === AUTOSAVE_STATUS.notChanged) {
    return messages.autoSaveOff;
  }

  return {
    [SAVED_STATUS]: messages.savedMessage,
    [AUTOSAVE_STATUS.notChanged]: messages.savedMessage,
    [AUTOSAVE_STATUS.saving]: messages.savingMessage,
    [AUTOSAVE_STATUS.changed]: messages.changedMessage,
  }[status];
};

export const shouldShowUndo = (
  showUndo: boolean,
  status: AUTOSAVE_STATUS,
  autoSaveOn: boolean
) => {
  if (!showUndo) {
    return false;
  }

  if (!autoSaveOn) {
    return status !== AUTOSAVE_STATUS.changed;
  }

  return true;
};
