import React, {AriaAttributes, useEffect, useRef, useState} from 'react';

import {
  extractDataAriaIdProps,
  hasVisibleChildren,
} from '@jotunheim/react-utils';

import Switch from '@jotunheim/react-switch';
import Tooltip, {TriggerTypes} from '@jotunheim/react-tooltip';
import Popover from '@jotunheim/react-popover';
import Fieldset from '@jotunheim/react-fieldset';
import {Appearances, Button, IconButton} from '@jotunheim/react-button';
import {
  check,
  contentSave,
  Icon,
  sync,
  undo,
  redo,
} from '@jotunheim/react-icons';
import {bemFactory} from '@jotunheim/react-themes';

import {AUTOSAVE_STATUS, AutosaveTexts, SAVED_STATUS} from '../types';
import {getMessage, shouldShowUndo} from '../utils';

import classNames from './AutoSaveNotification.module.css';

export type AutosaveNotificationProps = {
  status?: AUTOSAVE_STATUS;
  showUndo?: boolean;
  showRedo?: boolean;
  onUndo?: () => void;
  onRedo?: () => void;
  onSave?: () => void;
  texts?: AutosaveTexts;
  showDivider?: boolean;
  autoSaveOn?: boolean;
  undoDisabled?: boolean;
  redoDisabled?: boolean;
  showAutoSaveToggle?: boolean;
  onToggleAutoSave?: (boolean) => void;
} & AriaAttributes;

export const defaultTexts = {
  undoButton: 'Undo',
  redoButton: 'Redo',
  saveButton: 'Save',
  lastSaved: '',
  autoSaveToggle: 'Auto-save',
  changedMessage: 'Changes made...',
  savingMessage: 'Saving...',
  savedMessage: 'Saved',
  autoSaveOff: 'Auto-save Off',
};

const noopFunc = () => {};
const noopObject = {};
const {block, element} = bemFactory('comd-autosave', classNames);

const AutosaveNotification = ({
  status = AUTOSAVE_STATUS.notChanged,
  showDivider = true,
  showUndo = false,
  showRedo = false,
  undoDisabled = false,
  redoDisabled = false,
  showAutoSaveToggle = false,
  onUndo = noopFunc,
  onRedo = noopFunc,
  onSave = noopFunc,
  texts = noopObject,
  autoSaveOn = true,
  onToggleAutoSave = noopFunc,
  ...rest
}: AutosaveNotificationProps) => {
  const prevStatus = useRef<AUTOSAVE_STATUS>(status);
  const [dataStatus, setDataStatus] = useState<string>(status);
  const [messages, setMessages] = useState<AutosaveTexts>(defaultTexts);

  useEffect(() => {
    setDataStatus(status);
  }, [status]);

  useEffect(() => {
    setMessages({
      ...defaultTexts,
      ...texts,
    });
  }, [texts]);

  useEffect(() => {
    switch (status) {
      case AUTOSAVE_STATUS.notChanged:
        if (prevStatus.current === AUTOSAVE_STATUS.saving) {
          setDataStatus(SAVED_STATUS);
        }
        break;
    }

    prevStatus.current = status;
  }, [status]);

  return (
    <div
      data-testid="autosave-notification"
      className={block()}
      {...extractDataAriaIdProps(rest)}
      data-autosave-notification={true}
      data-autosave-state={dataStatus}>
      <Popover
        trigger={TriggerTypes.hover}
        closeOnOutsideClick={false}
        content={
          <Fieldset layoutStyle={Fieldset.LayoutStyle.Vertical}>
            {messages.lastSaved && <div>{messages.lastSaved}</div>}
            {showAutoSaveToggle && (
              <Switch
                id="autoSave"
                label={messages.autoSaveToggle}
                checked={autoSaveOn}
                onChange={(e) => onToggleAutoSave(e.currentTarget.checked)}
              />
            )}
          </Fieldset>
        }>
        <div className={element('message')}>
          <Button
            data-autosave-message
            suffix={getSuffix(dataStatus, autoSaveOn, element)}
            appearance={Appearances.tertiary}>
            {getMessage(messages, dataStatus, autoSaveOn)}
          </Button>
        </div>
      </Popover>

      <Buttons element={element}>
        {shouldShowUndo(showUndo, status, autoSaveOn) && (
          <Tooltip content={messages.undoButton}>
            <IconButton
              data-autosave-action="undo"
              onClick={onUndo}
              disabled={undoDisabled || status === AUTOSAVE_STATUS.saving}>
              <Icon path={undo} />
            </IconButton>
          </Tooltip>
        )}
        {shouldShowUndo(showRedo, status, autoSaveOn) && (
          <Tooltip content={messages.redoButton}>
            <IconButton
              data-autosave-action="redo"
              onClick={onRedo}
              disabled={redoDisabled || status === AUTOSAVE_STATUS.saving}>
              <Icon path={redo} />
            </IconButton>
          </Tooltip>
        )}

        {status === AUTOSAVE_STATUS.changed && !autoSaveOn && (
          <Tooltip content={messages.saveButton}>
            <IconButton data-autosave-action="save" onClick={onSave}>
              <Icon className={element('icon-save')} path={contentSave} />
            </IconButton>
          </Tooltip>
        )}
      </Buttons>

      {showDivider && <div className={element('divider')} />}
    </div>
  );
};

const Buttons = ({element, children}) => {
  if (!hasVisibleChildren(children)) {
    return null;
  }

  return <div className={element('buttons')}>{children}</div>;
};

const getSuffix = (status, autoSaveOn, element) => {
  if (!autoSaveOn && status === AUTOSAVE_STATUS.notChanged) {
    return null;
  }

  if ([AUTOSAVE_STATUS.notChanged, SAVED_STATUS].includes(status)) {
    return <Icon path={check} />;
  }

  if (status === AUTOSAVE_STATUS.saving) {
    return <Icon path={sync} className={element('icon-syncing')} />;
  }

  return undefined;
};

export default AutosaveNotification;
