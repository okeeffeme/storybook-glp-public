# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.48&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.49&targetRepoId=1246) (2023-04-12)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.48&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.48&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.46&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.47&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.45&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.46&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.44&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.45&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.43&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.44&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.42&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.43&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.41&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.42&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [6.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.40&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.41&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.38&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.40&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.38&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.39&targetRepoId=1246) (2023-02-17)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.37&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.38&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.36&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.37&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.35&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.36&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.34&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.35&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.33&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.34&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.32&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.33&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.31&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.32&targetRepoId=1246) (2023-01-25)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.30&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.31&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.29&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.30&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.27&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.29&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.27&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.28&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.26&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.27&targetRepoId=1246) (2023-01-11)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.25&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.26&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.24&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.25&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.23&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.24&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.21&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.23&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.21&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.22&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.18&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.21&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.18&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.20&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.18&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.19&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.17&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.18&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.16&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.17&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.14&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.15&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.13&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.14&targetRepoId=1246) (2022-10-11)

### Bug Fixes

- remove className usage of Button from AutosaveNotification ([NPM-929](https://jiraosl.firmglobal.com/browse/NPM-929)) ([fe5a68e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fe5a68ea95a31064c7855f5ad6e5b32ad70f7a50))

## [6.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.12&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.13&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.11&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.12&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.10&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.11&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.9&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.10&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.8&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.9&targetRepoId=1246) (2022-09-07)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.6&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.3&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.2&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.1&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

## [6.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.0&sourceBranch=refs/tags/@jotunheim/react-autosave-notification@6.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-autosave-notification

# 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.2.74](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.73&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.74&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.73](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.72&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.73&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.72](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.71&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.72&targetRepoId=1246) (2022-06-02)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.71](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.70&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.71&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.2.70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.69&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.70&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.69](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.68&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.69&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.66](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.65&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.66&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.65](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.64&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.65&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.64](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.63&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.64&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.63](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.62&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.63&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.61&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.62&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.61](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.60&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.61&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.58](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.57&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.58&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.57](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.56&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.57&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.56](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.55&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.56&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.54&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.55&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.54](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.53&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.54&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.53](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.52&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.53&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.52](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.51&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.52&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [5.2.51](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.50&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.51&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.49](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.48&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.49&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.48](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.47&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.48&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.47](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.46&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.47&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.46](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.45&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.46&targetRepoId=1246) (2021-09-06)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.45](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.44&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.45&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.44](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.43&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.44&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.43](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.42&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.43&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.41&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.42&targetRepoId=1246) (2021-07-26)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.40&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.41&targetRepoId=1246) (2021-07-23)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.39&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.40&targetRepoId=1246) (2021-07-22)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.38&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.39&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.37&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.38&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.36&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.37&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.35&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.36&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.34&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.35&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.33&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.34&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.32&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.33&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.31&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.32&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.30&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.31&targetRepoId=1246) (2021-06-01)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.29&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.30&targetRepoId=1246) (2021-05-31)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.28&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.29&targetRepoId=1246) (2021-05-27)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.27&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.28&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.26&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.27&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.25&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.26&targetRepoId=1246) (2021-05-17)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.23&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.24&targetRepoId=1246) (2021-04-29)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.22&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.23&targetRepoId=1246) (2021-04-20)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.21&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.22&targetRepoId=1246) (2021-04-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.20&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.21&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [5.2.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.19&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.20&targetRepoId=1246) (2021-04-06)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.18&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.19&targetRepoId=1246) (2021-03-22)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.17&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.18&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.16&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.17&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.15&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.16&targetRepoId=1246) (2021-03-15)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.14&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.15&targetRepoId=1246) (2021-03-10)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.13&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.14&targetRepoId=1246) (2021-03-05)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.12&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.13&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.11&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.12&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.9&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.10&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.8&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.9&targetRepoId=1246) (2021-01-27)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.7&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.8&targetRepoId=1246) (2021-01-21)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.6&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.7&targetRepoId=1246) (2021-01-19)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.5&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.6&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.4&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.5&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.3&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.4&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.2&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.3&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.1&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.2&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.2.0&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.1&targetRepoId=1246) (2020-12-10)

**Note:** Version bump only for package @confirmit/react-autosave-notification

# [5.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.10&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.2.0&targetRepoId=1246) (2020-12-10)

### Features

- Add Redu button ([NPM-636](https://jiraosl.firmglobal.com/browse/NPM-636)) ([96bd7dc](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/96bd7dce32242ed3dbb343773cfa5837fd3e29e1))

## [5.1.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.9&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.10&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.1.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.8&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.9&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.5&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.6&targetRepoId=1246) (2020-12-03)

### Bug Fixes

- update to use the new popover, to get correct behavior by default (no need to handle hover state separately) ([NPM-635](https://jiraosl.firmglobal.com/browse/NPM-635)) ([2cb3670](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2cb36705c51073a1e4e91edb9010be73899d4c07))

## [5.1.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.2&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.3&targetRepoId=1246) (2020-11-23)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.1.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.1&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.2&targetRepoId=1246) (2020-11-20)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [5.1.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@5.1.0&sourceBranch=refs/tags/@confirmit/react-autosave-notification@5.1.1&targetRepoId=1246) (2020-11-19)

**Note:** Version bump only for package @confirmit/react-autosave-notification

### v5.1.0

- Show "Auto-save Off" message instead of Saved when auto-save is off and no changes have been made

### v5.0.0

### BREAKING CHANGES

- Upgrade auto-save to latest spec (NPM-590)
- showSave prop is removed, save button behavior is controlled by new "autoSaveOn" prop, which defaults to true
- component is no longer based on Snackbar messages, but rather is displayed as a tertiary button, with a Popover on hover to show timestamp and auto-save toggle (if enabled)
- many other changes...

# [4.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@3.1.6&sourceBranch=refs/tags/@confirmit/react-autosave-notification@4.0.0&targetRepoId=1246) (2020-11-11)

### Features

- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))
- add @confirmit/react-themes as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([bac8574](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bac85740887525b06c80a7cca1acdd8586e850d6))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)

## [3.1.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@3.1.5&sourceBranch=refs/tags/@confirmit/react-autosave-notification@3.1.6&targetRepoId=1246) (2020-11-06)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [3.1.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@3.1.4&sourceBranch=refs/tags/@confirmit/react-autosave-notification@3.1.5&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [3.1.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-autosave-notification@3.1.3&sourceBranch=refs/tags/@confirmit/react-autosave-notification@3.1.4&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## [3.1.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-autosave-notification@3.1.2...@confirmit/react-autosave-notification@3.1.3) (2020-08-28)

**Note:** Version bump only for package @confirmit/react-autosave-notification

# [3.1.2]

### Features

- Message some times not displayed when toggling quickly between notChanged and changed

# [3.1.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-autosave-notification@3.0.1...@confirmit/react-autosave-notification@3.1.0) (2020-08-13)

### Features

- Hide message immediately when status changes from changed to notChanged

# [3.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-autosave-notification@3.0.1...@confirmit/react-autosave-notification@3.1.0) (2020-08-13)

### Features

- add virtual 'saved' status into the data attribute ([NPM-263](https://jiraosl.firmglobal.com/browse/NPM-263)) ([439f414](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/439f41436e214d1f88bd529aa436828529ad903d))

## [3.0.1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-autosave-notification@3.0.0...@confirmit/react-autosave-notification@3.0.1) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-autosave-notification

## CHANGELOG

### v3.0.0

- **BREAKING**
  - The component appearance is updated to correspond the DS specs
  - New component `@confirmit/react-snackbar` is used
  - Prop `className` removed
  - Props `canUndo` renamed to `showUndo`
- Typescript support
- The constant `AUTOSAVE_SAVING_DELAY` (=1500ms) added
- New props added: `showSave`, `onSave`, `texts`
- Refactor: new name `@confirmit/react-autosave-notification`

### v2.0.0

- **BREAKING**
  - peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
  - Remove default theme
  - Removed props
    - `successIcon`
    - `undoIcon`
- Refactor: new name `@confirmit/react-autosave-toaster`

### v1.1.0

- Removing package version in class names.

### v1.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.
- **BREAKING** Require a new peerDependency `@confirmit/react-transition-portal`

### v0.2.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v0.1.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v0.0.2

- Fix: lint error

### v0.0.1

- Initial version
