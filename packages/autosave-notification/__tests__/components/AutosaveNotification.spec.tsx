import React from 'react';
import {mount} from 'enzyme';

import AutosaveNotification, {
  AutosaveNotificationProps,
  defaultTexts,
} from '../../src/components/AutosaveNotification';

import {AUTOSAVE_STATUS, SAVED_STATUS} from '../../src/types';

const ACTION_BUTTON = (action: string) =>
  `button[data-autosave-action="${action}"]`;
const AUTOSAVE_DIVIDER = 'div.comd-autosave__divider';
const AUTOSAVE_MESSAGE = 'button[data-autosave-message]';
const AUTOSAVE_NOTIFICATION = 'div[data-autosave-notification]';
const getStatusSelector = (status: AUTOSAVE_STATUS | 'saved') =>
  `div[data-autosave-state="${status}"]`;

const render = (props: AutosaveNotificationProps = {}) =>
  mount(<AutosaveNotification {...props} />);

describe('Jotunheim React Autosave Notification :: ', () => {
  let component;

  const setProps = (props: AutosaveNotificationProps) => {
    component.setProps(props);
    component.update();
  };

  afterEach(() => {
    component.unmount();
  });

  it('should show saved message when not-changed', () => {
    component = render({
      status: AUTOSAVE_STATUS.notChanged,
    });

    expect(component.find(AUTOSAVE_MESSAGE).text()).toBe(
      defaultTexts.savedMessage
    );
  });

  it('should show autoSaveOff message when not-changed, and auto-save is off', () => {
    component = render({
      status: AUTOSAVE_STATUS.notChanged,
      autoSaveOn: false,
    });

    expect(component.find(AUTOSAVE_MESSAGE).text()).toBe(
      defaultTexts.autoSaveOff
    );
  });

  it('should render with correct status', () => {
    component = render({
      status: AUTOSAVE_STATUS.changed,
    });

    expect(component.find(AUTOSAVE_NOTIFICATION).exists()).toBe(true);

    expect(
      component.find(getStatusSelector(AUTOSAVE_STATUS.changed)).length
    ).toBe(1);

    expect(component.find(AUTOSAVE_MESSAGE).text()).toBe(
      defaultTexts.changedMessage
    );
  });

  it('should show Saved when changing from "saving" to "not-changed" ', () => {
    // arrange
    component = render({status: AUTOSAVE_STATUS.saving});
    expect(component.find(AUTOSAVE_MESSAGE).text()).toBe(
      defaultTexts.savingMessage
    );

    // act
    setProps({status: AUTOSAVE_STATUS.notChanged});

    // assert
    expect(component.find(getStatusSelector(SAVED_STATUS)).exists()).toBe(true);
    expect(component.find(AUTOSAVE_NOTIFICATION).text()).toBe(
      defaultTexts.savedMessage
    );
  });

  it('should not show Saved when changing from "changed" to "not-changed"', () => {
    // arrange
    component = render({status: AUTOSAVE_STATUS.changed});
    expect(component.find(AUTOSAVE_NOTIFICATION).text()).toBe(
      defaultTexts.changedMessage
    );

    // act
    setProps({status: AUTOSAVE_STATUS.notChanged});

    // assert
    expect(component.find(AUTOSAVE_NOTIFICATION).text()).toBe(
      defaultTexts.savedMessage
    );
    expect(
      component.find(getStatusSelector(AUTOSAVE_STATUS.notChanged)).exists()
    ).toBe(true);
    expect(component.find(getStatusSelector(SAVED_STATUS)).exists()).toBe(
      false
    );
  });

  it('should show divider by default', () => {
    component = render({});

    expect(component.find(AUTOSAVE_DIVIDER).exists()).toBe(true);
  });

  it('should not show divider when turned off', () => {
    component = render({
      showDivider: false,
    });

    expect(component.find(AUTOSAVE_DIVIDER).exists()).toBe(false);
  });

  describe('Undo button :: ', () => {
    it('should not show quick undo by default', () => {
      setProps({status: AUTOSAVE_STATUS.changed});
      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(false);

      setProps({status: AUTOSAVE_STATUS.saving});
      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(false);

      component = render({status: AUTOSAVE_STATUS.notChanged});
      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(false);
    });

    it('should show quick undo when turned on', () => {
      component = render({
        showUndo: true,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('undo')).prop('disabled')).toBe(
        false
      );
    });

    it('should show quick as disabled undo while saving', () => {
      component = render({
        showUndo: true,
        status: AUTOSAVE_STATUS.saving,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('undo')).prop('disabled')).toBe(true);
    });

    it('should show quick undo when changes are made', () => {
      component = render({
        showUndo: true,
        status: AUTOSAVE_STATUS.changed,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(true);
    });

    it('should not show quick undo while changes are made if auto-save is off', () => {
      component = render({
        showUndo: true,
        status: AUTOSAVE_STATUS.changed,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(false);
    });

    it('should show quick undo as disabled while saving if auto-save is on', () => {
      component = render({
        showUndo: true,
        status: AUTOSAVE_STATUS.saving,
        autoSaveOn: true,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('undo')).prop('disabled')).toBe(true);
    });

    it('should show quick undo as disabled while saving if auto-save is off', () => {
      component = render({
        showUndo: true,
        status: AUTOSAVE_STATUS.saving,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('undo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('undo')).prop('disabled')).toBe(true);
    });
  });

  describe('Redo button :: ', () => {
    it('should not show redo by default', () => {
      setProps({status: AUTOSAVE_STATUS.changed});
      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(false);

      setProps({status: AUTOSAVE_STATUS.saving});
      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(false);

      component = render({status: AUTOSAVE_STATUS.notChanged});
      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(false);
    });

    it('should show quick redo when turned on', () => {
      component = render({
        showRedo: true,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('redo')).prop('disabled')).toBe(
        false
      );
    });

    it('should show quick as disabled redo while saving', () => {
      component = render({
        showRedo: true,
        status: AUTOSAVE_STATUS.saving,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('redo')).prop('disabled')).toBe(true);
    });

    it('should show quick redo when changes are made', () => {
      component = render({
        showRedo: true,
        status: AUTOSAVE_STATUS.changed,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(true);
    });

    it('should not show quick redo while changes are made if auto-save is off', () => {
      component = render({
        showRedo: true,
        status: AUTOSAVE_STATUS.changed,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(false);
    });

    it('should show quick redo as disabled while saving if auto-save is on', () => {
      component = render({
        showRedo: true,
        status: AUTOSAVE_STATUS.saving,
        autoSaveOn: true,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('redo')).prop('disabled')).toBe(true);
    });

    it('should show quick redo as disabled while saving if auto-save is off', () => {
      component = render({
        showRedo: true,
        status: AUTOSAVE_STATUS.saving,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('redo')).exists()).toBe(true);
      expect(component.find(ACTION_BUTTON('redo')).prop('disabled')).toBe(true);
    });
  });

  describe('Save button :: ', () => {
    it('should not show save button by default', () => {
      component = render();

      expect(component.find(ACTION_BUTTON('save')).exists()).toBe(false);
    });

    it('should show save button when autoSave is off while changes are made', () => {
      component = render({
        status: AUTOSAVE_STATUS.changed,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('save')).exists()).toBe(true);
    });

    it('should not show save button when autoSave is off while not changed', () => {
      component = render({
        status: AUTOSAVE_STATUS.notChanged,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('save')).exists()).toBe(false);
    });

    it('should not show save button when autoSave is off while saving', () => {
      component = render({
        status: AUTOSAVE_STATUS.saving,
        autoSaveOn: false,
      });

      expect(component.find(ACTION_BUTTON('save')).exists()).toBe(false);
    });
  });
});
