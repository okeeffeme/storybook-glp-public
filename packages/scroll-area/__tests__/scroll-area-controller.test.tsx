import React from 'react';
import {screen, render} from '@testing-library/react';
import ScrollArea from '../src/scroll-area';
import ScrollAreaController from '../src/scroll-area-controller';

let items;

beforeAll(() => {
  items = [...Array(50).keys()].map((n) => <div key={n}>Item {n}!</div>);
});

describe('ScrollAreaController', () => {
  it('should scroll to top', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);
    scrollArea.scrollTop = 450;

    expect(scrollArea.scrollTop).toBe(450);

    scrollAreaController.scrollToTop();

    expect(scrollArea.scrollTop).toBe(0);
  });

  it('should scroll to bottom', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    expect(scrollArea.scrollTop).toBe(0);

    scrollAreaController.scrollToBottom();

    expect(scrollArea.scrollTop).toBe(scrollArea.scrollHeight);
  });

  it('should scroll to left', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    scrollArea.scrollLeft = 50;

    expect(scrollArea.scrollLeft).toBe(50);

    scrollAreaController.scrollToLeft();

    expect(scrollArea.scrollLeft).toBe(0);
  });

  it('should scroll to right', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    expect(scrollArea.scrollLeft).toBe(0);

    scrollAreaController.scrollToRight();

    expect(scrollArea.scrollLeft).toBe(scrollArea.scrollWidth);
  });

  it('should scroll by X axis in addition', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    scrollArea.scrollLeft = 50;

    expect(scrollArea.scrollLeft).toBe(50);

    scrollAreaController.scrollX(100);

    expect(scrollArea.scrollLeft).toBe(150);
  });

  it('should scroll by Y axis in addition', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    scrollArea.scrollTop = 50;

    expect(scrollArea.scrollTop).toBe(50);

    scrollAreaController.scrollY(100);

    expect(scrollArea.scrollTop).toBe(150);
  });

  it('should scroll by X axis', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    scrollArea.scrollLeft = 50;

    expect(scrollArea.scrollLeft).toBe(50);

    scrollAreaController.scrollXTo(100);

    expect(scrollArea.scrollLeft).toBe(100);
  });

  it('should scroll by Y axis', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');
    const scrollAreaController = new ScrollAreaController(scrollArea);

    scrollArea.scrollTop = 50;

    expect(scrollArea.scrollTop).toBe(50);

    scrollAreaController.scrollYTo(100);

    expect(scrollArea.scrollTop).toBe(100);
  });
});
