import React from 'react';
import {screen, render} from '@testing-library/react';
import ScrollArea from '../src/scroll-area';

let items;

beforeAll(() => {
  items = [...Array(50).keys()].map((n) => <div key={n}>Item {n}!</div>);
});

describe('ScrollArea', () => {
  it('should render component with both scrolls', () => {
    render(<ScrollArea>{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');

    expect(scrollArea).toHaveStyle('overflow-y:auto; overflow-x:auto');
  });

  it('should render component with vertical scroll', () => {
    render(<ScrollArea direction="vertical">{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');

    expect(scrollArea).toHaveStyle('overflow-y:auto; overflow-x:hidden');
  });

  it('should render component with horizontal scroll', () => {
    render(<ScrollArea direction="horizontal">{items}</ScrollArea>);

    const scrollArea = screen.getByRole('list');

    expect(scrollArea).toHaveStyle('overflow-y:hidden; overflow-x:auto');
  });
});
