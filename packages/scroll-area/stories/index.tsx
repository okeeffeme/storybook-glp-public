import React, {useRef, FC} from 'react';
import {storiesOf} from '@storybook/react';
import ScrollArea from '../src';
import ScrollAreaController from '../src/scroll-area-controller';

import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Button from '../../button/src';

import './demo.scss';

const Container: FC = ({children}) => (
  <div style={{padding: '10px', marginBottom: '30px'}}>{children}</div>
);

const range = (length: number) => Array.from({length}, (value, index) => index);

const fewRows = range(50).map((x) => (
  <div role="listitem" key={x} className="demo-scroll-area__item">
    Demo item - {x + 1}
  </div>
));

const fewRowsForBoth = (
  <div style={{width: '600px'}}>
    {range(50).map((x) => (
      <div role="listitem" key={x} className="demo-scroll-area__item">
        Demo item - {x + 1}
      </div>
    ))}
  </div>
);

const largeText = (
  <div role="listitem" className="demo-scroll-area__large-text">
    {range(100).map((x) => `Demo text - ${x + 1} `)}
  </div>
);

interface ScrollAreaProps {
  title?: string;
}

const VerticalScrollArea: FC<ScrollAreaProps> = ({
  children,
  title = 'Vertical scroll',
  ...props
}) => {
  return (
    <Container>
      <h3>{title}</h3>
      <ScrollArea
        direction="vertical"
        className="demo-scroll-area__scrollable-list"
        {...props}>
        {children}
      </ScrollArea>
    </Container>
  );
};

const HorizontalScrollArea: FC<ScrollAreaProps> = ({
  children,
  title = 'Horizontal scroll',
  ...props
}) => {
  return (
    <Container>
      <h3>{title}</h3>
      <ScrollArea
        direction="horizontal"
        className="demo-scroll-area__scrollable-list demo-scroll-area__scrollable-list--horizontal"
        {...props}>
        {children}
      </ScrollArea>
    </Container>
  );
};

const BothScrollArea: FC<ScrollAreaProps> = ({
  children,
  title = 'Both scroll',
  ...props
}) => {
  return (
    <Container>
      <h3>{title}</h3>
      <ScrollArea className="demo-scroll-area__scrollable-list" {...props}>
        {children}
      </ScrollArea>
    </Container>
  );
};

const InteractionWithScrollArea: FC = ({children, ...props}) => {
  const scrollAreaRef = useRef<HTMLDivElement | null>(null);

  let controller;

  if (scrollAreaRef.current) {
    controller = new ScrollAreaController(scrollAreaRef.current);
  }

  return (
    <Container>
      <div style={{display: 'flex', flexWrap: 'wrap'}}>
        <Button onClick={() => controller.scrollToTop()}>scrollTop</Button>
        <Button onClick={() => controller.scrollToBottom()}>
          scrollBottom
        </Button>
        <Button onClick={() => controller.scrollToLeft()}>scrollLeft</Button>
        <Button onClick={() => controller.scrollToRight()}>scrollRight</Button>
        <Button onClick={() => controller.scrollX(30)}>
          scrollHorizontally(30)
        </Button>
        <Button onClick={() => controller.scrollX(-30)}>
          scrollHorizontally(-30)
        </Button>
        <Button onClick={() => controller.scrollY(30)}>
          scrollVertically(30)
        </Button>
        <Button onClick={() => controller.scrollY(-30)}>
          scrollVertically(-30)
        </Button>
        <Button onClick={() => controller.scrollXTo(50)}>scrollXTo(50)</Button>
        <Button onClick={() => controller.scrollYTo(50)}>scrollYTo(50)</Button>
        <Button onClick={() => alert(controller.canScrollX())}>
          canScrollX()
        </Button>
        <Button onClick={() => alert(controller.canScrollY())}>
          canScrollY()
        </Button>
      </div>
      <br />
      <ScrollArea
        className="demo-scroll-area__scrollable-list"
        ref={scrollAreaRef}
        {...props}>
        {children}
      </ScrollArea>
    </Container>
  );
};

storiesOf('Components/scroll-area', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('basic', () => (
    <div>
      <VerticalScrollArea>{fewRows}</VerticalScrollArea>
      <HorizontalScrollArea>{largeText}</HorizontalScrollArea>
      <BothScrollArea>{fewRowsForBoth}</BothScrollArea>
    </div>
  ))
  .add('interaction with scroll area', () => (
    <div>
      <InteractionWithScrollArea>{fewRowsForBoth}</InteractionWithScrollArea>
    </div>
  ));
