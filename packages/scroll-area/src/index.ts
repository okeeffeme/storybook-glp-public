import ScrollArea from './scroll-area';
import ScrollAreaController from './scroll-area-controller';

export default ScrollArea;

export {ScrollArea, ScrollAreaController};
