import React, {
  forwardRef,
  FC,
  ForwardedRef,
  UIEventHandler,
  PropsWithChildren,
} from 'react';

interface ScrollAreaProps {
  className?: string;
  onScroll?: UIEventHandler<HTMLDivElement> | undefined;
  direction?: 'vertical' | 'horizontal' | 'both';
  forwardedRef?: ForwardedRef<HTMLDivElement>;
}

const ScrollArea: FC<PropsWithChildren<ScrollAreaProps>> = ({
  className,
  children,
  onScroll,
  direction = 'both',
  forwardedRef,
}) => (
  <div
    role="list"
    ref={forwardedRef}
    style={{
      overflowX: direction === 'vertical' ? 'hidden' : 'auto',
      overflowY: direction === 'horizontal' ? 'hidden' : 'auto',
    }}
    className={className}
    onScroll={onScroll}>
    {children}
  </div>
);

export default forwardRef<HTMLDivElement, PropsWithChildren<ScrollAreaProps>>(
  function scrollAreaWithRef(props, ref) {
    return <ScrollArea {...props} forwardedRef={ref} />;
  }
);
