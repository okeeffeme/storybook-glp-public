export default class ScrollAreaController {
  private element: HTMLElement;
  constructor(element: HTMLElement) {
    this.element = element;
  }

  scrollToTop() {
    this.element.scrollTop = 0;
  }

  scrollToBottom() {
    this.element.scrollTop = this.element.scrollHeight;
  }

  scrollToLeft() {
    this.element.scrollLeft = 0;
  }

  scrollToRight() {
    this.element.scrollLeft = this.element.scrollWidth;
  }

  scrollX(scrollAmount: number) {
    this.element.scrollLeft = this.element.scrollLeft + scrollAmount;
  }

  scrollY(scrollAmount: number) {
    this.element.scrollTop = this.element.scrollTop + scrollAmount;
  }

  scrollXTo(scrollValue: number) {
    this.element.scrollLeft = scrollValue;
  }

  scrollYTo(scrollValue: number) {
    this.element.scrollTop = scrollValue;
  }

  canScrollX() {
    return this.element.scrollHeight !== this.element.clientHeight;
  }

  canScrollY() {
    return this.element.scrollWidth !== this.element.clientWidth;
  }
}
