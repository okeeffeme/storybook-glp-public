# Jotunheim React Scroll Area

CSS-based ScrollArea. Provides a simple wrapper with overflow auto and some helpers
to get payload and interact with ScrollArea

## Basic usage

```jsx
import ScrollArea from '@jotunheim/react-scroll-area';

...

<ScrollArea>
  <SomeInnerComponent />
</ScrollArea>
```

## Props

- className:String
- children:Node
- onScroll:Func - callback that fires on scroll. Provides scroll event object
- direction:String - scroll axis. Values: 'vertical', 'horizontal', 'both' (default)

## Interaction with ScrollArea

- ScrollAreaController class provides the following methods:
  - scrollToTop() - maximum scroll to top
  - scrollToBottom() - maximum scroll to bottom
  - scrollToLeft() - maximum scroll to left
  - scrollToRight() - maximum scroll to right
  - scrollX(scrollAmount: number) - horizontal scroll on 'scrollAmount'
  - scrollY(scrollAmount: number) - vertical scroll on 'scrollAmount'
  - scrollXTo(scrollValue: number) - horizontal scroll to 'scrollValue'
  - scrollYTo(scrollValue: number) - vertical scroll to 'scrollValue'
  - canScrollX() - whether or not we could scroll horizontally
  - canScrollY() - whether or not we could scroll vertically

```jsx
import ScrollArea, {ScrollAreaController} from '@jotunheim/react-scroll-area';

...

componentDidMount() {
  this.scrollAreaController = new ScrollAreaController(this.scrollAreaRef.current);

  this.scrollAreaController.scrollToBottom();
}

<ScrollArea ref={this.scrollAreaRef}>
  <SomeInnerComponent />
</ScrollArea>
```
