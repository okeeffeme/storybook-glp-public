# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-area@6.2.0&sourceBranch=refs/tags/@jotunheim/react-scroll-area@6.2.1&targetRepoId=1246) (2023-01-27)

### Bug Fixes

- add private variable for scroll-area-controller ([NPM-1239](https://jiraosl.firmglobal.com/browse/NPM-1239)) ([2b09f70](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2b09f70989de6d9243c07f09296e9b0fc64fdb5b))

# [6.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-area@6.1.0&sourceBranch=refs/tags/@jotunheim/react-scroll-area@6.2.0&targetRepoId=1246) (2023-01-25)

### Features

- Convert scroll-area package to TypeScript ([NPM-1236](https://jiraosl.firmglobal.com/browse/NPM-1236)) ([485f6ee](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/485f6ee5e5167a18f6b306d354c3b30d9f79b832))

# [6.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-scroll-area@6.0.0&sourceBranch=refs/tags/@jotunheim/react-scroll-area@6.1.0&targetRepoId=1246) (2022-12-15)

### Features

- add role 'list' for ScrollArea component ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([91109e0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/91109e08e1fbf8928cbb34894d224c4b90787880))
- add role 'listitem' for ScrollArea's children components ([NPM-1133](https://jiraosl.firmglobal.com/browse/NPM-1133)) ([ec6a6d4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec6a6d4b16b4aea8e42efec7e218613ea805633d))

## 6.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [5.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-area@5.0.1&sourceBranch=refs/tags/@confirmit/react-scroll-area@5.0.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [5.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-scroll-area@5.0.0&sourceBranch=refs/tags/@confirmit/react-scroll-area@5.0.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## CHANGELOG

## v5.0.0

- Refactor: New package name: `@confirmit/react-scroll-area`

### v4.1.0

- ScrollAreaController now accepts element instead of ref

### v4.0.1

- Bugfix: Fixed the forwardRef implementation
- Chore: Fixed a story that was using things that do not exist

### v4.0.0

- **BREAKING**

  - Removed components:
    - VerticalScrollArea (now supported via 'direction' prop)
    - HorizontalScrollArea (now supported via 'direction' prop)
  - Removed properties:

    - indentFromScrollbar
    - smoothScrolling
    - shadows
    - verticalScrollTrackClassName
    - verticalScrollBarClassName
    - horizontalScrollTrackClassName
    - horizontalScrollBarClassName
    - hideScrollBars
    - disableScroll
    - stopScrollPropagation
    - reachBoundaries

  - onScroll callback now provides no payload but you could get one by calling
    'getScrollAreaPayload' helper and passing ScrollArea ref here

  - API
    - scrollTop (now supported via 'API' class)
    - scrollBottom (now supported via 'API' class)
    - scrollLeft (now supported via 'API' class)
    - scrollRight (now supported via 'API' class)
    - scrollHorizontally (now supported via 'API' class)
    - scrollVertically (now supported via 'API' class)
    - scrollXTo (now supported via 'API' class)
    - scrollYTo (now supported via 'API' class)
    - canScrollX (now supported via 'API' class)
    - canScrollY (now supported via 'API' class)
    - getElementVisibility (now supported via 'API' class)
    - scrollElementIntoViewX (removed, but 'API' class has 'scrollElementIntoView' method)
    - scrollElementIntoViewY (removed, but 'API' class has 'scrollElementIntoView' method)

### v3.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v2.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v1.5.0

- Feat: hideScrollbars={{horizontal: true}} set 100% width to content of ScrollArea

### v1.4.0

- Fix: Making the refreshing more stable.
- Feat: Adding the state objects `horizontalScrollbar` and `verticalScrollbar` to the payload of events. It can be useful to determine if scrollbars become visible or invisible.

### v1.3.0

- Removed the scroll shadow effect in print

### v1.2.0

- Feat: Package is built using both cjs and esm. No extra transformation in jest configs of applications are needed

### v1.0.0

- initial version
