import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import Button from '../../button/src';
import Snackbar, {store} from '../src';
import {SNACKBAR_CLOSE_TIMEOUT} from '../src/components/Snackbar';
const LONGER_MESSAGE =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

const SnackbarView = ({
  message = 'Message',
  action,
  autoDismissMessages = true,
}: {
  message?: string;
  action?: string;
  autoDismissMessages?: boolean;
}) => {
  const ref = React.createRef<HTMLDivElement>();

  const [index, setIndex] = React.useState(1);

  return (
    <div style={{padding: 20}}>
      <div style={{paddingBottom: 20}}>
        {autoDismissMessages
          ? `Snackbar automatically disappears in ${SNACKBAR_CLOSE_TIMEOUT}ms.`
          : `Snackbar will disappear in ${SNACKBAR_CLOSE_TIMEOUT}ms only when you click "Hide" button`}
        <br />
        If you hover over the snackbar, it will remain visible until you move
        the mouse out.
      </div>
      <Button
        appearance={Button.appearances.primarySuccess}
        onClick={() => {
          store.push(
            {
              message: message + ' - ' + index,
              action: action,
              handleAction: () => {
                if (ref.current) {
                  ref.current.innerText = `Action ${index} applied!`;
                }
              },
            },
            autoDismissMessages
          );
          setIndex(index + 1);
        }}>
        Add one more message
      </Button>
      {!autoDismissMessages && (
        <Button
          appearance={Button.appearances.primaryNeutral}
          onClick={() => store.hide()}>
          Hide message
        </Button>
      )}
      <div style={{padding: '20px 0'}} ref={ref} />
      <Snackbar />
    </div>
  );
};

storiesOf('Components/snackbar', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Default', () => <SnackbarView />)
  .add('With action', () => <SnackbarView action="Action" />)
  .add('With longer message', () => (
    <SnackbarView message={LONGER_MESSAGE} action="Action" />
  ))
  .add('With longer action', () => (
    <SnackbarView message={LONGER_MESSAGE} action="Long Action Text" />
  ))
  .add('With persistent message', () => (
    <SnackbarView autoDismissMessages={false} />
  ));
