import {MouseEventHandler} from 'react';

import {ActionTypes} from './ActionTypes';

export type StoreItem = {
  message: string;
  action?: string;
  handleAction?: MouseEventHandler;
};

export type Listener = () => void;

export type SnackbarState = {
  item: StoreItem;
  isOpen: boolean;
};

export type ShowSnackbar = {
  type: ActionTypes.show;
  item: StoreItem;
};

export type HideSnackbar = {
  type: ActionTypes.hide;
};

export type SnackbarActions = ShowSnackbar | HideSnackbar;
