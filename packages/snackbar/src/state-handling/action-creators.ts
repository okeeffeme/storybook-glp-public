import {ActionTypes} from './ActionTypes';
import {HideSnackbar, ShowSnackbar, StoreItem} from './types';

export const showSnackbar = (item: StoreItem): ShowSnackbar => ({
  type: ActionTypes.show,
  item,
});

export const hideSnackbar = (): HideSnackbar => ({type: ActionTypes.hide});
