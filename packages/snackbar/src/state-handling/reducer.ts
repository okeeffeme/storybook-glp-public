import {ActionTypes} from './ActionTypes';
import {SnackbarActions, SnackbarState} from './types';

const reducer = (state: SnackbarState, action: SnackbarActions) => {
  switch (action.type) {
    case ActionTypes.show:
      if (!state.isOpen || action.item !== state.item) {
        return {
          ...state,
          isOpen: true,
          item: action.item,
        };
      }
      break;

    case ActionTypes.hide:
      if (state.isOpen) {
        return {
          ...state,
          isOpen: false,
        };
      }
      break;

    default:
      throw new Error();
  }

  return state;
};

export default reducer;
