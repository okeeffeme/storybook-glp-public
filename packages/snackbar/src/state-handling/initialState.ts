import {SnackbarState} from './types';

const initialState: SnackbarState = {
  item: {
    message: '',
  },
  isOpen: false,
};

export default initialState;
