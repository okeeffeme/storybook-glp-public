import {StoreItem, Listener} from './types';

const storeCreator = () => {
  const listeners: Listener[] = [];
  let message: StoreItem | undefined;
  let hide = true;
  let hideImmediate = false;

  const notify = () => {
    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i];
      listener();
    }
  };

  return {
    subscribe(listener: Listener) {
      listeners.push(listener);

      const subscribed = true;

      return function unsubscribe() {
        if (!subscribed) return;

        const index = listeners.indexOf(listener);
        listeners.splice(index, 1);
      };
    },

    getMessage() {
      const messageToReturn = message;
      message = undefined;

      return messageToReturn;
    },

    getShouldHide() {
      return hide;
    },

    getShouldHideImmediate() {
      return hideImmediate;
    },

    push(item: StoreItem, shouldHide = true) {
      message = item;
      hide = shouldHide;
      hideImmediate = false;

      notify();
    },

    hide(shouldHideImmediate = false) {
      hide = true;
      hideImmediate = shouldHideImmediate;
      notify();
    },
  };
};

export default storeCreator();
