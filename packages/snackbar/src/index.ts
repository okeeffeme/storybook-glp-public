import Snackbar from './components/Snackbar';
import store from './state-handling/store';

export {Snackbar, store};

export default Snackbar;
