import React, {useEffect, useReducer, AriaAttributes, useRef} from 'react';

import {TransitionPortal} from '@jotunheim/react-transition-portal';
import {extractDataAriaIdProps, useForceUpdate} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import Button from '@jotunheim/react-button';

import store from '../state-handling/store';
import {hideSnackbar, showSnackbar} from '../state-handling/action-creators';
import reducer from '../state-handling/reducer';
import initialState from '../state-handling/initialState';
import classNames from './snackbar.module.css';

export const SNACKBAR_CLOSE_TIMEOUT = 4000;
export const SNACKBAR_ANIMATION_TIMEOUT = 150;

export type SnackbarProps = AriaAttributes;

export const Snackbar = ({...rest}: SnackbarProps) => {
  const {block, element, modifier} = bemFactory({
    baseClassName: 'comd-snackbar',
    classNames,
  });
  const timer = useRef<ReturnType<typeof setTimeout>>();
  const forceUpdate = useForceUpdate();

  const [
    {
      isOpen,
      item: {message, action, handleAction = () => {}},
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const clearTimer = () => {
    timer.current && clearTimeout(timer.current);
  };

  const handleMouseEnter = () => {
    clearTimer();
  };

  const handleMouseLeave = () => {
    forceUpdate();
  };

  useEffect(() => {
    // force update whenever the store notifies,
    // and return the unsubscribe listener.
    return store.subscribe(forceUpdate);
  }, [forceUpdate]);

  useEffect(() => {
    const item = store.getMessage();
    const shouldHide = store.getShouldHide();
    const shouldHideImmediate = store.getShouldHideImmediate();

    if (item) {
      dispatch(showSnackbar(item));
    }

    if (shouldHide) {
      if (shouldHideImmediate) {
        dispatch(hideSnackbar());
      } else {
        timer.current = setTimeout(() => {
          dispatch(hideSnackbar());
        }, SNACKBAR_CLOSE_TIMEOUT);
      }
    }

    return () => {
      clearTimer();
    };
  });

  const transitionClassNames = {
    appear: modifier('appear'),
    appearActive: modifier('appear-active'),
    appearDone: modifier('appear-done'),
    enter: modifier('enter'),
    enterActive: modifier('enter-active'),
    enterDone: modifier('enter-done'),
    exit: modifier('exit'),
    exitActive: modifier('exit-active'),
    exitDone: modifier('exit-done'),
  };

  return (
    <TransitionPortal
      open={isOpen}
      transitionEnterTimeout={SNACKBAR_ANIMATION_TIMEOUT}
      transitionLeaveTimeout={SNACKBAR_ANIMATION_TIMEOUT}
      transitionClassNames={transitionClassNames}>
      <div className={block()}>
        <div
          data-testid="snackbar"
          className={element('content')}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          {...extractDataAriaIdProps(rest)}
          data-snackbar={true}>
          <div className={element('message')}>{message}</div>
          {action && (
            <div className={element('action')}>
              <Button
                data-testid="snackbar-action-button"
                onClick={handleAction}
                appearance={Button.appearances.secondaryBanner}
                data-snackbar-action-button={action}>
                {action}
              </Button>
            </div>
          )}
        </div>
      </div>
    </TransitionPortal>
  );
};

export default Snackbar;
