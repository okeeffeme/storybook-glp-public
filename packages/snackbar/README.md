# Jotunheim React Snackbar

[Changelog](./CHANGELOG.md)

Snackbars provide brief messages about app processes at the bottom of the screen.
