import React from 'react';
import {render, screen, act} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import store from '../../src/state-handling/store';
import {StoreItem} from '../../src/state-handling/types';
import {
  Snackbar,
  SNACKBAR_ANIMATION_TIMEOUT,
  SNACKBAR_CLOSE_TIMEOUT,
} from '../../src/components/Snackbar';

jest.useFakeTimers();

describe('Jotunheim React Snackbar :: ', () => {
  const addTime = (...timeouts: number[]) => {
    for (const timeout of timeouts) {
      act(() => {
        jest.advanceTimersByTime(timeout);
      });
    }
  };

  const addMessage = (item: StoreItem, shouldHide?: boolean) => {
    act(() => {
      store.push(item, shouldHide);
    });
  };

  it('renders empty snackbar', () => {
    render(<Snackbar />);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('renders snackbar with message only', () => {
    render(<Snackbar />);

    const message = 'Message';

    addMessage({message});

    expect(screen.getByText(message)).toBeInTheDocument();
    expect(
      screen.queryByTestId('snackbar-action-button')
    ).not.toBeInTheDocument();
  });

  it('renders snackbar with message and action', () => {
    render(<Snackbar />);

    const message = 'Message';
    const action = 'Action';

    addMessage({message, action});

    expect(screen.getByText(message)).toBeInTheDocument();
    expect(screen.getByTestId('snackbar-action-button')).toHaveAttribute(
      'data-snackbar-action-button',
      action
    );
  });

  it('snackbar opens and closes automatically', () => {
    render(<Snackbar />);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();

    addMessage({message: 'Message'});

    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();

    addTime(SNACKBAR_CLOSE_TIMEOUT, SNACKBAR_ANIMATION_TIMEOUT);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('snackbar triggers action and does not close on action click', () => {
    render(<Snackbar />);

    const handleAction = jest.fn();

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();

    addMessage({message: 'Message', action: 'Action', handleAction});

    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();

    userEvent.click(screen.getByTestId('snackbar-action-button'));

    addTime(SNACKBAR_ANIMATION_TIMEOUT);

    expect(handleAction).toBeCalled();
    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();
  });

  it('snackbar messages change one another when new one comes', () => {
    render(<Snackbar />);

    const message1 = 'Message 1';
    const message2 = 'Message 2';

    addMessage({message: message1});

    expect(screen.getByText(message1)).toBeInTheDocument();

    addMessage({message: message2});

    expect(screen.getByText(message2)).toBeInTheDocument();
  });

  it('pause snackbar hiding timer on mouseOver and resume on mouseLeave', () => {
    render(<Snackbar />);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();

    addMessage({message: 'Message'});

    userEvent.hover(screen.getByText('Message'));

    addTime(100 * SNACKBAR_CLOSE_TIMEOUT);

    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();

    userEvent.unhover(screen.getByText('Message'));

    addTime(SNACKBAR_CLOSE_TIMEOUT, SNACKBAR_ANIMATION_TIMEOUT);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('snackbar message with shouldHide:false persists until hidden', () => {
    render(<Snackbar />);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();

    addMessage({message: 'Message'}, false);
    addTime(100 * SNACKBAR_CLOSE_TIMEOUT);

    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();

    act(() => {
      store.hide();
    });

    addTime(SNACKBAR_CLOSE_TIMEOUT, SNACKBAR_ANIMATION_TIMEOUT);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });

  it('persistent snackbar messages closes after the next temporary message comes', () => {
    render(<Snackbar />);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();

    addMessage({message: 'Message 1'}, false);
    addTime(10 * SNACKBAR_CLOSE_TIMEOUT);

    expect(screen.getByTestId('transition-portal')).toBeInTheDocument();

    addMessage({message: 'Message 2'});
    addTime(SNACKBAR_CLOSE_TIMEOUT, SNACKBAR_ANIMATION_TIMEOUT);

    expect(screen.queryByTestId('transition-portal')).not.toBeInTheDocument();
  });
});
