# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.42](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.42&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.42&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.41](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.40&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.41&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.40](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.39&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.40&targetRepoId=1246) (2023-03-28)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.38&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.39&targetRepoId=1246) (2023-03-17)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.37&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.38&targetRepoId=1246) (2023-03-16)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.36&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.37&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.35&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.36&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- ensure consumer data-testid will overwrite internal ([NPM-1265](https://jiraosl.firmglobal.com/browse/NPM-1265)) ([bb641ff](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bb641ff417a10142c89c3c2c533a3c2293b29b7d))

## [2.0.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.34&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.35&targetRepoId=1246) (2023-02-28)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.33&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.34&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.33](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.32&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.33&targetRepoId=1246) (2023-02-09)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.32](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.31&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.32&targetRepoId=1246) (2023-02-06)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.30&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.31&targetRepoId=1246) (2023-02-02)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.29&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.30&targetRepoId=1246) (2023-01-30)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.28&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.29&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.27&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.28&targetRepoId=1246) (2023-01-20)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.25&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.27&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.25&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.26&targetRepoId=1246) (2023-01-19)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.24&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.25&targetRepoId=1246) (2023-01-12)

### Bug Fixes

- add test id for Snackbar component within snackbar package ([NPM-1214](https://jiraosl.firmglobal.com/browse/NPM-1214)) ([624158b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/624158b796ef095f8991f873e2aa9cf39b524626))

## [2.0.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.23&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.24&targetRepoId=1246) (2023-01-06)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.23](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.22&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.23&targetRepoId=1246) (2023-01-05)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.21&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.22&targetRepoId=1246) (2022-12-27)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.19&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.21&targetRepoId=1246) (2022-12-22)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.19&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.20&targetRepoId=1246) (2022-12-16)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.16&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.19&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.16&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.18&targetRepoId=1246) (2022-12-06)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.16&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.17&targetRepoId=1246) (2022-12-02)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.15&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.16&targetRepoId=1246) (2022-11-16)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.14&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.15&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.12&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.13&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.11&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.12&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.10&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.11&targetRepoId=1246) (2022-10-03)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.9&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.10&targetRepoId=1246) (2022-09-28)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.8&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.9&targetRepoId=1246) (2022-09-08)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.6&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.7&targetRepoId=1246) (2022-08-25)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.3&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.4&targetRepoId=1246) (2022-08-12)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.2&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.3&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.1&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.2&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package @jotunheim/react-snackbar

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-snackbar@2.0.0&sourceBranch=refs/tags/@jotunheim/react-snackbar@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-snackbar

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@1.0.5&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.6&targetRepoId=1246) (2022-06-29)

**Note:** Version bump only for package @confirmit/react-snackbar

## [1.0.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@1.0.4&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.5&targetRepoId=1246) (2022-06-27)

**Note:** Version bump only for package @confirmit/react-snackbar

## [1.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@1.0.3&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.4&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [1.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@1.0.2&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.3&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-snackbar

## [1.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@1.0.1&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.2&targetRepoId=1246) (2022-04-21)

**Note:** Version bump only for package @confirmit/react-snackbar

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.39&sourceBranch=refs/tags/@confirmit/react-snackbar@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Remove support for default theme ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.4.39](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.38&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.39&targetRepoId=1246) (2022-04-18)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.38](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.37&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.38&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.37](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.36&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.37&targetRepoId=1246) (2022-03-29)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.36](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.35&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.36&targetRepoId=1246) (2022-03-14)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.35](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.34&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.35&targetRepoId=1246) (2022-03-02)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.34](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.33&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.34&targetRepoId=1246) (2022-02-25)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.31](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.30&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.31&targetRepoId=1246) (2022-01-19)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.30](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.29&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.30&targetRepoId=1246) (2022-01-14)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.28&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.29&targetRepoId=1246) (2021-12-30)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.28](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.27&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.28&targetRepoId=1246) (2021-12-23)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.27](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.26&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.27&targetRepoId=1246) (2021-11-29)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.25&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.26&targetRepoId=1246) (2021-11-23)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.25](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.24&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.25&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.4.24](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.23&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.24&targetRepoId=1246) (2021-10-20)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.22](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.21&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.22&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.21](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.20&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.21&targetRepoId=1246) (2021-09-14)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.19&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.20&targetRepoId=1246) (2021-09-13)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.19](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.18&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.19&targetRepoId=1246) (2021-08-20)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.18](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.17&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.18&targetRepoId=1246) (2021-08-05)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.17](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.16&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.17&targetRepoId=1246) (2021-07-28)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.16](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.15&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.16&targetRepoId=1246) (2021-07-15)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.15](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.14&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.15&targetRepoId=1246) (2021-07-12)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.14](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.13&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.14&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.13](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.12&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.13&targetRepoId=1246) (2021-07-01)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.12](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.11&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.12&targetRepoId=1246) (2021-06-24)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.11](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.10&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.11&targetRepoId=1246) (2021-06-15)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.9&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.10&targetRepoId=1246) (2021-06-14)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.8&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.9&targetRepoId=1246) (2021-05-25)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.7&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.8&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.4.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.6&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.7&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.5&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.6&targetRepoId=1246) (2021-02-15)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.3&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.4&targetRepoId=1246) (2020-12-14)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.2&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.3&targetRepoId=1246) (2020-12-11)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.4.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.4.1&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.2&targetRepoId=1246) (2020-12-09)

**Note:** Version bump only for package @confirmit/react-snackbar

# [0.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.3.5&sourceBranch=refs/tags/@confirmit/react-snackbar@0.4.0&targetRepoId=1246) (2020-11-11)

### Features

- @confirmit/react-transition-portal is regular dependency instead of peer. ([NPM-601](https://jiraosl.firmglobal.com/browse/NPM-601)) ([910cd79](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/910cd7953357771bdedbe3ba2d37e786c08c3651))
- add @confirmit/react-contexts as explicit peer dependency ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([b584fac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b584fac3db8c0a8b5aee2ae8b1c692ec19a26f22))

### BREAKING CHANGES

- add @confirmit/react-contexts as explicit peer dependency (NPM-599)
- @confirmit/react-transition-portal requires @confirmit/react-contexts to be installed as peer.

## [0.3.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.3.4&sourceBranch=refs/tags/@confirmit/react-snackbar@0.3.5&targetRepoId=1246) (2020-11-06)

### Bug Fixes

- update action button appearance in snackbar according to DS spec ([NPM-593](https://jiraosl.firmglobal.com/browse/NPM-593)) ([e232c29](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/e232c299529300abe1936cd0693ca628757fc12a))

## [0.3.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.3.3&sourceBranch=refs/tags/@confirmit/react-snackbar@0.3.4&targetRepoId=1246) (2020-10-21)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.3.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-snackbar@0.3.2&sourceBranch=refs/tags/@confirmit/react-snackbar@0.3.3&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-snackbar

## [0.3.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-snackbar@0.3.1...@confirmit/react-snackbar@0.3.2) (2020-08-28)

### Bug Fixes

- change the snackbar message and snackbar itself width (make it more responsive) ([NPM-263](https://jiraosl.firmglobal.com/browse/NPM-263)) ([04017a8](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/04017a88e8bff7ce4dbaae01a2e767bd0e1b7674))
- make the snackbar message width stretch with the content([NPM-263](https://jiraosl.firmglobal.com/browse/NPM-263)) ([153dfc4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/153dfc4c5bbf5a64423c3d02bd3b0f236665cb49))

# [0.3.1]

### Features

- Refactor to make immediate hiding truly immediate (no setTimeout)
- Refactor reducer to only update when needed

# [0.3.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-snackbar@0.1.0...@confirmit/react-snackbar@0.2.0) (2020-08-13)

### Features

- Support hiding messages immediately

### BREAKING CHANGES

- do not hide when clicking an action, needs to be handled from the action itself

# [0.2.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-snackbar@0.1.0...@confirmit/react-snackbar@0.2.0) (2020-08-13)

### Features

- extend data attribute for action button ([NPM-263](https://jiraosl.firmglobal.com/browse/NPM-263)) ([b93a7d1](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/b93a7d1d12820c19131b368f2b52f4df3ff932b2))

# [0.1.0](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-snackbar@0.0.1...@confirmit/react-snackbar@0.1.0) (2020-08-12)

### Features

- update peer dependency ([NPM-477](https://jiraosl.firmglobal.com/browse/NPM-477)) ([64fb85a](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/64fb85a8b7d955aa63b8ffbb3c49d2e04b3d0c5c))

### BREAKING CHANGES

- peerDependency `@confirmit/react-transition-portal": "^1.3.0"` is required

## CHANGELOG

### v0.0.1

- Initial version
