# Confirmit Icons

**DEPRECATED**
This package has been deprecated in favor of `@confirmit/react-icons` package. Please see [readme](../react-icons/readme.md) in that project for reasons, and follow the [migration guide](../react-icons/migration-guide.md).
