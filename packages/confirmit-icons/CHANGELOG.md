# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [14.3.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.3.0&sourceBranch=refs/tags/confirmit-icons@14.3.1&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package confirmit-icons

# [14.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.10&sourceBranch=refs/tags/confirmit-icons@14.3.0&targetRepoId=1246) (2022-11-04)

### Features

- bump svgo with babel-plugin ([NPM-1109](https://jiraosl.firmglobal.com/browse/NPM-1109)) ([4bda421](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4bda421aa5fb12693ed5a3d0a5eeaf9e9defe20c))

## [14.2.10](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.9&sourceBranch=refs/tags/confirmit-icons@14.2.10&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package confirmit-icons

## [14.2.9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.8&sourceBranch=refs/tags/confirmit-icons@14.2.9&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package confirmit-icons

## [14.2.8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.7&sourceBranch=refs/tags/confirmit-icons@14.2.8&targetRepoId=1246) (2022-07-08)

**Note:** Version bump only for package confirmit-icons

## [14.2.7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.6&sourceBranch=refs/tags/confirmit-icons@14.2.7&targetRepoId=1246) (2022-06-29)

### Bug Fixes

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [14.2.6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.5&sourceBranch=refs/tags/confirmit-icons@14.2.6&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [14.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.4&sourceBranch=refs/tags/confirmit-icons@14.2.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package confirmit-icons

## [14.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.3&sourceBranch=refs/tags/confirmit-icons@14.2.4&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [14.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/confirmit-icons@14.2.1&sourceBranch=refs/tags/confirmit-icons@14.2.2&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package confirmit-icons

## [14.2.1]

- Fix: App icons as previous version had conflicting styles that caused all colors to be the same

## [14.2.0]

- App Icons Updated

## [14.1.0]

- Custom Questions app icon

## [14.0.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/confirmit-icons@14.0.1...confirmit-icons@14.0.2) (2020-08-12)

**Note:** Version bump only for package confirmit-icons

## CHANGELOG

### v14.0.0

- **BREAKING**: peerDependency `@confirmit/react-themes@5.x` is required, instead of `confirmit-themes@3.x|4.x`.
- Remove deprecated icons from storybook examples.

### v13.1.0

- Removing package version in class names.

### v13.0.0

- **BREAKING** Package version as css class name is added to blocks and elements at compile time.

### v12.1.0

- Feat: new icon - Barcode/QR code node type.

### v12.0.0

- **BREAKING**
  - Change confirmit-themes to peerDependency, and update to version 3.x to support new React Context API.
  - Update peerDependencies `react` and `react-dom` to `^16.8.0`, to support hooks.
- Fix: Add missing @babel/runtime 7.x peerDependency.

### v11.1.0

- Feat: add 'Modern' type arrow icon.

### v11.0.0

- **BREAKING**
  - Update peerDependencies `react` and `react-dom` to `^16.3.0`.
  - Babel 7 used for transpilation

### v10.7.0

- Feat: add icon for new app, account overview

### v10.6.0

- Feat: new icon DateTime

### v10.5.0

- Feat: new icons - application icon for action management

### v10.4.0

- Feat: new icons - checked bubble and contact card

### v10.3.0

- Feat: new icon - undo arrow

### v10.2.2

- Fix: update Digital Feedback app icon to be more in line with the rest of the app icons.
- Chore: Update demo pages to automatically include any new app icons

### v10.2.1

- Digital Feedback app icon

### v10.1.3

- Fix: open text icon had an extra path element that should not have been present.

### v10.1.1

- Added CSS modules for scoping the CSS to the packages. Read more about the changes and how to adopt them in your project here: [How to modify the webpack config file](../../docs/CssModules.md)

### v10.1.0

- Feat: Add SingleWithLookup node type icon

### v10.0.0

- See [Migration guide to React 16](../../docs/MigrationGuideToReact16.md)
- **BREAKING** React 16.2 peer dependency
- **BREAKING** Package provides ES modules only

### v9.0.0

- **BREAKING:** `r2` appicon has been renamed to `studio` which is the correct name for this. You will only be affected by this breaking change if you are using this icon directly. App Header for example will not be affected by this, so you dont need to make any other changes to get the studio icon to appear there.

### v8.3.0

- Add `Home` app icon
- Add AppIcon to easily show app icons based on list returned from applications dictionary
  - This component used to be in confirmit-react-components package, but has been moved here.

### v8.2.0

- Add `material` type to ExternalIcon

### v8.1.0

- Add label icon

### v8.0.0

- **BREAKING:**
  - `ActionManagement` app icon renamed to `Actions`

### v7.2.0

- Add table icon

### v7.1.0

- Add spinner icon that can transition to success or error state

### v7.0.0

- **BREAKING:**
  - `Actions` app icon renamed to `ActionManagement`
  - `Connect` app icon renamed to `CrmConnectorForSalesforce`
- Add `generic` app icon

### v6.0.0

- precompiled css is used in components (might be a breaking change for webpack scss and css loaders)

### v5.0.0

- Only data- and aria- passed from IconWrapper component to inner div

### v4.2.0

- Add new arrow type, 'head'

### v4.1.0

- Add application icons
- Add browserslist config
- Update dependencies
- Set cacheDirectory to false in webpack because it did not pick up changes to the SVGs

### v4.0.0

- All props passed from IconWrapper component to inner div
- Trash icon: "style" prop changed to "type" prop, so style prop can be used to specify div style.

### v3.5.0

- Add calendar icon
- Add rounded-x icon

### v3.4.4

- Add missing peerdependency prop-types

### v3.4.3

- Fix testrunner and set fixed version number on dependencies

### v3.4.2

- Update dependencies

### v3.4.1

- Fix svgo rules when using inline svg

### v3.4.0

- Inline svg into components

### v3.2.0

- Add waffle icon as export
- Add cati application icon

### v3.1.0

- Add waffle icon

### v3.0.0

- add CHANGELOG.md
- split `path` into multiple paths to be able to style each path individually in these files:
  - src/node-types/svg/chart.svg
  - src/node-types/svg/condition.svg
- update svgo rules to not merge paths anymore, to be able to style each path individually
- update svgo rules to remove `style` attribute

### v2.0.0

- removed `fill` attributes on:
  - src/source-types/svg/am-program-source.svg
  - src/source-types/svg/combined-source.svg
  - src/source-types/svg/contact-database-source.svg

### v1.0.0

- initial public version
