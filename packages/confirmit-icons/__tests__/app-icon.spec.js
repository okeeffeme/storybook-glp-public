import React from 'react';
import {shallow} from 'enzyme';
import AppIcon from '../src/app-icon';

const surveyDesigner = {
  id: 'surveydesigner',
  name: 'Survey Designer',
  links: {
    self: '#http://localhost/surveydesigner/',
  },
};

describe('AppIcon', () => {
  it('should render with generic icon when passed unknown app', () => {
    const unknownApp = {
      id: 'unknownid',
      name: 'App Name',
      links: {
        self: '#http://localhost/',
      },
    };

    const wrapper = shallow(<AppIcon app={unknownApp} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with correct icon when passed known app', () => {
    const wrapper = shallow(<AppIcon app={surveyDesigner} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with custom css class', () => {
    const wrapper = shallow(
      <AppIcon app={surveyDesigner} className="custom-class" />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
