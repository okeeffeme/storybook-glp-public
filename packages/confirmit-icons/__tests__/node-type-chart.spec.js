import React from 'react';
import renderer from 'react-test-renderer';

import {Chart} from '../src/nodeTypes';

describe('Chart icon :: ', () => {
  it('renders correctly with 5 children', () => {
    const iconPaths = renderer.create(<Chart />).toJSON();
    expect(iconPaths.children[0].children.length).toBe(5);
  });
});
