import React from 'react';
import renderer from 'react-test-renderer';

import IconWrapper from '../src/icon-wrapper.jsx';

const SvgString = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" width="18" height="18">
  <circle cx="13.5" cy="13.5" r="2.5"/>
</svg>`;

const SvgComponent = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 18 18"
    width="18"
    height="18">
    <circle cx="13.5" cy="13.5" r="2.5" />
  </svg>
);

describe('Icon wrapper :: ', () => {
  it('renders correctly with svg string', () => {
    const logo = renderer
      .create(<IconWrapper>{SvgString}</IconWrapper>)
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders correctly with svg component', () => {
    const logo = renderer
      .create(
        <IconWrapper>
          <SvgComponent />
        </IconWrapper>
      )
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders correctly with svg element', () => {
    const logo = renderer
      .create(<IconWrapper>{SvgComponent()}</IconWrapper>)
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders aria attribute', () => {
    const logo = renderer
      .create(
        <IconWrapper aria-label="label">
          <SvgComponent />
        </IconWrapper>
      )
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders data attribute', () => {
    const logo = renderer
      .create(
        <IconWrapper data-label="data">
          <SvgComponent />
        </IconWrapper>
      )
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('doesnt render custom attribute', () => {
    const logo = renderer
      .create(
        <IconWrapper custom="attr">
          <SvgComponent />
        </IconWrapper>
      )
      .toJSON();
    expect(logo).toMatchSnapshot();
  });
});
