import React from 'react';
import renderer from 'react-test-renderer';

import {Spinner} from '../src/common';

describe('Spinner icon :: ', () => {
  it('renders correctly with no spin-state', () => {
    const logo = renderer.create(<Spinner />).toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders correctly with spin spin-state', () => {
    const logo = renderer
      .create(<Spinner spinnerState={Spinner.spinnerStates.spin} />)
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders correctly with error spin-state', () => {
    const logo = renderer
      .create(<Spinner spinnerState={Spinner.spinnerStates.error} />)
      .toJSON();
    expect(logo).toMatchSnapshot();
  });

  it('renders correctly with success spin-state', () => {
    const logo = renderer
      .create(<Spinner spinnerState={Spinner.spinnerStates.success} />)
      .toJSON();
    expect(logo).toMatchSnapshot();
  });
});
