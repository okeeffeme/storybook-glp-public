import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/combined-source.svg';

const CombinedSource = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default CombinedSource;
