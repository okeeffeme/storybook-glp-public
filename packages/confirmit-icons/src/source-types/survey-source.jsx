import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/survey-source.svg';

const SurveySource = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default SurveySource;
