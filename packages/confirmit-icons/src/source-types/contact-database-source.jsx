import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/contact-database-source.svg';

const ContactDatabaseSource = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ContactDatabaseSource;
