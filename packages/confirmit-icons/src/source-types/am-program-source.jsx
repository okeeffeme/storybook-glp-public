import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/am-program-source.svg';

const AMProgramSource = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default AMProgramSource;
