import React from 'react';

import IconWrapper from './icon-wrapper';
import Icon from './confirmit-logo.svg';

const ConfirmitLogo = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ConfirmitLogo;
