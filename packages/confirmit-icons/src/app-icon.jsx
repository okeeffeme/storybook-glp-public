import PropTypes from 'prop-types';
import React from 'react';

/* eslint import/namespace: ['error', { allowComputed: true }] */
import * as Apps from './applications';

const apps = Object.keys(Apps).reduce((previous, current) => {
  const Component = Apps[current];

  previous[current.toLowerCase()] = <Component />;

  return previous;
}, {});

const AppIcon = ({app, className}) => {
  return <div className={className}>{apps[app.id] || <Apps.Generic />}</div>;
};

AppIcon.propTypes = {
  app: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default AppIcon;
