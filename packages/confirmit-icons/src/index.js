import IconWrapper from './icon-wrapper';

import ConfirmitLogo from './confirmit-logo';
import AppIcon from './app-icon';
import * as Common from './common';
import * as NodeTypes from './nodeTypes';
import * as Forms from './forms';
import * as SourceTypes from './source-types';
import * as Applications from './applications';

export {
  ConfirmitLogo,
  AppIcon,
  Common,
  NodeTypes,
  Forms,
  SourceTypes,
  Applications,
};

export default IconWrapper;
