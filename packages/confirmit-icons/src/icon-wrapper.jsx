import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {extractDataAndAriaProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';
import classNames from './icons.module.css';

const IconWrapper = ({
  children,
  className,
  classNames,
  baseClassName,
  ...rest
}) => {
  const {block} = bemFactory({baseClassName, classNames});

  const classes = cn(className, block());

  const nextProps = {
    className: classes,
    ...extractDataAndAriaProps(rest),
  };

  if (typeof children === 'string')
    nextProps.dangerouslySetInnerHTML = {__html: children};
  else nextProps.children = children;

  return <div {...nextProps} />;
};

IconWrapper.displayName = 'IconWrapper';

IconWrapper.propTypes = {
  baseClassName: PropTypes.string,
  className: PropTypes.string,
  classNames: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

IconWrapper.defaultProps = {
  baseClassName: 'co-icon',
  classNames: classNames,
};

export default IconWrapper;
