import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/hamburger.svg';

const Hamburger = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Hamburger;
