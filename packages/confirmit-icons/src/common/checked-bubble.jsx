import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/checked-bubble.svg';

const CheckedBubble = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default CheckedBubble;
