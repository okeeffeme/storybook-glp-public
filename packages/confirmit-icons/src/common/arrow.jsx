import React from 'react';
import PropTypes from 'prop-types';

import IconWrapper from '../icon-wrapper';
import Standard from './svg/arrow.svg';
import Head from './svg/arrow--head.svg';
import Modern from './svg/arrow--modern.svg';

const Arrow = props => {
  const {type, ...rest} = props;
  const icon = React.createElement(styles[type]);

  return <IconWrapper {...rest}>{icon}</IconWrapper>;
};

const styles = (Arrow.styles = {
  standard: Standard,
  head: Head,
  modern: Modern,
});

Arrow.propTypes = {
  type: PropTypes.oneOf(Object.keys(styles)),
};

Arrow.defaultProps = {
  type: 'standard',
};

export default Arrow;
