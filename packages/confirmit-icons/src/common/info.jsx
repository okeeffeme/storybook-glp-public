import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/info.svg';

const Info = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Info;
