import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/pencil.svg';

const Pencil = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Pencil;
