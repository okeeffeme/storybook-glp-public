import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/waffle.svg';

const Waffle = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Waffle;
