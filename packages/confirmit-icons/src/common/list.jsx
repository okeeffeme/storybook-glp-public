import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/list.svg';

const List = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default List;
