import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/undo.svg';

const UndoArrow = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default UndoArrow;
