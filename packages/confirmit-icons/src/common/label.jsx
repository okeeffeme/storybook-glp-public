import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/label.svg';

const Label = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Label;
