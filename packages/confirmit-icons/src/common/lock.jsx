import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/lock.svg';

const Lock = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Lock;
