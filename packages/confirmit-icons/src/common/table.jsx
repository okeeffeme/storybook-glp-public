import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/table.svg';

const Table = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Table;
