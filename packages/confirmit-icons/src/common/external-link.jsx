import React from 'react';
import PropTypes from 'prop-types';

import IconWrapper from '../icon-wrapper';
import Standard from './svg/ext-link.svg';
import Material from './svg/ext-link--material.svg';

const ExternalLink = props => {
  const {type, ...rest} = props;
  const icon = React.createElement(styles[type]);
  return <IconWrapper {...rest}>{icon}</IconWrapper>;
};

const styles = (ExternalLink.styles = {
  standard: Standard,
  material: Material,
});

ExternalLink.propTypes = {
  type: PropTypes.oneOf(Object.keys(styles)),
};

ExternalLink.defaultProps = {
  type: 'standard',
};

export default ExternalLink;
