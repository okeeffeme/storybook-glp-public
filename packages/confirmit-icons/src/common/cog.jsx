import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/cog.svg';

const Cog = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Cog;
