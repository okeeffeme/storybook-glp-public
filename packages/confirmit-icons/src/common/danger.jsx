import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/danger.svg';

const Danger = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Danger;
