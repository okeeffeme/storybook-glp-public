import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/hierarchy.svg';

const Hierarchy = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Hierarchy;
