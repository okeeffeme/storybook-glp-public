import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/arrow-small.svg';

const Dropdown = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Dropdown;
