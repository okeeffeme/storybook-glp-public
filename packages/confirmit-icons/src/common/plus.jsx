import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/plus.svg';

const Plus = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Plus;
