import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/contact-card.svg';

const ContactCard = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ContactCard;
