import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/help.svg';

const Help = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Help;
