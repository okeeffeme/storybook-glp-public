import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/company.svg';

const Company = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Company;
