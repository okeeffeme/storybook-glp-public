import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/check.svg';

const Check = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Check;
