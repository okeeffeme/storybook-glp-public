import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/search.svg';

const Search = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Search;
