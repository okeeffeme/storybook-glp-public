import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/calendar.svg';

const Calendar = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Calendar;
