import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import IconWrapper from '../icon-wrapper';
import Success from './success';
import RoundedX from './rounded-x';

import Icon from './svg/spinner.svg';
import './css/spinner.css';

const Spinner = ({spinnerState, ...rest}) => {
  const c = cn('co-spinner', {
    [`co-spinner--${spinnerState}`]: spinnerState,
  });

  return (
    <div className={c}>
      <IconWrapper className="co-spinner__spinner" {...rest}>
        <Icon />
      </IconWrapper>
      <Success className="co-spinner__success" />
      <RoundedX className="co-spinner__error" />
    </div>
  );
};

const spinnerStates = {
  spin: 'spin',
  error: 'error',
  success: 'success',
};

Spinner.defaultProps = {
  spinnerState: 'spin',
};

Spinner.propTypes = {
  spinnerState: PropTypes.oneOf([
    spinnerStates.error,
    spinnerStates.success,
    spinnerStates.spin,
  ]),
};

Spinner.spinnerStates = spinnerStates;

export default Spinner;
