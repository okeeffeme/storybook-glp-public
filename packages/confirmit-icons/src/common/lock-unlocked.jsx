import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/lock-unlocked.svg';

const LockUnlocked = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default LockUnlocked;
