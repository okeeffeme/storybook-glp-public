import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/guide-arrow.svg';

const GuideArrow = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default GuideArrow;
