import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/success.svg';

const Success = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Success;
