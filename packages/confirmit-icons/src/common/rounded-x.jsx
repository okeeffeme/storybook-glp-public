import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/rounded-x.svg';

const RoundedX = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default RoundedX;
