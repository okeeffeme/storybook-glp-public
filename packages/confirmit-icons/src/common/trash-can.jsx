import React from 'react';
import PropTypes from 'prop-types';

import IconWrapper from '../icon-wrapper';
import Standard from './svg/trash-can.svg';
import Slim from './svg/trash-can--slim.svg';

const TrashCan = props => {
  const {type, ...rest} = props;
  const icon = React.createElement(styles[type]);

  return <IconWrapper {...rest}>{icon}</IconWrapper>;
};

const styles = (TrashCan.styles = {
  standard: Standard,
  slim: Slim,
});

TrashCan.propTypes = {
  type: PropTypes.oneOf(Object.keys(styles)),
};

TrashCan.defaultProps = {
  type: 'standard',
};

export default TrashCan;
