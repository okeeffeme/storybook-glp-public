import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/user.svg';

const User = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default User;
