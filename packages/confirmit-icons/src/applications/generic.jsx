import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/generic.svg';

const Generic = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Generic;
