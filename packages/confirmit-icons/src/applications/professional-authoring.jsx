import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/professional-authoring.svg';

const ProfessionalAuthoring = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ProfessionalAuthoring;
