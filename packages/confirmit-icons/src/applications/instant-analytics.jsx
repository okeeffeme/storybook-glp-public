import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/instant-analytics.svg';

const InstantAnalytics = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default InstantAnalytics;
