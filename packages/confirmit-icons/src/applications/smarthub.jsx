import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/smarthub.svg';

const Smarthub = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Smarthub;
