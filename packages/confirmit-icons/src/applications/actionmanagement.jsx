import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/actions.svg';

const ActionManagement = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ActionManagement;
