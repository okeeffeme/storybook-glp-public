import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/cati.svg';

const Cati = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Cati;
