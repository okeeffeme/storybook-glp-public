import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/home.svg';

const Home = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Home;
