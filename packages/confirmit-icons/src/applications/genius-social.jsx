import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/genius-social.svg';

const GeniusSocial = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default GeniusSocial;
