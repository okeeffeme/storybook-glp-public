import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/hierarchy-management.svg';

const HierarchyManagement = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default HierarchyManagement;
