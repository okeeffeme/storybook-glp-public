import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/reportal.svg';

const Reportal = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Reportal;
