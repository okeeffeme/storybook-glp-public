import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/end-user-management.svg';

const EndUserManagement = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default EndUserManagement;
