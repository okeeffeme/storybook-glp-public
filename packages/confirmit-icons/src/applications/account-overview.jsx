import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/account-overview.svg';

const AccountOverview = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default AccountOverview;
