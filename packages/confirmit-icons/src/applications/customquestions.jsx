import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/customquestions.svg';

const CustomQuestions = (props) => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default CustomQuestions;
