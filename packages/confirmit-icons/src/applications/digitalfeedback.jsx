import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/digitalfeedback.svg';

const DigitalFeedback = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default DigitalFeedback;
