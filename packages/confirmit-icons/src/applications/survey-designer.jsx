import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/survey-designer.svg';

const SurveyDesigner = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default SurveyDesigner;
