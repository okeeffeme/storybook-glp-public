import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/studio.svg';

const Studio = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Studio;
