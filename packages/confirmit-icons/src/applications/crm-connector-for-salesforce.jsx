import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/crm-connector-for-salesforce.svg';

const CrmConnectorForSalesforce = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default CrmConnectorForSalesforce;
