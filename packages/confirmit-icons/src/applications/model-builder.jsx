import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/model-builder.svg';

const ModelBuilder = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ModelBuilder;
