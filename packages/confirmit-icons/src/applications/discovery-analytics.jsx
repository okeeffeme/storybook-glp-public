import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/discovery-analytics.svg';

const DiscoveryAnalytics = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default DiscoveryAnalytics;
