import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/active-dashboards.svg';

const ActiveDashboards = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default ActiveDashboards;
