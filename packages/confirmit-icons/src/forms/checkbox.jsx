import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/checkbox.svg';

const CheckBox = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default CheckBox;
