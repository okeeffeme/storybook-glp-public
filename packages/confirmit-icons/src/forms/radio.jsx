import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/radio.svg';

const Radio = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Radio;
