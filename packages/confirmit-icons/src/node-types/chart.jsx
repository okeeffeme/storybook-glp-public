import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/chart.svg';

const Chart = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Chart;
