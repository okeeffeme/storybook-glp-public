import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/numeric.svg';

const Numeric = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Numeric;
