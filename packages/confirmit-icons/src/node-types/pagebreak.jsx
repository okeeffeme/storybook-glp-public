import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/pagebreak.svg';

const Pagebreak = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Pagebreak;
