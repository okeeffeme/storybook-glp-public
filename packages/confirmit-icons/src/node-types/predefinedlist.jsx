import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/predefinedlist.svg';

const Predefinedlist = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Predefinedlist;
