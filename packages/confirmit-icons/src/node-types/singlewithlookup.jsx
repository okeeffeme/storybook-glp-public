import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/singlewithlookup.svg';

const SingleWithLookup = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default SingleWithLookup;
