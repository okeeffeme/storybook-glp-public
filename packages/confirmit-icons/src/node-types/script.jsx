import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/script.svg';

const Script = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Script;
