import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/grid3d.svg';

const Grid3d = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Grid3d;
