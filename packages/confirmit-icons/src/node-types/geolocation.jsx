import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/geolocation.svg';

const Geolocation = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Geolocation;
