import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/date.svg';

const Date = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Date;
