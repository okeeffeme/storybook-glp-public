import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/block.svg';

const Block = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Block;
