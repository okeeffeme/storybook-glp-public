import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/codecapture.svg';

const Codecapture = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Codecapture;
