import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/open.svg';

const Open = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Open;
