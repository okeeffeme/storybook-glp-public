import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/callblock.svg';

const Callblock = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Callblock;
