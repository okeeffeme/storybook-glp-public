import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/loop.svg';

const Loop = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Loop;
