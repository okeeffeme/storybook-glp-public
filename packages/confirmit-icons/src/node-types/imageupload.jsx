import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/imageupload.svg';

const Imageupload = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Imageupload;
