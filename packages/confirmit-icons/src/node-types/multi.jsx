import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/multi.svg';

const Multi = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Multi;
