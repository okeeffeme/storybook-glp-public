import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/stop.svg';

const Stop = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Stop;
