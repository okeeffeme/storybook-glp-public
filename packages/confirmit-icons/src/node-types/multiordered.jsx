import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/multiordered.svg';

const Multiordered = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Multiordered;
