import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/condition.svg';

const Condition = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Condition;
