import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/directive.svg';

const Directive = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Directive;
