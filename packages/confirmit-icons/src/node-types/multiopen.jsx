import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/multiopen.svg';

const Multiopen = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Multiopen;
