import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/grid.svg';

const Grid = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Grid;
