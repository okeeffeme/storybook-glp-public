import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/single.svg';

const Single = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Single;
