import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/page.svg';

const Page = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Page;
