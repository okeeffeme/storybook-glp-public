import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/multigrid.svg';

const Multigrid = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Multigrid;
