import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/videocapture.svg';

const Videocapture = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Videocapture;
