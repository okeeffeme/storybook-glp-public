import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/audiocapture.svg';

const Audiocapture = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Audiocapture;
