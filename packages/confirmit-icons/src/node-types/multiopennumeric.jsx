import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/multiopennumeric.svg';

const Multiopennumeric = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Multiopennumeric;
