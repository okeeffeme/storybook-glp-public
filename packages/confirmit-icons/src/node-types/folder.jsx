import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/folder.svg';

const Folder = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Folder;
