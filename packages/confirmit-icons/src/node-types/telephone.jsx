import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/telephone.svg';

const Telephone = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Telephone;
