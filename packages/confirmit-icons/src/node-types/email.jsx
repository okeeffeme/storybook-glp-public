import React from 'react';

import IconWrapper from '../icon-wrapper';
import Icon from './svg/email.svg';

const Email = props => {
  return (
    <IconWrapper {...props}>
      <Icon />
    </IconWrapper>
  );
};

export default Email;
