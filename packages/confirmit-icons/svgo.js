/* global require __dirname process */
const path = require('path');
const glob = require('glob');
const shell = require('shelljs');

const folders = glob.sync(`${path.resolve(__dirname, 'src/*/svg')}`);

folders.forEach(folder => {
  const {code} = shell.exec(
    `cd node_modules/.bin && svgo --config=../../svgo.json --multipass --pretty --indent=2 -f ${folder}`
  );

  if (code !== 0) return process.exit(code);
});
