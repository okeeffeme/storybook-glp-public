import React from 'react';
import {storiesOf} from '@storybook/react';
import applications from './apps';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import * as Icons from '../src';

import './demo.scss';

storiesOf('Components/x_legacy_confirmit-icons', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Confirmit Logo', () => (
    <div className="confirmit-icons">
      <ul>
        <li>
          <Icons.ConfirmitLogo aria-label="ConfirmitLogo" />
        </li>
      </ul>
    </div>
  ))
  .add('Applications', () => (
    <div className="confirmit-icons">
      <ul>
        {Object.keys(Icons.Applications).map((icon) => {
          /* eslint import/namespace: ['error', { allowComputed: true }] */
          const Component = Icons.Applications[icon];
          return (
            <li key={icon}>
              <Component aria-label={icon} />
            </li>
          );
        })}
      </ul>
    </div>
  ))
  .add('AppIcon', () => (
    <div>
      <ul className="app-icon-list">
        {applications.map((app) => (
          <li style={{marginBottom: '20px'}} key={app.id}>
            <div>Name: {app.name}</div>
            <div>id: {app.id}</div>
            <Icons.AppIcon app={app} />
          </li>
        ))}
      </ul>
    </div>
  ));
