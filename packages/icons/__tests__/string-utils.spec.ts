import {createIconName, createExportStatement} from '../scripts/string-utils';
import {RESERVED_WORDS} from '../scripts/constants';

describe('Jotunheim React Icons :: String utils :: ', () => {
  it('should process files that starts with letters', () => {
    const result = createIconName('my-file.svg');

    expect(result).toBe('myFile');
  });

  it('should not return a icon name for files that starts with numbers', () => {
    const result = createIconName('1my-file.svg');

    expect(result).toBe(null);
  });

  RESERVED_WORDS.forEach((word) => {
    it(`should not return a icon name when using only reserved word '${word}' as the name`, () => {
      const result = createIconName(`${word}.svg`);

      expect(result).toBe(null);
    });

    it(`should return a icon name when starting with reserved word '${word}'`, () => {
      const result = createIconName(`${word}-some-name.svg`);

      expect(result).toBe(`${word}SomeName`);
    });
  });

  it('creates export statement based on valid input', async () => {
    const svg = `<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.444,22l8.556,-3" />
    </svg>
    `;
    const fileName = 'my-file.svg';

    const exportStatement = createExportStatement({svgFile: svg, fileName});

    expect(exportStatement).toBe(
      'export const myFile: string = "M13.444,22l8.556,-3";'
    );
  });

  it('does not create export statement when fileName is invalid', async () => {
    const svg = `<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.444,22l8.556,-3" />
    </svg>
    `;
    const fileName = '1my-file.svg';

    const exportStatement = createExportStatement({svgFile: svg, fileName});

    expect(exportStatement).toBe(undefined);
  });

  it('does not create export statement when svg element has more than 1 path', async () => {
    const svg = `<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.444,22l8.556,-3" />
      <path d="M13.444,22l8.556,-3" />
    </svg>
    `;
    const fileName = 'my-file.svg';

    const exportStatement = createExportStatement({svgFile: svg, fileName});

    expect(exportStatement).toBe(undefined);
  });

  it('does not create export statement when svg element has more than 1 child', async () => {
    const svg = `<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.444,22l8.556,-3" />
      <div />
    </svg>
    `;
    const fileName = 'my-file.svg';

    const exportStatement = createExportStatement({svgFile: svg, fileName});

    expect(exportStatement).toBe(undefined);
  });
});
