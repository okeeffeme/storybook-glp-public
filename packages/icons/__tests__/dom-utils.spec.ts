import {getSvgPath} from '../scripts/dom-utils';

describe('Jotunheim React Icons :: DOM utils :: ', () => {
  it('should return svg path d value for optimized file', async () => {
    const svg = `<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
    <path d="M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z" />
  </svg>
  `;

    const result = getSvgPath(svg, 'filename.svg');

    expect(result).toBe(
      'M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z'
    );
  });

  it('should return svg path d value for non-optimized file', async () => {
    const svg = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
        <path d="M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z" style="fill-rule:nonzero;"/>
    </svg>
    `;

    const result = getSvgPath(svg, 'filename.svg');

    expect(result).toBe(
      'M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z'
    );
  });

  it('should return null for invalid svg file with multiple paths', async () => {
    const svg = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
        <path d="M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z" style="fill-rule:nonzero;"/>
        <path d="M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z" style="fill-rule:nonzero;"/>
    </svg>
    `;

    const result = getSvgPath(svg, 'filename.svg');

    expect(result).toBe(null);
  });

  it('should return null for invalid svg file with incorrect viewbox', async () => {
    const svg = `<svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
    <path d="M13.444,22l8.556,-3.444l0,-9.667l-8.556,3.444l0,9.667Zm-11.444,-3.444l8.556,3.444l0,-9.667l-8.556,-3.444l0,9.667Zm10,-16.556l-10,4.111l10,4.111l10,-4.111l-10,-4.111Z" />
  </svg>
  `;

    const result = getSvgPath(svg, 'filename.svg');

    expect(result).toBe(null);
  });
});
