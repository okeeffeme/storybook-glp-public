import React from 'react';
import {render, screen} from '@testing-library/react';

import {colors} from '../../../global-styles/src';
import {
  SurveyIcon,
  surveyIcon,
  clipboardCheckMultiple,
  clipboardCheckMultipleOutline,
  clipboardExternal,
} from '../../src';

describe('Jotunheim React Icons :: Survey Icon :: ', () => {
  it('should show survey icon by default', () => {
    render(<SurveyIcon />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      surveyIcon
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product06Turquoise
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', undefined);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', undefined);
  });

  it('should show combined survey icon with "CombinedSurvey" variant', () => {
    render(<SurveyIcon variant={SurveyIcon.Variant.CombinedSurvey} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      clipboardCheckMultipleOutline
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product03GreenLight
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', undefined);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', undefined);
  });

  it('should show combined survey icon with "AutomappedSurvey" variant', () => {
    render(<SurveyIcon variant={SurveyIcon.Variant.AutoMappedSurvey} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      clipboardCheckMultiple
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product03GreenLight
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', undefined);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', undefined);
  });

  it('should load survey icon with "Survey" variant', () => {
    render(<SurveyIcon variant={SurveyIcon.Variant.Survey} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      surveyIcon
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product06Turquoise
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', undefined);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', undefined);
  });

  it('should load survey icon with "Survey" variant and allow to set size', () => {
    const size = '16';
    render(<SurveyIcon variant={SurveyIcon.Variant.Survey} size={size} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      surveyIcon
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product06Turquoise
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', size);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', size);
  });

  it('should load survey icon with "ForstaSurvey" variant', () => {
    render(<SurveyIcon variant={SurveyIcon.Variant.ForstaSurvey} />);
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      clipboardExternal
    );
    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product05TurquoiseDark
    );
    expect(screen.getByTestId('icon')).toHaveAttribute('height', undefined);
    expect(screen.getByTestId('icon')).toHaveAttribute('width', undefined);
  });
});
