import React from 'react';
import {render} from '@testing-library/react';

import {colors} from '../../../global-styles/src';
import {
  actions,
  ActionsIcon,
  contacts,
  ContactsIcon,
  graphOutline,
  TextAnalyticsIcon,
} from '../../src';

const fillColors = {
  ActionsIcon: colors.Product14Orange,
  ContactsIcon: colors.Product04TurquoiseDark,
  TextAnalyticsIcon: colors.Product01GreenDark,
};
const paths = {
  ActionsIcon: actions,
  ContactsIcon: contacts,
  TextAnalyticsIcon: graphOutline,
};
const DEFAULT_SIZE = '24';

describe('Jotunheim React Icons :: ', () => {
  [TextAnalyticsIcon, ActionsIcon, ContactsIcon].forEach((DatasetIcon) => {
    describe(`${DatasetIcon.displayName} :: `, () => {
      it(`should show the icon by default`, () => {
        const {container} = render(<DatasetIcon />);

        expect(container.querySelector('svg')).toBeTruthy();
        expect(container.querySelector('svg')).toHaveAttribute(
          'fill',
          fillColors[DatasetIcon.displayName ?? '']
        );
        expect(container.querySelector('svg')).toHaveAttribute(
          'width',
          DEFAULT_SIZE
        );
        expect(container.querySelector('svg')).toHaveAttribute(
          'height',
          DEFAULT_SIZE
        );
        expect(container.querySelector('path')).toHaveAttribute(
          'd',
          paths[DatasetIcon.displayName ?? '']
        );
      });

      it(`should allow to set size`, () => {
        const customSize = '16';

        const {container} = render(<DatasetIcon size={customSize} />);

        expect(container.querySelector('svg')).toBeTruthy();
        expect(container.querySelector('svg')).toHaveAttribute(
          'width',
          customSize
        );
        expect(container.querySelector('svg')).toHaveAttribute(
          'height',
          customSize
        );
      });
    });
  });
});
