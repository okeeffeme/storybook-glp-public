import React from 'react';
import {render, screen} from '@testing-library/react';
import {colors} from '../../../global-styles/src';
import {DigitalFeedbackIcon} from '../../src';

describe('Jotunheim React Icons :: DigitalFeedbackIcon :: ', () => {
  it('should pass no fill when no variant is passed', () => {
    render(<DigitalFeedbackIcon />);

    expect(screen.getByTestId('icon')).not.toHaveAttribute('fill');
  });

  it('should pass no fill when variant is set to an unknown variant', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore  - want to explicitly break the contract of props
    render(<DigitalFeedbackIcon variant="unknown" />);

    expect(screen.getByTestId('icon')).not.toHaveAttribute('fill');
  });

  it('should pass correct fill when Sdk program variant is passed', () => {
    render(
      <DigitalFeedbackIcon variant={DigitalFeedbackIcon.Variant.SdkProgram} />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product03GreenLight
    );
  });

  it('should pass correct fill when Web program variant is passed', () => {
    render(
      <DigitalFeedbackIcon variant={DigitalFeedbackIcon.Variant.WebProgram} />
    );

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product05TurquoiseDark
    );
  });
});
