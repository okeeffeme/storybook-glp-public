import React from 'react';
import {render, screen} from '@testing-library/react';

import {colors} from '../../../global-styles/src';
import {InformationIcon} from '../../src';

describe('Jotunheim React Icons :: InformationIcon :: ', () => {
  it('should pass correct fill', () => {
    render(<InformationIcon />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.TextDisabled
    );
  });
});
