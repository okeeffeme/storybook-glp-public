import React from 'react';
import {render, screen} from '@testing-library/react';
import {ReportIcon} from '../../src';
import {colors} from '../../../global-styles/src';

describe('Jotunheim React Icons :: ReportIcon :: ', () => {
  it('should have the correct fill when no variant is passed', () => {
    render(<ReportIcon />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.PrimaryAccent
    );
  });

  it('should have the correct fill when variant is set to report dashboard', () => {
    render(<ReportIcon variant={ReportIcon.Variant.ReportDashboard} />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.PrimaryAccent
    );
  });

  it('should have the correct fill when variant is set to fieldwork report', () => {
    render(<ReportIcon variant={ReportIcon.Variant.FieldworkReport} />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product09Purple
    );
  });

  it('should have the correct fill when  variant is set to program report', () => {
    render(<ReportIcon variant={ReportIcon.Variant.ProgramReport} />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.Product13Orange
    );
  });
});
