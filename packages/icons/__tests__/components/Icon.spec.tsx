import React from 'react';
import {render, screen} from '@testing-library/react';

import {colors} from '../../../global-styles/src';
import {Icon} from '../../src/components/Icon';

describe('Jotunheim React Icons :: ', () => {
  it('renders', () => {
    render(<Icon path="abc" />);
    expect(
      screen.getByTestId('icon').querySelector('path')
    ).toBeInTheDocument();
    expect(screen.getByTestId('icon')).toBeInTheDocument();
    expect(screen.getByTestId('icon').querySelector('path')).toHaveAttribute(
      'd',
      'abc'
    );
  });

  it('should pass "fill" prop to svg element', () => {
    render(<Icon path="abc" fill="#ABCDEF" />);

    expect(screen.getByTestId('icon')).toHaveAttribute('fill', '#ABCDEF');
  });

  it('should set fill to default value when no fill is passed', () => {
    render(<Icon path="abc" />);

    expect(screen.getByTestId('icon')).toHaveAttribute(
      'fill',
      colors.TextSecondary
    );
  });
});
