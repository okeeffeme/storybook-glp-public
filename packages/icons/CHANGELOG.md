# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.19.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.19.2&sourceBranch=refs/tags/@jotunheim/react-icons@3.19.2&targetRepoId=1246) (2023-04-06)

**Note:** Version bump only for package @jotunheim/react-icons

## [3.19.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.19.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.19.1&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-icons

# [3.19.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.18.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.19.0&targetRepoId=1246) (2023-03-28)

### Features

- add icons for surveys list context menu ([CATI-4293](https://jiraosl.firmglobal.com/browse/CATI-4293)) ([be49268](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/be49268040e31c33907fb686378c58a1e9a4c013))

# [3.18.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.17.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.18.0&targetRepoId=1246) (2023-03-17)

### Features

- add MaxDiff icon ([NPM-1277](https://jiraosl.firmglobal.com/browse/NPM-1277)) ([54ece9e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/54ece9e978c40fcb171963b0383c019a6d325f29))

# [3.17.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.16.3&sourceBranch=refs/tags/@jotunheim/react-icons@3.17.0&targetRepoId=1246) (2023-03-16)

### Features

- add icon for retained recordings ([CATI-4309](https://jiraosl.firmglobal.com/browse/CATI-4309)) ([6c8f149](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6c8f14986683c9fa74a2b2af564fdf9097570911))

## [3.16.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.16.2&sourceBranch=refs/tags/@jotunheim/react-icons@3.16.3&targetRepoId=1246) (2023-03-02)

**Note:** Version bump only for package @jotunheim/react-icons

## [3.16.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.16.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.16.2&targetRepoId=1246) (2023-02-28)

### Bug Fixes

- component SurveyIcon ([NPM-1194](https://jiraosl.firmglobal.com/browse/NPM-1194)) ([4607967](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4607967ae08852e69e819ae50a6ccf764ef137b3))

## [3.16.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.16.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.16.1&targetRepoId=1246) (2023-02-14)

**Note:** Version bump only for package @jotunheim/react-icons

# [3.16.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.15.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.16.0&targetRepoId=1246) (2023-02-02)

### Bug Fixes

- remove deprecated filter in storybook within the icons package ([NPM-1244](https://jiraosl.firmglobal.com/browse/NPM-1244)) ([14968e2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/14968e263805e306e2c3dd61d119b4bd38ac3a06))

### Features

- add icons for moving files and folders ([AUT-6427](https://jiraosl.firmglobal.com/browse/AUT-6427)) ([c8f1ae8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c8f1ae8f087ba74993893960ffa1a0f58c2d5023))

# [3.15.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.14.3&sourceBranch=refs/tags/@jotunheim/react-icons@3.15.0&targetRepoId=1246) (2023-01-30)

### Features

- convert icons package to TypeScript ([NPM-1231](https://jiraosl.firmglobal.com/browse/NPM-1231)) ([1426a8b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1426a8b5505de559dc88eeaa760c1997b2e0d029))

## [3.14.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.14.2&sourceBranch=refs/tags/@jotunheim/react-icons@3.14.3&targetRepoId=1246) (2023-01-23)

**Note:** Version bump only for package @jotunheim/react-icons

## [3.14.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.14.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.14.2&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- adding data-testid ([NPM-1188](https://jiraosl.firmglobal.com/browse/NPM-1188)) ([38609ce](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/38609cec25d907f540f9632f944083e88d4a9778))

## [3.14.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.14.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.14.1&targetRepoId=1246) (2023-01-19)

### Bug Fixes

- adding data-testid ([NPM-1188](https://jiraosl.firmglobal.com/browse/NPM-1188)) ([38609ce](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/38609cec25d907f540f9632f944083e88d4a9778))

# [3.14.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.13.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.14.0&targetRepoId=1246) (2023-01-05)

### Features

- Add Forsta Survey indicator icon ([NPM-1225](https://jiraosl.firmglobal.com/browse/NPM-1225)) ([9fc63ba](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9fc63babf7e828aa4dc2895d558072ff3be60048))

# [3.13.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.12.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.13.0&targetRepoId=1246) (2022-12-27)

### Features

- **icons:** add fileMultiple icon to use in survey designer ([NPM-1223](https://jiraosl.firmglobal.com/browse/NPM-1223)) ([da026f9](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/da026f9325e533b933f3f76e1131e581b3eabcbb))

# [3.12.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.10.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.12.0&targetRepoId=1246) (2022-12-22)

### Bug Fixes

- add test id for Icon component ([NPM-1172](https://jiraosl.firmglobal.com/browse/NPM-1172)) ([ac4f977](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ac4f9777bd80409b7050d0d6e32d9ffec11735a5))

### Features

- add key icon to icons package ([NPM-1131](https://jiraosl.firmglobal.com/browse/NPM-1131)) ([52575c4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/52575c4fdefc34698c1847abee00e6cf5785a146))

# [3.11.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.10.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.11.0&targetRepoId=1246) (2022-12-16)

### Features

- add key icon to icons package ([NPM-1131](https://jiraosl.firmglobal.com/browse/NPM-1131)) ([52575c4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/52575c4fdefc34698c1847abee00e6cf5785a146))

## [3.10.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.8.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.10.1&targetRepoId=1246) (2022-12-15)

**Note:** Version bump only for package @jotunheim/react-icons

# [3.10.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.8.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.10.0&targetRepoId=1246) (2022-12-06)

### Features

- added svg icon for Brandwatch ([8f400af](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8f400afc884b9689b51ff789af058b2fe6a1ad2a))

# [3.9.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.8.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.9.0&targetRepoId=1246) (2022-12-02)

### Features

- add new icons for AP ([AP-2163](https://jiraosl.firmglobal.com/browse/AP-2163)) ([076aa20](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/076aa207e4b5110109bfb1132a842391838e4e90))

# [3.8.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.7.4&sourceBranch=refs/tags/@jotunheim/react-icons@3.8.0&targetRepoId=1246) (2022-11-16)

### Features

- add new icons for DI ([DI-294](https://jiraosl.firmglobal.com/browse/DI-294)) ([219785d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/219785dc95e258b3729414ec04daf887794efa8b))

## [3.7.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.7.3&sourceBranch=refs/tags/@jotunheim/react-icons@3.7.4&targetRepoId=1246) (2022-11-04)

**Note:** Version bump only for package @jotunheim/react-icons

## [3.7.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.7.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.7.2&targetRepoId=1246) (2022-10-13)

### Features

- Add colored Report icon component

## [3.7.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.7.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.7.2&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-icons

## [3.7.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.7.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.7.1&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-icons

# [3.7.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.6.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.7.0&targetRepoId=1246) (2022-10-03)

### Features

- Add icon for CATI chat ([CATI-4212](https://jiraosl.firmglobal.com/browse/CATI-4212)) ([5f010af](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5f010af76be35af17e13d5c254ef10b8066535ef))

# [3.6.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.5.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.6.0&targetRepoId=1246) (2022-09-28)

### Features

- new zoom-in and zoom-out icons are added ([NPM-1086](https://jiraosl.firmglobal.com/browse/NPM-1086)) ([ec80da7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec80da7363becaf6b867b68bd0a42fe624db6b07))

# [3.5.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.4.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.5.0&targetRepoId=1246) (2022-09-08)

### Features

- add digital interviews icon ([DI-66](https://jiraosl.firmglobal.com/browse/DI-66)) ([5072b02](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5072b02b5b4d866caa3d6e7105a4c959b4c16ea3))

# [3.4.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.3.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.4.0&targetRepoId=1246) (2022-09-03)

### Features

- new check-circle-clock icon added ([SE-3617](https://jiraosl.firmglobal.com/browse/SE-3617)) ([2c45538](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2c45538bc930460487a92a2af29b0383acced8dc))

# [3.3.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.2.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.3.0&targetRepoId=1246) (2022-08-25)

### Features

- new pencil-ruler icon added ([NPM-1053](https://jiraosl.firmglobal.com/browse/NPM-1053)) ([8bfcabd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8bfcabdcce22b3d762a7fb7be872c89a8dac8f73))

# [3.1.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.0.1&sourceBranch=refs/tags/@jotunheim/react-icons@3.1.0&targetRepoId=1246) (2022-08-12)

### Features

- Add icons for CATI playback ([CATI-4218](https://jiraosl.firmglobal.com/browse/CATI-4218)) ([ad9e1cd](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ad9e1cdb85fada67097fd542460fe875feb48cd8))

## [3.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-icons@3.0.0&sourceBranch=refs/tags/@jotunheim/react-icons@3.0.1&targetRepoId=1246) (2022-07-15)

**Note:** Version bump only for package @jotunheim/react-icons

# 3.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

# [2.65.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.64.0&sourceBranch=refs/tags/@confirmit/react-icons@2.65.0&targetRepoId=1246) (2022-06-29)

### Bug Fixes

- update package name for react-alert ([NPM-1029](https://jiraosl.firmglobal.com/browse/NPM-1029)) ([a7d22c4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a7d22c4d63d93752e531b216cb8ee1b6132492bb))

### Features

- added home icon to Jotunheim ([NPM-1033](https://jiraosl.firmglobal.com/browse/NPM-1033)) ([7537ad7](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7537ad75e581bf66293f6e7a385b970dc53eedc4))

### Reverts

- Revert "fix: update package name for react-alert (NPM-1029)" ([bddc2f2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bddc2f229ea4a5a02de2f985ac10d404a7b8a938))

# [2.64.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.63.2&sourceBranch=refs/tags/@confirmit/react-icons@2.64.0&targetRepoId=1246) (2022-06-27)

### Features

- add briefcase-variant icon ([NPM-1030](https://jiraosl.firmglobal.com/browse/NPM-1030)) ([ec3849f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ec3849f78a38460f1f94457543c4ed87436f2183))

## [2.63.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.63.1&sourceBranch=refs/tags/@confirmit/react-icons@2.63.2&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

## [2.63.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.63.0&sourceBranch=refs/tags/@confirmit/react-icons@2.63.1&targetRepoId=1246) (2022-05-18)

**Note:** Version bump only for package @confirmit/react-icons

# [2.63.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.62.1&sourceBranch=refs/tags/@confirmit/react-icons@2.63.0&targetRepoId=1246) (2022-04-18)

### Features

- Add icons for CATI monitoring ([CATI-4002](https://jiraosl.firmglobal.com/browse/CATI-4002)) ([686d90f](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/686d90f6c84822240e36087cf06b06bade5b1355))

## [2.62.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.62.0&sourceBranch=refs/tags/@confirmit/react-icons@2.62.1&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-icons

# [2.62.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.61.0&sourceBranch=refs/tags/@confirmit/react-icons@2.62.0&targetRepoId=1246) (2022-03-14)

### Features

- add external survey icon ([NPM-988](https://jiraosl.firmglobal.com/browse/NPM-988)) ([3c67ed8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3c67ed85c35e527267b3f4797350a769cab3e269))

# [2.61.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.60.0&sourceBranch=refs/tags/@confirmit/react-icons@2.61.0&targetRepoId=1246) (2022-03-02)

### Features

- Add icons for CATI monitoring ([CATI-4060](https://jiraosl.firmglobal.com/browse/CATI-4060)) ([d772764](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d7727642287b4d3cde6baaae5eee4e1c5dfbaa6b))

# [2.60.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.59.0&sourceBranch=refs/tags/@confirmit/react-icons@2.60.0&targetRepoId=1246) (2022-02-25)

### Features

- Add new in app feedback icon ([SE-3562](https://jiraosl.firmglobal.com/browse/SE-3562)) ([a80e742](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a80e7420656a3d5a9006783599005d63ee2eea6a))

# 2.59.0

### Features

- Icon for multiple files and folders

# [2.58.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.57.0&sourceBranch=refs/tags/@confirmit/react-icons@2.58.0&targetRepoId=1246) (2022-01-19)

### Features

- Supported to set size for TextAnalyticsIcon, ActionsIcon and ContactsIcon ([NPM-912](https://jiraosl.firmglobal.com/browse/NPM-912)) ([3fa34ec](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3fa34ec16ba05f1697bba04982b3b3d5b746230c))

# [2.57.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.56.0&sourceBranch=refs/tags/@confirmit/react-icons@2.57.0&targetRepoId=1246) (2022-01-14)

### Features

- added text analytics coded icon ([NPM-909](https://jiraosl.firmglobal.com/browse/NPM-909)) ([34dc498](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/34dc498c5cda305f8e3f611005c0da9f7b847d1c))

# [2.56.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.55.0&sourceBranch=refs/tags/@confirmit/react-icons@2.56.0&targetRepoId=1246) (2021-12-30)

### Features

- Add icons for CATI BBCC ([CATI-3385](https://jiraosl.firmglobal.com/browse/CATI-3385)) ([c8fa38a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c8fa38a9e533147caa2e09635418cb0715f7b235))

# [2.55.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.54.0&sourceBranch=refs/tags/@confirmit/react-icons@2.55.0&targetRepoId=1246) (2021-12-23)

### Features

- add Pulse icon ([NPM-903](https://jiraosl.firmglobal.com/browse/NPM-903)) ([027fb85](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/027fb8548fba84b4ee2fe7da9bf91f6ab8b5d9f8))

# [2.54.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.53.0&sourceBranch=refs/tags/@confirmit/react-icons@2.54.0&targetRepoId=1246) (2021-11-29)

### Features

- replace icon ([AUT-6115](https://jiraosl.firmglobal.com/browse/AUT-6115)) ([625a4c8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/625a4c81d601ea0834c9fec3d8b6776a7e34cadc))

# [2.53.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.52.1&sourceBranch=refs/tags/@confirmit/react-icons@2.53.0&targetRepoId=1246) (2021-11-23)

### Features

- add icons for selecting and unselecting multiple sub-nodes ([NPM-739](https://jiraosl.firmglobal.com/browse/NPM-739)) ([a3e4a89](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a3e4a891a178e9a7bf6c7c376d41848c3659f810))

## [2.52.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.52.0&sourceBranch=refs/tags/@confirmit/react-icons@2.52.1&targetRepoId=1246) (2021-10-20)

### Bug Fixes

- replace @confirmit/react-modal with @confirmit/react-dialog because of package renaming ([NPM-347](https://jiraosl.firmglobal.com/browse/NPM-347)) ([cb47cac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cb47cac4cebcc454b6e83b9d461f168b5edf5311))

# [2.51.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.50.1&sourceBranch=refs/tags/@confirmit/react-icons@2.51.0&targetRepoId=1246) (2021-09-14)

### Features

- Add message icon ([SE-3443](https://jiraosl.firmglobal.com/browse/SE-3443)) ([6dfba02](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6dfba02b704d33b9a2bd5c8987d0b256fac454c5))

## [2.50.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.50.0&sourceBranch=refs/tags/@confirmit/react-icons@2.50.1&targetRepoId=1246) (2021-09-13)

### Bug Fixes

- update listViewTree icon to have correct dimensions ([NPM-792](https://jiraosl.firmglobal.com/browse/NPM-792)) ([211cf9a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/211cf9ac60616060804df523b90206e4f31b22be))

# [2.50.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.49.0&sourceBranch=refs/tags/@confirmit/react-icons@2.50.0&targetRepoId=1246) (2021-08-20)

### Features

- add clipboardCreate, digitalFeedbackProgramCreate and tableCreate icons ([NPM-825](https://jiraosl.firmglobal.com/browse/NPM-825)) ([0632e98](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0632e98e5e557bbb77f1f40a49201034a61649f8))

# [2.49.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.48.0&sourceBranch=refs/tags/@confirmit/react-icons@2.49.0&targetRepoId=1246) (2021-08-05)

### Features

- add icons for move up/down/to-top/to-bottom ([NPM-839](https://jiraosl.firmglobal.com/browse/NPM-839)) ([082621b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/082621b99d12ee6c199b501843bbc6d398f49a2d))

# [2.48.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.47.0&sourceBranch=refs/tags/@confirmit/react-icons@2.48.0&targetRepoId=1246) (2021-07-28)

### Features

- Added new tick marks icons ([SVD-1273](https://jiraosl.firmglobal.com/browse/SVD-1273)) ([3bed1fb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3bed1fb163631bfa8f0cbc76a8c36e96e670f301))

# [2.47.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.46.0&sourceBranch=refs/tags/@confirmit/react-icons@2.47.0&targetRepoId=1246) (2021-07-15)

### Features

- Add workflows icon ([AM-6812](https://jiraosl.firmglobal.com/browse/AM-6812)) ([6ad76a4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6ad76a47800cc9e3533994e51072526d8f863314))

# [2.46.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.45.0&sourceBranch=refs/tags/@confirmit/react-icons@2.46.0&targetRepoId=1246) (2021-07-12)

### Features

- Add new icons for Salesforce and MsDynamics ([HUB-8296](https://jiraosl.firmglobal.com/browse/HUB-8296)) ([43d7958](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/43d795873a74841c451058eeadf4633ca6d94a98))

# [2.45.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.44.0&sourceBranch=refs/tags/@confirmit/react-icons@2.45.0&targetRepoId=1246) (2021-07-01)

### Features

- Add response icon. ([HUB-7943](https://jiraosl.firmglobal.com/browse/HUB-7943)) ([755cdb6](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/755cdb65267fd57ff8a09d93892c38826458f6e0))

# [2.44.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.43.0&sourceBranch=refs/tags/@confirmit/react-icons@2.44.0&targetRepoId=1246) (2021-07-01)

### Features

- Add icons for Model Builder ([NPM-813](https://jiraosl.firmglobal.com/browse/NPM-813)) ([7958e4d](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7958e4d0533f0c23e867840ca470ab4efd6d9a11))

# [2.43.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.42.1&sourceBranch=refs/tags/@confirmit/react-icons@2.43.0&targetRepoId=1246) (2021-06-24)

### Features

- Add Digital Feedback icons ([SE-3309](https://jiraosl.firmglobal.com/browse/SE-3309)) ([3a644ba](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/3a644baa20b15f719e9219b266e0c33fdea99c84))

## [2.42.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.42.0&sourceBranch=refs/tags/@confirmit/react-icons@2.42.1&targetRepoId=1246) (2021-06-03)

**Note:** Version bump only for package @confirmit/react-icons

# [2.42.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.41.0&sourceBranch=refs/tags/@confirmit/react-icons@2.42.0&targetRepoId=1246) (2021-06-01)

### Features

- new survey node icon, 'noteText' ([NPM-796](https://jiraosl.firmglobal.com/browse/NPM-796)) ([8ff2387](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/8ff2387a4fe0ada2816a1406ac81c4d31bec636d))

# [2.41.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.40.0&sourceBranch=refs/tags/@confirmit/react-icons@2.41.0&targetRepoId=1246) (2021-05-31)

### Features

- Added new icons for shape property in Scatter Chart ([SVD-1616](https://jiraosl.firmglobal.com/browse/SVD-1616)) ([d806662](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d806662163a8ad816e9b5d2b7e038f75b172dc19))

# [2.40.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.39.0&sourceBranch=refs/tags/@confirmit/react-icons@2.40.0&targetRepoId=1246) (2021-05-27)

### Features

- three new thumbs icons added ([NPM-759](https://jiraosl.firmglobal.com/browse/NPM-759)) ([cec4a3c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/cec4a3cf11e49e1577927ea59ce2bd82f8b17997))

# [2.39.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.38.0&sourceBranch=refs/tags/@confirmit/react-icons@2.39.0&targetRepoId=1246) (2021-05-25)

### Features

- answer, scale, add-answer, add-scale, link-variant-off, call-merge icons ([NPM-785](https://jiraosl.firmglobal.com/browse/NPM-785)) ([5f2fc55](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5f2fc55f39e5c84f4497a93a3f2444389d6450d2))

# [2.38.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.37.1&sourceBranch=refs/tags/@confirmit/react-icons@2.38.0&targetRepoId=1246) (2021-05-17)

### Features

- add Calculator icon ([SREP-4485](https://jiraosl.firmglobal.com/browse/SREP-4485)) ([d2db6da](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/d2db6da68bfc9d4858fc0889d1611c321ca1f72b))

# [2.37.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.37.0&sourceBranch=refs/tags/@confirmit/react-icons@2.37.1&targetRepoId=1246) (2021-05-13)

### Features

- add survey icon as color coded icon

# [2.37.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.36.0&sourceBranch=refs/tags/@confirmit/react-icons@2.37.0&targetRepoId=1246) (2021-04-29)

### Features

- adding an icon of decimal value ([SREP-4485](https://jiraosl.firmglobal.com/browse/SREP-4485)) ([ebee951](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/ebee95101719fb2b3f454a02020202e2f559282a))

# [2.36.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.35.1&sourceBranch=refs/tags/@confirmit/react-icons@2.36.0&targetRepoId=1246) (2021-04-20)

### Features

- add checkbox icons for inherited and indeterminate states ([NPM-471](https://jiraosl.firmglobal.com/browse/NPM-471)) ([0cb42b8](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/0cb42b87d950013354ced29734b0b4f1f4bc39b1))

## [2.35.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.35.0&sourceBranch=refs/tags/@confirmit/react-icons@2.35.1&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

# [2.35.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.34.1&sourceBranch=refs/tags/@confirmit/react-icons@2.35.0&targetRepoId=1246) (2021-04-06)

### Features

- add digitalFeedback icon ([NPM-754](https://jiraosl.firmglobal.com/browse/NPM-754)) ([bdd5d97](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/bdd5d97fdb5a1e852655d5eb3813dd2ec3d887fa))
- add DigitalFeedbackIcon component, which contains different variants with fixed colors for different usecases ([NPM-754](https://jiraosl.firmglobal.com/browse/NPM-754)) ([c0be0ac](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c0be0aca087f769393fe9c3660d7b5c11935ddd5))

## [2.34.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.34.0&sourceBranch=refs/tags/@confirmit/react-icons@2.34.1&targetRepoId=1246) (2021-03-17)

**Note:** Version bump only for package @confirmit/react-icons

# [2.34.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.33.0&sourceBranch=refs/tags/@confirmit/react-icons@2.34.0&targetRepoId=1246) (2021-03-17)

### Features

- [SVD-1464](https://jiraosl.firmglobal.com/browse/SVD-1464) Add subdirectory-arrow-right icon ([SVD-1464](https://jiraosl.firmglobal.com/browse/SVD-1464)) ([6fdb735](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6fdb73534e7cd019854f8aaac646ae1c39a90959))
- [SVD-1464](https://jiraosl.firmglobal.com/browse/SVD-1464) Added new icon 'graph-outline' ([SVD-1464](https://jiraosl.firmglobal.com/browse/SVD-1464)) ([c833646](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/c833646db52888569f7e49711431b194607cb9b2))

# [2.33.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.32.0&sourceBranch=refs/tags/@confirmit/react-icons@2.33.0&targetRepoId=1246) (2021-03-15)

### Features

- increase size of menu-down and -right icons, so they work better in our IconButton when size is 'small'. Also introducing the -left and -up variants of this icon. ([NPM-712](https://jiraosl.firmglobal.com/browse/NPM-712)) ([f961c62](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f961c621d18647df1c6326bb453345a492f4b7c9))

# [2.32.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.31.0&sourceBranch=refs/tags/@confirmit/react-icons@2.32.0&targetRepoId=1246) (2021-03-05)

### Features

- add functionVariant icon ([SVD-1354](https://jiraosl.firmglobal.com/browse/SVD-1354)) ([dfd832c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dfd832c1ecfd318c072d5d222f52ed65c3a1f409))

# [2.31.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.30.1&sourceBranch=refs/tags/@confirmit/react-icons@2.31.0&targetRepoId=1246) (2021-02-15)

### Features

- add listStarted icon ([NPM-710](https://jiraosl.firmglobal.com/browse/NPM-710)) ([85b8a5b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/85b8a5bf52dd8229e8ad82a15de9cc8dd70ae940))

## [2.30.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.30.0&sourceBranch=refs/tags/@confirmit/react-icons@2.30.1&targetRepoId=1246) (2021-02-01)

**Note:** Version bump only for package @confirmit/react-icons

# [2.30.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.29.0&sourceBranch=refs/tags/@confirmit/react-icons@2.30.0&targetRepoId=1246) (2021-01-27)

### Features

- add login icon ([NPM-693](https://jiraosl.firmglobal.com/browse/NPM-693)) ([edf43fb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/edf43fbfbf876b461d16526bae634ac24c0b3393))

# [2.29.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.28.0&sourceBranch=refs/tags/@confirmit/react-icons@2.29.0&targetRepoId=1246) (2021-01-21)

### Bug Fixes

- update type ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([1ade0f3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1ade0f3ce108a8357b7904b32bdc958836ca48de))

### Features

- add event prop extraction for InformationIcon ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([fdfa3cb](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fdfa3cb35cb9daf9781a9305f948077c59259570))
- add InformationIcon, and internal refactor ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([f88891c](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/f88891ca9715c9b56823e5b553804aa44dd6ae40))
- use InformationIcon in components, and style cleanup ([NPM-631](https://jiraosl.firmglobal.com/browse/NPM-631)) ([5436ea1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5436ea15a84f20bcdbecbe74724026d532ed41de))

# [2.28.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.27.0&sourceBranch=refs/tags/@confirmit/react-icons@2.28.0&targetRepoId=1246) (2021-01-19)

### Features

- new pencilLock icon for read only mode ([NPM-675](https://jiraosl.firmglobal.com/browse/NPM-675)) ([7ef7c4e](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7ef7c4e1c8ea6ade3917f288296c67863a3a9171))

# [2.27.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.26.0&sourceBranch=refs/tags/@confirmit/react-icons@2.27.0&targetRepoId=1246) (2020-12-10)

### Features

- Add dots-horizontal icon ([HUB-7518](https://jiraosl.firmglobal.com/browse/HUB-7518)) ([7f33c9a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7f33c9a37356b7d2df46626f87d2bd1a43407232))

# [2.26.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.25.0&sourceBranch=refs/tags/@confirmit/react-icons@2.26.0&targetRepoId=1246) (2020-12-10)

### Features

- add email-lock icon ([EUM-200](https://jiraosl.firmglobal.com/browse/EUM-200)) ([65cc028](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/65cc0281b1506128db6a52ad76ea01193155c601))

# [2.25.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.24.1&sourceBranch=refs/tags/@confirmit/react-icons@2.25.0&targetRepoId=1246) (2020-12-10)

### Features

- Add Redo icon ([NPM-636](https://jiraosl.firmglobal.com/browse/NPM-636)) ([eadcfe5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eadcfe563534f57f0078199734bf7e2ff3ec1371))

## [2.24.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.24.0&sourceBranch=refs/tags/@confirmit/react-icons@2.24.1&targetRepoId=1246) (2020-12-04)

**Note:** Version bump only for package @confirmit/react-icons

# 2.24.0 (2020-12-03)

### Features

- add calendar-clock icon

## [2.23.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.23.0&sourceBranch=refs/tags/@confirmit/react-icons@2.23.1&targetRepoId=1246) (2020-12-03)

**Note:** Version bump only for package @confirmit/react-icons

# 2.23.0 (2020-11-24)

### Features

- add list-status icon

# [2.22.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.21.0&sourceBranch=refs/tags/@confirmit/react-icons@2.22.0&targetRepoId=1246) (2020-11-23)

### Features

- add account-plus icon ([NPM-626](https://jiraosl.firmglobal.com/browse/NPM-626)) ([eda3c2a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/eda3c2a2f3406677dff5f3c2a16ac61d2bd9f7c0))

# [2.21.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.20.0&sourceBranch=refs/tags/@confirmit/react-icons@2.21.0&targetRepoId=1246) (2020-11-20)

### Bug Fixes

- standadize svg image ([NPM-621](https://jiraosl.firmglobal.com/browse/NPM-621)) ([6068eb0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/6068eb0bef1e3ccbd420af9b0048c5c26cfa3eb9))
- update icon meta-data ([NPM-621](https://jiraosl.firmglobal.com/browse/NPM-621)) ([5ad24ad](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/5ad24ad96b2e5af530e9d858d99c184bd82e8702))

### Features

- mobile-friendly icon is added ([NPM-621](https://jiraosl.firmglobal.com/browse/NPM-621)) ([34d4381](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/34d43811f6382880ea7487fb2e8f3ecf124cf56a))

# [2.20.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.19.0&sourceBranch=refs/tags/@confirmit/react-icons@2.20.0&targetRepoId=1246) (2020-11-19)

### Features

- add account-edit and account-alert icons ([NPM-619](https://jiraosl.firmglobal.com/browse/NPM-619)) ([20dce2b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/20dce2bcb678fd74a37aa642d199e0157387b210))

# [2.19.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/pull-requests/712/diff) (2020-11-17)

### Features

- add filter-off icon ([NPM-611](https://jiraosl.firmglobal.com/browse/NPM-611))

# [2.18.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.17.0&sourceBranch=refs/tags/@confirmit/react-icons@2.18.0&targetRepoId=1246) (2020-11-06)

### Features

- add information icon variant that is used for displaying extra information in a tooltip ([NPM-585](https://jiraosl.firmglobal.com/browse/NPM-585)) ([669f951](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/669f9516c6cc45a46419d993a3c2387dea37a896))

# [2.17.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.16.0&sourceBranch=refs/tags/@confirmit/react-icons@2.17.0&targetRepoId=1246) (2020-10-15)

**Features:**

- add plus-box-multiple icon
- add database-edit icon

# v2.16.0

- Feat: Added account-group icon.

## [2.15.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.14.0&sourceBranch=refs/tags/@confirmit/react-icons@2.14.1&targetRepoId=1246) (2020-10-13)

### Features

- add text-subject icon

# [2.14.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.13.0&sourceBranch=refs/tags/@confirmit/react-icons@2.14.0&targetRepoId=1246) (2020-10-13)

### Features

- add chartBox icon ([NPM-562](https://jiraosl.firmglobal.com/browse/NPM-562)) ([2a22673](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2a22673080fa10bc94b126fbdd4f529ce84fd256))

# [2.13.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.12.0&sourceBranch=refs/tags/@confirmit/react-icons@2.13.0&targetRepoId=1246) (2020-10-06)

### Bug Fixes

- trashBin icon was incorrect size (too big), and not pixel aligned, which caused it to be blurry in many cases ([NPM-537](https://jiraosl.firmglobal.com/browse/NPM-537)) ([14aa46b](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/14aa46b4f8eb2ae5afcdc977b6a7cd4383d97f62)

### Features

- new icons, fileMusic, fileCode, fileVideo ([NPM-537](https://jiraosl.firmglobal.com/browse/NPM-537)) ([b71a845](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/b71a8459fa7a7e92293dd708482e051508947638)

# v2.12.0

- Feat: Added alpha-d-box icon.

# v2.11.0 (2020-09-28)

### Features

- **icons:** add euro and gbp currency icons ([AM-6027](https://jiraosl.firmglobal.com/browse/AM-6027)).

## [2.10.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.10.0&sourceBranch=refs/tags/@confirmit/react-icons@2.10.1&targetRepoId=1246) (2020-09-28)

**Note:** Version bump only for package @confirmit/react-icons

# v2.10.0 (2020-09-24)

### Features

- **icons:** add arrow-expand-horizontal icon as justify content alignment icon for Survey Designer ([SE-2933](https://jiraosl.firmglobal.com/browse/SE-2933)).

# [2.9.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-icons@2.8.0&sourceBranch=refs/tags/@confirmit/react-icons@2.9.0&targetRepoId=1246) (2020-09-18)

### Bug Fixes

- add more tags to nodetype icons ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([4bcfb94](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/4bcfb94697b893b689e251df8d5bf25dc5a9f93c)

### Features

- **icons:** nodetype icon for Page has been updated to use `file` instead of `fileDocument` ([NPM-402](https://jiraosl.firmglobal.com/browse/NPM-402)) ([adbabd2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/adbabd29b04224acdba863ddeaa097d9779bdba2)

## v2.8.0

- Feat: Added format-code, format-heading, format-quote, format-strikethrough, format-table icons.

## v2.7.0

- Feat: Added pin icon.

## v2.6.0

- Feat: Added default-custom-question icon.

## [2.5.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-icons@2.5.1...@confirmit/react-icons@2.5.2) (2020-09-02)

### Bug Fixes

- typo in icon-meta, and make storybook work even if there is a typo ([NPM-354](https://jiraosl.firmglobal.com/browse/NPM-354)) ([87dd025](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/commits/87dd025f79e2d66d0eeb63ddf1d7098b671adb71))

## v2.5.1

- Feat: Added form-textbox-password icon.

## v2.5.0

- Feat: Added widgets icon.

## v2.4.0

- Feat: Added file-word icon.

## [2.3.4](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-icons@2.3.3...@confirmit/react-icons@2.3.4) (2020-08-16)

### Features

- Added bullseye-arrow and trending-down icons

## [2.3.3](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-icons@2.3.2...@confirmit/react-icons@2.3.3) (2020-08-16)

**Note:** Version bump only for package @confirmit/react-icons

## [2.3.2](http://stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages/compare/@confirmit/react-icons@2.3.1...@confirmit/react-icons@2.3.2) (2020-08-12)

**Note:** Version bump only for package @confirmit/react-icons

# CHANGELOG

## v2.3.1

- Fix: `Icon.size` is of `string | number | undefined` type.

## v2.3.0

- Feat: Typescript support.
- Feat: `Icon` is wrapped with forwardRef.
- Feat: Added `Icon.fill` property.
- Feat: Added `Icon` named export.

## v2.2.0

- Feat: new icon:
  - `directions` - this replaces `chevronDown` when used as `NodeType.Directive`
- Fix: compiler script will now check SVG path content for duplicates. This should reduce chances of adding duplicate icons to the package.

## v2.1.0

- Feat: new icons:
  - `cogClockwise`
  - `database`
  - `formatListText`

## v2.0.0

- **BREAKING**: rename `testWithSameRespondent` to `clipboardTextPlay`
- Feat: new icons:
  - `bookVariant`
  - `clipboardCheckMultiple`
  - `clipboardCheckMultipleOutline`
  - `clipboardTextPlayOutline`
  - `contacts`
  - `textBoxSearchOutline`
  - `basicHub`
  - `portalHub`

## v1.7.1

- Feat: new icon:
  - `drag-vertical`

## v1.7.0

- Feat: New icons:
  - `package-open`
  - `package-close`

### v1.6.0

- Feat: you can now pass EventHandler props to the `Icon` component

### v1.5.0

- Refactor: restructure folder names in repository.

## v1.4.1

- Feat: New icons:
  - `menuRight`

## v1.4.0

- Feat: New icons:
  - `star`
  - `testWithNewRespondent`
  - `testWithSameRespondent`
  - `pencilBoxMultiple`
  - `human`
  - `formatUnderline`
  - `formatBold`
  - `formatItalic`

## v1.3.0

- Feat: New icons:
  - `copied`
  - `folderOutline`
  - `folderPlusOutline`
  - `formatHorizontalAlignCenter`
  - `formatHorizontalAlignRight`
  - `formatHorizontalAlignLeft`
  - `image`
  - `link`
  - `monitor`
  - `scratchpad`
  - `secondarySurvey`
  - `upload`
- Feat: Add some more metadata for icons
- Fix: Compile script will output any duplicate entries in icon-meta to console, so its easier to see what went wrong in compilation.

## v1.2.0

- Feat: New icons:
  - `formatPaint`
  - `backupRestore`

## v1.1.2

- Refactor: Update path to import css variables

## v1.1.1

- Refactor: Improve validation message for invalid icons

## v1.1.0

- Feat: New icons:
  - `cellphoneLink`
  - `cellphone`
  - `clipboardText`
  - `surveyModeGenericMobile`
  - `surveyModeOfflineApp`
  - `surveyModeSdk`
  - `tabletAndroid`

## v1.0.12

- Feat: New icons:
  - `messageText`
  - `accountBox`
  - `accountClockOutline`
  - `accountConvert`
  - `alphaMBoxOutline`
  - `alphaVBoxOutline`
  - `briefcase`
  - `briefcaseAccount`
  - `filter`
  - `merge`
  - `sitemap`
- Storybook fixes: search on custom string enabled, moved search box to bottom of page to not cover icons, search result includes matches on actual name as well (not only tags and aliases).

## v1.0.11

- Feat: Added `fastForward10`, `rewind10`, `volumeOff`, `volumeDown`, and `volumeUp` icons

## v1.0.9

- Refactor: Use variable for default icon color

## v1.0.8

- Feat: Added `trashBin` icon

## v1.0.6

- Feat: Added `emailSend` and 'emailEdit' icons

## v1.0.4

- Feat: Added `chartTableCombo` icons

## v0.0.14

- Feat: Added `OpenInNew` and `PopIn` icons

## v0.0.13

- Feat: Added `Expression` Icon

## v0.0.11

- Feat: Added `SignText`, `CardTextOutline`, `TableSearch`, `Earth`, `FileDocumentBoxRemove`, `ChartAreaspline`, `Precent`, `TableLarge`, `CommentTextMultipleOutline`, `Gauge`, `ViewGrid`, `ChartLine`, `ChartBar`, `FormatTitle`, `ImageMultiple`, `Sigma`, `FormatListNumbered`, `FileDocumentBoxOutline`, `File` icon

## v0.0.10

- Feat: Added `CloseCircle` icon

## v0.0.9

- Fix: Change compile script again to try to stabilise compile. Problems getting a build green in TeamCity.

## v0.0.8

- Fix: Slightly change how compile is done and add some extra logs, to see if compilation step stabilises. There seems to have been issues where transpilation has started before compilation step is complete.

## v0.0.7

- Feat: Add `poll` `chevronDoubleLeft` icons

## v0.0.3

- Fix: Add default `fill` color to `Icon`, with the value of `rgba(18, 24, 33, 0.6)`. This is added as an SVG attribute, and is very weak so its easily overridable with css (can override even with just a element selector).

## v0.0.2

- Fix: `ClockBreak` icon was not exported correctly because of path directions.

## v0.0.1

- Initial version
