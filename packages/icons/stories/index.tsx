import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';
import camelCase from 'camelcase';
import {text, color} from '@storybook/addon-knobs';

import Icon, * as icons from '../src';
import {iconMeta} from '../src/icon-meta';
import {Select} from '../../select/src';
import {Dialog} from '../../dialog/src';
import {colors} from '../../global-styles/src';
import {
  InformationIcon,
  DigitalFeedbackIcon,
  SurveyIcon,
  ContactsIcon,
  ActionsIcon,
  TextAnalyticsIcon,
} from '../src';
import ReportIcon from '../src/components/ReportIcon';

const sortIgnoreCase = (a, b) => {
  const nameA = a.toUpperCase();
  const nameB = b.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  return 0;
};

/* eslint-disable-next-line */
const GridContainer = ({children}) => (
  <div
    style={{
      margin: '10px',
      display: 'grid',
      gridTemplateColumns: 'repeat( auto-fit, minmax(120px, auto) )',
      gridGap: '20px 10px',
    }}>
    {children}
  </div>
);

type IconDescriptor = {
  exportName: string;
  description: string;
  aliases: string[];
  tags: string[];
};

type Filter = {
  value: string;
};

/*eslint import/namespace: [2, { allowComputed: true }]*/
storiesOf('Components/icons', module)
  .addParameters({
    readme: {
      sidebar: [
        `Latest version: ${componentPackage.version}`,
        componentReadme,
        componentChangelog,
      ],
    },
  })
  .add('Icons', () => {
    const [currentFilter, setCurrentFilter] = React.useState<Filter[]>([]);
    const [currentIcon, setCurrentIcon] = React.useState<IconDescriptor | null>(
      null
    );
    const availableAliases = new Set<string>([]);
    const availableTags = new Set<string>([]);

    const mapped = iconMeta.map((meta) => {
      meta.tags?.forEach((tag) => {
        availableTags.add(tag);
      });
      meta.aliases?.forEach((alias) => {
        availableAliases.add(alias);
      });

      const exportName = camelCase(meta.fileName.replace('.svg', ''));
      availableAliases.add(exportName);

      return {
        ...meta,
        exportName,
      };
    });

    const filterOptions = [
      {
        label: 'Tags',
        options: Array.from(availableTags)
          .sort(sortIgnoreCase)
          .map((item) => ({label: item, value: item})),
      },
      {
        label: 'Aliases',
        options: Array.from(availableAliases)
          .sort(sortIgnoreCase)
          .map((item) => ({label: item, value: item})),
      },
    ];

    console.log({mapped});

    return (
      <div
        style={{
          display: 'grid',
          gridTemplateRows: '1fr auto',
          rowGap: '20px',
          padding: '20px',
          height: '100vh',
        }}>
        <div>
          <GridContainer>
            {mapped
              .filter((icon) =>
                currentFilter.length > 0
                  ? currentFilter.some(
                      (item) =>
                        icon.tags.includes(item.value) ||
                        icon.aliases.includes(item.value) ||
                        icon.exportName.includes(item.value)
                    )
                  : true
              )
              .sort((a, b) => sortIgnoreCase(a.exportName, b.exportName))
              .map((icon) => {
                return (
                  <button
                    onClick={() => setCurrentIcon(icon)}
                    key={icon.exportName}>
                    <Icon
                      path={icons[icon.exportName]}
                      size={text('size', '24')}
                      className={text('className', '')}
                      fill={color('fill', colors.TextSecondary)}
                    />
                    <div>{icon.exportName}</div>
                  </button>
                );
              })}
          </GridContainer>
        </div>

        <div
          style={{
            padding: '20px',
            position: 'sticky',
            bottom: '0',
            background: '#fff',
            marginLeft: '-20px',
            marginRight: '-20px',
            marginBottom: '-20px',
            boxShadow: '0 -1px 3px currentColor',
          }}>
          <Select
            value={currentFilter}
            label="Filter by name, tags and/or alias"
            onChange={setCurrentFilter}
            isCreatable={true}
            createPrefix="Any icon name, tag or alias includes: "
            isMulti={true}
            options={filterOptions}
            isClearable={true}
            helperText={
              'Filter will match any includes. Click on any icon above to view details about icon.'
            }
          />
        </div>

        {currentIcon && (
          <Dialog open={true} onHide={() => setCurrentIcon(null)}>
            <Dialog.Header>{currentIcon.exportName}</Dialog.Header>
            <Dialog.Body>
              <div
                style={{
                  display: 'grid',
                  gridAutoRows: 'auto',
                  gridGap: '30px',
                }}>
                <div
                  style={{
                    display: 'grid',
                    gridGap: '16px',
                    gridAutoFlow: 'column',
                    gridAutoColumns: 'min-content',
                  }}>
                  <div>
                    <h4>16</h4>
                    <Icon path={icons[currentIcon.exportName]} size={'16'} />
                  </div>
                  <div>
                    <h4>24</h4>
                    <Icon path={icons[currentIcon.exportName]} size={'24'} />
                  </div>
                  <div>
                    <h4>48</h4>
                    <Icon path={icons[currentIcon.exportName]} size={'48'} />
                  </div>
                  <div>
                    <h4>72</h4>
                    <Icon path={icons[currentIcon.exportName]} size={'72'} />
                  </div>
                </div>
                <div>
                  <h4>Usage:</h4>
                  <div>
                    <pre>
                      <code
                        dangerouslySetInnerHTML={{
                          __html: `import Icon, {${currentIcon.exportName}} from '@jotunheim/react-icons';

const SomeComponent = () => {
  &lt;Icon path={${currentIcon.exportName}} /&gt;
};`,
                        }}
                      />
                    </pre>
                  </div>
                </div>
                {currentIcon.description && (
                  <div>
                    <h4>Description:</h4>
                    <div style={{whiteSpace: 'break-spaces'}}>
                      {currentIcon.description}
                    </div>
                  </div>
                )}
                {currentIcon.aliases && currentIcon.aliases.length > 0 && (
                  <div>
                    <h4>Aliases:</h4>
                    <ul>
                      {currentIcon.aliases.sort(sortIgnoreCase).map((alias) => (
                        <li key={alias}>{alias}</li>
                      ))}
                    </ul>
                  </div>
                )}
                {currentIcon.tags && currentIcon.tags.length > 0 && (
                  <div>
                    <h4>Tags:</h4>
                    <ul>
                      {currentIcon.tags.sort(sortIgnoreCase).map((tag) => (
                        <li key={tag}>{tag}</li>
                      ))}
                    </ul>
                  </div>
                )}
              </div>
            </Dialog.Body>
          </Dialog>
        )}
      </div>
    );
  })
  .add('Color Coded Icons', () => {
    return (
      <div>
        <p>These icons are used in special circumstances</p>

        <h1>InformationIcon</h1>
        <p>
          This should be used when you need to display some info in a tooltip.
          It has a fixed size and color.
        </p>
        <InformationIcon />

        <h1>DigitalFeedbackIcon</h1>
        <p>
          This is used mainly in the "main app list" for Digital Feedback, and
          it has a couple different variants to distinguish the different types
          of Digital Feedback programs. The size can be changed with the `size`
          prop, as any other Icon.
        </p>

        <h2>With no variant:</h2>
        <DigitalFeedbackIcon />
        <h2>With WebProgram variant:</h2>
        <DigitalFeedbackIcon variant={DigitalFeedbackIcon.Variant.WebProgram} />
        <h2>With SdkProgram variant:</h2>
        <DigitalFeedbackIcon variant={DigitalFeedbackIcon.Variant.SdkProgram} />

        <h1>Survey Icon</h1>
        <p>This should be used as icon for different types of survey.</p>

        <h2>Survey (Default)</h2>
        <SurveyIcon />

        <h2>Combined Survey Icon:</h2>
        <SurveyIcon variant={SurveyIcon.Variant.ManualMappedSurvey} />

        <h2>Automaped Survey Icon</h2>
        <SurveyIcon variant={SurveyIcon.Variant.AutoMappedSurvey} size={24} />

        <h2>Forsta Survey Icon</h2>
        <SurveyIcon variant={SurveyIcon.Variant.ForstaSurvey} />

        <h1>Related Dataset Icons</h1>

        <h2>Actions Icon</h2>
        <p>
          This is used in Data Sources Panel in Studio as an icon for action
          management datasets
        </p>
        <ActionsIcon />

        <h2>Contacts Icon</h2>
        <p>
          This is used in Data Sources Panel in Studio as an icon for contacts
          datasets
        </p>
        <ContactsIcon />

        <h1>Automation Icons</h1>

        <h2>TextAnalytics Icon</h2>
        <p>
          This is used in Studio as an icon for text analytics datasets in
          several places including SourcePicker, Datasource Explorer and New
          Report Creation Wizard.
        </p>
        <TextAnalyticsIcon />

        <h1>Report Icons</h1>

        <p>This is used as icon for different types of report</p>

        <h2>Studio Report Icon</h2>
        <ReportIcon />

        <h2>Fieldwork Report Icon</h2>
        <ReportIcon variant={ReportIcon.Variant.FieldworkReport} />

        <h2>Program Report Icon</h2>
        <ReportIcon variant={ReportIcon.Variant.ProgramReport} />
      </div>
    );
  });
