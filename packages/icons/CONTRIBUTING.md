# Adding new icons to package

## Design rules for new icons

This package should be "compatible" with MaterialDesignIcons, and we use a lot of icons directly from there.

This means that all icons should be:

- Created on a 24\*24px grid, with a viewBox of "0 0 24 24"
- Monocolor
- Have a 2px padding (which means that the actual size of icons usually is 20\*20), however there are exceptions for this rule [See Google Material Design guidelines for details](https://material.io/design/iconography/#grid-keyline-shapes)
- Exported as 1 single path element

## Add new icons to package

All new icons should be approved by UX group.

- Get icon from UX
- Rename it to all-lowercase, and use dash (-) as space separators
- Move it to `src/svg`
- Add entry to `icon-meta.js`, following the same pattern that is there. Here is a short summary:
  - If UX provided a description for the icon, add it
  - If you got the icon from MDI package, please also add that name as an `alias`
  - If you know where the icon will be used, please tag it with the appropriate area (package, app, etc.)
  - If it is a "generic" type icon (like a clock variant), you should also add some generic tags (like `clock`), so its easy to see how many variants we have of similar icons.
  - Common tags are listed in a constant - and if you add a new common tag (package, app, or generic term like `clock`), please add it to the appropriate constant list
- Run `npm run compile`. If there are any errors, the compile will abort with an error message. Possible reasons for errors;
  - The icon does not have the correct viewBox (should be `0 0 24 24`)
  - The icon is exported incorrectly and contains multiple elements (it should only contain 1 `path` inside the `svg`)
  - There is a name error
    - name is using a reserved word (these are reserved words in JavaScript - the entire list can be seen in scripts/constants.js)
    - name does not start within the range [a-z]

## Beware of fill-rule:evenodd

This is usually something you only need to worry about when using custom icons.

When SVG's are exported from some applications, they sometimes apply some css or attributes to the SVG. These properties and styles are not included when they are exported through our code, since we only extract the `d` value of the `path` attribute. In some cases, the way the paths are generated in the design program might affect how "cutouts" behave.

This is unfortunately difficult (maybe impossible) to detect automatically, so it relies on that we detect this when adding new icons. We should do a manual verification that the icon looks correctly in the Storybook page. Looking at the SVG just in the PR is not enough, since there the source SVG is rendered.

In the following example, the icon has been exported incorrectly, which causes some of the cutouts to not be applied, because it relies on the fill-rule being set to a specific value:

![Incorrect exported](./docs/img/incorrect.png)

This is how it should look:

![Correct exported](./docs/img/correct.png)

To fix this, the icon needs to be modified and re-exported. Specifically you need to "reverse" the path direction of the paths that are not correctly cut out. The way to do this is a bit different depending on what program you use, but a quick google for "reverse svg path" + the app you are using usually gives some good hints.
