# Jotunheim React Icons

[Changelog](./CHANGELOG.md)

[Contribution guide](./CONTRIBUTING.md)

## Usage

The Icon wrapper supports a `size` prop (string - number (without unit will be treated as pixel size, but you can use regular css units as well)) which can be set to override the default 24 px size, and a `path` prop which should be the `d` value of a svg path. The MDI package, and our icons export only this path.

You can also add className, data-\*. aria-\*, and id props that will be attached to the SVG element. There is no div wrapper around the SVG element. The icons also have a default color of `rgba(18, 24, 33, 0.6)`.

Most icons that exist in the old icon packages will be added to this package as well. The name might not be the same, but old names should be added as aliases in this package.

See [Migration guide](./docs/migration-guide.md) for details on what to be aware of when changing from old icon packages to this package.

### JSX example

```jsx
import Icon, {report} from '@jotunheim/react-icons';

const myComponent = () => {
  return <Icon path={report} />;
};

// With optionally setting size and className
const mySizedComponent = () => {
  return <Icon path={report} size="16" className="my-classname" />;
};
```
