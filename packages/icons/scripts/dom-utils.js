const {JSDOM} = require('jsdom');
const {logAndExit} = require('./log-utils');

const hasCorrectViewBox = svg => {
  const dom = new JSDOM(svg);
  const svgElement = dom.window.document.querySelector('svg');

  if (!svgElement) {
    return false;
  }

  const viewBox = svgElement.getAttribute('viewBox');
  if (viewBox !== '0 0 24 24') {
    return false;
  }
  return true;
};

const hasOnlyOnePathElement = svg => {
  const dom = new JSDOM(svg);
  const pathElements = dom.window.document.querySelectorAll('path');

  if (!pathElements || pathElements.length !== 1) {
    return false;
  }
  return true;
};

const hasCorrectElementCount = svg => {
  const closingTags = (svg.match(/\/>/gim) || []).length;
  if (closingTags > 1) {
    return false;
  }
  return true;
};

const getSvgPath = (svg, fileName) => {
  if (
    !hasCorrectViewBox(svg) ||
    !hasOnlyOnePathElement(svg) ||
    !hasCorrectElementCount(svg)
  ) {
    logAndExit(`${fileName} is not valid.
It should
 - only contain 1 element inside the <svg>
 - that 1 element should be a <path> element with a "d" attribute
 - viewbox should be "0 0 24 24"

If there are any <g> elements, or tags with no content
(<rect width="24" height="24"/> is a common one), you can remove them and try again.

If there are other elements with content, you need to talk to the designer
and get them to merge the paths and re-export the icon.
`);
    return null;
  }

  const svgPath = svg.match(/ d="([^"]+)"/)[1];

  return svgPath;
};

module.exports = {
  getSvgPath,
};
