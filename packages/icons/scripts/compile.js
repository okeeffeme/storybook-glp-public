/* this script imports /src files which are written as es modules so convert them on the fly */
require('@babel/register')({
  extensions: ['.jsx', '.js', '.ts', '.tsx'],
  plugins: ['@babel/plugin-transform-modules-commonjs'],
});
const {createFileContent} = require('./string-utils');
const {
  writeFile,
  readPackageJson,
  processFiles,
  svgFilesCount,
  removeCompiledFiles,
  findDuplicateMetaEntries,
  findDuplicateIconPaths,
} = require('./file-utils');
const {logAndExit, log} = require('./log-utils');

const flattenNestedArray = array => {
  return array.reduce((acc, val) => acc.concat(val), []);
};

(async () => {
  log('removing old files');
  await removeCompiledFiles();

  const duplicateSvgPaths = await findDuplicateIconPaths();

  if (duplicateSvgPaths.size > 0) {
    logAndExit(
      `Duplicate SVG paths are found in: \n${Array.from(duplicateSvgPaths).join(
        '\n'
      )}\n\nThis means one or more of the icons probably already exist. Please add them as aliases instead.`
    );
  }

  const duplicateEntries = findDuplicateMetaEntries();

  if (duplicateEntries.length > 0) {
    logAndExit(
      `Duplicate entries in icon-meta was found: \n${duplicateEntries.join(
        '\n'
      )}`
    );
  }
  const metaData = await readPackageJson();
  const exportStatements = await (await processFiles()).filter(Boolean);
  const sourceCount = await svgFilesCount();

  if (sourceCount !== exportStatements.length) {
    logAndExit(
      `Export count (${exportStatements.length}) does not match svg source count (${sourceCount}) - check output for errors and try again`
    );
  }

  const text = createFileContent({
    version: metaData.version,
    name: metaData.name,
    content: flattenNestedArray(exportStatements).join('\n'),
  });

  log('creating ts file');
  await writeFile('index.ts', text);
})();
