const colors = require('colors/safe');

const {IS_LOGGING_ENABLED} = require('./constants');

const logAndExit = message => {
  if (IS_LOGGING_ENABLED) {
    /* eslint-disable */
    console.log(colors.red(message));
    process.exit(-1);
  }
};

const log = message => {
  if (IS_LOGGING_ENABLED) {
    /* eslint-disable */
    console.log(colors.cyan(message));
  }
};

module.exports = {
  logAndExit,
  log,
};
