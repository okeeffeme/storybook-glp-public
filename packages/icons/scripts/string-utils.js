const camelCase = require('camelcase');
const path = require('path');
const fs = require('fs');

const {logAndExit} = require('./log-utils');
const {getSvgPath} = require('./dom-utils');
const {RESERVED_WORDS} = require('./constants');

const createIconName = (fileName) => {
  const name = fileName.trim().replace('.svg', '');

  const validFilenameStart = /^[a-z]/gim;

  const camelCasedName = camelCase(name);

  const reservedWord = RESERVED_WORDS.find((x) => x === camelCasedName);

  if (!validFilenameStart.test(name) || reservedWord) {
    let msg = `'${name}' is not a valid name.
The filename has to start within the range [a-z]`;
    if (reservedWord) {
      msg += ` and it cannot use the reserved word '${reservedWord}'.`;
    }
    logAndExit(msg);
    return null;
  }

  return camelCase(name);
};

const getBaseExportContent = () => {
  return fs.readFileSync(path.join('src', 'BaseExports.ts'), 'utf8');
};

const createFileContent = ({version, name, content}) => {
  return `/* eslint-disable */
/**
 * ${name} version ${version}
 *
 * This output is auto-generated from ${
   createFileContent.name
 } function in ${path.basename(__filename)}. Please do not edit manually.
 */

${content}

${getBaseExportContent()}
`;
};

const createExportStatement = ({svgFile, fileName}) => {
  const iconName = createIconName(fileName);
  const svgPath = getSvgPath(svgFile, fileName);
  if (!svgPath || !iconName) {
    return;
  }

  return `export const ${iconName}: string = "${svgPath}";`;
};

module.exports = {
  camelCase,
  createIconName,
  createFileContent,
  createExportStatement,
};
