const fs = require('fs').promises;
const path = require('path');

const {createExportStatement} = require('./string-utils');
const {SVG_ROOT_PATH} = require('./constants');
const {logAndExit} = require('./log-utils');
const {getSvgPath} = require('./dom-utils');

const {iconMeta} = require('../src/icon-meta');

const handleRemoveFileError = error => {
  // If remove file errors because file doesnt exist, ignore it
  if (error.code !== 'ENOENT') {
    logAndExit(error);
  }
};

const getFileContent = async path => {
  return await fs.readFile(path, {
    encoding: 'utf8',
  });
};

const svgFilesCount = async () => {
  const files = await fs.readdir(SVG_ROOT_PATH);
  return files.length;
};

const writeFile = async (filename, content) => {
  await fs.writeFile(path.join(__dirname, `../src/${filename}`), content);
};

const removeCompiledFiles = async () => {
  try {
    await fs.unlink(path.join(__dirname, `../src/index.js`));
  } catch (error) {
    handleRemoveFileError(error);
  }
  try {
    await fs.unlink(path.join(__dirname, `../src/index.ts`));
  } catch (error) {
    handleRemoveFileError(error);
  }
};

const readPackageJson = async () =>
  JSON.parse(await getFileContent(path.join(__dirname, '../package.json')));

const processFiles = async () => {
  return Promise.all(
    iconMeta.map(async icon => {
      try {
        const svgFile = await getFileContent(
          path.join(SVG_ROOT_PATH, icon.fileName)
        );
        const exportStatement = createExportStatement({
          svgFile,
          fileName: icon.fileName,
        });
        return exportStatement;
      } catch (error) {
        logAndExit(`Error processing file ${icon.fileName}:
${error}
`);
      }
    })
  );
};

const findDuplicateMetaEntries = () => {
  let uniqueEntries = new Set();

  const duplicates = iconMeta.reduce((accumulator, current) => {
    const currentFileName = current.fileName;

    if (uniqueEntries.has(currentFileName)) {
      accumulator.push(currentFileName);
    } else {
      uniqueEntries.add(currentFileName);
    }

    return accumulator;
  }, []);

  return duplicates;
};

const findDuplicateIconPaths = async () => {
  let uniqueEntries = new Map();
  let dupeNames = new Set();

  const files = await fs.readdir(SVG_ROOT_PATH);

  await Promise.all(
    files.map(async current => {
      const currentFileName = current;

      const svgFile = await getFileContent(
        path.join(SVG_ROOT_PATH, currentFileName)
      );
      const svgPath = getSvgPath(svgFile, currentFileName);

      if (uniqueEntries.has(svgPath)) {
        dupeNames.add(currentFileName);
        dupeNames.add(uniqueEntries.get(svgPath));
      } else {
        uniqueEntries.set(svgPath, currentFileName);
      }
    })
  );

  return dupeNames;
};

module.exports = {
  readPackageJson,
  getFileContent,
  writeFile,
  processFiles,
  svgFilesCount,
  removeCompiledFiles,
  findDuplicateMetaEntries,
  findDuplicateIconPaths,
};
