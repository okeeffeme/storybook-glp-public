# Why do we need another icon package?

## TL:DR;

Consistency and better PR review when adding new icons, and easily extend icons with overlays.

## Longer version

This is a replacement for our internal icon packages `confirmit-icons` and `confirmit-icons-material`. This package will only contain icons that follow a strict design guide, and some React wrappers for rendering these in different variations. The base wrapper has no styles applied, and it renders an SVG directly instead of a div wrapping an SVG which was the case with `confirmit-icons` package. Other wrappers may have some internal styling applied.

### Design guidelines

Our icons are created with the same guidelines that the MaterialDesignIcons project uses (icons are created on a 24\*24px grid, with a 2px gutter around), and our icons are eported in a similar way as the MDI JS/TS package.

### Old icon packages

Unfortunately none of the icons in `confirmit-icons` package were created with any system in mind, so icons from there are different sizes and aspect ratios. Keep that in mind when transitioning to this package, to make sure everything looks good.

Things are slightly better for `confirmit-icons-material`, as most icons here are created on 24\*24px grid. Some of them however do not have a 2px gutter around the icon and others do. Some icons also seem to have been created on a 48\*48 px grid instead of 24\*24.

There are a few "icons" in our old packages that are not really considered icons (not square, multicolored, etc.), and these have been removed. An alternative for these will be created later.
