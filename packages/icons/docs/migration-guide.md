# Migration guide to @confirmit/react-icons

You can also read [Why do we need another icon package?](./why.md) to see some reasons to deprecate the current packages in favor of this one.

## From confirmit-icons-material package

Alternative icons for these icons are tagged with `confirmit-icons-material`, and has the old icon name as an alias.

The new `Icon` component has been simplified a lot, and leaves the most up to the consumer.

The following props are not supported:

- lighter
- link
- flip
- turn
- iconId
- viewBox
- iconSubclass
- iconTitle
- classNames
- children
- baseClassName

And the following properties have changed:

- size -> used to be an object with width and height properties, but is now a string. This can be used as a unitless number (will be interpreted as px values), or you can add a number followed by a valid css unit (f.ex 1em).

Some icons also had support for extra props, but these also are not supported. This means that anything custom you need, except rendering an icon, you need to provide yourself. If there is a common need to style icon X in a certain way, raise this with other devs and UX so we can consider if we should create a separate component for it or some sort of abstraction.

Most of the old icons should be a drop-in replacement, but some are created with a slightly different grid or size. **For this reason it is important that you manually review and verify that the usages still look ok after you change to the new package**.

## From confirmit-icons package

Alternative icons for these icons are tagged with `confirmit-icons`, and has the old icon name as an alias (including the "namespace", like "Common" or "Forms").

Icons without a clear alternative:

- Common.DateTime
- Common.Label
- Common.TrashCan
- Common.Table
- Common.GuideArrow
- Common.ContactCard
- Common.CheckedBubble
- Common.RoundedX
- SourceTypes.\* (all source type icons)

If you are using any of these in your app, ask your UX person what to do. Maybe we need to find a simliar icon to use, or rethink the UX.

Since there was no standard size for these icons, you will most likely need to adjust local styles when you update these icons. **For this reason it is important that you manually review and verify that the usages still look ok after you change to the new package**.
