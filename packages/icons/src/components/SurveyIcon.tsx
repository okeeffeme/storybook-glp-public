import React, {forwardRef, Ref} from 'react';
import {colors} from '@jotunheim/global-styles';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import Icon, {
  clipboardCheckMultiple,
  clipboardCheckMultipleOutline,
  clipboardExternal,
  surveyIcon,
} from '../index';

enum Variant {
  AutoMappedSurvey = 'AutoMappedSurvey',
  ManualMappedSurvey = 'CombinedSurvey',
  ForstaSurvey = 'ForstaSurvey',
  CombinedSurvey = 'ComdinedSurvey',
  Survey = 'Survey',
}

type SurveyIconType = {
  size?: string | number;
  variant?: Variant;
};

type ForwardRefSurveyIcon = React.ForwardRefExoticComponent<
  SurveyIconType & React.RefAttributes<HTMLElement>
> & {
  Variant: typeof Variant;
};

const getIcon = ({iconVariant}): {path: string; color: string} => {
  const defaultVariant = {path: surveyIcon, color: colors.Product06Turquoise};

  const icon = {
    [Variant.AutoMappedSurvey]: {
      path: clipboardCheckMultiple,
      color: colors.Product03GreenLight,
    },
    [Variant.CombinedSurvey]: {
      path: clipboardCheckMultipleOutline,
      color: colors.Product03GreenLight,
    },
    [Variant.ManualMappedSurvey]: {
      path: clipboardCheckMultipleOutline,
      color: colors.Product03GreenLight,
    },
    [Variant.ForstaSurvey]: {
      path: clipboardExternal,
      color: colors.Product05TurquoiseDark,
    },
  };

  return icon[iconVariant] || defaultVariant;
};

const SurveyIcon: ForwardRefSurveyIcon = forwardRef(
  ({size, variant, ...rest}: SurveyIconType, ref: Ref<HTMLElement>) => {
    const {path, color} = getIcon({iconVariant: variant});

    return (
      <Icon
        path={path}
        size={size}
        fill={color}
        ref={ref}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}
      />
    );
  }
) as ForwardRefSurveyIcon;

SurveyIcon.displayName = 'SurveyIcon';
SurveyIcon.Variant = Variant;

export default SurveyIcon;
