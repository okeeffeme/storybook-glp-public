import React, {Ref} from 'react';
import {colors} from '@jotunheim/global-styles';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import Icon, {actions, contacts, graphOutline} from '../index';

type RelatedDatasetIconType = {
  size?: string | number;
};

type ForwardRefRelatedDatasetIcon = React.ForwardRefExoticComponent<
  RelatedDatasetIconType & React.RefAttributes<HTMLElement>
>;

type ForwardRefRenderFC = React.ForwardRefRenderFunction<
  HTMLElement,
  RelatedDatasetIconType
>;

type Props = {
  displayName: string;
  getIcon: () => {path: string; fill: string};
};

const forwardRef = (
  component: ForwardRefRenderFC
): ForwardRefRelatedDatasetIcon => {
  const componentRef = React.forwardRef<HTMLElement, RelatedDatasetIconType>(
    component
  );
  componentRef.displayName = component.displayName;
  return componentRef;
};

const renderIcon = ({displayName, getIcon}: Props): ForwardRefRenderFC => {
  const RelatedDatasetIcon = (
    {size, ...rest}: RelatedDatasetIconType,
    ref: Ref<HTMLElement>
  ) => {
    const {path, fill} = getIcon();
    return (
      <Icon
        path={path}
        fill={fill}
        size={size}
        ref={ref}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}
      />
    );
  };
  RelatedDatasetIcon.displayName = displayName;
  return RelatedDatasetIcon;
};

export const ActionsIcon: ForwardRefRelatedDatasetIcon = forwardRef(
  renderIcon({
    displayName: 'ActionsIcon',
    getIcon: () => ({path: actions, fill: colors.Product14Orange}),
  })
);

export const ContactsIcon: ForwardRefRelatedDatasetIcon = forwardRef(
  renderIcon({
    displayName: 'ContactsIcon',
    getIcon: () => ({path: contacts, fill: colors.Product04TurquoiseDark}),
  })
);

export const TextAnalyticsIcon: ForwardRefRelatedDatasetIcon = forwardRef(
  renderIcon({
    displayName: 'TextAnalyticsIcon',
    getIcon: () => ({path: graphOutline, fill: colors.Product01GreenDark}),
  })
);
