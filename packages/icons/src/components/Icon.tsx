import React, {forwardRef, Ref} from 'react';

import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';
import {colors} from '@jotunheim/global-styles';

type IconProps = {
  path: string;
  className?: string;
  size?: string | number;
  fill?: string;
};

export const Icon = forwardRef(function Icon(
  props: IconProps,
  ref: Ref<HTMLElement | null>
) {
  const {
    path,
    className,
    size = '24',
    fill = colors.TextSecondary,
    ...rest
  } = props;

  return (
    <svg
      data-testid="icon"
      ref={ref}
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      className={className}
      fill={fill}
      {...extractOnEventProps(rest)}
      {...extractDataAriaIdProps(rest)}>
      <path d={path} />
    </svg>
  );
});

Icon.displayName = 'Icon';

export default Icon;
