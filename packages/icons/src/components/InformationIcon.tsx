import React, {forwardRef, Ref} from 'react';
import {colors} from '@jotunheim/global-styles';

import Icon, {information} from '../index';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';

const InformationIcon = forwardRef((props, ref: Ref<HTMLElement>) => (
  <Icon
    path={information}
    size={16}
    fill={colors.TextDisabled}
    ref={ref}
    {...extractOnEventProps(props)}
    {...extractDataAriaIdProps(props)}
  />
));

InformationIcon.displayName = 'InformationIcon';

export default InformationIcon;
