import React, {forwardRef, Ref} from 'react';
import {colors} from '@jotunheim/global-styles';

import Icon, {digitalFeedback} from '../index';
import {
  extractDataAriaIdProps,
  extractOnEventProps,
} from '@jotunheim/react-utils';

enum Variant {
  WebProgram = 'web-program',
  SdkProgram = 'sdk-program',
}

type DigitalFeedbackIconType = {
  size?: string | number;
  variant?: Variant;
};

const getColor = ({variant}) => {
  switch (variant) {
    case Variant.WebProgram:
      return colors.Product05TurquoiseDark;
    case Variant.SdkProgram:
      return colors.Product03GreenLight;
  }
  return null;
};

type ForwardRefDigitalFeedbackIcon = React.ForwardRefExoticComponent<
  DigitalFeedbackIconType & React.RefAttributes<HTMLElement>
> & {
  Variant: typeof Variant;
};

const DigitalFeedbackIcon: ForwardRefDigitalFeedbackIcon = forwardRef(
  (
    {variant, size, ...rest}: DigitalFeedbackIconType,
    ref: Ref<HTMLElement>
  ) => {
    const color = getColor({variant});

    return (
      <Icon
        path={digitalFeedback}
        size={size}
        fill={color}
        ref={ref}
        {...extractOnEventProps(rest)}
        {...extractDataAriaIdProps(rest)}
      />
    );
  }
) as ForwardRefDigitalFeedbackIcon;

DigitalFeedbackIcon.displayName = 'DigitalFeedbackIcon';
DigitalFeedbackIcon.Variant = Variant;

export default DigitalFeedbackIcon;
