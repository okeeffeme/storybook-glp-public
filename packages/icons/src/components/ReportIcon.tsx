import {colors} from '@jotunheim/global-styles';
import React, {forwardRef, Ref} from 'react';
import Icon, {chartBox} from '../index';

enum Variant {
  ReportDashboard = 'report-dashboard',
  FieldworkReport = 'fieldwork-report',
  ProgramReport = 'program-report',
}

type ReportIconType = {
  size?: number;
  variant?: Variant;
};

const getColor = ({variant}) => {
  switch (variant) {
    case Variant.ReportDashboard:
      return colors.PrimaryAccent;
    case Variant.FieldworkReport:
      return colors.Product09Purple;
    case Variant.ProgramReport:
      return colors.Product13Orange;
  }
  return colors.PrimaryAccent;
};

type ForwardRefReportIcon = React.ForwardRefExoticComponent<
  ReportIconType & React.RefAttributes<HTMLElement>
> & {
  Variant: typeof Variant;
};

const ReportIcon: ForwardRefReportIcon = forwardRef(
  ({variant, size}: ReportIconType, ref: Ref<HTMLElement>) => {
    const color = getColor({variant});
    return <Icon path={chartBox} size={size} fill={color} ref={ref} />;
  }
) as ForwardRefReportIcon;

ReportIcon.Variant = Variant;

export default ReportIcon;
