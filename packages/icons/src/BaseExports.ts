import Icon from './components/Icon';
import InformationIcon from './components/InformationIcon';
import DigitalFeedbackIcon from './components/DigitalFeedbackIcon';
import SurveyIcon from './components/SurveyIcon';
import ReportIcon from './components/ReportIcon';
import {
  ActionsIcon,
  ContactsIcon,
  TextAnalyticsIcon,
} from './components/RelatedDatasetIcons';

export {
  Icon,
  InformationIcon,
  DigitalFeedbackIcon,
  SurveyIcon,
  ContactsIcon,
  ActionsIcon,
  TextAnalyticsIcon,
  ReportIcon,
};
export default Icon;
