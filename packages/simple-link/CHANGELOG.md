# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-link@2.0.3&sourceBranch=refs/tags/@jotunheim/react-simple-link@2.0.4&targetRepoId=1246) (2023-03-30)

**Note:** Version bump only for package @jotunheim/react-simple-link

## [2.0.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-link@2.0.2&sourceBranch=refs/tags/@jotunheim/react-simple-link@2.0.3&targetRepoId=1246) (2022-10-13)

**Note:** Version bump only for package @jotunheim/react-simple-link

## [2.0.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-link@2.0.1&sourceBranch=refs/tags/@jotunheim/react-simple-link@2.0.2&targetRepoId=1246) (2022-10-11)

**Note:** Version bump only for package @jotunheim/react-simple-link

## [2.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@jotunheim/react-simple-link@2.0.0&sourceBranch=refs/tags/@jotunheim/react-simple-link@2.0.1&targetRepoId=1246) (2022-06-30)

**Note:** Version bump only for package @jotunheim/react-simple-link

# 2.0.0 (2022-06-29)

- Move design system components to "jotunheim" scope ([NPM-1034](https://jiraosl.firmglobal.com/browse/NPM-1034)) ([1376f77](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1376f77dc76d5d36940bd8889f91f00da7514d97))

## [1.0.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@1.0.0&sourceBranch=refs/tags/@confirmit/react-simple-link@1.0.1&targetRepoId=1246) (2022-05-23)

### Bug Fixes

- Accept React 18 in peer dependencies ([NPM-995](https://jiraosl.firmglobal.com/browse/NPM-995)) ([9e2fac5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/9e2fac5a871fe515e30d78a9d623605fa8de725a))

# [1.0.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.5&sourceBranch=refs/tags/@confirmit/react-simple-link@1.0.0&targetRepoId=1246) (2022-04-21)

### BREAKING CHANGES

- Release version 1.0.0 ([STUD-3322](https://jiraosl.firmglobal.com/browse/STUD-3322))

## [0.2.5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.4&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.5&targetRepoId=1246) (2022-04-06)

**Note:** Version bump only for package @confirmit/react-simple-link

## [0.2.4](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.3&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.4&targetRepoId=1246) (2021-11-02)

### Bug Fixes

- update @confirmit/react-contexts peer dependency version to 0.x || 1.x ([AM-7117](https://jiraosl.firmglobal.com/browse/AM-7117)) ([7411e26](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/7411e26dc8c8c4647b4052bbbd1cc2b2b59eb7e4))

## [0.2.3](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.2&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.3&targetRepoId=1246) (2021-09-16)

**Note:** Version bump only for package @confirmit/react-simple-link

## [0.2.2](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.1&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.2&targetRepoId=1246) (2021-04-12)

### Bug Fixes

- accept React 17 in peerDeps ([NPM-757](https://jiraosl.firmglobal.com/browse/NPM-757)) ([a207842](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/a2078420259abc27ce7c349125bdb29efdabd51e))

## [0.2.1](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.2.0&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.1&targetRepoId=1246) (2020-12-14)

### Bug Fixes

- should not change url for non-relative links ([NPM-656](https://jiraosl.firmglobal.com/browse/NPM-656)) ([2af39f5](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/2af39f5c9dba0858c4b49e56698446ef17355e53))

# [0.2.0](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/compare/diff?targetBranch=refs/tags/@confirmit/react-simple-link@0.1.0&sourceBranch=refs/tags/@confirmit/react-simple-link@0.2.0&targetRepoId=1246) (2020-12-11)

### Bug Fixes

- make sure correct url is pushed to history. support # href. add test coverage. ([NPM-652](https://jiraosl.firmglobal.com/browse/NPM-652)) ([dd1dc88](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/dd1dc8870be4361e623b8ead03f77d25fa6c15c2))

### Features

- Support basename for simple link ([NPM-652](https://jiraosl.firmglobal.com/browse/NPM-652)) ([fb7fcda](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/fb7fcda552fa78db12efc836450f68990a41775c))

# 0.1.0 (2020-11-11)

### Features

- add @confirmit/simple-link package ([NPM-599](https://jiraosl.firmglobal.com/browse/NPM-599)) ([1403a5a](https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/commits/1403a5ae9178c714f117e516294bcf5441290b78))

## CHANGELOG

### v0.0.1

- Initial version
