import parseUrl from '../../src/utls/parseUrl';

describe('parseUrl', () => {
  it('should parse empty url', () => {
    const location = parseUrl('');

    expect(location).toEqual({
      pathname: '',
      search: '',
      hash: '',
    });
  });

  it('should parse url without search and hash', () => {
    const location = parseUrl('/some/url');

    expect(location).toEqual({
      pathname: '/some/url',
      search: '',
      hash: '',
    });
  });

  it('should parse url with search', () => {
    const location = parseUrl('/some/url?query=abc');

    expect(location).toEqual({
      pathname: '/some/url',
      search: '?query=abc',
      hash: '',
    });
  });

  it('should parse url with hash', () => {
    const location = parseUrl('/some/url#q=30');

    expect(location).toEqual({
      pathname: '/some/url',
      search: '',
      hash: '#q=30',
    });
  });

  it('should parse url with search and hash', () => {
    const location = parseUrl('/some/url?some=a23#another=33');

    expect(location).toEqual({
      pathname: '/some/url',
      search: '?some=a23',
      hash: '#another=33',
    });
  });

  it('should parse url with hash and search as part of hash', () => {
    const location = parseUrl('/some/url#some=a23?another=33');

    expect(location).toEqual({
      pathname: '/some/url',
      search: '',
      hash: '#some=a23?another=33',
    });
  });
});
