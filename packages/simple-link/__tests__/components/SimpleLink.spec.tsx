import React from 'react';
import SimpleLink from '../../src/components/SimpleLink';
import {fireEvent, render as renderRTL, screen} from '@testing-library/react';
import {HistoryContext, Location} from '../../../contexts/src';

function createHref(location: Location) {
  return '/base' + location.pathname;
}

describe('SimpleLink', () => {
  it('should use history if href is relative and left click', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});
    expect(history.push).toBeCalledWith({
      pathname: '/relative/link',
      search: '',
      hash: '',
    });
  });

  it('should use history if target is _self, href is relative and left click', () => {
    const {history, preventDefault} = render({
      target: '_self',
      href: '/relative/link',
    });
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});
    expect(history.push).toBeCalledWith({
      pathname: '/relative/link',
      search: '',
      hash: '',
    });
  });

  it('should not use history if target is not _self', () => {
    const {history, preventDefault} = render({
      target: '_blank',
      href: '/relative/link',
    });
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if href is absolute', () => {
    const {history, preventDefault} = render({href: 'confirmit.com'});
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if not left click', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 2});

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if altKey is pressed', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {
      preventDefault,
      button: 0,
      altKey: true,
    });

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if ctrlKey is pressed', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {
      preventDefault,
      button: 0,
      ctrlKey: true,
    });

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if metaKey is pressed', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {
      preventDefault,
      button: 0,
      metaKey: true,
    });

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if shiftKey is pressed', () => {
    const {history, preventDefault} = render({href: '/relative/link'});
    fireEvent.click(screen.getByRole('link'), {
      preventDefault,
      button: 0,
      shiftKey: true,
    });

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if href is empty', () => {
    const {history, preventDefault} = render({href: ''});
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if default is prevented', () => {
    const {history, preventDefault} = render({href: ''});
    fireEvent.click(screen.getByRole('link'), {
      preventDefault,
      button: 0,
      defaultPrevented: true,
    });

    expect(preventDefault).not.toBeCalled();
    expect(history.push).not.toBeCalled();
  });

  it('should not use history if history is not provided', () => {
    const preventDefault = jest.fn();
    renderRTL(<SimpleLink href="/relative/link">Follow me</SimpleLink>);
    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});
    expect(preventDefault).not.toBeCalled();
  });

  it('should set correct href if createHref method on history is specified', () => {
    const {history, preventDefault} = render({
      href: '/relative/link',
      createHref,
    });

    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});
    expect(screen.getByRole('link')).toHaveAttribute(
      'href',
      `/base/relative/link`
    );
    expect(history.push).toBeCalledWith({
      pathname: '/relative/link',
      search: '',
      hash: '',
    });
  });

  it('should set correct href if createHref method on history is not specified', () => {
    const {history, preventDefault} = render({
      href: '/base/relative/link',
    });

    fireEvent.click(screen.getByRole('link'), {preventDefault, button: 0});
    expect(screen.getByRole('link')).toHaveAttribute(
      'href',
      `/base/relative/link`
    );
    expect(history.push).toBeCalledWith({
      pathname: '/base/relative/link',
      search: '',
      hash: '',
    });
  });

  it('should render without href if not specified', () => {
    render({
      href: undefined,
    });
    expect(screen.getByText('Follow me')).not.toHaveAttribute('href');
  });

  it('should render without href if not specified when createHref is specified', () => {
    render({
      href: undefined,
      createHref,
    });

    expect(screen.getByText('Follow me')).not.toHaveAttribute('href');
  });

  it('should render with href as # when createHref is specified', () => {
    render({
      href: '#',
      createHref,
    });
    expect(screen.getByRole('link')).toHaveAttribute('href', '#');
  });

  it('should render with href as # when createHref is not specified', () => {
    render({
      href: '#',
    });
    expect(screen.getByRole('link')).toHaveAttribute('href', '#');
  });

  it('should not adjust url when its not a relative url', () => {
    render({
      href: 'http://app.confirmit.com/',
      createHref,
    });

    expect(screen.getByRole('link')).toHaveAttribute(
      'href',
      'http://app.confirmit.com/'
    );
  });
});

const render = ({
  href,
  target = undefined,
  createHref,
}: {
  href?: string;
  target?: string;
  createHref?: (location: Location) => string;
} = {}) => {
  const history = {
    push: jest.fn(),
    createHref,
  };

  const preventDefault = jest.fn();
  renderRTL(
    <HistoryContext.Provider value={history}>
      <SimpleLink href={href} target={target}>
        Follow me
      </SimpleLink>
    </HistoryContext.Provider>
  );

  return {
    history,
    preventDefault,
  };
};
