import {Location} from '@jotunheim/react-contexts';

export default function parseUrl(url: string): Location {
  /* if search and/or hash are undefined than search and/or hash of current location is used, so we intentionally
   * set it to empty string */
  const location: Location = {
    pathname: '',
    search: '',
    hash: '',
  };

  if (url) {
    const hashIndex = url.indexOf('#');
    if (hashIndex >= 0) {
      location.hash = url.substr(hashIndex);
      url = url.substr(0, hashIndex);
    }

    const searchIndex = url.indexOf('?');
    if (searchIndex >= 0) {
      location.search = url.substr(searchIndex);
      url = url.substr(0, searchIndex);
    }

    if (url) {
      location.pathname = url;
    }
  }

  return location;
}
