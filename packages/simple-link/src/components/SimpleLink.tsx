import React, {
  AnchorHTMLAttributes,
  DetailedHTMLProps,
  forwardRef,
  Ref,
  useContext,
} from 'react';
import {HistoryContext} from '@jotunheim/react-contexts';
import parseUrl from '../utls/parseUrl';

type SimpleLinkProps = DetailedHTMLProps<
  AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
>;

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

export const SimpleLink = forwardRef(function SimpleLink(
  props: SimpleLinkProps,
  ref: Ref<HTMLAnchorElement>
) {
  const history = useContext(HistoryContext);
  const isRelativeLink = props.href?.[0] === '/';

  const handleClick = (e) => {
    props.onClick?.(e);
    const isLeftClick = e.button === 0;
    const isSelfTarget = !props.target || props.target === '_self';
    if (
      history &&
      props.href &&
      isRelativeLink &&
      !e.defaultPrevented &&
      isLeftClick &&
      isSelfTarget &&
      !isModifiedEvent(e)
    ) {
      e.preventDefault();

      history.push(parseUrl(props.href));
    }
  };

  let href = props.href;
  if (history && href && isRelativeLink) {
    href = history.createHref?.(parseUrl(href)) ?? href;
  }

  return <a ref={ref} {...props} href={href} onClick={handleClick} />;
});

export default SimpleLink;
