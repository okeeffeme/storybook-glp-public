// This script is useful to create local packages and use it in particular app to test changes.

const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const glob = require('glob');
const argv = require('minimist')(process.argv.slice(2));
const {echo, exec} = require('shelljs');

const pkg = argv.pkg ? argv.pkg : '*';

(async function() {
  const folders = glob.sync(path.resolve(__dirname, `../packages/${pkg}`));
  const packages = folders
    .map(folder => {
      const packageJson = path.resolve(__dirname, `${folder}/package.json`);
      if (fs.existsSync(packageJson)) {
        return {
          packageFolder: folder,
          ...JSON.parse(fs.readFileSync(packageJson, 'utf8')),
        };
      }
    })
    .filter(Boolean);

  const artifacts = packages.map(pkg => ({
    folder: pkg.packageFolder,
    packageName: pkg.name,
  }));

  if (artifacts.length) {
    echo(
      chalk.cyan(
        `These packages will be packed : \n\r${artifacts
          .map(x => x.packageName)
          .join('\n\r')}`
      )
    );
    const pathsToPack = artifacts.map(
      ({folder}) => `./packages/${path.basename(folder)}`
    );
    exec(`npm pack ${pathsToPack.join(' ')}`);
  }
})();
