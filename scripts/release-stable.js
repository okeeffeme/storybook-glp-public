const {execFileSync} = require('child_process');
const minimist = require('minimist');

const args = minimist(process.argv.slice(2));
const useConventionalCommits = args['c'];

const preCheck = require('./pre-check');

preCheck('stable');

const getLernaParameters = () => {
  const parameters = [
    'version',
    '--include-merged-tags',
    '--exact',
    '--no-commit-hooks',
  ];

  if (useConventionalCommits) {
    parameters.push('--conventional-commits');
    parameters.push('--conventional-graduate');
  }

  return parameters;
};

const lernaParameters = getLernaParameters();

const lernaCommand = process.platform == 'win32' ? 'lerna.cmd' : 'lerna';
execFileSync(lernaCommand, lernaParameters, {stdio: 'inherit'});
