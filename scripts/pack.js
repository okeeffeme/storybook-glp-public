const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const glob = require('glob');
const request = require('request-promise');
const {echo, exec} = require('shelljs');

const [, , userName, password] = process.argv.slice();
if (!userName || !password) {
  throw new Error('userName or password were not passed');
}

const getOptions = name => ({
  uri: `http://artifactory.firmglobal.com/artifactory/api/search/artifact?repos=npm-confirmit-local&name=${name}`,
  headers: {
    Authorization:
      'Basic ' + new Buffer(userName + ':' + password).toString('base64'),
  },
  json: true,
});

(async function() {
  const folders = glob.sync(`${path.resolve(__dirname, '../packages/*')}`);
  const packages = folders
    .map(folder => path.resolve(__dirname, `${folder}/package.json`))
    .map(x => JSON.parse(fs.readFileSync(x, 'utf8')));

  const artifacts = packages.map(x => ({
    name: x.name,
    archiveName: `${x.name}-${x.version}.tgz`,
  }));

  const requestResults = await Promise.all(
    artifacts.map(x => request(getOptions(x.archiveName)))
  );
  const results = requestResults.map((x, i) => ({
    folder: folders[i],
    packageName: artifacts[i].name,
    willBePacked: x.results.length === 0,
  }));

  const packagesToSkip = results
    .filter(x => !x.willBePacked)
    .map(x => x.packageName);
  packagesToSkip.length &&
    echo(
      chalk.yellow(
        `Packing of these packages is not needed :\n\r${packagesToSkip.join(
          '\n\r'
        )}`
      )
    );

  const packagesToPack = results.filter(x => x.willBePacked);
  if (packagesToPack.length) {
    echo(
      chalk.cyan(
        `These packages will be packed : \n\r${packagesToPack
          .map(x => x.packageName)
          .join('\n\r')}`
      )
    );
    const pathsToPack = packagesToPack.map(
      ({folder}) => `./packages/${path.basename(folder)}`
    );
    exec(`npm pack ${pathsToPack.join(' ')}`);
  }
})();
