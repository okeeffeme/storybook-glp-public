const fs = require('fs');
const path = require('path');
const {PackagesRootFolder, Folders} = require('./file-template-utils');

const createFolderStructure = ({packageRootFolderName}) => {
  for (const folder in Folders) {
    fs.mkdirSync(
      `./${PackagesRootFolder}/${packageRootFolderName}/${Folders[folder]}`
    );
  }
};

const packageJson = ({
  user,
  email,
  packageTitle,
  packageName,
  packageDescription,
  packageRootFolderName,
  deps = {},
  devDeps = {},
  peerDeps = {},
}) => {
  const config = {
    name: `@confirmit/${packageName}`,
    title: `${packageTitle}`,
    version: '1.0.0',
    description: `${packageDescription}`,
    main: './dist/cjs/index.js',
    module: './dist/esm/index.js',
    sideEffects: ['**/*.css', '**/*.scss'],
    homepage: `https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/browse/${PackagesRootFolder}/${packageName}`,
    repository: {
      type: 'git',
      url: 'ssh://git@stashosl.firmglobal.com:7999/cnpm/confirmit-react-packages.git',
    },
    author: `${user} <${email}>`,
    contributors: [`${user} <${email}>`],
    files: ['dist', 'src'],
    license: 'UNLICENSED',
    peerDependencies: {
      '@babel/runtime': '7.x',
      classnames: '2.x',
      react: '^16.8 || ^17 || ^18',
      'react-dom': '^16.8 || ^17 || ^18',
      ...peerDeps,
    },
    dependencies: {
      ...deps,
    },
    devDependencies: devDeps,
    browserslist: [
      'last 5 Chrome versions',
      'last 5 Firefox versions',
      'last 5 Edge versions',
      'last 3 Safari major versions',
    ],
  };

  const packageJsonFile = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/package.json`
  );

  fs.writeFileSync(packageJsonFile, JSON.stringify(config, null, 2));
};

const changelog = ({packageRootFolderName}) => {
  // Whitespace in this const is intentional, since it defines how the markdown is shown
  const changelogContent = `## CHANGELOG

### v1.0.0

- Initial version
`;

  const file = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/CHANGELOG.md`
  );

  fs.writeFileSync(file, changelogContent);
};

const readme = ({packageRootFolderName, packageTitle, packageDescription}) => {
  // Whitespace in this const is intentional, since it defines how the markdown is shown
  const readmeContent = `# ${packageTitle}

[Changelog](./CHANGELOG.md)

${packageDescription}
`;

  const readmeFile = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/README.md`
  );

  fs.writeFileSync(readmeFile, readmeContent);
};

const npmrc = ({packageRootFolderName}) => {
  // whitespace is intentional
  const fileContent = `registry=https://proget2.firmglobal.com/npm/confirmit-npm
package-lock=false
save-exact=true
`;
  fs.writeFileSync(
    path.join(`./${PackagesRootFolder}/${packageRootFolderName}/.npmrc`),
    fileContent
  );
};

module.exports = {
  createFolderStructure,
  packageJson,
  changelog,
  readme,
  npmrc,
};
