const cssClassName = (string) => {
  return string.toLowerCase().replace('confirmit-', '').replace('react-', '');
};

const PackagesRootFolder = 'packages';

const Folders = {
  root: '/',
  tests: '/__tests__',
  src: '/src',
  stories: '/stories',
  srcComponents: '/src/components',
  testsComponent: '/__tests__/components',
};

module.exports = {
  PackagesRootFolder,
  Folders,
  cssClassName,
};
