const fs = require('fs');
const path = require('path');
const {
  PackagesRootFolder,
  Folders,
  cssClassName,
} = require('./file-template-utils');

const packageEntry = ({componentName, packageRootFolderName}) => {
  const filePath = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/${Folders.src}/index.ts`
  );
  // whitespace is intentional
  const fileContent = `import ${componentName} from './components/${componentName}';

export default ${componentName};
`;

  fs.writeFileSync(filePath, fileContent);
};

const componentStorybookIndex = ({componentName, packageRootFolderName}) => {
  const filePath = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/${Folders.stories}/index.stories.tsx`
  );
  // whitespace is intentional
  const sidebarFragment =
    '[`Latest version: ${componentPackage.version}`, componentReadme, componentChangelog]';
  const fileContent = `import React from 'react';
import {storiesOf} from '@storybook/react';
import componentReadme from '../README.md';
import componentChangelog from '../CHANGELOG.md';
import componentPackage from '../package.json';

import ${componentName} from '../src';

storiesOf('Components/${packageRootFolderName}', module)
.addParameters({
    readme: {
      sidebar: ${sidebarFragment},
    },
  })
  .add('Default', () => <${componentName} />);
`;

  fs.writeFileSync(filePath, fileContent);
};

const componentTestIndex = ({
  componentName,
  packageTitle,
  packageRootFolderName,
}) => {
  const filePath = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/${Folders.testsComponent}/${componentName}.spec.tsx`
  );
  // whitespace is intentional
  const fileContent = `import React from 'react';
import {shallow} from 'enzyme';
import {${componentName}} from '../../src/components/${componentName}';

describe('${packageTitle} :: ', () => {
  it('renders', () => {
    const component = shallow(<${componentName} />);

    // Please add a non-snapshot test
    expect(false).toBe(true);
  });
});
`;

  fs.writeFileSync(filePath, fileContent);
};

const componentIndex = ({
  packageName,
  componentName,
  packageRootFolderName,
}) => {
  const filePath = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/${Folders.srcComponents}/${componentName}.tsx`
  );
  // whitespace is intentional
  const fileContent = `import React, {AriaAttributes} from 'react';
import cn from 'classnames';

import {extractDataAriaIdProps} from '@jotunheim/react-utils';
import {bemFactory} from '@jotunheim/react-themes';

import classNames from './${componentName}.module.css';

const {block, element, modifier} = bemFactory({
  baseClassName: 'comd-${cssClassName(packageName)}',
  classNames,
});

type ${componentName}Props = {

} & AriaAttributes;

export const ${componentName} = ({
  ...rest
}: ${componentName}Props) => {
  const classes = cn(block());

  return (
    <div className={classes} {...extractDataAriaIdProps(rest)}>
      {${componentName}}
    </div>
  );
};

export default ${componentName};
`;

  fs.writeFileSync(filePath, fileContent);
};

const cssFile = ({componentName, packageName, packageRootFolderName}) => {
  const filePath = path.join(
    `./${PackagesRootFolder}/${packageRootFolderName}/${Folders.srcComponents}/${componentName}.module.scss`
  );
  // whitespace is intentional
  const fileContent = `@import '../../../global-styles/src/configuration.scss';

.comd-${cssClassName(packageName)} {

}
`;

  fs.writeFileSync(filePath, fileContent);
};

module.exports = {
  componentIndex,
  packageEntry,
  componentStorybookIndex,
  componentTestIndex,
  cssFile,
};
