const fs = require('fs');

const {logError, logInfo} = require('./console-utils');
const fileTemplatesGeneric = require('./file-templates-generic');
const fileTemplatesJavaScript = require('./file-templates-javascript');
const fileTemplatesTypeScript = require('./file-templates-typescript');

const updateFileContent = ({fileName, insertMarker, textToInsert}) => {
  const fileContent = fs.readFileSync(fileName, 'utf-8');
  const insertAtIndex = fileContent.indexOf(insertMarker);

  if (insertAtIndex === -1) {
    logError(`Could not find the insert key '${insertAtIndex}'`, 'error');
    process.exit(1);
  }

  const newContent = `${fileContent.slice(
    0,
    insertAtIndex
  )}${textToInsert}\n${fileContent.slice(insertAtIndex)}`;

  fs.writeFileSync(fileName, newContent);
};

const updateStoryBookConfig = ({packageRootFolderName}) => {
  const insertAt = '/* npm-init: insert-marker */';

  updateFileContent({
    fileName: './.storybook/main.ts',
    insertMarker: insertAt,
    textToInsert: `'../packages/${packageRootFolderName}/stories/index.stories.@(js|jsx|ts|tsx)',`,
  });
};

const ExistingPackageNames = (() => {
  return fs.readdirSync('./packages/');
})();

const bootstrapFiles = ({
  packageRootFolderName,
  packageName,
  packageTitle,
  packageDescription,
  user,
  email,
  componentName,
  deps = {},
  devDeps = {},
  peerDeps = {},
  usePlainJavaScript,
}) => {
  const templates = usePlainJavaScript
    ? fileTemplatesJavaScript
    : fileTemplatesTypeScript;

  peerDeps = usePlainJavaScript
    ? {
        'prop-types': '15.x',
        ...peerDeps,
      }
    : peerDeps;

  fileTemplatesGeneric.createFolderStructure({packageRootFolderName});
  logInfo('Created folder structure');

  fileTemplatesGeneric.changelog({packageRootFolderName});
  logInfo('Created changelog');

  fileTemplatesGeneric.readme({
    packageRootFolderName,
    packageTitle,
    packageDescription,
  });
  logInfo('Created readme');

  fileTemplatesGeneric.npmrc({packageRootFolderName});
  logInfo('Created .npmrc');

  fileTemplatesGeneric.packageJson({
    packageRootFolderName,
    user,
    email,
    packageTitle,
    packageName,
    packageDescription,
    deps,
    devDeps,
    peerDeps,
  });
  logInfo('Created package.json');

  templates.cssFile({
    componentName,
    packageName,
    packageRootFolderName,
  });
  logInfo('Created css file');

  templates.componentIndex({
    componentName,
    packageName,
    packageRootFolderName,
  });
  logInfo('Created component index');

  templates.packageEntry({
    componentName,
    packageName,
    packageRootFolderName,
  });
  logInfo('Created package entry');

  templates.componentStorybookIndex({
    componentName,
    packageRootFolderName,
  });
  logInfo('Created Storybook index');

  templates.componentTestIndex({
    packageRootFolderName,
    componentName,
    packageName,
    packageTitle,
  });
  logInfo('Created test index');
};

module.exports = {
  bootstrapFiles,
  ExistingPackageNames,
  updateStoryBookConfig,
};
