const inquirer = require('inquirer');
const shell = require('shelljs');

const fileUtils = require('./file-utils');
const gitUtils = require('./git-utils');
const {log, logInfo, logHelp, logError} = require('./console-utils');

const user = gitUtils.getGitUserName();
const email = gitUtils.getGitEmail();

gitUtils.gitFetch();
gitUtils.checkIfBehindMaster();

const getFolderNameFromPackageName = (packageName) =>
  packageName.replace('@jotunheim/', '').replace('react-', '');

// lower-case with dashes for spaces
const generatePackageName = (title) =>
  title.trim().replace(/\s+/g, '-').toLowerCase().replace('jotunheim-', '');

// CamelCased no spaces
const generateComponentName = (title) =>
  generatePackageTitle(title)
    .replace(/\s+/g, '')
    .replace('Forsta', '')
    .replace('Jotunheim', '');

// Camel Cased with spaces and removes "reserved" words
const generatePackageTitle = (input) =>
  input
    .toLowerCase()
    .replace(/(^| )(\w)/g, (x) => x.toUpperCase())
    .replace('Forsta', '')
    .replace('Jotunheim', '')
    .replace('React', '')
    .replace(/\s+/g, ' ')
    .trim();

const printInfoMessages = () => {
  logInfo(`Package Title format (the script will throw a validation error if you dont follow these rules):
  1. Has to start with Jotunheim React for React packages, or just Forsta for non-react packages (automatically prepended if you leave it out)
  2. Can only contain a-z,A-Z,0-9 and spaces
  3. Has to be unique (in this repo - no external sources are checked)
  4. Length has to be at least 9/17 characters (including "Forsta"/"Jotunheim React")
  5. Will automatically be Capitalized`);

  logInfo(`The package title will be used for:
  1. Package Title in package.json (obviously :)) (Example package name: "Jotunheim React My New Package")
  2. Folder- and filenames - will be converted to lowercase and hyphen-separated (for example @jotunheim/react-my-new-package)
  3. Componentname - will remove Forsta prefix and any spaces, and get PascalCased (for example MyNewPackage)
  4. CSS classnames - will remove Forsta prefix and replace any spaces with dashes, and get lowercased (for example my-new-package)`);

  logHelp(
    'Answer the following questions to get started, or press CTRL+C to abort...'
  );
};

printInfoMessages();

inquirer
  .prompt([
    {
      type: 'confirm',
      name: 'isReactComponent',
      message: 'Is It React Component Package?',
      default: true,
    },
    {
      type: 'confirm',
      name: 'usePlainJavaScript',
      message:
        'The package will use TypeScript. Do you want to use plain JavaScript instead?',
      default: false,
    },
    {
      type: 'input',
      name: 'packageTitle',
      message: 'Package Title',
      filter: (input, answers) => {
        const {isReactComponent} = answers;
        let capitalized = generatePackageTitle(input);

        if (isReactComponent) {
          capitalized = `Jotunheim React ${capitalized}`;
        } else {
          capitalized = `Forsta ${capitalized}`;
        }

        return capitalized;
      },
      validate: (input, answers) => {
        if (!input.match(/^([0-9a-zA-Z]+ ?)*$/)) {
          return 'Package title can only contain letters a-z, A-Z, numbers 0-9 and spaces';
        }
        if (
          answers.isReactComponent === false &&
          !input.startsWith('Forsta ')
        ) {
          return 'Package Title should start with "Forsta "';
        }
        if (answers.isReactComponent && !input.startsWith('Jotunheim React ')) {
          return 'Package Title should start with "Jotunheim React "';
        }
        if (
          fileUtils.ExistingPackageNames.includes(generatePackageName(input))
        ) {
          return `A package with the name ${input} already exists - please see if it fits your use-case, or choose a different name`;
        }
        if (answers.isReactComponent === false && input.length <= 9) {
          return 'Package title needs to be at least 9 characters (including "Forsta ")';
        }
        if (answers.isReactComponent && input.length <= 17) {
          return 'Package title needs to be at least 17 characters (including "Jotunheim React ")';
        }
        return true;
      },
    },
    {
      type: 'input',
      name: 'packageDescription',
      message: 'Package Description',
      default: '',
    },
  ])
  .then((answers) => {
    // cleanup linked files first, as this sometimes causes issues. This will be re-added in the bootstrap step at the end anyway
    logInfo('Removing bootstrapped files...');
    shell.exec('npm run clean', {silent: true});

    logInfo('Generating files...');

    const packageTitle = answers.packageTitle.trim();
    const packageName = generatePackageName(packageTitle);
    const componentName = generateComponentName(packageTitle);
    const packageRootFolderName = packageName
      .toLowerCase()
      .replace('confirmit-', '')
      .replace('react-', '');

    const depNames = ['@jotunheim/react-utils'];
    const devDepNames = ['@jotunheim/react-themes'];
    const peerDepNames = ['@jotunheim/react-themes'];

    const deps = depNames.reduce((hash, name) => {
      const folderName = getFolderNameFromPackageName(name);
      const pkg = require(`'./../../packages/${folderName}/package.json`);
      hash[name] = `${pkg.version}`;
      return hash;
    }, {});

    const devDeps = devDepNames.reduce((hash, name) => {
      const folderName = getFolderNameFromPackageName(name);
      const pkg = require(`'./../../packages/${folderName}/package.json`);
      hash[name] = `${pkg.version}`;
      return hash;
    }, {});

    const peerDeps = peerDepNames.reduce((hash, name) => {
      const folderName = getFolderNameFromPackageName(name);
      const pkg = require(`'./../../packages/${folderName}/package.json`);
      hash[name] = `${pkg.version.split('.')[0]}.x`;
      return hash;
    }, {});

    fileUtils.bootstrapFiles({
      packageRootFolderName,
      user,
      email,
      packageTitle,
      packageName,
      componentName,
      packageDescription: answers.packageDescription,
      deps,
      devDeps,
      peerDeps,
      usePlainJavaScript: answers.usePlainJavaScript,
    });

    fileUtils.updateStoryBookConfig({packageRootFolderName});

    logInfo('Finished generating files!');
    logInfo('Bootstrapping new package...This will take a few moments...');

    // new package needs to be bootstrapped to be able to run npm run dev
    const bootstrapResult = shell.exec('npm run bootstrap', {silent: true});
    if (bootstrapResult.code !== 0) {
      logError(bootstrapResult.stdout);
      logError(bootstrapResult.stderr);
      logError('Uh oh... something went wrong in bootstrapping the package...');
      logError(
        'Check the error message above, or seek help in #frontend channel in Slack'
      );
      process.exit(1);
    } else {
      log('Finished bootstrapping!', 'silly');
      logInfo(
        `Your new package is located in packages/${packageRootFolderName}`
      );
      logHelp(
        'You can start working on the package now :) (execute "npm run dev" to start storybook)'
      );
    }
    // gitUtils.gitCommit(packageName);
  });
