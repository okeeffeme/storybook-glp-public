const path = require('path');
const glob = require('glob');

const fileNames = glob.sync(path.join(__dirname, '../packages/*/package.json'));

const sharedPackages = new Map(
  fileNames.map((fileName) => {
    const pkg = require(fileName);
    return [pkg.name, pkg];
  })
);

const isSharedPackage = (name) => {
  return sharedPackages.has(name);
};

module.exports = function getPeerDeps(packageName) {
  const packageJson = sharedPackages.get(packageName);
  const peers = Object.keys(packageJson.peerDependencies || {}).filter(
    isSharedPackage
  );
  const dependencies = Object.keys(packageJson.dependencies || {}).filter(
    isSharedPackage
  );
  for (const dep of dependencies) {
    const pkg = sharedPackages.get(dep);
    peers.push(...Array.from(getPeerDeps(pkg.name)));
  }

  return new Set(peers);
};
