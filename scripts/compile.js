const path = require('path');
const glob = require('glob');
const moment = require('moment');

const argv = require('minimist')(process.argv.slice(2));
const noCache = argv.cache === 'false';
const pkg = argv.pkg ? argv.pkg : '*';

const writeScss = require('./compile/write-scss');
const writeJs = require('./compile/write-js');
const transformJs = require('./compile/transform-js');
const {
  emitDeclarations,
  createTSProgram,
} = require('./compile/emit-declarations');
const cacheJs = noCache
  ? require('./compile/no-cache-js')
  : require('./compile/cache-js');

(async function() {
  let cssTiming = 0;
  let jsTiming = 0;
  let tsTiming = 0;
  let emitTiming = 0;

  console.log(`Compiling...`);

  const packages = `../packages/${pkg}`;

  const folders = glob.sync(`${path.resolve(__dirname, packages)}`);
  const distPart = 'dist';
  const srcPart = 'src';

  const tsStart = moment();
  const tsFiles = glob.sync(
    path.resolve(__dirname, `../packages/*/src/**/*.{ts,tsx}`)
  );

  createTSProgram({fileNames: tsFiles});

  tsTiming += moment().diff(tsStart, 'ms');

  for (const folder of folders) {
    const src = `${folder}/${srcPart}`;
    const dist = `${folder}/${distPart}`;
    const distESM = `${dist}/esm`;
    const distCJS = `${dist}/cjs`;

    const cssStart = moment();

    const scssFiles = glob.sync(`${src}/**/!(_)*.scss`);
    const scssPipe = writeScss({src, distCJS, distESM});
    for (const file of scssFiles) {
      await scssPipe(file);
    }

    cssTiming += moment().diff(cssStart, 'ms');

    const jsStart = moment();

    const jsFiles = glob.sync(`${src}/**/*.{ts,tsx,js,jsx}`);
    const jsPipe = writeJs({src, distCJS, distESM});
    jsFiles.forEach(jsPipe(cacheJs(transformJs())));

    jsTiming += moment().diff(jsStart, 'ms');
    const emitStart = moment();

    emitDeclarations({
      src,
      distCJS,
      distESM,
      fileNames: jsFiles.filter(
        file => file.endsWith('.ts') || file.endsWith('.tsx')
      ),
    });

    emitTiming += moment().diff(emitStart, 'ms');
  }

  const totalTiming = tsTiming + cssTiming + jsTiming + emitTiming;
  console.log(`TS build has taken ${tsTiming}ms.`);
  console.log(`CSS build has taken ${cssTiming}ms.`);
  console.log(`JS build has taken ${jsTiming}ms.`);
  console.log(`Types emitting has taken ${emitTiming}ms.`);
  console.log(`Total ${totalTiming}ms.`);
})();
