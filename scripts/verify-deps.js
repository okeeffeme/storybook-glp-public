const fs = require('fs');
const glob = require('glob');
const path = require('path');
const babel = require('@babel/core');
const traverse = require('@babel/traverse').default;
const getPeerDeps = require('./getPeerDeps');

const folders = glob.sync(path.resolve(__dirname, '../packages/*'));

const isRelative = (importPath) => {
  return importPath.startsWith('.');
};

const isBabel = (importPath) => {
  return importPath.startsWith('@babel');
};

const isNodeModule = (importPath) => {
  return importPath === 'path';
};

const isReact = (importPath) => {
  return importPath === 'react' || importPath === 'react-dom';
};

const isBinary = (importPath) => {
  return importPath === 'svgo';
};

//rather than trying to traverse sass files for imports, ignore @jotunheim/global-styles to side-step unused check
const ignoreUnused = new Set('@jotunheim/global-styles');

/*
 * Extract package name from imported name
 * @jotunheim/react-button => react-button
 * react-select/creatable => react-select
 */
const extractPackageName = (importPath) => {
  const hasNamespace = importPath.startsWith('@');
  const packageNameParts = importPath.split('/');

  return hasNamespace
    ? `${packageNameParts[0]}/${packageNameParts[1]}`
    : packageNameParts[0];
};

let code = 0;
let packageVersions = new Map();

const lookForImport = (file, onImportFound) => {
  const {ast} = babel.transformFileSync(file, {ast: true});

  // find all import declarations and push them to Set which means duplications won't be counted
  // filter out relative and babel helpers imports
  traverse(ast, {
    enter(path) {
      if (path.isImportDeclaration()) {
        onImportFound(path.node.source.value);
      } else if (
        path.isExportDeclaration() &&
        path.node.source &&
        path.node.source.type === 'StringLiteral'
      ) {
        onImportFound(path.node.source.value);
      }
    },
  });
};

for (const folder of folders) {
  if (fs.existsSync(path.join(folder, 'package.json'))) {
    const files = glob.sync(path.join(folder, 'src/**/*.{js,jsx,ts,tsx}'));
    const pkg = require(path.join(folder, 'package.json'));
    const packageName = pkg.name;
    const peers = getPeerDeps(packageName);
    const deps = new Set();

    packageVersions.set(packageName, pkg.version);

    /* Gather all imported packages in src */
    for (const file of files) {
      lookForImport(file, (importPath) => {
        if (
          !isRelative(importPath) &&
          !isBabel(importPath) &&
          !isNodeModule(importPath)
        ) {
          deps.add(extractPackageName(importPath));
        }
      });
    }

    const missing = [];

    // Check if found imports are listed either in deps or peers
    // We can suggest some deps to be listed as peers in future.
    for (const value of deps) {
      const hasNoDep = !pkg.dependencies || !pkg.dependencies[value];
      const hasNoPeer = !pkg.peerDependencies || !pkg.peerDependencies[value];

      if (hasNoDep && hasNoPeer) {
        code = 1;
        missing.push(value);
      }
    }

    if (missing.length) {
      console.error(
        `Package "${packageName}" has missing dependencies or peer dependencies: "${missing.join(
          '", "'
        )}"`
      );
    }

    // Check implicit peer deps
    const missingPeers = [];
    for (const value of peers) {
      const hasNoPeer = !pkg.peerDependencies || !pkg.peerDependencies[value];

      if (hasNoPeer) {
        code = 1;
        missingPeers.push(value);
      }
    }

    if (missingPeers.length) {
      console.error(
        `Package "${packageName}" has missing implicit peer dependencies: "${missingPeers.join(
          '", "'
        )}"`
      );
    }

    // Check peer deps are present in devDependencies
    const missingDevPeers = [];
    for (const value of peers) {
      const hasNoDev = !pkg.devDependencies || !pkg.devDependencies[value];

      if (hasNoDev) {
        code = 1;
        missingDevPeers.push(value);
      }
    }

    if (missingDevPeers.length) {
      console.error(
        `Package "${packageName}" has peer dependencies but missing dev dependencies: "${missingDevPeers.join(
          '", "'
        )}"`
      );
    }

    /* Check unused deps in package.json */
    const unused = [];
    for (const name of [
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {}),
    ]) {
      if (
        !isBabel(name) &&
        !isReact(name) &&
        !isBinary(name) &&
        !deps.has(name) &&
        !peers.has(name) &&
        !ignoreUnused.has(name)
      ) {
        code = 1;
        unused.push(extractPackageName(name));
      }
    }

    if (unused.length) {
      console.error(
        `Package "${packageName}" has unused dependencies or peer dependencies: "${unused.join(
          '", "'
        )}"`
      );
    }
  }
}

/* Check version of dependencies */
for (const folder of folders) {
  if (fs.existsSync(path.join(folder, 'package.json'))) {
    const pkg = require(path.join(folder, 'package.json'));

    const deps = {
      ...pkg.dependencies,
      ...pkg.devDependencies,
    };

    for (const packageName in deps) {
      if (packageVersions.has(packageName)) {
        const actual = deps[packageName];
        const expected = `${packageVersions.get(packageName)}`;
        if (actual !== expected) {
          code = 1;
          console.error(
            `Package "${pkg.name}" has wrong dependency or devDependency version of ${packageName}. Should be "${expected}" instead of "${actual}"`
          );
        }
      }
    }
  }
}

process.exit(code);
