const path = require('path');
const fs = require('fs');
const sass = require('sass');
const chokidar = require('chokidar');
const glob = require('glob');
const sassGraph = require('sass-graph');

const {postProcessCss} = require('./compile/postProcessCss');

const graphSource = path.resolve(__dirname, '../packages');
const graphOptions = {extensions: ['scss']};

const folders = glob.sync(path.resolve(__dirname, '../packages/*/src'));
const watcher = chokidar.watch(folders, {
  ignoreInitial: true,
});

const eventTypes = {
  added: 'added',
  modified: 'modified',
};

const compileScss = eventType => async changedFilePath => {
  if (!changedFilePath.endsWith('.scss')) {
    return;
  }

  const filePaths = [];

  if (eventType === eventTypes.added) {
    filePaths.push(changedFilePath);
  } else if (eventType === eventTypes.modified) {
    const graph = sassGraph.parseDir(graphSource, graphOptions);
    filePaths.push(changedFilePath);
    graph.visitAncestors(changedFilePath, ancestorFilePath => {
      filePaths.push(ancestorFilePath);
    });
  }

  for (const filePath of filePaths) {
    const dirname = path.dirname(filePath);
    const basename = path.basename(filePath, '.scss');
    const filename = path.join(dirname, `${basename}.css`);
    if (!basename.startsWith('_')) {
      console.log(`[watchScss]: ${filePath} is changed. Transforming...`);
      try {
        const {css} = sass.renderSync({
          file: filePath,
          includePaths: [path.resolve(__dirname, '../../node_modules')],
        });

        try {
          const {css: processedCss} = await postProcessCss({
            from: filePath,
            css,
          });

          fs.writeFileSync(filename, processedCss);
        } catch (error) {
          console.error(`[watchScss]: Unable to write file ${filename}`);
          console.error(error);
        }
      } catch (error) {
        console.error(`[watchScss]: ${error.formatted}`);
      }
    }
  }
};

watcher
  .on('add', compileScss(eventTypes.added))
  .on('change', compileScss(eventTypes.modified));
