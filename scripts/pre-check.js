#!/usr/bin/env node

const colors = require('colors/safe');
const shell = require('shelljs');
const path = require('path');

const updateRemotes = () => {
  // Get latest refs so we can easily check that we are in not behind origin/master
  return shell.exec('git remote update', {silent: true});
};

const getCommitsBehindMaster = () => {
  return shell
    .exec('git rev-list HEAD..origin/master --count', {silent: true})
    .stdout.trim();
};

const verifyDeps = () => {
  const p = path.resolve(__dirname, './verify-deps.js');

  return shell.exec(`node ${p}`, {silent: true}).stderr.trim();
};

const isOnRoot = () => {
  const cdup = shell
    .exec('git rev-parse --show-cdup', {silent: true})
    .stdout.trim();

  return cdup === '';
};

const hasUncommitedChanges = () => {
  const exitCode = shell.exec('git diff-index --quiet HEAD --', {silent: true})
    .code;

  return Boolean(exitCode);
};

const logErrorAndExit = (text, exitCode = -1) => {
  logMessage(text, colors.red);
  process.exit(exitCode);
};

const logMessage = (text, color = colors.green) => {
  console.log(color(` - ${text}`));
};

module.exports = async (type) => {
  console.log(colors.magenta('[pre-check]'));
  logMessage(
    'Performing automated checks, please wait... (Press CTRL+C any time to abort)',
    colors.cyan
  );

  if (hasUncommitedChanges()) {
    logErrorAndExit(
      'There are uncommitted changes, please fix this and run command again.'
    );
  }

  if (!isOnRoot()) {
    logErrorAndExit(
      'This command should be executed from the projects root directory.'
    );
  }

  updateRemotes();

  const commitsBehindMaster = getCommitsBehindMaster();

  if (commitsBehindMaster > 0) {
    logErrorAndExit(`You are ${commitsBehindMaster} commits behind origin/master. To reduce errors when calculating new versions,
  please merge with master first (execute "git pull origin master").`);
  }

  const dependencyErrors = verifyDeps();

  if (dependencyErrors) {
    logErrorAndExit(`Incorrect dependencies found:
  ${dependencyErrors}

  update these to correct versions and try again.`);
  }

  if (type === 'stable') {
    logMessage(
      'IMPORTANT: Do NOT rewrite commit history (rebase, squash, fixup, etc.) after this command has executed.',
      colors.cyan
    );
    logMessage(
      'IMPORTANT: Are you going to release an ALPHA version? If so, please exit this script and execute `npm run alpha` instead.',
      colors.cyan
    );
    logMessage(
      'IMPORTANT: Please ensure all tasks in the PR are resolved, and that changelogs are up to date.',
      colors.cyan
    );
    logMessage(
      "IMPORTANT: Please merge the PR as soon as possible after updating versions to avoid versioning conflicts with other PR's.",
      colors.cyan
    );
  }

  console.log(colors.magenta('[pre-check] OK'));
};
