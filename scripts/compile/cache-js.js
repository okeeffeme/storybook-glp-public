const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

const ensureDirectory = require('../utils/ensureDirectory');

const secret = 'confirmit-react-packages-v13';
const cacheDirectory = path.resolve(
  __dirname,
  '../../node_modules/__confirmit-react-packages-cache__'
);

const readFileContentFromCache = hash => {
  const cjs = path.join(cacheDirectory, `${hash}-cjs`);
  const esm = path.join(cacheDirectory, `${hash}-esm`);
  if (fs.existsSync(cjs) && fs.existsSync(esm)) {
    return {
      cjs: fs.readFileSync(cjs),
      esm: fs.readFileSync(esm),
    };
  }
};

const writeFileContentToCache = (hash, content) => {
  const cjs = path.join(cacheDirectory, `${hash}-cjs`);
  const esm = path.join(cacheDirectory, `${hash}-esm`);
  ensureDirectory(cjs);

  fs.writeFileSync(cjs, content.cjs);
  fs.writeFileSync(esm, content.esm);
};

module.exports = transformJs => file => {
  const buffer = fs.readFileSync(file);

  const hmac = crypto.createHmac('sha1', secret);
  hmac.update(buffer);
  const hash = hmac.digest('hex');

  let content = readFileContentFromCache(hash);
  if (!content) {
    content = transformJs(file);
    writeFileContentToCache(hash, {
      cjs: content.cjs,
      esm: content.esm,
    });
  }

  return content;
};
