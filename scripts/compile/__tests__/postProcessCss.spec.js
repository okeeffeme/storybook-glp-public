import {postProcessCss} from '../postProcessCss';

describe('postProcessCss', () => {
  it('should discard comments', async () => {
    const css = `
/* comment to discard */
.comd-block {}
`;
    const output = await postProcessCss({css, from: 'fake-file-path'});

    const expectedCss = `
.comd-block{}
`;
    expect(output.css).toBe(expectedCss);
  });
});
