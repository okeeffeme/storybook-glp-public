const path = require('path');
const fs = require('fs');
const ts = require('typescript');

const ensureDirectory = require('../utils/ensureDirectory');

// Create a Program with an in-memory emit
const createdFiles = {};

function createTSProgram({fileNames}) {
  const options = {
    allowJs: false,
    declaration: true,
    emitDeclarationOnly: true,
  };

  const host = ts.createCompilerHost(options);
  host.writeFile = (fileName, contents) => (createdFiles[fileName] = contents);

  // Prepare and emit the d.ts files
  const program = ts.createProgram(fileNames, options, host);
  program.emit();
}

/* This function is used to generate d.ts for each .ts/.tsx file
 * To speed up the process files are passed in batches and
 * this is different from transpilation where we feed babel with files one by one.
 *  */
function emitDeclarations({src, distCJS, distESM, fileNames}) {
  // Loop through all the input files
  fileNames.forEach((file) => {
    console.log('Generating d.ts file for', file);

    const relativePathToSrcFile = path.relative(src, file);
    const dirname = path.dirname(relativePathToSrcFile);
    const extension = path.extname(relativePathToSrcFile);
    const basename = path.basename(relativePathToSrcFile, extension);
    const filename = path.join(dirname, `${basename}.d.ts`);

    const cjsOutFile = path.resolve(distCJS, filename);
    const esmOutFile = path.resolve(distESM, filename);

    const dts = file.replace(extension, '.d.ts');
    const dtsContent = createdFiles[dts];

    ensureDirectory(cjsOutFile);
    ensureDirectory(esmOutFile);

    fs.writeFileSync(cjsOutFile, dtsContent);
    fs.writeFileSync(esmOutFile, dtsContent);
  });
}

module.exports = {
  emitDeclarations,
  createTSProgram,
};
