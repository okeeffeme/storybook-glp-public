const babel = require('@babel/core');

module.exports = () => file => {
  console.log(`BABEL transforming: ${file}...`);
  const {code: esm, ast} = babel.transformFileSync(file, {
    ast: true,
  });
  const {code: cjs} = babel.transformFromAst(ast, esm, {
    babelrc: false,
    configFile: false,
    filename: file,
    plugins: ['@babel/plugin-transform-modules-commonjs'],
  });

  return {
    cjs,
    esm,
  };
};
