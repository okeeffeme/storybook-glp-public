const path = require('path');
const fs = require('fs');
const sass = require('sass');

const ensureDirectory = require('../utils/ensureDirectory');
const {postProcessCss} = require('./postProcessCss');

module.exports = ({src, distCJS, distESM}) => async file => {
  console.log(`SCSS transforming: ${file}...`);
  const {css} = sass.renderSync({
    file,
    includePaths: [path.resolve(__dirname, '../../node_modules')],
  });

  const relativePathToSrcFile = path.relative(src, file);
  const dirname = path.dirname(relativePathToSrcFile);
  const basename = path.basename(relativePathToSrcFile, '.scss');
  const filename = path.join(dirname, `${basename}.css`);

  const srcOutFile = path.resolve(src, filename);
  const cjsOutFile = path.resolve(distCJS, filename);
  const esmOutFile = path.resolve(distESM, filename);

  ensureDirectory(srcOutFile);
  ensureDirectory(cjsOutFile);
  ensureDirectory(esmOutFile);

  const {css: processedCss} = await postProcessCss({from: srcOutFile, css});
  fs.writeFileSync(cjsOutFile, processedCss);
  fs.writeFileSync(esmOutFile, processedCss);
  fs.writeFileSync(srcOutFile, processedCss);
};
