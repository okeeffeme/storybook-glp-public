const postcss = require('postcss');

module.exports = {
  postProcessCss: async function({css, from}) {
    const postCssPlugins = [require('postcss-discard-comments')];
    return postcss(postCssPlugins).process(css, {from});
  },
};
