const http = require('http');
const execa = require('execa');

// When running on IIS, port nr will be specified as PORT env variable
const port = process.env.PORT || 8234;

const CHANGED = 'repo:refs_changed';
const MASTER_BRANCH = 'refs/heads/master';

const logError = (msg) => {
  console.log('An error occurred during rebuild:');
  console.log(msg);
};

const executeShellCommand = async (command) => {
  console.log(`executing command "${command}"...`);
  const result = await execa.command(command);

  if (result.exitCode !== 0) {
    logError(result.stderr);
  }
  console.log(`executing command "${command}" completed`);
};

const executePowershellScript = async (command) => {
  console.log(`executing PS command "${command}"`);
  const result = await execa.command(command, {shell: 'powershell.exe'});

  if (result.exitCode !== 0) {
    logError(result.stderr);
  }
  console.log(`executing command "${command}" completed`);
};

const handleChanged = async (data) => {
  const {changes} = data;
  const branch = changes[0].refId;

  if (branch === MASTER_BRANCH) {
    console.log('master branch updated - rebuild Storybook');

    await executeShellCommand('git checkout -- .');
    await executeShellCommand('git pull');
    await executeShellCommand('npm run clean');
    await executeShellCommand('npm prune');
    await executeShellCommand('npm install');
    await executeShellCommand('npm run bootstrap');
    await executeShellCommand('npm run build-static');

    await executePowershellScript('Restart-WebAppPool "DesignSystem"');

    console.log('build complete');
    console.log('');
  }
};

const server = http.createServer();
server.on('request', async (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  if (req.method !== 'POST') {
    res.statusCode = 405;
    res.end('{"error":"METHOD_NOT_ALLOWED"}');
    return;
  }

  let body = '';

  req.on('data', (data) => {
    body += data;
  });

  req.on('end', async () => {
    let parsed;

    try {
      if (body) {
        parsed = JSON.parse(body);

        const {eventKey, ...rest} = parsed;

        if (eventKey == CHANGED) {
          await handleChanged(rest);
        }
      }

      res.end(
        JSON.stringify({
          error: false,
        })
      );
    } catch (e) {
      console.log(e);
      res.statusCode = 400;
      res.end('{"error":"CANNOT_PARSE"}');
    }
  });
});

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});
