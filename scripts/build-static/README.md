This is a mini server, that listens for webhook calls from Bitbucket, and whenever master branch is updated, it does a checkout of the new changes, and does a new build for the Storybook so the demo pages (http://designsystem.firmglobal.com) will be updated.

It is running on iisnode on co-osl-code01.

## Setup webhook in Bitbucket

There is a webhook defined on the projects Bitbucket page: https://stashosl.firmglobal.com/plugins/servlet/webhooks/projects/CNPM/repos/confirmit-react-packages/ - you need to have project admin access to access this page. Here you can change the port and url for where the webhook should post. Currently it is posting to http://co-osl-code01:8234.
