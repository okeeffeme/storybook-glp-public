const path = require('path');
const files = process.argv.slice(2);

const packageNames = new Set();
for (let file of files) {
  const root = path.resolve(__dirname, '..');
  const relativePath = path.relative(root, file);

  const folders = relativePath.split(path.sep);
  const index = folders.indexOf('packages');
  if (index === 0) {
    packageNames.add(folders[index + 1]);
  }
}

if (packageNames.size > 1) {
  console.error('Only one commit per package is allowed!!!');
  process.exit(1);
}
