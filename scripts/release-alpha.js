const {execFileSync} = require('child_process');
const shell = require('shelljs');

const preCheck = require('./pre-check');

preCheck('alpha');

const getJiraIdFromBranchName = () => {
  const JIRA_ID_PATTERN = new RegExp(/[A-Z0-9]{2,}-[1-9][0-9]*/);
  const branchName = shell
    .exec('git symbolic-ref --short HEAD', {silent: true})
    .stdout.trim();

  const match = JIRA_ID_PATTERN.exec(branchName);

  return match ? match[0] : null;
};

const getAlphaPreId = () => {
  const jiraId = getJiraIdFromBranchName();
  if (!jiraId)
    throw `alpha versions can only be created on branches that have a JIRA id.`;
  return `alpha-${jiraId}`;
};

const getLernaParameters = () => {
  const parameters = [
    'version',
    'prerelease',
    '--preid',
    getAlphaPreId(),
    '--include-merged-tags',
    '--no-git-tag-version', // Dont create git tags for alpha versions, as it seems to cause problems when later releases should be created.
    '--exact',
    '--no-commit-hooks',
  ];

  return parameters;
};

const lernaParameters = getLernaParameters();

const lernaCommand = process.platform == 'win32' ? 'lerna.cmd' : 'lerna';
execFileSync(lernaCommand, lernaParameters, {stdio: 'inherit'});

// Because of --no-git-tag-version, Lerna will also not create a commit, and not push, so we need to do it manually
// As a bonus, this change will also fix NPM-534, since a alpha release no longer will count as a change entry. Only "stable" releases will count as changes.
// This also means you can release a new alpha version without any changes.
console.log('Pushing changes...');
shell.exec('git add **/package.json', {silent: true});
shell.exec('git commit -m "Publish alpha versions"', {silent: true});
shell.exec('git push', {silent: true});
