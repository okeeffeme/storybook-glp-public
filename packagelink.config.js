/* eslint-env node */
const path = require('path');
const glob = require('glob');
const fs = require('fs');

module.exports = {
  publish: {
    packages: glob
      .sync(path.resolve(__dirname, 'packages/*'))
      .filter((folder) => {
        const pkgJson = path.resolve(__dirname, `${folder}/package.json`);
        if (fs.existsSync(pkgJson)) {
          return true;
        }
        return false;
      }),
  },
};
