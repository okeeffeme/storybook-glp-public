# Confirmit-React-Packages

This is monorepo mantained by [lerna](https://github.com/lerna/lerna) tool

## Contribution Guide

If you would like to contribute this repo: [read this](docs/contributionGuide.stories.mdx)

## Scripts

```
  dev - runs storybook for all packages
  add-package - run this script and answer a few questions to add a new package easily
  bootstrap - installs modules and links packages to each other
  compile - runs "compile" for each package. compiles scripts to dist folder of each package.
  alpha - runs wizard to release ALPHA versions of the changed packages
  release - runs wizard to release stable versions of packages
  release-conventional - creates releasenotes and a release, and uses conventional commits to decide version numbers automatically. You must follow conventional commits rules to use that.
  changelog - generates changelogs using conventional commits notation. you must follow conventional commits rules to use that.
  test - runs jest tests for each package
  eslint - runs eslint for each package
  stylelint - runs stylelint for each package
```

## Conventional commits

This repository supports [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) but not forces anyone to use it yet.
In order to use conventional commits to generate versions and changelog you must follow specification while writing commit message.
As we use monorepo, each commit that bumps version must belong
to a single package else there could be unintentional version bumps in other packages.

## Best practises

If you are about to start using package from this repo: [read this](docs/bestPractices.stories.mdx)

## Css Modules

We support css modules. Would you like to use it in your app? [read this](docs/cssModules.stories.mdx)

## Codemods

Some breaking changes can be update with codemods: [read this](docs/codemods.stories.mdx)

## Solidarity

This repo is set up with [Solidarity](https://github.com/infinitered/solidarity) to ensure we are all working with the same Node/NPM versions, and that you have set up an email with confirmit.com on your git user. You will get an error if you break any of these rules when running `npm run dev`, or `npm run update-versions`.

For some reason, `git bash` does not give detailed output on what rules you are breaking - so if you just see `Solidarity checks failed`, try running in a different terminal to see the actual failures.

## Typescript

This repo allows developing using Typescript. Check [TypeScript Guideline](docs/typeScriptGuideline.stories.mdx)

# Troubleshooting

## Popper.js error

There is currently a known issue with popper.js causing problems when working in this repo. If you see an error in console related to popper.js when trying to install/bootstrap/dev, try executing `npm run clean` first.
