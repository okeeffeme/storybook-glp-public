<Meta title="Guides/Contribution Guide" />

# Contribution Guide

## Getting started

- Clone repo
- execute `npm install`
- execute `npm run bootstrap`. This will link packages that depend on eachother together.
- execute `npm run dev` to start dev server, and access Storybook on [http://localhost:9001](http://localhost:9001)
- execute `npm run alpha` to release alpha versions of packages to try out in your app
- execute `npm run release` to release stable versions of packages

## Running tests

- Executing `npm run test` will run all unit tests

  - To run unit tests for a specific package, execute `npm run test -- packages/{packageName}/`

  #### Debugging and running particular tests in webstorm

  - Choose test or test suite to run in editor window (clicking on green circle close to the row number or right click on "describe" or "it")
  - Select either debug or run test
  - First run will fail as it chooses the test folder as working directory and finds package.json of particular package where no jest config is set
  - You have to manually choose jest config file for created webstorm configuration, usually it prompts available ones
  - Rerun test once again

## Development

All changes should be connected with a JIRA id. The [Frontend projects](https://jiraosl.firmglobal.com/secure/RapidBoard.jspa?rapidView=73&view=planning.nodetail&issueLimit=100) board should contain the tasks related to this project.
If the changes you are doing are directly related to some feature in another app, you should still create a task on this board, and you can link the JIRA id's together. It is important to create a task on this board to create visibility to other developers since this project is a shared initiative between all frontend developers.

After a PR has been opened, please do not rewrite commit history (rebase, squash, fixup, etc.) since this makes is difficult for reviewers to only look at new code changes. This is especially important for large PR's.

After the PR has been approved and is ready to merge (all tasks are resolved, changelogs are updated, and `npm run release` has been run), please make sure to merge the PR as soon as possible to avoid versioning conflicts with other PR's.

Changes to README and tests does **not** require a new version to be published, as these changes are ignored when calculating versions.

### Commit style and changelogs

- This repository is set up to use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/), which means you can generate changelogs automatically if you follow the message style convention when creating commits.
  - Start each commit message with a "type". The most important types to keep in mind are:
    - "fix:" a commit that fixes a bug (this correlates with PATCH in semantic versioning)
    - "feat:" a commit that introduces a new backwards-compatible feature (this correlates with MINOR in semantic versioning)
    - "BREAKING CHANGE:" a commit that introduces a breaking change in behavior
      (this correlates with MAJOR in semantic versioning)
  - Other relevant types are:
    - "refactoring:" does not change the behavior of the code, just improvements/changes to the internal workings of the code
    - "chore:" changes unrelated to the code, f.ex. documentation, build process, etc.
    - "test:" updates to test code

This also makes it important to commit related code together, and commit often. F.ex if you are making changes in two unrelated packages, where one of them contain a BREAKING CHANGE, you need to ensure these changes are done in separate commits, so that the other package that only contains a bugfix does not also get a MAJOR version bump.

If you follow this convention for your messages, you can generate the changelogs automatically during release with `npm run release-conventional`. You can check what the changelogs will contain with `npm run changelog`, but the results of this script should be discarded.

If you do not follow this convention, you can still add manual entries to the CHANGELOG.md files.

### Adding new package to repo

1. Create feature branch
2. Execute `npm prune && npm install` to ensure you have the latest dependencies installed, and cleanup any unused leftover dependencies.
3. Execute `npm run add-package`, and answer the questions.

   - "Forsta" or "Jotunheim React " will be automatically added to the title if you do not write it
   - Each word will be capitalized, the rest will be lowercase

4. After add-package script is finished, follow the steps in "Adding changes to existing packages" (skip to step 4)

### Adding changes to existing packages

#### Using conventional commits

1. Create feature branch
2. Execute `npm prune && npm install` to ensure you have the latest dependencies
3. Execute `npm run bootstrap` to update modules and links between modules
4. Make your changes (don't forget to add tests and a Storybook example)
5. Commit often, and keep related code changes together (see [Commit style and changelogs](#commit-style-and-changelogs))
6. Execute `npm run test` to run unit tests
7. Create pull request and get it approved

   - Once PR is approved ensure that you merge master into your feature branch, so that version calculation will be correct.

8. Execute `npm run release-conventional`

   - **Do not edit the version number in package.json manually, because this will not generate git tags, and will make it more difficult to track changes.**
   - Check the CHANGELOG of the package(s) you make changes to, and verify they are the same that are generated from release command

9. After running the release script, merge the changes in your pull request once the build is green. This should be done in a timely fashion so it will not cause conflicts for others that also want to merge.

---

#### Not using conventional commits

1. Create feature branch
2. Execute `npm prune && npm install` to ensure you have the latest dependencies
3. Execute `npm run bootstrap` to update modules and links between modules
4. Make your changes (don't forget to add tests and a Storybook example)
5. Commit often, and keep related code changes together (see [Commit style and changelogs](#commit-style-and-changelogs))
6. Execute `npm run test` to run unit tests
7. Create pull request and get it approved

   - Once PR is approved ensure that you merge master into your feature branch, so that version calculation will be correct.

8. Execute `npm run release` - You will be asked about new version for every changed package.

   - **Do not edit the version number in package.json manually, because this will not generate git tags, and will make it more difficult to track changes.**
   - Breaking changes -> update major version
   - New feature -> update minor version
   - Bug fix/refactoring/test fixes -> update patch version

9. Go through CHANGELOG.md or the CHANGELOG section in readme.md of the package(s) you make changes to, and update the version number to the correct one (that you got from the release command)

   - Editing README/CHANGELOG does **not** require a new version to be published

10. After running the release script and pushing your additions to the CHANGELOG, merge the changes in your pull request once the build is green. This should be done in a timely fashion so it will not cause conflicts for others that also want to merge.

### Side Effects

- Packages are supporting sideEffects webpack feature by default.
- Consider to verify sideEffects property of packages.json. Usually theme.js and _.css/_.scss files are enough to add to sideEffects.

## Trying out unpublished packages locally

### Using npm-link

- cd into package directory (f.ex cd packages/button)
- execute `npm link`
- go to the project where you want to use this package, f.ex root folder for Survey Designer
- execute `npm link @confirmit/react-button` (replace `@confirmit/react-button` with the package you are using)
- after you have done some changes, execute `npm run compile -- --pkg confirmit-name-of-package`. After this is done, the changes should be reflected in the app you are using it from. The `--pkg` parameter is optional. If it is omitted, all packages will be compiled (which will take longer). This script has to be run from the root directory.
- For full documentation, refer to [official npm docs](https://docs.npmjs.com/cli/link.html)

### Building package and including it in package.json

This is the recommended approach if you want to share the package with other people. For a local development on the same machine, see Using npm-link instead.

If you need to test your new or changed package in another project, you can create a local build of the package by running

```
npm run pack-packages:to-root
```

This will build all packages to tgz-files placed in the root folder of confirmit-react-packages.
Then you can install the package you want by copying the corresponding tgz-file to your projects
root folder and running

```
npm install <package name>.tgz
```

### publish alpha versions

- real installation of component package
- possibility to test in Pull Requests.
- publish of alphas could be time consuming if you would like to test changes often

### packagelink library [link](https://www.npmjs.com/package/@satispunk/packagelink)

This is similar but more automated than `npm run pack-packages:to-root`

- real installation of components
- possibility to test in Pull Requests.
- much faster installation than using alpha versions

Use `npm run packagelink` then packs all shared components to the system temporary folder.
Install packagelink in consumer application or use it globaly. Use `packagelink install [packagename]` and
it will put tgz archive to you application and install it. If you would like to test a batch of component
it could be more convenient to use `packagelink.config.js` with the list of package names.
Make sure that implicit dependency are also listed in `packagelink.config.js` else `npm` will try to install it from
remote repository.
Example:

- `Popover` component has dependency on `PortalTrigger`
- Application has dependency on `Popover` only. No need to specify `PortalTrigger` in application as there is no direct usage.
- If we install only `Popover` with packagelink, then in application it will depend on already published but incorrect version of `PortalTrigger` (which is taken from remote repo).
- To avoid this you need to specify `PortalTrigger` dependency in `packagelink.config.js`
