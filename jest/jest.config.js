const path = require('path');
const fs = require('fs');
const glob = require('glob');
const packageFolders = glob
  .sync(`${path.resolve(__dirname, '../packages/*')}`)
  .filter((folder) => {
    const pkgJson = path.resolve(__dirname, `${folder}/package.json`);
    if (fs.existsSync(pkgJson)) {
      return true;
    }
    return false;
  });

const packageAliases = packageFolders.reduce((aliases, folder) => {
  const pkg = require(path.join(folder, 'package.json'));
  const packageName = pkg.name;
  const folderName = path.basename(folder);
  const packageEntry = path.resolve(__dirname, `../packages/${folderName}/src`);

  if (fs.existsSync(packageEntry)) {
    aliases[`${packageName}$`] = packageEntry;
  }

  return aliases;
}, {});

module.exports = {
  testMatch: ['**/?(*.|*-)+(spec|test).[jt]s?(x)'],
  setupFilesAfterEnv: ['./jest/jest.setup.js'],
  transform: {
    '^.+\\.jsx?$': './jest/jest.transform.js',
    '^.+\\.tsx?$': './jest/jest.transform.js',
  },
  rootDir: path.resolve(__dirname, '..'),
  testEnvironment: 'jsdom',
  coverageReporters: ['html', 'text'],
  coveragePathIgnorePatterns: ['/node_modules/', '/__tests__/'],
  moduleNameMapper: {
    '.+\\.(svg|css|scss)$': 'identity-obj-proxy',
    ...packageAliases,
  },
  testResultsProcessor: 'jest-teamcity-reporter',
  snapshotSerializers: ['<rootDir>/node_modules/enzyme-to-json/serializer'],
  testURL: 'http://localhost',
};
