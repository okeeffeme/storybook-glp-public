/* eslint-env browser, jest */
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import moment from 'moment-timezone';
import '@testing-library/jest-dom';

// Make date-picker and calendar tests independent on where they are run
moment.tz.setDefault('CET');

Enzyme.configure({adapter: new Adapter(), disableLifecycleMethods: true});

global.MutationObserver = function () {
  return {
    observe() {},
    disconnect() {},
  };
};
