import {addons} from '@storybook/addons';
import {create} from '@storybook/theming';

addons.setConfig({
  theme: create({
    base: 'dark',
    brandTitle: 'Forsta React Packages',
    brandUrl:
      'https://stashosl.firmglobal.com/projects/CNPM/repos/confirmit-react-packages/browse',
  }),
  panelPosition: 'right',
  sidebar: {
    showRoots: true,
  },
});
