import React from 'react';
import {addReadme} from 'storybook-readme';
import {addDecorator} from '@storybook/react';

import './styles/storybook.scss';

export const parameters = {
  readme: {
    theme: {
      bodyColor: 'inherit',
    },
  },
  backgrounds: {
    default: 'white',
    values: [
      {
        name: 'white',
        value: '#ffffff',
      },
      {
        name: 'grey',
        value: '#888888',
      },
      {
        name: 'darkgrey',
        value: '#333333',
      },
      {
        name: 'black',
        value: '#000000',
      },
    ],
  },
  options: {
    storySort: (a, b) =>
      a[1].kind === b[1].kind
        ? 0
        : a[1].id.localeCompare(b[1].id, {numeric: true}),
  },
  paddings: {
    values: [
      {name: 'Small', value: '8px'},
      {name: 'Medium', value: '16px'},
      {name: 'Large', value: '32px'},
    ],
    default: 'Medium',
  },
};

export const globalTypes = {};

addDecorator(addReadme);
addDecorator((storyFn) => (
  <div>
    <React.StrictMode>{storyFn()}</React.StrictMode>
  </div>
));
