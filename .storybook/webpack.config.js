const path = require('path');
const fs = require('fs');
const glob = require('glob');

const folders = glob.sync(`${path.resolve(__dirname, '../packages/*')}`);

const cssLoader = ({extraModulesOptions = {}} = {}) => {
  return {
    loader: 'css-loader',
    options: {
      modules: {
        mode: 'icss',
        ...extraModulesOptions,
      },
      importLoaders: 1,
    },
  };
};

module.exports = async ({config /*, mode*/}) => {
  config.devServer = config.devServer || {};
  config.devServer.stats = 'errors-warnings';

  // Find polyfill module in preview bundle and remove it, to be sure we provide needed polyfills as peerDeps
  const index = config.entry.findIndex((item) => {
    return item.match(/polyfills/);
  });
  config.entry.splice(index, 1);

  /* configure aliases of package names,
   * so in storybook components refers to 'src' version of component,
   * which allows to make cross package changes without extra compile step
   */
  config.resolve = config.resolve || {};
  config.resolve.alias = config.resolve.alias || {};
  folders.forEach((folder) => {
    const pkgJson = path.join(folder, 'package.json');

    if (fs.existsSync(pkgJson)) {
      const pkg = require(pkgJson);
      const packageName = pkg.name;
      const folderName = path.basename(folder);
      const packageEntry = path.resolve(
        __dirname,
        `../packages/${folderName}/src`
      );

      if (fs.existsSync(packageEntry)) {
        config.resolve.alias[`${packageName}$`] = packageEntry;
      }
    }
  });

  // Remove if module federation can be enabled in storybooks instead
  Object.assign(config.resolve.alias, {
    'ds/AppHeader17': path.resolve(__dirname, `../packages/app-header/src`),
  });

  const babelLoaderIndex = config.module.rules.findIndex((rule) => {
    return rule.test && rule.test.toString() === /\.(mjs|jsx?)$/.toString();
  });

  if (babelLoaderIndex > -1) {
    const babelLoader = config.module.rules[babelLoaderIndex];
    babelLoader.test = /\.(mjs|tsx?|jsx?)$/;
  }

  // Comment to disable css modules
  const cssLoaderIndex = config.module.rules.findIndex((rule) => {
    return rule.test && rule.test.toString() === /\.css$/.toString();
  });
  if (cssLoaderIndex > -1) {
    config.module.rules.splice(cssLoaderIndex);
    config.module.rules.unshift({
      test: /\.module\.css$/,
      use: [
        'style-loader',
        cssLoader({
          extraModulesOptions: {
            localIdentName: '[local]___[hash:base64:5]',
          },
        }),
      ],
    });
    config.module.rules.unshift({
      test: /^((?!\.module).)*\.css$/,
      use: ['style-loader', cssLoader()],
    });
  }

  config.module.rules.push({
    test: /\.woff2$/,
    type: 'asset/resource',
  });

  config.module.rules.push({
    test: /\.png$/,
    type: 'asset/resource',
  });

  config.module.rules.push({
    test: /\.scss$/,
    use: ['style-loader', cssLoader(), 'sass-loader'],
  });

  config.resolve.extensions.push('.ts', '.tsx');
  return config;
};
