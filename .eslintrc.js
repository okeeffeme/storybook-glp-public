/* eslint-env node */
const esRules = {
  'no-prototype-builtins': 0,
  'no-restricted-imports': [
    'error',
    {
      paths: [
        {
          name: 'lodash',
          message:
            "Please import the specific lodash method(s) you need; ex: import uniq from 'lodash/uniq' ",
        },
      ],
    },
  ],
  'import/no-named-as-default': 0,
  'react/no-unescaped-entities': 0,
  'react/no-find-dom-node': 0,
  'react-hooks/rules-of-hooks': 'error',
  'react-hooks/exhaustive-deps': 'error',
  'shared-packages/no-direct-imports-in-tests-and-stories': 2,
  'shared-packages/no-imports-from-dist': 2,
  'shared-packages/imports-from-src-index-in-tests-and-stories': 2,
};

module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true,
      modules: true,
    },
  },
  extends: [
    'prettier',
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:import/react',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  plugins: ['react', 'import', 'prettier', 'react-hooks', 'shared-packages'],
  settings: {
    react: {
      version: 'detect',
    },
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            extensions: ['', '.js', '.jsx', '.ts', '.tsx'],
          },
        },
      },
    },
  },
  rules: esRules,
  ignorePatterns: [`!.storybook/config.js`],
  overrides: [
    {
      files: ['babel.config.js', 'scripts/**/*.{js,ts}', 'eslint/**/*.{js,ts}'],
      env: {
        node: true,
        es6: true,
      },
      globals: {
        __dirname: true,
      },
    },
    {
      files: ['**/__tests__/**/*.{js,jsx,ts,tsx}'],
      env: {
        jasmine: true,
        jest: true,
        browser: true,
        es6: true,
      },
    },
    {
      files: ['**/__tests__/**/*.{js,jsx,ts,tsx}'],
      env: {
        jasmine: true,
        jest: true,
        browser: true,
        es6: true,
      },
    },
    {
      files: ['packages/*/stories/**/*.{js,jsx,ts,tsx}'],
      env: {
        browser: true,
        es6: true,
      },
      globals: {
        module: true,
      },
    },
    {
      files: ['packages/*/src/**/*.{js,jsx,ts,tsx}'],
      env: {
        browser: true,
        es6: true,
      },
      globals: {
        process: true,
      },
    },
    {
      files: ['**/*.ts', '**/*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
      },
      extends: [
        'prettier',
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:import/react',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:@typescript-eslint/recommended',
        'prettier',
      ],
      plugins: ['react', 'import', 'prettier', 'react-hooks'],
      settings: {
        react: {
          version: 'detect',
        },
        'import/resolver': {
          webpack: {
            config: {
              resolve: {
                extensions: ['', '.js', '.jsx', '.ts', '.tsx'],
              },
            },
          },
        },
      },
      rules: {
        ...esRules,
        'react/prop-types': 0,
        '@typescript-eslint/no-explicit-any': 2,
        '@typescript-eslint/no-empty-function': 0,
        '@typescript-eslint/explicit-function-return-type': 0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
        '@typescript-eslint/no-unused-vars': ['error'],
      },
    },
  ],
};
