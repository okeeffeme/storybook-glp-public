/* global module */
module.exports = {
  singleQuote: true,
  trailingComma: 'es5',
  bracketSpacing: false,
  jsxBracketSameLine: true,
  endOfLine: 'auto',
  proseWrap: 'preserve',
};
