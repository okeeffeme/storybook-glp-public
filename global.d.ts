type ClassNames = Record<string, string>;

declare module '*.module.css' {
  const classNames: ClassNames;
  export = classNames;
}

declare module '*.module.scss' {
  const classNames: ClassNames;
  export = classNames;
}

declare module '*.css';
declare module '*.scss';

declare module '*.md' {
  const value: string;
  export = value;
}

declare module '*.png' {
  const value: string;
  export = value;
}

declare module '*.svg' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const value: any;
  export = value;
}

declare namespace NodeJS {
  interface Global {
    DISABLE_JEST_WORKER_CHECK?: boolean;
  }
}

interface Document {
  documentMode?: string;
}
