const presets = [
  [
    '@babel/preset-env',
    {
      modules: false,
    },
  ],
  '@babel/preset-react',
  '@babel/preset-typescript',
];
const plugins = [
  '@babel/plugin-proposal-export-default-from',
  '@babel/plugin-proposal-export-namespace-from',
  '@babel/plugin-transform-runtime',
  '@babel/plugin-proposal-class-properties',
  [
    'inline-react-svg',
    {
      svgo: {
        full: true,
        plugins: [
          {
            // https://github.com/svg/svgo#built-in-plugins
            name: 'preset-default',
            params: {
              overrides: {
                removeUnknownsAndDefaults: {
                  keepRoleAttr: true,
                  // aria-* attrs brings React warning message of unknown "'aria-*'" attribute name.
                  // https://github.com/airbnb/babel-plugin-inline-react-svg/pull/43
                  keepAriaAttrs: false
                },
                cleanupNumericValues: {
                  floatPrecision: 3,
                },
                removeDesc: {
                  removeAny: true,
                },
                minifyStyles: false,
                convertColors: false,
                removeViewBox: false,
                removeHiddenElems: false,
                mergePaths: false,
              },
            },
          },
          'cleanupListOfValues',
          'removeRasterImages',
          'sortAttrs',
        ],
      },
    },
  ],
];

module.exports = {
  presets,
  plugins,
};
