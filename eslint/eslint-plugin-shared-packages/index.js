const path = require('path');
const glob = require('glob');
const fs = require('fs');
const folders = glob.sync(path.resolve(__dirname, '../../packages/*'));

const isRelative = importPath => {
  return importPath.startsWith('.');
};

const confirmitPackages = new Set();
for (const folder of folders) {
  const packageJson = path.join(folder, 'package.json');

  if (fs.existsSync(packageJson)) {
    const pkg = require(packageJson);
    const packageName = pkg.name;
    confirmitPackages.add(packageName);
  }
}

/*
 * Extract package name from imported name
 * @confirmit/react-button => react-button
 * react-select/creatable => react-select
 */
const extractPackageName = importPath => {
  const hasNamespace = importPath.startsWith('@');
  const packageNameParts = importPath.split('/');

  return hasNamespace
    ? `${packageNameParts[0]}/${packageNameParts[1]}`
    : packageNameParts[0];
};

const extractPackageNameFromPath = filePath => {
  const root = path.resolve(__dirname, '../../packages');
  return filePath
    .substring(root.length + 1)
    .split(path.sep)
    .shift();
};

const isFromPackages = filePath => {
  const root = path.resolve(__dirname, '../../packages');

  return filePath.startsWith(root);
};

const isBuiltInPackage = importPath => {
  if (isRelative(importPath)) return;

  const packageName = extractPackageName(importPath);

  return confirmitPackages.has(packageName);
};

module.exports = {
  rules: {
    /* This rule is used to disallow imports like `import {Modal} from 'react-modal';`
     * in stories and tests. So no need to provide those packages in devDependencies.
     * Problem behind that is that in stories and tests modules can import each other, ie
     * modal package imports button, and button package imports modal, this causes cyclic dependencies in the package
     * which may cause different issues during build or compilation.
     * */
    'no-direct-imports-in-tests-and-stories': {
      create: function(context) {
        return {
          ImportDeclaration(node) {
            const fileName = context.getFilename();
            if (!fileName.match(/__tests__/) && !fileName.match(/stories/))
              return;

            const importPath = node.source.value;
            if (isBuiltInPackage(importPath)) {
              context.report({
                node,
                message: `Invalid import of "${extractPackageName(
                  importPath
                )}". Use relative import instead.`,
              });
            }
          },
        };
      },
    },
    /* This rule is used to disallow imports like
     * `import {Modal} from '../../packages/modal/src/components/modal';` in stories and tests.
     * Reason that imported component may be an internal one and imported by accident (named instead of default),
     * another reason that in stories and test we should use "official" api of components and
     * don't try to use internal imports
     * */
    'imports-from-src-index-in-tests-and-stories': {
      create: function(context) {
        return {
          ImportDeclaration(node) {
            const fileName = context.getFilename();
            if (!fileName.match(/__tests__/) && !fileName.match(/stories/)) {
              return;
            }

            const importPath = node.source.value;
            if (isRelative(importPath)) {
              const absoluteImportPath = path.resolve(
                path.dirname(fileName),
                importPath
              );

              /* Check only imports under /packages */
              if (!isFromPackages(absoluteImportPath)) {
                return;
              }

              const importedPackageName = extractPackageNameFromPath(
                absoluteImportPath
              );
              const packageName = extractPackageNameFromPath(fileName);

              /* Don't check imports of own modules */
              if (packageName === importedPackageName) {
                return;
              }

              /* Allow imports of scss files, since this is only used build-time and is compiled to css */
              if (absoluteImportPath.includes('.scss')) {
                return;
              }

              if (
                absoluteImportPath.match(/(src)((\/|\\)index(\.(ts|js))?)?$/)
              ) {
                return;
              }

              context.report({
                node,
                message: `Invalid import ${importPath}. Import path should ends with [packageName]/src/index.{js,ts}`,
              });
            }
          },
        };
      },
    },
    /* This rule is used to disallow imports like `import {Modal} from '../../packages/modal/dist/esm';`
     * Those imports are made by mistake.
     * */
    'no-imports-from-dist': {
      create: function(context) {
        return {
          ImportDeclaration(node) {
            const fileName = context.getFilename();
            if (!fileName.match(/packages/)) return;

            const importPath = node.source.value;
            if (importPath.includes('dist')) {
              context.report({
                node,
                message: `Invalid import in file "of "${importPath}". Imports from dist folders are not allowed.`,
              });
            }
          },
          ExportNamedDeclaration(node) {
            const fileName = context.getFilename();
            if (!fileName.match(/packages/)) return;

            if (!node.source || node.source.type === 'StringLiteral') return;

            const importPath = node.source.value;
            if (importPath.includes('dist')) {
              context.report({
                node,
                message: `Invalid import in file "of "${importPath}". Imports from dist folders are not allowed.`,
              });
            }
          },
        };
      },
    },
  },
};
