#!/usr/bin/env sh

# the above sets this file as a posix shell script

# set for debugging
# set -x

# https://sipb.mit.edu/doc/safe-shell/
set -euf

if ! command -v envsubst &> /dev/null; then
  echo "[INFO] I need envsubst but it's not installed. Installing...";
  apt-get update && apt-get install -y gettext-base
  echo "[INFO] done!";
fi

echo "[INFO] Building .npmrc from template"
cat ./ci/npmrc.template.txt | envsubst > ./.npmrc
cat ./.npmrc
echo "[INFO] done!"

