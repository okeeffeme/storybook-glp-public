param(
    [parameter(Mandatory=$true)] [string]$profileDirectory,
    [parameter(Mandatory=$true)] [string]$username,
    [parameter(Mandatory=$true)] [string]$password,
    [parameter(Mandatory=$true)] [string]$email
)

function Create-NPMConfigFile([string] $directory, [string]$username, [string]$password, [string]$email)
{
    $auth = 'Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($username):$($password)"));

    $response = invoke-webrequest -uri "http://artifactory.firmglobal.com/artifactory/api/npm/auth" -Headers @{ Authorization = $auth } -UseBasicParsing
    $npmrcRegistryUrl = "http://artifactory.firmglobal.com/artifactory/api/npm/npm-confirmit-virtual"
    $npmrcRegistry = "registry=$npmrcRegistryUrl"

    $directories = ,"$directory" + ((ls -dir "$directory\packages"  | Select-Object FullName) | % {$_.FullName })

    ForEach ($currentDir in $directories)
    {
        $filePath = "$currentDir\.npmrc"
        new-item -path $filePath -type file -force
        add-content $filePath $response.Content
        add-content $filePath $npmrcRegistry
    }

    write-host "NPM configuration written to $configFile"
}

Create-NPMConfigFile $profileDirectory $username $password $email
